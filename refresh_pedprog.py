import json
import os
import sys
import pathlib
from shutil import copyfile
# first convert TOC to json
os.system(
    "node latex/gentoc_json.js  ../docs/.vuepress/pedprog_toc.js > /tmp/toc.json")


def copy(src, dest):
    os.makedirs(os.path.dirname(dest), exist_ok=True)
    copyfile(src, dest)


def make(origfile, genfile):
    if not os.path.exists(genfile):
        print(
            "target doesn't exist: generating file from {}".format(origfile), file=sys.stderr)
        copy(origfile, genfile)
    else:
        if os.path.exists(origfile):
            if os.path.getmtime(origfile) > os.path.getmtime(genfile):
                print("source is fresh so generating new target from {}".format(
                    origfile), file=sys.stderr)
                copy(origfile, genfile)
            else:
                print("target is fresh and exists {}".format(
                    origfile), file=sys.stderr)
        else:
            print("wtf? {}".format(
                genfile), file=sys.stderr)


with open('/tmp/toc.json', encoding="utf-8") as f:
    data = json.load(f)
    for chapter in data:
        for section in chapter['children']:
            (dirname, filename) = os.path.split(section[0])
            if(not dirname.startswith('pedprog/model')):
                print("skipping not generated file {}".format(
                    section[0]), file=sys.stderr)
                continue
            origdir = dirname[len("pedprog/model"):]

            genfile = os.path.normpath("docs/{}/{}".format(dirname, filename))
            origfile = os.path.normpath("docs/{}/{}".format(origdir, filename))
            make(origfile, genfile)
# PDF
data = None
with open('/tmp/toc.json', encoding="utf-8") as f:
    data = json.load(f)

os.makedirs("latex/build", exist_ok=True)

# no copy the image files
# os.system("cp -r docs/pics latex/build/")

includes = []

MASTER = "master.tex"
os.chdir("latex/build")
os.system("cat ../template.tex > " + MASTER)
with open(MASTER, 'a') as outfile:
    for chapter in data:
        outfile.write("\\chapter{" + chapter['title'] + "}\n")
        for section in chapter['children']:
            mdFile = "../../docs/{}".format(section[0])
            toFile = mdFile.split('/')[-1].split('.')[0] + ".tex"
            myCmd = 'pandoc -f markdown -t latex ' + mdFile + ' > ' + toFile
            print(myCmd)
            os.system(myCmd)
            include = toFile.split('.')[0]
            outfile.write('\\include{'+include+'}\n')
os.system("echo '\\end{document}' >> " + MASTER)

os.system("xelatex " + MASTER)
os.system("xelatex " + MASTER)
os.chdir("../..")
os.system("cp latex/build/master.pdf docs/.vuepress/public/pedprog.pdf")
