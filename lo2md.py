import json
periods = ["1-2", "3-4", "5-6", "7-8", "9-10", "11-12"]
semesters = [1, 2, 3, 4]
subjects = ['Biológia-egészségtan',
            'Dráma és tánc',
            'Ének-zene',
            'Erkölcstan',
            'Etika',
            'Fizika',
            'Földrajz',
            'Hon- és népismeret',
            'Idegen nyelv',
            'Informatika',
            'Kémia',
            'Környezetismeret',
            'Magyar nyelv és irodalom',
            'Matematika',
            'Mozgóképkultúra és médiaismeret',
            'Technika, életvitel és gyakorlat',
            'Természetismeret',
            'Testnevelés és sport',
            'Történelem, társadalmi és állampolgári ismeretek',
            'Vizuális kultúra'
            ]
with open('learning_outcomes.json') as json_file:
    table = json.load(json_file)['learning_outcomes']


def filter_los_by_subject_period_semester(subject, period, semester):
    return [lo for lo in table if lo['semester'] ==
            semester and lo['period'] == period and lo['orig_subject'] == subject]


def toSemester(period, semester):
    year = int(period.split("-")[0])
    if (semester > 2):
        year += 1

    if (semester % 2 == 0):
        real_semester = "Évvége"
    else:
        real_semester = "Első félév"

    return (year, real_semester)


with open('docs/tanmenet.md', 'w') as tanmenet:
    for period in periods:
        for semester in semesters:
            (year, real_semester) = toSemester(period, semester)
            if real_semester == "Első félév":
                print("## {}. évfolyamszint".format(year), file=tanmenet)

            print("### {}".format(real_semester), file=tanmenet)
            for subject in subjects:
                los = filter_los_by_subject_period_semester(
                    subject, period, semester)
                if len(los) > 0:
                    print("#### {}".format(
                        subject), file=tanmenet)
                    for lo in los:
                        print("* {}".format(lo['lo']), file=tanmenet)
print("'docs/tanmenet.md' created")


with open('docs/tantargyak.md', 'w') as tantargyak:
    for subject in subjects:
        print("## {}".format(
            subject), file=tantargyak)
        for period in periods:
            for semester in semesters:
                (year, real_semester) = toSemester(period, semester)
                if semester == 1 or semester == 3:
                    if len(filter_los_by_subject_period_semester(
                        subject, period, semester)) + len(filter_los_by_subject_period_semester(
                            subject, period, semester+1)) > 0:
                        # csak akkor írjuk ki, ha van cucc
                        print("### {}. évfolyamszint".format(
                            year), file=tantargyak)
                los = filter_los_by_subject_period_semester(
                    subject, period, semester)
                if len(los) > 0:
                    print("#### {}".format(real_semester), file=tantargyak)
                    for lo in los:
                        print("* {}".format(lo['lo']), file=tantargyak)
print("'docs/tantargyak.md' created")
