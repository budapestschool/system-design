import json
import os
os.makedirs("latex/build", exist_ok=True)
# first convert TOC to json
os.system(
    "node latex/gentoc_json.js  ../docs/.vuepress/toc.js > latex/build/toc.json")

# no copy the image files
os.system("cp -r docs/pics latex/build/")

includes = []
with open('latex/build/toc.json', encoding="utf-8") as f:
    data = json.load(f)

print(data)

MASTER = "egyedi_program.tex"
os.chdir("latex/build")
os.system("cat ../egyedi_template.tex > " + MASTER)
with open(MASTER, 'a') as outfile:
    for chapter in data:
        outfile.write("\\chapter{" + chapter['title'] + "}\n")
        for section in chapter['children']:
            mdFile = "../../docs/{}".format(section[0])
            toFile = mdFile.split('/')[-1].split('.')[0] + ".tex"
            myCmd = 'pandoc -f --filter ../filter.py -f markdown+ascii_identifiers -t latex ' + mdFile + ' > ' + toFile
            print(myCmd)
            os.system(myCmd)
            include = toFile.split('.')[0]
            outfile.write('\\include{'+include+'}\n')
os.system("echo '\\end{document}' >> " + MASTER)

os.system("latexmk -xelatex " + MASTER)
os.chdir("../..")
os.system("cp latex/build/egyedi_program.pdf" +  docs/.vuepress/public/bps_model.pdf")
