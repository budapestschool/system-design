import re

BOF = 1
LOOKING  = 2
IN_LOS = 3
IN_LO = 4
state  = BOF
buffer = ""
with open('test.txt', 'r') as content_file:
    for line in content_file.readlines():

      if state == BOF:
          print("subject", line)
          state = LOOKING
      elif state == LOOKING and line.upper() == "TANULÁSI EREDMÉNYEK\n":
        state = IN_LOS
      elif state == IN_LOS and line[0] == "-":
        state = IN_LO 
        buffer = line.strip()
      elif state == IN_LO and (line[0] == "-" or line.upper() == "FEJLESZTÉSI FELADATOK ÉS ISMERETEK\n" or line == "A témakör tanulása eredményeként a tanuló:\n"):
  
        if buffer[-1] == ";":
            buffer = buffer[:-1] + '.'
        if buffer[0] == "-":
            buffer = buffer[1:]
        buffer = buffer.strip()
        print(buffer.capitalize())
        if line[0] == "-":
          buffer = line.strip()
        elif line.strip().upper() == "FEJLESZTÉSI FELADATOK ÉS ISMERETEK\n":
          state = LOOKING
        
      elif state == IN_LO and line[0] != "-" and line != "\n":
        buffer += " " + line.strip()

                