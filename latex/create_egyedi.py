import reader
import os
import re

# os.makedirs("latex/build", exist_ok=True)

TOC_JSON_FILE = "build/toc.json"
TOC_JS_FILE = "../docs/.vuepress/toc.js"
if not os.path.exists(TOC_JSON_FILE):
    generate = "N"
elif os.stat(TOC_JSON_FILE).st_mtime < os.stat(TOC_JS_FILE).st_mtime:
    generate = "M"
else:
    generate = None
if generate:
    print("{} {}".format(generate, TOC_JSON_FILE))
    os.system("node ./gentoc_json.js  ../docs/.vuepress/toc.js > build/toc.json")

skipped_chapters = ["Szervezet működése"]


def convert(section_file):
    mdFile = "../../{}".format(section_file)
    toFile = mdFile.split("/")[-1].split(".")[0] + ".tex"
    # we convert only if mdFile is newer than toFile

    if not os.path.exists(toFile):
        generate = "N"
    elif os.stat(toFile).st_mtime < os.stat(mdFile).st_mtime:
        generate = "M"
    else:
        generate = None
    if generate:
        myCmd = (
            "pandoc --filter ../filter.py -f markdown+ascii_identifiers -t latex "
            + mdFile
            + " > "
            + toFile
        )
        print("{} {}".format(generate, toFile))
        os.system(myCmd)
    return toFile


def print_chapters(outfile, chapters_to_print):
    for chapter_title, sections in chapters_to_print:
        if chapter_title in skipped_chapters:
            continue

        outfile.write("\\chapter{" + chapter_title + "}\n")
        for _, section_file in sections:
            toFile = convert(section_file)
            open(toFile, "r").read()

            outfile.write(open(toFile, "r").read())
            # outfile.write("\\input{" + toFile + "}\n")


def create_latex(template, master, backmatter, school_specific, chapters_to_print):
    os.system("cat ../{} > {}".format(template, master))
    os.system("cp ../references.bib .")
    with open(master, "a") as outfile:

        outfile.write("\\mainmatter\n")
        if school_specific is not None:
            outfile.write("\\part{BPS Code Technikum}\n")
            print_chapters(outfile, school_specific)
            outfile.write("\\part{A Budapest School Model}\n")
        print_chapters(outfile, chapters_to_print)
        outfile.write("\\backmatter\n")
        print_chapters(outfile, backmatter)
        outfile.write("\\printbibliography\n\\end{document}")

    os.system("latexmk -xelatex  -interaction=nonstopmode " + master)
    i = 6 / 0


def md_to_tex_with_template(template_file, section_file, to_file):
    converted_file = convert(section_file)
    content = open(converted_file, "r").read()
    template = open(template_file, "r").read()
    content = template.replace("{{CONTENT}}", content)
    with open(to_file, "w") as outfile:
        outfile.write(content)


def make_toc(TECHNIKUM):

    chapters = reader.chapters()
    school_specific = None
    model_specific = chapters
    if TECHNIKUM:
        front_matter_chapters = []

        school_specific = [
            (
                "A technikum sajátosságai",
                [
                    ("foo", "docs/iskola-tipusok/code21.md"),
                    ("foo", "docs/tantargyak-technikum.md"),
                ],
            )
        ] + [(chap, secs) for (chap, secs) in chapters if chap == "Működési feltételek"]

        model_specific = [
            (
                title,
                [
                    (title2, file_name)
                    for (title2, file_name) in sections
                    if file_name not in ["docs/iskola-celja/code21.md"]
                ],
            )
            for (title, sections) in chapters
            if title
            not in [
                "Tanulási eredmények",
                "Működési feltételek",
                "Különböző iskolatípusok",
            ]
        ]

    else:
        front_matter_chapters = []

        model_specific = [
            (
                title,
                [
                    (title2, file_name)
                    for (title2, file_name) in sections
                    if file_name
                    not in [
                        "docs/iskola-celja/code21.md",
                        "docs/tantargyak-technikum.md",
                    ]
                ],
            )
            for (title, sections) in chapters
            if title
            not in [
                #  "Tanulási eredmények",
                #  "Működési feltételek",
                "Különböző iskolatípusok",
            ]
        ]
        for i, (title, sections) in enumerate(model_specific):
            if title == "Helyi tanterv":
                sections.append(("Tanulási eredmények", "docs/tantargyak-altgim.md"))
                model_specific[i] = (title, sections)

    return (front_matter_chapters, school_specific, model_specific)


def create_main_file(TECHNIKUM):
    (front_matter_chapters, school_specific, model_specific) = make_toc(TECHNIKUM)
    if TECHNIKUM:
        create_latex(
            "egyedi_template-technikum.tex",
            "egyedi-program-technikum.tex",
            front_matter_chapters,
            school_specific,
            model_specific,
        )
    else:
        create_latex(
            "egyedi_template-altgim.tex",
            "egyedi-program-altgim.tex",
            front_matter_chapters,
            school_specific,
            model_specific,
        )

    if not TECHNIKUM:
        md_to_tex_with_template(
            "../miben_template.tex",
            "/docs/jogszabalyok/miben-terunk-el.md",
            "miben-terunk-el-level.tex",
        )
        md_to_tex_with_template(
            "../miben_template.tex",
            "/docs/jogszabalyok/miert-terunk-el.md",
            "miert-terunk-el-level.tex",
        )

        md_to_tex_with_template(
            "../miben_template.tex",
            "/docs/jogszabalyok/miben-nem-terunk-el.md",
            "miben-nem-terunk-el-level.tex",
        )

        os.system("xelatex -interaction=batchmode miben-terunk-el-level.tex")
        os.system("xelatex -interaction=batchmode miben-terunk-el-level.tex")
        os.system("xelatex -interaction=batchmode miert-terunk-el-level.tex")
        os.system("xelatex  -interaction=batchmode miert-terunk-el-level.tex")
        os.system("latexmk -xelatex miben-nem-terunk-el-level.tex")

        os.system(
            "pandoc -f latex -t docx -o egyedi-program-altgim.docx egyedi-program-altgim.tex"
        )

        os.system(
            "zip kuldendo-emmi.zip egyedi-program-altgim.pdf miben-terunk-el-level.pdf miben-nem-terunk-el-level.pdf miert-terunk-el-level.pdf egyedi-program-altgim.docx"
        )


def create_epub():
    (front_matter_chapters, school_specific, model_specific) = make_toc(False)
    files = []
    for (title, sections) in model_specific:
        for (title, file) in sections:
            files.append(file)

    os.system(
        "pandoc -o tanulni-tanulunk.epub --toc --toc-depth=2 --metadata-file=../../epub/metadata.yml "
        + " ".join(["../../" + f for f in files])
    )


def count_words_in_markdown(markdown):
    text = markdown

    # Comments
    text = re.sub(r"<!--(.*?)-->", "", text, flags=re.MULTILINE)
    # Tabs to spaces
    text = text.replace("\t", "    ")
    # More than 1 space to 4 spaces
    text = re.sub(r"[ ]{2,}", "    ", text)
    # Footnotes
    text = re.sub(r"^\[[^]]*\][^(].*", "", text, flags=re.MULTILINE)
    # Indented blocks of code
    text = re.sub(r"^( {4,}[^-*]).*", "", text, flags=re.MULTILINE)
    # Custom header IDs
    text = re.sub(r"{#.*}", "", text)
    # Replace newlines with spaces for uniform handling
    text = text.replace("\n", " ")
    # Remove images
    text = re.sub(r"!\[[^\]]*\]\([^)]*\)", "", text)
    # Remove HTML tags
    text = re.sub(r"</?[^>]*>", "", text)
    # Remove special characters
    text = re.sub(r"[#*`~\-–^=<>+|/:]", "", text)
    # Remove footnote references
    text = re.sub(r"\[[0-9]*\]", "", text)
    # Remove enumerations
    text = re.sub(r"[0-9#]*\.", "", text)

    return (len(text.split()), len(text))


def generate_wordcount():
    os.chdir("build")
    (front_matter_chapters, school_specific, model_specific) = make_toc(False)

    for title, sections in model_specific:
        for subtitle, f in sections:
            markdown = open("../../" + f, "r").read()
            wc, cc = count_words_in_markdown(markdown)
            print(f"{title}\t{subtitle}\t{wc}\t{cc}")
    os.chdir("..")


def build_all():
    os.system("cp -r ../docs/pics build/")
    os.chdir("build")
    print("we are in ", os.getcwd())
    # create_epub()
    # create_main_file(True)
    create_main_file(False)
    os.chdir("../..")

    os.system(
        "cp latex/build/tanulni-tanulunk.epub docs/.vuepress/public/tanulni-tanulunk.epub"
    )
    os.system(
        "cp latex/build/egyedi-program-altgim.pdf docs/.vuepress/public/egyedi-program-altgim.pdf"
    )
    os.system(
        "cp latex/build/egyedi-program-technikum.pdf docs/.vuepress/public/egyedi-program-technikum.pdf"
    )


build_all()
