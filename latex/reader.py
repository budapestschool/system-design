import json
import os


def chapters():
    with open("toc.json", encoding="utf-8") as f:
        data = json.load(f)

    return [
        (
            chapter["title"],
            [
                (section[1], "docs/{}".format(section[0]))
                for section in chapter["children"]
                if type(section) is list
            ],
        )
        for chapter in data
    ]


def create_docx():
    n = 1
    os.makedirs("build/docx", exist_ok=True)
    for (title, sections) in chapters():
        dir = "build/docx/{}. {}".format(n, title)
        os.makedirs(dir, exist_ok=True)
        m = 1
        for (sectitle, mdfile) in sections:
            myCmd = 'pandoc -f markdown -t docx -o "{}/{}.{}.docx"  {}'.format(
                dir, m, sectitle, mdfile
            )
            print(myCmd)
            m += 1
            os.system(myCmd)
        n += 1
