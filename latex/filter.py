#!/usr/bin/env python

"""
Pandoc filter to convert all level 2+ headings to paragraphs with
emphasized text.
"""
import panflute as pf
import re


def latex(s):
    return pf.RawInline(s, format="latex")


p_cit = re.compile(r"\(([^,]+), +([0-9]+)\)")


def is_citation(s):
    m = p_cit.search(s)
    if m:
        return m.groups()[0] + m.groups()[1].replace("#", ":")
    else:
        return None

# ../../tanari-szerepek.md#tanulasszervezo -> tanari-szerepek#tanulasszervezo
p_internal = re.compile(r"[^ ]+\.md#([^ ]+)")


def is_internal_link(s):
    m = p_internal.search(s)
    if m:
        return m.groups()[0]

def myemph(e, doc):
    if not hasattr(doc, "intip"):
        doc.intip = False

    if type(e) == pf.Link and doc.format == "latex":
        linktxt = pf.stringify(pf.Para(*e.content)).strip()
        url = e.url
        citation = is_citation(linktxt)
        internal = is_internal_link(url)
        if citation:
            return pf.Span(latex("\\autocite{"), pf.Str(citation), latex("}"))
        elif internal:
            return latex(
                linktxt
                + " (\\ref{"
                + internal
                + "}.~fejezet \\apageref{"
                + internal
                + "}.~oldalon)"
            )
    elif type(e) == pf.Para:
        first = e.content[0]
        if type(first) == pf.Str and first.text == ":::":
            return []

    # elif type(e) == pf.Header and doc.format == "latex":
    #     e.parent.content.insert(
    #         e.index + 1, pf.RawBlock("\\label{" + (e.identifier) + "}", format='latex')
    #     )
    elif type(e) == pf.Image and doc.format == "latex":
        # "../../pics/foo/bar.jpeg" -> "pics/foo/bar.jpg"
        url = None
        components = e.url.split("/")
        components.reverse()
        for c in components:
            if url is None:
                url = c
            else:
                url = c + "/" + url
            if c == "pics":
                break

        e.url = url
        return e


if __name__ == "__main__":
    pf.toJSONFilter(myemph)
