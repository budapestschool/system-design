module.exports = {
	sidebar:
		[
			{
				children: [

					[
						"pedprog/model/iskola-celja/mi-a-celunk.md",
						"Az iskola célja"
					],
					[
						"pedprog/model/iskola-celja/alapelvek.md",
						"A tanulni tanulás hét pillére"
					],
					[
						"pedprog/model/iskola-celja/emberkep.md",
						"Emberkép"
					],
				/*	[
						"iskola-celja/kapcsolodasok.md",
						"Kapcsolat a hazai és nemzetközi oktatási reformokkal"
					],
				*/	[
						"pedprog/model/tanulas-megkozelitese/pedagogiai-pszichologiai-hatter.md",
						"Pedagógiai és pszichológiai háttér"
					],
					[
						"pedprog/model/tanulas-megkozelitese/pedagogiai-modszerek.md",
						"Pedagógiai módszerek"
					],
					[
						"pedprog/model/tanulas-megkozelitese/kiemelt-fejlesztesi-iranyelvek.md",
						"Kiemelt fejlesztési irányelvek"
					],

					[
						"pedprog/model/tanulas-megkozelitese/kiemelt-figyelmet-igenylo-gyerekek.md",
						"A kiemelt figyelmet igénylő gyerekek"
					]
				],
				collabsable: "true",
				title: "Nevelési program"
			},
			{
				collabsable: "true",
				title: "Helyi Tanterv",
				children: [
					[
						"pedprog/valasztott_kerettanterv.md",
						"Választott kerettanterv"

					],
					[
						"pedprog/model/tankonyvek.md",
						"Tankönyvek kiválasztása"
					],
					[
						"pedprog/model/tanulasi-elmeny/visszajelzes-ertekeles.md",
						"Visszajelzés, értékelés"
					],
					[
						"pedprog/torvenyi-ertekeles.md",
						"Osztályzatok, érdemjegyek"
					],
					[
						"pedprog/model/biztonsagos_kornyezet.md",
						"Biztonságos tanulási környezet"
					],

					[
						"pedprog/model/szemelyisegfejlesztes.md",
						"A személyiség- és egészségfejlesztés"
					],
					[
						"pedprog/model/mindennapos-testmozgas.md",
						"Mindennapos testmozgás"
					],
					[
						"pedprog/model/kozossegfejlesztes.md",
						"A közösségfejlesztés"
					],
					[
						"pedprog/model/elsosegely.md",
						"Elsősegély-nyújtási alapismeretek"
					],
					[
						"pedprog/model/eselyegyenloseg.md",
						"Út az esélyegyenlőség felé"
					],
					[
						"pedprog/model/onkentesszolgalat.md",
						"Önkéntes közösségi szolgálat"
					],
					[
						"pedprog/model/nemzetisegek.md",
						"Nemzetiségek megismerése"
					],
					[
						"pedprog/model/kornyezeti-neveles.md",
						"Környezeti nevelés"
					],

				]
			}
		]
}