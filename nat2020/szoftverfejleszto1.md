Adott kapcsolási rajz alapján egyszerűbb áramköröket épít próbapanel segítségével vagy forrasztásos technológiával.
Ismeri az elektronikai alapfogalmakat, kapcsolódó fizikai törvényeket, alapvető alkatrészeket és kapcsolásokat.
A funkcionalitás biztosítása mellett törekszik az esztétikus kialakításra (pl. minőségi forrasztás, egyenletes alkatrész sűrűség, olvashatóság).
Az elektromos berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
Alapvető villamos méréseket végez önállóan a megépített áramkörökön.
Ismeri az elektromos mennyiségek mérési metódusait, a mérőműszerek használatát.
Elvégzi a számítógépen és a mobil eszközökön az operációs rendszer (pl. Windows, Linux, Android, iOS), valamint az alkalmazói szoftverek telepítését, frissítését és alapszintű beállítását. Grafikus felületen, valamint parancssorban használja a Windows, és Linux operációs rendszerek alapszintű parancsait és szolgáltatásait (pl. állomány- és könyvtárkezelési műveletek, jogosultságok beállítása, szövegfájlokkal végzett műveletek, folyamatok kezelése).
Ismeri a számítógépen és a mobil informatikai eszközökön használt operációs rendszerek telepítési és frissítési módjait, alapvető parancsait és szolgáltatásait, valamint alapvető beállítási lehetőségeit.
Törekszik a felhasználói igényekhez alkalmazkodó szoftverkörnyezet kialakítására.
Önállóan elvégzi a kívánt szoftverek telepítését, szükség esetén gondoskodik az eszközön korábban tárolt adatok biztonsági mentéséről.
Elvégzi a PC perifériáinak csatlakoztatását, szükség esetén új alkatrészt szerel be vagy alkatrészt cserél egy számítógépben.
Ismeri az otthoni és irodai informatikai környezetet alkotó legáltalánosabb összetevők (PC, nyomtató, mobiltelefon, WiFi router stb.) szerepét, alapvető működési módjukat. Ismeri a PC és a mobil eszközök főbb alkatrészeit (pl. alaplap, CPU, memória) és azok szerepét.
Törekszik a végrehajtandó műveletek precíz és előírásoknak megfelelő elvégzésére.
Az informatikai berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
Alapvető karbantartási feladatokat lát el az általa megismert informatikai és távközlési berendezéseken (pl. szellőzés és csatlakozások ellenőrzése, tisztítása).
Tisztában van vele, hogy miért szükséges az informatikai és távközlési eszközök rendszeres és eseti karbantartása. Ismeri legalapvetőbb karbantartási eljárásokat.
A hibamentes folyamatos működés elérése érdekében fontosnak tartja a megelőző karbantartások elvégzését.
Otthoni vagy irodai hálózatot alakít ki WiFi router segítségével, elvégzi WiFi router konfigurálását, a vezetékes- és vezeték nélküli eszközök (PC, mobiltelefon, set-top box stb.), csatlakoztatását és hálózati beállítását.
Ismeri az informatikai hálózatok felépítését, alapvető technológiáit (pl. Ethernet), protokolljait (pl. IP, HTTP) és szabványait (pl. 802.11-es WiFi szabványok). Ismeri az otthoni és irodai hálózatok legfontosabb összetevőinek (kábelezés, WiFi router, PC, mobiltelefon stb.) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
Törekszik a felhasználói igények megismerésére, megértésére, és szem előtt tartja azokat a hálózat kialakításakor.
Néhány alhálózatból álló kis- és közepes vállalati hálózatot alakít ki forgalomirányító és kapcsoló segítségével, elvégzi az eszközök alapszintű hálózati beállításait (pl. forgalomirányító interfészeinek IP-cím beállítása, alapértelmezett átjáró beállítása).
Ismeri a kis- és közepes vállalati hálózatok legfontosabb összetevőinek (pl. kábelrendező szekrény, kapcsoló, forgalomirányító) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
Alkalmazza a hálózatbiztonsággal kapcsolatos legfontosabb irányelveket (pl. erős jelszavak használata, vírusvédelem alkalmazása, tűzfal használat).
Ismeri a fontosabb hálózatbiztonsági elveket, szabályokat, támadás típusokat, valamint a szoftveres és hardveres védekezési módszereket.
Megkeresi és elhárítja az otthoni és kisvállalati informatikai környezetben jelentkező hardveres és szoftveres hibákat.
Ismeri az otthoni és kisvállalati informatikai környezetben leggyakrabban felmerülő hibákat (pl. hibás IP-beállítás, kilazult csatlakozó) és azok elhárításának módjait.
Önállóan behatárolja a hibát. Egyszerűbb problémákat önállóan, összetettebbeket szakmai irányítással hárít el.
Internetes források és tudásbázisok segítségével követi, valamint feladatainak elvégzéséhez lehetőség szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
Naprakész információkkal rendelkezik a legmodernebb információs technológiákkal és trendekkel kapcsolatban.
Nyitott és érdeklődő a legmodernebb információs technológiák és trendek iránt.
Önállóan szerez információkat a témában releváns szakmai platformokról.
Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével.
Ismeri a HTML5, a CSS3 alapvető elemeit, a stíluslapok fogalmát, felépítését. Érti a reszponzív megjelenítéshez használt módszereket, keretrendszerek előnyeit, a reszponzív webdizájn alapelveit.
A felhasználói igényeknek megfelelő funkcionalitás és design összhangjára törekszik.
Önállóan létrehozza és megformázza a weboldalt.
Munkája során jelentkező problémák kezelésére vagy folyamatok automatizálására egyszerű alkalmazásokat készít Python programozási nyelv segítségével.
Ismeri a Python nyelv elemeit, azok céljait (vezérlési szerkezetek, adatszerkezetek, változók, aritmetikai és logikai kifejezések, függvények, modulok, csomagok). Ismeri az algoritmus fogalmát, annak szerepét.
Jól átlátható kódszerkezet kialakítására törekszik.
Önállóan készít egyszerű alkalmazásokat.
Git verziókezelő rendszert, valamint fejlesztést és csoportmunkát támogató online eszközöket és szolgáltatásokat (pl.: GitHub, Slack, Trello, Microsoft Teams, Webex Teams) használ.
Ismeri a Git, valamint a csoportmunkát támogató eszközök és online szolgáltatások célját, működési módját, legfontosabb funkcióit.
Törekszik a feladatainak megoldásában a hatékony csoportmunkát támogató online eszközöket kihasználni.
A Git verziókezelőt, valamint a csoportmunkát támogató eszközöket és szolgáltatásokat önállóan használja.
Társaival hatékonyan együttműködve, csapatban dolgozik egy informatikai projekten. A projektek végrehajtása során társaival tudatosan és célirányosan kommunikál.
Ismeri a projektmenedzsment lépéseit (kezdeményezés, követés, végrehajtás, ellenőrzés, dokumentáció, zárás).
Más munkáját és a csoport belső szabályait tiszteletben tartva, együttműködően vesz részt a csapatmunkában.
A projektekben irányítás alatt, társaival közösen dolgozik. A ráosztott feladatrészt önállóan végzi el.
Munkája során hatékonyan használja az irodai szoftvereket.
Ismeri az irodai szoftverek főbb funkcióit, felhasználási területeit.
Az elkészült termékhez prezentációt készít és bemutatja, előadja azt munkatársainak, vezetőinek, ügyfeleinek.
Ismeri a hatékony prezentálás szabályait, a prezentációs szoftverek lehetőségeit.
Törekszik a tömör, lényegre törő, de szakszerű bemutató összeállítására.
A projektcsapat tagjaival egyeztetve, de önállóan elkészíti az elvégzett munka eredményét bemutató prezentációt.
