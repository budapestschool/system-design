Használja a Git verziókezelő rendszert, valamint a fejlesztést támogató csoportmunkaeszközöket és szolgáltatásokat (pl. GitHub, Slack, Trello, Microsoft Teams, Webex Teams).
Ismeri a legelterjedtebb csoportmunkaeszközöket, valamint a Git verziókezelőrendszer szolgáltatásait.
Igyekszik munkatársaival hatékonyan, igazi csapatjátékosként együtt dolgozni. Törekszik a csoporton belül megkapott feladatok precíz, határidőre történő elkészítésére, társai segítésére.
Szoftverfejlesztési projektekben irányítás alatt dolgozik, a rábízott részfeladatok megvalósításáért felelősséget vállal.
Az általa végzett szoftverfejlesztési feladatok esetében kiválasztja a legmegfelelőbb technikákat, eljárásokat és módszereket.
Elegendő ismerettel rendelkezik a meghatározó szoftverfejlesztési technológiák (programozási nyelvek, keretrendszerek, könyvtárak stb.), illetve módszerek erősségeiről és hátrányairól.
Nyitott az új technológiák megismerésére, tudását folyamatosan fejleszti.
Önállóan dönt a fejlesztés során használt technológiákról és eszközökről.
A megfelelő kommunikációs forma (e-mail, chat, telefon, prezentáció stb.) kiválasztásával munkatársaival és az ügyfelekkel hatékonyan kommunikál műszaki és egyéb információkról magyarul és angolul.
Ismeri a különböző kommunikációs formákra (e-mail, chat, telefon, prezentáció stb.) vonatkozó etikai és belső kommunikációs szabályokat.
Angol nyelvismerettel rendelkezik (KER B1 szint). Ismeri a gyakran használt szakmai kifejezéseket angolul.
Kommunikációjában konstruktív, együttműködő, udvarias. Feladatainak a felhasználói igényeknek leginkább megfelelő, minőségi megoldására törekszik.
Felelősségi körébe tartozó feladatokkal kapcsolatban a vállalati kommunikációs szabályokat betartva, önállóan kommunikál az ügyfelekkel és munkatársaival.
Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével. Kereső optimalizálási beállításokat alkalmaz.
Ismeri a HTML5 és a CSS3 szabvány alapvető nyelvi elemeit és eszközeit (strukturális és szemantikus HTML-elemek, attribútumok, listák, táblázatok, stílus jellemzők és függvények). Ismeri a a reszponzív webdizájn alapelveit és a Bootstrap keretrendszer alapvető szolgáltatásait.
Törekszik a weblapok igényes és a használatot megkönnyítő kialakítására.
Kisebb webfejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
Egyszerűbb webhelyek dinamikus viselkedését (eseménykezelés, animáció stb.) biztosító kódot, készít JavaScript nyelven.
Alkalmazási szinten ismeri a JavaScript alapvető nyelvi elemeit, valamint az aszinkron programozás és az AJAX technológia működési elvét. Tisztában van a legfrissebb ECMAScript változatok (ES6 vagy újabb) hatékonyság növelő funkcióival.
Egyszerűbb JavaScript programozási feladatokat önállóan végez el.
RESTful alkalmazás kliens oldali komponensének fejlesztését végzi JavaScript nyelven.
Tisztában van a REST szoftverarchitektúra elvével, alkalmazás szintjén ismeri az AJAX technológiát.
A tiszta kód elveinek megfelelő, megfelelő mennyiségű megjegyzéssel ellátott, kellőképpen tagolt, jól átlátható, kódot készít.
Ismeri a tiszta kód készítésének alapelveit.
Törekszik arra, hogy az elkészített kódja jól átlátható, és mások számára is értelmezhető legyen.
Adatbázis-kezelést is végző konzolos vagy grafikus felületű asztali alkalmazást készít magas szintű programozási nyelvet (C#, Java) használva.
Ismeri a választott magas szintű programozási nyelv alapvető nyelvi elemeit, illetve a hozzá tartozó fejlesztési környezetet.
Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
Kisebb asztali alkalmazás-fejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
Adatkezelő alkalmazásokhoz relációs adatbázist tervez és hoz létre, többtáblás lekérdezéseket készít.
Tisztában van a relációs adatbázis-tervezés és -kezelés alapelveivel. Haladó szinten ismeri a különböző típusú SQL lekérdezéseket, azok nyelvi elemeit és lehetőségeit.
Törekszik a redundanciamentes, világos szerkezetű, legcélravezetőbb kialakítású adatbázis szerkezet megvalósítására.
Kisebb projektekhez néhány táblás adatbázist önállóan tervez meg, nagyobb projektekben a biztosított adatbáziskörnyezetet használva önállóan valósít meg lekérdezéseket.
Önálló- vagy komplex szoftverrendszerek részét képző kliens oldali alkalmazásokat fejleszt mobil eszközökre.
Ismeri a választott mobil alkalmazás fejlesztésére alkalmas nyelvet és fejlesztői környezetet. Tisztában van a mobil alkalmazásfejlesztés alapelveivel.
Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
Kisebb projektek mobil eszközökre optimalizált kliens oldali alkalmazását önállóan megvalósítja meg.
Webes környezetben futtatható kliens oldali (frontend) alkalmazást készít JavaScript keretrendszer (pl. React, Vue, Angular) segítségével.
Érti a frontend fejlesztésre szolgáló JavaScript keretrendszerek célját. Meg tudja nevezni a 3-4 legelterjedtebb keretrendszert. Alkalmazás szintjén ismeri a könyvtárak és modulok kezelését végző csomagkezelő rendszereket (package manager, pl. npm, yarn). Ismeri a választott JavaScript keretrendszer működési elvét, nyelvi és strukturális elemeit.
Törekszik maximálisan kihasználni a választott keretrendszer előnyeit, követi az ajánlott fejlesztési mintákat.
Kisebb frontend alkalmazásokat önállóan készít el, nagyobb projektekben irányítás mellett végzi el a kijelölt komponensek fejlesztését.
RESTful alkalmazás adatbázis-kezelési feladatokat is ellátó szerveroldali komponensének (backend) fejlesztését végzi erre alkalmas nyelv vagy keretrendszer segítségével (pl. Node.js, Spring, Laravel).
Érti a RESTful szoftverarchitektúra lényegét. Tisztában van legalább egy backend készítésére szolgáló nyelv vagy keretrendszer működési módjával, nyelvi és strukturális elemeivel. Alkalmazás szintjén ismeri az objektum-relációs leképzés technológiát (ORM).
Igyekszik backend működését leíró precíz, a frontend fejlesztők számára könnyen értelmezhető dokumentáció készítésére.
Kisebb backend alkalmazásokat önállóan készít el, nagyobb projektekben részletes specifikációt követve, irányítás mellett végzi el a kijelölt komponensek fejlesztését.
Objektum orientált (OOP) programozási módszertant alkalmazó asztali, webes és mobil alkalmazást készít.
Ismeri az objektumorientált programozás elvét, tisztában van az öröklődés, a polimorfizmus, a metódus/konstruktor túlterhelés fogalmával.
Törekszik az OOP technológia nyújtotta előnyök kihasználására, valamint igyekszik követni az OOP irányelveket és ajánlásokat.
Kisebb projektekben önállóan tervezi meg a szükséges osztályokat, nagyobb projektekben irányítás mellett, a projektben a projektcsapat által létrehozott osztálystruktúrát használva, illetve azt kiegészítve végzi a fejlesztést.
Tartalomkezelő rendszer (CMS, pl. WordPress) segítségével webhelyet készít, egyéni problémák megoldására saját beépülőket hoz létre.
Ismeri a tartalomkezelő-rendszerek célját és alapvető szolgáltatásait. Ismeri a beépülők célját és alkalmazási területeit.
Törekszik az igényes kialakítású és a felhasználók számára könnyű használatot biztosító webhelyek kialakításra.
Kevésbé összetett portálokat igényes vizuális megjelenést biztosító sablonok, valamint magas funkcionalitást biztosító beépülők használatával önállóan valósít meg. Összetettebb projekteken irányítás mellett, grafikus tervezőkkel, UX szakemberekkel és más fejlesztőkkel együttműködve dolgozik.
Manuális és automatizált szoftvertesztelést végezve ellenőrzi a szoftver hibátlan működését, dokumentálja a tesztek eredményét.
Ismeri a unit tesztelés, valamint más tesztelési, hibakeresési technikák alapelveit és alapvető eszközeit.
Törekszik a mindenre kiterjedő, az összes lehetséges hibát felderítő tesztelésre, valamint a tesztek körültekintő dokumentálására.
Saját fejlesztésként megvalósított kisebb projektekben önállóan végzi a tesztelést, tesztelői szerepben nagyobb projektekben irányítás mellett végez meghatározott tesztelési feladatokat.
Szoftverfejlesztés vagy -tesztelés során felmerülő problémákat old meg és hibákat hárít el webes kereséssel és internetes tudásbázisok használatával (pl. Stack Overflow).
Ismeri a hibakeresés szisztematikus módszereit, a problémák elhárításának lépéseit.
Ismeri a munkájához kapcsolódó internetes keresési módszereket és tudásbázisokat.
Törekszik a hibák elhárítására, megoldására, és arra, hogy azokkal lehetőség szerint ne okozzon újabb hibákat.
Internetes információszerzéssel önállóan old meg problémákat és hárít el hibákat.
Munkája során hatékonyan használja az irodai szoftvereket, műszaki tartalmú dokumentumokat és bemutatókat készít.
Ismeri az irodai szoftverek haladó szintű szolgáltatásait.
Precízen készíti el a műszaki tartalmú dokumentációkat, prezentációkat. Törekszik arra, hogy a dokumentumok könnyen értelmezhetők és mások által is szerkeszthetők legyenek.
Felelősséget vállal az általa készített műszaki tartalmú dokumentációkért.
Munkája során cél szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtaulizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
Alapszintű alkalmazási szinten ismeri a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
Nyitott az új technológiák megismerésére, és törekszik azok hatékony, a felhasználói igényeknek és a költséghatékonysági elvárásoknak megfelelő felhasználására a szoftverfejlesztési feladatokban.
Részt vesz szoftverrendszerek ügyfeleknél történő bevezetésében, a működési környezetet biztosító IT-környezet telepítésében és beállításában.
Ismeri a számítógép és a mobil informatikai eszközök felépítését (főbb komponenseket, azok feladatait) és működését. Ismeri az eszközök operációs rendszerének és alkalmazói szoftvereinek telepítési és beállítási lehetőségeit.
A szoftverrendszerek bevezetése és a működési környezet kialakítása során törekszik az ügyfelek elvárásainak megfelelni, valamint tiszteletben tartja az ügyfél vállalati szabályait.
Az elvégzett eszköz- és szoftvertelepítésekért felelősséget vállal.
A szoftverfejlesztés és tesztelési munkakörnyezetének kialakításához beállítja a hálózati eszközöket, elvégzi a vezetékes és vezetéknélküli eszközök csatlakoztatását és hálózatbiztonsági beállítását. A fejlesztett szoftverben biztonságos, HTTPS protokollt használó webes kommunikációt valósít meg.
Ismeri az IPv4 és IPv6 címzési rendszerét és a legalapvetőbb hálózati protokollok szerepét és működési módját (IP, TCP, UDP, DHCP, HTTP, HTTPS, telnet, ssh, SMTP, POP3, IMAP4, DNS, TLS/SSL stb.). Ismeri a végponti berendezések IP-beállítási és hibaelhárítási lehetőségeit. Ismeri az otthoni és kisvállalati hálózatokban működő multifunkciós forgalomirányítók szolgáltatásait, azok beállításának módszereit.
