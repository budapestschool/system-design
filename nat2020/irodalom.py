#!/usr/bin/python3
# -*- coding: utf-8 -*-
alkotok_8 = [
    "Ady Endre",
    "Áprily Lajos",
    "Arany János",
    "Babits Mihály",
    "Balassi Bálint",
    "Berzsenyi Dániel",
    "Choli Daróczy József",
    "Agatha Christie",
    "Csokonai Vitéz Mihály",
    "Csukás István",
    "Daniel Defoe",
    "Tonke Dragt",
    "Dsida Jenő",
    "Fazekas Mihály",
    "Fekete István",
    "Gárdonyi Géza",
    "Herczeg Ferenc",
    "Illyés Gyula",
    "Janus Pannonius",
    "Jókai Mór",
    "József Attila",
    "Juhász Gyula",
    "Kányádi Sándor",
    "Karinthy Frigyes",
    "Kittenberger Kálmán",
    "Kós Károly",
    "Kosztolányi Dezső",
    "Kölcsey Ferenc",
    "Lázár Ervin",
    "Mándy Iván",
    "Márai Sándor",
    "Mikes Kelemen",
    "Mikszáth Kálmán",
    "Molière",
    "Molnár Ferenc",
    "Móra Ferenc",
    "Móricz Zsigmond",
    "Nagy László",
    "Nyirő József",
    "George Orwell",
    "Örkény István",
    "Petőfi Sándor",
    "Pilinszky János",
    "Radnóti Miklós",
    "Reményik Sándor",
    "Antoine Saint-Exupéry",
    "William Shakespeare",
    "Sütő András",
    "Szabó Lőrinc",
    "Szabó Magda",
    "Széchenyi Zsigmond",
    "Tamási Áron",
    "Tóth Árpád",
    "Jules Verne",
    "Vörösmarty Mihály",
    "Wass Albert",
    "Weöres Sándor",
    "Zrínyi Miklós",
]

memoriterek_8 = [
    "Ady Endre: Őrizem a szemed",
    "Arany János: Rege a csodaszarvasról (részletek)",
    "Arany János: Családi kör (részletek)",
    "Arany János: A walesi bárdok (részletek)",
    "Arany János: Toldi részletek",
    "Csokonai Vitéz Mihály: Tartózkodó kérelem",
    "Csokonai Vitéz Mihály: A Reményhez",
    "József Attila: Születésnapomra ",
    "József Attila: Mama",
    "Kányádi Sándor: Két nyárfa",
    "Kölcsey Ferenc: Himnusz -- teljes szöveg",
    "Kölcsey Ferenc: Huszt",
    "Kölcsey Ferenc: Emléklapra",
    "Petőfi Sándor: János vitéz (részletek)",
    "Petőfi Sándor: Nemzeti dal",
    "Petőfi Sándor: Szeptember végén",
    "Petőfi Sándor: Szabadság, szerelem",
    "Radnóti Miklós: Nem tudhatom",
    "Reményik Sándor: Templom és iskola (részletek)",
    "Vörösmarty Mihály: Szózat",
    "Weöres Sándor: Ó ha cinke volnék",
]

kotelezo_8 = [
    "Arany János: Toldi",
    "Gárdonyi Géza: Egri csillagok",
    "Jókai Mór: A nagyenyedi két fűzfa",
    "Mikszáth Kálmán: Szent Péter esernyője vagy A két koldusdiák",
    "Mikszáth Kálmán: A néhai bárány",
    "Molnár Ferenc: A Pál utcai fiúk",
    "Móricz Zsigmond: Légy jó mindhalálig vagy Pillangó",
    "Móricz Zsigmond: Hét krajcár",
    "Petőfi Sándor: János vitéz",
    "Shakespeare, William: Szentivánéji álom vagy Molière: A képzelt beteg",
    "Szabó Magda: Abigél",
    "egy szabadon választható magyar ifjúsági vagy meseregényt",
    "egy szabadon választható világirodalmi ifjúsági regényt",
]

mondak_8 = "A tanuló olvast feldolgozta 7-7 hét bibliai történetet, hét görög mítoszt, hét magyar mítoszt, hét hőst és hét mondát"


# gimnazium
eletmuvek_12 = [
    "Ady Endre",
    "Arany János",
    "Babits Mihály",
    "Herczeg Ferenc",
    "Jókai Mór",
    "József Attila",
    "Kosztolányi Dezső",
    "Mikszáth Kálmán",
    "Petőfi Sándor",
    "Vörösmarty Mihály",
]

portrek_12 = [
    "Balassi Bálint",
    "Berzsenyi Dániel",
    "Csokonai Vitéz Mihály",
    "Janus Pannonius",
    "Kányádi Sándor",
    "Kölcsey Ferenc",
    "Móricz Zsigmond",
    "Örkény István",
    "Szabó Magda",
    "Wass Albert",
    "Zrínyi Miklós",
]

metszetek_12 = [
    "Áprily Lajos",
    "Dsida Jenő",
    "Juhász Gyula",
    "Karinthy Frigyes",
    "Krúdy Gyula",
    "Mikes Kelemen",
    "Nagy László",
    "Pilinszky János",
    "Radnóti Miklós",
    "Reményik Sándor",
    "Szabó Dezső",
    "Szabó Lőrinc",
    "Tóth Árpád",
    "Weöres Sándor",
]

szemelvenyek_12 = [
    "Anonymus",
    "Apáczai Csere János",
    "Arany László",
    "Bessenyei György",
    "Gárdonyi Géza",
    "Gyergyai (Gergei) Albert",
    "Gyóni Géza",
    "Hajnóczy Péter",
    "Halotti beszéd és könyörgés",
    "Heltai Gáspár",
    "Kassák Lajos",
    "Károli Gáspár",
    "Kazinczy Ferenc",
    "Kecskeméti Vég Mihály",
    "Kisfaludy Károly",
    "Kuruc-kori költészet",
    "Márai Sándor",
    "II. Rákóczi Ferenc",
    "Nagy Gáspár",
    "Ómagyar Mária-siralom",
    "Örkény István",
    "Pázmány Péter",
    "Romhányi József",
    "Sylvester János",
    "Szenczi Molnár Albert",
    "Szent István király legendája Hartvik püspöktől",
    "Margit-legenda",
    "Szép Ernő",
    "Sztárai Mihály",
    "Tompa Mihály",
    "Vajda János",
]

vilagirodalom_szovegek = [
    "történet és szöveget a görög mitológiából",
    "babiloni teremtésmítoszt",
    "részlet a Bibliából",
]
vilagirodalmi_alkotok_12 = [
    "Aiszóposz",
    "Alkaiosz",
    "Anakreón",
    "Balzac, Honoré de vagy Stendhal",
    "Boccaccio Borowski, Tadeusz",
    "Burns, Robert Byron",
    "Catullus, Caius Valerius",
    "Dante, Alighieri",
    "Defoe, Daniel",
    "Dosztojevszkij, Fjodor Mihajlovics",
    "Eliot Thomas Stearns",
    "francia szimbolisták",
    "García Márquez, Gabriel",
    "Goethe, Johann Wolfgang von",
    "Gogol, Nyikolaj Vaszilijevics",
    "Heine, Heinrich",
    "Homérosz",
    "Horatius Flaccus, Quintus",
    "Hrabal, Bohumil",
    "Hugo, Victor",
    "Kafka, Franz",
    "La Fontaine, Jean de",
    "Mann, Thomas",
    "Mickiewicz, Adam",
    "Orwell, George",
    "Ovidius Naso, Publius",
    "Petrarca",
    "Poe, Edgar Allan",
    "Pound, Ezra",
    "Puskin, Alexszandr Szergejevics",
    "Swift, Jonathan",
    "Szapphó",
    "Szent Ágoston",
    "Szent Ferenc",
    "Theokritosz",
    "Todi, Jacopone da",
    "Tolsztoj, Lev Nyikolajevics Vergilius Maro, Publius",
    "Villon, François",
    "Vogelweide, Walter von der",
    "Voltaire",
]

szinhazi_szerzok_12 = [
    "Beckett, Samuel Barclay",
    "Brecht, Bertolt",
    "Csehov, Anton Pavlovics vagy Ibsen, Henrik",
    "Dürrenmatt, Friedrich",
    "Katona József",
    "Madách Imre",
    "Molière",
    "Örkény István",
    "Shakespeare, William",
    "Szabó Magda",
    "Szophoklész",
]

kotelezo_12 = [
    "Biblia részletei",
    "Boccaccio, Giovanni: Dekameron, (részletek)",
    "Dante Alighieri: Isteni színjáték -- Pokol (részletek)",
    "Homérosz: Odüsszeia (részletek)",
    "Jókai Mór: A huszti beteglátogatók (novella)",
    "Jókai Mór: Az arany ember",
    "Katona József: Bánk bán",
    "Mikes Kelemen: Törökországi levelek (részletek)",
    "Molière: A fösvény",
    "Petőfi Sándor: A helység kalapácsa, Az apostol (részlet)",
    "Shakespeare, William: Romeo és Júlia vagy Hamlet, dán királyfi",
    "Shakespeare, William: Szent Margit legendája (részlet)",
    "Szophoklész: Antigoné",
    "Villon, François: A nagy testamentum (részletek)",
    "Vörösmarty Mihály: Csongor és Tünde",
    "Zrínyi Miklós: Szigeti veszedelem (részletek). Arany János: Toldi estéje",
    "Babits Mihály: Jónás könyve",
    "Babits Mihály: Jónás imája",
    "Balzac, Honoré de: Goriot apó (részletek) vagy Stendhal: Vörös és fekete (részletek)",
    "Beckett, Samuel Barclay: Godot-ra várva vagy Dürrenmatt, Friedrich: A fizikusok",
    "Herczeg Ferenc: Az élet kapuja",
    "Ibsen, Henrik: Nóra/A vadkacsa vagy Csehov, Anton Pavlovics: A sirály/Ványa bácsi",
    "Madách Imre: Az ember tragédiája",
    "Mikszáth Kálmán: Beszterce ostroma",
    "Móricz Zsigmond: Úri muri, Tragédia",
    "Örkény István: Tóték",
    "Szabó Magda: Az ajtó",
    "Tolsztoj, Lev Nyikolajevics: Ivan Iljics halála",
    "Wass Albert: Adjátok vissza a hegyeimet!",
]

memoriterek_12 = [
    "Anakreón: Gyűlölöm azt...",
    "Balassi Bálint: Egy katonaének (részlet)",
    "Adj már csendességet (részlet)",
    "Berzsenyi Dániel: A közelítő tél (1. versszak)",
    "A magyarokhoz (I.) (1. versszak)",
    "Osztályrészem (1. versszak)",
    "Catullus: Gyűlölök és szeretek",
    "Csokonai Vitéz Mihály: Tartózkodó kérelem (általános iskolai memoriter felújítása)",
    "Csokonai Vitéz Mihály: A Reményhez",
    "Halotti beszéd és könyörgés (részlet)",
    "Homérosz: Odüsszeia (részlet)",
    "Janus Pannonius: Pannonia dicsérete",
    "Kölcsey Ferenc: Himnusz (általános iskolai memoriter felújítása)",
    "Zrínyi második éneke (részletek)",
    "Ómagyar Mária-siralom (részlet)",
    "Petőfi Sándor: Fa leszek, ha...",
    "Petőfi Sándor: Nemzeti dal (általános iskolai memoriter felújítása)",
    "Petőfi Sándor: A bánat? Egy nagy oceán",
    "Petőfi Sándor: Szeptember végén (általános iskolai memoriter felújítása)",
    "Vörösmarty Mihály: Szózat (általános iskolai memoriter felújítása)",
    "Vörösmarty Mihály: Gondolatok a könyvtárban (részlet)",
    "Vörösmarty Mihály: Előszó (részlet)",
    "Ady Endre: Góg és Magóg fia vagyok én...",
    "Ady Endre: Kocsi-út az éjszakában",
    "Áprily Lajos: Március",
    "Arany János: Toldi estéje (részletek)",
    "Arany János: egy szabadon választott balladája a nagykőrösi korszakból",
    "Epilógus (részlet)",
    "Babits Mihály: A lírikus epilógja",
    "Babits Mihály:  Jónás imája",
    "József Attila: Reménytelenül (Lassan, tűnődve)",
    "József Attila: Óda (részlet)",
    "Kányádi Sándor: Valaki jár a fák hegyén",
    "Kosztolányi Dezső: Hajnali részegség részlet)",
    "Nagy László: Ki viszi át a Szerelmet",
    "Radnóti Miklós: Hetedik ecloga (részlet)",
    "Reményik Sándor: Halotti vers a hulló leveleknek (részlet)",
]
for k in alkotok_8:
    print(
        "A tanuló 8. évfolyam végére elolvasta és feldolgozta {} egyes műveit.".format(
            k
        )
    )

for m in memoriterek_8:
    print("Memoriterként 8. évfolyam végére felidézi {} c. művét.".format(m))

for k in kotelezo_8:
    if "szabadon" in k:
        print("Elolvasott {}.".format(k))
    else:
        print("Elvolvasta {} c. művét.".format(k))


for e in eletmuvek_12:
    print("A tanuló ismeri {} életművét.".format(e))
    #  Értelmezi az életművének fontosabb összefüggését, azok etikai, történeti, lélektani vagy társadalmi vonatkozásait. Érti a szerző nemzeti-kulturális jelentőségét, helyét a magyar irodalom történetében. Memoriterként felidéz néhány szövegrészt a szerző életművéből.".format(e))

for p in portrek_12:
    print(
        "A tanuló ismeri {} szerzők néhány rövidebb (lírai, kisprózai) alkotását vagy hosszabb művéből választott részletet, valamint azok történeti, etikai, lélektani vagy társadalmi vonatkozását. ".format(
            p
        )
    )

for m in metszetek_12:
    print(
        "{} szerző szövegét olvasta és értelmezte, és ezek alapján készített róla irodalmi metszetet.".format(
            m
        )
    )

for sz in szemelvenyek_12:
    print(
        "A megismerte {} szemelvényt/alkotót/művet, és ennek segítségével irodalomtörténeti, művelődéstörténeti korokat mutatott be.".format(
            sz
        )
    )

for vsz in vilagirodalom_szovegek:
    print("A tanuló megismert néhány {}.".format(vsz))

for v in vilagirodalmi_alkotok_12:
    print("A tanuló megismerte {} néhány fontos szövegét.".format(v))

for sz in szinhazi_szerzok_12:
    print(
        "A tanuló megismerte {} alkotót. Egy művében kiemeli az alaphelyzetet jelentő konfliktusokat, azokat értelmezi.".format(
            sz
        )
    )

for k in kotelezo_12:
    if "biblia" in k:
        print("A tanuló megismerte a Biblia néhány részletét.")
    else:
        print("A tanuló elolvasta {} c. művét.".format(k))

for m in memoriterek_12:
    print("A tanuló megtanulta a {} c. művét.".format(m))
