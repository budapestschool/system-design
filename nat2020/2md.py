import json
from sys import argv
import sys
import locale

subjects = [
    "Állampolgári ismeretek ",
    "Biológia ",
    "Digitális kultúra ",
    "Dráma és színház ",
    "Első élő idegen nyelv",
    "Ének-zene ",
    "Etika",
    "Fizika ",
    "Földrajz ",
    "Hon- és népismeret ",
    "Komplex természettudomány",
    "Kémia ",
    "Környezetismeret ",
    "Magyar nyelv és irodalom ",
    "Matematika ",
    "Második idegen nyelv",
    "Mozgóképkultúra és médiaismeret ",
    "Pénzügyi és vállalkozói ismeretek",
    "Szoftverfejlesztés és -tesztelés",
    "Technika és tervezés ",
    "Természettudomány ",
    "Testnevelés és egészségfejlesztés ",
    "Történelem ",
    "Vizuális kultúra ",
]

TECHNIKUM = False
TECHNIKUM_KUKA = [
    "Biológia ",
    "Dráma és színház ",
    "Ének-zene ",
    "Etika",
    "Fizika ",
    "Földrajz ",
    "Hon- és népismeret ",
    "Kémia ",
    "Környezetismeret ",
    "Mozgóképkultúra és médiaismeret ",
    "Testnevelés és egészségfejlesztés ",
    "Vizuális kultúra ",
]

OSSZEVONT = ["Biológia ", "Földrajz ", "Fizika ", "Kémia "]


def read_csv_from_file(file):
    result = []
    with open(file, "r", encoding="utf-8") as infile:
        for line in infile:
            subject, period, lo = line.split("\t")
            result.append((subject, period, lo))
    return result


def subjects_los(los, subj):
    # egy tantargyhoz tartozo lo, de mar csak perdio es lo
    return [(period, lo) for (subject, period, lo) in los if subject == subj]


def get_valid_period(los):
    # mikor van ez a tantargy?
    res = list(set([period for (period, lo) in los]))
    res.sort(key=lambda period: int(period.split("-")[-1]))
    return res


def get_los(los, period):
    return [lo for (perdio, lo) in los if perdio == period]


def print_subjects_los(los, subj, ofile):
    # megnezzuk, hogy egyatalan van-e a tantargy (mert mondjuk a filter kiszedte technikumban)
    slos = subjects_los(los, subj)
    periods = get_valid_period(slos)
    if len(periods) < 1:
        return

    print("## {}".format(subj), file=ofile)
    for period in periods:
        print("### {}. évfolyamon".format(period.strip()), file=ofile)
        for lo in get_los(slos, period):
            print("* " + lo, file=ofile)


def move_lo(orig_lo):
    (subject, period, lo) = orig_lo
    if "népismeret" in subject:
        period = "6"
    elif "Állampolgári" in subject:
        period = period.split("-")[1]
    elif period == " 7-8 " and subject in [
        "Biológia ",
        "Földrajz ",
        "Fizika ",
        "Kémia ",
    ]:
        subject = "Természettudomány"
    elif "Etika" in subject:
        subject = "Etika"
    elif "Élő idegen nyelv " == subject:
        subject = "Első élő idegen nyelv"
    elif TECHNIKUM and ("Digitális k" in subject) and period == " 9-12 ":
        period = "9"
    return (subject, period, lo)


def filter_lo(orig_lo):
    (subject, period, lo) = orig_lo
    # technikum 9.-tol kezdodik
    if TECHNIKUM and int(period.split("-")[-1]) < 9:
        # kiszedjuk a 9. alatti dolgoka
        return False

    elif TECHNIKUM and subject in TECHNIKUM_KUKA:
        return False

    return True


def read_los():
    _, filename = argv
    los = read_csv_from_file(filename)

    # megcsinaljuk a valasztasok a natban
    los = list(map(move_lo, los))
    los += [
        ("Második idegen nyelv", "9-12", lo)
        for (subject, period, lo) in read_csv_from_file(filename)
        if subject == "Élő idegen nyelv " and (period == " 4 " or period == " 5-8 ")
    ]
    (szoft1, szoft2) = read_software()

    los += [("Szoftverfejlesztés és -tesztelés", "9-10", line)
            for line in szoft1]
    los += [("Szoftverfejlesztés és -tesztelés", "11-13", line)
            for line in szoft2]
    los += [("Komplex természettudomány", "9", line)
            for line in red_complex_term_tud()]
    los += [
        ("Pénzügyi és vállalkozói ismeretek", "10", line)
        for line in read_prepared_md_files("penzugy_es_vallalkozas.md")
    ]

    # torlunk dolgokat
    los = [lo for lo in los if filter_lo(lo)]
    # TODO: ez is mehet filterbe
    los = [
        (subject, period, lo)
        for (subject, period, lo) in los
        if not (
            (
                (subject == "Mozgóképkultúra és médiaismeret " and (period == " 9-10 "))
                or (subject == "Dráma és színház " and period == " 12 ")
            )
        )
    ]
    return los


def main_print_tantargyi_halo():
    los = read_los()
    print("| Tantárgy", end=" | ")
    for i in range(1, 13):
        print(i, end=" | ")
    print("")
    print("|---------", end="-|")
    for i in range(1, 13):
        print("--", end=":|")
    print("")

    for subject in subjects:
        sl = subjects_los(los, subject)

        periods = get_valid_period(sl)
        years = [0] * 12
        for p in periods:
            ps = p.split("-")

            if len(ps) == 1:
                first = int(ps[0])
                last = first
            else:
                first = int(ps[0])
                last = int(ps[1])

            # milyen hosszu az idoszak
            span = last - first + 1
            # mennyit kell tanulni
            total_lo = len(get_los(sl, p))
            for i in range(first, last):
                years[i - 1] = int(total_lo / span)

            # utolso evre a maradek tort tanulasi eredmenyek atmennek
            years[last - 1] = total_lo - sum(years[first - 1: last])
        print("| ", subject, end="\t |")
        for i in years:
            if i > 0:
                print(i, end=" | ")
            else:
                print(" ", end=" | ")
        print("")


def main_printdb():
    los = read_los()
    result = {}

    for subject in subjects:
        # megnezzuk, hogy egyatalan van-e a tantargy (mert mondjuk a filter kiszedte technikumban)
        slos = subjects_los(los, subject)
        periods = get_valid_period(slos)
        if len(periods) < 1:
            continue
        for period in periods:
            name = "{}@{}".format(subject.strip(), period.strip())
            result[name] = [lo.strip() for lo in get_los(slos, period)]

    print(json.dumps(result, ensure_ascii=False).encode("utf-8").decode())


def main_printmd():

    los = read_los()
    with open("../docs/tantargyak.md", "w") as ofile:
        print("# Tantárgyak tartalma, a tanulási eredmények", file=ofile)
        for subject in subjects:
            print_subjects_los(los, subject, ofile)

    print("'docs/tantargyak.md' created")


# nehany tantargy siman megvan mint md listak. Pl. a szoftver fejleszto vagy a complex term tud
# ezeket csak beolvassuk
def read_prepared_md_files(file):
    with open(file) as f:
        content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content]
        return content


def read_software():
    return (
        read_prepared_md_files("szoftverfejleszto1.md"),
        read_prepared_md_files("szoftverfejleszto2.md"),
    )


def red_complex_term_tud():
    return read_prepared_md_files("komplex_term_tud.md")


# main_print_tantargyi_halo()
main_printdb()
