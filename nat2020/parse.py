#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import sys


def tantargy_head(line):
    line = line.replace("*", "").strip()
    myre = re.compile(r"II\.(.)\.(.)\.((.)\.)? ([^\*]+)")
    tablazat = re.compile(r"táblázat")
    if "Melléklet" in line or "Táblázat" in line or "alpontja" in line:
        return None
    if tablazat.search(line) is None:
        mo = myre.search(line)
        if mo:
            field_1, field_2, field_3, field_4, field_5 = mo.groups()
            return (field_1, field_2, field_4, field_5)
    return None


ST_START = 0
ST_IN_GROUP = 1
ST_IN_SUBJECT = 2
current_group = None
current_subject = None

state = ST_START
ST_IN_LO_ITEM = 4
ST_IN_LOS = 3
state_2 = ST_START
current_period = None
current_lo = ""
f = open("nat.md", encoding="utf-8")
lineno = 0
for line in f:
    lineno += 1
    if tantargy_head(line):

        h1, h2, h3, name = tantargy_head(line)
        if state == ST_START:
            if h3 is None:
                # amikor csak 3.8 van irva, akkor az egy nagy fejezetez
                state = ST_IN_GROUP
                current_group = name
                if (
                    name == "Matematika"
                    or name == "Etika / hit- és erkölcstan"
                    or name == "Magyar nyelv és irodalom"
                ):
                    # ezek a masodszintu csoport, ami alatt nincs kulon tantargy
                    current_subject = name
                    state = ST_IN_SUBJECT
            else:
                raise Exception("invalid state start but h3 " + line)
        elif state == ST_IN_GROUP:
            if h3 is None:
                raise Exception(
                    "invalid line, group in group? " + current_group + " " + line
                )
            else:
                state = ST_IN_SUBJECT
                current_subject = name
                state_2 = ST_START
        elif state == ST_IN_SUBJECT:
            # itt zarunk egy targyat
            if h3 is not None:
                # uj targy ugyanabban a groupban
                current_subject = name
            else:
                current_subject = None
                state = ST_IN_GROUP
                state_2 = ST_START
                current_group = name
                if (
                    name == "Matematika"
                    or name == "Etika / hit- és erkölcstan"
                    or name == "Magyar nyelv és irodalom"
                    or name == "Testnevelés és egészségfejlesztés"
                ):
                    # ezek a masodszintu csoport, ami alatt nincs kulon tantargy
                    current_subject = name
                    state = ST_IN_SUBJECT
    if "TANULÁSI EREDMÉNYEK" in line.upper():
        # most kezdodnek a tanulasi eredmenyek
        if state == ST_IN_SUBJECT:
            period = None
            if "9--12." in line:
                period = "9-12"
            elif "1--4." in line:
                period = "1-4"
            elif "4." in line:
                period = "4"
            elif "5--6." in line:
                period = "5-6"
            elif "5--8." in line:
                period = "5-8"
            elif "5--7." in line:
                period = "5-7"
            elif "3--4." in line:
                period = "3-4"
            elif "7--8." in line:
                period = "7-8"
            elif "9--10." in line:
                period = "9-10"
            elif "12." in line:
                period = "12"
            elif "4." in line:
                pediod = "4"

            if period is None:
                continue

            # elkezdodott a tanulasi eredmeny egy szakaszban
            current_period = period
            state_2 = ST_IN_LOS

    if state_2 == ST_IN_LOS:
        myre = re.compile(r"^[1-90]+\. +(.*)")
        mo = myre.search(line)
        if mo:
            current_lo = mo.groups()[0]
            state_2 = ST_IN_LO_ITEM
    elif state_2 == ST_IN_LO_ITEM:
        if len(line) < 2:
            # megvan a LO
            state_2 = ST_IN_LOS
       
            current_lo.strip()
            if current_lo[-1] == ";":
                current_lo = current_lo[:-1] + '.'
            print(current_subject.lower().capitalize(), "\t", current_period, "\t", current_lo.capitalize() )
            current_lo = ""
        else:
            current_lo = current_lo + " " + line.strip()

