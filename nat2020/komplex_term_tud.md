Ismeri a tudományos kutatás alapszabályait és azokat alkalmazza
Önálló tudományos kutatást tervez meg és végez el
Önálló kutatása összeállításakor tudományos modelleket használ
Tudományos kutatások során elvégzi a megszerzett adatok feldolgozását és értelmezését
Érti a tudomány szerepét és szükségszerűségét a társadalmi folyamatok alakításában
Hiteles források felhasználásával egy tudományos probléma kritikus elemzését adja a megszerzett információk alapján
Elméleti és gyakorlati eszközöket választ és alkalmaz egy adott tudományos probléma ismertetéséhez
Tudományos kutatási eredményeket érthetően mutat be digitális eszközök segítségével
Önállóan és kiscsoportban biztonságosan végez természettduományos kísérleteket
Felismeri a saját és társai által végzett tudományos kísérletek etikai és társadalmi vonatkozásait
Ismeri és alkalmazza az energia felhasználásának és átalakulásának elméleti és gyakorlati lehetőségeit (energiaáramláson alapuló ökoszisztémák, a föld saját energiaforrásai stb.)
Felismeri és kísérletei során alkalmazza azt a tudást, hogy az anyag atomi természete határozza meg a fizikai és kémiai tulajdonságokat és azok kölcsönhatásából eredő módosulását. (Példák kísérletekre: kémiai reakciók, molekuláris biológiai, anyagok körforgása a különböző ökoszisztémákban)
Felismeri és kísérletei során alkalmazza azt a tudást, hogy a természet ismert rendszerei előrejelezhető módon változnak (evolúció, klímaváltozás, földtörténeti korok, Föld felszíni változása, tektonikus mozgások, ember környezeti hatásai stb.)
Felismeri és kísérletei során alkalmazza azt a tudást, hogy a tárgyak mozgása előrejelezhető (erők, égitestek mozgása, molekuláris mozgások, hangok, fények mozgása)
Felismeri és kísérletei során alkalmazza azt a tudást, hogy a világ megismerésének egyik alapja a szerveződési szintek és típusok megértése (periódusos rendszer, sjetszintű szerveződések, állat és növényvilág rendszertana, a világegyetem szervező elvei)
Ismeri a föld népességgének aktuális kihívásait beleértve azok társadalmi és egészségügyi kockázatait (betegség-megelőzés, járványok, élelmezés)
Ismeri a hulladékgazdálkodás aktuális kihívásait
Ismeri az energia fogalmát és az egyéni és társadalmi energiafelhasználás különböző lehetőségeit
Megkülönbözteti egymástól a természetes és mesterséges anyagokat és felismeri, hogy miként állapítható egy anyag összetétele
Ismeri a városi és falusi életmód és életterek közötti különbségeket, azok környezetre gyakorolt hatását
Használja a regionalitás fogalmát és érti annak szerepét a gazdasági folyamatok alakulásában
Ismeri a levegő és a víz fizikai és kémiai jellemzőit, felismeri ezek élettani hatásait
Tudja, hogy milyen vízkészletekkel rendelkezik a Föld és azok felhasználásának milyen hatása van a környezeti, ipari, turisztikai és szállítmányozási folyamatokra
Ismeri a növények tápanyagigényét és fejlődésük alapjait
Ismeri a vadon élő állatközösségeket fenyegető veszélyeket és azt a kihívást, amit ez az emberekre gyakorol
Ismeri a különböző kultúrák eltérő gazdasági termelési szokásait (növénytermesztés, állattartás)
Ismeri a talaj természetét és annak megművelésének különböző formáit
Ismeri a föld légkörét befolyásoló globális folyamatokat (tengerek és szelek áramlása, klímaváltozás, üvegházhatású gázok)
Ismeri az emberi táplálkozás során hasznos ételeket, megkülönbözteti a gyógyító és a káros anyagokat, önállóan tud egészséges ételt készíteni
Ismeri az emberi agy alapvető működési szabályait és az azt befolyásoló tényezőket
Tisztában van az Univerzum létrejöttének ma ismert elméletével, valamint a Naprendszer kialakulásának folyamatával
Ismeri a tanulás és az emberi kommunikáció biológiai alapjait
Ismeri az emberi szervezet egészségét alapvetően befolyásoló tényezőket, a stressz, az öröklött hajlamok és genetikai tulajdonságok, valamint a környezeti hatások szerepét
Ismeri az emberi szexualitás kulturális, társadalmi és biológiai alapjait. Önálló véleménye van a nemi szerepek fontosságáról, érti a nemi identitás komplex jellegét.
Ismeri a hálózatok és a hálózatkutatás szerepét modern világunkban, az életközösségek, a sejtszintű gondolkodás és az információs technológiák területén.
Ismeri a genetikai információ átadásának alapvető szabályait.
