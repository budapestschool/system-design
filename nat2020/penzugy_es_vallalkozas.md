A tanuló érti a nemzetgazdaság szereplőinek (háztartások, vállalatok, állam, pénzintézetek) feladatait, a köztük lévő kapcsolatrendszer sajátosságait.
Tudja értelmezni az állam gazdasági szerepvállalásának jelentőségét, ismeri főbb feladatait, azok hatásait.
Tisztában van azzal, hogy az adófizetés biztosítja részben az állami feladatok ellátásnak pénzügyi fedezetét.
Ismeri a mai bankrendszer felépítését, az egyes pénzpiaci szereplők főbb feladatait.
Képes választani az egyes banki lehetőségek közül.
Tisztában van az egyes banki ügyletek előnyeivel, hátrányaival, kockázataival.
A bankok kínálatából bankot, bankszámla csomagot tud választani.
Tud érvelni a családi költségvetés mellett, a tudatos, hatékony pénzgazdálkodás érdekében.
Önismereti tesztek, játékok segítségével képes átgondolni milyen foglalkozások, tevékenységek illeszkednek személyiségéhez.
Tisztában van az álláskeresés folyamatával, a munkaviszonnyal kapcsolatos jogaival, kötelezettségeivel.
Ismer vállalkozókat, vállalatokat, össze tudja hasonlítani az alkalmazotti, és a vállalkozói személyiségjegyeket.
Érti a leggyakoribb vállalkozási formák jellemzőit, előnyeit, hátrányait.
Tisztában van a nem nyereségérdekelt szervezetek gazdaságban betöltött szerepével.
Ismeri a vállalkozásalapítás, -működtetés legfontosabb lépéseit, képes önálló vállalkozói ötlet kidolgozására.
Meg tudja becsülni egy vállalkozás lehetséges költségeit, képes adott időtartamra költségkalkulációt tervezni.
Tisztában van az üzleti tervezés szükségességével, mind egy új vállalkozás alapításakor, mind már meglévő vállalkozás működése esetén.
Tájékozott az üzleti terv tartalmi elemeiről.
Megismeri a nem üzleti (társadalmi, kulturális, egyéb civil) kezdeményezések pénzügyi-gazdasági igényeit, lehetőségeit.
Felismeri a kezdeményezőkészség jelentőségét az állampolgári felelősségvállalásban.
Felismeri a sikeres vállalkozás jellemzőit, képes azonosítani az esetleges kudarc okait, javaslatot tud tenni a problémák megoldására.
