# 2024
## augusztusi módosítás

- [x] osztályzás szövegének módosítása
- [x] új intervenciós folyamat leírása
- [x] új konfliktuskezelő folyamat (ezt még le kell írnom)
- [x] fenntarthatóság tantárgy legyen választható 12-ben is, ahogy Zita kérte
- [ ] TÉR kerüljön be az SzMSz-be
- [ ] Kréta kerüljön be az SzMSz-be
- [ ] hiányzások ez alapján https://docs.google.com/document/d/1lOB1HsZSuRP0KzTkqIOVX5RsYoah4NEtaoaAWbbLN-U/edit
- [x] 2024 dátumai
- [ ] tanulási pont




# 2023

változtatások
done

- szines folyamatok javitasa, a hulye gyereknév

todo

- nellitől megkérdezni, h a gyogyped folyamat hova keruljon
  - minden folyamatban lévő gyógypedagógiai esetnek van visszakereshető dokumentációja
- HP trimeszter dátumok
- Viki: gyogyped koordinátor
  - közös szerepekből kivenni az sni
  - bevenni tanártípus gyógyped koordinátor
  - HP szaktanárok egyes szám a menüben
  - https://model.budapestschool.org/tanulas-megkozelitese/kiemelt-figyelmet-igenylo-gyerekek.html atirni

- osztályozás
  1. Viki: javaslatbol gdocsban irni egy modelszoveget
  2. osztalyozas evfolyamlepes fejezet atirasa
  3. HP: evfolyam, osztalyozas, tanulasi eredmeny szavakra keresni
    tanulási eredmények
    minőségbiztosításban
    > A BPS modell az évfolyamokra úgy tekint, mint egy [szerepjáték nehézségi
    szintjeire](https://en.wikipedia.org/wiki/Experience_point): akkor léphet egy gyerek a következő szintre, ha
    az  évfolyamhoz köthető tantárgyi tanulási eredményekből eleget összegyűjtött.
- HP: Többszintű visszajelzés részben a trimeszterzáró és a modulzáró jelenjen meg külön
- Viki: korcsoportok, és korcsoportváltó rubric - knowledge base-ben
  - nyito leirasba is berakni
  - HP: emberkepnel osszerakni 3 szakaszra,
  - HP: emberkepnel a kimeneteket a korcsoportvalto rubriccal osszenezni
- VIKI: mentornapló legyen egyértelmű, hogy nem kötelező. De trimeszter szerződés viszont.
- bms minumum
    - a gyerekeknek van rendszeres mentorbeszélgetésük min. 2hetente
    - trimeszterenként min. 1 konzultáció a családdal (szerződő beszélgetés)
    - a gyerekek minden trimeszter végén kapnak részletes írásos visszajelzést a mentoruktól (trimeszterzáró feedback)
    - modul záródátuma után legkésőbb 2 héttel
- szmsz
  - a mikroiskola tanárcsapata részt vesz a közösségi események min. 50%-án
senior tanárai bekapcsolódnak a fejlesztési projektbekbe (1 projekt/év)
- rendszeres (legalább 5 alkalommal a tanévben) megszervezett szülői körök
- tanulási szerződések visszakereshetőek legyenek a minimum számára: visszakereshető hármas szerződések,
- partnerségbe kerüljön be a "rendszeres híradás az iskolai történésekről, tanulásról"
- osztályozo naplo hitelesites
- igazgato kepviseloje

- BPS code kerüljön ki

- hozzáférések
