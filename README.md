# A model.budapestschool.org forrása
Ez egy vuepress oldal, amit a docs folderben található markdown-ból generálunk. A baloldali tartalomjegyzéket a
a docs/.vuepress/toc.js-ben találod.

## Building locally

This project uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can get started.

```
npm install -g yarn
```

Then you need to install the project dependencies:

```
yarn install
```

Now you're ready to go.
To run the local dev server just use the following command:

```
yarn start
```

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml)

This sets up a `node:14-buster` environment, then uses `yarn install` to install dependencies and `yarn build` to build out the website to the `./public` directory.
It also caches the `node_modules` directory to speed up subsequent builds.

## PDF generation
There is a small script that generas a pdf of the whole document. See the `generate-pdf.js`. The gitlab CI script calls `yarn generate` script as well and we copy the generated pdf to the `public` folder
