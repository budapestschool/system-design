# Évfolyam, osztályzatok, bizonyítvány

A Budapest Schoolban tanuló gyerekek saját tanulási célokat tűznek ki, tanulnak, alkotnak, trimeszterenként frissítik a portfóliójukat, mentorukkal és a szaktanárokkal értékelik haladásukat, és ha kell, újraterveznek.
Saját céljaik vannak, és sokszor a saját céljaik eléréséhez maguknak kell felállítaniuk követelményeket._„Ha pszichológus akarok lenni, akkor emeltszintű biológia érettségit kell tennem. Ehhez a mai tudásom alapján nekem legalább hetente két órát kell a felkészüléssel foglalkoznom."_ Egyféle elvárásrendszer azonban adott a gyerek és az iskola számára, és ez a Nemzeti Alaptanterv elvárásrendszere.

## Évfolyam

A Nemzeti Alaptanterv tantárgyanként megadott elvárt tanulási eredményekkel határozza meg, hogy mit kell a gyerekeknek megtanulniuk ahhoz, hogy a NAT szerint is haladni tudjanak az 1. évfolyamtól a 12. évfolyamig. Az iskola feladata a gyerekeknek ebben a haladásban segíteni és a haladást nyomonkövetni. A BPS iskolában a tanulás nyomonkövetését a többszempontú, kidolgozott [értékelő táblázatok](visszajelzes-ertekeles.md#az-ertekelo-tablazatok) teszik lehetővé. A modulok végén a tanárok által elkészített visszajelzésekből egyértelműen kiolvasható, hogy a gyerekek milyen mértékben érték el a kitűzött tanulási célokat: teljesítették, túlszárnyalták vagy esetleg még nem érték el az elvárt tudásszintet. Az értékelő táblázatokban szereplő szöveges visszajelzések a NAT tanulási eredmények által kifejezett tantárgyi fejlesztési célokat és ismereteket is magukba foglalnak - így ezekből a visszajelzésekből megállapítható a gyerekek tantárgyi előrehaladása. A tanév végéig gyűjtött szöveges visszajelzéseket mérlegelve a BPS-iskola tanárcsapata dönt arról, hogy a gyerek készen áll-e az évfolyamváltásra.

### Kevert korosztály és az évfolyamok

A gyerekek a BPS-iskolában sokszor vannak náluk fiatalabb vagy épp idősebb gyerekekkel. Sokszor, sokféle csoportbontásban dolgoznak: ugyanaz a két gyerek, aki együtt, egy csoportban tanul angolul, lehet, hogy külön tanul matematikáról, mert teljesen máshol tartanak a megértésben. Így az a kérdés, hogy _„hányadikos vagy?''_ nem annyira releváns a mindennapi tanulásban, mint az, hogy _„mit tanulsz épp?''_

## Érdemjegyek és osztályzatok

A BPS-iskolában a tanulást támogató szöveges visszajelzéseket és a haladás dokumentálását és nyomonkövetését elősegítő [értékelő táblázatokat](visszajelzes-ertekeles.md#az-ertekelo-tablazatok) tekintjük az értékelés alapjának. Ezért az osztályzás gyakorlata csak szükség esetén lép érvénybe: az iskola az érdemjegyek helyett használt folyamatos, többszempontú, visszajelzéseit csak abban az esetben váltja osztályzatokká, azaz végzi el a félévi és az év végi minősítést osztályzatokkal, amikor iskolaváltás vagy továbbtanulás miatt erre szükség van, vagy a szülő vagy a gyerek ezt kéri.

Az osztályzatok kialakítása csak a gyerek által elért eredmények, azaz a dokumentált szöveges visszajelzések (értékelő táblázatok) és portfóliója alapján történhet. Az osztályzatok kialakításához szükséges minden adatot egy online elérhető adatbázis tartalmaz.

### Osztályzatok kialakítása

A BPS-iskolában az osztályzatok azt fejezik ki, hogy az adott gyereket az adott évben hogyan látták a tanárai a dokumentált szöveges visszajelzések és a portfóliója alapján. Az iskolában az osztályzás folyamata egy alátámasztott döntés kell, hogy legyen: a gyerek portfóliója és a szöveges visszajelzések demonstrálják, hogy az adott tantárgyból a NAT tanulási eredmények által behatárolt tantárgyi fejlesztési célok és ismeretek mentén milyen mértékben érte el az elvárásokat. A tanári mérlegelés lehetővé teszi, hogy az osztályzatok kialakításakor a tanárcsapat a gyerek erőfeszítését, önmagához viszonyított fejlődését is figyelembe vegye.

Az osztályzatokkal történő értékelés a BPS iskolában korcsoportonként eltérő.

#### Értékelés a különböző évfolyamokon

_1-2. évfolyamon_

Első ill. második évfolyamon félévzáráskor és év végén az iskola a gyerekek tanulási tantárgyi haladását érdemjegyekkel történő minősítés helyett többszempontú, kidolgozott szintleírásokat tartalmazó értékelő táblázatok segítségével értékeli. Félévzáráskor és évzáráskor az iskola tantárgyi szöveges szummatív értékelést állít ki. A BPS-ben a szummatív értékelésnek nem célja a gyerekek eredményeinek árnyalt visszajelzése, cél viszont annak megállapítása, hogy a gyerek halad-e az évfolyamlépéshez szükséges tantárgyi célok elérésével. A szummatív szöveges értékelésben a tanárok két lehetőség közül jelölhetnek meg egyet:

Félévzáráskor:
    _Az eredményeid alapján haladsz a tantárgyi elvárások teljesítésével._
    _Az eredményeid még nem érik el a tantárgyi elvárások minimumát._


Tanévzáráskor
    _Teljesítetted az elvárásokat ebből a tárgyból_
    _Még nem állsz készen az évfolyamváltásra ebből a tárgyból_


A Nemzeti Köznevelési Törvénnyel összhangban a Budapest School iskolákban első és második évfolyamon félévzáráskor, valamint első évfolyamon év végén a szöveges tantárgyi visszajelzéseket osztályzatra nem váltjuk át. 


_3 - 10. évfolyamon_

Félévzáráskor az iskola a gyerekek tanulási tantárgyi haladását érdemjegyekkel történő minősítés helyett többszempontú, szintleírásokat tartalmazó értékelő táblázatok segítségével értékeli. Félévzáráskor és évzáráskor az iskola tantárgyi szöveges szummatív értékelést állít ki,. A BPS-ben a szummatív értékelésnek nem célja a gyerekek eredményeinek árnyalt visszajelzése, cél viszont annak megállapítása, hogy a gyerek halad-e az évfolyamlépéshez szükséges tantárgyi célok elérésével. A szummatív szöveges értékelésben a tanárok két lehetőség közül jelölhetnek meg egyet:
Félévzáráskor: 
    _Az eredményeid alapján haladsz a tantárgyi elvárások teljesítésével._ 
    _Az eredményeid még nem érik el a tantárgyi elvárások minimumát._


Tanévzáráskor
    _Teljesítetted az elvárásokat ebből a tárgyból_
    _Még nem állsz készen az évfolyamváltásra ebből a tárgyból_

A gyerekek többszempontú értékelőtáblázatban adott  visszajelzéseket  az iskola a szülő kérésére tantárgyi osztályzatokra váltja át azokból a tantárgyakból és évfolyamokból, amelyekből a szülő ezt kéri.


_11. évfolyamon_

Félévzáráskor és évzáráskor az iskola a gyerekek tanulási tantárgyi haladását érdemjegyekkel történő minősítés helyett többszempontú, kidolgozott értékelő táblázatok segítségével értékeli és az iskola tantárgyi szöveges szummatív értékelést állít ki. 
A BPS-ben a szummatív értékelésnek nem célja a gyerekek eredményeinek árnyalt visszajelzése, cél viszont annak megállapítása, hogy a gyerek halad-e az évfolyamlépéshez szükséges tantárgyi célok elérésével. A szummatív szöveges értékelésben a tanárok két lehetőség közül jelölhetnek meg egyet:

Félévzáráskor: 
    _Az eredményeid alapján haladsz a tantárgyi elvárások teljesítésével._
    _Az eredményeid még nem érik el a tantárgyi elvárások minimumát._


Tanévzáráskor
    _Teljesítetted az elvárásokat ebből a tárgyból_
    _Még nem állsz készen az évfolyamváltásra ebből a tárgyból_

A kötelező érettségi tárgyakból (matematika, magyar nyelv és irodalom, történelem, első idegen nyelv, természettudomány) az iskola a gyerek értékelő táblázatai és portfóliója alapján osztályzatokat állapít meg. A készségtárgyak és más további elméleti tantárgyak (állampolgári ismeretek, digitális kultúra, dráma és színház, ének-zene, etika, fenntarthatóság, technika és tervezés, testnevelés, vizuális kultúra) esetében az iskola a többszempontú szöveges visszajelzéseket akkor váltja át oszályzatokra, ha a szülő ezt kéri. 

_12. évfolyamon_

Félévzáráskor és évzáráskor az iskola a gyerekek tanulási tantárgyi haladását többszempontú, kidolgozott értékelő táblázatok segítségével és osztályzatokkal is értékeli. A többi évfolyamhoz hasonlóan az iskola tantárgyi szöveges szummatív értékelést is kiállít, amely kifejezi, hogy a gyerek az adott tárgyból elérte az érettségihez szükséges szintet, vagy még nem áll készen az érettségire bocsátásra. A szummatív szöveges értékelésben a tanárok két lehetőség közül jelölhetnek meg egyet:

Félévzáráskor: 
    _Az eredményeid alapján haladsz a tantárgyi elvárások teljesítésével._
    _Az eredményeid még nem érik el a tantárgyi elvárások minimumát._


Tanévzáráskor
    _Teljesítetted az elvárásokat ebből a tárgyból_
    _Még nem állsz készen az érettségire ebből a tárgyból_


### Osztályzatok megállapítása

Az osztályzatok megállapításának folyamata minden esetben a következő:

- Az osztályzatok alapjául szolgáló demonstráció gyűjtése: a gyerek szöveges visszajelzéseit a modulok során a tanárok dokumentálják, a gyerek eredményeit bemutató portfólióját trimeszterenként frissítik.
- Az osztályzási folyamatban résztvevő gyerekek számára az első trimeszter ill. a harmadik trimeszter zárásakor a tantárgyakért felelős szaktanárok ajánlott osztályzatokat állapítanak meg a tantárgyspecifikus szöveges visszajelzések és portfólió alapján.
- Az ajánlott osztályzatokat a gyerek elfogadhatja vagy korrekciós lehetőséget kérhet.
- A tantárgyi osztályzatokról a tanárcsapat hozzájárulásos döntést hoz a félévi ill. év végi értekezlet során a szöveges visszajelzések és a gyerek portfóliója alapján, és ezeket az iskola a félévi, valamint év végi értesítőben, valamint a bizonyítványban rögzíti.

Az Ntk. előírja a tanuló magatartásának és szorgalmának értékelését és minősítését. Ezt a gyerek mentora trimeszterenként szövegesen vagy értékelő táblázatokban adja át.

## Osztályozó vizsga, javító vizsga, különbözeti vizsga a BPS-ben

### Osztályozó vizsga

Az alábbi esetekben merülhet fel, hogy osztályozó vizsgára van szükség:
 - Ha a gyerek évfolyamot ugrik annak érdekében, hogy tudásának és képességeinek leginkább megfelelő évfolyamon folytathassa a tanulást
 - Egy adott tárgyból szükséges még egy lezárt év ahhoz, hogy a gyerek előre hozott érettségit tehessen
 - Külföldi tanulmányait a BPS-ben folytató gyerek esetén a hozott bizonyítvány és dokumentált visszajelzések alapján nem eldönthető a gyerek évfolyambesorolása.
 - Egyéni tanrendben tanuló gyerekek esetén a félévi és év végi értékelés megállapításához
250 órát meghaladó hiányzás esetén, ha a gyerek portfóliója és kapott szöveges visszajelzései alapján nem állapítható meg, hogy készen áll-e az évfolyamlépésre.  

Évfolyamugrás valamint előrehozott érettségi esetén az osztályozó vizsga folyamat elindítását a szülőnek szükséges kérvényeznie. Ha az osztályozó vizsgára évfolyambesorolás, egyéni tanrend vagy hiányzás miatt van szükség, akkor a folyamatot a Budapest School client service csapat jelzésére indul el, a gyerek tanulóközösségének tanárcsapatával és a családdal egyeztetve határozza meg a vizsga időpontját, amelyen kötelező megjelenni. 

Azokban az esetekben, amikor osztályozó vizsgára kerül sor, a BPS értékelési és visszajelzési alapelveivel összhangban:
1. _Alsós korcsoportban_ (minden esetben): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatoi) értékelik, valamint a tanárcsapat dönt a megfelelő szöveges szummatív értékelésről, miszerint a gyerek 
A. Teljesítette az elvárásokat az adott a tárgyból.
B. Még nem áll készen az évfolyamváltásra az adott tárgyból

2. _Felsős és középiskolás korcsoportban_ (ha az osztályozó vizsga évfolyamugrás, külföldi tanulmányok, egyéni tanrend vagy hiányzás miatt szükséges): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatok) értékelik, és a tanárcsapat dönt a megfelelő szöveges szummatív értékelésről (ld. fent) VAGY: a család kérésére a rubric visszajelzés alapján osztályzatot állapít meg. 
3. _Középiskolás korcsoportban_ (ha az osztályozó vizsga előrehozott érettségi miatt szükséges): az osztályozó vizsgát a tanárok a szöveges visszajelzéssel (értékelő táblázatokkal) értékelik, és a tanárcsapat a visszajelzés alapján dönt az osztályzatról. 

### Javító vizsga
Javító vizsgára akkor van szükség, ha a tanév végén a gyerek egész év során gyűjtött szöveges visszajelzései és portfóliója alapján a tanárcsapat hozzájárulásos döntést hoz arról, hogy a gyerek egy vagy több tárgyból még nem áll készen az évfolyamváltásra. 

Azokban az esetekben, amikor javító vizsgára kerül sor, a BPS értékelési és visszajelzési alapelveivel összhangban:
1. _1-7. évfolyamokon:_ az osztályozó vizsgát a tanárcsapat szöveges visszajelzéssel (értékelő táblázatokkal) értékeli, valamint dönt a megfelelő szöveges szummatív értékelésről, miszerint a gyerek 
A. Teljesítette az elvárásokat az adott a tárgyból.
B. Még nem áll készen az évfolyamváltásra az adott tárgyból
2. _8-12. évfolyamon_  (ha az osztályozó vizsgát nem core vagy érettségihez szükséges tantárgyból tesz a gyerek): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatokkal) értékelik, és a tanárcsapat dönt a megfelelő szöveges szummatív értékelésről (ld. fent) VAGY: a család kérésére a rubric visszajelzés alapján osztályzatot állapít meg. 
3. _Középiskolás korcsoportban_ (ha az osztályozó vizsgát core vagy érettségihez szükséges tárgyból tesz a gyerek): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatokkal) értékelik, és a tanárcsapat a visszajelzés alapján dönt az osztályzatról. 


### Különbözeti vizsga
Különbözeti vizsgára iskolaváltás esetén lehet szükség abból a tárgyból, amelyet a gyerek a küldő intézményben más tematika szerint tanult, vagy egyáltalán nem tanult. 

Azokban az esetekben, amikor különbözeti vizsgára kerül sor, a BPS értékelési és visszajelzési alapelveivel összhangban:
1. _1-7. évfolyamokon:_ az osztályozó vizsgát a tanárcsapat szöveges visszajelzéssel (értékelő táblázatokkal) értékeli, valamint dönt a megfelelő szöveges szummatív értékelésről, miszerint a gyerek 
A. Teljesítette az elvárásokat az adott a tárgyból.
B. Még nem áll készen az évfolyamváltásra az adott tárgyból
2. _8-12. évfolyamon_  (ha az osztályozó vizsgát nem core vagy érettségihez szükséges tantárgyból tesz a gyerek): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatokkal) értékelik, és a tanárcsapat dönt a megfelelő szöveges szummatív értékelésről (ld. fent) VAGY: a család kérésére a rubric visszajelzés alapján osztályzatot állapít meg. 
3. _Középiskolás korcsoportban_ (ha az osztályozó vizsgát core vagy érettségihez szükséges tárgyból tesz a gyerek): az osztályozó vizsgát a tanárok szöveges visszajelzéssel (értékelő táblázatokkal) értékelik, és a tanárcsapat a visszajelzés alapján dönt az osztályzatról. 

### Miért elfogadható ez az osztályozó vizsga folyamat?

Az évfolyamok teljesítésének és az osztályzatok megítélésének folyamata a gyerek évközi alkotásait, munkáját, teljesítményét tartalmazó portfólió, és a modulok végén kapott szöveges visszajelzések (értékelő táblázatok) alapján történik, ezért a folyamat megfelel a 20/2012.(VIII.31.) EMMI-rendelet 64. § (1) azon elvárásának, hogy a tanuló osztályzatait _„évközi teljesítménye és érdemjegyei"_ alapján kell megállapítani, azzal a megkötéssel, hogy a BPS modell elfogadja az érdemjegyeknél gazdagabb szöveges és értékelőtáblázat-alapú visszajelzést.

Ha a szöveges visszajelzések és a portfólió alapján nem állapítható meg az évfolyamszintlépéshez és
az osztályzathoz szükséges tanulási elvárások teljesülése, akkor a gyereknek ki kell egészítenie a portfólióját. Ez a tanárcsapat döntése alapján történhet egy tudáspróbával, egy projektfeladattal, vagy más, a gyerek tudásának demonstrálására alkalmas megoldás segítségével. Vagy el kell fogadnia, hogy _még_ nem érte el a
következő évfolyamszintre lépéshez szükséges tanulási eredményeket.

Ugyancsak kiegészítendő a portfólió, ha igazolt vagy igazolatlan mulasztások miatt az iskolában végzett munka nem volt elegendő az évfolyamváltáshoz szükséges tanulási elvárások teljesítésére. 

Ha a gyereket bármely okból felmentették a tanórai foglalkozásokon való részvétel alól, akkor a 20/2012.(VIII.31.) EMMI-rendelet 64. § (2) alapján osztályozóvizsgát kell tennie. Ebben az esetben is a fenti folyamatot kell követni az iskolának.
