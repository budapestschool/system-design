# Érettségire készülés

A BPS-ben a helyi tantervben szereplő tantárgyakból lehet középszintű érettségit tenni.

Az emelt szintű érettségire fakultációs órák felvételével lehet felkészülni. A
BPS az emelt szintű érettségire való felkészítést a következő tantárgyakból
vállalja: magyar nyelv és irodalom, történelem, matematika, digitális kultúra,
fizika, biológia, földrajz, angol, spanyol, német és testnevelés.


## Érettségi időszakok jelentkezés

Az iskolánkban középszintű érettségi vizsga tehető a tavaszi vizsgaidőszakban, mivel nem vagyunk emelt szintű vizsgaközpont és nem szervezünk érettségit az őszi vizsgaidőszakban. 
Aki érettségi vizsgát szeretne tenni (közép- vagy emelt szinten) az őszi vizsgaidőszakra a kormányhivatal által kijelölt iskolában kell jelentkeznie (lakóhely szerint, a kormányhivatal honlapján látható lista alapján) adott év szeptember 5-ig. 

Aki a tavaszi vizsgaidőszakban szeretne közép- vagy emelt szintű érettségi vizsgát tenni, a BPS Középiskolában jelentkezhet adott év február 15-ig. 


## Érettségi tárgyak

A BPS Középiskolában a következő tárgyakból lehet középszintű érettségi vizsgát tenni: 

### Kötelező érettségi tárgyak
- Magyar nyelv és irodalom (12-ik évfolyam elvégzésekor)
- Matematika (12-ik évfolyam elvégzésekor)
- Történelem (12-ik évfolyam elvégzésekor)
- Első idegen nyelv - angol (10. Évfolyam végétől előrehozott érettségi tehető - osztályozó vizsgák szükségesek - vagy 12. Évfolyam elvégzésekor)


### Választható érettségi tárgyak
Középszinten az érettségi az iskolánkban tehető le, emelt szinten a kormányhivatal által kijelölt iskolában. 

- Állampolgári ismeretek (12. Évfolyam elvégzésekor)
- Digitális kultúra (10. Évfolyam végétől előrehozott érettségi tehető - osztályozó vizsgák szükségesek - vagy 12. Évfolyam elvégzésekor)
- Ének-zene (10. Évfolyam elvégzése után)
- Fenntarthatóság (11. Évfolyam elvégzése után előrehozott érettségi tehető vagy 12-ik évfolyam elvégzésekor)
- Második idegen nyelv (10. Évfolyam végétől előrehozott érettségi tehető - osztályozó vizsgák szükségesek - vagy 12. Évfolyam elvégzésekor)
- Mozgókép és médiaismeret (12. Évfolyam elvégzésekor)
- Természettudomány (10. Évfolyam végétől előrehozott érettségi tehető - osztályozó vizsgák szükségesek - vagy 12. Évfolyam elvégzésekor)
- Testnevelés és egészségfejlesztés (12. Évfolyam elvégzésekor)
- Vizuális kultúra ((10. Évfolyam elvégzése után))

A 2020-as NAT szerint és 2020 és 2021 szeptemberében 9. Évfolyamot kezdett diákjaink érettségi vizsgát tehetnek még
- Biológia (10. Évfolyam végétől előrehozott érettségi tehető, vagy 12. Évfolyam elvégzésekor)
- Földrajz (10. Évfolyam végétől előrehozott érettségi tehető, vagy 12. Évfolyam elvégzésekor)
- Kémia (10. Évfolyam végétől előrehozott érettségi tehető, 12. Évfolyam elvégzésekor)
- Fizika (10. Évfolyam végétől előrehozott érettségi tehető, vagy 12. Évfolyam elvégzésekor)
tantárgyakból. 

Az ezt követő évfolyamok természettudományból tehetnek nálunk érettségi vizsgát, mivel 2022 szeptemberével bevezettük ezt a tantárgyat. Ha egy 2022 után gimnáziumot kezdett diák szeretne biológiából, földrajzból, kémiából vagy fizikából érettségi vizsgát tenni, egyeztessen az iskolával. Ebben az esetben 9-ik és 10-ik, adott esetben 11-ik év adott tantárgyi ismereteiből osztályozó vizsgát kell tennie külön, így bocsátható ezekből a tárgyakból érettségi vizsgára, hiszen nem diszciplinárisan tanulta a természettudományokat, érettségi vizsgát viszont úgy szeretne tenni. 
