# A tanév ritmusa

A tanév több ciklus ismétlődésével írható le. A személyre szabott, saját célok által irányított tanulást a trimeszterek ciklusa határozza meg, az iskolarendszerben megszokott értékeléseket és bizonyítványokat a féléves ciklus határozza meg. Miért vezeti be az iskola a trimesztereket? Hogy tanévenként kétszer szánjunk minőségi időt arra, hogy a gyerekek ránézzenek a tanulási útjaikra, ha kell tervezzenek újra, és kapjanak új lendületet.

## Trimeszterek

A tanév tanulási útját három szakaszra bontja az iskola, ezeket három mérföldkő zárja le. Ezeket az időszakokat hívjuk _trimesztereknek_. Egy trimeszteren belül a tanulási célok
tervezése után következik a tanulás, és a ciklust a visszajelzés és
értékelés zárja. Amint egy ciklus véget ér, elkezdődik egy új.

Ez a felosztás követi az üzleti világ
negyedéves tervezését, néhány egyetem trimeszterekre bontását, de
leginkább az évszakokat. Minden periódus után értékeljük az elmúlt három
hónapot, ünnepeljük az eredményeket, és megtervezzük a következő
időszakot.

Az egyes trimeszterek átlagosan 10--11 hétig tartanak úgy, hogy a tanítással
eltöltött napokat és a tanítási szünetekek mindig az állami oktatásirányítás által kiadott
tanév rendjéhez igazodva határozzuk meg. A trimeszterek első
hete mindig a tervezéssel, utolsó hete mindig az értékeléssel telik.
Trimeszterenként átlagosan további egy hét a közösség építésével, önálló
tanulással zajlik.

A trimesztereken belül az egyes tanulóközösségek között lehetnek
néhány hetes eltérések, melyek a közösség sajátosságait követik.

A ciklusok állandósága adja a tanulás irányításához szükséges kereteket.
Ezek megtartásáért az egyes tanulóközösségek tanulásszervezői felelnek, működésüket a fenntartó monitorozza. A tanév ritmusát, a trimeszteralapú tervezés és a félévalapú iskolarendszer-szintű visszajelzés illeszkedését a következő táblázat
mutatja.

| Időszak    | Tevékenység                         |
| ---------- | ----------------------------------- |
| szeptember | közösségépítés                      |
|            | saját célok meghatározása           |
|            | modulok kialakítása és meghirdetése |
|            | tanulás, alkotás                    |
| október    | tanulás, alkotás                    |
| november   | tanulás, alkotás                    |
| december   | portfólió frissítése                |
|            | reflexiók                           |
|            | visszajelzések                      |
|            | célok felülvizsgálata               |
|            | modulok változtatása igény esetén   |
| január     | tanulás, alkotás                    |
|            | féléves értékelés kiadása           |
| február    | tanulás, alkotás                    |
| március    | portfólió frissítése                |
|            | reflexiók                           |
|            | visszajelzések                      |
|            | célok felülvizsgálata               |
|            | modulok változtatása igény esetén   |
| április    | tanulás, alkotás                    |
| május      | tanulás, alkotás                    |
| fél június | évzárás, értékelés, bizonyítványok  |

## Szeptemberi közösségépítő időszak
A tanulóközösségek tanárai a közösség igényei szerint az első hetet vagy heteket 
közösségépítéssel töltik. Ez az időszak az első trimeszterbe esik. 

## Féléves és évvégi értékelés

A külső rendszereknek és a törvényeknek való megfelelés miatt
az iskola (az ötödik évfolyamtól) két félévre bontva határozza meg a tananyagtartalmakat, és
ezzel összhangban az iskola automatikusan félévenkénti szöveges értékelést állít ki
a portfólió alapján, ami nem más, mint a portfólióba
bekerült dokumentált visszajelzések összegyűjtése.

A félév vége január vége (amit egy miniszteri rendelet évente határoz meg), ami a BPS-iskolában második trimeszterre esik, ezért a
féléves értékelést az első trimeszter végén rögzített állapot szerint
adja ki az iskola. Az 5--12. évfolyamon tanuló gyerekeknek decemberben és januárban
módjuk és lehetőségük van a portfóliójuk frissítésére, ha a féléves
értékelés és az osztályzatokra váltás eredménye számukra iskolaváltás,
továbbtanulás miatt, vagy egyéb okból fontos. Részletesebben az [évfolyamról, osztályzatokról, bizonyítványról szóló fejezet](bizonyitvany-evfolyamok.md#evfolyam-osztalyzatok-bizonyitvany) határozza meg a folyamatot.

Év végén, június első két hete az évvégi zárásra és minősítésre történő felkészülés időszaka, ami a portfólió frissítését,
bővítését, kiegészítését jelenti.
