# A tanulási szerződés

A tanulási szerződés a gyerek, mentor és a szülő közötti, ami rögzíti

1. a gyerek, a mentor (iskola) és a szülő igényeit, elvárásait; ezek
   lehetnek: _„szeretném, ha a gyerekem naponta olvasna"_ típusú
   folyamatra vonatkozó kérések, vagy erősebb _„változtatnod kell a
   viselkedéseden, ha a közösségben akarsz maradni"_ típusú igények, határok
   megfogalmazása;

2. a gyerek céljait a következő trimeszterre, vagy a tanév végéig;

3. a gyerek, mentorok (iskola) és szülő vállalásait, amivel támogatják
   a cél elérését és a felek igényének teljesülését.

A tanulási szerződésre jellemző:

- A kitűzött célokat minél specifikusabban és mérhetőbben kell
  megfogalmazni. Javasolt az [OKR](https://en.wikipedia.org/wiki/OKR) (Objectives and Key Results, azaz Cél
  és Kulcseredmények) vagy a [SMART](https://en.wikipedia.org/wiki/SMART_criteria) (Specific, Measurable,
  Achievable, Relevant, Time-bound, azaz Specifikus, Mérhető,
  Elérhető, Releváns és Időhöz kötött) technika
  alkalmazása, hogy minél specifikusabb, teljesíthetőbb, tervezhetőbb
  és könnyen mérhető célokat tűzzenek ki.

- A kitűzött célokban való megállapodást követően, megállapodást kell
  kötni arról is, hogy ki és mit tesz azért, hogy a gyerek a célokat
  elérje.

- A mentor a teljes tanulóközösséget (a többi tanárt, a közösséget)
  képviseli a megállapodás során.

A tanulási szerződést néha hívjuk _megállapodásnak_ is. A megállapodás
és szerződés szavakat az iskola szinonimának tekinti. A _learning
contract_ az önirányított tanulást hangsúlyozó felnőttképzéssel
foglalkozó irodalomban bevett szakkifejezés már a 80-as évektől
[(Knowles, 1977)](https://books.google.hu/books/about/Self_directed_Learning.html?id=ljVZAAAAYAAJ&redir_esc=y).

Egy másik szakterületen, a pszichoterápiás munkában a
terápiás szerződések megkötésekor a közös munka kereteinek kialakítását
és fenntarthatóságát hangsúlyozzák [(Perczel, 2009)](http://semmelweis.hu/klinikai-pszichologia/files/2018/11/PFD_Bevezet%C3%A9s-a-pszichoter%C3%A1pi%C3%A1ba_2018.pdf). Erre is utalunk a
tanulási szerződés elnevezéssel. És van, amikor a _hármas szerződés_
kifejezést használjuk, hangsúlyozva, hogy mind a három szereplőnek, gyereknek, tanárnak és szülőnek
elfogadhatónak kell tartania a szerződés tartalmát.
