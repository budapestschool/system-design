# Tantárgyak

A Budapest School a ma gyerekeinek kínál olyan oktatást, ami segíti
felkészíteni őket a jövő kihívásaira. Információs társadalmunk
legnagyobb kihívása az adaptációs képességünk fejlesztése, ez az alapja
annak, hogy képesek legyünk eligazodni a folyamatosan változó, komplex
világunkban. A tanulásunk célja, hogy boldog, hasznos és egészséges
tagjai legyünk a társadalomnak. Iskolánkban a tanulás három rétege, a
tudásszerzés, a megtanultakat elmélyítő önálló gondolkodás és az aktív
alkotás egyszerre jelennek meg.

Az iskola a célok eléréséhez a NAT tantárgyi struktúráját használja a tanulás tartalmi keretezéséhez. A keretezésen azt értjük, hogy a tantárgyak tartalma határozza meg, hogy mivel kell mindenképp foglalkozni a Budapest School-iskolában, mit kell mindenképp megtanulni. Az egyes foglalkozások ezen tantárgyak tanulási eredményeinek elérését támogatják.

## Választott kerettanterv

Az iskola a miniszter által közzétett, _Kerettanterv az általános iskola 1-4., 5-8. és Kerettanterv a gimnáziumok 9–12. évfolyamára_ című tantárgyi kerettantervek [(Kerettanterv, 2020)](https://www.oktatas.hu/kozneveles/kerettantervek/2020_nat) és a
_Nemzeti alaptanterv_ tananyagtartalmát kínálja a gyerekeknek. Ezek a tantervek adják a tartalmi szabályozás kereteit. Az iskola arra törekszik, hogy ezt egy minél inkább önvezérelt, személyre szabottabb módon tanulják a gyerekek.

A Budapest Schoolban a tantárgy a mindennapokban nem feltétlenül jelenik meg önálló tanóraként. A tantárgy elvégzésének és az évfolyamlépés feltétele, hogy a gyerekek portfóliója és szöveges visszajelzései demonstrálják a NAT-ban tanulási eredmények formájában megadott elvárásokat.

## Tanulási eredmények — a formális tanulás alapegységei

Az iskola a NAT _tanulási eredményeit_ veszi alapul. A tanulási eredmények (learning outcomes)
tudás, képesség, kompetencia, attitűd kontextusában meghatározott
kijelentések arra vonatkozóan, hogy a tanulónak mit kell tudnia, mit
kell értenie, és mire legyen képes, miután lezárt egy tanulási
folyamatot, függetlenül attól, hogy hol, hogyan, mikor szerezte meg
ezeket a kompetenciákat [(Cedefop, 2008)](https://www.cedefop.europa.eu/en/publications-and-resources/publications/4079)

A tanulási eredmények elérése a a modulokon keresztül vagy az egyéni, önálló tanulás során történik. A tanulás történhet az iskolában vagy azon kívül, lehet formális, nem-formális vagy informális. Az iskola támogatja a tanárokat abban, hogy minél differenciáltabb, gyerekekre szabott módon tudják segíteni a tantárgyi fejlesztési célok elérését és ismeretek megszerzését. Az egyes a modulok különféle tanulási eredmények elérését is támogathatják, ezzel több tantárgy részcéljait is teljesíthetik. Egy-egy modul célját a tanulásszervezők az elérendő tanulási eredmények segítségével, és saját célokkal, érdeklődéssel kiegészítve adják meg, figyelembe véve az életkori sajátosságok, az egymásra épülés és az átjárhatóság követelményeit.

A BPS modell tantárgyankénti bontásban adja meg a különböző korcsoportokra vonatkozó, elérendő tanulási eredmények halmazát, amik megfelelnek a NAT tanulási eredményeinek. Ezek adják a tanulásszervezés minden tanulóközösségben érvényes alapkövetelményeit, a tartalmi kereteket és fejlesztési célokat. A NAT (és a közzétett kerettantervek) témakörei ajánlások, ahogy a jogszabály fogalmaz sorrendjük változtatható és koherens rendszerbe építhető. Ezt a rendszert alakítják ki a tanárok a modulok során, és a gyerekek az egyéni tanulóidők alkalmával.

Két tanár, két csoport vagy két gyerek között a tanmenet tekintetében akár jelentős eltérések lehetnek addig, amíg a gyerekek számára a NAT tanulási eredmények elérését lehetővé teszik.


## Tantárgyi óraszámok

| Tantárgy                          |   1 |   2 |   3 |   4 |   5 |   6 |   7 |   8 |   9 |  10 |  11 |  12 |
| --------------------------------- | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: |
| Állampolgári ismeretek            |     |     |     |     |     |     |     |   1 |     |     |     |   1 |
| Digitális kultúra                 |     |     |   1 |   1 |   1 |   1 |   1 |   1 |   2 |   1 |   2 |     |
| Dráma és színház                  |     |     |     |     |     |     |   1 |     |     |     |     |     |
| Első idegen nyelv                 |     |     |     |   2 |   3 |   3 |   3 |   3 |   3 |   3 |   4 |   4 |
| Ének-zene                         |   2 |   2 |   2 |   2 |   2 |   1 |   1 |   1 |   1 |   1 |     |     |
| Etika                             |   1 |   1 |   1 |   1 |   1 |   1 |   2 |   2 |     |     |     |     |
| Hon- és népismeret                |     |     |     |     |     |   1 |     |     |     |     |     |     |
| Környezetismeret                  |     |     |   1 |   1 |     |     |     |     |     |     |     |     |
| Magyar nyelv és irodalom          |   7 |   7 |   5 |   5 |   4 |   4 |   3 |   3 |   3 |   4 |   4 |   4 |
| Második idegen nyelv              |     |     |     |     |     |     |     |     |   3 |   3 |   3 |   3 |
| Matematika                        |   4 |   4 |   4 |   4 |   4 |   4 |   3 |   3 |   3 |   3 |   3 |   3 |
| Mozgóképkultúra és médiaismeret   |     |     |     |     |     |     |     |     |     |     |     |   1 |
| Technika és tervezés              |   1 |   1 |   1 |   1 |   1 |   1 |   1 |     |     |     |     |     |
| Természettudomány                 |     |     |     |     |   2 |   2 |   4 |   5 |   5 |   5 |   2 |     |
| Testnevelés és egészségfejlesztés |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |
| Történelem                        |     |     |     |     |   2 |   2 |   2 |   2 |   2 |   2 |   3 |   3 |
| Vizuális kultúra                  |   2 |   2 |   2 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |     |     |
| Mentoridő                         |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |
| Projekt                           |     |     |     |     |     |     |     |     |   3 |   3 |     |     |
| Kötött célú órakeret              |     |     |     |     |     |     |     |     |     |     |   4 |   4 |
| _Összesen_                        |  23 |  23 |  23 |  24 |  27 |  27 |  28 |  28 |  32 |  32 |  31 |  29 |

- _Hon- és népismeret_ tantárgy a 6. évfolyamon van.
- A _Biológia_, _Fizika_, _Földrajz_ és _Kémia_ diszciplináris tartalmak az iskola a 7-10. évfolyamon egy integrált _Természettudomány_ tantárgy részeként jelennek meg. A tantárgy keretében a diszciplináris tantárgyak kerettantervi tartalmai megtanításra kerülnek. A tantárgy keretében kitűzőtt tanulási eredményeket a helyi tanterv tartalmazza. Az iskola garantálja, hogy minden tanuló megszerzi a Nat-ban és a kerettantervekben előírt természettudományos tudást, ismereteket legalább a természettudományos órán.

  Abban az esetben, ha egy gyereknek továbbtanulás vagy érettségi miatt szüksége van a diszciplináris tantárgyakból osztályzatokra, az iskola megszervezi számára az osztályozó vizsgákat és biztosítja számára az egyéni felkészülés lehetőségét.

- A _Második idegennyelv_ tantárgy célja, hogy a gyerekek a 12. évfolyam végére elérjék a KER szerinti A2 szintet.
- A 12. évfolyamon az iskola a _Mozgóképkultúra és médiaismeret_ tantárgyat választja.
- Közösségi nevelés (osztályfőnöki) tantárgy helyett heti egy óra _mentoridőt_ kap minden gyerek.
- A NAT-ban szereplő szabadon választható órakeret terhére _Fenntarthatóság_ tantárgyat választhatnak a diákok 11. és 12. évfolyamon (az oktatásért felelős miniszter által leírt kerettantervekből választva).

A BPS modell a NAT tantárgyi követelményeihez a tanulási eredményeken keresztül kapcsolódik. A modell nem köti meg, hogyan éri el a gyerek az eredményt, csak hogy mit kell elérnie. Ezért a tantárgyi óraszámok szerepe harmadrendű az iskolában.

A modulok során több tantárgyi tananyagot is érinthetnek a modul
résztvevői. Egy modul így több tantárgyi órát is lefedhet, ráadásul ez
óraszám-megtakarítással is jár. 5 óra angolul tartott drámafoglalkozás
egyszerre számíthat 5 óra _magyar nyelv és irodalom_ és 5 óra
_idegennyelv_ órának.

A modulok tantárgyi óraszámát a teljes modul hosszára kell számítani,
nem pedig hetente. Egy összevont természettudományi modul, ami érinti a kémiát
és a fizikát is, nem kell, hogy minden héten járjon kémia tanulási
eredménnyel. Több tantárgyat lefedő modul esetén a tantárgyi óraszámokat
úgy kell számolni, hogy először meg kell állapítani, hogy átlagban az
idő hány százalékában foglalkozik a modul egy-egy tantárgy anyagával,
majd a modul teljes hosszából becsülhető a tantárgyi óraszám. Például
egy _tudományos kísérletezés_ modul során az idő 30%-ában foglalkozunk
kémiával, 40%-ában fizikával és 30%-ában szociálpszichológiával. A modul
egy trimeszteren keresztül tart, kéthetente 4 órában. Ebből számolható a
modul teljes hossza, ami itt _12/2x4 = 24_ óra. Ez hetente 24x0.3 = 7.2 óra kémiának és 7.2 óra fizikának felel meg.

Az önálló tanulás tantárgyi óraszámait nem lehet ilyen egyszerűen tantárgyi óraszámokra leképezni, mert ehhez minden gyerek minden pillanatában rögzíteni kéne, hogy akkor éppen melyik tantárgy tananyagával foglalkozik.

A tanulás tantárgyak szerinti eloszlását az iskola folyamatosan követi,és gyerekenként, illetve évfolyamonként képes kimutatást készíteni egy adott időszakban a tantárgyi óraszámok teljesüléséről.

#### Mentoridő

Minden gyerek hetente részt vesz mentori foglalkozáson, amikor a mentor a gyerek számára minőségi figyelmet ad. A mentor ilyenkor a gyereket segíti a saját céljainak
megfogalmazásában, és abban, hogy legyen lehetősége reflektálni a saját fejlődésére. Ez az óra a gyerekek és a mentorok számára kötelező foglalkozás.

#### Projektóra

A BPS iskolában a 9. és 10. évfolyamon 3-3 óra projektóra jelenik meg, ahol projektoktatás során a téma meghatározása, a a munkamenet megtervezése és megszervezése, a témák feldolgozása és a feladat megoldása a gyerekek érdeklődésére, a gyerekek és a tanárok közös tevékenysége és együttműködése alapján alakul.

A projektórák célja, hogy a gyerekek projekteken csapatban dolgozzanak, közös produktumokat hozzanak létre, és hogy a projekthez szükséges, a pedagógia program által amúgy is előírt tantárgyi tanulási eredményekből minél több relevánsat elérjenek.

A projektórák témáját a pedagógia program előre nem rögzíti, hiszen az a tanárok egy a gyerekek együttműködésére épül, azonban a projekteket a tanároknak és a gyerekeknek trimeszterenként le kell dokumentálni.

##### Értékelés

Projektórákon való részvételt, a munkát és az elkészült eredményeket a tanárok és a gyerekek többszempontú értekelőtáblázatok segítségével értékelik.

Projektórából osztályozó vizsga nem szervezhető. Amennyiben egy gyerek valamilyen okból a projektórákon nem tudott részt venni, akkor következő évfolyamba akkor léphet, ha a tantárgyi tanulási eredményeket elérte. Magyarul fogalmazva: a projektórák a tantárgyi tananyagon túlmutató tevékenységek.

### A választott kerettanterv által meghatározott óraszám feletti kötelező tanórai foglalkozások

A BPS modell nem határoz meg kötelező tanórai foglalkozásokat. A modell azt szeretné, ha a gyerekek minél inkább azt tanulnák, amire nekik szükségük van, vagy amit szeretnének. Az iskola biztosítja, hogy minden gyerek megismerkedjen az [elsősegéllyel](/helyi-tanterv/elsosegely.md#elsosegely-nyujtasi-alapismeretek), a körülötte éle [nemzetiségekkel](/helyi-tanterv/nemzetisegek.md#nemzetisegek-megismerese) és legyen lehetősége [mindennapos testmozgásra](/helyi-tanterv/mindennapos-testmozgas.md#mindennapos-testmozgas).

### A kerettantervben meghatározottakon felül a nem kötelező tanórai foglalkozások

A kerettantervben meghatározottakon felül az iskola nem határoz meg előre nem kötelező tanórai foglalkozásokat, ezért azok megtanítandó és elsajátítandó tananyaga, az ehhez szükséges kötelező, kötelezően választandó vagy szabadon választható tanórai foglalkozások megnevezése, óraszáma nincs rögzítve. A tanulásszervező tanárok új foglalkozásokat hirdethetnek.

## A tantárgyak szerepe a mindennapokban

A Budapest School iskoláiban a tantárgyak ugyanúgy kapnak szerepet, mint
a NAT által definiált kulcskompetenciák, fejlesztési területek: a tanár
nem mondja azt a gyerekeknek, hogy „most kezdeményezőképességet és
vállalkozói kompetenciát fejlesztünk"; a fejlesztés a különböző feladatok
elvégzésének eredménye. A Budapest School
iskolákban a komplex tevékenységek és a fókuszált gyakorlás vannak előtérben. A tantárgyak a
tanulás tartalmi elemeinek forrásai és keretei: a tanulandó dolgok
halmazaként működnek. Az, hogy milyen csoportosításban történik a
tanulás, az a tanárokra és (felsőtől) a gyerekekre van bízva.

A tantárgyak ezért elsősorban a tanulási-tanítási egységek
kialakításakor, a modulok kiírásakor és azok kimeneti értékelésekor
jelennek meg, a mindennapok struktúráját, a napi- és hetirendet azonban
a modulok adják. Egyes modulok több tantárgy fejlesztési céljainak is
eleget tehetnek, több tantárgy tanulási eredményének elérését is célul
tűzhetik ki, összhangban a NAT-tal. A tantárgyaknak ezzel együtt fontos
célja, hogy segítsék a tanulás tartalmi egyensúlyának fennmaradását.

Minden tanulási-tanítási egység, azaz modul lezártakor külön értékelni kell
a tantárgyankénti tudáselsajátítást. Tulajdonképpen nem történik más,
mint például az, hogy egy összevont természettudományi
foglalkozássorozat végén értékeljük, hogy mennyit bővült a gyerek
tudása matematikából, biológiából, kémiából és fizikából.

## Modulok, visszajelzések és tanulási eredmények

A gyerekek egyik feladata az iskolában, hogy tanulási eredményeket érjenek el. Ezt megtehetik a modulok elvégzésével, vagy más tanulási helyzetekben. A modulok leírásaiban a tanárok világossá teszik, milyen célok érhetőek el a modul teljesítésével, a modulokhoz kidolgozott értékelő táblázatok segítségével pedig visszajelzést készítenek a gyerekek számára arról, hogyan sikerült elérniük a kitűzött célokat. Vagyis  a gyerekek előrehaladása tanulásban a modulokhoz kapcsolódó sokszempontú visszajelzések, azaz az értékelő táblázatok segítségével nyomonkövethető mind a gyerek, mind a szülők mind pedig a tanárcsapat számára. 

#### A modulok különféle tanulási eredmények elérését teszik elérhetővé

A modulokok tervezésekor és összeállításakor a tanulásszervezők a
szaktanárokkal közösen határozzák meg a modul céljait, de azok
meghirdetéséért mindig a tanulásszervezők felelnek. A modulok céljaiban a tanárok megfogalmazzák, hogy a modul elvégzésével a gyerekek mire lesznek képesek, milyen ismeretekre tesznek szert, milyen produktumokat hozhatnak létre. A modulok céljainak megfogalmazásakor a tanárok a NAT tantárgyi tanulási eredményekre támaszkodhatnak. 

Arra is van lehetőség, hogy egy modulban a tanárok különböző tantárgyakhoz tartozó fejlesztési célokat válasszanak, ezzel biztosítva az interdiszciplinaritást, valamint a
Budapest School [kiemelt fejlesztési területeihez](/tanulas-megkozelitese/kiemelt-fejlesztesi-teruletek.md#kiemelt-fejlesztesi-teruletek) való integrált
kapcsolódást.

A tanulási eredmények időbeni egymásra épülést feltételeznek,
melyben azonban van lehetőség előre- és hátrafelé is lépni. Előre,
amenynyiben a modul meghirdetésekor az arra jelentkező gyerekcsoportnál
a megfelelő előkészítés megtörtént, hátra, amennyiben ezt
elmélyítés/felzárkóztatás jelleggel szükségesnek ítéli a mentor vagy a
modultartó tanár. Vagyis akkor foglalkozzon egy gyerek a
10 000-es számkörrel, ha a 100-as számkört már begyakorolta. Az egymásra
épülés biztosításáért a modult meghirdető tanulásszervező felel.

Számos tanulási eredmény nem egyszeri alkalommal érhető el, mert akár több trimeszteren át tartó ismétlődést, gyakorlást, elmélyítést feltételeznek. Például a "Képes a múlt és jelen alapvető társadalmi, gazdasági, politikai és kulturális folyamatairól, jelenségeiről véleményt alkotni." történelem tanulási eredmény ismétlődően megjelenhet a fejlesztési célok között egy felsős gyerek moduljaiban, miközben a tanulócsoport újabb és újabb történelmi korszakról bővíti a tudását. 

#### Új tanulási eredmények

A gyerekek a modulok során, valamint az egyéni projektektek megvalósítása közben, vagy nem-formális és informális tanulási helyzetekben olyan tanulási eredményeket is elérhetnek, amelyek nem szerepelnek a NAT által megfogalmazott tantárgyi tanulási eredmények között. Az ezen tanulási élmények eredményeként demonstrált tanulás (pl. megszerzett új ismeret vagy jártasság ) is bekerülhet a gyerek portfóliójába, melyekre a gyerek visszajelzéseket kérhet és kaphat tanáraitól.  

#### Tanulási eredmények és a tantárgyi haladás nyomonkövetése

A gyerek előrehaladásának nyomonkövetését a tanulásban a dokumentált szöveges visszajelzések és a portfóliók teszik lehetővé. A modulok végén létrehozott többszempontú, kidolgozott szintleírásokat tartalmazó értékelő táblázatok adnak visszajelzést arról, hogy a kitűzött célokat a gyerek milyen mértékben érte el és demonstrálta. Az értékelő táblázatokban megnevezett visszajelzési szempontok kapcsolódnak a NAT tanulási eredményekből következő tantárgyi fejlesztési célokhoz. Így a BPS iskolában a modulok céljai, a foglalkozásokon megvalósuló tevékenységek és a visszajelzések szempontjai egy konzisztens, holisztikus tanári tervezés nyomán létrejövő tanulásszervezés részei. 


