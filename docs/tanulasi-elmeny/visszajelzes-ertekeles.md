# Visszajelzés, értékelés

Ahhoz, hogy hatékony legyen a tanulás, fejlődés, fontos, hogy a
gyerekek, tanárok és szülők is tudják, hogy

1. hol tart most a gyerek, mit tud most,

2. hova akar vagy kell eljutni, azaz, mi a célja,

3. mi kell ahhoz, hogy elérje a célját.

Ezek mellett mindenkinek hinnie kell abban, hogy odafigyeléssel,
gyakorlással a gyerek meg tud tanulni egy konkrét dolgot. Fontos, hogy
magas legyen a gyerekek énhatékonysága, erős legyen az önbizalmuk, és
nem szabad félniük a hibázástól, a nem-tudástól, mert a tanulás első
lépése, hogy elfogadjuk, hogy valamit nem tudunk. Azaz fontos, hogy
fejlődésfókuszú legyen gondolkodásuk (growth mindset) [(Dweck, 2006)](https://www.mindsetworks.com/science/),
azaz

4. hinniük kell, hogy el tudják érni a céljukat.

A visszajelzés, értékelés akkor jó és hasznos, azaz hatékony, ha ebben
a négy dologban segít. Mai tudásunk szerint ehhez:

- Rendszeresen visszajelzést kell kapniuk és adniuk.

- A tanulási céloknak és visszajelzéseknek minél specifikusabbaknak kell lenniük azaz például ne a 8. osztályos _matematikatudást_ értékeljük, hanem hogy mennyire képes valaki _fagráfokat használni feladatmegoldások során_. (Ez a konkrét példa a matematika tantárgy egyik tanulási eredménye.)

- A _„hol tartok most"_ diagnózisnak mindig cselekvésre, viselkedésre kell vonatkoznia. Ne az legyen a visszajelzés, hogy _„ügyes vagy egyenletekből"_, hanem hogy _„gyorsan és pontosan oldottad meg a 4 egyenletet"_. A legjobb, amikor a visszajelzés konkrét megfigyelésen alapul, és tudni, hogy mikor, hol történt az eset: _„amikor társaiddal Minecraftban házat építettél, akkor pontosan kiszámoltad a ház területét"._

- Ha a cél nem a mások legyőzése, akkor a visszajelzés se tartalmazzon olyan állítást, ami másokkal való összehasonlítást tartalmaz (így kerüljük a _tehetség_ szót is, aminek bevett definíciója szerint az átlagnál jobb képesség). A mások szintjéhez hasonlított szint felmérése akkor (és csak akkor) fontos, amikor a cél egy versenyszituációban jó eredményt elérni.

- A gyerek legyen részese a visszajelzésnek, ő is értékelje saját munkáját. Így fejlődik a [metakogníciója](https://educationendowmentfoundation.org.uk/public/files/Publications/Metacognition/EEF_Metacognition_and_self-regulated_learning.pdf). Értse, tudja, hogy miért és mire vonatkozik a visszajelzés.

- A visszajelzésnek transzparensen hatással kell lennie a tanulásszervezésre. Legyen része a folyamatnak, és a gyerek, tanár és a szülő is értse, hogy a visszajelzés alapján mit és hogyan csinálunk másképp.

## Az értékelő táblázatok

A Budapest School visszajelzéseinek sokkal részletesebbeknek kell
lenniük, mint azt a tantárgyi érdemjegyek és osztályzatok lehetővé
teszik, ezért az érdemjegyek helyett a iskola értékelő táblázatokat (angolul
rubric) alkalmaz. Az értékelő táblázatban szerepelnek az értékelés
szempontjai és szempontonkénti szintek, rövid leírásokkal. Ezek alapján
a gyerekek maguk is láthatják, hogy hol tartanak, hogyan javíthatnak még
a munkájukon. A táblázatok formája minden visszajelzés esetén (értsd
modulonként, célonként) változtatható.

## Többszintű visszajelzés

A Budapest School-iskolákban a gyerekek többféle visszajelzést kapnak.

1. Minden modul elvégzése után a modul céljai, témája, fókusza alapján a gyerekek többszempontú, értékelő--táblázatos visszajelzést kapnak a tanulásukról, alkotásaikról, eredményeikről, fejlődésükről és viselkedésükről. Ez a _modulzáró visszajelzés_.

2. Trimeszter végén a gyererekek nemcsak a lezárult modulokról kapnak visszajelzést, hanem egy összegzést az elmúlt három hónapos időszakról is. Ebben a _trimeszterzáró visszajelzésben_ a gyerekek visszajelzést kapnak arról,

   1. hogy általában hogyan haladtak a tanulási és saját célok felé;
   2. hogyan vannak a közösségben, hogyan érzik magukat az iskolában;
   3. milyen a kapcsolata mentorával;
   4. és mennyire boldogulnak az önálló tanulás kihívásaival.

TODO: van most bármi szummatív?

> Ennek része, hogy a tantárgyi tanulási eredmények alapján hogyan haladt a gyerek a tantárgyakhoz tartozó követelmények teljesítésében. Ennek részletes leírását az [évfolyamok, osztályzatok, bizonyítvány fejezet](bizonyitvany-evfolyamok.md#evfolyam-osztalyzatok-bizonyitvany) tárgyalja.

## Értékelés nyelvezete

Fontos alapelv, hogy minél inkább a folyamatot, cselekvést jutalmazzuk,
értékeljük. Tehát arra fókuszáljunk, hogy _mit csinált_ a gyerek, és ne
arra, hogy _milyen_ a gyerek. Azokra a viselkedésmintákra adjunk
megerősítő visszajelzést, amelyeket látni szeretnénk a gyerekben később.
Lehetőleg kerüljük a statikus jellemzőkre, személyiségjegyekre vonatkozó
értékelést. Így nem azt mondjuk, hogy _„mindig jó vagy matekból"_,
hanem, hogy _„kitartóan és odafigyelve oldottad meg a feladatot"_.

Álljon itt néhány példa cselekvésre vonatkozó visszajelzésre.

- Fantasztikus, ma egy nagy kihívást választottál!
- Bátran vállaltad a rizikót!
- Nagyon jó! Tényleg sokat próbálgattad.

- Kitartóan csináltad, erre nagyon büszke vagyok!

- De jó, valami újat próbáltál ki ma!

- Köszönöm, hogy ma valakinek segítettél.

- Nagyon nagy öröm látni a haladásodat!

- Ne feledd, mindannyian tudunk a hibáinkból tanulni. Örüljünk annak, hogy ma valamit jobban tudunk, mint előtte.

- Hú, egy nehéz feladatot oldottál meg!

- Szép munka! Kipróbáltál egy másik módszert.

Az iskolában történő folyamatos visszajelzés a mindennapok része.
