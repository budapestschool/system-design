# korcsoportok a BPS-iskolában

## Mit jelent a korcsoportváltás a BPS-iskolában

Összetett folyamat megállapítani, hogy egy gyerek mikor áll készen arra, hogy a következő szakaszban folytassa a tanulását, vagyis hogy alsóból felsőbe, felsőből középiskolába lépjen. Ezekben a helyzetekben a tanárcsapatoknak a szülőkkel partnerségben számos szempontot kell mérlegelniük. Minden gyerek esetében van egy összbenyomás arról, kész-e új környezetben, új hatások által tovább fejlődni, vagy számára a legjobb, ha az aktuális korcsoportban maradva erősíti meg a tudását, képességeit. 

Egy negyedik vagy nyolcadik évfolyamos gyerek számára a következő korcsoportba történő átlépésnek az érettségről, egy mérföldkő lezárásáról, egy új kezdet megünnepléséről kell szólnia. Ezért a Budapest Schoolban a korcsoportváltás a tanárcsapatok és családok együttműködésére épülő komplex, többlépéses folyamat, amelyet minden mikroiskolában követünk egységes mérföldkövek és visszajelzési szempontok mentén, annak érdekében, hogy mind a gyerek, mind a szülők és tanárok számára biztonságos és kiszámítható legyen a váltás. Vagyis:

1. nem függhet egy mikroiskola egyedi felvételi gyakorlatától az, hogy egy már BPS-es gyerek a következő korcsoportban folytathatja-e a tanulást a BPS-ben
2. minden mikroiskolában a közösen elfogadott visszajelzési szempontokat alkalmazzuk egy gyerek korcsoport érettségének a megállapításához
3. ha a mikroiskoláknak nem is lehet egyedi felvételi gyakorlata annak eldöntésére, hogy egy másik mikroiskolából és korcsoportból érkező BPS-es gyerek folytathatja-e a tanulást az adott tanulóközösségben, egyedi gyógypedagógiai profilja viszont lehet, és ezt figyelembe kell venni egy különleges bánásmódot igénylő gyerek BPS-es tanulmányainak  eldöntésekor.

## A korcsoportváltás folyamata

A BPS-ben a korcsoportváltás egy sokszereplős, több mérföldkőből álló folyamat. A folyamat részeként a családok jobban megismerhetik a fogadó iskola tanárcsapatát, ill. a váltás előtt álló gyerekek saját élményeket is szerezhetnek a lehetséges fogadó BPS-iskolájukról. A folyamat központi eleme a korcsoportváltó visszajelzés létrehozása, ebben a küldő és fogadó iskola tanárcsapata egyaránt rész kell, hogy vegyen. 

A BPS-ben nem kizárólag a tantárgyi eredmények vagy osztályzatok függvénye, hogy egy gyerek folytathatja-e a tanulást egy másik BPS iskolában, hanem egy sokszempontú, komplex szöveges visszajelzés értékelése dönti el azt a kérdést, hogy a gyerek készen áll-e azokra a tanulási kihívásokra, amelyek az emelt korcsoportú BPS tanulóközösségben várnak rá. 
Vagyis az, hogy egy gyerek a 4. vagy 8. évfolyam befejezését követően egy másik BPS iskolában tanul-e tovább két lényeges kérdéstől függ: 
1. a gyerek szeretne-e a BPS-ben tovább tanulni; 
2. a korcsoportváltó visszajelzése alapján készen áll-e erre. 
A különös bánásmódot igénylő gyerekek esetén egy további szempont is felmerül: a választott fogadó BPS-iskola gyógypedagógiai profilja alapján biztosított-e, hogy a gyerek megkapja azokat a fejlesztéseket, amelyekre szüksége lehet.


### A 4. és 8. évfolyamos korcsoportváltó folyamat lépései: 

1. A tanárcsapatok felkészülése: a korcsoportváltásban érintett iskolák egyeztetnek egymással, a küldő iskola tanárai megismerik a fogadó iskola jellemzőit és az elvárásokat, amelyekre a korcsoportváltó gyerekeknek készen kell állniuk.
2. Közös szülői kör, amelynek során a 4., és 8. évfolyamos gyerekek szülei megismerhetik a felsőbb korcsoportú BPS iskolát.  
3. A küldő iskola kitölti a korcsoportváltó visszajelzés rá vonatkozó szempontjait a váltásra készülő gyerekekről. 
4. A szülők és a gyerek a mentor segítségével megismerik és átbeszélik a kapott visszajelzést. Ha felmerül annak a lehetősége, hogy a gyerek nem fog készen állni a tanév végére egy vagy több fejlesztési területen a korcsportváltásra, a szülők, gyerek és mentor megállapodást kötnek a következő lépésekről. 
5. Ismerkedés a fogadó iskolával és tanáraival: a korcsoportváltásra készülő gyerekek közös modulok, műhelyek, vagy playdate keretében élményeket gyűjtenek a fogadó iskoláról. A fogadó iskola tanárai hospitálhatnak a küldő iskolában, hogy jobban megismerjék a gyerekeket. 
6. Mentorok egyeztetése: a küldő és fogadó iskola mentorai egyeztetnek arról, hogyan látták a gyerekeket (készen állnak-e) a korcsoportváltó visszajelzések és az ismerkedő alkalmak alapján. 
7. A fogadó iskola kitölti a korcsoportváltó szöveges értékelő táblázat rá vonatkozó részét.
8. A szülők értesítést kapnak arról, hogy a következő tanévben a gyerekük készen áll-e arra, hogy egy magasabb korcsoportú BPS iskolában folytassa a tanulást, amennyiben ezt szeretné. 


### Felkészülés a korcsoportváltásra 3. és 7. évfolyamokon
A Budapest Schoolban egy új korcsoportba történő átlépés nem egyetlen döntési momentum, hanem egy több hónapon át tartó folyamat. Ezt erősíti, hogy a 2023/24-es tanévtől kezdve a 3. és 7. évfolyamos gyerekek számára a tanáraik a harmadik trimeszter elején elkészítik a korcsoportváltó visszajelzés 3. ill. 7. évfolyamra illesztett verzióját - ezt nevezzük _pillanatkép visszajelzésnek_. 
A pillanatkép visszajelzést a mentorok megmutatják és megbeszélik a családdal - teszik mindezt annak érdekében, hogy a következő tanévben a gyerek számára a korcsoportváltás minél kiszámíthatóbb legyen. Ha a pillanatkép visszajelzés eredményeként felmerül, hogy a gyereknek a korcsoportváltás szempontjából releváns fejlesztési területeken nehézségei vannak még, akkor az iskola és a szülők meg tudnak állapodni egy olyan fejlesztési terv kereteiről, amely lehetővé teszi, hogy a gyerek a következő tanév végére nagyobb magabiztossággal megérjen a váltásra - ha váltani szeretne. 


## A korcsoportváltó visszajelzés
A korcsoportváltó folyamat középpontjában egy sokszempontú, komplex visszajelzés áll, ez a korcsoportváltó értékelő táblázat.

Az alsós korcsoport végén a korcsoportváltó értékelő táblázat segítségével megvizsgált fő területek:
 - írás, olvasás, matematikai készségek (írásbeli szövegalkotás, szóbeli szövegalkotás, alapműveletek és matematikai kommunikáció 10000-es számkörben, figyelem, motiváció)
 - tanulás (önálló tanulás, tanulás mint fejlődési folyamat, (digitális) forráshasználat)
 - intraperszonális készségek (reflektivitás, rugalmasság, erősségek és fejlesztendő területek ismerete, önállóság)
 - interperszonális készségek (kapcsolódás a társaiddal, közösségi aktivitás, szabályok ismerete és betartása, konfliktuskezelés, érzelemszabályozás, segítség elfogadása és kérése, kommunikáció).

A felsős fogadó iskola visszajelzési szempontjai pedig:
 - feladatfókusz
 - bevonódás
 - együttműködés
 - kerettartás
 - elakadás a foglalkozásokon

Az általános iskola befejezéséhez közeledve a korcsoportváltó értékelő táblázat komplex képet ad a gyerekek aktuális ismereteiről és szociális képességeiről. A tárgyi tudásszint/hard skillek mellett azokat a szempontokat jeleníti meg, amiken keresztül megragadhatók egy általános iskolás és egy középiskolás gyerek ismeretszerzésének minőségi eltérései. Ezek:
 - írás, olvasás, matematikai készségek (írásbeli szövegalkotás, szóbeli szövegalkotás, szövegértés, alapműveletek és matematikai kommunikáció a racionális számok körében, matematikai problémamegoldás, nyelvelsajátítási motiváció, figyelem, motiváció)
 - tanulás (önálló tanulás, tanulás mint fejlődési folyamat, (digitális) forráshasználat)
 - intraperszonális készségek (reflektivitás, rugalmasság, erősségek és fejlesztendő területek ismerete, önállóság)
 - interperszonális készségek (kapcsolódás a társaiddal, közösségi aktivitás, szabályok ismerete és betartása, konfliktuskezelés, érzelemszabályozás, segítség elfogadása és kérése, kommunikáció, asszertivitás).

 A korcsoportváltó gyerekeket fogadó középiskola visszajelzési szempontjai pedig:
 - ismeretlen feladathoz való hozzáállás
 - előzetes matematikai ismeretek előhívása
 - íráskép és helyesírás
 - kreatív írás
 - nyelvi magabiztosság
 - bevonódás
 - együttműködés
 - szóbeli interakciók
 - komplex feladatmegoldás kapott instrukció alapján
 - prezentációs készség


A korcsoportváltó értékelő táblázat abban segít, hogy egy objektív szempontrendszer segítségével az iskola és a szülők releváns érveket mérlegeljenek egy transzparens döntési folyamatban. Az a cél lebeg a szemünk előtt, hogy a gyerekek érzelmileg biztonságos, és elég kihívást biztosító környezetben folytassák egyéni tanulási útjukat.
