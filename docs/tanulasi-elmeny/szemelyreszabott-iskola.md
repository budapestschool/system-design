# A személyre szabható közösségi iskola

Az iskola több helszínen működik. A gyerekek az iskolában kisebb közösségekbe tartoznak, a
[_tanulóközösségekbe_](/tanulasi-elmeny/tanulo-kozosseg.md#a-budapest-school-tanulokozossegei). Ezek a közösségek legfeljebb hat évfolyamot átölelő, 6-60 fős tanulási közösségekként működnek. A tanulóközösségek se jogi, se szervezeti szempontból nem önálló iskolák, nem összevont osztályok. A tanulóközösség nem más, mint az iskola egyik szervezeti egysége, ami önálló identitással, sajátos szubkultúrával, szabályokkal rendelkezhet.

A kisközösségükön belül a gyereket többfajta csoportbontásban tanulnak, és idősebb korban (10-12 év felett) különböző osztályok, közösségek tagjai is összeállhatnak egy-egy foglalkozásra, kurzusra, de identitásban, ,,egymáshoz-tartozás érzésben" a tanulóközösség az elsődleges közösségük. A közösség tagjai együtt és egymástól tanulnak.

Az egyes tanulóközösségeket [_tanulásszervező_ tanárok (tanulásszervezők)](tanari-szerepek.md#tanulasszervezo)
csapata vezeti. Minden gyereknek van egy kitüntetett tanára, a
[_mentora_](tanari-szerepek.md#mentor), aki egyéni figyelmével a fejlődésben segíti. Minden gyerek a
mentortanára segítségével és a szülők aktív részvételével
trimeszterenként meghatározza a [_saját tanulási céljait_](sajat-celok.md#sajat-tanulasi-celok).

A tanulásszervezők [_modulokat_](modulok.md#tanulasi-tanitasi-egysegek-a-modulok) hirdetnek ezen célokból, és a tantárgyak tanulási eredmények alapján. A modulok reflektálnak a mai világ alapvető
kérdéseire, integrálják a tudományterületeket és művészeti ágakat, azaz
a tantárgyakat, és egyenlő lehetőséget adnak a tudásszerzésre, az önálló
gondolkodásra és az alkotásra a gyerekek mindennapjaiban.

A modulok végeztével a gyerekek eredményei bekerülnek saját
[_portfóliójukba_](portfolio.md#portfolio), melyek tartalmazhatnak önálló vagy csoportos
alkotásokat, tudáspróbákat, vizsgafeladatokat, egymás felé történő
visszajelzéseket, a fejlődést jól mérő dokumentációkat vagy bármit,
amire a gyerek és tanárai büszkék vagy amit fontosnak tartanak. Erre a
portfólióra épül a Budapest School [_visszajelző és értékelő rendszere_](visszajelzes-ertekeles.md#visszajelzes-ertekeles).

A gyerekek mindennapjait meghatározó modulok több műveltségi területet,
többféle kompetenciát, több tantárgy anyagát is lefedhetik, és egy
tantárgy anyagát több modul is érintheti. Ezért is mondhatjuk, hogy a
BPS iskolákban a tantárgyközi tevékenységek vannak
előtérben. Az iskola szándéka, hogy a gyerekek folyamatosan
fejlődjenek a világ tudományos megismerésében (STEM), a saját és mások
kulturális közegéhez való kapcsolódásban (KULT), valamint a testi-lelki
egyensúlyuk fenntartásában (Harmónia), vagyis a [_kiemelt tantárgyközi
fejlesztési területekben_](/tanulas-megkozelitese/kiemelt-fejlesztesi-teruletek.md#kiemelt-fejlesztesi-teruletek).

Az iskola szerint az a tanárok döntése, hogy a gyerekek kémia órán
kísérleteznek, vagy kísérletezés órán foglalkoznak kémiával. A
iskola annyit határoz meg, hogy a 7--10. évfolyamszinten kémia
tantárgyhoz kapcsolódóan 73 különböző tanulási eredményt kell elérni, és
kísérletezéssel kapcsolatban pedig 21 különböző tanulási eredményt több
különböző tantárgyból (ezekből csak 8 kapcsolódik a kémia tantárgyhoz).

Tehát a tantárgyak a tanulás tartalmi elemeinek forrása és keretei: a
tanulandó dolgok halmazaként működik. Az, hogy milyen csoportosításban
történik a tanulás, az a szaktanárokra van bízva. A gyerekek lehet,
hogy csak félévente, az elszámolás időszakában találkoznak a tantárgyak
taxonómiájával. Ebben az időszakban veti össze minden gyerek és mentor,
hogy amit tanultak, alkottak és amiben fejlődtek, az hogyan viszonyul a
társadalom és a törvények elvárásaival, a Nemzeti alaptantervvel.

Az iskola a NAT tantárgyak témaköreit, tartalmát és követelményeit
_tanulási eredmények_ halmazaként adja meg. A gyerekek feladata az
iskolában, hogy tanulási eredményeket érjenek el és így sajátítsák el a
tantárgyak által szabott követelményeket. Tanulási eredményeket modulok
elvégzésével (is) lehet elérni, tehát a modulok elsődleges feladata,
hogy a tanulási eredményekhez vezető utat mutassák.

Az iskolában egyszerre jelennek
meg a NAT tantárgyi elvárásai, a
közoktatást szabályozó törvények szándékai, a gyerekek saját céljai és a mai
világra való integrált reflexió.

## A tanulás rendszerszemléletű megközelítése

Az oktatás tartalmának előzetes szabályozása helyett a Budapest School a
tanulás módjára helyezi a hangsúlyt. Az iskola alapelve, hogy integratív
módon folyamatosan keresse és fejlessze a pedagógiai, pszichológiai és
szervezetfejlesztési módszereket, amelyek korszerű módon tudják segíteni
a tanulás tanulását, az egyéni és csoportos fejlődést, a konfliktusok
feloldását.

A tanulás tartalmát tekintve a Budapest School a NAT tartalmára támaszkodik. A Budapest School Modell pedig a tanulás rendszerét, annak folyamatát szabályozza. E dokumentum alapján a NAT tanulási eredményein történő végighaladás mellett a
Budapest School nagy hangsúlyt fektet a gyerekek saját tanulási céljaira
és a célállítás módjára.
