# Portfólió

A tanulási-tanítási egységek, a modulok eredményeiből, a produktumokból
és visszajelzésekből a gyerek és a mentor portfóliót állít össze, hogy a
tanulás mintázatait észlelhesse, és a tanárok tudatosabban tudják a
gyereket segíteni a céljai kitalálásában és elérésében. A portfólió a
gyerek eredményeinek nyomon követését is szolgálja, és egyúttal a szülők
felé történő visszajelzés eszköze is. Minden gyerek portfóliója
folyamatosan épül: tartalmazza az általa elvégzett feladatokat,
projekteket vagy azok dokumentációját, alkotásait, eredményeit, az
esetleges vizsgák eredményeit és a társaitól, tanáraitól kapott
visszajelzéseket. A _portfólió célja_, hogy minden információ meglegyen
ahhoz, hogy

- a gyerek és mentora fel tudja mérni, hogy sikerült-e a kitűzött célokat elérni, illetve mire van szüksége még a gyereknek új célok eléréséhez;

- a szülő folyamatosan rálásson a gyereke tanulási útjára;

- megítélhető legyen, hogy a tantárgyi követelményekhez képest hol tart a gyerek;

- a gyerek a portfólió megtekintésével visszaemlékezhessen a tanultakra, ismételhessen, tudása elmélyülhessen;

- eredményei alapján bizonyítványt lehessen kiállítani.

![Portfólió  tartalmazza a gyerek által elvégzett feladatokat,
projekteket, alkotásait.](../pics/4a_portfolio_soma.jpg)

A portfólió folyamatosan frissül, a mindennapi, formális, nem-formális
és informális tanulási helyzetek bármikor adhatnak okot a portfólió
frissítésére. Az iskola életében kiemelt szerepe van a következő
eseményeknek.

Minden _modul végeztével_ a portfólióba kerül:

1. A képesség elsajátításának, tanulási eredmény elérésének a ténye. Ha a modul során a gyerek megtanult százas számkörben alapműveleteket végezni, akkor annyi kerül be a portfólióba, hogy „_Szóban és írásban összead, kivon, szoroz és oszt a százas számkörben_". Amennyiben a készséget-képességet a gyerek és a tanár megítélése alapján nem sikerült megfelelően elsajátítani, úgy a gyakorlás ténye kerül be a portfólióba.

2. Az alkotás vagy a projektmunka eredménye, ha a modul célja egy alkotás létrehozása volt.

3. A részvétel ténye, ha a jelenlét volt a modul célja (például kirándulás az Országos Kéktúra útvonalán).

4. Az elvégzett vizsgák, tudáspróbák, képességfelmérők, diagnózisok eredményei tanulási eredmények igazolásával is járnak.

5. A _kipakolás_ célja, hogy a gyerekek a tanároknak, szülőknek és más érintetteknek bemutassák elvégzett munkájukat, azaz a portfólióváltozásukat. A kipakolásra való felkészülés tulajdonképpen a portfólió összeállítása, prezentálásra való felkészítése, a _portfólió frissítése_.

6. Társas visszajelzés eredményeként minden gyerek kap visszajelzést a társaitól. Ilyenkor összegyűjtik, mit tett a gyerek, ami a többiek elismerését és háláját kivívta. Ez is releváns adatokkal szolgálhat a portfólióhoz.

7. A gyerek saját értékelése, reflexiója arról, hogyan értékeli, amit elért, fontos eleme a portfóliónak.

8. A tanárok adhatnak kompetenciatanúsítványokat. Ezek rövid, specifikus visszajelzések, amelyek mutatják, ha valamit a gyerek megcsinált, valamiben fejlődött.

A mentorok segítenek a gyerekeknek a tanulás módját, folyamatát és
eredményeit bemutatni portfólióban.

![A portfólió segíti, hogy a szülők is jobban tudják követni a gyerekük tanulását, fejlődését.](../pics/4b_portfolio_flora.jpg)

#### Formai követelmények

A portfóliónak rendezettnek, hozzáférhetőnek, elérhetőnek,
visszakereshetőnek és könnyen bővíthetőnek kell lennie. A tanulóközösségeknek olyan
(technológiai) megoldást kell választaniuk, aminek
alapján a gyerek, tanár és a szülő _naponta_ tudja a portfóliót
bővíteni, és akár _heti rendszerességgel_ át tudják tekinteni
időrendben, modulonként vagy tantárgyanként a portfólió bővülését.
