# Tanulási-tanítási egységek, a modulok

A _tanulási-tanítási egységek_, a Budapest School Modell szóhasználatában
a _modulok_ a tanulásszervezés _alapegységei_: olyan foglalkozások
megtervezett sorozata, amelyek során egy meghatározott időn belül a
gyerekek valamely képességüket fejlesztik, valamilyen ismeretet
elsajátítanak, vagy valamilyen produktumot létrehoznak. A tanítási
egységek célja sokféle lehet, de kötelező elvárás, hogy a résztvevők a
portfóliójukba való bejegyzésre érdemes eredményt hozzanak létre, és hogy
legyen egyértelmű céljuk.

::: tip Modul = építő kocka

A modulok tehát önálló céllal, fókusszal, kerettel bíró foglalkozásegységek. Akár szakköröknek, workshopoknak, mikrotantárgyaknak, projekteknek vagy foglalkozásoknak is hívhatnánk őket. A BPS modell a legsemlegesebb szót akarja az használni, hogy a tanárok és a gyerekek tudják feltölteni tartalommal a modulokat.

:::

### A modulok a tanmenet egységei

A modulok fogalmat a NAT és az Nkt. is ismeri. Ezekben a jogszabályokban tantárgyi témaköri egységet jelentenek, bár maga a NAT nem definiálja a fogalmat. A BPS modellben a modul csak annyit jelent: összefüggő foglalkozások egysége, azaz a tanulási-tanítási egységet és a modul szavakat szinonimaként kezeli. A diszciplináris tanítás egysége a diszciplináris modul, a multidiszciplináris tanítás egysége a multidiszciplináris modul.

A BPS modellben kétféle tervezéstípust ötvözünk:

1. a hosszú távú célokból kiinduló, majd azt a szakaszosan a mindennapokra lebontó felülről lefelé tervezést ötvözzük
2. az elsődlegesen a gyerekek élményére fókuszáló és abból építkező lentről felfelé való tervezési megközelítéssel.

Az előbbi megközelítésben a tantervi célokból alakulnak a tantárgyak, azokat tematikára, tematikus egységekre bontjuk. Ebből alakulnak ki tanítási egységek, majd foglakozásokra bontva alakul ki a gyerekek élményének megtervezése. Ezt hívhatjuk felülről-lefele (angolul top-down) tervezésnek. A lentről-felfelé (bottom-up) tervezéssel pedig, a kívánt gyerek-élménytől indulunk ki, és a tanulási-tanítási egységekből építjük újra fel a tantárgyak tartalmát, illetve az elvárt tanulási eredményeket.

Például _A koronavírus terjedése_ című foglalkozás elvezethet oda, hogy a gyerek _mértani sorozatokra vonatkozó ismereteit használja gazdasági, pénzügyi, természettudományi és társadalomtudományi problémák megoldásában._ Fentről-lefelé tervezés ehhez a foglalkozáshoz úgy jutott, hogy az 9-12. évfolyam Matematika tantárgy céljaiból levezette, hogy foglalkozni kell a mértani sorozatokkal társadalmi változások elemzéséhez. A tanár úgy gondolta, hogy nem a kamatos kamattal foglalkozik, hanem a gyerekek számára éppen érdekesebb koronavírussal, azaz cél eléréséhez a gyerekek érdeklődése mentén talált utat.

A lentről felfelé tervezés pedig a gyerekek érdeklődéséből indul ki, ami jelen esetben a koronavírus. És esetleg később veszi észre a tanár és a gyerek, hogy már az 9-12. Matematika elvárásainak egy részét is teljesítették. Hisz ez előre nem volt tervezve, mert cél nem a tantárgyi követelmények teljesítése volt, hanem a téma feldolgozása.

A két tervezési módot ötvözi a BPS modell. Az agilis tervezés eredményeképp feltehetőleg az alulról felfelé tervezés indul meg: itt és most mire van igényük a gyerekeknek. Idővel, például az év vége fele, vagy az érettségi vizsgához közeledve egyre erősebb lesz a fentről lefelé tervezés, és ez így van helyén. Ott és akkor az a kérdés merül fel, hogy mire van még szükség, hogy azokat a célokat is elérjük. De az is előfordulhat, hogy egy közösség inkább fentről lefelé tervezne: biztos, ami biztos, érjük el a tantárgyi célokat, és az úton legyünk kreatívak, vagy épp akkor, amikor már a szükséges eredményeket biztos elértük.

_A fentről lefelé, vagy lentről felfelé tervezés során létrejövő tanulási-tanítási egységeket, a tanmeneteket építő egységeket hívja a BPS modell moduloknak._

## Foglalkozások, modulok - szóhasználat, definíciók

Bár a BPS modell a modulok szintjén írja le a tanulás-tanítás élményét, a tanulási élmény leginkább meghatározó elemei mégis a _foglalkozások_. Az a dedikált idő, amit a (szak)tanár és a gyerek azért alakítanak ki, hogy a gyerek tanuljon. A modul nem más, mint egybefüggő foglalkozások sorozata. Ezért a BPS modellben, ahol a modul szót használunk, ott a foglalkozásokok szót is használhatnák.

A BPS modell nem a tanóra szót használja, hogy kihangsúlyozza: egy foglalkozás célja nem csak egy adott tantárgy következő témakörének feldolgozása lehet. Foglalkozás többek közt lehet

1. tantárgycsoportok összevont foglalkozása;
2. témanapok, témahetek eseményei;
3. projektekben való dolgozás;
4. önálló tanulás, gyakorlás;
5. közös önálló tanulás, tanulószobai tevékenység;
6. vagy épp egy tantárgy témakörének feldolgozása.

A foglalkozások tanügyigazgatási értelemben megegyeznek a tanórákkal. A foglalkozások hossza különböző lehet a törvények által megadott keretek között. A foglalkozások tanórának számolhatóak el, ha tantárgyi tartalom elsajátítása céljuk. Ha több tantárgy tartalmát érinti a foglalkozás, akkor több tantárgy tanórájának számolandó el.

### Modulok - a tantárgyak között szabadon mozoghatunk

A Budapest School Modell hangsúlyozza a tanulási-tanítási egység, a
modulok szerepét, ezért nevezi a tanmenetet _moduláris tanmenetnek_. Ugyanazt az eredményt éri el egy tanár, aki egy tantárgy
tanóráinak egyes egységeire külön foglalkozásokat tervez (és akár külső előadókat, tanárokat is meghív besegíteni), mint az a tanár, aki tanulási-tanítási
egységeket, modulokat tervez, és ezekből építi fel a tanévet.

Egy tanulási-tanítási egység több tantárgy tananyagtartalmát is
lefedheti.

A mindennapi tanulás a tanulás-tanítási egységek, a modulok elvégzésével
történik, ezzel biztosítva, hogy rugalmas keretek között,
pontosan megfogalmazott célok mentén, a gyerekek számára érthető,
átlátható és sajátnak megélt tartalommal történjen.

A tanulási-tanítási egységeket, vagyis a tanulás tartalmának és
formájának alapegységét a tanulásszervezők három kötelező összetevőből
állítják össze:

1. a NAT tantárgyainak tartalmából,

2. a gyerekek, tanárok érdeklődéséből, aktuális tudásából,

3. és a környezetük és a világ aktuális kihívásaiból.

A három komponensből a legelső a legstatikusabb, hiszen a NAT meghatározza a tantárgyakat és azok
tartalmát, valamint azt, hogy milyen lehetséges eredmények elérését
várjuk az ezekben való fejlődéstől. Az egyes tanítási egységekban ezek
személyre, illetve a csoport igényeire szabhatóak, hiszen az elérhető
eredményeket különféle gyakorlati és elméleti tanulási módszerekkel el
lehet érni.

A gyerekek és tanárok érdeklődése — ami a sajátként megélt cél és a
minél nagyobb fokú bevonódás alapfeltétele — alakítja ki a tanítási
egységek témáját, a projekteket, és a gyerekek egyéni tanulási idejét is
meghatározhatja.

Mindemellett az iskola szándéka, hogy a tanárok, gyerekek reagáljanak a
környezetükre, a világ aktuális kihívásaira, kérdéseire. A NAT
meghatározza például, hogy a gyerek _„Problémákat old meg táblázatkezelő program segítségével."_. Az azonban, hogy a gyerekek milyen táblázatokat szerkesztenek
szívesen, csak a tanítási egységek összeállításakor és a tanítási
egységek elvégzése során derül ki. Nagyon hasonló táblázatkezelési
képességeket lehet fejleszteni, ha valaki az önvezető autóktól várt
csökkenő számú balesetekről , vagy ha az egy főre jutó károsanyag-kibocsátás és a
GDP-növekedés alakulásának arányáról készít táblázatot.

A tanulási-tanítási egységekben épülő moduláris tanmenet fő célja, hogy
egyszerre képes legyen alkalmazkodni a menet közben felmerülő tanulási
igényekhez, adjon átlátható struktúrát a tanulásnak, és hogy a
tanulóközösség minél rugalmasabban tudja támogatni a tanulást úgy, hogy a
saját, a közösségi és a társadalmi célok harmóniába kerülhessenek.

Ez is mutatja, hogy bár közösek a kereteink, végtelen az elképzelhető
tanítási egységek (a tanulási utak építőkövei, és így a különböző
tanulási utak) száma. Ezért tartja fontosabbnak a Budapest School Modell
annak meghatározását, hogy hogyan kell a tanítási egységeket, a
modulokat létrehozni, mint azt, hogy a tanítási egységeket, a modulokat
tételesen felsorolja.

### Modulok sokfélék lehetnek

Egy-egy tanítási-tanulási egység, azaz egy modul során a gyerekek képesek

- produktum létrehozására szerveződő projektben részt venni;
- felfedezni, feltalálni, kutatni, vizsgálni, azaz kérdésekre választ keresni;
- egy jelenséget több nézőpontból megismerni;
- valamely képességüket, készségüket fejleszteni;
- adott vizsgára gyakorló feladatokkal felkészülni;
- közösségi programokban részt venni;
- az önismeretükkel, a tudatosságukkal, a testi-lelki jóllétükkel foglalkozni.

### Tantárgyi és multidiszciplináris modulok

Egyes modulok, a _tantárgyi modulok_ célja egy tantárgy ismereteinek elsajátítása, vagy egy-egy tantárgyi vizsgára, pl. érettségi vizsgára való felkészülés. A tantárgyi modulokat tantárgyi szaktanárok tartják.

A _multidiszciplináris modulok_ esetén több tantárgy ismereteinek integrálását igénylő (multidiszciplináris) téma kerül a középpontba. A multidiszciplináris modulok tervezéséhez erős szempontot adnak a [kiemelt fejlesztési területek](/tanulas-megkozelitese/kiemelt-fejlesztesi-teruletek.md).

## Órabontások, csoportbontások, osztályok

Egy foglalkozás megszervezhető különböző évfolyamok, különböző osztályok tanulóiból álló csoportok részére is, ahogy azt a 20/2020. EMMI rendelet 13 § (1) is kimondja. Ebben az esetben a foglalkozások differenciált és kooperatív tanulásszervezést igényelnek.

Ebből következik, hogy egy modul, azaz a foglalkozások sorozata is megszervezhető különböző évfolyamok, különböző osztályok tanulóiból álló csoportok részére is.

## A modulok meghirdetése

A tanulási-tanítási egységek kiválasztása, felkínálása a
tanulásszervezők feladata, hiszen ők figyelnek és reagálnak a gyerekek,
szülők céljaira és igényeire. A meghirdetett tanítási egységekből áll
össze a tanulás trimeszterenkénti tanulási rendje.

A tanulásszervezők az egyes tanulási-tanítási egységek tematikáját,
hosszát és feladatát a gyerekek tanulási céljainak megismerését követően
és NAT-ban meghatározott tantárgyi tanulási eredményeket
figyelembe véve határozzák meg.

A tanulási-tanítási egységekbe, modulokba való
csatlakozásról a mentor, a szülő és a gyerek közösen dönt, mindig szem
előtt tartva, hogy folyamatos előrelépés legyen a már elért egyéni és
tantárgyi eredményekben is. Egy modul megkezdésének lehet feltétele egy
korábbi modul elvégzése, a gyerek képességszintje, a jelentkezők száma,
és lehet egyedüli feltétele a gyerekek érdeklődése.

Egy szaktanár különféle tematikájú tanítási egységeket tarthat
attól függően, hogy a saját célok, a tantárgyi eredmények mit kívánnak, és hogy a
tanulásszervezők, valamint a szaktanárok kapacitása mit enged meg.

Amikor egy gyerek moduljai befejeződnek, és újat vesz fel, a
tanulásszervező feladata a gyereket segíteni abban, hogy az érdeklődési
körének, tanulási céljainak, és a soron következő, még el nem ért
tantárgyi eredményekben való fejlődéshez megfelelő tanítási egységek
közül választhasson.

A tanulásszervezők feladata a tantárgyi eredményelvárások nyomon
követése is. A tanítási egységek kidolgozásához és azok megtartásához
külsős szakembereket is meghívhatnak, azonban ilyenkor is a
tanulásszervezők felelnek azért, hogy a tanítási egységekkal elérni
kívánt tanulási célok teljesüljenek.

## A modulok egységek formátuma

A moduláris rendszer elég nagy szabadságot ad a tanároknak abban, hogy
hogyan szervezik a mindennapokat. Ezért is fontos, hogy már a modul
meghirdetése előtt néhány szempont szerint kialakítsák a tanítási
egységek kereteit.

#### Célok

Minden tanulási-tanítási egységnek, modulnak előre meg kell határozni a
célját. A tanulási szerződések célkitűzéseihez hasonlóan itt is minél
specifikusabban és mérhetőbben kell megfogalmazni a célokat. Javasolt az
OKR [(Doerr, 2018)](https://books.google.hu/books/about/Measure_What_Matters.html?id=XIU5DwAAQBAJ&redir_esc=y)
vagy a SMART [(Doran, 1981)](https://en.wikipedia.org/wiki/SMART_criteria)
technika alkalmazása, hogy minél specifikusabb,
teljesíthetőbb, tervezhetőbb és könnyen mérhető célokat tűzzenek ki.

#### Értékelés

A tanulási-tanítási egység végén minden résztvevő személyes, több
szempont alapján készült értékelést, visszajelzést kap a
tanulási-tanítási egységgel kapcsolatos tevékenységére és elért
eredményeire. Ez a [modulzáró visszajelzés](/tanulasi-elmeny/visszajelzes-ertekeles.md#tobbszintu-visszajelzes). A visszajelzés struktúráját előre meg kell határozni és
még a modul kezdete előtt meg kell osztani a résztvevőkkel. A modulvezető tanár a modulzáró visszajelzéseket a modul vége után maxmimum két héten belül elkészíti.

Természetesen a visszajelzés szempontjai változhatnak a tanulási-tanítási egység során, ha változik a tanulási-tanítási
egység tartalma, szempontjai. Ebben az esetben ezt mindenki számára
nyilvánvalóvá kell tenni.

#### Időtartam

Egy-egy tanulási-tanítási egység hossza és a tanulási-tanítási egységhez
kapcsolódó foglalkozások száma és gyakorisága változó: egy alkalomtól
legfeljebb egy teljes trimeszteren keresztül tarthat. A
tanulási-tanítási egység végén a tanulásszervező és a gyerek(ek) a
modult lezárják, értékelik és az elért eredményeket rögzítik a
(tanulási) portfólióban. Egy tanulási-tanítási egység folytatásaként a
következő trimeszterben új tanulási-tanítási egységet alakítanak ki a
tanárok.

#### Módszertan, formátum

A tanulási-tanítási egységek nemcsak témájukban, céljaikban,
időtartamukban, hanem módszertanukban, folyamataikban is különbözhetnek:
bizonyos tanulási-tanítási egységekben a felfedeztető (inquiry based)
módszer, másokban az ismétlő (repetitív) gyakorlás a célravezető. Így
mindig a tanulási-tanítási egység céljához, a tanárok és a gyerekek
képességeihez és igényeihez választható a legjobb módszer.
Tanulási-tanítási egységenként változhat, hogy a folyamatot a gyerekek
vagy a tanárok befolyásolják-e, és milyen mértékben. Két példa az
eltérésre:

1. Egy digitális kézműves tanulási-tanítási egység célja, hogy építsünk valamit, ami programozható. Annak kitalálása, hogy mit és hogyan építünk, a gyerekek feladata. Itt a tanár csak támogatja a tanulás folyamatát, azaz _facilitál_.

2. Egy „_A vizuális kommunikáció fejlődése a XX. század második felében_" tanulási-tanítási egység esetén a tanár előre felépíti a tanmenetet, például hogy mely alkotók munkásságát, alkotásait fogja bemutatni, és ezeket a gyerekekkel sorban végigveszi. Ilyenkor is bővülhet azonban a tematika a gyerekek érdeklődése, felvetései mentén.

## A tanulási-tanítási egységek helyszíne

A tanulás az egyes tanulóközösségek helyszínén, egy másik Budapest School-épületben, a tanár által kiválasztott külső helyszínen, vagy akár
online, virtuális térben történik. A tanulásra úgy tekintünk, mint az
élethez szorosan kapcsolódó holisztikus fejlődési igényre, melynek
jegyében az elsődleges szocializációs térről és formáról, a szülői,
családi környezetről sem akarjuk a tanulást leválasztani. Az élethosszig
tartó tanulás jegyében a tanulás tere az iskolai időszak után és az
iskola terein kívül is folytatódik.

A gyerekek több ok miatt is tanulnak az iskolán kívül:

1. tanulási-tanítási egységek foglalkozásai szervezhetők külső helyszínekre, úgymint múzeumokba, erdei iskolákba, parkokba, vállalatokhoz, vagy tölthetik az idejüket „kint a társadalomban".

2. Amennyiben ez saját céljuk elérését nem veszélyezteti, és a folyamatos fejlődés biztosított, a mentoruk tudomásával a gyerekek az önirányított tanulás elvére figyelemmel a telephelyen kívüli egyéb helyszínen is elvégezhetnek egy-egy tanulási-tanítási egységet.

A tanulási-tanítási egység lezárásaként a gyerekek és tanárok
visszajelzést adnak egymásnak, aminek része, hogy megosztják saját
élményeiket, reflektálnak a közös időre, összegyűjtik és értékelik az
elért eredményt, és kitérnek az esetleges fejlődési lehetőségekre.

## Külsős tanárok aránya

A pedagógiai program egy fontos megkötést ad a tanulási-tanítási
egységek megtartására: a tanulóközösség tanulásszervezőinek kell vezetniük a
gyerekek moduljainak nagy részét. Másképp fogalmazva: korlátozva van a
külsős, nem tanulásszervezők által tartott modulok óraszáma, ahogy ezt
a lenti táblázat mutatja. Ennek a megkötésnek az az oka, hogy

- Kisebb korban szeretnénk, ha kevesebb, állandóan jelenlévő felnőtt vezetné a gyerekek tanulását (a kéttanítós rendszer mintájára).

- Biztosított legyen, hogy a gyerekek a tanulóközösség tanulásszervezőiel elegendő időt töltsenek.

- Érettségihez közeledve legyen lehetőség minél több külsős, akár speciális szaktudással bíró embertől tanulni.

| Évfolyamszint               | 1-2 | 3-4 | 5-8 | 9-11 | 11-12 |
| --------------------------- | --- | --- | --- | ---- | ----- |
| „Belsős" modulok min.aránya | 70% | 60% | 55% | 50%  | 40%   |

## Modulok nyomonkövetése

Minden trimeszter megkezdésekor a tanulásszervező tanárok meghirdetik a
kötelező, a kötelezően választható és a választható modulokat, azaz
rögzítik, hogy

- ki a modul vezetője, és melyik pedagógus munkakörben alkalmazott tanulásszervező felelős (elszámoltatható a [RACI](https://www.pmsz.hu/hirek-aktualitasok/havi-mustra/havi-mustra-a-felelosseg-hozzarendelesi-matrixrol) menedzsment

  rendszer értelmezésében) a modulért;

- mi a modul célja, keretei és várható eredményei;
- hol, mikor és milyen rendszerességű foglalkozások lesznek;
- és mi a részvétel feltétele (előzetes tudás, kor, maximum létszám), azon belül, hogy teljes folyamatra kell-e

  elköteleződni,vagy esetileg is lehet a modult látogatni.

Ezután eldől, hogy ki mikor melyik modulon vesz részt. Ennek rendszerét
a tanárok alakítják ki: beoszthatják a gyerekeket, ahogy ők ezt jónak
látják, vagy épp hagyatkozhatnak a gyerekek választására. A lényeg, hogy
alakuljon ki a rendszer a trimeszter megkezdése előtt.

#### Nyomonkövethetőség

Az őszi első trimeszterben a tanulóközösség
ismerkedéssel kezd. Ebben a trimeszterben október 1. a modulrendszer
felállításának határideje. A második trimeszter esetén január 1.,
tavasszal pedig április 1. a határidő. Az ezektől a határidőktől kezdve kialakuló
rendszerben mindennap lehet tudni, hogy ki, hol, kivel, melyik modulok
keretében tanul. Azaz kialakul a gyerekek órarendje, ami nagyon hasonló
a megszokott órarendekhez: _mikor melyik foglalkozáson és hol vagyok_.

A Budapest School iskolában annyi a különbség, hogy a
személyreszabhatóság miatt az egy osztályba, tanulóközösségbe járó gyerekek órarendje
akár nagy mértékben is eltérhet egymástól. Tehát itt nem egy osztálynak
vagy tanulóközösségnek van órarendje, hanem a gyerekeknek van saját
órarendjük. A fenntartó felelőssége kialakítani azt a számítógépes
rendszert, ami a gyerekek órarendjét a gyerekek, szülők, tanárok,
tanulóközösségek és a teljes Budapest School iskola szintjén átláthatóvá és
nyomonkövethető teszi.

## Inkrementális fejlesztés

A moduláris rendszer nagy szabadságot enged a tanároknak abban, hogy a
mindennapokat olyan tevékenység köré szervezzék, ami szerintük a
gyerekek tanulását a legjobban szolgálja. Ez a szabadság
bizonytalanságot is adhat: ha bármilyen modult szervezhetünk, akkor
milyen modult szervezzünk? A BPS modell a tanároknak azt
javasolja, hogy induljanak ki egy számukra ismert, stabil rendszerből,
és azt fejlesszék lépésről lépésre.

Sokaknak a legbiztosabb alap a jól ismert rendszer: a NAT tantárgyaiból létrehozott modulok, amik követik a
kiadott kerettantervek tematikus egységeit. A moduláris rendszer
ezt is lehetővé teszi. Van, amikor innen
indulva, lassan, trimeszterenként változtatva érhetjük el a legjobb
eredményt: ezután össze lehet vonni tantárgyakat és egy modulba
szervezni például a magyar nyelv és irodalmat és a történelmet egy
trimeszterre, vagy az összes természettudományi tantárgyat egy
kísérletezős modulba. Lehet tömbösítve vagy epochálisan szervezni a
mindennapi tanulást.

A modulrendszer le tudja fedni a szakkörök, iskola utáni foglalkozások,
nyári táborok rendszerét is. Egy tanulóközösség a szokásos tantárgyi
modulok mellé meghirdethet más iskolákban szakkörnek nevezett modulokat:
robotika, néptánc, fociedzés. A modulrendszer sajátja, hogy ezeket a
szakköröket ugyanúgy tudja kezelni, mint a történelem érettségire
felkészítő fakultációt.

A modulrendszer lehetővé teszi a projektpedagógiával való szabad
kísérletezést is. Lehet olyan modulrendszert alkotni, ahol minden páros
héten projekteken dolgoznak a gyerekek, a páratlan héteken pedig
klasszikus tantárgyi struktúrák mentén szervezett modulokban haladnak az
akadémiai tudás elsajátításával.

## Biztonságos felfedezés

A tanárokat bátorítjuk egy olyan saját, rugalmas struktúra
kialakítására, amely jól működik és biztonságot nyújt mind számukra,
mind pedig a gyerekek és a szülők számára. A Budapest School
tanulásmonitoring rendszere miatt mindig tudjuk, hogy egy gyerek egy-egy
tantárgy tanulási eredményeivel hogyan haladt. Ez biztonsági hálót ad
a tanároknak: mindig tudjuk, hogy a gyerekek milyen irányban haladnak,
lemaradtak-e valamiből, előre szaladtak-e valami másból. Egyben
folyamatos visszajelzést ad a modulstruktúráról. Ezért mondhatjuk, hogy
nyugodtan kereshetjük a jobb struktúrát, a tökéleteset sose tudjuk
elérni („better, never the best").

## Kiemelt modultípusok

### Élménynapok

Egy különleges modultípust, az élménynapokat a BPS modell külön is kiemel.
Ezek azok a napok, amikor a gyerekek előre
tervezetten és rendszeresen a tanárokkal együtt _kimennek az iskolából_.
Ezt a napot nevezzük _élménynapnak_. Az, hogy a hét melyik napján és
hogy hány hetente szerveznek a tanulásszervezők élménynapot, az ő
döntésük. Javasolt a pénteket választani, de telephelyenként eltérhet, hogy mikor szervezik az élménynapot.

Ezeken a napokon nagyon változatos programok szervezhetők,
a külső moduloktól, a nem formális (tanórán és iskolán kívül szervezett)
és az informális (nem szervezett, spontán tevékenység során megvalósuló)
tanulási formákig minden. Az az egyetlen egy megkötés, hogy legalább egy
héttel előtte értesíteni kell a szülőket és a fenntartót a tervezett
programról. Hiszen azt tudni és dokumentálni kell, hogy hol
vannak a gyerekek. A terv nem azt jelenti, hogy mindig nagyon struktúrált foglalkozást kell szervezni. Az is terv, hogy csak kint vagyunk a Duna-parton és ott játszunk együtt.

Átszervezhető-e egy élménynap másik napra? (Mert például egy múzeum csak
egy másik nap van nyitva?) Igen, ha erről legalább egy héttel előtte
minden érintett tudomást szerez. Lehet-e egy héten két napon is a külső
tanulás? Igen, ha ez a gyerekek tanulását és fejlődését segíti, és a
tanulásszervezők biztonságban meg tudják szervezni ezeket a napokat úgy,
hogy a meghirdetett és folyamatban lévő modulok vezetői hozzájárulásukat
adják.

### Távtanuló modulok

Amikor már kialakul a gyerekekben az önálló olvasás, információ-feldolgozás képessége, akkor képesek a távtanulásra (e-learning), azaz, hogy online tananyagokat önirányítottan, önállóan feldolgozzanak. Ettől a kortól kezdve (körülbelül 5. évfolyam) a tanulásszervezők meghirdethetnek online modulokat. Az első időszakban ajánlatos valamiféle kevert élményt adni a gyerekeknek: önállóan egy online felületen tanulnak, de együtt vannak, tanári felügyelet mellett az iskolában. Később, természetesen nincs szükség arra, hogy egy térben és időben legyenek a gyerekek a tanárral. Ilyenkor a modulnak lehet, hogy csak egy kis része kerül a napirendbe, amikor együtt átbeszélik az élményeket a gyerekek. A [3. és 4. tanulási szakaszban](/iskola-celja/emberkep.md#harmadik-szakasz-12—16-ev) lévő gyerekeknek már meghirdethető teljesen online modul is.

> **Ki van távol? A gyerek vagy a tanár?** Az iskola egész napos iskola, és így 9 és 16 óra között a gyerekek csak külön megállapodás vagy hiányzás esetén vannak távol az iskolától. A távtanulásnak ilyenkor is van értelme: különböző gyerekek különböző témákról tanulhatnak például a [Nemzeti Köznevelési Portál](https://portal.nkp.hu/) segítségével, amíg csak egy általános, felügyeletet ellátó tanár van jelen a teremben. Ilyenkor érdekes módon a (szak)tanárnak nem kell az iskolában lennie.
