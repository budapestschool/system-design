# Mentorálás

Minden gyereknek van egy _mentora_, aki a saját céljainak
megfogalmazásában és a fejlődése követésében segíti. A mentor segít az általa mentorált gyereknek,
hogy a tantárgyi fejlesztési célok és a saját magának megfogalmazott
saját célok között megtalálja az egyensúlyt, és segít megalkotni a
gyerek _saját tanulási tervét_.

Minden gyerek rendszeresen, legalább kéthetente, találkozik a mentorával, hogy átbeszéljék hol is tart a gyerek a céljai elérésében, milyen lehetőségei, döntései és tervei vannak. A mentor feladata segíteni a gyereket felismerni céljait, lehetőségeit és megtalálni a saját válaszait. A kapcsolat lényege, hogy intim, közvetlen, őszinte, gyerekfókuszú és rendszeres.

## Mentoridő

A gyerekeknek van rendszeres, minimum kéthetente egy órás _mentorbeszélgetésük_, amikor a mentor a gyerek számára egyéni, minőségi figyelmet ad. A mentor ilyenkor a gyereket segíti a saját céljainak
megfogalmazásában és hogy legyen lehetősége reflektálni a saját fejlődésére. Ez az idő a gyerekek és a mentorok számára kötelező foglalkozás.

### Mentoridő struktúrája

A mentoridő, struktúrált, a mentor által vezetett beszélgetés. Ezekhez sorvezetőként a következő kérdéseket javasolja a BPS-model:

- Mi _van_ most? Hogy van a mentorált, mi foglalkoztatja mostanában. Mi az, amiről mindenképpen kell ma beszélni?
- Mi _volt_ az elmúlt időszakban? Mivel haladt, mit tanult, miyen problémái voltak a mentoráltnak? Mi ment jól, mi volt nehézség? Milyen tanulságok vannak?
- Mi _lehetne_ az új időszakra a cél, miket lehetne megcsinálni, kipróbálni, változtatni, folytatni?
- Mi _lesz_ ebből a terv? Mi az a 2-3 dolog, amit ezért biztosan meg fogsz tenni? Milyen érzések vannak benned ezzel kapcsolatban? Kire/mire számíthatsz ebben? Milyen nehézségekre számítasz? Mik azok az erőforrásaid amik már most rendelkezésedre állnak ahhoz, hogy ezeken a nehézségeken átlendülj? Hogyan tudok neked én segíteni?

Érdemes feljegyezni, hogy milyen kérdések merültek fel, és a mentorált milyen válaszokat adott magának.

## GROW modell

Minden mentornak meg kell találnia azokat a kérdéseket, amik ott és akkor segítik a mentorált fejlődését. Alapjában azt javasoljuk, hogy minden mentor a GROW modellt szabja magára. A GROW modell egy nagyon egyszerű és nagyszerű coaching modell. A szó maga azt jelenti: „növekedés”, a betűk feloldása pedig a következő:

- *G*oal: CÉL
- current *R*eality: JELENLEGI HELYZET
- *O*ptions, *o*bstacles: LEHETŐSÉGEK, AKADÁLYOK
- *w*ill, *w*ay forward: SZÁNDÉK, ÚT ELŐRE

A GROW coaching modell használatát a szakirodalom egy utazás megtervezéséhez hasonlítja. Ahhoz, hogy egyáltalán elérj valahová, tudnod kell, hová akarsz menni (cél), és azt is, hogy hol vagy jelenleg (jelenlegi helyzet). Aztán az úticél eléréséhez ismerned kell, hogy hogyan juthatsz oda, mik lehetnek az akadályok, végül pedig el kell határoznod magad, hogy el is indulsz és legyőzöd ezeket.

G: Célkitűzés. A mentor segít a gyereknek abban, hogy reális célt tűzzön ki magának. A célkitűzésben segíthet az, ha a cél SMART cél, tehát legyen elérhető, reális, időkerethez kötött, mérhető és konkrét.

R: Jelenlegi helyzet meghatározása. Kritikus pont, hiszen sokszor a gyerek úgy akar célt kitűzni, hogy nem gondolja át alaposan jelenlegi helyzetét és azt, így nem rendelkezik a megfelelő mennyiségű információval ahhoz, hogy reálisan lássa lehetőségeit.

O: Ha sikerül az előző lépésben ismertetett helyzetmeghatározás, a mentor és a gyerek ötletbörzét tarthat a lehetőségekről. A lényeg megtalálni a lehető legtöbb választási lehetőséget, hogy aztán könnyebb legyen dönteni hogy merre induljon a gyerek. Fontos, hogy a mentor segítse kliensét elszakadni gondolati korlátaitól, segítsen neki “messzebbre látni”. Fontos felmérni az akadályokat is.

W: Szándék, elhatározás. A mentor ebben a szakaszban segít a kliensnek biztosítani azt, hogy valóban el is induljon a célja felé. A munka könnyebik része volt a terep felmérése és a lehetőségeket áttekintése.
Most jön az oroszlánrésze, amikor a kliensnek tennie is kell, méghozzá nap mint nap azért, hogy el is érje a célját. A mentor feladata a támogatás, motiválás, inspiráció és a számonkérés is.

## Mentor vagy coach

A BPS-mentor néha coach-a, néha mentora a gyereknek. A szakirodalmi különbség a két fogalom között az, hogy a coach nem ismeri a kliensének szakterületét, nem ad tanácsot, nem ért jobban a kliensénél a területhez, ezért tényleg csak abban tud segíteni, hogy a kliens megtalálja a saját válaszait. Ezzel szemben a mentor az, aki „csinálta már" (have done it), tapasztalai alapján tanácsokkal tudja ellátni a mentoráltat. A BPS-mentor ebben az értelemben egy tapasztalt, integráns felnőtt, aki tud segíteni a gyereknek érettebb, önálóbb, boldogabb felnőtté vállni. Szóval néha tanácsokat ad. A BPS-mentor ugyanakkor tud coach is lenni. Nem kell ahhoz nagyon érteni az emeltszíntű biológiához, hogy segíteni tudjunk egy gyereket abban, hogy ha a céljai úgy kívánják, akkor hetente kétszer üljön le tanulni.
