# Saját tanulási célok, tanulási szerződés és a szerződő beszélgetések

Minden gyerek megfogalmazza és trimeszterenként újrafogalmazza a _saját
tanulási céljait_: eredményeket, amelyeket el akar érni, képességeket,
amelyeket fejleszteni akar, szokásokat, amelyeket ki akar alakítani. A
saját célok elfogadásakor a gyerek és a mentora a szülőkkel együtt
_tanulási szerződést_ köt.

Csak olyan célok kerülhetnek a saját célok közé, amelyek minden
érintettnek biztonságosak. Annyi megkötés van, hogy céloknak mindenképp ki kell terjedni a
tantárgyi tanulási eredményekre (_"hogy és mennyit akarok elérni a NAT-ból származó tanulási eredményekből"_) és a tantárgyi rendszeren kívüli célokra, feladatokra is.

Trimeszterenként a tanulásszervezők és a gyerekek megállnak, reflektálnak az
elmúlt időszakra, és a tapasztalatok, valamint az elért célok
ismeretében és az új célok figyelembevételével
újraszervezik a foglalkozások rendjét, tehát azt, hogy mikor és mit
csinálnak majd a gyerekek az iskolában. A mindennapi tevékenység során
tapasztalt élmények, alkotások, elvégzett feladatok, kitöltött vizsgák,
tehát mindaz, ami a gyerekekkel történik, bekerül a portfóliójukba. Még
az is, amit nem terveztek meg előre.

A gyerekeket a mentoruk segíti a saját célok kitűzésében, a különböző
választásoknál, a portfólióépítésben, a reflektálásban. A tanulási célok
kitűzése az önirányított tanulás fokozatos fejlődésével és az életkor
előrehaladtával folyamatosan egyre önállóbb tevékenységgé válik.
Tanulási útjukon, céljai kitűzésében a mentor kíséri végig a gyerekeket.

A Budapest School személyre szabott tanulásszervezésének
jellegzetessége, hogy a gyerekek a saját céljuk irányába haladnak, az
adott célhoz az adott kontextusban leghatékonyabb úton. Tehát mindenki
rendelkezik saját célokkal, még akkor is, ha egy közösség tagjainak
céljai a tantárgyi tanulási eredmények azonossága, vagy a hasonló
érdeklődés miatt akár 80% átfedést mutatnak.

A NAT műveltségi területeiben és a tantárgyakban megfogalmazott
követelmények teljesítése is célja a tanulásnak, a tanulás fő irányítója
azonban más. Mi azt kérdezzük a gyerekektől, hogy _ezen felül_ mi az ő
személyes céljuk.

## Hármas beszélgetés, szerződő beszélgetés

A tanulási szerződés egy megállapodás a tanárok, a gyerek és a szülők között, ami a trimeszter rendszerességgel megtörténő szerződő beszélgetés során születik meg. BPS-szülők, a mentorok és a BPS-gyerekek kötelezően résztvesznek ezen az online vagy jelenléti beszélgetésen. Természetesen előfordulhat, hogy egyik szülő nem tud részt venni, vagy kivételes körülmény miatt a szerződés aszinkron születek meg.

## Tanulási szerződés

Trimeszterenként a gyerek-mentor-szülő által kitűzött saját célokat, az ezekhez
kapcsolatos vállalásokat a tanulási szerződések tartalmazzák. A tanulási szerződések a gyerekek, az érintet tanárok és a szülők számára visszakereshetőek és elérhetőek, azokat a gyerek portfóliónak részének kell tekinteni.
