# Tankönyvek kiválasztása

A szaktanárok minden esetben maguk választják a modulhoz szükséges
tankönyveket, szoftvereket, weboldalakat és egyéb eszközöket úgy, hogy

- az megfelelő legyen annak a csoportnak, arra a célra, amit el akar érni;

- minden esetben legyen mindenki számára elérhető (az esetek többségében értsd ingyenes) megoldás;

- a szaktanárokat bátorítjuk arra, hogy új dolgokat próbáljanak ki, és tapasztalataikat az iskola többi tanárával osszák meg.

Mivel az iskolában a NAT-ban és a miniszter által közreadott kerettantervben meghatározott tanulási
eredményeket kell elérni, az ehhez szükséges ismeretek
megszerzéséhez a Budapest School az Oktatási Hivatal általi jegyzékben
államilag támogatott, OFI által fejlesztett tankönyveket veszi alapul. A
Budapest School tanárcsapatainak lehetőségük van arra, hogy ettől eltérő,
a mindenkori tankönyvjegyzékben szereplő tankönyvvel segítsék a
tanulási eredmények elérését. És arra is
lehetőségük van, hogy egyáltalán ne használjanak tankönyvet, mert sokszor
az internet elegendő információt tartalmaz.

A Budapest School programjának alapja, hogy a gyerekek egyéni
céljaira szabott tanulási terveket készít. Ennek előfeltétele, hogy a
könyvek használata is ehhez kapcsolódó módon, rugalmasan történjen,
minden esetben az adott tanulási modul igényeihez szabva. Ennek
érdekében a program pedagógusai folyamatosam állítják össze a gyerekek
eltérő céljaihoz és képességszintjeihez igazodó differenciált
tevékenységek és feladatsorok rendszerét.

A Budapest School-iskolában egy minden (modul)csoport csak olyan
tankönyvet választhat, amelyik minden család számára elérhető. Ha valamelyik család
nem tudja a könyvet magának megvásárolni, akkor a csoport többi tagja
megvásárolja neki.
