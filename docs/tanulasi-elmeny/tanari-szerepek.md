# Különböző tanári szerepek

A Budapest Schoolban a gyerekek azokat a felnőtteket tekintik
tanáruknak, akik minőségi időt töltenek velük, és segítik, támogatják
vagy vezetik őket a tanulásukban. Több szerepre bontjuk a tanár
fogalmát: a gyerek legjobban egy felnőtthöz kapcsolódik, a
_mentorához_, aki rá különösen figyel. A foglalkozásokon megjelenhetnek
további tanárok, a _szaktanárok_, akik egy adott foglalkozást,
szakkört, órát, kurzust vezetnek. Szervezetileg minden tanulóközösségnek van egy állandó _tanárcsapata_, a
_tanulásszervezők_, akik a mentorokból és a szaktanárokból állnak össze. A tanulóközösség mindennapjait a tanulásszervezők
határozzák meg.

A tanulásszervezők, mentorok legalább egy tanévre
elköteleződnek, szemben a szaktanárokkal, akik lehet, hogy csak egy pár
hetes projekt keretében vesznek részt a munkában. A tanulásszervezők
általában mentorok is, de nem minden esetben. Egy tanulásszervező lehet több tanulóközösségben
is ebben a szerepben, és így mentor is lehet több tanulóközösségben.

## Mentor

Minden gyereknek van egy _mentora_, aki a saját céljainak
megfogalmazásában és a fejlődése követésében segíti. Minden mentorhoz
több gyerek tartozik, de nem több, mint 12. A mentor együtt dolgozik a
tanulóközösség többi tanulásszervezőjével, a szülőkkel és az általa
mentorált gyerekekkel. A mentor segít az általa mentorált gyereknek,
hogy a tantárgyi fejlesztési célok és a saját magának megfogalmazott
saját célok között megtalálja az egyensúlyt, és segít megalkotni a
gyerek _saját tanulási tervét_.

A mentor a kapocs a Budapest School, a szülő és a gyerek között.

- Képviseli a Budapest Schoolt, a tanulóközösséget a szülő felé.

- Ismeri a Budapest Schoolt, a lehetőségeket, a tanulásszervezés
  folyamatait.

- Együtt tanul a többi Budapest School-mentorral, együtt dolgozik a
  tanártársaival.
- Ismeri, segíti, képviseli a gyereket.
- Az első trimeszter alatt a mentor megismeri mentoráltjának
  személyiségjegyeit, képességeit, érdeklődését, motivációit.
  Megismeri a mentorált családot.
- Trimeszterenként a mentorált gyerekkel és szülőkkel egyetértésben
  kialakítja, majd folyamatosan monitorozza a mentorált gyerek
  haladását.
- Tudja, hol és merre tart a mentoráltja, ismeri a képességeit,
  körülményeit, szándékait, vágyait.
- Minimum kéthetente találkozik mentoráltjával. Követi, tudja, hogy a
  gyerek hogy érzi magát az iskolában, a családban, a mindennapokban.
- Segít a saját célok elérésében, felügyeli a haladást.
- Megerősíti a mentoráltjai pszichológiai biztonságérzetét.
- Mentorként nyomon követi, monitorozza a mentoráltjai fejlődését, és
  szükség esetén továbblendíti, inspirálja őket. Visszajelzéseket ad a mentoráltjainak. A mentorált gyerekkel együtt rendszeresen reflektál
  a tanulási céljaikra és haladásukra.
- Segít abban, hogy az elért célok a portfólióba kerüljenek, biztosítja, hogy a mentorált gyerek portfóliója friss
  legyen.

- Összeveti a portfólió tartalmát a tantárgyak fejlesztési
  céljaival és jelzi, ha egy-egy tantárgyból hiány mutatkozik.
- Segíti a mentorált választásait.
- Együtt dolgozik, gondolkozik a szülőkkel, képviseli igényüket a
  közösség előtt.
- Megállapodik a családdal a kapcsolattartás szabályaiban.
- Bevezeti a családot a Budapest School rendszerébe.
- Erős partneri kapcsolatot épít ki a szülőkkel.
- Rendszeresen információt oszt meg.
- Elérhető.
- Asszertíven kommunikál.
- Konkrét, specifikus, mérhető megállapodásokat köt.
- Segít a gyerekekkel közös célokat állítani.
- Amikor a gyereknek külső fejlesztésre, mentorra, tanárra, trénerre
  van szüksége, akkor a családot segíti a megfelelő segítő
  felkeresésében, a külsőssel kapcsolatot tart, és konzultál a
  mentoráltja haladásáról.
- A szülő számára a mentor az elsődleges kapcsolattartó a
  különféle iskolai ügyekkel kapcsolatban.

A mentor egyszerre felelős a mentorált gyerek előrehaladásának
segítéséért, és közös felelőssége van a mentortársakkal, hogy az
iskolában a lehető legtöbbet tanuljanak a gyerekek. A mentor
folyamatosan figyelemmel követi az egyéni tanulási tervben
megfogalmazottakat, és ezzel kapcsolatos visszajelzést ad a mentoráltnak
és a szülőnek.

## Szaktanárok

A szaktanárok egy-egy foglalkozás, vagy foglalkozássorozat, modul, azaz tanulási egység megszervezéséért, lebonyolításáért felelnek. A mentort fő fókusza, hogy a gyerekek jól vannak-e. A szaktanárok fő fókusza, hogy mit tanulnak a gyerekek. Ők segítik a gyerekeket egy tanulási cél felé való
haladásban akár egyetlen alkalommal,
vagy éppen egy egész trimeszteren át tartó tanulási, alkotási folyamatban.

Ők általában az adott tudományos, művészeti, nyelvi vagy bármilyen más terület szakértői.

- Izgalmas, érdekes foglalkozásokat tartanak, amire felkészülnek, és amiben a
  gyerekeket flow-ban [(Csikszentmihalyi, 1991)](https://www.ted.com/talks/mihaly_csikszentmihalyi_on_flow?language=hu) tudják tartani.

- Amikor a gyerekek velük vannak, akkor folyamatosan dolgoznak, figyelnek, fókuszálnak,
  koncentrálnak, tanulnak.

- Kedvesen és határozottan vezetik a csoportot, figyelnek arra, hogy
  mindenkit bevonjanak.

- Változatos, gazdag módszertani eszköztárukból mindig a foglalkozáshoz
  megfelelő módszert tudják elővenni.

- A foglalkozásaik fejlesztik a gyerekek kritikai gondolkozását, kreativitását, kommunikációját, úgy általában a 21. századi kompetenciákat [(Trilling, 2009)](https://en.wikipedia.org/wiki/21st_century_skills).

## Tanulásszervező

Egy tanulóközösséget vezető tanárok
állandó tanári csapatát 1--12 tanulásszervező alkotja, akik egyedileg
meghatározott szerepek mentén a tanulóközösség mindennapjainak
működtetéséért felelnek. A
tanulásszervezők tarthatnak foglalkozásokat, sőt kívánatos is, hogy
dolgozzanak a gyerekekkel, ne csak szervezzék az életüket. Ők rendelik
meg a külső szaktanároktól a munkát, ilyen értelemben a tanulási utak
projektmenedzserei.

- Kiszámítható, átlátható rendszert építenek, ahol a szülők és a gyerekek
  is biztonságban, informálva, bevonva érzik magukat.

- A tanulási célokkal rezonáló foglalkozásokat, modulokat hirdetnek meg, szerveznek le.

- A szülők és a gyerekek is érzik, értik, hogyan „történik" a tanulás.
  Biztosítják számukra az átláthatóságot.
- A szaktanároknak megadnak minden szükséges információt, kontextust,
  hogy hatékonyan tudják végezni a munkájukat.

- Megadják a résztvevőknek az informált választás lehetőségét.

## Gyógypedagógiai felelős

A tanulóközösségekben a gyógypedadagógiai felelős szerepet betöltő tanár nyújt segítséget akkor, ha egy gyereknek gyógypedagógiai támogatásra van szüksége: a gyógypedagógiai szükséglet feltételezésétől, a diagnózis elérésén át a szükséges fejlesztés megszervezéséig. A gyógypedagiai felelős feladatai közé tartozik:
 - Megszervezi a mikroiskolában lévő gyerekeknek a szükséges gyógypedagógiai támogatást, akár külsős gyógypedagógus igénybevételével: pedagógiai véleményt készít (és/vagy igényel más tanároktól), a diagnózishoz szükséges hozzáférhető információkat begyűjti
 - A mentortanárokkal együttműködve segíti és utánköveti a gyerekek fejlődését gyógypedagógiai szempontból: megszervezi a szakértői vélemény szerinti javasolt fejlesztő foglalkozásokat. 
 - Kapcsolatot tart a Budapest School Általános Iskola és Gimnázium gyógypedagógiai koordinátorával. 
 - Összekötő szerepet tölt be a szülők és a fejlesztő szakemberek között. 


# Egyéb szerepek

## Iskolapszichológus

A Budapest School mikroiskoláiban iskolapszichológusok dolgoznak az itt tanuló gyerekek és családok, illetve az itt dolgozó tanárok mentális és pszichés nehézségek prevenciója és megsegítése érdekében.

Az iskolapszichológus mindig az adott iskola, az ott jelen lévő családok és tanárok igényei szerint dolgozik. Az adott mikroiskolában vannak fix feladatai,  azonban a munkája nagy része az éppen felmerülő kérdésekhez alkalmazkodik. 


Az iskolapszichológus feladatai a teljesség igénye nélkül:
 - Csoportos foglalkozások megtartása- témájuk alapján közösségfejlesztő, EQ-fejlesztő, önismereti, bullying, pszichoedukációs, szexedukációs, pályaorientációs, tanulásmódszertani, stb.
 - Pszichológiai tanácsadás nyújtása egyéni helyzetben a gyereknek/serdülőnek- akár a gyerek foglalkozásának időpontjában, a tanulásszervezőkkel előre egyeztetve. Az alkalmak során pszichoterápiás beavatkozás nem történik.
 - Szülőkonzultáció- az iskolába járó gyerekek szülei egyéni konzultáció keretében fordulhatnak az iskolapszichológushoz gyermekükkel kapcsolatos kérdéseikkel
 - Gyerekek, családok megfelelő szakemberhez való irányítása- az iskolapszichológus a gyerek felmérése és megismerése után beszéli meg a szülővel (14 év fölött a serdülő jelenlétében), hogy szükséges-e további szakembert felkeresni (egyéni terápia, csoportterápia, családterápia, mediáció, stb.)
 - Más szakemberekkel való egyeztetés (pl. iskolán belüli gyógypedagógus, külsős pszichológus)
 - Már továbbirányított esetek utánkövetése
 - Tematikus szülőcsoportok megtartása (szülői körök részeként, vagy önálló alkalmakként), vagy a tanulásszervezőkkel való közös ötletelés, felkészülés az általuk tartott alkalmak előtt
 - Hospitálás prevenciós és a gyerekek, csoportok megismerése céljából,
 - Gyerekek / gyerekcsoportok megfigyelése a tanulásszervezők kérésére,
 - Krízisintervenció biztosítása krízishelyzetben,
 - Tanárokkal való konzultáció- a tanárok egyénileg, vagy a tanárcsapattal közösen fordulhatnak az iskolapszichológushoz az iskolával kapcsolatos kérdéseikkel (pl. egy-egy gyerek viselkedésének megváltozásánál, a gyerekek közötti megoldatlan konfliktus, vagy bullying esetén, a gyerekcsoport dinamikájának megváltozásánál),
 - Shadow teacher-rel való rendszeres konzultáció.

A szülőkonzultációk és a gyerekekkel való egyéni foglalkozások célja annak a felmérése, hogy az iskolapszichológushoz forduló gyereknek vagy családnak van-e szüksége további segítségre és ha igen, milyen irányba tud elindulni. 18 éven aluli gyermek egyéni konzultációjához szükséges szülői beleegyező nyilatkozat.

A BPS-ben dolgozó iskolapszichológus vállalja, hogy  a pszichológusok Szakmai Etikai Kódexe szerint végzi munkáját.
