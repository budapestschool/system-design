# Tantárgyi óraszámok

| Tantárgy                          |   1 |   2 |   3 |   4 |   5 |   6 |   7 |   8 |   9 |  10 |  11 |  12 |
| --------------------------------- | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: | --: |
| Állampolgári ismeretek            |     |     |     |     |     |     |     |   1 |     |     |     |   1 |
| Biológia                          |     |     |     |     |     |     |     |     |   3 |   2 |     |     |
| Digitális kultúra                 |     |     |   1 |   1 |   1 |   1 |   1 |   1 |   2 |   1 |   2 |     |
| Dráma és színház                  |     |     |     |     |     |     |   1 |     |     |     |     |     |
| Első idegen nyelv                 |     |     |     |   2 |   3 |   3 |   3 |   3 |   3 |   3 |   4 |   4 |
| Ének-zene                         |   2 |   2 |   2 |   2 |   2 |   1 |   1 |   1 |   1 |   1 |     |     |
| Etika                             |   1 |   1 |   1 |   1 |   1 |   1 |   2 |   2 |     |     |     |     |
| Fizika                            |     |     |     |     |     |     |     |     |   2 |   3 |     |     |
| Földrajz                          |     |     |     |     |     |     |     |     |   2 |   1 |     |     |
| Hon- és népismeret                |     |     |     |     |     |   1 |     |     |     |     |     |     |
| Kémia                             |     |     |     |     |     |     |     |     |   1 |   2 |     |     |
| Környezetismeret                  |     |     |   1 |   1 |     |     |     |     |     |     |     |     |
| Magyar nyelv és irodalom          |   7 |   7 |   5 |   5 |   4 |   4 |   3 |   3 |   3 |   4 |   4 |   4 |
| Második idegen nyelv              |     |     |     |     |     |     |     |     |   3 |   3 |   3 |   3 |
| Matematika                        |   4 |   4 |   4 |   4 |   4 |   4 |   3 |   3 |   3 |   3 |   3 |   3 |
| Mozgóképkultúra és médiaismeret   |     |     |     |     |     |     |     |     |     |     |     |   1 |
| Technika és tervezés              |   1 |   1 |   1 |   1 |   1 |   1 |   1 |     |     |     |     |     |
| Természettudomány                 |     |     |     |     |   2 |   2 |   4 |   5 |     |     |   2 |     |
| Testnevelés és egészségfejlesztés |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |   5 |
| Történelem                        |     |     |     |     |   2 |   2 |   2 |   2 |   2 |   2 |   3 |   3 |
| Vizuális kultúra                  |   2 |   2 |   2 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |     |     |
| Közösségi nevelés (osztályfőnöki) |     |     |     |     |   1 |   1 |   1 |   1 |   1 |   1 |   1 |   1 |
| Kötött célú órakeret              |     |     |     |     |     |     |     |     |     |     |   4 |   4 |
|                                   |     |     |     |     |     |     |     |     |     |     |     |     |
| _Összesen_                        |  22 |  22 |  22 |  23 |  26 |  26 |  27 |  27 |  31 |  31 |  26 |  24 |

- _Hon- és népismeret_ tantárgy a 6. évfolyamon van.
- A _Biológia_, _Fizika_, _Földrajz_ és _Kémia_ a diszciplináris tartalmak az iskola a 7. és 8. évfolyamon egy integrált _Természettudomány_ tantárgy részeként jelennek meg.
- _Második idegennyelv_ tantárgy célja, hogy a gyerekek 12. évfolyam végére elérjék a KER szerinti A2 szintet.
- A 12. évfolyamon az iskola a _Mozgóképkultúra és médiaismeret_ tantárgyat választja.
