# Tantárgyi óraszámok

| Tantárgy                          |   9 |  10 |  11 |  12 |  13 |
| --------------------------------- | --: | --: | --: | --: | --: |
| Állampolgári ismeretek            |     |     |     |   1 |     |
| Digitális kultúra                 |   1 |     |     |     |     |
| Első élő idegen nyelv             |   4 |   4 |   3 |   3 |   3 |
| Komplex természettudomány         |   3 |     |     |     |     |
| Magyar nyelv és irodalom          |   4 |   5 |   3 |   3 |     |
| Második idegen nyelv              |   1 |   2 |   2 |   2 |   2 |
| Matematika                        |   4 |   4 |   3 |   3 |     |
| Mentorálás és egyéni tanulási idő |   5 |   4 |   5 |   5 |   3 |
| Pénzügyi és vállalkozói ismeretek |     |   1 |     |     |     |
| Szoftverfejlesztés és -tesztelés  |   7 |   9 |  14 |  14 |  25 |
| Testnevelés                       |   4 |   4 |   3 |   3 |   3 |
| Történelem                        |   3 |   3 |   2 |   2 |     |
| _Összesen_                        |  36 |  36 |  35 |  36 |  36 |
