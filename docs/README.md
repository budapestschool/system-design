# Budapest School Modell

A BPS Modell a BPS-iskola működését írja le: egyszerre egy bevezető új családok vagy szülők számára, 
sorvezető a munkatársak számára és jogszabályilag kötött keretrendszer az iskola működésére. A BPS Modell sűríti mindazt
amit az elmúlt években a tanulásról megtanultunk. Annak az útnak a tapasztalatait összegezzük, amely során az álomból az első csoportjaink létrejöttek, majd újabbak és újabbak alakultak. Ahogy nőtt a Budapest School, úgy kezdtük egyre jobban megérteni, mit is csinálunk és miként kell ezt a tanárok, a családok, a gyerekek, valamint a társadalom és a jogalkotó elvárásaihoz igazítanunk.

Az 2018 óra azon dolgoztunk, hogy magántanulók közösségéből államilag elfogadott iskolává válhasson a Budapest School. Arra a kérdésre kerestük a választ, hogy létrehozhatunk-e egy személyre szabott, önvezérelt tanulási modellt úgy, hogy az mindenben megfeleljen a törvényi előírásoknak. Ezért írtunk egy _kerettantervet_, ami arra a kérdésre válaszol, hogy mit tanulnak a gyerekek. Amikor ezzel elkészültünk, leírtuk azt is, hogyan tanulnak, és mire a kérdés minden részlete kibomlott előttünk, elkészült a _pedagógiai programunk_ is. Menet közben ráébredtünk, hogy a mit és a hogyan kérdése közötti határok jóval elmósódottabbak, mint azt elsőre gondolnánk. A Budapest Schoolba járó gyerekek ugyanis éppannyira dönthetnek a tartalomról, mint amennyire formálói lehetnek a mindennapok működésének is. Ahogy a jogalkotó is megváltoztatja a törvényeket, hogy szándéka szerint jobbá tegye az állam működését, úgy egy tanuló szervezet is folyamatosan újraírja a saját szabályait igazodva a körülményekhez. A hogyan és mit tanulnak a gyerekek kérdésekre adott válaszainkat gyúrtuk egybe. Így jött létre a Budapest School Modell.

## A modell több nézete
Ide majd még írunk valami szépet. A lényeg, hogy ez a szöveg egyszerre több minden
1. pedagógia program, ha [a tanulás megközelítésétől az alapelvektől indul az olvasó](/iskola-celja/mi-az-a-bps.md)
2. szülők és tanárok számára jó [áttekintő arról, hogyan milyen a tanulási élrmény a BPS-ben](/tanulasi-elmeny/szemelyreszabott-iskola.md).
3. szervezeti és működési szabályzat formáját és tartalmát a magyar jogszabályok egész komolyan szabályozzák. Ha jogi szemlélettel indulna el az olvasó akkor a
[SZMSZ](/iskola-szervezet/szmsz.md) a jó kiindulási pont.

## Egyéni és közösségi szempontok

A Budapest School Modell egyszerre akar a gyerekek számára egy önvezérelt és
személyreszabott tanulási környezetet biztosítani, ami képes agilisen
reagálni a gyerekek és a környezet igényeire és egy stabil, biztonságos,
kiszámítható rendszert is adni, ami biztosítja az iskola és más iskolák
közötti átjárhatóságot, a továbbtanulást.

![bps model](./pics/celok_egyensulya.jpg)
