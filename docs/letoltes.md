# Letöltés

- Az Általános Iskola és Gimnázium egyedi programja, a BPS modell [pdf formátumban.](/egyedi-program-altgim.pdf)

- Ha véletlenül tudod, mi az a markdown, és a forrásfájlokhoz is hozzá akarsz férni, akkor itt eléred a [teljes gitlab repot](https://gitlab.com/budapestschool/system-design).

- Az akkreditációhoz kapcsolódó dokumentumokat [külön oldalon](/jogszabalyok/akkreditacio.md) szedtük össze.
