# Elsősegély-nyújtási alapismeretek

A cél, hogy a gyerekek és tanárok megtanulják aktívan úgy alakítani
környezetüket és viselkedésüket, hogy a balesetek számát minimalizálják,
hogy felismerjék, amikor segítségre van szükség, hogy hatékonyan
segítsenek és tudjanak segítséget hívni. Ehhez gyakorlásra, a témával
kapcsolatos védett időre van szükség. Ezért a 2., 4.,
6., 8. és 10. évfolyam csak akkor teljesíthető, ha a gyerek minden
második évben elvégez egy minimum négyórás modult, aminek célja,

- hogy a gyerekek sajátítsák el a legalapvetőbb és legkorszerűbb elsősegélynyújtási módokat, azaz tudjanak egymásnak segíteni baj esetén (nemcsak elméletben, hanem a gyakorlatban is);

- sajátítsák el, mikor és hogyan kell mentőt, segítséget hívni;

- foglalkozzanak azzal, hogy hogyan tudják környezetüket, csoportjukat, tanulóközösségüket biztonságosabbá tenni, és ezt dokumentálják is.

A modul szervezője próbáljon meg elsősegélynyújtási bemutatót szervezni
a gyerekeknek az Országos Mentőszolgálat, a Magyar Ifjúsági Vöröskereszt, az Ifjúsági Elsősegélynyújtók Országos Egyesületének vagy más,
magyar vagy külföldi képesítést szerzett szakembernek a bevonásával.

Az elsősegélynyújtási alapismeretek elsajátításával kapcsolatos
feladatok megvalósításának elősegítése érdekében az iskola

- kapcsolatot épít ki az Országos Mentőszolgálattal, a Magyar Ifjúsági Vöröskereszttel vagy az Ifjúsági Elsősegélynyújtók Országos Egyesületével. Tanulóink — választásuk szerint — bekapcsolódhatnak az elsősegélynyújtással kapcsolatos iskolán kívüli vetélkedőkbe;

- minden második évben legalább egyszer a tanároknak lehetőséget biztosít elsősegély-tanfolyam látogatására.
