# NAT céljainak támogatása

A Nemzeti alaptantervben szereplő fejlesztési célok elérését és a
kulcskompetenciák fejlődését több minden támogatja:

- Egyrészt a tantárgyak 100%-ban lefedik a NAT fejlesztési céljait kulcskompetenciáit, műveltségi területeit és tananyagtartalmát.

- Másrészt az iskola életében, folyamatában való részvétel sok esetben már önmagában biztosítja a kulcskompetenciák fejlődését és a NAT fejlesztési céljainak teljesülését.

A NAT fejlesztési céljainak elérését nemcsak a tantárgyak, hanem az
iskola struktúrája is támogatja.

| **A NAT fejlesztési céljai**                 | **Struktúra**                |
| -------------------------------------------- | ---------------------------- |
| Az erkölcsi nevelés                          | közösség                     |
| Nemzeti öntudat, hazafias nevelés            | projektek                    |
| Állampolgárságra, demokráciára nevelés       | közösség                     |
| Az önismeret és a társas kultúra fejlesztése | saját tanulási út, közösség  |
| A családi életre nevelés                     | saját tanulási út, közösség  |
| A testi és lelki egészségre nevelés          | közösség                     |
| Felelősségvállalás másokért, önkéntesség     | közösség, projektek          |
| Fenntarthatóság, környezettudatosság         | projektek                    |
| Pályaorientáció                              | saját tanulási út            |
| Gazdasági és pénzügyi nevelés                | projektek                    |
| Médiatudatosságra nevelés                    | projektek                    |
| A tanulás tanítása                           | saját tanulási út, mentorság |

A _saját tanulási_ út fogalma például önmagában segíti a tanulás
tanulását, hiszen az a gyerek, aki képes önmagának saját célt állítani
(mentori segítséggel), azt elérni, és a folyamatra való reflektálás
során képességeit javítani, az fejleszti a tanulási képességét.

Egy másik példa: a Budapest School-iskolában a _közösség_ maga
hozza a működéséhez szükséges szabályokat, folyamatosan alakítja és
fejleszti saját működését a tagok aktív részvételével. Ez az aktív
állampolgárságra, a demokráciára való nevelésnek a Nemzeti Alaptantervben
előírt céljait is támogatja.

A NAT kulcskompetenciáinak fejlesztését támogatják a tantárgyak és
az iskola felépítése is az alábbiak szerint:

| **NAT kulcskompetenciái**                                                         | **Struktúra**                                          |
| --------------------------------------------------------------------------------- | ------------------------------------------------------ |
| Tanulás kompetenciái                                                              | saját tanulási út, mentorság                           |
| Kommunikációs kompetenciák (anyanyelvi és idegen nyelvi)                          | tanulási szerződés, portfólió                          |
| Digitális kompetenciák                                                            | digitális portfóliókezelés                             |
| Matematikai, gondolkodási kompetenciák                                            |                                                        |
| Személyes és társas kapcsolati kompetenciák                                       | közösség fókusz                                        |
| Kreativitás, a kreatív alkotás, önkifejezés és kulturális tudatosság kompetenciái | interdiszciplináris modulok, KULT fejlesztési cél      |
| Munkavállalói, innovációs és vállalkozói kompetenciák                             | saját tanulási út, önálló tanulás, közösség, projektek |

## A tantárgyközi tudás- és képességterületek fejlesztésének feladata

A Budapest School iskola elvégzi a NAT kulcskompetenciáinak
fejlesztését, támogatja a NAT által meghatározott fejlesztési területek
céljait, és ellátja a műveltségi területekhez rendelt fejlesztési
feladatokat. A foglalkozások többsége nem tantárgyak alapján szerveződik, így
nálunk a tantárgyközi tudás az alapértelmezett.
