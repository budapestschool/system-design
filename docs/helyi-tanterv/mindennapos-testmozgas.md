# Mindennapos testmozgás

Az iskolában „tanulóközösségeknek saját fókuszuk, helyszínük, stílusuk alakulhat ki".
Ennek részeként a gyerekek mindennapos testmozgását is tanulóközösségenként másképp, az
identitásuknak és lehetőségeiknek megfelelőn alakítják ki. Az iskola csak
annyit ír elő, hogy mindennap legyen minimum 35 perc, aminek elsődleges
célja a testmozgás és testnevelés.

Nincs olyan testmozgásforma, amit ez a program preferálna, vagy ami
kizárt lenne. A mindennapos testmozgás fizikai szükségleteinek
megszervezése is változatos módon történhet:

1. Egyes telephelyek saját tornatermében, tornaszobájában vagy udvarán;

2. szerződés, megállapodás alapján, elérhető közelben lévő más oktatási vagy sportlétesítményben (úszás, korcsolya, torna, foci, falmászás,
   sípálya);

3. a szabadban, épített helyszínhez nem kötődő mozgásszervezés (pl. kirándulás, séta, barangolás, felfedező játékok);

4. a feladatellátási helyeken, magyarul a tantermekben, olyan mozgások esetében, amelyek nem igényelnek speciálisan sportolásra kiképzett helyiséget (pl. jóga).

A testmozgás minden esetben a mozgást ismerő, abban képesítéssel,
végzettséggel vagy megfelelő gyakorlattal rendelkező személy vezetésével
vagy felügyelete mellett zajlik. Az iskola feladata monitorozni a
mindennapos testnevelés megvalósulását: ki, mikor, hol, milyen
testmozgást vezetett a gyerekeknek.
