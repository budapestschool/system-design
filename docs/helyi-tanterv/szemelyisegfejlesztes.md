# Az egészséges és boldog gyerek

A Budapest School-gyerekek boldogak, egészségesek, hasznosak
közösségüknek. Képesek önmaguknak célokat állítani, azokat elérni.
Képesek már kisgyerekkortól sajátjukként megélni a tanulást és ahhoz
kapcsolódóan célokat elérni, és fokozatosan tanulják meg azt, hogy
egyénileg és csoportosan is tudnak nagyszabású projekteket véghezvinni.
Tesznek a saját egészségükért, jövőjükért, társaikért, kapcsolódnak
önmagukhoz és társaikhoz.

A gyerekek személyiségfejődését két szinten támogatja az iskola. Az első
szintet a mindennapi működés adja, mert az iskola működése önmagában
személyiség- és egészségfejlesztő hatással bír. A második szintet a
harmónia kiemelt fejlesztési irányelv biztosítja, ami holisztikus
megközelítésével támogatja a gyerekek fizikai, lelki jóllétét és
kapcsolódásukat a környezethez.

## Működésből adódó fejlesztések

Az iskola alapműködése, hogy a gyerekek csoportban, közösségben élnek,
tanulnak, dolgoznak, ezért „természetes", hogy fejlődik az _empátiájuk,
kooperációs, kollaborációs képességük és érzelmi intelligenciájuk_.
Alapelvünk: minél közelebb áll az iskola működése a jövő
hétköznapjaihoz, a családhoz és a munkahelyhez, annál jobban támogatja a boldog családi
életre, a sikeres munkahelyre való felkészülést már önmagában az iskolában való
aktív részvétel is. Ehhez hasonlóan, ahogy támogató,
funkcionális, boldog családban felnőtt gyerekek nagyobb valószínűséggel
lesznek maguk is egészségesebbek és boldogabbak.

A _fejlődésfókuszú gondolkodásmód_ kialakítását kulcstényezőnek
gondoljuk a gyerekeink hosszú távú boldogulásához. Ezért az iskolában a saját célok
által irányított tanulási környezettől kezdve a jutalmazás, értékelés,
visszajelzés módjáig minden azt a célt szolgálja, hogy a
gyerekek képesek legyenek pozitívan gondolkodni magukról, ami az
ép és egészséges embernek talán egyik legfontosabb jellemzője.

A _teljes körű iskolai egészségfejlesztést_ az alábbi négy
egészségfejlesztési feladat rendszeres végzése adja:

- egészséges táplálkozás megvalósítása (elsősorban megfelelő, magas minőségű, lehetőleg helyi alapanyagokból)

- mindennapi testmozgás minden gyereknek (változatos foglalkozásokkal, koncentráltan az egészségjavító elemekre, módszerekre, pl.tartásjavító torna, tánc, jóga)

- a gyerekek érett személyiséggé válásának elősegítése személyközpontú pedagógiai módszerekkel és a művészetek személyiségfejlesztő hatékonyságú alkalmazásával (ének, tánc, rajz, mesemondás, népi játékok, stb.)

- a környezeti, médiatudatossági, fogyasztóvédelmi, balesetvédelmi egészségfejlesztési modulok, modulrészletek hatékony (azaz „bensővéváló") oktatása

## Egészségügyi felmérés szervezése és hatása a gyerekek életére

A Budapest School-gyerekek megelőző jelleggel rendszeresen iskolaorvosi,
védőnői és fogorvosi felülvizsgálaton vesznek részt. Az orvosi, védőnői
és fogorvosi vizsgálatot a fenntartó a mindenkori jogszabályokban
meghatározott rendszerességgel szervezi meg. Jelenleg ezeket külső
helyszínen, megbízott orvossal, fogorvossal és védőnővel szervezi meg a
fenntartó.

Minden tanulóközösség tanulásszervező csapata a tanév megkezdése előtt
kijelöli az egészségnapokat: amikor a védőnői, orvosi, fogorvosi és
egyéb fizikai és mentális felülvizsgálatokat megszervezi. Húsz főig egy,
afölött pedig két napot kell megjelölni és a fenntartóval egyeztetni.
Egyeztetni azért kell, hogy a különböző tanulóközösségek között ne legyen
időpontütközés. Ha a gyerek az egészségnapon hiányzik az iskolából,
akkor a szülő feladata a felülvizsgálatot megszerveznie.

Ezeken a napokon a gyerekek és a tanárok iskolaidőben elutaznak a
rendelőkbe, felkeresik az orvost, fogorvost, védőnőt. Mivel a gyerekek
kivizsgálása feltehetőleg egyesével történik, az éppen nem soron lévőknek
sokat kell várnia. Ezért a tanárok erre az időre mikromodulokat terveznek az egészség témában. Ilyenkor a gyerekek olyan tanulási eredményeket
érhetnek el, mint a _„Tisztában van az egészség megőrzésének
jelentőségével, és tudja, hogy maga is felelős ezért."_ (5. évfolyam 1.
félév).

A felülvizsgálatok eredményeit a gyerekek a mentorokkal megbeszélik,
és ha szükséges, akkor az eredmények alapján fejlődési célokat
fogalmaznak meg. A vizsgálatok eredményeit a gyerekek a portfóliójukban
ugyanúgy megőrzik, mint például egy tudáspróba eredményét.
