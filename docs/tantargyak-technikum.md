# Tantárgyak tartalma, a tanulási eredmények

## Állampolgári ismeretek

### 12. évfolyamon

- Megérti a család szerepét, alapvető feladatait az egyén és a nemzet szempontjából egyaránt.

- Értékeli a nemzeti identitás jelentőségét az egyén és a közösség szempontjából is.

- Ismeri a választások alapelveit és a törvényhozás folyamatát.

- Megismeri a demokratikus jogállam működésének alapvető sajátosságait.

- Érti és vallja a haza védelmének, a nemzetért történő tenni akarás fontosságát.

- A mindennapi életének megszervezésében alkalmazza a jogi és gazdasági-pénzügyi ismereteit.

- Saját pénzügyeiben tudatos döntéseket hoz.

- Felismeri az életpálya-tervezés és a munkavállalás egyéni és társadalmi jelentőségét.

- Ismeri a munka világát érintő alapvető jogi szabályozást, a munkaerőpiac jellemzőit, tájékozódik a foglalkoztatás és a szakmaszerkezet változásairól.

- Értelmezi a család mint a társadalom alapvető intézményének szerepét és jellemzőit.

- Társaival megbeszéli a párválasztás, a családtervezés fontos szakaszait, szempontjait és a gyermekvállalás demográfiai jelentőségét: tájékozódás, minták, orientáló példák, átgondolt tervezés, felelősség.

- Felismeri, hogy a családtagok milyen szerepet töltenek be a szocializáció folyamatában.

- Értelmezi a családi szocializációnak az ember életútját befolyásoló jelentőségét.

- Felismeri az alapvető emberi jogok egyetemes és társadalmi jelentőségét.

- Bemutatja magyarország alaptörvényének legfontosabb részeit: alapvetés; az állam; szabadság és felelősség.

- Érti a társadalmi normák és az egyéni cselekedetek, akaratok, célok egyeztetésének, összehangolásának követelményét. elméleti és tapasztalati úton ismereteket szerez a társadalmi felelősségvállalásról, a segítségre szorulók támogatásának lehetőségeiről.

- Megérti a honvédelem szerepét az ország biztonságának fenntartásában, megismeri a haza védelmének legfontosabb feladatcsoportjait és területeit, az egyén kötelezettségeit.

- Felismeri és értelmezi az igazságosság, az esélyegyenlőség biztosításának jelentőségét és követelményeit.

- Értelmezi a választójog feltételeit és a választások alapelveit.

- Értelmezi a törvényalkotás folyamatát.

- Megérti a nemzeti érzület sajátosságait és a hazafiság fontosságát, lehetséges megnyilvánulási formáit.

- Véleményt alkot a nemzetállamok és a globalizáció összefüggéseiről.

- Felismeri a világ magyarsága mint nemzeti közösség összetartozásának jelentőségét.

- Érti és felismeri a honvédelem mint nemzeti ügy jelentőségét.

- Felismeri és értékeli a helyi, regionális és országos közgyűjtemények nemzeti kulturális örökség megőrzésében betöltött szerepét.

- Azonosítja a mindennapi ügyintézés alapintézményeit.

- Életkori sajátosságainak megfelelően jártasságot szerez a jog területének mindennapi életben való alkalmazásában.

- Tájékozott a munkavállalással kapcsolatos szabályokban.

- Megtervezi egy fiktív család költségvetését.

- Saját pénzügyi döntéseit körültekintően, megalapozottan hozza meg.

- Megismeri a megalapozott, körültekintő hitelfelvétel szempontjait, illetve feltételeit.

- Azonosítja az állam gazdasági szerepvállalásának elemeit.

- Felismeri és megérti a közteherviselés nemzetgazdasági, társadalmi és morális jelentőségét.

- Életvitelébe beépülnek a tudatos fogyasztás elemei, életmódjában figyelmet fordít a környezeti terhelés csökkentésére, érvényesíti a fogyasztóvédelmi szempontokat.

- Értelmezi a vállalkozás indítását befolyásoló tényezőket.

- Felismeri a véleménynyilvánítás, érvelés, a párbeszéd és a vita társadalmi hasznosságát.

- Képes arra, hogy feladatait akár önálló, akár társas tanulás révén végezze el, célorientáltan képes az együttműködésre.

- Önállóan vagy társaival együttműködve javaslatokat fogalmaz meg.

- Tiszteletben tartja a másik ember értékvilágát, gondolatait és véleményét, ha szükséges, kritikusan viszonyul emberi cselekedetekhez, magatartásformákhoz.

- Megismerkedik a tudatos médiafogyasztói magatartással és a közösségi média használatával.

- A tanulási tevékenységek szakaszaiban használja az infokommunikációs eszközöket, lehetőségeket, tisztában van azok szerepével, innovációs potenciáljával és veszélyeivel is.

## Digitális kultúra

### 9. évfolyamon

- Elmélyülten dolgozik digitális környezetben, önellenőrzést végez.

- Megvizsgálja és értékeli az általa vagy társai által alkalmazott, létrehozott, megvalósított eljárásokat.

- Társaival együttműködve online és offline környezetben egyaránt megold különböző feladatokat, ötleteit, véleményét megfogalmazza, részt vesz a közös álláspont kialakításában.

- Kiválasztja az általa ismert informatikai eszközök és alkalmazások közül azokat, melyek az adott probléma megoldásához szükségesek.

- Eredményétől függően módosítja a problémamegoldás folyamatában az adott, egyszerű tevékenységsorokat.

- A rendelkezésére álló eszközökkel, forrásokból meggyőződik a talált vagy kapott információk helyességéről.

- Közvetlen otthoni vagy iskolai környezetéből megnevez néhány informatikai eszközt, felsorolja fontosabb jellemzőit.

- Megfogalmazza, néhány példával alátámasztja, hogyan könnyíti meg a felhasználó munkáját az adott eszköz alkalmazása.

- Egyszerű feladatokat old meg informatikai eszközökkel. esetenként tanítói segítséggel összetett funkciókat is alkalmaz.

- Önállóan vagy tanítói segítséggel választ más tantárgyak tanulásának támogatásához applikációkat, digitális tananyagot, oktatójátékot, képességfejlesztő digitális alkalmazást.

- Kezdetben tanítói segítséggel, majd önállóan használ néhány, életkorának megfelelő alkalmazást, elsősorban információgyűjtés, gyakorlás, egyéni érdeklődésének kielégítése céljából.

- A feladathoz, problémához digitális eszközt, illetve alkalmazást, applikációt, felhasználói felületet választ; felsorol néhány érvet választásával kapcsolatosan.

- Adott szempontok alapján megfigyel néhány, grafikai alkalmazással készített produktumot; személyes véleményét megfogalmazza.

- Grafikai alkalmazással egyszerű, közvetlenül hasznosuló rajzot, grafikát, dokumentumot hoz létre.

- Egy rajzos dokumentumot adott szempontok alapján értékel, módosít.

- Állításokat fogalmaz meg grafikonokról, infografikákról, táblázatokról; a kapott információkat felhasználja napi tevékenysége során.

- Információkat keres, a talált adatokat felhasználja digitális produktumok létrehozására.

- Értelmezi a problémát, a megoldási lehetőségeket eljátssza, megfogalmazza, egyszerű eszközök segítségével megvalósítja.

- Információt keres az interneten más tantárgyak tanulása során, és felhasználja azt.

- Egyszerű prezentációt, ábrát, egyéb segédletet készít.

- Felismer, eljátszik, végrehajt néhány hétköznapi tevékenysége során tapasztalt, elemi lépésekből álló, adott sorrendben végrehajtandó cselekvést.

- Egy adott, mindennapi életből vett algoritmust elemi lépésekre bont, értelmezi a lépések sorrendjét, megfogalmazza az algoritmus várható kimenetelét.

- Feladat, probléma megoldásához többféle algoritmust próbál ki.

- A valódi vagy szimulált programozható eszköz mozgását értékeli, hiba esetén módosítja a kódsorozatot a kívánt eredmény eléréséig. tapasztalatait megfogalmazza, megvitatja társaival.

- Adott feltételeknek megfelelő kódsorozatot tervez és hajtat végre, történeteket, meserészleteket jelenít meg padlórobottal vagy más eszközzel.

- Alkalmaz néhány megadott algoritmust tevékenység, játék során, és néhány egyszerű esetben módosítja azokat.

- Információkat keres az interneten, egyszerű eljárásokkal meggyőződik néhány, az interneten talált információ igazságértékéről.

- Kiválasztja a számára releváns információt, felismeri a hamis információt.

- Tisztában van a személyes adat fogalmával, törekszik megőrzésére, ismer néhány példát az e-világ veszélyeivel kapcsolatban.

- Ismeri és használja a kapcsolattartás formáit és a kommunikáció lehetőségeit a digitális környezetben.

- Ismeri a mobileszközök alkalmazásának előnyeit, korlátait, etikai vonatkozásait.

- Közvetlen tapasztalatokat szerez a digitális eszközök használatával kapcsolatban.

- Képes feladat, probléma megoldásához megfelelő applikáció, digitális tananyag, oktatójáték, képességfejlesztő digitális alkalmazás kiválasztására.

- Ismer néhány, kisiskolások részére készített portált, információforrást, digitálistananyag-lelőhelyet.

- Önállóan használja a digitális eszközöket, az online kommunikáció eszközeit, tisztában van az ezzel járó veszélyekkel.

- Elsajátítja a digitális írástudás eszközeit, azokkal feladatokat old meg.

- Megismeri a felmerülő problémák megoldásának módjait, beleértve az adott feladat megoldásához szükséges algoritmus értelmezését, alkotását és számítógépes program készítését és kódolását a blokkprogramozás eszközeivel.

- Digitális tudáselemek felhasználásával, társaival együttműködve különböző problémákat old meg.

- Megismeri a digitális társadalom elvárásait, lehetőségeit és veszélyeit.

- Célszerűen választ a feladat megoldásához használható informatikai eszközök közül.

- Az informatikai eszközöket önállóan használja, a tipikus felhasználói hibákat elkerüli, és elhárítja az egyszerűbb felhasználói szintű hibákat.

- Értelmezi az informatikai eszközöket működtető szoftverek hibajelzéseit, és azokról beszámol.

- Önállóan használja az operációs rendszer felhasználói felületét.

- Önállóan kezeli az operációs rendszer mappáit, fájljait és a felhőszolgáltatásokat.

- Használja a digitális hálózatok alapszolgáltatásait.

- Tapasztalatokkal rendelkezik a digitális jelek minőségével, kódolásával, tömörítésével, továbbításával kapcsolatos problémák kezeléséről.

- Egy adott feladat kapcsán önállóan hoz létre szöveges vagy multimédiás dokumentumokat.

- Ismeri és tudatosan alkalmazza a szöveges és multimédiás dokumentum készítése során a szöveg formázására, tipográfiájára vonatkozó alapelveket.

- A tartalomnak megfelelően alakítja ki a szöveges vagy a multimédiás dokumentum szerkezetét, illeszti be, helyezi el és formázza meg a szükséges objektumokat.

- Ismeri és kritikusan használja a nyelvi eszközöket (például helyesírás-ellenőrzés, elválasztás).

- A szöveges dokumentumokat többféle elrendezésben jeleníti meg papíron, tisztában van a nyomtatás környezetre gyakorolt hatásaival.

- Ismeri a prezentációkészítés alapszabályait, és azokat alkalmazza.

- Etikus módon használja fel az információforrásokat, tisztában van a hivatkozás szabályaival.

- Digitális eszközökkel önállóan rögzít és tárol képet, hangot és videót.

- Digitális képeken képkorrekciót hajt végre.

- Ismeri egy bittérképes rajzolóprogram használatát, azzal ábrát készít.

- Bemutató-készítő vagy szövegszerkesztő programban rajzeszközökkel ábrát készít.

- Érti, hogyan történik az egyszerű algoritmusok végrehajtása a digitális eszközökön.

- Megkülönbözteti, kezeli és használja az elemi adatokat.

- Értelmezi az algoritmus végrehajtásához szükséges adatok és az eredmények kapcsolatát.

- Egyszerű algoritmusokat elemez és készít.

- Ismeri a kódolás eszközeit.

- Adatokat kezel a programozás eszközeivel.

- Ismeri és használja a programozási környezet alapvető eszközeit.

- Ismeri és használja a blokkprogramozás alapvető építőelemeit.

- A probléma megoldásához vezérlési szerkezetet (szekvencia, elágazás és ciklus) alkalmaz a tanult blokkprogramozási nyelven.

- Az adatokat táblázatos formába rendezi és formázza.

- Cellahivatkozásokat, matematikai tudásának megfelelő képleteket, egyszerű statisztikai függvényeket használ táblázatkezelő programban.

- Az adatok szemléltetéséhez diagramot készít.

- Problémákat old meg táblázatkezelő program segítségével.

- Tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes szimulációjáról.

- Vizsgálni tudja a szabályozó eszközök hatásait a tantárgyi alkalmazásokban.

- Ismeri az információkeresés technikáját, stratégiáját és több keresési szempont egyidejű érvényesítésének lehetőségét.

- Önállóan keres információt, a találatokat hatékonyan szűri.

- Az internetes adatbázis-kezelő rendszerek keresési űrlapját helyesen tölti ki.

- Ismeri, használja az elektronikus kommunikáció lehetőségeit, a családi és az iskolai környezetének elektronikus szolgáltatásait.

- Ismeri és betartja az elektronikus kommunikációs szabályokat.

- Mozgásokat vezérel szimulált vagy valós környezetben.

- Adatokat gyűjt szenzorok segítségével.

- Tapasztalatokkal rendelkezik az eseményvezérlésről.

- Ismeri a térinformatika és a 3d megjelenítés lehetőségeit.

- Tapasztalatokkal rendelkezik az iskolai oktatáshoz kapcsolódó mobileszközökre fejlesztett alkalmazások használatában.

- Tisztában van a hálózatokat és a személyes információkat érintő fenyegetésekkel, alkalmazza az adatok védelmét biztosító lehetőségeket.

- Védekezik az internetes zaklatás különböző formái ellen, szükség esetén segítséget kér.

- Ismeri a digitális környezet, az e-világ etikai problémáit.

- Ismeri az információs technológia fejlődésének gazdasági, környezeti, kulturális hatásait.

- Ismeri az információs társadalom múltját, jelenét és várható jövőjét.

- Online gyakorolja az állampolgári jogokat és kötelességeket.

- Ismeri az informatikai eszközök és a működtető szoftvereik célszerű választásának alapelveit, használja a digitális hálózatok alapszolgáltatásait, az online kommunikáció eszközeit, tisztában van az ezzel járó veszélyekkel, ezzel összefüggésben ismeri a segítségnyújtási, segítségkérési lehetőségeket.

- Gyakorlatot szerez dokumentumok létrehozását segítő eszközök használatában.

- Megismeri az adatkezelés alapfogalmait, képes a nagyobb adatmennyiség tárolását, hatékony feldolgozását biztosító eszközök és módszerek alapszintű használatára, érti a működésüket.

- Megismeri az algoritmikus probléma megoldásához szükséges módszereket és eszközöket, megoldásukhoz egy magas szintű formális programozási nyelv fejlesztői környezetét önállóan használja.

- Hatékonyan keres információt; az ikt-tudáselemek felhasználásával társaival együttműködve problémákat old meg.

- Ismeri az e-világ elvárásait, lehetőségeit és veszélyeit.

- Ismeri és tudja használni a célszerűen választott informatikai eszközöket és a működtető szoftvereit, ismeri a felhasználási lehetőségeket.

- Ismeri a digitális eszközök és a számítógépek fő egységeit, ezek fejlődésének főbb állomásait, tendenciáit.

- Tudatosan alakítja informatikai környezetét, ismeri az ergonomikus informatikai környezet jellemzőit, figyelembe veszi a digitális eszközök egészségkárosító hatásait, óvja maga és környezete egészségét.

- Önállóan használja az informatikai eszközöket, elkerüli a tipikus felhasználói hibákat, elhárítja az egyszerűbb felhasználói hibákat.

- Céljainak megfelelően használja a mobileszközök és a számítógépek operációs rendszereit.

- Igénybe veszi az operációs rendszer és a számítógépes hálózat alapszolgáltatásait.

- Követi a technológiai változásokat a digitális információforrások használatával.

- Használja az operációs rendszer segédprogramjait, és elvégzi a munkakörnyezet beállításait.

- Tisztában van a digitális kártevők elleni védekezés lehetőségeivel.

- Használja az állományok tömörítését és a tömörített állományok kibontását.

- Ismeri egy adott feladat megoldásához szükséges digitális eszközök és szoftverek kiválasztásának szempontjait.

- Speciális dokumentumokat hoz létre, alakít át és formáz meg.

- Tapasztalatokkal rendelkezik a formanyomtatványok, a sablonok, az előre definiált stílusok használatáról.

- Gyakorlatot szerez a fotó-, hang-, videó-, multimédia-szerkesztő, a bemutató-készítő eszközök használatában.

- Alkalmazza az információkeresés során gyűjtött multimédiás alapelemeket új dokumentumok készítéséhez.

- Dokumentumokat szerkeszt és helyez el tartalomkezelő rendszerben.

- Ismeri a html formátumú dokumentumok szerkezeti elemeit, érti a css használatának alapelveit; több lapból álló webhelyet készít.

- Létrehozza az adott probléma megoldásához szükséges rasztergrafikus ábrákat.

- Létrehoz vektorgrafikus ábrákat.

- Digitálisan rögzít képet, hangot és videót, azokat manipulálja.

- Tisztában van a raszter-, a vektorgrafikus ábrák tárolási és szerkesztési módszereivel

- Érti az egyszerű problémák megoldásához szükséges tevékenységek lépéseit és kapcsolatukat.

- Ismeri a következő elemi adattípusok közötti különbségeket: egész, valós szám, karakter, szöveg, logikai.

- Ismeri az elemi és összetett adattípusok közötti különbségeket.

- Érti egy algoritmus-leíró eszköz alapvető építőelemeit, érti a típusalgoritmusok felhasználásának lehetőségeit.

- Példákban, feladatok megoldásában használja egy formális programozási nyelv fejlesztői környezetének alapszolgáltatásait.

- Szekvencia, elágazás és ciklus segítségével algoritmust hoz létre, és azt egy magas szintű formális programozási nyelven kódolja.

- A feladat megoldásának helyességét teszteli.

- Adatokat táblázatba rendez.

- Táblázatkezelővel adatelemzést és számításokat végez.

- A problémamegoldás során függvényeket célszerűen használ.

- Nagy adathalmazokat tud kezelni.

- Az adatokat diagramon szemlélteti.

- Ismeri az adatbázis-kezelés alapfogalmait.

- Az adatbázisban interaktív módon keres, rendez és szűr.

- A feladatmegoldás során az adatbázisba adatokat visz be, módosít és töröl, űrlapokat használ, jelentéseket nyomtat.

- Strukturáltan tárolt nagy adathalmazokat kezel, azokból egyedi és összesített adatokat nyer ki.

- Tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes szimulációjáról.

- Hétköznapi, oktatáshoz készült szimulációs programokat használ.

- Tapasztalatokat szerez a kezdőértékek változtatásának hatásairól a szimulációs programokban.

- Ismeri és alkalmazza az információkeresési stratégiákat és technikákat, a találati listát a problémának megfelelően szűri, ellenőrzi annak hitelességét.

- Etikus módon használja fel az információforrásokat, tisztában van a hivatkozás szabályaival.

- Használja a két- vagy többrésztvevős kommunikációs lehetőségeket és alkalmazásokat.

- Ismeri és alkalmazza a fogyatékkal élők közötti kommunikáció eszközeit és formáit.

- Az online kommunikáció során alkalmazza a kialakult viselkedési kultúrát és szokásokat, a szerepelvárásokat.

- Ismeri és használja a mobiltechnológiát, kezeli a mobileszközök operációs rendszereit és használ mobilalkalmazásokat.

- Céljainak megfelelő alkalmazást választ, az alkalmazás funkcióira, kezelőfelületére vonatkozó igényeit megfogalmazza.

- Az applikációkat önállóan telepíti.

- Az iskolai oktatáshoz kapcsolódó mobileszközökre fejlesztett alkalmazások használata során együttműködik társaival.

- Tisztában van az e-világ -- e-szolgáltatások, e-ügyintézés, e-kereskedelem, e-állampolgárság, it-gazdaság, környezet, kultúra, információvédelem -- biztonsági és jogi kérdéseivel.

- Tisztában van a digitális személyazonosság és az információhitelesség fogalmával.

- A gyakorlatban alkalmazza az adatok védelmét biztosító lehetőségeket.

## Első élő idegen nyelv

### 9-12. évfolyamon

- Szóban és írásban is megold változatos kihívásokat igénylő, többnyire valós kommunikációs helyzeteket leképező feladatokat az élő idegen nyelven.

- Szóban és írásban létrehoz szövegeket különböző szövegtípusokban.

- Értelmez nyelvi szintjének megfelelő hallott és írott célnyelvi szövegeket kevésbé ismert témákban és szövegtípusokban is.

- A tanult nyelvi elemek és kommunikációs stratégiák segítségével írásbeli és szóbeli interakciót folytat és tartalmakat közvetít idegen nyelven.

- Kommunikációs szándékának megfelelően alkalmazza a nyelvi funkciókat és megszerzett szociolingvisztikai, pragmatikai és interkulturális jártasságát.

- Nyelvtudását képes fejleszteni tanórán kívüli eszközökkel, lehetőségekkel és helyzetekben is, valamint a tanultakat és gimnáziumban a második idegen nyelv tanulásában is alkalmazza.

- Felkészül az aktív nyelvtanulás eszközeivel az egész életen át történő tanulásra.

- Használ hagyományos és digitális alapú nyelvtanulási forrásokat és eszközöket.

- Alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra, ismeretszerzésre hagyományos és digitális csatornákon.

- Törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és intonáció megközelítésére.

- Beazonosítja nyelvtanulási céljait és egyéni különbségeinek tudatában, ezeknek megfelelően fejleszti nyelvtudását.

- Első idegen nyelvéből sikeresen érettségit tesz a céljainak megfelelő szinten.

- Visszaad tankönyvi vagy más tanult szöveget, elbeszélést, nagyrészt folyamatos és érthető történetmeséléssel, a cselekményt logikusan összefűzve.

- Összefüggően, érthetően és nagyrészt folyékonyan beszél az ajánlott tématartományokhoz tartozó és az érettségi témákban a tanult nyelvi eszközökkel, felkészülést követően.

- Beszámol saját élményen, tapasztalaton alapuló vagy elképzelt eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő rövid jellemzésével.

- Ajánlott tématartományhoz kapcsolódó képi hatás kapcsán saját gondolatait, véleményét és érzéseit is kifejti az ismert nyelvi eszközökkel.

- Összefoglalja ismert témában nyomtatott vagy digitális alapú ifjúsági tartalmak lényegét röviden és érthetően.

- Közép- és emelt szintű nyelvi érettségi szóbeli feladatokat old meg.

- Összefüggő, folyékony előadásmódú szóbeli prezentációt tart önállóan, felkészülést követően, az érettségi témakörök közül szabadon választott témában, ikt-eszközökkel támogatva mondanivalóját.

- Kreatív, változatos műfajú szövegeket alkot szóban, kooperatív munkaformákban.

- Beszámol akár az érdeklődési körén túlmutató környezeti eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

- Összefüggően, világosan és nagyrészt folyékonyan beszél az ajánlott tématartományhoz tartozó és az idevágó érettségi témákban, akár elvontabb tartalmakra is kitérve.

- Alkalmazza a célnyelvi normához illeszkedő, természeteshez közelítő kiejtést, beszédtempót és intonációt.

- Írásban röviden indokolja érzéseit, gondolatait, véleményét már elvontabb témákban.

- Leír összetettebb cselekvéssort, történetet, személyes élményeket, elvontabb témákban.

- Információt vagy véleményt közlő és kérő, összefüggő feljegyzéseket, üzeneteket ír.

- Alkalmazza a formális és informális regiszterhez köthető sajátosságokat.

- Használ szövegkohéziós és figyelemvezető eszközöket.

- Megold változatos írásbeli, feladatokat szövegszinten.

- Papíralapú vagy ikt-eszközökkel segített írott projektmunkát készít önállóan vagy kooperatív munkaformában.

- Összefüggő szövegeket ír önállóan, akár elvontabb témákban.

- A szövegek létrehozásához nyomtatott vagy digitális segédeszközt, szótárt használ.

- Beszámol saját élményen, tapasztalaton alapuló, akár az érdeklődési körén túlmutató vagy elképzelt személyes eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

- Beszámol akár az érdeklődési körén túlmutató közügyekkel, szórakozással kapcsolatos eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

- A megfelelő szövegtípusok jellegzetességeit követi.

- Értelmezi a szintjének megfelelő célnyelvi, komplexebb tanári magyarázatokat a nyelvórákon.

- Megérti a célnyelvi, életkorának és érdeklődésének megfelelő hazai és nemzetközi hírek, események lényegét.

- Kikövetkezteti a szövegben megjelenő elvontabb nyelvi elemek jelentését az ajánlott témakörökhöz kapcsolódó témákban.

- Értelmezi a szövegben megjelenő összefüggéseket.

- Megérti, értelmezi és összefoglalja az összetettebb, a tématartományhoz kapcsolódó összefüggő hangzó szöveget, és értelmezi a szövegben megjelenő összefüggéseket.

- Megérti és értelmezi az összetettebb, az ajánlott témakörökhöz kapcsolódó összefüggő szövegeket, és értelmezi a szövegben megjelenő összefüggéseket.

- Megérti az ismeretlen nyelvi elemeket is tartalmazó hangzó szöveg lényegi tartalmát.

- Megérti a hangzó szövegben megjelenő összetettebb részinformációkat.

- Megérti az elvontabb tartalmú hangzószövegek lényegét, valamint a beszélők véleményét is.

- Alkalmazza a hangzó szövegből nyert információt feladatok megoldása során.

- Célzottan keresi az érdeklődésének megfelelő autentikus szövegeket tanórán kívül is, ismeretszerzésre és szórakozásra.

- A tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét számára kevésbé ismert témákban és szituációkban is.

- A tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét akár anyanyelvi beszélők köznyelvi kommunikációjában a számára kevésbé ismert témákban és szituációkban is.

- Megérti és értelmezi a legtöbb televíziós hírműsort.

- Megért szokványos tempóban folyó autentikus szórakoztató és ismeretterjesztő tartalmakat, változatos csatornákon.

- Elolvas és értelmez nyelvi szintjének megfelelő irodalmi szövegeket.

- Megérti és értelmezi a lényeget az ajánlott tématartományokhoz kapcsolódó összefüggő, akár autentikus írott szövegekben.

- Megérti és értelmezi az összefüggéseket és a részleteket az ajánlott tématartományokhoz kapcsolódó összefüggő, akár autentikus írott szövegekben.

- Értelmezi a számára ismerős, elvontabb tartalmú szövegekben megjelenő ismeretlen nyelvi elemeket.

- A szövegkörnyezet alapján kikövetkezteti a szövegben előforduló ismeretlen szavak jelentését.

- Megérti az ismeretlen nyelvi elemeket is tartalmazó írott szöveg tartalmát.

- Megérti és értelmezi az írott szövegben megjelenő összetettebb részinformációkat.

- Kiszűr konkrét információkat nyelvi szintjének megfelelő szövegből, és azokat összekapcsolja egyéb ismereteivel.

- Alkalmazza az írott szövegből nyert információt feladatok megoldása során.

- Keresi az érdeklődésének megfelelő, célnyelvi, autentikus szövegeket szórakozásra és ismeretszerzésre tanórán kívül is.

- Egyre változatosabb, hosszabb, összetettebb és elvontabb szövegeket, tartalmakat értelmez és használ.

- Részt vesz a változatos szóbeli interakciót és kognitív kihívást igénylő nyelvórai tevékenységekben.

- Szóban ad át nyelvi szintjének megfelelő célnyelvi tartalmakat valós nyelvi interakciót leképező szituációkban.

- A társalgásba aktívan, kezdeményezően és egyre magabiztosabban bekapcsolódik az érdeklődési körébe tartozó témák esetén vagy az ajánlott tématartományokon belül.

- Társalgást kezdeményez, a megértést fenntartja, törekszik mások bevonására, és szükség esetén lezárja azt az egyes tématartományokon belül, akár anyanyelvű beszélgetőtárs esetében is.

- A társalgást hatékonyan és udvariasan fenntartja, törekszik mások bevonására, és szükség esetén lezárja azt, akár ismeretlen beszélgetőtárs esetében is.

- Előkészület nélkül részt tud venni személyes jellegű, vagy érdeklődési körének megfelelő ismert témáról folytatott társalgásban,

- Érzelmeit, véleményét változatos nyelvi eszközökkel szóban megfogalmazza és arról interakciót folytat.

- A mindennapi élet különböző területein, a kommunikációs helyzetek széles körében tesz fel releváns kérdéseket információszerzés céljából, és válaszol megfelelő módon a hozzá intézett célnyelvi kérdésekre.

- Aktívan, kezdeményezően és magabiztosan vesz részt a változatos szóbeli interakciót és kognitív kihívást igénylő nyelvórai tevékenységekben.

- Társaival a kooperatív munkaformákban és a projektfeladatok megoldása során is törekszik a célnyelvi kommunikációra.

- Egyre szélesebb körű témákban, nyelvi kommunikációt igénylő helyzetekben reagál megfelelő módon, felhasználva általános és nyelvi háttértudását, ismereteit, alkalmazkodva a társadalmi normákhoz.

- Váratlan, előre nem kiszámítható eseményekre, jelenségekre és történésekre jellemzően célnyelvi eszközökkel is reagál tanórai szituációkban.

- Szóban és írásban, valós nyelvi interakciók során jó nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő szinten vesz részt az egyes tématartományokban és az idetartozó érettségi témákban.

- Informális és életkorának megfelelő formális írásos üzeneteket ír, digitális felületen is.

- Véleményét írásban, tanult nyelvi eszközökkel megfogalmazza és arról írásban interakciót folytat.

- Véleményét írásban változatos nyelvi eszközökkel megfogalmazza és arról interakciót folytat.

- Írásban átad nyelvi szintjének megfelelő célnyelvi tartalmakat valós nyelvi interakciók során.

- Írásban és szóban, valós nyelvi interakciók során jó nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő szinten vesz részt az egyes tématartományokban és az idetartozó érettségi témákban.

- Összetett információkat ad át és cserél.

- Egyénileg vagy kooperáció során létrehozott projektmunkával kapcsolatos kiselőadást tart önállóan, összefüggő és folyékony előadásmóddal, digitális eszközök segítségével, felkészülést követően.

- Használ célnyelvi tartalmakat tudásmegosztásra.

- Ismer más tantárgyi tartalmakat, részinformációkat célnyelven,

- Összefoglal és lejegyzetel, írásban közvetít rövid olvasott vagy hallott szövegeket.

- Környezeti témákban a kommunikációs helyzetek széles körében hatékonyan ad át és cserél információt.

- Írott szöveget igénylő projektmunkát készít olvasóközönségnek.

- Írásban közvetít célnyelvi tartalmakat valós nyelvi interakciót leképező szituációkban.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével további alapvető érzéseket fejez ki (pl. aggódást, félelmet, kételyt).

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez érdeklődést és érdektelenséget, szemrehányást, reklamálást.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez kötelezettséget, szándékot, kívánságot, engedélykérést, feltételezést.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez ítéletet, kritikát, tanácsadást.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez segítségkérést, ajánlást és ezekre történő reagálást.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez ok-okozat viszony vagy cél meghatározását.

- Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez emlékezést és nem emlékezést.

- Összekapcsolja a mondatokat megfelelő kötőszavakkal, így követhető leírást ad, vagy nem kronológiai sorrendben lévő eseményeket is elbeszél.

- A kohéziós eszközök szélesebb körét alkalmazza szóbeli vagy írásbeli megnyilatkozásainak érthetőbb, koherensebb szöveggé szervezéséhez.

- Több különálló elemet összekapcsol összefüggő lineáris szempontsorrá.

- Képes rendszerezni kommunikációját: jelzi szándékát, kezdeményez, összefoglal és lezár.

- Használ kiemelést, hangsúlyozást, helyesbítést.

- Körülírással közvetíti a jelentéstartalmat, ha a megfelelő szót nem ismeri.

- Ismert témákban a szövegösszefüggés alapján kikövetkezteti az ismeretlen szavak jelentését, megérti az ismeretlen szavakat is tartalmazó mondat jelentését.

- Félreértéshez vezető hibáit kijavítja, ha beszédpartnere jelzi a problémát; a kommunikáció megszakadása esetén más stratégiát alkalmazva újrakezdi a mondandóját.

- A társalgás vagy eszmecsere menetének fenntartásához alkalmazza a rendelkezésére álló nyelvi és stratégiai eszközöket.

- Nem értés esetén képes a tartalom tisztázására.

- Mondanivalóját kifejti kevésbé ismerős helyzetekben is nyelvi eszközök széles körének használatával.

- A tanult nyelvi elemeket adaptálni tudja kevésbé begyakorolt helyzetekhez is.

- Szóbeli és írásbeli közlései során változatos nyelvi struktúrákat használ.

- A tanult nyelvi funkciókat és nyelvi eszköztárát életkorának megfelelő élethelyzetekben megfelelően alkalmazza.

- Szociokulturális ismeretei (például célnyelvi társadalmi szokások, testbeszéd) már lehetővé teszik azt, hogy társasági szempontból is megfelelő kommunikációt folytasson.

- Szükség esetén eltér az előre elgondoltaktól, és mondandóját a beszédpartnerekhez, hallgatósághoz igazítja.

- Az ismert nyelvi elemeket vizsgahelyzetben is használja.

- Megértést nehezítő hibáit önállóan javítani tudja.

- Nyelvtanulási céljai érdekében alkalmazza a tanórán kívüli nyelvtanulási lehetőségeket.

- Célzottan keresi az érdeklődésének megfelelő autentikus szövegeket tanórán kívül is, ismeretszerzésre és szórakozásra.

- Felhasználja a célnyelvű, legfőbb hazai és nemzetközi híreket ismeretszerzésre és szórakozásra.

- Használ célnyelvi elemeket más tudásterületen megcélzott tartalmakból.

- Használ célnyelvi tartalmakat ismeretszerzésre.

- Használ ismeretterjesztő anyagokat nyelvtudása fejlesztésére.

- Hibáit az esetek többségében önállóan is képes javítani.

- Hibáiból levont következtetéseire többnyire épít nyelvtudása fejlesztése érdekében.

- Egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki magának.

- Megfogalmaz hosszú távú nyelvtanulási célokat saját maga számára.

- Nyelvtanulási céljai érdekében tudatosabban foglalkozik a célnyelvvel.

- Céljai eléréséhez megtalálja és használja a megfelelő eszközöket, módokat.

- Céljai eléréséhez társaival párban és csoportban is együttműködik.

- Beazonosít nyelvtanulási célokat és ismeri az ezekhez tartozó nyelvtanulási és nyelvhasználati stratégiákat.

- Használja a nyelvtanulási és nyelvhasználati stratégiákat nyelvtudása fenntartására és fejlesztésére.

- Hatékonyan alkalmazza a tanult nyelvtanulási és nyelvhasználati stratégiákat.

- Céljai eléréséhez önszabályozóan is dolgozik.

- Az első idegen nyelvből sikeres érettségit tesz legalább középszinten.

- Nyelvi haladását fel tudja mérni.

- Használ önértékelési módokat nyelvtudása felmérésére.

- Egyre tudatosabban használja az ön-, tanári, vagy társai értékelését nyelvtudása fenntartására és fejlesztésére.

- Használja az ön-, tanári, vagy társai értékelését nyelvtudása fenntartására és fejlesztésére.

- Hiányosságait, hibáit felismeri, azokat egyre hatékonyabban kompenzálja, javítja a tanult stratégiák felhasználásával.

- Nyelvtanulási céljai érdekében él a valós nyelvhasználati lehetőségekkel.

- Használja a célnyelvet életkorának és nyelvi szintjének megfelelő aktuális témákban és a hozzájuk tartozó szituációkban.

- Az ismert nyelvi elemeket vizsgahelyzetben is használja.

- Beszéd- és írásprodukcióját tudatosan megtervezi, hiányosságait igyekszik kompenzálni.

- Nyelvi produkciójában és recepciójában önállóságot mutat, és egyre kevesebb korlát akadályozza.

- Törekszik releváns digitális tartalmak használatára beszédkészségének, szókincsének és kiejtésének továbbfejlesztése céljából.

- Digitális eszközöket és felületeket is magabiztosan használ nyelvtudása fejlesztésére.

- Digitális eszközöket és felületeket is használ a célnyelven ismeretszerzésre és szórakozásra.

- Alkalmazza a célnyelvi kultúráról megszerzett ismereteit informális kommunikációjában,

- Ismeri a célnyelvi országok történelmének és jelenének legfontosabb vonásait,

- Tájékozott a célnyelvi országok jellemzőiben és kulturális sajátosságaiban.

- Tájékozott, és alkalmazni is tudja a célnyelvi országokra jellemző alapvető érintkezési és udvariassági szokásokat.

- Ismeri és keresi a főbb hasonlóságokat és különbségeket saját anyanyelvi és a célnyelvi közösség szokásai, értékei, attitűdjei és meggyőződései között.

- Átadja célnyelven a magyar értékeket.

- Ismeri a célnyelvi és saját hazájának kultúrája közötti hasonlóságokat és különbségeket.

- Interkulturális tudatosságára építve felismeri a célnyelvi és saját hazájának kultúrája közötti hasonlóságokat és különbségeket, és a magyar értékek átadására képessé válik.

- Környezetének kulturális értékeit célnyelven közvetíti.

- Kikövetkezteti a célnyelvi kultúrákhoz kapcsolódó egyszerű, ismeretlen nyelvi elemeket.

- A célnyelvi kultúrákhoz kapcsolódó tanult nyelvi elemeket magabiztosan használja.

- Interkulturális ismeretei segítségével társasági szempontból is megfelelő kommunikációt folytat írásban és szóban.

- Felismeri a legfőbb hasonlóságokat és különbségeket az ismert nyelvi változatok között.

- Megfogalmaz főbb hasonlóságokat és különbségeket az ismert nyelvi változatok között.

- Alkalmazza a nyelvi változatokról megszerzett ismereteit informális kommunikációjában.

- Megérti a legfőbb nyelvi dialektusok egyes elemeit is tartalmazó szóbeli közléseket.

- Digitális eszközökön és csatornákon keresztül is alkot szöveget szóban és írásban.

- Digitális eszközökön és csatornákon keresztül is megérti az ismert témához kapcsolódó írott vagy hallott szövegeket.

- Digitális eszközökön és csatornákon keresztül is alkalmazza az ismert témához kapcsolódó írott vagy hallott szövegeket.

- Digitális eszközökön és csatornákon keresztül is folytat célnyelvi interakciót az ismert nyelvi eszközök segítségével.

- Digitális eszközökön és csatornákon keresztül is folytat a természeteshez közelítő célnyelvi interakciót az ismert nyelvi eszközök segítségével.

- Digitális eszközökön és csatornákon keresztül is megfelelő nyelvi eszközökkel alkot szöveget szóban és írásban.

- Alkalmazza az életkorának és érdeklődésének megfelelő digitális műfajok főbb jellemzőit.

## Komplex természettudomány

### 9. évfolyamon

- Ismeri a tudományos kutatás alapszabályait és azokat alkalmazza
- Önálló tudományos kutatást tervez meg és végez el
- Önálló kutatása összeállításakor tudományos modelleket használ
- Tudományos kutatások során elvégzi a megszerzett adatok feldolgozását és értelmezését
- Érti a tudomány szerepét és szükségszerűségét a társadalmi folyamatok alakításában
- Hiteles források felhasználásával egy tudományos probléma kritikus elemzését adja a megszerzett információk alapján
- Elméleti és gyakorlati eszközöket választ és alkalmaz egy adott tudományos probléma ismertetéséhez
- Tudományos kutatási eredményeket érthetően mutat be digitális eszközök segítségével
- Önállóan és kiscsoportban biztonságosan végez természettduományos kísérleteket
- Felismeri a saját és társai által végzett tudományos kísérletek etikai és társadalmi vonatkozásait
- Ismeri és alkalmazza az energia felhasználásának és átalakulásának elméleti és gyakorlati lehetőségeit (energiaáramláson alapuló ökoszisztémák, a föld saját energiaforrásai stb.)
- Felismeri és kísérletei során alkalmazza azt a tudást, hogy az anyag atomi természete határozza meg a fizikai és kémiai tulajdonságokat és azok kölcsönhatásából eredő módosulását. (Példák kísérletekre: kémiai reakciók, molekuláris biológiai, anyagok körforgása a különböző ökoszisztémákban)
- Felismeri és kísérletei során alkalmazza azt a tudást, hogy a természet ismert rendszerei előrejelezhető módon változnak (evolúció, klímaváltozás, földtörténeti korok, Föld felszíni változása, tektonikus mozgások, ember környezeti hatásai stb.)
- Felismeri és kísérletei során alkalmazza azt a tudást, hogy a tárgyak mozgása előrejelezhető (erők, égitestek mozgása, molekuláris mozgások, hangok, fények mozgása)
- Felismeri és kísérletei során alkalmazza azt a tudást, hogy a világ megismerésének egyik alapja a szerveződési szintek és típusok megértése (periódusos rendszer, sjetszintű szerveződések, állat és növényvilág rendszertana, a világegyetem szervező elvei) -
-
- Ismeri a föld népességgének aktuális kihívásait beleértve azok társadalmi és egészségügyi kockázatait (betegség-megelőzés, járványok, élelmezés)
- Ismeri a hulladékgazdálkodás aktuális kihívásait
- Ismeri az energia fogalmát és az egyéni és társadalmi energiafelhasználás különböző lehetőségeit
- Megkülönbözteti egymástól a természetes és mesterséges anyagokat és felismeri, hogy miként állapítható egy anyag összetétele
- Ismeri a városi és falusi életmód és életterek közötti különbségeket, azok környezetre gyakorolt hatását
- Használja a regionalitás fogalmát és érti annak szerepét a gazdasági folyamatok alakulásában
- Ismeri a levegő és a víz fizikai és kémiai jellemzőit, felismeri ezek élettani hatásait
- Tudja, hogy milyen vízkészletekkel rendelkezik a Föld és azok felhasználásának milyen hatása van a környezeti, ipari, turisztikai és szállítmányozási folyamatokra
- Ismeri a növények tápanyagigényét és fejlődésük alapjait
- Ismeri a vadon élő állatközösségeket fenyegető veszélyeket és azt a kihívást, amit ez az emberekre gyakorol
- Ismeri a különböző kultúrák eltérő gazdasági termelési szokásait (növénytermesztés, állattartás)
- Ismeri a talaj természetét és annak megművelésének különböző formáit
- Ismeri a föld légkörét befolyásoló globális folyamatokat (tengerek és szelek áramlása, klímaváltozás, üvegházhatású gázok)
- Ismeri az emberi táplálkozás során hasznos ételeket, megkülönbözteti a gyógyító és a káros anyagokat, önállóan tud egészséges ételt készíteni
- Ismeri az emberi agy alapvető működési szabályait és az azt befolyásoló tényezőket
- Tisztában van az Univerzum létrejöttének ma ismert elméletével, valamint a Naprendszer kialakulásának folyamatával
- Ismeri a tanulás és az emberi kommunikáció biológiai alapjait
- Ismeri az emberi szervezet egészségét alapvetően befolyásoló tényezőket, a stressz, az öröklött hajlamok és genetikai tulajdonságok, valamint a környezeti hatások szerepét
- Ismeri az emberi szexualitás kulturális, társadalmi és biológiai alapjait. Önálló véleménye van a nemi szerepek fontosságáról, érti a nemi identitás komplex jellegét.
- Ismeri a hálózatok és a hálózatkutatás szerepét modern világunkban, az életközösségek, a sejtszintű gondolkodás és az információs technológiák területén.
- Ismeri a genetikai információ átadásának alapvető szabályait.

## Magyar nyelv és irodalom

### 9-12. évfolyamon

- Az anyanyelvről szerzett ismereteit alkalmazva képes a kommunikációjában a megfelelő nyelvváltozat kiválasztására, használatára.

- Felismeri a kommunikáció zavarait, kezelésükre stratégiát dolgoz ki.

- Felismeri és elemzi a tömegkommunikáció befolyásoló eszközeit, azok céljait és hatásait.

- Reflektál saját kommunikációjára, szükség esetén változtat azon.

- Ismeri az anyanyelvét, annak szerkezeti felépítését, nyelvhasználata tudatos és helyes.

- Ismeri a magyar nyelv hangtanát, alaktanát, szófajtanát, mondattanát, ismeri és alkalmazza a tanult elemzési eljárásokat.

- Felismeri és megnevezi a magyar és a tanult idegen nyelv közötti hasonlóságokat és eltéréseket.

- Ismeri a szöveg fogalmát, jellemzőit, szerkezeti sajátosságait, valamint a különféle szövegtípusokat és megjelenésmódokat.

- Felismeri és alkalmazza a szövegösszetartó grammatikai és jelentésbeli elemeket, szövegépítése arányos és koherens.

- Ismeri a stílus fogalmát, a stíluselemeket, a stílushatást, a stíluskorszakokat, stílusrétegeket, ismereteit a szöveg befogadása és alkotása során alkalmazza.

- Szövegelemzéskor felismeri az alakzatokat és a szóképeket, értelmezi azok hatását, szerepét, megnevezi típusaikat.

- Ismeri a nyelvhasználatban előforduló különféle nyelvváltozatokat (nyelvjárások, csoportnyelvek, rétegnyelvek), összehasonlítja azok főbb jellemzőit.

- Alkalmazza az általa tanult nyelvi, nyelvtani, helyesírási, nyelvhelyességi ismereteket.

- A retorikai ismereteit a gyakorlatban is alkalmazza.

- Ismeri és érti a nyelvrokonság fogalmát, annak kritériumait.

- Ismeri a magyar nyelv eredetének hipotéziseit, és azok tudományosan megalapozott bizonyítékait.

- Érti, hogy nyelvünk a történelemben folyamatosan változik, ismeri a magyar nyelvtörténet nagy korszakait, kiemelkedő jelentőségű nyelvemlékeit.

- Ismeri a magyar nyelv helyesírási, nyelvhelyességi szabályait.

- Tud helyesen írni, szükség esetén nyomtatott és digitális helyesírási segédleteket használ.

- Etikusan és kritikusan használja a hagyományos, papír alapú, illetve a világhálón található és egyéb digitális adatbázisokat.

- Elolvassa a kötelező olvasmányokat, és saját örömére is olvas.

- Felismeri és elkülöníti a műnemeket, illetve a műnemekhez tartozó műfajokat, megnevezi azok poétikai és retorikai jellemzőit.

- Megérti, elemzi az irodalmi mű jelentésszerkezetének szintjeit.

- Értelmezésében felhasználja irodalmi és művészeti, történelmi, művelődéstörténeti ismereteit.

- Összekapcsolja az irodalmi művek szerkezeti felépítését, nyelvi sajátosságait azok tartalmával és értékszerkezetével.

- Az irodalmi mű értelmezése során figyelembe veszi a mű keletkezéstörténeti hátterét, a műhöz kapcsolható filozófiai, eszmetörténeti szempontokat is.

- Összekapcsolja az irodalmi művek szövegének lehetséges értelmezéseit azok társadalmi-történelmi szerepével, jelentőségével.

- Összekapcsol irodalmi műveket különböző szempontok alapján (motívumok, történelmi, erkölcsi kérdésfelvetések, művek és parafrázisaik).

- Összehasonlít egy adott irodalmi művet annak adaptációival (film, festmény, zenemű, animáció, stb.), összehasonlításkor figyelembe veszi az adott művészeti ágak jellemző tulajdonságait.

- Epikai és drámai művekben önállóan értelmezi a cselekményszálak, a szerkezet, az időszerkezet (lineáris, nem lineáris), a helyszínek és a jellemek összefüggéseit.

- Epikai és drámai művekben rendszerbe foglalja a szereplők viszonyait, valamint összekapcsolja azok motivációját és cselekedeteit.

- Epikai művekben értelmezi a különböző elbeszélésmódok szerepét (tudatábrázolás, egyenes és függő beszéd, mindentudó és korlátozott elbeszélő stb.).

- A drámai mű értelmezésében alkalmazza az általa tanult drámaelméleti és drámatörténeti fogalmakat (pl. analitikus és abszurd dráma, epikus színház, elidegenedés).

- A líra mű értelmezésében alkalmazza az általa tanult líraelméleti és líratörténeti fogalmakat (pl. lírai én, beszédhelyzetek, beszédmódok, ars poetica, szereplíra).

- A tantárgyhoz kapcsolódó fogalmakkal bemutatja a lírai mű hangulati és hangnemi sajátosságait, hivatkozik a mű verstani felépítésére.

- Szükség esetén a mű értelmezéséhez felhasználja történeti ismereteit.

- A mű értelmezésében összekapcsolja a szöveg poétikai tulajdonságait a mű nemzeti hagyományban betöltött szerepével.

- Tájékozottságot szerez régiója magyar irodalmáról.

- Tanulmányai során ismereteket szerez a kulturális intézmények (múzeum, könyvtár, színház) és a nyomtatott, illetve digitális formában megjelenő kulturális folyóiratok, adatbázisok működéséről.

- Különböző megjelenésű, típusú, műfajú, korú és összetettségű szövegeket olvas, értelmez.

- A különböző olvasási típusokat és a szövegfeldolgozási stratégiákat a szöveg típusának és az olvasás céljának megfelelően választja ki és kapcsolja össze.

- A megismert szöveg tartalmi és nyelvi minőségéről érvekkel alátámasztott véleményt alkot.

- Hosszabb terjedelmű szöveg alapján többszintű vázlatot vagy részletes gondolattérképet készít.

- Azonosítja a szöveg szerkezeti elemeit, és figyelembe veszi azok funkcióit a szöveg értelmezésekor.

- Egymással összefüggésben értelmezi a szöveg tartalmi elemeit és a hozzá kapcsolódó illusztrációkat, ábrákat.

- Különböző típusú és célú szövegeket hallás alapján értelmez és megfelelő stratégia alkalmazásával értékel és összehasonlít.

- Összefüggő szóbeli szöveg (előadás, megbeszélés, vita) alapján önállóan vázlatot készít.

- Felismeri és értelmezésében figyelembe veszi a hallott és az írott szövegek közötti funkcionális és stiláris különbségeket.

- Folyamatos és nem folyamatos, hagyományos és digitális szövegeket olvas és értelmez maga által választott releváns szempontok alapján.

- Feladatai megoldásához önálló kutatómunkát végez nyomtatott és digitális forrásokban, ezek eredményeit szintetizálja.

- Felismeri és értelmezi a szövegben a kétértelműséget és a félrevezető információt, valamint elemzi és értelmezi a szerző szándékát.

- Megtalálja a közös és eltérő jellemzőket a hagyományos és a digitális technikával előállított, tárolt szövegek között, és véleményt formál azok sajátosságairól.

- Törekszik arra, hogy a különböző típusú, stílusú és regiszterű szövegekben megismert, számára új kifejezéseket beépítse szókincsébe, azokat adekvát módon használja.

- Önállóan értelmezi az ismeretlen kifejezéseket a szövegkörnyezet vagy digitális, illetve nyomtatott segédeszközök használatával.

- Ismeri a tanult tantárgyak, tudományágak szakszókincsét, azokat a beszédhelyzetnek megfelelően használja.

- Megadott szempontrendszer alapján szóbeli feleletet készít.

- Képes eltérő műfajú szóbeli szövegek alkotására: felelet, kiselőadás, hozzászólás, felszólalás.

- Rendelkezik korának megfelelő retorikai ismeretekkel.

- Felismeri és megnevezi a szóbeli előadásmód hatáskeltő eszközeit, hatékonyan alkalmazza azokat.

- Írásbeli és szóbeli nyelvhasználata, stílusa az adott kommunikációs helyzetnek megfelelő. írásképe tagolt, beszéde érthető, artikulált.

- A tanult szövegtípusoknak megfelelő tartalommal és szerkezettel önállóan alkot különféle írásbeli szövegeket.

- Az írásbeli szövegalkotáskor alkalmazza a tanult szerkesztési, stilisztikai ismereteket és a helyesírási szabályokat.

- Érvelő esszét alkot megadott szempontok vagy szövegrészletek alapján.

- Ismeri, érti és etikusan alkalmazza a hagyományos, digitális és multimédiás szemléltetést.

- Különböző, a munka világában is használt hivatalos szövegeket alkot hagyományos és digitális felületeken (pl. kérvény, beadvány, nyilatkozat, egyszerű szerződés, meghatalmazás, önéletrajz, motivációs levél).

- Megadott vagy önállóan kiválasztott szempontok alapján az irodalmi művekről elemző esszét ír.

- A kötelező olvasmányokat elolvassa, és saját örömére is olvas.

- Tudatosan keresi a történeti és esztétikai értékekkel rendelkező olvasmányokat, műalkotásokat.

- Olvasmányai kiválasztásakor figyelembe veszi az alkotások kulturális regiszterét.

- Társai érdeklődését figyelembe véve ajánl olvasmányokat.

- Választott olvasmányaira is vonatkoztatja a tanórán megismert kontextusteremtő eljárások tanulságait.

- Önismeretét irodalmi művek révén fejleszti.

- Részt vesz irodalmi mű kreatív feldolgozásában, bemutatásában (pl. animáció, dramaturgia, átirat).

- A környező világ jelenségeiről, szövegekről, műalkotásokról véleményt alkot, és azt érvekkel támasztja alá.

- Megnyilvánulásaiban, a vitákban alkalmazza az érvelés alapvető szabályait.

- Vitahelyzetben figyelembe veszi mások álláspontját, a lehetséges ellenérveket is.

- Feladatai megoldásához önálló kutatómunkát végez nyomtatott és digitális forrásokban, a források tartalmát mérlegelő módon gondolja végig.

- A feladatokat komplex szempontoknak megfelelően oldja meg, azokat kiegészíti saját szempontjaival.

- A kommunikációs helyzetnek és a célnak megfelelően tudatosan alkalmazza a beszélt és írott nyelvet, reflektál saját és társai nyelvhasználatára.

## Matematika

### 9-12. évfolyamon

- Ismeretei segítségével, a megfelelő modell alkalmazásával megold hétköznapi és matematikai problémákat, a megoldást ellenőrzi és értelmezi.

- Megérti a környezetében jelen lévő logikai, mennyiségi, függvényszerű, térbeli és statisztikai kapcsolatokat.

- Sejtéseket fogalmaz meg és logikus lépésekkel igazolja azokat.

- Adatokat gyűjt, rendez, ábrázol, értelmez.

- A matematikai szakkifejezéseket és jelöléseket helyesen használja írásban és szóban egyaránt.

- Megérti a hallott és olvasott matematikai tartalmú szövegeket.

- Felismeri a matematika különböző területei közötti kapcsolatokat.

- A matematika tanulása során digitális eszközöket és különböző információforrásokat használ.

- A matematikát más tantárgyakhoz kapcsolódó témákban is használja.

- Matematikai ismereteit alkalmazza a pénzügyi tudatosság területét érintő feladatok megoldásában.

- Adott halmazt diszjunkt részhalmazaira bont, osztályoz.

- Matematikai vagy hétköznapi nyelven megfogalmazott szövegből a matematikai tartalmú információkat kigyűjti, rendszerezi.

- Felismeri a matematika különböző területei közötti kapcsolatot.

- Látja a halmazműveletek és a logikai műveletek közötti kapcsolatokat.

- Halmazokat különböző módokon megad.

- Halmazokkal műveleteket végez, azokat ábrázolja és értelmezi.

- Véges halmazok elemszámát meghatározza.

- Alkalmazza a logikai szita elvét.

- Adott állításról eldönti, hogy igaz vagy hamis.

- Alkalmazza a tagadás műveletét egyszerű feladatokban.

- Ismeri és alkalmazza az „és", a (megengedő és kizáró) „vagy" logikai jelentését.

- Megfogalmazza adott állítás megfordítását.

- Megállapítja egyszerű „ha \... , akkor \..." és „akkor és csak akkor" típusú állítások logikai értékét.

- Helyesen használja a „minden" és „van olyan" kifejezéseket.

- Tud egyszerű állításokat indokolni és tételeket bizonyítani.

- Megold sorba rendezési és kiválasztási feladatokat.

- Konkrét szituációkat szemléltet és egyszerű feladatokat megold gráfok segítségével.

- Adott problémához megoldási stratégiát, algoritmust választ, készít.

- A problémának megfelelő matematikai modellt választ, alkot.

- A kiválasztott modellben megoldja a problémát.

- A modellben kapott megoldását az eredeti problémába visszahelyettesítve értelmezi, ellenőrzi és az észszerűségi szempontokat figyelembe véve adja meg válaszát.

- Geometriai szerkesztési feladatoknál vizsgálja és megállapítja a szerkeszthetőség feltételeit.

- Ismeri és alkalmazza a következő egyenletmegoldási módszereket: mérlegelv, grafikus megoldás, szorzattá alakítás.

- Megold elsőfokú egyismeretlenes egyenleteket és egyenlőtlenségeket, elsőfokú kétismeretlenes egyenletrendszereket.

- Megold másodfokú egyismeretlenes egyenleteket és egyenlőtlenségeket; ismeri és alkalmazza a diszkriminánst, a megoldóképletet és a gyöktényezős alakot.

- Megold egyszerű, a megfelelő definíció alkalmazását igénylő exponenciális egyenleteket, egyenlőtlenségeket.

- Egyenletek megoldását behelyettesítéssel, értékkészlet-vizsgálattal ellenőrzi.

- Ismeri a mérés alapelvét, alkalmazza konkrét alap- és származtatott mennyiségek esetén.

- Ismeri a hosszúság, terület, térfogat, űrtartalom, idő mértékegységeit és az átváltási szabályokat. származtatott mértékegységeket átvált.

- Sík- és térgeometriai feladatoknál a problémának megfelelő mértékegységben adja meg válaszát.

- Ismeri és alkalmazza az oszthatóság alapvető fogalmait.

- Összetett számokat felbont prímszámok szorzatára.

- Meghatározza két természetes szám legnagyobb közös osztóját és legkisebb közös többszörösét, és alkalmazza ezeket egyszerű gyakorlati feladatokban.

- Ismeri és alkalmazza az oszthatósági szabályokat.

- Érti a helyi értékes írásmódot 10-es és más alapú számrendszerekben.

- Ismeri a számhalmazok épülésének matematikai vonatkozásait a természetes számoktól a valós számokig.

- A kommutativitás, asszociativitás, disztributivitás műveleti azonosságokat helyesen alkalmazza különböző számolási helyzetekben.

- Racionális számokat tizedes tört és közönséges tört alakban is felír.

- Ismer példákat irracionális számokra.

- Ismeri a valós számok és a számegyenes kapcsolatát.

- Ismeri és alkalmazza az abszolút érték, az ellentett és a reciprok fogalmát.

- A számolással kapott eredményeket nagyságrendileg megbecsüli, és így ellenőrzi az eredményt.

- Valós számok közelítő alakjaival számol, és megfelelően kerekít.

- Ismeri és alkalmazza a négyzetgyök fogalmát és azonosságait.

- Ismeri és alkalmazza az n-edik gyök fogalmát.

- Ismeri és alkalmazza a normálalak fogalmát.

- Ismeri és alkalmazza az egész kitevőjű hatvány fogalmát és a hatványozás azonosságait.

- Ismeri és alkalmazza a racionális kitevőjű hatvány fogalmát és a hatványozás azonosságait.

- Ismeri és alkalmazza a logaritmus fogalmát.

- Műveleteket végez algebrai kifejezésekkel.

- Ismer és alkalmaz egyszerű algebrai azonosságokat.

- Átalakít algebrai kifejezéseket összevonás, szorzattá alakítás, nevezetes azonosságok alkalmazásával.

- Ismeri és alkalmazza az egyenes és a fordított arányosságot.

- Ismeri és alkalmazza a százalékalap, -érték, -láb, -pont fogalmát.

- Ismeri és használja a pont, egyenes, sík (térelemek) és szög fogalmát.

- Ismeri és feladatmegoldásban alkalmazza a térelemek kölcsönös helyzetét, távolságát és hajlásszögét.

- Ismeri és alkalmazza a nevezetes szögpárok tulajdonságait.

- Ismeri az alapszerkesztéseket, és ezeket végre tudja hajtani hagyományos vagy digitális eszközzel.

- Ismeri és alkalmazza a háromszögek oldalai, szögei, oldalai és szögei közötti kapcsolatokat; a speciális háromszögek tulajdonságait.

- Ismeri és alkalmazza a háromszög nevezetes vonalaira, pontjaira és köreire vonatkozó fogalmakat és tételeket.

- Ismeri és alkalmazza a pitagorasz-tételt és megfordítását.

- Kiszámítja háromszögek területét.

- Ismeri és alkalmazza speciális négyszögek tulajdonságait, területüket kiszámítja.

- Ismeri és alkalmazza a szabályos sokszög fogalmát; kiszámítja a konvex sokszög belső és külső szögeinek összegét.

- Átdarabolással kiszámítja sokszögek területét.

- Ki tudja számolni a kör és részeinek kerületét, területét.

- Ismeri a kör érintőjének fogalmát, kapcsolatát az érintési pontba húzott sugárral.

- Ismeri és alkalmazza a thalész-tételt és megfordítását.

- Ismer példákat geometriai transzformációkra.

- Ismeri és alkalmazza a síkbeli egybevágósági transzformációkat és tulajdonságaikat; alakzatok egybevágóságát.

- Ismeri és alkalmazza a középpontos hasonlósági transzformációt, a hasonlósági transzformációt és az alakzatok hasonlóságát.

- Ismeri és alkalmazza a hasonló síkidomok kerületének és területének arányára vonatkozó tételeket.

- Megszerkeszti egy alakzat tengelyes, illetve középpontos tükörképét, pont körüli elforgatottját, párhuzamos eltoltját hagyományosan és digitális eszközzel.

- Ismeri a vektorokkal kapcsolatos alapvető fogalmakat.

- Ismer és alkalmaz egyszerű vektorműveleteket.

- Alkalmazza a vektorokat feladatok megoldásában.

- Ismeri hegyesszögek szögfüggvényeinek definícióját a derékszögű háromszögben.

- Ismeri tompaszögek szögfüggvényeinek származtatását a hegyesszögek szögfüggvényei alapján.

- Ismeri a hegyes- és tompaszögek szögfüggvényeinek összefüggéseit.

- Alkalmazza a szögfüggvényeket egyszerű geometriai számítási feladatokban.

- A szögfüggvény értékének ismeretében meghatározza a szöget.

- Ismeri és alkalmazza a szinusz- és a koszinusztételt.

- Ismeri és alkalmazza a hasáb, a henger, a gúla, a kúp, a gömb, a csonkagúla, a csonkakúp (speciális testek) tulajdonságait.

- Lerajzolja a kocka, téglatest, egyenes hasáb, egyenes körhenger, egyenes gúla, forgáskúp hálóját.

- Kiszámítja a speciális testek felszínét és térfogatát egyszerű esetekben.

- Ismeri és alkalmazza a hasonló testek felszínének és térfogatának arányára vonatkozó tételeket.

- Megad pontot és vektort koordinátáival a derékszögű koordináta-rendszerben.

- Koordináta-rendszerben ábrázol adott feltételeknek megfelelő ponthalmazokat.

- Koordináták alapján számításokat végez szakaszokkal, vektorokkal.

- Ismeri és alkalmazza az egyenes egyenletét.

- Egyenesek egyenletéből következtet az egyenesek kölcsönös helyzetére.

- Kiszámítja egyenesek metszéspontjainak koordinátáit az egyenesek egyenletének ismeretében.

- Megadja és alkalmazza a kör egyenletét a kör sugarának és a középpont koordinátáinak ismeretében.

- Megad hétköznapi életben előforduló hozzárendeléseket.

- Adott képlet alapján helyettesítési értékeket számol, és azokat táblázatba rendezi.

- Táblázattal megadott függvény összetartozó értékeit ábrázolja koordináta-rendszerben.

- Képlettel adott függvényt hagyományosan és digitális eszközzel ábrázol.

- Adott értékkészletbeli elemhez megtalálja az értelmezési tartomány azon elemeit, amelyekhez a függvény az adott értéket rendeli.

- A grafikonról megállapítja függvények alapvető tulajdonságait.

- Számtani és mértani sorozatokat adott szabály alapján felír, folytat.

- A számtani, mértani sorozat n-edik tagját felírja az első tag és a különbség (differencia)/hányados (kvóciens) ismeretében.

- A számtani, mértani sorozatok első n tagjának összegét kiszámolja.

- Mértani sorozatokra vonatkozó ismereteit használja gazdasági, pénzügyi, természettudományi és társadalomtudományi problémák megoldásában.

- Adott cél érdekében tudatos adatgyűjtést és rendszerezést végez.

- Hagyományos és digitális forrásból származó adatsokaság alapvető statisztikai jellemzőit meghatározza, értelmezi és értékeli.

- Adatsokaságból adott szempont szerint oszlop- és kördiagramot készít hagyományos és digitális eszközzel.

- Ismeri és alkalmazza a sodrófa (box-plot) diagramot adathalmazok jellemzésére, összehasonlítására.

- Felismer grafikus manipulációkat diagramok esetén.

- Tapasztalatai alapján véletlen jelenségek jövőbeni kimenetelére észszerűen tippel.

- Ismeri és alkalmazza a klasszikus valószínűségi modellt és a laplace-képletet.

- Véletlen kísérletek adatait rendszerezi, relatív gyakoriságokat számol, nagy elemszám esetén számítógépet alkalmaz.

- Konkrét valószínűségi kísérletek esetében az esemény, eseménytér, elemi esemény, relatív gyakoriság, valószínűség, egymást kizáró események, független események fogalmát megkülönbözteti és alkalmazza.

- Ismeri és egyszerű esetekben alkalmazza a valószínűség geometriai modelljét.

- Meghatározza a valószínűséget visszatevéses, illetve visszatevés nélküli mintavétel esetén.

- A megfelelő matematikai tankönyveket, feladatgyűjteményeket, internetes tartalmakat értőn olvassa, a matematikai tartalmat rendszerezetten kigyűjti és megérti.

- A matematikai fogalmakat és jelöléseket megfelelően használja.

- Önállóan kommunikál matematika tartalmú feladatokkal kapcsolatban.

- Matematika feladatok megoldását szakszerűen prezentálja írásban és szóban a szükséges alapfogalmak, azonosságok, definíciók és tételek segítségével.

- Szöveg alapján táblázatot, grafikont készít, ábrát, kapcsolatokat szemléltető gráfot rajzol, és ezeket kombinálva prezentációt készít és mutat be.

- Ismer a tananyaghoz kapcsolódó matematikatörténeti vonatkozásokat.

- Számológép segítségével alapműveletekkel felírható számolási eredményt; négyzetgyököt; átlagot; szögfüggvények értékét, illetve abból szöget; logaritmust; faktoriálist; binomiális együtthatót; szórást meghatároz.

- Digitális környezetben matematikai alkalmazásokkal dolgozik.

- Megfelelő informatikai alkalmazás segítségével szöveget szerkeszt, táblázatkezelő programmal diagramokat készít.

- Ismereteit digitális forrásokból kiegészíti, számítógép segítségével elemzi és bemutatja.

- Prezentációhoz informatív diákat készít, ezeket logikusan és következetesen egymás után fűzi és bemutatja.

- Kísérletezéshez, sejtés megfogalmazásához, egyenlet grafikus megoldásához és ellenőrzéshez dinamikus geometriai, grafikus és táblázatkezelő szoftvereket használ.

- Szerkesztési feladatok euklideszi módon történő megoldásához dinamikus geometriai szoftvert használ.

## Második idegen nyelv

### 9-12. évfolyamon

- Megismerkedik az idegen nyelvvel, a nyelvtanulással és örömmel vesz részt az órákon.

- Bekapcsolódik a szóbeliséget, írást, szövegértést vagy interakciót igénylő alapvető és korának megfelelő játékos, élményalapú élő idegen nyelvi tevékenységekbe.

- Szóban visszaad szavakat, esetleg rövid, nagyon egyszerű szövegeket hoz létre.

- Lemásol, leír szavakat és rövid, nagyon egyszerű szövegeket.

- Követi a szintjének megfelelő, vizuális vagy nonverbális eszközökkel támogatott, ismert célnyelvi óravezetést, utasításokat.

- Felismeri és használja a legegyszerűbb, mindennapi nyelvi funkciókat.

- Elmondja magáról a legalapvetőbb információkat.

- Ismeri az adott célnyelvi kultúrákhoz tartozó országok fontosabb jellemzőit és a hozzájuk tartozó alapvető nyelvi elemeket.

- Törekszik a tanult nyelvi elemek megfelelő kiejtésére.

- Célnyelvi tanulmányain keresztül nyitottabbá, a világ felé érdeklődőbbé válik.

- Megismétli az élőszóban elhangzó egyszerű szavakat, kifejezéseket játékos, mozgást igénylő, kreatív nyelvórai tevékenységek során.

- Lebetűzi a nevét.

- Lebetűzi a tanult szavakat társaival közösen játékos tevékenységek kapcsán, szükség esetén segítséggel.

- Célnyelven megoszt egyedül vagy társaival együttműködésben megszerzett, alapvető információkat szóban, akár vizuális elemekkel támogatva.

- Felismeri az anyanyelvén, illetve a tanult idegen nyelven történő írásmód és betűkészlet közötti különbségeket.

- Ismeri az adott nyelv ábécéjét.

- Lemásol tanult szavakat játékos, alkotó nyelvórai tevékenységek során.

- Megold játékos írásbeli feladatokat a szavak, szószerkezetek, rövid mondatok szintjén.

- Részt vesz kooperatív munkaformában végzett kreatív tevékenységekben, projektmunkában szavak, szószerkezetek, rövid mondatok leírásával, esetleg képi kiegészítéssel.

- Írásban megnevezi az ajánlott tématartományokban megjelölt, begyakorolt elemeket.

- Megérti az élőszóban elhangzó, ismert témákhoz kapcsolódó, verbális, vizuális vagy nonverbális eszközökkel segített rövid kijelentéseket, kérdéseket.

- Beazonosítja az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű szövegben a tanult nyelvi elemeket.

- Kiszűri a lényeget az ismert nyelvi elemeket tartalmazó, nagyon rövid, egyszerű hangzó szövegből.

- Azonosítja a célzott információt a nyelvi szintjének és életkorának megfelelő rövid hangzó szövegben.

- Támaszkodik az életkorának és nyelvi szintjének megfelelő hangzó szövegre az órai alkotó jellegű nyelvi, mozgásos nyelvi és játékos nyelvi tevékenységek során.

- Felismeri az anyanyelv és az idegen nyelv hangkészletét.

- Értelmezi azokat az idegen nyelven szóban elhangzó nyelvórai szituációkat, melyeket anyanyelvén már ismer.

- Felismeri az anyanyelve és a célnyelv közötti legalapvetőbb kiejtésbeli különbségeket.

- Figyel a célnyelvre jellemző hangok kiejtésére.

- Megkülönbözteti az anyanyelvi és a célnyelvi írott szövegben a betű- és jelkészlet közti különbségeket.

- Beazonosítja a célzott információt az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű, a nyelvtanításhoz készült, illetve eredeti szövegben.

- Csendes olvasás keretében feldolgozva megért ismert szavakat tartalmazó, pár szóból vagy mondatból álló, akár illusztrációval támogatott szöveget.

- Megérti a nyelvi szintjének megfelelő, akár vizuális eszközökkel is támogatott írott utasításokat és kérdéseket, és ezekre megfelelő válaszreakciókat ad.

- Kiemeli az ismert nyelvi elemeket tartalmazó, egyszerű, írott, pár mondatos szöveg fő mondanivalóját.

- Támaszkodik az életkorának és nyelvi szintjének megfelelő írott szövegre az órai játékos alkotó, mozgásos vagy nyelvi fejlesztő tevékenységek során, kooperatív munkaformákban.

- Megtapasztalja a közös célnyelvi olvasás élményét.

- Aktívan bekapcsolódik a közös meseolvasásba, a mese tartalmát követi.

- A tanórán begyakorolt, nagyon egyszerű, egyértelmű kommunikációs helyzetekben a megtanult, állandósult beszédfordulatok alkalmazásával kérdez vagy reagál, mondanivalóját segítséggel vagy nonverbális eszközökkel kifejezi.

- Törekszik arra, hogy a célnyelvet eszközként alkalmazza információszerzésre.

- Rövid, néhány mondatból álló párbeszédet folytat, felkészülést követően.

- A tanórán bekapcsolódik a már ismert, szóbeli interakciót igénylő nyelvi tevékenységekbe, a begyakorolt nyelvi elemeket tanári segítséggel a tevékenység céljainak megfelelően alkalmazza.

- Érzéseit egy-két szóval vagy begyakorolt állandósult nyelvi fordulatok segítségével kifejezi, főként rákérdezés alapján, nonverbális eszközökkel kísérve a célnyelvi megnyilatkozást.

- Elsajátítja a tanult szavak és állandósult szókapcsolatok célnyelvi normához közelítő kiejtését tanári minta követése által, vagy autentikus hangzó anyag, digitális technológia segítségével.

- Felismeri és alkalmazza a legegyszerűbb, üdvözlésre és elköszönésre használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Felismeri és alkalmazza a legegyszerűbb, bemutatkozásra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Felismeri és használja a legegyszerűbb, megszólításra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Felismeri és használja a legegyszerűbb, a köszönet és az arra történő reagálás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Felismeri és használja a legegyszerűbb, a tudás és nem tudás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Felismeri és használja a legegyszerűbb, a nem értés, visszakérdezés és ismétlés, kérés kifejezésére használt mindennapi nyelvi funkciókat életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

- Közöl alapvető személyes információkat magáról, egyszerű nyelvi elemek segítségével.

- Új szavak, kifejezések tanulásakor ráismer a már korábban tanult szavakra, kifejezésekre.

- Szavak, kifejezések tanulásakor felismeri, ha új elemmel találkozik és rákérdez, vagy megfelelő tanulási stratégiával törekszik a megértésre.

- A célok eléréséhez társaival rövid feladatokban együttműködik.

- Egy feladat megoldásának sikerességét segítséggel értékelni tudja.

- Felismeri az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is.

- Felhasznál és létrehoz rövid, nagyon egyszerű célnyelvi szövegeket szabadidős tevékenységek során.

- Alapvető célzott információt megszerez a tanult témákban tudásának bővítésére.

- Megismeri a főbb, az adott célnyelvi kultúrákhoz tartozó országok nevét, földrajzi elhelyezkedését, főbb országismereti jellemzőit.

- Ismeri a főbb, célnyelvi kultúrához tartozó, ünnepekhez kapcsolódó alapszintű kifejezéseket, állandósult szókapcsolatokat és szokásokat.

- Megérti a tanult nyelvi elemeket életkorának megfelelő digitális tartalmakban, digitális csatornákon olvasott vagy hallott nagyon egyszerű szövegekben is.

- Létrehoz nagyon egyszerű írott, pár szavas szöveget szóban vagy írásban digitális felületen.

- Szóban és írásban megold változatos kihívásokat igénylő feladatokat az élő idegen nyelven.

- Szóban és írásban létrehoz rövid szövegeket, ismert nyelvi eszközökkel, a korának megfelelő szövegtípusokban.

- Értelmez korának és nyelvi szintjének megfelelő hallott és írott célnyelvi szövegeket az ismert témákban és szövegtípusokban.

- A tanult nyelvi elemek és kommunikációs stratégiák segítségével írásbeli és szóbeli interakciót folytat, valamint közvetít az élő idegen nyelven.

- Kommunikációs szándékának megfelelően alkalmazza a tanult nyelvi funkciókat és a megszerzett szociolingvisztikai, pragmatikai és interkulturális jártasságát.

- Nyelvtudását egyre inkább képes fejleszteni tanórán kívüli helyzetekben is különböző eszközökkel és lehetőségekkel.

- Használ életkorának és nyelvi szintjének megfelelő hagyományos és digitális alapú nyelvtanulási forrásokat és eszközöket.

- Alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra, ismeretszerzésre hagyományos és digitális csatornákon.

- Törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és intonáció megközelítésére.

- Érti a nyelvtudás fontosságát, és motivációja a nyelvtanulásra tovább erősödik.

- Aktívan részt vesz az életkorának és érdeklődésének megfelelő gyermek-, illetve ifjúsági irodalmi alkotások közös előadásában.

- Egyre magabiztosabban kapcsolódik be történetek kreatív alakításába, átfogalmazásába kooperatív munkaformában.

- Elmesél rövid történetet, egyszerűsített olvasmányt egyszerű nyelvi eszközökkel, önállóan, a cselekményt lineárisan összefűzve.

- Egyszerű nyelvi eszközökkel, felkészülést követően röviden, összefüggően beszél az ajánlott tématartományokhoz tartozó témákban, élőszóban és digitális felületen.

- Képet jellemez röviden, egyszerűen, ismert nyelvi fordulatok segítségével, segítő tanári kérdések alapján, önállóan.

- Változatos, kognitív kihívást jelentő szóbeli feladatokat old meg önállóan vagy kooperatív munkaformában, a tanult nyelvi eszközökkel, szükség szerint tanári segítséggel, élőszóban és digitális felületen.

- Megold játékos és változatos írásbeli feladatokat rövid szövegek szintjén.

- Rövid, egyszerű, összefüggő szövegeket ír a tanult nyelvi szerkezetek felhasználásával az ismert szövegtípusokban, az ajánlott tématartományokban.

- Rövid szövegek írását igénylő kreatív munkát hoz létre önállóan.

- Rövid, összefüggő, papíralapú vagy ikt-eszközökkel segített írott projektmunkát készít önállóan vagy kooperatív munkaformákban.

- A szövegek létrehozásához nyomtatott, illetve digitális alapú segédeszközt, szótárt használ.

- Megérti a szintjének megfelelő, kevésbé ismert elemekből álló, nonverbális vagy vizuális eszközökkel támogatott célnyelvi óravezetést és utasításokat, kérdéseket.

- Értelmezi az életkorának és nyelvi szintjének megfelelő, egyszerű, hangzó szövegben a tanult nyelvi elemeket.

- Értelmezi az életkorának megfelelő, élőszóban vagy digitális felületen elhangzó szövegekben a beszélők gondolatmenetét.

- Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó, élőszóban vagy digitális felületen elhangzó rövid szöveg tartalmát.

- Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő, élőszóban vagy digitális felületen elhangzó szövegből, és azokat összekapcsolja egyéb ismereteivel.

- Alkalmazza az életkorának és nyelvi szintjének megfelelő hangzó szöveget a változatos nyelvórai tevékenységek és a feladatmegoldás során.

- Értelmez életkorának megfelelő nyelvi helyzeteket hallott szöveg alapján.

- Felismeri a főbb, életkorának megfelelő hangzószöveg-típusokat.

- Hallgat az érdeklődésének megfelelő autentikus szövegeket elektronikus, digitális csatornákon, tanórán kívül is, szórakozásra vagy ismeretszerzésre.

- Értelmezi az életkorának megfelelő szituációkhoz kapcsolódó, írott szövegekben megjelenő összetettebb információkat.

- Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó rövid írott szöveg tartalmát.

- Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő szövegből, és azokat összekapcsolja más iskolai vagy iskolán kívül szerzett ismereteivel.

- Megkülönbözteti a főbb, életkorának megfelelő írott szövegtípusokat.

- Összetettebb írott instrukciókat értelmez.

- Alkalmazza az életkorának és nyelvi szintjének megfelelő írott, nyomtatott vagy digitális alapú szöveget a változatos nyelvórai tevékenységek és feladatmegoldás során.

- A nyomtatott vagy digitális alapú írott szöveget felhasználja szórakozásra és ismeretszerzésre önállóan is.

- Érdeklődése erősödik a célnyelvi irodalmi alkotások iránt.

- Megért és használ szavakat, szókapcsolatokat a célnyelvi, az életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel kapcsolatban

- Kommunikációt kezdeményez egyszerű hétköznapi témában, a beszélgetést követi, egyszerű, nyelvi eszközökkel fenntartja és lezárja.

- Az életkorának megfelelő mindennapi helyzetekben a tanult nyelvi eszközökkel megfogalmazott kérdéseket tesz fel, és válaszol a hozzá intézett kérdésekre.

- Véleményét, gondolatait, érzéseit egyre magabiztosabban fejezi ki a tanult nyelvi eszközökkel.

- A tanult nyelvi elemeket többnyire megfelelően használja, beszédszándékainak megfelelően, egyszerű spontán helyzetekben.

- Váratlan, előre nem kiszámítható eseményekre, jelenségekre és történésekre is reagál egyszerű célnyelvi eszközökkel, személyes vagy online interakciókban.

- Bekapcsolódik a tanórán az interakciót igénylő nyelvi tevékenységekbe, abban társaival közösen részt vesz, a begyakorolt nyelvi elemeket tanári segítséggel a játék céljainak megfelelően alkalmazza.

- Üzeneteket ír,

- Véleményét írásban, egyszerű nyelvi eszközökkel megfogalmazza, és arról írásban interakciót folytat.

- Rövid, egyszerű, ismert nyelvi eszközökből álló kiselőadást tart változatos feladatok kapcsán, hagyományos vagy digitális alapú vizuális eszközök támogatásával.

- Felhasználja a célnyelvet tudásmegosztásra.

- Találkozik az életkorának és nyelvi szintjének megfelelő célnyelvi ismeretterjesztő tartalmakkal.

- Néhány szóból vagy mondatból álló jegyzetet készít írott szöveg alapján.

- Egyszerűen megfogalmazza személyes véleményét, másoktól véleményük kifejtését kéri, és arra reagál, elismeri vagy cáfolja mások állítását, kifejezi egyetértését vagy egyet nem értését.

- Kifejez tetszést, nem tetszést, akaratot, kívánságot, tudást és nem tudást, ígéretet, szándékot, dicséretet, kritikát.

- Információt cserél, információt kér, információt ad.

- Kifejez kérést, javaslatot, meghívást, kínálást és ezekre reagálást.

- Kifejez alapvető érzéseket, például örömöt, sajnálkozást, bánatot, elégedettséget, elégedetlenséget, bosszúságot, csodálkozást, reményt.

- Kifejez és érvekkel alátámasztva mutat be szükségességet, lehetőséget, képességet, bizonyosságot, bizonytalanságot.

- Értelmez és használja az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is,

- Felhasználja a célnyelvet ismeretszerzésre.

- Használja a célnyelvet életkorának és nyelvi szintjének megfelelő aktuális témákban és a hozzájuk tartozó szituációkban.

- Találkozik életkorának és nyelvi szintjének megfelelő célnyelvi szórakoztató tartalmakkal.

- Összekapcsolja az ismert nyelvi elemeket egyszerű kötőszavakkal (például: és, de, vagy).

- Egyszerű mondatokat összekapcsolva mond el egymást követő eseményekből álló történetet, vagy leírást ad valamilyen témáról.

- A tanult nyelvi eszközökkel és nonverbális elemek segítségével tisztázza mondanivalójának lényegét.

- Ismeretlen szavak valószínű jelentését szövegösszefüggések alapján kikövetkezteti az életkorának és érdeklődésének megfelelő, konkrét, rövid szövegekben.

- Alkalmaz nyelvi funkciókat rövid társalgás megkezdéséhez, fenntartásához és befejezéséhez.

- Nem értés esetén a meg nem értett kulcsszavak vagy fordulatok ismétlését vagy magyarázatát kéri, visszakérdez, betűzést kér.

- Megoszt alapvető személyes információkat és szükségleteket magáról egyszerű nyelvi elemekkel.

- Ismerős és gyakori alapvető helyzetekben, akár telefonon vagy digitális csatornákon is, többnyire helyesen és érthetően fejezi ki magát az ismert nyelvi eszközök segítségével.

- Tudatosan használ alapszintű nyelvtanulási és nyelvhasználati stratégiákat.

- Hibáit többnyire észreveszi és javítja.

- Ismer szavakat, szókapcsolatokat a célnyelven a témakörre jellemző, életkorának és érdeklődésének megfelelő más tudásterületen megcélzott tartalmakból.

- Egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki magának.

- Céljai eléréséhez megtalálja és használja a megfelelő eszközöket.

- Céljai eléréséhez társaival párban és csoportban együttműködik.

- Nyelvi haladását többnyire fel tudja mérni,

- Társai haladásának értékelésében segítően részt vesz.

- A tanórán kívüli, akár játékos nyelvtanulási lehetőségeket felismeri, és törekszik azokat kihasználni.

- Felhasználja a célnyelvet szórakozásra és játékos nyelvtanulásra.

- Digitális eszközöket és felületeket is használ nyelvtudása fejlesztésére,

- Értelmez egyszerű, szórakoztató kisfilmeket

- Megismeri a célnyelvi országok főbb jellemzőit és kulturális sajátosságait.

- További országismereti tudásra tesz szert.

- Célnyelvi kommunikációjába beépíti a tanult interkulturális ismereteket.

- Találkozik célnyelvi országismereti tartalmakkal.

- Találkozik a célnyelvi, életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel.

- Megismerkedik hazánk legfőbb országismereti és történelmi eseményeivel célnyelven.

- A célnyelvi kultúrákhoz kapcsolódó alapvető tanult nyelvi elemeket használja.

- Idegen nyelvi kommunikációjában ismeri és használja a célnyelv főbb jellemzőit.

- Következetesen alkalmazza a célnyelvi betű és jelkészletet

- Egyénileg vagy társaival együttműködve szóban vagy írásban projektmunkát vagy kiselőadást készít, és ezeket digitális eszközök segítségével is meg tudja valósítani.

- Találkozik az érdeklődésének megfelelő akár autentikus szövegekkel elektronikus, digitális csatornákon tanórán kívül is.

## Pénzügyi és vállalkozói ismeretek

### 10. évfolyamon

- A tanuló érti a nemzetgazdaság szereplőinek (háztartások, vállalatok, állam, pénzintézetek) feladatait, a köztük lévő kapcsolatrendszer sajátosságait.
- Tudja értelmezni az állam gazdasági szerepvállalásának jelentőségét, ismeri főbb feladatait, azok hatásait.
- Tisztában van azzal, hogy az adófizetés biztosítja részben az állami feladatok ellátásnak pénzügyi fedezetét.
- Ismeri a mai bankrendszer felépítését, az egyes pénzpiaci szereplők főbb feladatait.
- Képes választani az egyes banki lehetőségek közül.
- Tisztában van az egyes banki ügyletek előnyeivel, hátrányaival, kockázataival.
- A bankok kínálatából bankot, bankszámla csomagot tud választani.
- Tud érvelni a családi költségvetés mellett, a tudatos, hatékony pénzgazdálkodás érdekében.
- Önismereti tesztek, játékok segítségével képes átgondolni milyen foglalkozások, tevékenységek illeszkednek személyiségéhez.
- Tisztában van az álláskeresés folyamatával, a munkaviszonnyal kapcsolatos jogaival, kötelezettségeivel.
- Ismer vállalkozókat, vállalatokat, össze tudja hasonlítani az alkalmazotti, és a vállalkozói személyiségjegyeket.
- Érti a leggyakoribb vállalkozási formák jellemzőit, előnyeit, hátrányait.
- Tisztában van a nem nyereségérdekelt szervezetek gazdaságban betöltött szerepével.
- Ismeri a vállalkozásalapítás, -működtetés legfontosabb lépéseit, képes önálló vállalkozói ötlet kidolgozására.
- Meg tudja becsülni egy vállalkozás lehetséges költségeit, képes adott időtartamra költségkalkulációt tervezni.
- Tisztában van az üzleti tervezés szükségességével, mind egy új vállalkozás alapításakor, mind már meglévő vállalkozás működése esetén.
- Tájékozott az üzleti terv tartalmi elemeiről.
- Megismeri a nem üzleti (társadalmi, kulturális, egyéb civil) kezdeményezések pénzügyi-gazdasági igényeit, lehetőségeit.
- Felismeri a kezdeményezőkészség jelentőségét az állampolgári felelősségvállalásban.
- Felismeri a sikeres vállalkozás jellemzőit, képes azonosítani az esetleges kudarc okait, javaslatot tud tenni a problémák megoldására.

## Szoftverfejlesztés és -tesztelés

### 9-10. évfolyamon

- Adott kapcsolási rajz alapján egyszerűbb áramköröket épít próbapanel segítségével vagy forrasztásos technológiával.
- Ismeri az elektronikai alapfogalmakat, kapcsolódó fizikai törvényeket, alapvető alkatrészeket és kapcsolásokat.
- A funkcionalitás biztosítása mellett törekszik az esztétikus kialakításra (pl. minőségi forrasztás, egyenletes alkatrész sűrűség, olvashatóság).
- Az elektromos berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
- Alapvető villamos méréseket végez önállóan a megépített áramkörökön.
- Ismeri az elektromos mennyiségek mérési metódusait, a mérőműszerek használatát.
- Elvégzi a számítógépen és a mobil eszközökön az operációs rendszer (pl. Windows, Linux, Android, iOS), valamint az alkalmazói szoftverek telepítését, frissítését és alapszintű beállítását. Grafikus felületen, valamint parancssorban használja a Windows, és Linux operációs rendszerek alapszintű parancsait és szolgáltatásait (pl. állomány- és könyvtárkezelési műveletek, jogosultságok beállítása, szövegfájlokkal végzett műveletek, folyamatok kezelése).
- Ismeri a számítógépen és a mobil informatikai eszközökön használt operációs rendszerek telepítési és frissítési módjait, alapvető parancsait és szolgáltatásait, valamint alapvető beállítási lehetőségeit.
- Törekszik a felhasználói igényekhez alkalmazkodó szoftverkörnyezet kialakítására.
- Önállóan elvégzi a kívánt szoftverek telepítését, szükség esetén gondoskodik az eszközön korábban tárolt adatok biztonsági mentéséről.
- Elvégzi a PC perifériáinak csatlakoztatását, szükség esetén új alkatrészt szerel be vagy alkatrészt cserél egy számítógépben.
- Ismeri az otthoni és irodai informatikai környezetet alkotó legáltalánosabb összetevők (PC, nyomtató, mobiltelefon, WiFi router stb.) szerepét, alapvető működési módjukat. Ismeri a PC és a mobil eszközök főbb alkatrészeit (pl. alaplap, CPU, memória) és azok szerepét.
- Törekszik a végrehajtandó műveletek precíz és előírásoknak megfelelő elvégzésére.
- Az informatikai berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
- Alapvető karbantartási feladatokat lát el az általa megismert informatikai és távközlési berendezéseken (pl. szellőzés és csatlakozások ellenőrzése, tisztítása).
- Tisztában van vele, hogy miért szükséges az informatikai és távközlési eszközök rendszeres és eseti karbantartása. Ismeri legalapvetőbb karbantartási eljárásokat.
- A hibamentes folyamatos működés elérése érdekében fontosnak tartja a megelőző karbantartások elvégzését.
- Otthoni vagy irodai hálózatot alakít ki WiFi router segítségével, elvégzi WiFi router konfigurálását, a vezetékes- és vezeték nélküli eszközök (PC, mobiltelefon, set-top box stb.), csatlakoztatását és hálózati beállítását.
- Ismeri az informatikai hálózatok felépítését, alapvető technológiáit (pl. Ethernet), protokolljait (pl. IP, HTTP) és szabványait (pl. 802.11-es WiFi szabványok). Ismeri az otthoni és irodai hálózatok legfontosabb összetevőinek (kábelezés, WiFi router, PC, mobiltelefon stb.) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
- Törekszik a felhasználói igények megismerésére, megértésére, és szem előtt tartja azokat a hálózat kialakításakor.
- Néhány alhálózatból álló kis- és közepes vállalati hálózatot alakít ki forgalomirányító és kapcsoló segítségével, elvégzi az eszközök alapszintű hálózati beállításait (pl. forgalomirányító interfészeinek IP-cím beállítása, alapértelmezett átjáró beállítása).
- Ismeri a kis- és közepes vállalati hálózatok legfontosabb összetevőinek (pl. kábelrendező szekrény, kapcsoló, forgalomirányító) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
- Alkalmazza a hálózatbiztonsággal kapcsolatos legfontosabb irányelveket (pl. erős jelszavak használata, vírusvédelem alkalmazása, tűzfal használat).
- Ismeri a fontosabb hálózatbiztonsági elveket, szabályokat, támadás típusokat, valamint a szoftveres és hardveres védekezési módszereket.
- Megkeresi és elhárítja az otthoni és kisvállalati informatikai környezetben jelentkező hardveres és szoftveres hibákat.
- Ismeri az otthoni és kisvállalati informatikai környezetben leggyakrabban felmerülő hibákat (pl. hibás IP-beállítás, kilazult csatlakozó) és azok elhárításának módjait.
- Önállóan behatárolja a hibát. Egyszerűbb problémákat önállóan, összetettebbeket szakmai irányítással hárít el.
- Internetes források és tudásbázisok segítségével követi, valamint feladatainak elvégzéséhez lehetőség szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
- Naprakész információkkal rendelkezik a legmodernebb információs technológiákkal és trendekkel kapcsolatban.
- Nyitott és érdeklődő a legmodernebb információs technológiák és trendek iránt.
- Önállóan szerez információkat a témában releváns szakmai platformokról.
- Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével.
- Ismeri a HTML5, a CSS3 alapvető elemeit, a stíluslapok fogalmát, felépítését. Érti a reszponzív megjelenítéshez használt módszereket, keretrendszerek előnyeit, a reszponzív webdizájn alapelveit.
- A felhasználói igényeknek megfelelő funkcionalitás és design összhangjára törekszik.
- Önállóan létrehozza és megformázza a weboldalt.
- Munkája során jelentkező problémák kezelésére vagy folyamatok automatizálására egyszerű alkalmazásokat készít Python programozási nyelv segítségével.
- Ismeri a Python nyelv elemeit, azok céljait (vezérlési szerkezetek, adatszerkezetek, változók, aritmetikai és logikai kifejezések, függvények, modulok, csomagok). Ismeri az algoritmus fogalmát, annak szerepét.
- Jól átlátható kódszerkezet kialakítására törekszik.
- Önállóan készít egyszerű alkalmazásokat.
- Git verziókezelő rendszert, valamint fejlesztést és csoportmunkát támogató online eszközöket és szolgáltatásokat (pl.: GitHub, Slack, Trello, Microsoft Teams, Webex Teams) használ.
- Ismeri a Git, valamint a csoportmunkát támogató eszközök és online szolgáltatások célját, működési módját, legfontosabb funkcióit.
- Törekszik a feladatainak megoldásában a hatékony csoportmunkát támogató online eszközöket kihasználni.
- A Git verziókezelőt, valamint a csoportmunkát támogató eszközöket és szolgáltatásokat önállóan használja.
- Társaival hatékonyan együttműködve, csapatban dolgozik egy informatikai projekten. A projektek végrehajtása során társaival tudatosan és célirányosan kommunikál.
- Ismeri a projektmenedzsment lépéseit (kezdeményezés, követés, végrehajtás, ellenőrzés, dokumentáció, zárás).
- Más munkáját és a csoport belső szabályait tiszteletben tartva, együttműködően vesz részt a csapatmunkában.
- A projektekben irányítás alatt, társaival közösen dolgozik. A ráosztott feladatrészt önállóan végzi el.
- Munkája során hatékonyan használja az irodai szoftvereket.
- Ismeri az irodai szoftverek főbb funkcióit, felhasználási területeit.
- Az elkészült termékhez prezentációt készít és bemutatja, előadja azt munkatársainak, vezetőinek, ügyfeleinek.
- Ismeri a hatékony prezentálás szabályait, a prezentációs szoftverek lehetőségeit.
- Törekszik a tömör, lényegre törő, de szakszerű bemutató összeállítására.
- A projektcsapat tagjaival egyeztetve, de önállóan elkészíti az elvégzett munka eredményét bemutató prezentációt.

### 11-13. évfolyamon

- Használja a Git verziókezelő rendszert, valamint a fejlesztést támogató csoportmunkaeszközöket és szolgáltatásokat (pl. GitHub, Slack, Trello, Microsoft Teams, Webex Teams).
- Ismeri a legelterjedtebb csoportmunkaeszközöket, valamint a Git verziókezelőrendszer szolgáltatásait.
- Igyekszik munkatársaival hatékonyan, igazi csapatjátékosként együtt dolgozni. Törekszik a csoporton belül megkapott feladatok precíz, határidőre történő elkészítésére, társai segítésére.
- Szoftverfejlesztési projektekben irányítás alatt dolgozik, a rábízott részfeladatok megvalósításáért felelősséget vállal.
- Az általa végzett szoftverfejlesztési feladatok esetében kiválasztja a legmegfelelőbb technikákat, eljárásokat és módszereket.
- Elegendő ismerettel rendelkezik a meghatározó szoftverfejlesztési technológiák (programozási nyelvek, keretrendszerek, könyvtárak stb.), illetve módszerek erősségeiről és hátrányairól.
- Nyitott az új technológiák megismerésére, tudását folyamatosan fejleszti.
- Önállóan dönt a fejlesztés során használt technológiákról és eszközökről.
- A megfelelő kommunikációs forma (e-mail, chat, telefon, prezentáció stb.) kiválasztásával munkatársaival és az ügyfelekkel hatékonyan kommunikál műszaki és egyéb információkról magyarul és angolul.
- Ismeri a különböző kommunikációs formákra (e-mail, chat, telefon, prezentáció stb.) vonatkozó etikai és belső kommunikációs szabályokat.
- Angol nyelvismerettel rendelkezik (KER B1 szint). Ismeri a gyakran használt szakmai kifejezéseket angolul.
- Kommunikációjában konstruktív, együttműködő, udvarias. Feladatainak a felhasználói igényeknek leginkább megfelelő, minőségi megoldására törekszik.
- Felelősségi körébe tartozó feladatokkal kapcsolatban a vállalati kommunikációs szabályokat betartva, önállóan kommunikál az ügyfelekkel és munkatársaival.
- Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével. Kereső optimalizálási beállításokat alkalmaz.
- Ismeri a HTML5 és a CSS3 szabvány alapvető nyelvi elemeit és eszközeit (strukturális és szemantikus HTML-elemek, attribútumok, listák, táblázatok, stílus jellemzők és függvények). Ismeri a a reszponzív webdizájn alapelveit és a Bootstrap keretrendszer alapvető szolgáltatásait.
- Törekszik a weblapok igényes és a használatot megkönnyítő kialakítására.
- Kisebb webfejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
- Egyszerűbb webhelyek dinamikus viselkedését (eseménykezelés, animáció stb.) biztosító kódot, készít JavaScript nyelven.
- Alkalmazási szinten ismeri a JavaScript alapvető nyelvi elemeit, valamint az aszinkron programozás és az AJAX technológia működési elvét. Tisztában van a legfrissebb ECMAScript változatok (ES6 vagy újabb) hatékonyság növelő funkcióival.
- Egyszerűbb JavaScript programozási feladatokat önállóan végez el.
- RESTful alkalmazás kliens oldali komponensének fejlesztését végzi JavaScript nyelven.
- Tisztában van a REST szoftverarchitektúra elvével, alkalmazás szintjén ismeri az AJAX technológiát.
- A tiszta kód elveinek megfelelő, megfelelő mennyiségű megjegyzéssel ellátott, kellőképpen tagolt, jól átlátható, kódot készít.
- Ismeri a tiszta kód készítésének alapelveit.
- Törekszik arra, hogy az elkészített kódja jól átlátható, és mások számára is értelmezhető legyen.
- Adatbázis-kezelést is végző konzolos vagy grafikus felületű asztali alkalmazást készít magas szintű programozási nyelvet (C#, Java) használva.
- Ismeri a választott magas szintű programozási nyelv alapvető nyelvi elemeit, illetve a hozzá tartozó fejlesztési környezetet.
- Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
- Kisebb asztali alkalmazás-fejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
- Adatkezelő alkalmazásokhoz relációs adatbázist tervez és hoz létre, többtáblás lekérdezéseket készít.
- Tisztában van a relációs adatbázis-tervezés és -kezelés alapelveivel. Haladó szinten ismeri a különböző típusú SQL lekérdezéseket, azok nyelvi elemeit és lehetőségeit.
- Törekszik a redundanciamentes, világos szerkezetű, legcélravezetőbb kialakítású adatbázis szerkezet megvalósítására.
- Kisebb projektekhez néhány táblás adatbázist önállóan tervez meg, nagyobb projektekben a biztosított adatbáziskörnyezetet használva önállóan valósít meg lekérdezéseket.
- Önálló- vagy komplex szoftverrendszerek részét képző kliens oldali alkalmazásokat fejleszt mobil eszközökre.
- Ismeri a választott mobil alkalmazás fejlesztésére alkalmas nyelvet és fejlesztői környezetet. Tisztában van a mobil alkalmazásfejlesztés alapelveivel.
- Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
- Kisebb projektek mobil eszközökre optimalizált kliens oldali alkalmazását önállóan megvalósítja meg.
- Webes környezetben futtatható kliens oldali (frontend) alkalmazást készít JavaScript keretrendszer (pl. React, Vue, Angular) segítségével.
- Érti a frontend fejlesztésre szolgáló JavaScript keretrendszerek célját. Meg tudja nevezni a 3-4 legelterjedtebb keretrendszert. Alkalmazás szintjén ismeri a könyvtárak és modulok kezelését végző csomagkezelő rendszereket (package manager, pl. npm, yarn). Ismeri a választott JavaScript keretrendszer működési elvét, nyelvi és strukturális elemeit.
- Törekszik maximálisan kihasználni a választott keretrendszer előnyeit, követi az ajánlott fejlesztési mintákat.
- Kisebb frontend alkalmazásokat önállóan készít el, nagyobb projektekben irányítás mellett végzi el a kijelölt komponensek fejlesztését.
- RESTful alkalmazás adatbázis-kezelési feladatokat is ellátó szerveroldali komponensének (backend) fejlesztését végzi erre alkalmas nyelv vagy keretrendszer segítségével (pl. Node.js, Spring, Laravel).
- Érti a RESTful szoftverarchitektúra lényegét. Tisztában van legalább egy backend készítésére szolgáló nyelv vagy keretrendszer működési módjával, nyelvi és strukturális elemeivel. Alkalmazás szintjén ismeri az objektum-relációs leképzés technológiát (ORM).
- Igyekszik backend működését leíró precíz, a frontend fejlesztők számára könnyen értelmezhető dokumentáció készítésére.
- Kisebb backend alkalmazásokat önállóan készít el, nagyobb projektekben részletes specifikációt követve, irányítás mellett végzi el a kijelölt komponensek fejlesztését.
- Objektum orientált (OOP) programozási módszertant alkalmazó asztali, webes és mobil alkalmazást készít.
- Ismeri az objektumorientált programozás elvét, tisztában van az öröklődés, a polimorfizmus, a metódus/konstruktor túlterhelés fogalmával.
- Törekszik az OOP technológia nyújtotta előnyök kihasználására, valamint igyekszik követni az OOP irányelveket és ajánlásokat.
- Kisebb projektekben önállóan tervezi meg a szükséges osztályokat, nagyobb projektekben irányítás mellett, a projektben a projektcsapat által létrehozott osztálystruktúrát használva, illetve azt kiegészítve végzi a fejlesztést.
- Tartalomkezelő rendszer (CMS, pl. WordPress) segítségével webhelyet készít, egyéni problémák megoldására saját beépülőket hoz létre.
- Ismeri a tartalomkezelő-rendszerek célját és alapvető szolgáltatásait. Ismeri a beépülők célját és alkalmazási területeit.
- Törekszik az igényes kialakítású és a felhasználók számára könnyű használatot biztosító webhelyek kialakításra.
- Kevésbé összetett portálokat igényes vizuális megjelenést biztosító sablonok, valamint magas funkcionalitást biztosító beépülők használatával önállóan valósít meg. Összetettebb projekteken irányítás mellett, grafikus tervezőkkel, UX szakemberekkel és más fejlesztőkkel együttműködve dolgozik.
- Manuális és automatizált szoftvertesztelést végezve ellenőrzi a szoftver hibátlan működését, dokumentálja a tesztek eredményét.
- Ismeri a unit tesztelés, valamint más tesztelési, hibakeresési technikák alapelveit és alapvető eszközeit.
- Törekszik a mindenre kiterjedő, az összes lehetséges hibát felderítő tesztelésre, valamint a tesztek körültekintő dokumentálására.
- Saját fejlesztésként megvalósított kisebb projektekben önállóan végzi a tesztelést, tesztelői szerepben nagyobb projektekben irányítás mellett végez meghatározott tesztelési feladatokat.
- Szoftverfejlesztés vagy -tesztelés során felmerülő problémákat old meg és hibákat hárít el webes kereséssel és internetes tudásbázisok használatával (pl. Stack Overflow).
- Ismeri a hibakeresés szisztematikus módszereit, a problémák elhárításának lépéseit.
- Ismeri a munkájához kapcsolódó internetes keresési módszereket és tudásbázisokat.
- Törekszik a hibák elhárítására, megoldására, és arra, hogy azokkal lehetőség szerint ne okozzon újabb hibákat.
- Internetes információszerzéssel önállóan old meg problémákat és hárít el hibákat.
- Munkája során hatékonyan használja az irodai szoftvereket, műszaki tartalmú dokumentumokat és bemutatókat készít.
- Ismeri az irodai szoftverek haladó szintű szolgáltatásait.
- Precízen készíti el a műszaki tartalmú dokumentációkat, prezentációkat. Törekszik arra, hogy a dokumentumok könnyen értelmezhetők és mások által is szerkeszthetők legyenek.
- Felelősséget vállal az általa készített műszaki tartalmú dokumentációkért.
- Munkája során cél szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtaulizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
- Alapszintű alkalmazási szinten ismeri a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
- Nyitott az új technológiák megismerésére, és törekszik azok hatékony, a felhasználói igényeknek és a költséghatékonysági elvárásoknak megfelelő felhasználására a szoftverfejlesztési feladatokban.
- Részt vesz szoftverrendszerek ügyfeleknél történő bevezetésében, a működési környezetet biztosító IT-környezet telepítésében és beállításában.
- Ismeri a számítógép és a mobil informatikai eszközök felépítését (főbb komponenseket, azok feladatait) és működését. Ismeri az eszközök operációs rendszerének és alkalmazói szoftvereinek telepítési és beállítási lehetőségeit.
- A szoftverrendszerek bevezetése és a működési környezet kialakítása során törekszik az ügyfelek elvárásainak megfelelni, valamint tiszteletben tartja az ügyfél vállalati szabályait.
- Az elvégzett eszköz- és szoftvertelepítésekért felelősséget vállal.
- A szoftverfejlesztés és tesztelési munkakörnyezetének kialakításához beállítja a hálózati eszközöket, elvégzi a vezetékes és vezetéknélküli eszközök csatlakoztatását és hálózatbiztonsági beállítását. A fejlesztett szoftverben biztonságos, HTTPS protokollt használó webes kommunikációt valósít meg.
- Ismeri az IPv4 és IPv6 címzési rendszerét és a legalapvetőbb hálózati protokollok szerepét és működési módját (IP, TCP, UDP, DHCP, HTTP, HTTPS, telnet, ssh, SMTP, POP3, IMAP4, DNS, TLS/SSL stb.). Ismeri a végponti berendezések IP-beállítási és hibaelhárítási lehetőségeit. Ismeri az otthoni és kisvállalati hálózatokban működő multifunkciós forgalomirányítók szolgáltatásait, azok beállításának módszereit.

## Történelem

### 9-12. évfolyamon

- Megbízható ismeretekkel bír az európai, valamint az egyetemes történelem és mélyebb tudással rendelkezik a magyar történelem fontosabb eseményeiről, történelmi folyamatairól, fordulópontjairól.

- Képes a múlt és jelen társadalmi, gazdasági, politikai és kulturális folyamatairól, jelenségeiről többszempontú, tárgyilagos érveléssel alátámasztott véleményt alkotni, ezekkel kapcsolatos problémákat megfogalmazni.

- Ismeri a közös magyar nemzeti és európai, valamint az egyetemes emberi civilizáció kulturális örökségének, kódrendszerének lényeges elemeit.

- Különbséget tud tenni történelmi tények és történelmi interpretáció, illetve vélemény között.

- Képes következtetni történelmi események, folyamatok és jelenségek okaira és következményeire.

- Képes a tanulási célhoz megfelelő információforrást választani, a források között szelektálni, azokat szakszerűen feldolgozni és értelmezni.

- Kialakul a hiteles és tárgyilagos forráshasználat és kritika igénye.

- Képes a múlt eseményeit és jelenségeit a saját történelmi összefüggésükben értelmezni, illetve a jelen viszonyait kapcsolatba hozni a múltban történtekkel.

- Ismeri a demokratikus államszervezet működését, a társadalmi együttműködés szabályait, a piacgazdaság alapelveit; autonóm és felelős állampolgárként viselkedik.

- Kialakul és megerősödik a történelmi múlt, illetve a társadalmi, politikai, gazdasági és kulturális kérdések iránti érdeklődés.

- Kialakulnak a saját értékrend és történelemszemlélet alapjai.

- Elmélyül a nemzeti identitás és hazaszeretet, büszke népe múltjára, ápolja hagyományait, és méltón emlékezik meg hazája nagyjairól.

- Megerősödnek az európai civilizációs identitás alapelemei.

- Megerősödik és elmélyül a társadalmi felelősség és normakövetés, az egyéni kezdeményezőkészség, a hazája, közösségei és embertársai iránt való felelősségvállalás, valamint a demokratikus elkötelezettség.

- Ismeri az ókori civilizációk legfontosabb jellemzőit, valamint az athéni demokrácia és a római állam működését, hatásukat az európai civilizációra.

- Felidézi a monoteista vallások kialakulását, legfontosabb jellemzőiket, tanításaik főbb elemeit, és bemutatja terjedésüket.

- Bemutatja a keresztény vallás civilizációformáló hatását, a középkori egyházat, valamint a reformáció és a katolikus megújulás folyamatát és kulturális hatásait; érvel a vallási türelem, illetve a vallásszabadság mellett.

- Képes felidézni a középkor gazdasági és kulturális jellemzőit, világképét, a kor meghatározó birodalmait és bemutatni a rendi társadalmat.

- Ismeri a magyar nép őstörténetére és a honfoglalásra vonatkozó tudományos elképzeléseket és tényeket, tisztában van legfőbb vitatott kérdéseivel, a különböző tudományterületek kutatásainak főbb eredményeivel.

- Értékeli az államalapítás, valamint a kereszténység felvételének jelentőségét.

- Felidézi a középkori magyar állam történetének fordulópontjait, legfontosabb uralkodóink tetteit.

- Ismeri a magyarság törökellenes küzdelmeit, fordulópontjait és hőseit; felismeri, hogy a magyar és az európai történelem alakulását meghatározóan befolyásolta a török megszállás.

- Be tudja mutatni a kora újkor fő gazdasági és társadalmi folyamatait, ismeri a felvilágosodás eszméit, illetve azok kulturális és politikai hatását, valamint véleményt formál a francia forradalom európai hatásáról.

- Összefüggéseiben és folyamatában fel tudja idézni, miként hatott a magyar történelemre a habsburg birodalomhoz való tartozás, bemutatja az együttműködés és konfrontáció megnyilvánulásait, a függetlenségi törekvéseket és értékeli a rákóczi-szabadságharc jelentőségét.

- Ismeri és értékeli a magyar nemzetnek a polgári átalakulás és nemzeti függetlenség elérésére tett erőfeszítéseit a reformkor, az 1848/49-es forradalom és szabadságharc, illetve az azt követő időszakban; a kor kiemelkedő magyar politikusait és azok nézeteit, véleményt tud formálni a kiegyezésről.

- Fel tudja idézni az ipari forradalom szakaszait, illetve azok gazdasági, társadalmi, kulturális és politikai hatásait; képes bemutatni a modern polgári társadalom és állam jellemzőit és a 19. század főbb politikai eszméit, valamint felismeri a hasonlóságot és különbséget azok mai formái között.

- Fel tudja idézni az első világháború előzményeit, a háború jellemzőit és fontosabb fordulópontjait, értékeli a háborúkat lezáró békék tartalmát, és felismeri a háborúnak a 20. század egészére gyakorolt hatását.

- Bemutatja az első világháború magyar vonatkozásait, a háborús vereség következményeit; példákat tud hozni a háborús helytállásra.

- Képes felidézni azokat az okokat és körülményeket, amelyek a történelmi magyarország felbomlásához vezettek.

- Tisztában van a trianoni békediktátum tartalmával és következményeivel, be tudja mutatni az ország talpra állását, a horthy-korszak politikai, gazdasági, társadalmi és kulturális viszonyait, felismeri a magyar külpolitika mozgásterének korlátozottságát.

- Össze tudja hasonlítani a nemzetiszocialista és a kommunista ideológiát és diktatúrát, példák segítségével bemutatja a rendszerek embertelenségét és a velük szembeni ellenállás formáit.

- Képes felidézni a második világháború okait, a háború jellemzőit és fontosabb fordulópontjait, ismeri a holokausztot és a hozzávezető okokat.

- Bemutatja magyarország revíziós lépéseit, a háborús részvételét, az ország német megszállását, a magyar zsidóság tragédiáját, a szovjet megszállást, a polgári lakosság szenvedését, a hadifoglyok embertelen sorsát.

- Össze tudja hasonlítani a nyugati demokratikus világ és a kommunista szovjet blokk politikai és társadalmi berendezkedését, képes jellemezni a hidegháború időszakát, bemutatni a gyarmati rendszer felbomlását és az európai kommunista rendszerek összeomlását.

- Bemutatja a kommunista diktatúra magyarországi kiépítését, működését és változatait, az 1956-os forradalom és szabadságharc okait, eseményeit és hőseit, összefüggéseiben szemléli a rendszerváltoztatás folyamatát, felismerve annak történelmi jelentőségét.

- Bemutatja a gyarmati rendszer felbomlásának következményeit, india, kína és a közel-keleti régió helyzetét és jelentőségét.

- Ismeri és reálisan látja a többpólusú világ jellemzőit napjainkban, elhelyezi magyarországot a globális világ folyamataiban.

- Bemutatja a határon túli magyarság helyzetét, a megmaradásért való küzdelmét trianontól napjainkig.

- Ismeri a magyar cigányság történetének főbb állomásait, bemutatja jelenkori helyzetét.

- Ismeri a magyarság, illetve a kárpát-medence népei együttélésének jellemzőit, példákat hoz a magyar nemzet és a közép-európai régió népeinek kapcsolatára, különös tekintettel a visegrádi együttműködésre.

- Ismeri hazája államszervezetét, választási rendszerét.

- Önállóan tud használni általános és történelmi, nyomtatott és digitális információforrásokat (tankönyv, kézikönyvek, szakkönyvek, lexikonok, képzőművészeti alkotások, könyvtár és egyéb adatbázisok, filmek, keresők).

- Önállóan információkat tud gyűjteni, áttekinteni, rendszerezni és értelmezni különböző médiumokból és írásos vagy képi forrásokból, statisztikákból, diagramokból, térképekről, nyomtatott és digitális felületekről.

- Tud forráskritikát végezni és különbséget tenni a források között hitelesség, típus és szövegösszefüggés alapján.

- Képes azonosítani a különböző források szerzőinek a szándékát, bizonyítékok alapján értékeli egy forrás hitelességét.

- Képes a szándékainak megfelelő információkat kiválasztani különböző műfajú forrásokból.

- Összehasonlítja a forrásokban talált információkat saját ismereteivel, illetve más források információival és megmagyarázza az eltérések okait.

- Képes kiválasztani a megfelelő forrást valamely történelmi állítás, vélemény alátámasztására vagy cáfolására.

- Ismeri a magyar és az európai történelem tanult történelmi korszakait, időszakait, és képes azokat időben és térben elhelyezni.

- Az egyes események, folyamatok idejét konkrét történelmi korhoz, időszakhoz kapcsolja vagy viszonyítja, ismeri néhány kiemelten fontos esemény, jelenség időpontját, kronológiát használ és készít.

- Össze tudja hasonlítani megadott szempontok alapján az egyes történelmi korszakok, időszakok jellegzetességeit az egyetemes és a magyar történelem egymáshoz kapcsolódó eseményeit.

- Képes azonosítani a tanult egyetemes és magyar történelmi személyiségek közül a kortársakat.

- Felismeri, hogy a magyar történelem az európai történelem része, és példákat tud hozni a magyar és európai történelem kölcsönhatásaira.

- Egyszerű történelmi térképvázlatot alkot hagyományos és digitális eljárással.

- A földrajzi környezet és a történeti folyamatok összefüggéseit példákkal képes alátámasztani.

- Képes különböző időszakok történelmi térképeinek összehasonlítására, a történelmi tér változásainak és a történelmi mozgások követésére megadott szempontok alapján a változások hátterének feltárásával.

- Képes a történelmi jelenségeket általános és konkrét történelmi fogalmak, tartalmi és értelmező kulcsfogalmak felhasználásával értelmezni és értékelni.

- Fel tud ismerni fontosabb történelmi fogalmakat meghatározás alapján.

- Képes kiválasztani, rendezni és alkalmazni az azonos korhoz, témához kapcsolható fogalmakat.

- Össze tudja foglalni rövid és egyszerű szaktudományos szöveg tartalmát.

- Képes önállóan vázlatot készíteni és jegyzetelni.

- Képes egy-egy korszakot átfogó módon bemutatni.

- Történelmi témáról kiselőadást, digitális prezentációt alkot és mutat be.

- Történelmi tárgyú folyamatábrákat, digitális táblázatokat, diagramokat készít, történelmi, gazdasági, társadalmi és politikai modelleket vizuálisan is meg tud jeleníteni.

- Megadott szempontok alapján történelmi tárgyú szerkesztett szöveget (esszét) tud alkotni, amelynek során tételmondatokat fogalmaz meg, állításait több szempontból indokolja és következtetéseket von le.

- Társaival képes megvitatni történelmi kérdéseket, amelynek során bizonyítékokon alapuló érvekkel megindokolja a véleményét, és választékosan reflektál mások véleményére, árnyalja saját álláspontját.

- Képes felismerni, megfogalmazni és összehasonlítani különböző társadalmi és történelmi problémákat, értékrendeket, jelenségeket, folyamatokat.

- A tanult ismereteket problémaközpontúan tudja rendezni.

- Hipotéziseket alkot történelmi személyek, társadalmi csoportok és intézmények viselkedésének mozgatórugóiról.

- Önálló kérdéseket fogalmaz meg történelmi folyamatok, jelenségek és események feltételeiről, okairól és következményeiről.

- Önálló véleményt tud alkotni történelmi eseményekről, folyamatokról, jelenségekről és személyekről.

- Képes különböző élethelyzetek, magatartásformák megfigyelése által következtetések levonására, erkölcsi kérdéseket is felvető történelmi helyzetek felismerésére és megítélésére.

- A változás és a fejlődés fogalma közötti különbséget ismerve képes felismerni és bemutatni azokat azonos korszakon belül, vagy azokon átívelően.

- Képes összevetni, csoportosítani és súlyozni az egyes történelmi folyamatok, jelenségek, események okait, következményeit, és ítéletet alkotni azokról, valamint a benne résztvevők szándékairól.

- Összehasonlít különböző, egymáshoz hasonló történeti helyzeteket, folyamatokat, jelenségeket.

- Képes felismerni konkrét történelmi helyzetekben, jelenségekben és folyamatokban valamely általános szabályszerűség érvényesülését.

- Összehasonlítja és kritikusan értékeli az egyes történelmi folyamatokkal, eseményekkel és személyekkel kapcsolatos eltérő álláspontokat.

- Feltevéseket fogalmaz meg, azok mellett érveket gyűjt, illetve mérlegeli az ellenérveket.

- Felismeri, hogy a jelen társadalmi, gazdasági, politikai és kulturális viszonyai a múltbeli események, tényezők következményeiként alakultak ki.
