# Pedagógiai és pszichológiai háttér

Az iskolák működésének tanulmányozása mellett a Budapest School komoly tudományos-elméleti háttérre alapozta koncepcióját.

Ezek közül is oktatási programjának központjában [Carol Dweck
fejlődésközpontú szemlélete](http://tantrend.hu/hir/fejlodeskozpontu-szemlelet-az-iskolaban) áll. Emellett nagy
hangsúlyt fektetünk az alábbi elméletek gyakorlati alkalmazására is:

Reformpedagógiai irányzatok elméletei, különös tekintettel:

- Montessori-pedagógia (Maria Montessori),
- kritikai pedagógia (Paulo Freire)
- élménypedagógia (John Dewey)
- felfedeztető tanulás (Jerome Bruner)
- projektmódszer (William Kilpatrick)
- kooperatív tanulás (Spencer Kagan)

Pszichológia és szociálpszichológiai kutatások eredményei:

- kognitív interakcionista tanuláselmélet (Jean Piaget)
- személyközpontú pszichológia (Carl Rogers)
- kommunikáció és konfliktuskezelés (Thomas Gordon)
- erőszakmentes kommunikáció (Marshall Rosenberg)
- pozitív pszichológia eredményei, különös tekintettel: flow-elmélet,
  kreativitás kutatások (Csíkszentmihályi Mihály)
- érzelmi és társas intelligencia (Peter Salovey, John D. Mayer,
  Daniel Goleman)
- motivációkutatások; ezeket jól foglalja össze Daniel H. Pink műve Motiváció 3.0 műve [(Pink, 2011)](https://www.danpink.com/drive./)
- hősiesség pszichológiai alapjai (Phil Zimbardo)
- fejlődésfókuszú szemlélet (Carol Dweck)
