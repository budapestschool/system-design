# Út az esélyegyenlőség felé

A Budapest School lehetőséget kíván biztosítani arra, hogy a
a társadalom minél szélesebb rétegeiből kerülhessenek be
gyerekek, hogy a családok társadalmi, gazdasági státuszától függetlenül,
kizárólag saját fejlődési útjuk kiteljesedése jegyében válhassanak a
közösség részévé.

Tudjuk, hogy az esélyek sosem egyenlőek, tenni viszont lehet azért, hogy
minél kiegyenlítettebbek legyenek. A következő területeken a családok
kiválasztásakor és az iskolába járó gyerekekkel való partneri
kapcsolatban azon dolgozunk, hogy senki se érezhesse magát hátrányosan
megkülönböztetve testi, szellemi, kulturális, szociális, nemi vagy
hitbéli egyediségei miatt.

#### A társadalmi státuszban

A Budapest Schoolba járó családok legalább 30%-a kevesebb
hozzájárulást fizet, mint amennyi az iskola működésének teljes
bekerülési költsége. Az ő számuk, az általuk fizetendő hozzájárulás
mértéke a támogató családoktól függ. Minél érzékenyebb, minél
nagyvonalúbb egy közösség azok irányába, akik nem tehetik meg, hogy a
gyerekük tanulásáért kifizessék az ahhoz szükséges költségeket, annál
nagyobb mértékű a társadalmi státuszbéli esélyegyenlőség egy adott
Budapest School tanulóközösségben.

#### A nemek arányában

A Budapest School tanulóközösségeiben a nemek arányának
kiegyenlítésére törekszünk. Folyamatosan monitorozzuk a fiúk és lányok arányát az egyes
közösségekben, és ha az elcsúszik valamely irányba, akkor a
felvételi során a kiegyenlítés irányába hozunk döntéseket.

#### A kulturális és vallási egyediségekben

A BPS-iskolában meghatározó, észrevehető a családok értékrendje, mert a családok részei a közösségnek.
A családok pedig jöhetnek azonos, de jöhetnek diverz kulturális és
vallási környezetből is. Az ő szempontjaik tiszteletben tartása
mindaddig kiemelten fontos, amíg az nem veszélyezteti a közösségben
együtt tanuló gyerekek fejlődését.

#### A fejlődés sebességében

A Budapest School lehetőséget biztosít arra, hogy minden
gyerek a saját tempójában, a saját maga által kijelölt és komfortos
tanulási úton haladjon addig, amíg ebben a mentortanárával és a
szüleivel is közös megállapodást kötnek. A fejlődés sebessége azonban
nem akadályozhatja a közösségben tanulást. A tanulásszervezők
felelőssége annak meghatározása, hogy a közösségben lassabban
fejlődő gyerekek mennyiben veszélyeztetik, vagy mennyiben segítik a
tanulásszervezést a közösség egésze szempontjából.

#### A tanulás tartalmában

A BPS-iskolában minden gyereknek van saját célja.
Ennek hossza, komplexitása minden esetben egyedi, függ a gyerek
érdeklődésétől, érettségétől, családi helyzetétől, mentális és fizikai
állapotától. A tanulás tartalmában mindenkinek van lehetősége arra, hogy
a maga útját járja, ha figyelembe veszi, hogy ezt a közösség részeként
kell tennie.

#### A testi és szellemi egyediségben

A Budapest School tanulóközösségeiben a tanulásszervezők felelőssége annak
eldöntése, hogy egy adott közösség milyen mértékben tud befogadni
sérült, vagy sajátos nevelési igényű gyerekeket. Az ő befogadásukkor és a
velük való kiemelt foglalkozáskor mindig arra a kérdésre kell
válaszolni, hogy tudjuk-e garantálni a gyerek fejlődését, elég
biztonságos-e a közeg a számára, és az iskolában való szerepe miként
segíti a többi gyerek fejlődését.

#### A döntéshozásban

A Budapest School-döntéshozatal nem többségi és nem konszenzusos
megállapodások alapján történik. A döntések esélyt adnak arra, hogy
minden kellően biztonságos, elég jó javaslatot ki lehessen próbálni
akkor, ha az nem áll szemben a közösen elfogadott célokkal, és nem sérti
bármely egyén érdekeit olyan mértékben, ami sérti a biztonságérzetét.
