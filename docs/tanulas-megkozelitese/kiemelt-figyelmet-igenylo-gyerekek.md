# A különleges bánásmódot igénylő gyerekek
 
A sajátos nevelési igényű (SNI) és beilleszkedési, tanulási, magatartási nehézségekkel (BTMN) küzdő gyerekeknek joga, hogy képességprofiljuknak megfelelő pedagógiai, gyógypedagógiai, konduktív pedagógiai ellátásban részesüljenek. A gyerekeknek ezt az igényét minden esetben és kizárólag az erre a feladatra dedikált szakértői bizottságok állapíthatják meg vagy zárhatják ki állami vagy magánellátás keretén. A Budapest Schoolban a kiemelt figyelmet igénylő gyerekek tanulásának támogatása tanulóközösségenként eltérhet, de az alábbi alapelvek érvényesítése fontos:
 
- A trimeszterenkénti részletes rubric visszajelzések lehetővé teszik a gyerek haladásának nyomon követését, így a tanulással összefüggő elakadások és iskolai jólléttel kapcsolatos problémák időben történő észlelését is.
 
- A mentoroknak ismerniük kell a gyerekek sajátosságait. A szülőnek és a mentornak őszintén, egymást segítve kell a gyerek egyéni támogatására felkészülniük. Amikor felmerül a sajátos nevelési igény (SNI) ill. beilleszkedési, tanulási, magatartási nehézségek (BTMN) gyanúja, ez sokszor nehéz beszélgetéseket is jelent - ebben a mentorok a gyógypedagógiai felelős (LINK!!!) valamint a gyógypedagógiai koordinátor (LINK!!!) segítségére támaszkodhatnak.
 
- A tanulásszervezésnek, a moduloknak, a foglalkozásoknak a gyerekek képességprofiljának ismeretében differenciáltnak kell lenniük. Fontos, hogy a gyerekek sajátos igényeit a szaktanárok, külsős modultartó tanárok is ismerjék, a személyre szabott támogatás biztosítása érdekében.
 
- Minden folyamatban lévő és lezárult gyógypedagógiai vizsgálatnak kell, hogy legyen visszakereshető dokumentációja. 
 
- A szakvéleményekben javasolt fejlesztőórák a szülőkkel egyeztetve és együttműködve megvalósulnak a gyerek hetirendjével összhangban. A fejlesztés ívét egyéni fejlesztési tervben rögzíti a fejlesztést végző szakember.
 
- A tanároknak együtt kell működniük a gyerek fejlesztésében részt vevő szakemberekkel.

