# Pedagógiai módszerek

A Budapest School iskola tanárainak feladata, hogy mindig keressék azt a
módszert, azt a környezetet, ami az adott gyerekekkel, adott
környezetben, adott időben a leginkább működik. Nem tudjuk megmondani
előre, hogy mikor milyen módszert érdemes választani, de azt tudjuk,
hogy mi alapján keressük a megfelelő technikákat. Vannak olyan
módszerek, amelyek a saját célok lehetőségeinek kitágítását és azok
elérését nagyban támogatják. Ezek alkalmazása javasolt a csoportmunkák
és az egyéni gyakorlások alatt.

Azt is tudjuk, hogy nem baj, ha nem elsőre találtuk meg a megfelelő
módszert, mert a próbálkozások során rengeteg új információt nyerünk,
amelyek segítségével már könnyebb megtalálni a valóban megfelelő
megoldást. Az alábbiakban néhány a Budapest School számára meghatározó
fontosságú módszert emelünk ki.

## Rugalmas csoportbontások

Pedagógiai elvek az osztályok, csoportbontások, foglalkozások megszervezése tekintetében
A BPS Általános Iskola és Gimnázium az Nkt. 4. § 24. pontjában, 25. § (7) bekezdésében, 26. §-ában, 31. § (1), (2) bekezdésében valamint a 20/2012. EMMI rendelet 13. § (1) bekezdésében foglalt felhatalmazása alapján a 20/2012. (VIII. 31.) EMMI rendelet 2. mellékletében definiált eltérő pedagógiai elveket, valamint a 20/2012. (VIII. 31.) EMMI rendelet 7.§ (4) bekezdése szerint definiált eltérő pedagógiai módszereket alkalmaz. A fentiek alapján a BPS csoportbontási és foglalkozás-szervezési elvei a következők:

- alsó tagozaton összevont osztály megszervezése lehetséges;
- az osztályok létszámára vonatkozó minimális rendelkezéseket nem alkalmazza a BPS;
- a tanórákat és foglalkozásokat a BPS van, hogy különböző évfolyamok, különböző osztályok tanulóiból álló csoportok részére szervezi meg és van, amikor egy évfolyamra járó gyerekek csoportjának, azaz főleg az osztályoknak szervez egy foglalkozást;
- a BPS alkalmazza a projektoktatás módszerét, melynek során a témaegységek feldolgozása, a feladat megoldása a tanulók érdeklődésére, a tanulók és a pedagógusok közös tevékenységére, együttműködésére épül.

Magyarul megfogalmazva: a gyerekeket a tanároknak úgy kell csoportokra bontaniuk, ahogy ez szerintük a leginkább támogatja a gyerekek tanulását. Ha egy 10 éves gyerek jól tud egy 14 éves gyerektől tanulni, és mindenki élvezi ezt, akkor egy kevert korosztályú, kooperatív differenciálás a legcélrevezetőbb. Egy emeltszíntű matematika érettségire felkészítő csoportban célszerű az egy tudásszinten lévő gyerekeket egy csoportba tenni.

Tehát amennyiben a különböző évfolyamra járó gyerekek oktatása az adott tantárgyat, foglalkozást vagy modult illetően hatékonyabb az évfolyamonkénti csoportokban történő oktatással szemben, úgy olyan csoportok kialakíthatók, amelyekben a legidősebb és a legfiatalabb gyerek évfolyamának különbsége hatnál nem több.

A különböző évfolyamra járó gyerekek csoportmunkája során is figyelemmel kell lenni a tanulók életkori sajátosságaira. A tanárnak differnciálni kell. Leginkább abban, hogy az eltérő évfolyamszinten lévő gyerekeknek eltérő tanulási eredmények elérése a célja.

A csoportok összeállításakor figyelembe kell venni a közösség minden tagjának, és minden csoportjának, így az osztályok, évfolyamok és tanulóközösségek érdekeit.

## Projektmódszer

Projektmódszert alkalmazó modulok során fő célunk, hogy a gyerekek
aktívak és kreatívak legyenek, és ezért a tevékenységek sokszínűségét
helyezzük fókuszba. Projektmódszer alkalmazásakor is arra bíztatjuk a tanárokat, hogy a legváltozatosabb és legadekvátabb módszereket alkalmazzák.

### A projektmunka folyamata

#### Téma, cél

Az első lépés a projekt céljának és tartalmának meghatározása. Erre javaslatot
tehet a tanár, a gyerekek, vagy akár egy szülő is. Fontos,
hogy a gyerekek a projekt témáját vagy célját már önmagában
értelmesnek, relevánsnak tartsák.

#### Ötletroham

Egy-egy téma feldolgozását csoportalakítással és ötletrohammal
kezdjük. Ennek célja, hogy a résztvevők bevonódjanak, illetve
megmutassák, hogy nekik milyen elképzeléseik vannak az adott
témáról, továbbá milyen produktummal, eredménnyel szeretnék zárni a
folyamatot. A létrehozott produktumoknak csak a képzelet szabhat
határt. Lehetnek videók, prezentációk, fotók, rapdalok, telefonos
applikációk, rajzok, tablók, tudományos cikkek stb.

#### Kutatói kérdés

Ezek után úgynevezett kutatói kérdéseket teszünk fel, melyek
meghatározzák a vizsgálat irányát. A kérdések feldolgozása a
legváltozatosabb módokon történhet. Az egyéni munkától kezdve a
frontális instruáláson vagy a kooperatív csoportmunkán keresztül
egészen a dráma- és zenefoglalkozásokig minden hasznosítható a
tanár, a csoport és a téma igényeihez mérten.

#### Elmélyült csoportmunka

A projekt azon szakasza, amikor a tervek, kutatások alapján az
implementáción dolgozik a csapat.

#### Prezentáció

A létrehozott produktumok bemutatására külön hangsúlyt kell
fektetni. Ennek több módja is lehet: prezentációk, demonstrációk,
plakátok, projektfesztiválok.

Az iskolai projektek célja egy fejlődésfókuszú tanár és gyerek számára
mindig kettős: egyrészt cél a téma feldolgozása, a produktum
létrehozása, másrészt az iskola fő célja, hogy a gyerekek, a csapatok
mindig fejlesszék alkotó, együttműködő, problémamegoldó képességüket.
Ezért a projekt folyamatára való reflektálás, visszajelzés ugyanolyan
fontos, mint maga a cél elérése.

A munka során külön figyelmet kell fordítani arra, hogy mindent
dokumentáljanak a résztvevők. Lehetőleg online felületen.

### A projekt értékelése

A projekt során több értékelési pontot érdemes beépíteni. A
foglalkozások végén a résztvevők visszajeleznek a folyamatra, értékelik
a saját, a csoport és a tanár munkáját. A folyamat végén az egész
projektfolyamatot értékelik, szintén kitérve a saját, a csoport és a
tanár munkájára. A produktumok, az eredmény értékelése csoport- és
egyéni szinten is megtörténik.

Az értékelés a folyamatra fókuszál, és nem csak az eredményre, hogy
fejlessze a fejlődésfókuszú gondolkodásmódot.

## Önszerveződő tanulási környezet

A Sugata Mitra által kialakított módszertan (angolul Self Organizing
Learning Environment) lényege, hogy a tanárok arra bátorítják a
gyerekeket, hogy csoportban, az internet segítségével _nagy kérdéseket_
válaszoljanak meg. A jó kérdés az, amire nem egyszerű a válasz, sőt
lehet, hogy nincs is rá egyfajta válasz. Cél, hogy a gyerekek maguk
alakítsák a folyamatot, formálják a kérdést, és találjanak válaszokat.

- A tanár kialakítja a teret: körülbelül négy gyerekre jut számítógép, amit körbe lehet ülni.
- A gyerekek maguk formálják a csoportjukat, sőt még csoportot is válthatnak a munka során. Mozoghatnak, kérdezhetnek, „leshetnek" más csoportoktól.
- Körülbelül 30--45 perc után a csoportok prezentálják a kutatásuk eredményét.

#### A jó kérdések

A _nagy kérdések_ nyílt és nehéz kérdések, és
előfordulhat, hogy senki sem tudja még rájuk a választ. A cél, hogy mély
és hosszú beszélgetéseket generáljanak. Ezek azok a kérdések, amikre
érdemes nagyobb elméleteket állítani, amelyeket jobb csoportban megvitatni,
amelyekről érvelni lehet és kritikusan gondolkodni.

A jó kérdések több témát, területet (tantárgyat) kapcsolnak össze: a
_„Mi a hangya"_ kérdés például nem érint annyi különböző területet,
mint a _„Mi történne a Földdel, ha minden hangya eltűnne?"_ kérdés.

#### Fegyelmezés nélkül

A tanár feladata a folyamat során meghatározni a _nagy kérdést_, és
tartani a kereteket. A cél, hogy a gyerekek maguk szervezzék saját
munkájukat, így minimális beavatkozás javasolt a tanár részéről.
Kezdetben, gyakorló csoportoknál, a tanárnak sokszor kell emlékeztetnie
magát, hogy idővel kialakul a rend. _„Bízz a folyamatban!"_ Amikor a
tanár úgy látja, hogy nem megy a munka, akkor csak finoman emlékezteti a
csoportokat, hogy lassan jön a prezentáció ideje. Amikor valaki a
csoportjáról panaszkodik, akkor elmondhatja, hogy szabad csoportot
váltani. Ha valaki zavarja a többieket, akkor megfigyelheti, hogy a
gyerekek tudnak-e már konfliktust feloldani. Ha valaki nem vesz részt a
munkában, akkor gondolkozhat olyan kérdésen, ami az éppen demotivált
gyerekeket is bevonzza.

## Az önálló tanulás

Budapest School [célja](../iskola-celja/emberkep.md#emberkep), hogy a _„gyerekek az iskola befejeztével önállóan és kreatívan gondolkodó, önmagával és közösségével integránsan élő érett nagykorúvá váljanak"_. Például az érettségi évére a gyerekek az iskolában _„már megtanulnak szakaszosan célokat állítani"_ és _„önállóan készülnek az érettségire"_. Az önálló tanulás képességét már a legkisebb kortól kezdve folyamatosan gyakorolni, fejleszteni kell. Nem az a kérdés, hogy tud-e egy gyerek önállóan tanulni, hiszen járni és beszélni is önállóan tanult minden gyerek. A fő kérdés, hogy hogyan tud egyre nagyobb célokat elérni, egyre nehezebb képességeket, összetettebb tudást megtanulni és egyre komplexebb projektekben részt venni. A BPS modell feltételezi, hogy mindenki képes az önálló tanulásra, és mindenki tudja fejleszteni ezt a képességét. A BPS modell [négy tanulási szakasza](../iskola-celja/emberkep.md#emberkep) tulajdonképpen az önálló tanulási képesség négy szintjét írják le.

A modell a tanulás megközelítésével és strukturálásával is támogatja az önálló tanulás képességének fejlesztését. Legkisebb kortól kezdve gyakorolják a gyerekek a [saját cél állítást](/tanulasi-elmeny/sajat-celok.md#sajat-tanulasi-celok). Az erős keretek és határokon belüli _választás szabadságát_ a [moduláris tanmenet](/tanulasi-elmeny/modulok.md#tanulasi-tanitasi-egysegek-a-modulok) biztosítja. És a közösségi kultúra része a rendszeres és [folyamatos reflexió](/tanulasi-elmeny/visszajelzes-ertekeles.md#visszajelzes-ertekeles). Nem utolsó szempont, hogy a BPS modell teret ad a tanároknak önállóan, kreatívan és alkotó módon hozzáállni a munkájukhoz. Az önállóság, kreativitás és reflexió a kultúra, azaz a mindennapi működés része kell hogy legyen.

Ezért is fontos, hogy a gyerekek egyre többet gyakorolják az _önálló tanulást, azaz azokat a célorientált tevékenységeket, amikor nem a tanár (szülő) határozza meg hogy mikor, kivel és hogyan tanul egy gyerek._

> A házi feladat, az otthoni tanulás, a könyvtárban tanulás, a tanulószobai tanulás, az online tanulás, az egyéni kutatás, a szakirodalom feldolgozás, a tanulókörös tanulás, a korrepetálás, az iskolaújság készítése, a saját projekteken dolgozás, a fordított/tükrözött osztályterem, mind-mind olyan elfoglaltságok, amikor a gyerekek maguk irányíthatják a saját tanulási és alkotási folyamataikat.
>
> Az iskola szempontjóbál önálló tanulásnak tekinthető az is, amikor a gyerek önállóan beiratkozik egy nyári táborba, ahol robotikát tanul, délutáni iskola utáni tanfolyamon vesz részt, vagy épp egy másik intézményben készül a nemzetközi érettségire és felvételire.

Az önírányított, önálló tanulási módokat a BPS modell a tanulási élmény ugyanolyan fontos elemének tartja, mint a tanárok által vezetett foglalkozásokat. Például ugyanolyan értékes (tanulási) eredménynek kell tekinteni, ha egy gyerek egy matematika tanórán egy tanártól hall a _logikai szitáról_, ha a tankönyből szerzi ismereteit a tanulószobán, vagy ha egy online tananyagból tanul erről a fogalomról, például a [Nemzeti Köznevelési Portálról](https://portal.nkp.hu/Search?keyword=logikai%20szita), esetleg a [Khan Academy](https://www.khanacademy.org/math/statistics-probability/probability-library/basic-set-ops/e/basic_set_notation) oldalán, vagy ha társától tanulja meg, mit takar ez a fogalom, és hogyan lesz számára hasznos a céljai elérésében.

### Egyéni tanulási idők

Az önálló tanulás egyik megjelenése az _egyéni tanulási idő (ETI)_ vagy az _ügyfélszolgálat_ nevű tanóraként meghirdetett és vezetett (tehát pedagógus munkakörben alkalmazott tanár által vezetett) foglalkozás típusok: ilyenkor a gyerekek több tantárggyal foglalkozhatnak egyszerre, mindenki hozza a saját kérdését, feladatát és a csoportban a gyerekek együtt vagy egymástól tanulnak. A tanár pedig azon kívül, hogy biztosítja a kereteket és a biztonságot, igény esetén elérhető is.

## Megfontolt gyakorlás

Ahogy Anders Ericsson pszichológus [1993-ban megjelent, szerzőtársakkal írt tanulmánya](http://projects.ict.usc.edu/itw/gel/EricssonDeliberatePracticePR93.PDF) is kimutatja, bárki tudja valamennyi készségét, képességét fejleszteni, ha megtervezetten, megfontoltan gyakorolja. A Budapest School azt az álláspontot képviseli, hogy gyakorlással — a hagyományos készségtárgyakhoz hasonlóan — sokféle képesség és tudás fejleszthető. Tehát odafigyeléssel és megfontolt gyakorlással egy gyerek egyre jobban fog tudni írásbeli érettségi vizsgát tenni magyarból, geopolitikai elemzéseket végezni, hiperbolikus függvényekkel egyenletet
megoldani, domináns csoporttagokkal együttműködni vagy stresszhelyzetben
önmagát lenyugtatni.

A készség- és képességfejlesztés legjobb eszköze a megtervezett
gyakorlás: a fejlődés érdekében okosan gyakorlunk. A megfontolt
gyakorlás jellemzője:

- **Világos és specifikus cél** Fontos, hogy tudjuk, mit gyakorlunk, mit akarunk elérni. Lehetőleg a
  cél legyen mérhető és reálisan elérhető.

- **Fókusz** Gyakorlás során egy dologra érdemes figyelni.

- **Komfortzónán kívül kell lenni** Az edzőnek, tanárnak, trénernek néha érdemes a tanulót kicsit „nyomni". Emlékeztetni, hogy mindig lehet kicsit többet elérni.

- **Folyamatos visszajelzés** Nagyon gyakran kap a tanuló visszajelzést, mindig tudja, hogy mikor
  és miben fejlődött.
