# Mit tanulnak a gyerekek a Budapest School-ban?

A Budapest School-ban a gyerekeink három dolgot tanulnak meg:

1. Megtanulják, hogyan lehetnek jól, vagyis hogyan élhetnek harmóniában önmagukkal és környezetükkel, és hogyan fejleszthetik tudatos gyakorlással testi és lelki egészségüket, boldogságukat.

2. Megtanulnak tanulni, fejlődni és eredményeket elérni: gyerekeink aktívan és önállóan tanulnak, gondolkodnak, kreatívan oldanak meg problémákat, átlátják az összefüggéseket. Képesek önállóan célokat kitűzni és azokat egyénileg vagy csapatban elérni, ami megmutatkozik konkrét eredményeikben és projektjeik sikerében.

3. És megtanulnak tenni másokért: képesek odafigyelni másokra, jó hatással vannak a környezetükre, felelősséget vállalnak, kapcsolódnak másokhoz, gazdagabbá teszik a közösségeiket és a társadalmat, amelyben élnek.

Ehhez a gyerekeknek alapvető képességeiket kell fejleszteniük, és mélyreható, tematikus ismereteket is el kell sajátítaniuk.

## Alapvető képességek

1. Komplex gondolkodás - A 21. század kihívásaiban kulcsfontosságú, hogy a gyerekek képesek legyenek logikus, innovatív és objektív döntéseket hozni egy változékony, bizonytalan, összetett és többértelműség dominál világban. Ehhez a egyszerűen foglamazva okosnak kell lennüik: elemezniük kell a rendelkezésre álló információkat, értelmezni az összefüggéseket és saját életük, valamint környezetük javát szolgáló, új megoldásokat kell találniuk. Ez segíti őket saját jóllétünkben, akadémiai sikereikben és környezetük pozitív alakításában.

2. Önálló tanulás, fejlődés - A mondás szerint “Jó pap holtig tanul”. 21. századi gyerekeinknek hatékony tanulási és alkalmazkodási képességekre van szükségük, hiszen az iskolai sikerikhez sem egy előre meghatározott egyenes út vezet, ráadásul jövőbeli szakmájuk talán még nem is létezik. Minden új eredményhez személyreszabott és alapos tanulás vezet. Ezért nem elég, hogy tudják, mit szeretnének tanulni, hanem az is elengedhetetlen, hogy képesek legyenek megtanulni azt, amire szükségük van.

3. Célok elérése – A gyorsan változó világban, ahol a jövő kihívásai folyamatos alkalmazkodást és újratervezést igényelnek, gyerekeinknek képesnek kell lenniük határozott célokat kitűzni és azok eléréséért tudatosan és hatékonyan cselekedni. Aktívan formálniuk kell a jövőjüket, tenni saját és mások jóllétéért. Cselekednek, végrehajtanak, eredményeket érnek el, nem csak álmodoznak. Kihívást jelentő célokat tűznek ki maguknak, amelyeket kemény munkával, hatékony stratégiákkal és mások segítségével sikeresen el is érnek.

4. Kommunikáció – A kapcsolatokban, a családban, a közösségekben és a társadalomban a kommunikáció köt össze minket embereket. Gyerekeink akkor tudnak igazán jól lenni és jó hatással lenni, ha képesek partneri módon, kedvesen és határozottan kommunikálni, legyen szó szemtől szembeni beszélgetésről, kisebb vagy nagyobb nyilvánosságról, vagy az online térben való kommunikációról.

5. Kreatív problémamegoldás – Gyerekeinknek a 21. században folyamatosan olyan új kérdésekre és problémákra kell választ találniuk, amelyek a korábbi generációk számára még ismeretlenek voltak. Közben az automatizáció és az AI terjedésével a rutin feladatokat egyre inkább gépek végzik. Nem elég, ha gyerekeink egyértelmű válasszal igénylő vizsgafeladatokra tudnak helyesen válaszolni. Ahhoz, hogy értéket tudjanak létrehozni és megoldásokat találjanak, meg kell tanulniuk más szemszögből látni a dolgokat, és képzeletüket használva új ötleteket létrehozni.

6. Szociális és érzelmi intelligencia - Az EQ segít jobban megérteni és kezelni az érzelmeket, hatékonyabban kommunikálni, és erősebb kapcsolatokat kialakítani. A magas érzelmi intelligenciával rendelkező emberek hatékonyabban kommunikálnak, jobb velük együttműködni, jobb vezetők és jobb barátok, ráadásul az EQ nagyobb hatással lehet a tanulási teljesítményre és a későbbi sikerekre, mint az IQ. Ezek a képességek elengedhetetlenek a személyes boldogsághoz, egészséges kapcsolatokhoz és a társadalmi kohézióhoz.

7. Adatok, evidenciák és információkezelés - Ma már nem elég kritikusan, mérlegelve gondolkodni: gyerekeinknek képesnek kell lenni objektív, megalapozott döntéseket hozni, különbséget tenni a lényeges és lényegtelen, az igaz és a nem igaz között, valamint felismerni az evidenciákat torzító tényezőket.

## Tematikus ismeretek

A Budapest School-ban nem kívánunk részt venni az “általános képességfejlesztés vs. lexikális tudás” vitában; számunkra egyaránt fontos a képességek fejlesztése és, hogy gyerekeink mély ismeretek szerezzenek meghatározott témákban és tudományterületeken.

1. Természettudomány és fenntarthatóság
Azt szeretnénk, hogy gyerekeink megértsék a Földet és az univerzumot alakító alapvető törvényszerűségeket, valamint képesek legyenek befolyásolni a körülöttünk lévő világot, együtt élni a környezetükkel. Komplex jelenségeket interdiszciplináris megközelítéssel látnak át és fenntartható, környezettudatos életmódot folytatnak, hozzájárulva az emberiség és bolygónk jövőjének védelméhez és aktívan hozzájárulnak környezetük jobbá tételéhez.

*Mit tartalmaz ez a terület?*

- A Földünket és az univerzumot alakító alap-törvényszerűségek megértése	
- Tudományos gondolkodás , vizsgálat
- Komplex jelenségek interdiszciplináris megértésének képessége
- Fenntartható, környezettudatos életmód

2. Kultúra és társadalom
Az egyre behálózottabb világban gyermekeinknek különböző kultúrákat és társadalmi hálózatokat kell megérteniük. Az állandóan változó családi, munkahelyi és társadalmi környezetben fontos, hogy stabil identitásukra építve képesek legyenek kapcsolódni, megérteni, és együtt élni embertársaikkal.
Ahhoz, hogy aktív alakítói legyenek a társadalmi folyamatoknak és megismerjék globális kihívásainkat, érteniük kell történelmünket, saját kultúránkat és más kultúrák szerepét. Képesnek kell lenniük kifejezni magukat, akár szavakkal, akár a művészet eszközeivel.

*Mit tartalmaz ez a terület?*

- Saját múltunk, eredetünk, identitásunk
- Társadalmi folyamatok és jelenségek
- Mai lokális és globális kihívások megismerése a múlt kontextusában
- Kulturális diverzitás és a kultúra mint az emberi viselkedést leíró eszköztár megismerése
- Az etika és a morál alapkérdéseinek felfedezése és megkérdőjelezése

3. Művészet és innováció
A művészet és az innováció kulcsfontosságú a kreativitás és az önkifejezés fejlesztésében, amelyek alapvetőek a személyes fejlődés és a társadalmi változások szempontjából. A művészetek új perspektívákat és kifejezési módokat kínálnak, segítve a gyerekeket abban, hogy jobban megértsék önmagukat és növeljék önbizalmukat. Emellett lehetőséget biztosítanak a különböző kultúrák megismerésére, ami empátiát és nyitottságot fejleszt. Az innováció és technológia ötvözése révén a gyerekek megismerkedhetnek a digitális művészetekkel, kódolással és dizájnnal. A hagyományos és modern művészeti eszközök használata lehetővé teszi számukra, hogy kreatív ötleteiket valósággá alakítsák. A Budapest School célja, hogy a gyerekek aktív és kreatív résztvevői legyenek a világnak, hozzájárulva saját és környezetük fejlődéséhez

*Mit tartalmaz ez a terület?*

- Amikor valami újat hozunk létre
- Médiahasználat
- Művészetek stílus és formavilága,
- Művészetek mint önkifejezési eszköz a vizuális (hagyományos képzőművészetektől a digitális művészetekig) és előadóművészetekben (dráma, tánc, zene)
- A művészetek és az irodalom funkciójának megértése és a mindennapi életbe való beépítése
- Innovatív fejlesztésekhez szükséges technológiai (kódolás) és gyakorlati (maker) eszköztár megismerése és alkalmazása
- Design


4. Idegennyelvek, globális állampolgárság
Egy globális világban felnőve, gyermekeinknek együtt kell élniük és dolgozniuk sokféle emberrel különböző kultúrákból. Az idegennyelvek ismerete lehetővé teszi számukra a hatékony kommunikációt és együttműködést, ami elengedhetetlen a sikerhez mind a munkahelyi, mind a személyes kapcsolatokban. A globális szemlélet pedig segít abban, hogy megértsék a világ összefonódását és saját szerepüket benne, így képesek lesznek lokálisan is hatással lenni, miközben globálisan gondolkodnak. Ez a tudatosság növeli empátiájukat, kulturális érzékenységüket és társadalmi felelősségérzetüket, amelyek szükségesek a jövő kihívásaival való szembenézéshez. Céunk, hogy gyermekeink nyitottak, toleránsak és aktív résztvevők legyenek a globális közösségben, így hozzájárulva egy békésebb és összetartóbb világ kialakításához.

*Mit tartalmaz ez a terület?*

- Írott és beszélt idegen nyelvi kommunikáció
- Interkultúrális kommunikáció
- Globális állampolgárság


5. Testi és lelki harmónia
A Budapest Schoolban külön tantárgyként kezeljük a testi és lelki harmóniát, mert ma már tudjuk, hogy az egészség és boldogság tanulható. Az utóbbi évek kutatásai megmutatták, hogyan élhetünk hosszabb, egészségesebb és kiegyensúlyozottabb életet. Ezért tanítjuk a gyerekeknek, hogyan működik a testük és az elméjük, valamint hogyan maradhatnak egészségesek és boldogok. Foglalkozunk a szexualitás, a függőségek, a mobiltelefonok hatásai, az egészséges táplálkozás és a testmozgás témáival. Az extra fókusz segít nekik tudatos és kiegyensúlyozott életet élni.

*Mit tartalmaz ez a terület?*

- Önismeret és önbizalom
- Rugalmasság (rezíliencia)
- Egészséges testi fejlődés
- Saját igényekhez képest megfelelő táplálkozás
- A természettel való kapcsolódás

A tantárgyak a Budapest Schoolban a tanulás tartalmának minimumát jelölik ki, a tantárgyi struktúrát a Nemzeti alaptanterv határozza meg. Az iskola a tantárgyakon átívelő fejlesztési területeket határoz meg. 

Az iskolában egy foglalkozás egyszerre több fejlesztési irányelvnek is megfelelhet.
Egy modul során gépesíthetünk egy ökológiai tangazdaságot (Természettudomány) úgy, hogy közben
megismerjük a gazdálkodás történelmi és szocio-kulturális hátterét
(Kultúra és társadalom), és a szabad levegőn végzett munkával a testünket és a lelkünket
egyaránt ápoljuk (Testi és lelki harmónia).

