Budapest School Modell célja, hogy az iskola a lehető legjobb módon támogassa gyerekeket azt tanulni, amit szeretnek vagy amire szükségük van. Ez a magyarországi jogi keretek között többféle intézménytípusban is megvalósulhat. A BPS Modell minden intézménytípusban ugyanazt a személyreszabott, önírányított tanulási környezetet határozza meg. Eltérések három okból vannak az intézményeink között

1. a BPS modell lényege, hogy nagyon sok döntést az iskola szintjén hoznak meg a tanárok, akik arra vannak bátorítva, hogy a BPS Modell keretein belül alakítsák ki a saját módszerüket, megközelítésüket, napirendjüket, hagyományaikat, kultúrájukat a gyerekekkel és a szülőkkel közösen. A tanulóközösségeknek így kialakulnak sajátosságai.

2. A különböző intézménytípusokban más tantárgyi tanulási eredményeket határoznak meg a jogszabályok. Ezekhez a keretekhez értelemszerűen igazodik az iskola.

3. Az önálló tanulási, a konfliktuskezelési és együttműködési képességek szintje idővel változik a gyerekeknek. Természetes, hogy más tanulási környezet segíti a 8 éves és a 14 éves gyereket.

A [Budapest School Általános Iskola és Gimnázium](/iskola-tipusok/altgim.md) program szerint működő tanulóközösségek alap ,,polihisztorképzők". A program csak annyit mond, hogy mindenből egy kicsit, — vagy másképp mondva — mindent kiegyensúlyozottan felkínál az iskola a gyerekeknek.

Minden iskolában kialakulhat specializálódás, amikor egy-egy gyerek egy-egy területen nagyon mélyre megy. A [Budapest School Code Program](/iskola-tipusok/code21.md) alapján működő tanulóközösségek már induláskor a szoftverfejlesztést mint extra érdeklődést választották magukénak. Itt a gyerekek az általános tudáson kívül még azt is eldöntötték, hogy a szoftverfejlesztésre extra figyelmet szentelnek. Mert ez érdekli őket. Az iskola azt biztosítja, hogy hasonló érdeklődésű gyerekek tudjanak egy közösségbe tartozni.
