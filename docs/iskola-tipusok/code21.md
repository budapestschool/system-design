# `BPS Code`, egy régi iskolatípus a jövő generációjának – újragondolt technikum

A `BPS Code` versenyképes, a mai kor szakmai igényeinek megfelelő és a jövő kihívásaira válaszokat adó technikumi program. Az iskolában a diákok örömmel tanulnak, egészségesen fejlődnek, és már tanulmányaik során kapcsolódnak a társadalomhoz, hogy a közösség hasznos tagjává válhassanak. Olyan iskola, amely egyszerre készít fel arra, hogy az onnan kikerülő diákok szakemberként megállhassák a helyüket, és arra, hogy önálló és felelős döntéseket hozhassanak felnőtt életük és pályájuk megkezdésének szakaszában. Ehhez a következő kiemelt fejlesztési területekre fókuszál az iskola:

- **Kreatív innováció.** A technikum első évétől megjelenik termékek és szolgáltatások tervezésének formájában. A tanulók valódi, aktuális problémákra válaszokat kereső projekteken dolgoznak, melyek kialakításában ők is aktívan szerepet vállalnak.

- **Szaktudás**. A tanulók a programozás, a digitális kézművesség és alkotás egyes szakterületein magas képzettséget szereznek, kompetenciáik kiterjednek a területen használt nyelvezetre, a legkorszerűbb eszközök és módszerek alkalmazására. Ezzel a piacon komoly versenyelőnyük lesz.

- **Agilis, flexibilis struktúra.** A tanulásszervezés modern eszközei támogatják a személyre szabott, az egyéni igényeket kiszolgálni és a lehetőségeket fejleszteni tudó tanulást. A diákok tanulmányaik során felkészülnek arra, hogy szaktudásuk a világ változásaihoz adaptálódjon.

- **[T-alakú személyiségjegyeket](https://en.wikipedia.org/wiki/T-shaped_skills)** fejlesztünk: A diákok képesek elmélyülten haladni a szakterületükön, miközben más szakterületen működő emberekkel is könnyedén kooperálnak. Így válnak képessé komplex problémák megoldására.

## Alkotunk és kódolunk – iskolát építünk

A `BPS Code` techinum a BPS modell alapján működik. Ez egy modern, nyílt iskolaplatform, amely többféle specifikus, téma(szakma)orientált tanulási műhely (Budapest School terminológiában: tanulóközösség) létrehozását támogatja.

A BPS modell szerint működik más intézmény is, általános iskola, gimnázium és a `BPS Code` technikum is. A technikumba járó gyerekek és a programozás iránt érdeklődő gimnazisták tanulási élménye között lehet, hogy nincs is különbség, még ha különböző intézménybe is járnak.

- Egy hatosztályos „erős” gimnázium világversenyt nyerő robotika szakköre remekül tud együtt dolgozni egy technikumban duális képzésen programozást tanuló csoporttal az önvezető Golf autók átalakításán, hogy segítsék az idős emberek boltba jutását.
- Egy digitális médiát technikumban tanuló gyerek együtt tanul emelt színtű biológiát az „elit” gimnáziumba járókkal, ha éppen úgy dönt, hogy pszichológiát akar egyetemen tanulni.

## BPS alapelvek szerint működő technikum

A BPS modell a [tanulás hét pillére](/iskola-celja/alapelvek.md#a-tanulni-tanulas-het-pillere) támaszkodik. Ezek a `BPS Code` esetében a következőket jelentik.

1. Az iskola rugalmas és integratív, a tanulók fejlődéséhez igazodó személyreszabott tanulási környezetet nyújt.
2. A tanulás önvezérelt és aktív folyamat.
3. A tanulás az iskolában kezdődik és a világban folytatódik.
4. A tanulás mércéje épp annyira egymás segítése, mint az egyéni eredmények elérése.
5. A tanulás projektek mentén halad.
6. Egymásra figyelünk, együtt tanulunk közösség vagyunk.
7. A tanárok a tanulás szervezik, segítik, a tanulók partnerei.

## Új iskolamodell – tanulási tesztkörnyezet

Néhány strukturális részletben egyedi megoldásokat tesztel az iskola. A ma már 340 gyerek tanulását segítő, 2015 óta fejlesztett Budapest School Modell alapján működik a technikum. Ezek elemeit fejlesztjük tovább most, és alkalmazzuk a Szakképzés 4.0 koncepció startégiája alapján. Ennek a tanulási tesztkörnyezetnek a létrehozását az ITM támogatja, az eredményeket folyamatosan monitorozza, ezzel lehetőséget biztosítva arra, hogy a tapasztalatok akár 1-2 éven belül alkalmazhatóak legyenek más iskolákban is.

Az iskola több helyszínen működik. A gyerekek az iskolában kisebb közösségekbe tartoznak, a
[_tanulóközösségekbe_](/tanulasi-elmeny/tanulo-kozosseg.md#a-budapest-school-tanulokozossegei). Ezek a közösségek több évfolyamot átölelő, 6-60 fős tanulási közösségekként működnek. A tanulóközösségek se jogi, se szervezeti szempontból nem önálló iskolák. A tanulóközösség nem más, mint az iskola egyik szervezeti egysége, ami önálló identitással, sajátos szubkultúrával rendelkezhet.

A kisközösségükön belül a gyereket többfajta csoportbontásban tanulnak és különböző közösségek tagjai is összeállhatnak egy-egy foglalkozásra, kurzusra, de identitásban, ,,egymáshoz-tartozás érzésben" a tanulóközösség az elsődleges közösségük. A közösség tagjai együtt és egymástól tanulnak.

Az egyes tanulóközösségeket [_tanulásszervező_ tanárok (tanulásszervezők)](/tanulasi-elmeny/tanari-szerepek.md#tanulasszervezo)
csapata vezeti. Minden gyereknek van egy kitüntetett tanára, a
[_mentora_](/tanulasi-elmeny/tanari-szerepek.md#mentor), aki egyéni figyelmével a fejlődésben segíti. Minden gyerek a
mentortanára segítségével és a szülők aktív részvételével
trimeszterenként meghatározza a [_saját tanulási céljait_](/tanulasi-elmeny/sajat-celok.md#sajat-tanulasi-celok).

A tanulásszervezők [_modulokat_](/tanulasi-elmeny/modulok.md#tanulasi-tanitasi-egysegek-a-modulok) hirdetnek ezen célokból, és a tantárgyak tanulási eredmények alapján. A modulok reflektálnak a mai világ alapvető
kérdéseire, integrálják a tudományterületeket és művészeti ágakat, azaz
a tantárgyakat, és egyenlő lehetőséget adnak a tudásszerzésre, az önálló
gondolkodásra és az alkotásra a gyerekek mindennapjaiban.

A modulok végeztével a gyerekek eredményei bekerülnek saját
[_portfóliójukba_](/tanulasi-elmeny/portfolio.md#portfolio), melyek tartalmazhatnak önálló vagy csoportos
alkotásokat, tudáspróbákat, vizsgafeladatokat, egymás felé történő
visszajelzéseket, a fejlődést jól mérő dokumentációkat vagy bármit,
amire a gyerek és tanárai büszkék vagy amit fontosnak tartanak. Erre a
portfólióra épül a Budapest School [_visszajelző és értékelő rendszere_](/tanulasi-elmeny/visszajelzes-ertekeles.md#visszajelzes-ertekeles).

A gyerekek mindennapjait meghatározó modulok több műveltségi területet,
többféle kompetenciát, több tantárgy anyagát is lefedhetik, és egy
tantárgy anyagát több modul is érintheti. Ezért is mondhatjuk, hogy a
BPS iskolákban a tantárgyközi tevékenységek vannak
előtérben. Az iskola szándéka, hogy a gyerekek folyamatosan
fejlődjenek a világ tudományos megismerésében (STEM), a saját és mások
kulturális közegéhez való kapcsolódásban (KULT), valamint a testi-lelki
egyensúlyuk fenntartásában (Harmónia), vagyis a [_kiemelt tantárgyközi
fejlesztési területekben_](/tanulas-megkozelitese/kiemelt-fejlesztesi-teruletek.md#kiemelt-fejlesztesi-teruletek).

Az iskola szerint az a tanárok döntése, hogy a gyerekek matematika vagy digitális kultúra órán
foglalkoznak algoritmusokkal, vagy algoritmusok órán foglalkoznak matematikával. A
iskola annyit határoz meg, hogy a 9–12. évfolyamszinten matematika
tantárgyhoz kapcsolódóan 135 különböző tanulási eredményt kell elérni, és
algoritmusokkal kapcsolatban pedig 11 különböző tanulási eredményt több
különböző tantárgyból.

Tehát a tantárgyak a tanulás tartalmi elemeinek forrása és keretei: a
tanulandó dolgok halmazaként működik. Az, hogy milyen csoportosításban
történik a tanulás, az a szaktanárokra van bízva. A gyerekek lehet,
hogy csak félévente, az elszámolás időszakában találkoznak a tantárgyak
taxonómiájával. Ebben az időszakban veti össze minden gyerek és mentor,
hogy amit tanultak, alkottak és amiben fejlődtek, az hogyan viszonyul a
társadalom és a törvények elvárásaival, a Nemzeti alaptantervvel.

Az iskola a NAT tantárgyak és a _szoftverfejlesztés és -tesztelés képzési kimeneti követelményeinek_ témaköreit, tartalmát és követelményeit
_tanulási eredmények_ halmazaként adja meg. A gyerekek feladata az
iskolában, hogy tanulási eredményeket érjenek el és így sajátítsák el a
tantárgyak által szabott követelményeket. Tanulási eredményeket modulok
elvégzésével (is) lehet elérni, tehát a modulok elsődleges feladata,
hogy a tanulási eredményekhez vezető utat mutassák.

Az iskolában egyszerre jelennek
meg a NAT tantárgyi elvárásai, a
közoktatást szabályozó törvények szándékai, szoftverfejlesztés és -tesztelés képzési kimeneti követelményei, a gyerekek saját céljai és a mai
világra való integrált reflexió.

### A tanulás rendszerszemléletű megközelítése

Az oktatás tartalmának előzetes szabályozása helyett a BPS Modell — és így a `Code21` technikum is — a
tanulás módjára helyezi a hangsúlyt. Az iskola alapelve, hogy integratív
módon folyamatosan keresse és fejlessze a pedagógiai, pszichológiai és
szervezetfejlesztési módszereket, amelyek korszerű módon tudják segíteni
a tanulás tanulását, az egyéni és csoportos fejlődést, a konfliktusok
feloldását.

A tanulás tartalmát tekintve a `BPS Code` a NAT tartalmára és a szoftverfejlesztés és -tesztelés képzési kimeneti követelményeire
támaszkodik. A Budapest School Modell pedig a tanulás rendszerét, annak folyamatát szabályozza.

## Eltérések a megszokott iskoláktól

### Mentor, tanulóközösség közösség

Az iskola alapegysége a [tanuló közösség, ami egy 6-60 fős kevert korosztályú közösség](/tanulasi-elmeny/tanulo-kozosseg.md). Ők folyamatosan az igények és a feladatok mentén újjáalakuló csoportbontásokban tanulnak. Vagyis a csoportok, a feladatok és projektek, valamint a tanulási célok, és nem évfolyamok szerint szerveződnek. A közösséget [3-5 fős tanárcsapat](/tanulasi-elmeny/tanulo-kozosseg.md#a-tanulokozossegeket-tanarok-vezetik) vezeti, akik a specifikus feladatokhoz szaktanárokat hívnak be. Minden gyereknek van [mentora](/tanulasi-elmeny/tanari-szerepek.md#mentor), aki a tanulási céljai meghatározásában, érzelmi és társas biztonságában, valamint a tanulási útja követésében segíti mentoráltjait.

| Iskola, amibe mi jártunk                                               | Iskola, ahol a gyerekeink tanulnak                                                                                              |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Osztályfőnök                                                           | Mentortanár                                                                                                                     |
| Életkor tekintetében homogén, korcsoport alapján szervezett osztályok. | Életkor tekintetében heterogén, a tanulás célja és az alapító okiratban lefektetett kritériumok alapján szerveződött csoportok. |
| A tanulók osztályok szerint általában együtt tanulnak.                 | A tanulók differenciáltan szervezett csoportbontásban tanulnak.                                                                 |
| Az ismeretek tantárgyak szerint szegmentáltak.                         | Az ismeretek komplex projektek köré szervezett, interdiszciplináris modulok.                                                    |

### Moduláris tanulás

A gyerekek tanulnak, gyakorolnak, projekteken dolgoznak, a NAT tanulási eredményei, illetve a saját maguk, vagy a tanáraik által meghatározott tanulási eredmények elérése érdekében. Tanulásuk – a tantárgyak tanulási eredményeit is figyelembe vevő – tanulási modulok során történik. Ezek egyes esetekben a tantárgyak alkotóelemeire vagy [összevont, tantárgyközi tartalmakra](/tanulasi-elmeny/modulok.md#tanulasi-tanitasi-egysegek-a-modulok) épülnek. Például: angolul Javascriptet tanulni egyszerre angol nyelvi és programozásóra is.

A tantárgyfelosztást és a rögzített óraszámokat felváltja az interdiszciplináris tanulási modulok, alakítható tanulási utak, negyedévenkénti – NAT által meghatározott tanulási eredmények alapján történő – tudásmérés.

| Iskola, amibe mi jártunk                                              | Iskola, ahol a gyerekeink tanulnak                                   |
| --------------------------------------------------------------------- | -------------------------------------------------------------------- |
| Egész tanévre szóló tantárgyfelosztás és órarend .                    | Negyedévre szóló órarend, amiben interdiszciplináris modulok vannak. |
| A tanítási-tanulási folyamatszervezés szaktanár-szaktantárgy fókuszú. | A tanítási-tanulási folyamatszervezés tanár-diákközösség fókuszú.    |

### Önálló tanulás

Az iskola külön értéknek tekinti, ha a gyerek [önállóan](/tanulas-megkozelitese/pedagogiai-modszerek.md#az-onallo-tanulas), hiteles források felhasználásával, például videókból tanulja meg a természettudományos alapismereteket. A tanári szerep ekkor nagyobbrészt a tanulás facilitálására, az új lehetőségek megmutatására, a folyamatos kihívásban tartásra irányul, és kevésbé a tudásátadásra.

| Iskola, amibe mi jártunk                                                      | Iskola, ahol a gyerekeink tanulnak                                                                                                            |
| ----------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Egységes órarend, kínálatvezérelt, választható szakkörök, fakultációk.        | Igény- és szükségletközpontú egyéni órarend a modulokból - megadott kritériumok szerint összeállítva.                                         |
| A haladás tempója egységes.                                                   | Az egyéni haladási utak képesség- és érdeklődés függvényében jelentősen eltérhetnek egymástól.                                                |
| A tananyagban való haladást osztálynaplóban rögzíti a tanár.                  | A tananyagban való egyéni haladást a tanulók és tanárok a portfóliójukban rögzítik.                                                           |
| A minősítést az értékelések alapján adott érdemjegy jelenti.                  | A minősítés alapját a pedagógiai és tematikus szakaszonként adott szöveges értékelések képezik, amelyek érdemjegyre válthatók.                |
| A tanuló továbbhaladásának feltétele az elégséges érdemjegy.                  | A tanuló továbbhaladásának feltétele a tantárgyi követelmények legalább 40%-os teljesítettsége.                                               |
| Formális tanulási alkalomnak a tanár által vezetett foglakozások tekinthetők. | Formális tanulásnak tekinthető minden tanár által vezetett vagy csak kontrollált, önálló, kis csoportos, a tanulók által vezérelt tanulás is. |

### Szakismeretek már a technikum első évétől – rugalmas tanulási struktúra

Ha a diákokat egy téma nagyon érdekli, akkor az iskola feladata kísérni és támogatni őket a tanulásban. Az alapozó + szakmai 2+3 év helyett, negyedévenkénti újratervezés van. Már az iskola elején megjelenik az elmélyült szakmai tanulás. A tanárok a diákokkal együtt alakítják a struktúrát, annak megfelelően ami a leginkább segíti a tanulók folyamatos fejlődését. Ha a tanulást az 1 hét projektidőszak 1 hét „rendes iskolai” hét beosztás segíti az adott csoportban, akkor azt követik. Ha ugyanez a csoport negyedévvel később a délelőtt programozunk, délután haladunk a NAT követelményekkel beosztást tartja jobbnak, akkor változtatnak a tanulás struktúráján.

### Pedagógusdiploma

Az iskola legfontosabb segítő szereplője a mentortanár, aki személyes odafigyeléssel segíti mentoráltjait, és akikkel együtt szervezik a tanulási környezetet. Tőlük az egyetlen előzetes elvárás, a tinédzserek [támogatásában szerzett 3 éves tapasztalat.](/jogszabalyok/pedagogusok.md#pedagogusokra-vonatkozo-eloirasok) Minden további tanárral kapcsolatos legfontosabb elvárás, hogy az adott területeken, melyekhez kapcsolódóan modulokat hirdet meg, megfelelően tudja támogatni a tanulókat abban, hogy a kötelező tanulási eredményeket és a közösen kialakítottakat elérhessék.

| Iskola, amibe mi jártunk                  | Iskola, ahol a gyerekeink tanulnak                                                                                                                                                                                         |
| ----------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Csak pedagógus diplomával lehet tanítani. | Az alapító okiratban meghatározott kompetenciákkal lehet tanítani. Bizonyos területeken elvárt a pedagógusdiploma, ám van, ahol elsősorban a szaktudás, illetve a feladatban szerzett képzettség és tapasztalat az elvárt. |

### Vezetési modell

A klasszikus igazgató-típusú vezetői modell kevésbé reflektál a XXI. századi agilis szakképzés koncepciójára. Az igazgatótól a hatályos jogszabályok nem várják el, hogy vezetői tapasztalata legyen, csupán a közoktatás-vezetői szakvizsga meglétét. Pedagógiai tapasztalattal rendelkező, de elsősorban menedzsertípusú, tapasztalt vezető működése alatt biztosítható a `Code21` Technikum kitűzött céljainak elérése.

Az egyes – jövőbeni – telephelyekre tekintettel a tanulóközösségi-modellre, [szükségtelen](/jogszabalyok/pedagogusok.md#intezmenyvezeto-es-helyettes) intézményvezető-helyettes, tagintézmény-vezető kijelölése.

| Iskola, amibe mi jártunk                                                                                             | Iskola, ahol a gyerekeink tanulnak                                                                                                          |
| -------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| Az igazgatói poszt betöltésének feltétele a közoktatás-vezetői szakvizsga.                                           | Az igazgatói poszt betöltésének feltétele a pedagógiai, oktatási területen szerzett legalább 5 éves és legalább 3 éves vezetői tapasztalat. |
| Az intézményvezetői-döntéshozatali folyamatok az igazgató, illetve helyettesei feladat- és felelősségkörében vannak. | Az intézményvezetői-döntéshozatali folyamatok az igazgató, illetve a megbízott pedagógusok feladat- és felelősségkörében vannak.            |

### Épületek

Kisebb irodaépületben, átalakított üzlethelyiségben, esetleg volt ipari épületben is [elképzelhető](/jogszabalyok/epuletek.md) a tanulás. A tanulás terei a mai kor igényeihez mérten rugalmasan alakíthatóak, és nem egy helyben történnek. Az iskola inkább bázis, ahonnan a tanulás szerveződik, ennek megfelelően nem feltétlenül kell rendelkezzen minden eszközzel a tanuláshoz. Ezek külső helyszíneken is elérhetőek lehetnek.

| Iskola, amibe mi jártunk                                                                                                 | Iskola, ahol a gyerekeink tanulnak                                                                                                                                                                                            |
| ------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| A nagyméretű intézményekre optimalizált oktatási, nevelési funkciójú épületre előírt szabvány szerinti épületkialakítás. | A — közegészségügyi és tűzvédelmi szempontokból biztonságos — kisebb ingatlanra vagy ingatlanegyüttesre optimalizált, rugalmas, tanulást támogató terekkel rendelkező épület vagy épületegyüttes, ami eltérhet a szabványtól. |
