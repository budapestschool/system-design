# Budapest Shool Általános Iskola és Gimnázium

Ahhoz, hogy a gyerekek sikeresen vegyék a jövő akadályait, arra van szükség, hogy fenntartsuk a bennük lévő természetes kíváncsiságot, a tanulás vágyát.

A világ állandóan változik. Korábban a változás évszázadokban volt mérhető, ma néhány év elegendő ahhoz, hogy érzékelhetővé váljon. A gyors változásokhoz mindannyiunknak, még az iskolának is alkalmazkodnia kell.

Ez az iskola valamennyi szereplőjére és elemére igaz: változik a tanári szaktudás és a tanulási módszertan, a tudásanyag, a szükséges képességek tárháza, de változik a gyerekek mindennapi érdeklődése is.

A jövőbe nem látunk, de egy dolgot biztosan tudunk: akármik lesznek a jövő kihívásai, nekünk fontos, hogy ez a gyerekek számára ne félelmetes és szorongást keltő legyen, hanem lehetőséget, kihívást és örömet okozzon.

A célunk, hogy a fiatalok az iskolában és utána is könnyen megtalálják a saját útjukat. Ennek jegyében építettünk fel mindent a Budapest School Általános Iskola és Gimnáziumban. Iskolánkban nincsenek a tanulót és a tanárt korlátozó falak. A tanulás és az alkotás történhet az iskolában, és azon kívül is. Nem lehet hatékonyan megtanulni tankönyvből, hogy a vízben felénk úszó krokodil szemtávolsága jól jelzi, mekkora állattal nézünk szembe. Ezért, ha kell, elmegyünk az állatkertbe, egy kémiai laborba, kovácsműhelybe, vállalati irodába vagy az erdőbe.

A tanulók minden nap az egyéni érdeklődésüknek megfelelő tanulási és alkotási helyzetekkel találkoznak, köztük olyanokkal, amelyek a képességeik határát feszegetik. Ahhoz, hogy a gyerekek sikeresen vegyék a jövő akadályait, arra van szükség, hogy fenntartsuk a bennük lévő természetes kíváncsiságot, a tanulni tanulás vágyát, a tanuláshoz és alkotáshoz elengedhetetlen belső motivációt.
