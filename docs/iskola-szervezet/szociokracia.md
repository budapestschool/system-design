# Szervezet architektúrája

Az iskolák hagyományosan hierarchikus vezetési és munkaszervezési
elveket követnek. Főnök—beosztott viszonyok, felülről jövő döntések és
autoritás határozza meg a mindennapi folyamatokat. Az állandóság és a
biztos siker érdekében ezek az iskolák a centralizált hatalmat
alkalmazzák: a főnök dönt, a beosztottak végrehajtanak. A Budapest
School a mai világra reflektálva egy dinamikusabb, gyors változásokat
támogató modellt dolgozott ki az egyes tanulóközösségek hatékony és
mégis decentralizált működtetésére.

Olyan kihívásokra reflektál ez a működési mód amelyek nagyobb
komplexitást, transzparenciát és szélesebb körű kommunikációs
lehetőségeket kívánnak meg. Így rövidülhet a döntéshozatal, a gazdasági,
a környezetet és a tanulás körülményeit érintő kérdésekben pedig
növekszik az érintettek felhatalmazása és bevonódása. A Budapest School
egy olyan modell működtetését határozza meg, amely
önfenntartó, környezettudatos és etikus. Támogatja a munkavállalók
kreativitását és lelkesedését és egy agilis, gyorsan változó és fejlődő
szervezet épülését segíti.

## A Budapest School szervezeti modellje, szociokrácia

A szociokrácia egy olyan új „szociális technológiát" biztosít a
szervezetek irányítására és működtetésére, amelyet a hagyományos,
hierarchikus szervezetektől eltérő alapvető szabályok határoznak meg.

A célirányos és változásra fogékony
szervezetek számára kidolgozott öngazdálkodó gyakorlatról van szó.

A megközelítés — az agilis és a _lean_ megoldásokhoz hasonlóan — „just in
time", vagyis épp a kellő pillanatban reagál a felmerülő változásokra és
lehetőségekre. Mindezt pedig a vállalat minden szintjén önállóan teszi.

Az iskolai szervezet alapelemei a következők:

- egy keretrendszer, ami lefekteti a „játékszabályokat" és újraosztja
  a hatalmat,

- egy módszer a szervezet újrastrukturálásához és az emberek
  szerepeinek és önállóságának meghatározásához, és

- egy egyedülálló döntéshozatali folyamat a hagyományos döntési
  folyamatok felfrissítéséhez.

## Dinamikusan változó szerepek

A klasszikus iskolában minden alkalmazott rendelkezik egy munkaköri
leírással, mely felsorolja feladatait és hatáskörét. Ez általában a
legkevésbé sincs összhangban a nap mint nap végzett feladatokkal. A
Budapest School iskolában a dolgozók gyakran számos szereppel
rendelkeznek, és ezeket akár akár teljesen különböző
munkacsapatokon belül betölthetik. A szerepek folyamatos alakuláson mennek
keresztül és a csapat résztvevői frissítik őket. A szerepkörök tehát
nincsenek szorosan egy emberhez kötve: mindig az látja majd el, aki
ért hozzá, van szabad kapacitása és elvállalja. Ezzel pedig sok
személyes konfliktus kerülhető el.

Ahogy például _a fociban is pontosan
tudod, hogy a labdát a csatárnak kell passzolnod_. Nem azért, mert jóban vagytok, hanem mert ő van a legjobb helyzetben,
hogy gólt lőjön a csapatnak. Még ha nem is vagy vele jóban, nem kedveled
vagy ki nem állhatod, akkor is neki fogsz passzolni, mert a játék
stratégiája szerint így kell tenned. Ugyanígy az iskolánkban az egyes
szerepek rendelkeznek hatalommal, nem pedig a személyek. Ez azt is
jelenti, hogy a szerepek és a hatalom állandóan változhatnak anélkül,
hogy a játékszabályokat megsértenénk. Nincs állandó főnök—beosztott
viszony.

## Szétosztott hatalom

A hagyományos iskolai struktúrában az igazgató és a vezetőség tagjai nem
delegálnak hatalmat. Minden döntés átmegy az ő kezeiken. A
Budapest School-iskolában a hatalmat ténylegesen elosztjuk, így a
hierarchia helyett egymással kapcsolatban lévő és kommunikáló, de
önállóan döntő csapatok (úgynevezett körök) a döntéshozók. A körök
közötti kapcsolat jelentősen meg tudja növelni az iskolai alkalmazkodóképességét.

## Állandó tyúklépések

Budapest School-iskolában a szervezeti struktúrát minden hónapban
felülvizsgálhatják: megvizsgálják, hogy az adott szerepek milyen
feladatokkal és döntésekkel járnak. A változások tömérdek apró lépésben
történnek, szünet nélkül, folyamatosan. Így a csapat kihasználhatja a
lehetőségeket, hogy tanuljon az esetleges hibákból, fejlessze önmagát, és
tökéletesítse a folyamatokat. Ahogy Alexis Gonzales-Black, a Zappos
munkatársa mondja (ez a cég talán a legelsők között csatlakozott satlakozott a „holakrácia” nevű rendszernek a valóságba való átültetéséhez
), „a holacracy nem fogja megoldani helyetted a
problémáidat; viszont jó eszköz arra, hogy te saját
magadnak megoldhasd őket."

## Mindenki számára átlátható szerepek

„Mi mindig így csináljuk" — hangzik el számos iskolában a válasz, hogy
ez vagy az miért éppen így történik. Gyakran senki sem tudja, hogy az
adott szabály miért létezik, mi célt szolgál, ki döntött róla vagy ki
tudná megváltoztatni. Ez pedig a hatalom szétosztását lehetetlenné
teszi. A Budapest School iskolában _a hatalom nem a csoport élén álló vezetők kezében van,_ hanem az expliciten definiált folyamatokhoz kötődik. Ezek a
„játékszabályok" mindenki számára elérhetők és ismertek, legyen szó
akár régi motorosokról, akár az újoncokról.

A Budapest School modellel tehát nem káoszt és hatalmi játszmákat
generálunk, hanem _eltöröljük a hagyományos hierarchiából fakadó lassú folyamatokat,_ az átláthatatlan felelősségi köröket és szabad utat engedünk a
kreativitásnak, az önállóságnak. Azáltal, hogy felhatalmazza az
embereket arra, hogy értelmes döntéseket hozhassanak és részt vehessenek
a változásban, a holakrácia felszabadítja a szervezet kihasználatlan
erőforrásait.
