# Operátor, a tanulóközösségek alapítója

Vannak tanulóközösségek, amik szervezését, indítását egy _alapító_ _operátor_ vagy _operátorcsapat_ kezdte el. Az operátorok a létrehozók, az alkotók, az alapítók, akik minden nehézséget leküzdve a semmiből létrehoznak egy önállóan működni képes tanulóközösséget. Nélkülük biztos nem jött volna létre a tanulóközösség.

## Az operátor, a mindenes

Kezdetben az operátor az egyszemélyes mindenes, hiszen egyedül kezdte a tanulóközösség tanulás-szervezőjeként. Ő felel a pénzügyekért, a tanárfelvételért, a csoportbontásokért, a tanulásszervezésért, a szülők bevonásáért, a gyerekfelvételért és még a mosdókat is ki kell tisztítani. Már, ha előtte elintéze, hogy legyenek mosdók. Az esetek többségében ő találja, bérli ki, vagy vásárolja meg, újítja fel a tanuló közösségnek otthont adó, később telephelyként működő épületet és dönt az első tanár csatlakozásáról.

Az operátorok aktívak, tesznek, megoldanak, elintéznek, sokszor hirtelen a Budapest School közösség képviselőjévé válnak.

## Az operátor a tanulásszervező, a tanárcsapat tagja

Amikor már megjelennek azok a tanárok, akik a tanulásszervezésért, a tanulóközösség vezetésért felelnek, tehát a tanulásszervezők (mentorok, mentortanárok és néhány kiemelt szaktanár), az operátor akkor is a tanulásszervező csapat része marad. Nem átadja a tanulóközösséget a tanulásszervezőknek, hanem elkezdik csapatban vezetni a közösséget. Innentől kezdve a csapat minden tagja, így az operátor és minden tanulásszervező be van vonva minden döntésbe: mindenki hozzájárulása szükséges minden, a csapatot és a tanulóközösséget érintő kérdésben.

Ez azt is jelenti, hogy csak olyan döntést fogadhat el a csapat, ami _mindenkinek_ elfogadható, amit mindenki _„elég jónak és biztonságosnak talál"_. Mindenki mondhatja azt, hogy egy javaslat nem oké neki, szerinte inkább csinálja a csapat másképp. Ráadásul javaslatokat mindenki hozhat. Ez azt jelenti, hogy mindenkinek lehet hatása a működésre.

> _Példa: Anna, az egyik tanulásszervező javasolja, hogy délutánonként kisebb csoportokba bontsák a gyerekeket, mert elfáradnak, és figyelmüket könnyebb lenne úgy megtartani. Ehhez Anna azt javasolja, hogy bízzanak meg egy külsős szaktanárt, hogy ő tudjon párhuzamosan művészeti klubot tartani. Péter, a csapat másik tagja kifejezi ellenvetését, mert épp így is alig van nullán a tanulóközösség, és inkább tartalékot kéne képezni. Neki szüksége van az anyagi biztonságra. A csapat megértve Anna és Péter igényeit is, kidolgoz egy olyan megoldást, ahol extra külsős tanár nélkül is megoldódik az Anna által felvetett probléma: ha a délutáni foglalkozás előtt időt töltenek az udvaron, akkor jobban fogják tudni tartani a figyelmet, külön csoportbontás nélkül is._

## Az operátor és a csapat

A szociokrácia rendszerében mindenki vállalhat szerepeket, hatalommal bíró feladatköröket, ha és amikor erre a csapat felruházza. Ezért az operátor, aki ugye kezdetben az egyszemélyes mindenes volt, feladatköröket ad át a többi tanulásszervezőnek. Nagy különbség van azonban aközött, hogy megkérlek, hogy légy szíves a kirándulásra készíts össze egészségügyi dobozt, vagy, hogy a csapattal megbeszéljük, hogy van 1. egy balesetvédelmi felelős szerep, amit javasolsz, hogy ezentúl 2. te végezd el. Az első esetben szívességet kérünk egymástól, a másik esetben feladatokat, felelősségeket, BPS terminológiában szerepeket adunk át, delegálunk.

Fontos, hogy a szerepek átadása nem az operátor és az új felelős közti megállapodás. Ez a csapat életét érintő változtatás, tehát mindenkit be kell vonni a döntésbe: fontos, hogy mindenkinek elfogadható, hogy kipróbáljuk, hogy ezentúl ezt a szerepet a javasolt ember viszi tovább?

> Példa: Anna, egy tanulóközösség operátora sokszor elfelejti kiküldeni a szülői előtt és után a szülőknek szánt kérdőíveket. Ebből több konfliktus adódott, mert Péter egy mentor a csapatban, nagyon szeretné tudni, hogy hogy vannak a szülők. Anna és Péter egy elég komoly vita (veszekedés?) után vannak. Arra jutottak, hogy inkább Péter felvállalná a szülői kérdőívek kiküldését és a szülők noszogatását. A következő tanári meetingen elmondják a javaslatukat, hogy Péter legyen a szülői visszajelzők kezelője, és kérik a csapatot, hogy Pétertől várják el, hogy időben kimenjen a levél a visszajelzőkről, és legalább 2 emlékeztetőt küldjön Péter, és legalább egy nappal a szülői előtt összesítse az eredményeket. Azaz adnak egy pontos szerepdefiníciót, és javasolják a csapatnak, hogy ezt a szerepet ezentúl Péter vigye. A csapat minden tagja nagyon örül, mert érezték, hogy ezt a feladatot érdemes lenne jobban végezni. Kifejezik hálájukat Annának és Péternek, hogy a nehézségek ellenére fejlesztik a rendszert.\_

### Mi van, ha az operátor nem akar vagy tud mindenben részt venni

Vannak esetek, amikor nem javít a gyerekek tanulási élményén vagy a tanárok jóllétén, ha az operátor minden döntésben részt vesz, vagy az operátornak egyéb teendői miatt nincs kapacitása a közösség életében nagy mértékben részt venni, feladatokat, szerepeket vállalni. Ilyenkor mondhatja az*t, hogy “bízom bennetek, szóljatok, amikor úgy érzitek, hogy kéne róla tudnom. De kérlek titeket, ami a pénzügyeket érinti, arról szóljatok.”* Sokat kell ilyenkor dolgozni azon, hogy mindenki tudja, értse és el is találja, hogy mikor kell bevonni az operátort és mikor nem.

### Mi van, ha a tanárokat nem érdeklik a pénzügyek

Van, amikor azt érzed, hogy bizonyos dolgokról egyszerűen csak nem akarsz tudni, csak működjön a dolog. Ilyenkor egyszerűen felhatalmazhatjuk egymást, hogy egy adott kérdésben légy szíves hozd meg a döntést, és akár még informálnod sem kell engem. Tanárként azt kérem, hogy mondd meg, mennyi pénzt költhetek havonta filctollra és programokra, és most egyelőre nem akarom tudni, hogy a bérleti díj és a takarítóra költhető keret hogyan függ össze.

A probléma abból eredhet, hogy az operátor először egyedül csinált mindent, nem volt kivel megbeszélnie a dolgokat. Aztán jöttél te, mint tanár, és nem akartál mindent megbeszélni, mert sok volt az információ. Aztán van az a kényelmetlen érzés, hogy szeretnél bevonva lenni, de nem vagy. Mindenki azt hiszi, hogy ebben megállapodtatok, pedig nem. Csak történetileg alakult így a helyzet. Ez az a pont, amikor nem az SZMSZ-hez kell nyúlnod, hanem fel kell hívnod az operátort.

### Az operátor a tanárcsapat főnöke

Nem. Ha a főnök azt jelenti, hogy a főnök döntést hoz, a hierarchiában alatta lévő munkavállalók pedig végrehajtják a döntést, akkor egyértelmű NEM a válasz. A BPS szervezetek nem így működnek. Az operátor, mint más is, javaslatokat tesz, és mindenki hozzájárulása esetén a csapat azt végrehajtja. Ugyanakkor az valószínű, hogy az operátorok habitusuk, sajátos helyzetük miatt több javaslatot fognak, legalábbis az első pár évben tenni, mint a többiek.

Minden csapatnak van azonban egy [csapatvezetője](/iskola-szervezet/governance.md#csapatok-vezetoi), aki azért felel, hogy „a csapat működjön: tiszták legyenek a szerepek és megtörténjen az, amiben megállapodott a csapat, és működjenek és fejlődjenek a folyamatok.” Ahhoz, hogy ezt a célját elérje „facilitál, moderál, szintetizál, kísér, kérdez.”

Kezdetben ezt a szerepet is az operátor látja el. A csapat dönthet úgy, hogy ezt a szerepet az operátor átadja egy másik tanulásszervezőnek, aki ezáltal a csapat koordinátora lesz. Azonban ehhez minden tag hozzájárulása szükséges. Tehát a BPS-ben nem szavazunk ki senkit, hanem megbeszéljük, hogy jobb lenne, ha bizonyos szerepet most más végezne.

### Az opertátor felelelősséget vállal mindenért

Nem. Nincs olyan ember a rendszerben, aki mindenért egy személyben felelős lenne. Egy tornatanár nem felelős minden balesetért, ami tornaórán történik. Történhet olyan baleset, ami ellen ő nem tudott volna mit tenni. Azért viszont felelős, hogy ha egyszer már megtanultuk, hogy balesetveszélyes mélyvízbe ugrania annak, aki nem tud úszni, hogy megkérdezze mindenkitől, hogy tud-e úszni, mielőtt elkezdődik a foglalkozás. Ugyanígy: egy operátor/ koordinátor nem felelős azért, hogy mit csinál egy másik munkatárs, vagy egy gyerek az iskolában, vagy hogy egy szülő hogyan viselkedik, de azért felelős, hogy ha nem tartunk be szabályokat és megállapodásokat, akkor tegyen azért, hogy a helyzet megváltozzon.

## Az operátor különleges szerepe: ő a pénzügyi stabilitás biztosítója

A pénz olyan, mint az oxigén. Szükségünk van rá a tanulóközösség működtetéséhez. És mivel a Budapest School tanulóközösségek önálló költségvetéssel bírnak, fontos, hogy egy tanulóközösségnek ne legyen több a kiadása, mint a bevétele.

Az operátorok által életre hívott tanulóközösségek esetén az operátor az, aki személyesen felelősséget vállal a pénzügyi stabilitásért. Értsd: ha többet akar a tanulóközösség költeni, mint a bevétele, akkor neki kell a zsebébe nyúlni. És ameddig az operátor ezt a felelősséget nem osztja szét a többi tanulásszervező között (azaz nem szövetkezetként működnek), addig ő mindig \_[első lesz az](https://en.wikipedia.org/wiki/Primus_inter_pares)\_egyenlők között.

**Akkor ez azt is jelenti, hogy a pénzügyi felelősségvállalás miatt ő maga meghozhat pénzügyeket érintő döntéseket?** Attól, hogy operátorként az a dolgod, hogy 1. ne legyen hiány, 2. ha van, akkor teremtsd elő a hiányt, attól még nem hozol meg minden döntést egyedül. Mert a ne-legyen-hiány célt sokféleképpen el lehet érni. A csapatnak kemény munka árán ki kell dolgoznia a saját megállapodásait arról, hogy mi az a döntés, amire felhatalmazást kaptál.

### Az operátor kilépése

Ha konfliktus alakul ki a csoportban, akkor bárki mondhatja azt, hogy ,,én kilépek”. Csak az operátor nem. Vagyis csak nagyon nehezen. Ha eltűnik az operátor, akkor még az is lehet, hogy megszűnik a tanulóközösség. Ha mégis olyan helyzet áll elő, hogy kilép, akkor új operártort kell találni vagy át kell alakítani a struktúrát. Az operátor ilyenkor is aktívan részt vesz a helyzet megoldásában (pl. az utódja megtalálásában, az átadás-átvételben, stb).

## Koordinátor

Amikor az operátor átadja a csapatvezető szerepet másnak, akkor ezt az embert koordinátornak kezdjük el hívni, hogy mindenki értse: itt már a pénzügyi felelősség és a csapatvezetőség szétvált ebben a csapatban. A koordinátor szintén nem főnök, hanem facilitátor. Sokat gondolkodik azon, hogy mindenki tudja-e, hogy ki mit és mikor csinál, ő a circle leader.

Vannak azok a tanulóközösségek, ahol az operátor szerepet a Lab játsza. Ilyenkor mindig van egy koordinátor a tanulásszervezők között.
