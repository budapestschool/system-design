# Igazgató

> Ki az igazgató a BPS-ben? Ki írja alá a munkaszerződéseket és bizonyítványokat? Miért nincs igazgató választás?

Egy magyar állam által elismert iskolában igazgatónak (pontosan fogalmazva: intézményvezetőnek) lennie kell. A Nemzeti Köznevelési Törvény több mint 50 esetben meg is említi az igazgatót, mint döntéshozót. A BPS-szervezet ugyanakkor másképp működik, mint az NKT által alapnak vett szervezet.

Az NKT metafórája szerint az iskola egy jól működő gépezet, az igazgató pedig a főmérnök, aki végülis eldönti, hogy a futószalag leáll-e. A BPS szervezetről mi inkább úgy gondolkozunk, mint egy önirányító csapatok hálózata, amiben a döntéseket a közösség tagjai, az érintettek köre, általában a gyerekekkel foglalkozó tanárcsapatok hozzák meg. A hagyományos szervezetek nehezen képzelhetőek el igazgató nélkül, a BPS-szervezetben meg néha nehéz megérteni, hogy – az állam által megkívánt – igazgató mit is csinál.

BPS-szervezetben így az igazgató – nevével ellentétben – nem igazgat, nem dönt, nem kormányoz, nem facilitál, nem prioritizál, nem felügyel, nem igazol hanem vendégül lát. Amikor a hivatal kapcsolatba akar velünk lépni, akkor az igazgató a hivatal számára elérhető.

## Mit ír alá az igazgató

Az igazgatót a BPS szervezeti szabályai kötik, hogy csak olyan dolgot írhat alá, azt viszont mérlegelés nélkül alá kell írnia,

1. aminél nincs lehetőségünk nem feltüntetni igazgatót és
2. aminek tartalmát az adott ügyben érintettek köre elfogadott.

Ilyen dokumentum, a félévi értesítő, az év végi bizonyítvány, a MÁK normatíva igénylés, a csatlakozási szerződés, a munkaszerződés. Ezeket a hivatalok kizárólag igazgatói aláírással együtt fogadnak el, és az nem opció, hogy nem készítjük vagy adjuk be őket. Tartalmukról ugyanakkor nem az igazgató dönt.

## Miért felel az igazgató?

Mivel az igazgató önállóan döntéseket nem hozhat, így egyedül azért felel, hogy tényleg betartva a szabályokat, csak olyan dolgokat ír alá, amit aláírhat.

## Szerepleírás

#### Az igazgató keretei:

- [SZMSZ](szmsz.md) rögzíti, hogy munkaügyben csak akkor járhat el, ha a BPS _tanár felvételi felelősségterület_ és az érintett tanárcsapat a mikroiskola tanárok elfogadtak egy javaslatot.
- Biztonyítványt csak a mybps által generált módon állíthat elő.
- Adatot nem változtathat a mybps, admin rendszerben.
- Bankszámlához nem nyúlhat.
- Csak hivatalokban mutatkozhat be igazgatóként.
- Szülői megkeresésre a support@budapestschool.org címre kell küldenie a megkeresést.

#### A jó igazgató:

- Kedvesen beszél a hivatalokkal, közvetlen, informális és őszinte velük.
- Figyel arra, hogy a hivatalok megkapják, amire szükségük van, ha az a szervezetben elérhető.
- Gyerekeknek, szülőknek, tanároknak és adminnak jól érthetően elmondja, hogy ő más szerepben van, mint az általuk megszokott igazgatók.
- Aktívan fenntartja azt az állapotot, hogy az igazgató nem igazgat és nem hoz intézményi és pedagógiai döntéseket.
- Hivatallal való beszélgetés kapcsán a fenntartó által javasolt embert meghatalmazza, hogy képviselje az ügyben az iskolát. Ha a kijelölt ember is ezt is akarja.
- Aktívan képviseli a BPS-szervezet ,,furcsa" működését akár kollégák, hivatalokkal kapcsolatos kommunikációban is.
