
# A Budapest School működésének szabályai

A Budapest School, mint iskola és mint szervezet működését a BPS Modell írja le. Tanügyigazgatási (jogszabályi) értelemben a BPS Modell egyszerre Pedagógiai Program, Szervezeti Működési Szabályzat és Házirdend: tartalmaz minden
előírást, amit a tanügyigazgatási dokumentumok kapcsán a jogszabályok
előírnak. Számunka a BPS Modell azt írja le, hogyan kívánjuk elérni az alapvető [céljainkat](/iskola-celja/mi-a-celunk.md)

Ebben a fejezetben (dokumentumban) a működés azon elemeit emeljük ki, amit a törvény (a 20/2012. EMMI rendelet) előír a Szervezeti Működési Szabályzat tekintetében. Ez a kiemelés ugyanakkor nem teljes, és néhány dolog értelmezéséhez ismerni kell a Modell többi elemét is.

## Ki hozza a BPS szabályait?

A Modell meghatározza a Budapest School döntési alapelvét, ami a szociokrácia. A
Modell (és annak részei) módosításainak elfogadása ezen alapelvből következően
zajlik: a Budapest School minden dolgozójával (tanárok, óraadók,
adminisztrátorok, fejlesztők alapítók) megosztásra kerül a módosítás tervezete,
és mindenki jelezheti fenntartás ('concern') vagy ellenvetés ('objection'). A
módosítás szövegét addig változtatjuk együtt, míg mindenki számára elfogadható
('safe enough') lesz. Onnan tudjuk, hogy megtörtént érvényesen a változás, hogy
minden objectiont eljuttatunk abba az állapotba, hogy az azt jelzőknek is 'safe
enough'.

Kiinduló pontunk, hogy _"Az iskola szervezetét a tagjai
együtt alakítják. A szervezet élő, folyamatosan változik."_ Ennek az
,,együtt-alakításnak" fontos eleme, hogy a Budapest Schoolban az iskola egyik
szereplője sem (sem a fenntartó képviselője, sem az igazgató) jogosult egyedül
szabályt hozni vagy dönteni.

## Nincs egyszemélyi döntés

Az Alapító Okirat egyértelműen rögzíti, hogy "_a gazdasági, pénzügyi, munkaügyi
feladatokat a fenntartó látja el."_ Így az Nkt. által az intézményvezető
szerepénél előírt jogkörök a Budapest Schoolban kizárólag együttesen
gyakorolhatók. A működésünk megkezdésekor elfogadott Egyedi Pedagógiai Program
kijelöli, hogy kik az intézményvezető mellett a döntésre jogosultak: _"feladatai
elvégzésében segíthetik más tanárok, megbízott szakértők, és a fenntartó is_".

A BPS Alapító Okirata, a BPS részére az EMMI által engedélyezett egyedi
megoldások és BPS működését engedélyező eljárásban elfogadott Egyedi Pedagógiai Program által létrejött rendszerből az alábbi szabályok következnek:

1. Az intézményvezető nem hozhat egyszemélyi döntést, nem alkothat egyedül
   szabályt.

2. Tanárok alkalmazásával, jogviszonyuk megszüntetésével kapcsolatos döntés
   esetén a tanulóközösség egyértelmű javaslata köti a munkáltatói jogkör
   gyakorlóját. A _tanár felvételi felelősségterület_ gazdájá ellenvetése
   (objection) ellenében ugyanakkor nem hozható meg ez a döntés, tekintettel a
   fenntartó alapító okiratban rögzített, holisztikus felelősségére.

3. A gyermekek, családok csatlakozásának elfogadásához a _családfelvétel
   felelősségi terület_ gazdájának és a tanulóközösség közös döntése szükséges.
   Ez a döntés köti az intézményvezetőt.

4. Amennyiben nem világos, hogy egy adott kérdésben az iskola egyes szereplői
   közül ki köteles egy adott előírást teljesíteni, egy adott feladatot
   elvégezni, úgy főszabály szerint ez a feladat a fenntartó kötelezettsége.

### A tanulóközösségek

Az elfogadott Egyedi Pedagógiai Program egyértelműen rögzíti, hogy a Budapest School
szervezete - a szabályalkotás szempontjából is - a "tanulóközösségekre" épül:

> _"A Budapest School iskola a gyerekek (családok) és tanárok kisebb
> közösségeiből, a tanulóközösségekből, áll. Ezek a közösségek (szervezeti
> szaknyelven) önálló szervezeti részegységek a nagy iskolában."_

A tanulóközösség részei a tanulásszervezők, akik mentortanárok, tanárok,
operátorok és koordinátorok.

### Helyi vezető (Operátor/Koordinátor → menedzser)

Az Operátor és a Koordinátor tanulásszervezők, a tanulóközösségek döntéseiben a
tanárokkal azonos módon vesznek részt. A tanulóközösségi szinten a döntések
képviselete az intézmény egésze felé a tanárcsapat, vagy általa kijelölt
közvetítő útján történik - ez a képviselő tipikus esetben az Operátor vagy a
Koordinátor. Ugyanez a személy felelős azért, hogy az intézmény javaslatait,
döntéseit közvetítse a tanulóközösség felé. Az Operátor és a Koordinátor közötti
legjelentősebb különbség, hogy az Operátor anyagi felelősséget vállal a
tanulóközösség fenntartható működéséért, amit a fenntartóval kötött szerződésben
rögzítenek.

A BPS az [Operátor/Koordinátor](/iskola-szervezet/operator.md#az-operator-a-mindenes) szerepét egységesíti, minden tanulóközösséget
helyi vezető irányít (menedzser). A menedzser a tanulóközösség sikerének záloga,
rajtuk múlik, hogy a tanárok mennyire tudnak hatékonyak lenni.

## Milyen ügycsoportokra alkotunk szabályokat?

<!-- prettier-ignore -->
| **ügycsoport** | **az ügycsoporthoz tartozó szabály erre keresi a választ** | **20/2012. EMMI rendelet előírásainak megfelelés** |
| -------------- | ---------------------------------------------------------- | -------------------------------------------------- |
| a BPS és a szülők közötti kapcsolattartás | ki és hogyan tartja a kapcsolatot a családokkal? | 4.§ (1) g), p) |
| a BPS külső képviselete | ki és hogyan képviseli a BPS-t kifelé? | 4.§ (1) e), f), t), i) |
| családok csatlakozása a BPS-hez | hogyan veszünk fel új családokat? | 5.§ (1) b), c) |
| tanárok, egyéb dolgozók csatlakozása a BPS-hez | hogyan veszünk fel új tanárokat, más munkatársakat? | |
| szakmai munka fejlesztése | hogyan fogok tanárként fejlődni? | 4.§ (1) b), k) |
| tanulóközösségek közötti kapcsolattartás | hogyan tudunk közös dolgokat szervezni, tudást megosztani tanulóközösségek között? | 4.§ (1) d) |
| a tanárcsapatok feladatelosztása | milyen rendszerben hozzuk meg a saját döntéseinket, osszuk el a saját feladatainkat a tanárcsapaton belül? | 4.§ (1) h) |
| az iskola elektronikus adminisztrációs rendszerei | hogyan működnek és mire valók a BPS elektronikus rendszerei, a my.bps és a KRÉTA? | 4. § (1) r), s), (2) g); 5. § (1) a) |
| my.bps elektronikus rendszer üzemeltetése | hogyan működik és mire való a BPS elektronikus rendszere, a my.bps? | 4.§ (1) r), s), (2) g) |
| a tanulóközösségek alapvető működési rendje | mik az iskolában, mint fizikai környezetben tartózkodás alapszabályai? | 4.§ (1) a), c), 5.§ (2) f), h) |
| egészségügyi ellátás rendszere | hogyan és ki biztosítja a gyerekek egészségügyi ellátását? | 4.§ (1) l) |
| fizikai biztonsággal kapcsolatos szabályok | hogyan és ki biztosítja a gyerekek fizikai biztonságát? | 4.§ (1) m), n), (2) f) |
| iskolán kívüli programok szabályrendszere | az iskolán kívüli programokra kell külön szabály? | 5.§ (1) g) |
| gyerek-iskola kommunikáció, konfliktus | mik a gyerek-iskola konfliktus kezelésének részletei? | 4.§ (1) q), (2) f), 5.§ (1) d) |
| foglalkozások megtervezése | milyen rendszerben tervezzük a foglalkozásokat? | 4.§ (2) a), 5.§ (2) d), 5.§ (1) i) |
| tanulóközösségek napi-, heti- éves rendje összeállításának szabályai | hogyan állíthatjuk össze a tanulás rendszerét? | 5.§ (2) a)-c), 4.§ (1) j) |
| hiányzás, távolmaradás nyomon követése | hogyan rögzítsük a hiányzást? | 5.§ (1) a) |
| évfolyamugrások rendszere | hogy tud a gyerek egy tanév alatt több évfolyamot haladni? | 5.§ (1) h) |
| akkreditációs szabályunk | működési engedélynek való megfelelés |  |

## Szabályzatok

A "szabály" kifejezés formai megkötést nem jelent, szabálynak értelmezzünk egy
policyt, egy folyamatleírást, az érintett témát feldolgozó dokumentumokat is.
Ami fontos, hogy a szabályokból egyértelműen kiderüljön, hogy

- mi a probléma, kérdés megoldásának konkrét módja

- kik részesei a döntésnek, folyamatnak

- ki az (milyen szerepű ember), aki a felelőse a folyamatnak, a döntés
  meghozatalának

Minden szabályról írásbeli dokumentumnak kell születnie, mely a BPS-ben mindenki
számára hozzáférhető. Minden formátum írásbelinek számít, amely visszakereshető
és hozzáférhető.

Az _iskolakísérő felelősségterület_ gazdájának feladata, hogy minden
tanulóközösséget segítsen ezen szabályok megalkotásában. Ezt a feladatát úgy
teljesíti, hogy minden itt előírt szabályhoz megfelelő sablont készít, és tesz
elérhetővé, amelyben végigvezeti a tanulóközösséget a szabály megalkotásának
sarokpontjain, a kérdéseken, amikre meg kell találni a saját válaszaikat.

Ezen kívül a tanulóközösség jogosult saját szabályt alkotni bármilyen, a
közösséget érintő témában - egyetlen korlát, hogy az itt felsoroltaktól és a BPS
Modell többi részétől nem térhet el.

## A BPS és a szülők közötti kapcsolattartás

A szülők és az iskola között elsődlegesen a tanulóközösség tanárcsapata tartja a
kapcsolatot. Olyan ügyekben, ami a gyerek jogviszonyát érinti a _tanügyi
adminisztrációs felelősségterület_ gazdája, ami a pénzügyeket érinti, a
_pénzügyi felelősségterület_ gazdája tartja a kapcsolatot a családdal.

Minden tanulóközösség szeptember 1. és június 15. között rendszeresen szülői
kört tart, amelyre valamennyi szülő hivatalos. A szülői kör formátuma alapvetően
személyes, helyszíne a tanulóközösség helyszíne. Indokolt esetben a
tanulóközösség dönthet online formátum mellett. A szülői kör előtt a családokat
online kérdőívben kérdezik meg a tanulóközösségek arról, milyen kérdéseket
tartanak aktuálisan fontosnak, a találkozót követően pedig - szintén online
formában - lehetőségük van a visszajelzésre.

A kapcsolattartás formája elsődlegesen elektronikus. A csatlakozási szerződés
aláírásával a szülők és az iskola közötti emailen történő kommunikáció
írásbelinek tekintendő.

A _össz bps naptár felelősségi terület gazdája_ minden családdal megosztja az
éves naptárt legkésőbb a tanév megkezdését követő 30 napon belül.

## A BPS külső képviselete

### Az intézmény vezetője egymagában nem dönt

Az Nkt. szerint az intézményt az intézményvezető képviseli. A Budapest Schoolban
az intézményvezető valamennyi feladatát a fenntartóval és/vagy a tanárcsapattal
(a tanulóközösségekkel) együttesen gyakorolja. Emellett a fenntartó
kötelezettsége a gazdasági-pénzügyi alapok biztosítása, az intézményvezető nem
rendelkezhet ezen kérdésekben. Ezen okok - és a fentiekben kifejtett döntési
szerkezet - miatt a fenntartó és az intézményvezető közösen alakítják ki az
intézmény képviseletének rendjét.

### Intézményi delegáltak

Néhány esetben, ahol hivatalok vagy más külső szereplők az intézményvezető jelenlétét követelik meg, az intézmény képviselheti _intézményvezető helyettesítésére felhatalmazott_ munkatárs. A felhatalmazás korlátozott időre és korlátozott ügyre jöhet csak létre. Felhatalmazás akkor érvényes, ha ahhoz az igazgató és az ügyben érintett minden munkatárs hozzájárulását adta:

- pedagógus minősítés esetén a minősülő pedagógus,
- tanfelügyeleti ellenőrzés esetén az ellenőrzött pedagógus

hozzájárulása szükséges az igazgatóé mellett. Ha ilyen ügyben intézményi delegált képviseli az iskolát, akkor a KIR-ben a képviselőt vezető-helyettes besorolásban rögzíteni kell az felhatalmazás idejére.

### Intézmény képviselete a gyerekeket érintő ügyekben

- pedagógiai szakszolgálatnál vizsgálati kérelmet a tanulóközösség [gyógypedagógiai felelőse](tanulasi-elmeny/tanari-szerepek.md#gyogypedagogiai-felelos),
- közösségi szolgálattal kapcsolatos szerződések és dokumentációt az érintett gyerek mentortanára írhat alá.

### Nem delegálható feladatok

1. alkalmazottak jogviszonyát keletkeztető és megszüntető, illetve egyéb
   munkajogi relevanciájú jognyilatkozatok, szerződések - tekintettel arra, hogy
   az Nkt. szerint a munkáltatói jogok gyakorlója az intézményvezető,

2. a gyerekek tanulói jogviszonyát keletkeztető, megszüntető szerződések -
   tekintettel a köznevelés-igazgatás rendszerének általános működésére, annak
   fenntarthatóságára, az átjárhatóság biztosítására.

### Az intézményvezető állandó helyettesítése

Az intézményvezető megbízásának bármely okból történő megszűnése esetén a
fenntartó köteles haladéktalanul kijelölni ideiglenes intézményvezetőt, akinek
meg kell felelnie az működési engedély megszerzésére irányuló eljárásban
elfogadott Pedagógiai Programban foglaltaknak: "_a fenntartó jogosult olyan
személy ideiglenes megbízására, aki pedagógus végzettséggel és legalább 2 éves,
igazolt vezetői tapasztalattal rendelkezik."_

## Családok csatlakozása a BPS-hez

Családok bármikor csatlakozhatnak a BPS-hez, a [Modell Felvétel és átvétel](/iskola-szervezet/felvetel-atvetel.md#szempontok-a-donteshez)
fejezetében leírtak szerint.

A család felvételéhez a tanulóközösség taulásszervezőinek hozzájárulása
szükséges.

A csatlakozáshoz a családnak alá kell írni egy csatlakozási szerződést, amely
rögzíti a BPS és a család között a tiszta megállapodás alapjait, létrehozza a
gyerek és az Iskola között a tanulói jogviszonyt, és amelyben a család vállalja,
hogy szülői hozzájárulással támogatja a BPS szervezetét.

A felvételi folyamat (ki mikor kinek mennyi időn belül válaszol) fejlesztése,
működtetése, monitorozása a felvételi folyamat _fejlesztési folyamat felelősségi
terület_ gazdája végzi.

## Tanárok, egyéb dolgozók csatlakozása a BPS-hez

A tanárok felvételének alapjait a Modell [Tanárok kiválasztása](/iskola-szervezet/tanarok.md#alapelveink)
fejezete
rögzíti.

Valamennyi BPS-ben dolgozó felnőtt (tanár és egyéb dolgozó) alkalmazásának
feltétele, hogy rendelkezzen erkölcsi bizonyítvánnyal.

Minden tanárt legalább egy tanulóközösséghez veszünk fel.

A mentorok, mentortanárok és szaktanárok kiválasztásánál a tanulóközösségnek
szem előtt kell tartania a NAT által előírt tantárgyi struktúrát.

A tanulóközösség minden mentora írásban rögzíti a benyomásait az adott
jelöltről, ez szükséges része a felvételi folyamatnak. A mentorok, mentortanárok
és szaktanárok felvételéhez a tanulóközösség tanulásszervezőinek hozzájárulása
és a _tanárfelvételi felelősségterület_ gazdájának hozzájárulása szükséges.

Az óraadó tanárok és egyéb dolgozók, akik a tanulóközösségekben végzik
feladataikat, felvételéhez a tanulóközösség tanulásszervezőinek hozzájárulása
szükséges.

A nem tanárként, nem tanulóközösségnél dolgozó munkatársak felvételéhez
kizárólag a fogadó kör tagjainak hozzájárulása szükséges.

## A szakmai munka fejlesztése

### Együtt tanulás

A BPS-ben a szakmai munka az ,,elosztott pedagógiai vezetés" koncepciójára épül.
Minden tanárnak lehetősége van folyamatosan fejlődni, és egyben alakítani a
szakmai munkát.

A _tanárakadémia_ felelősségterület gazdájának feladata megteremteni a
lehetőségét a tanárok fejlődéséhez. A rendszer építése folyamatos, a jelenlegi
állapot szerint a fő összetevői, alkalmai:

- **csapatütörtök**: hétközben délután 3 órás alkalom. A témák és megközelítés módszerei
  hétről-hétre másak, bármelyik tanár hozhat javaslatot témára, amelyet az adott
  héten együtt beszélünk át. A _tanárakadémia_ felelősségterület feladata az
  események megszervezése, facilitálása, és összefoglaló készítése a teljes
  közösség számára.

  - **"szövet szombat"**: évente három alkalommal egész napos együtt tanulás. Tematizált alkalmak, esetenként felkért külső szakértőkkel.

- **powerweek**: tanév előtt augusztus végén, illetve január legelején 3 napos
  együtt tanulási alkalmak. Itt az iskola előtt álló, következő powerweekig
  tartó időszak megalapozása, megtervezése az alapvető cél, elsősorban a teljes
  szervezet szempontjából.

Ezeken az alkalmakon a _tanárakadémia_ felelősségterület gazdájának feladata az
események megszervezése, facilitálása, és összefoglaló készítése a teljes
közösség számára.

Egyes szakmai területekre tanárok kezdeményezésére munkacsoportok alakulnak.
Ezek munkáját a _tanárakadémia_ felelősségterület csapata segíti, bekapcsolja
őket az együtt-tanulási alkalmakba.


### A fejlesztés a LAB feladata

Amíg a gyerekek fejlődnek, a szülők biztonságban érzik magukat és a gyerekeket,
a tanárok hatékonynak érzik magukat, és mindeközben minden résztvevő boldog,
akkor jól végezzük a munkánkat. Mindezen aspektusokat nyomon követi az iskola a
következő eszközökkel:

A tanárok részére rendszeresen, minimum évi 4 alkalommal kérdőíves kutatásban
méri fel a _diagnosztikai felelősségterület_ gazdája, hogy mennyire érzik saját
munkájukat hatékonynak

Ezen felül évente minden tanulóközösség "jólétét" felméri a _diagnosztikai
felelősségterület_ gazdája a helyszínen szervezett ún. team health check
keretében. A felmérést végző szakértő az eredményeket a tanulóközösség bármely
tagja és az intézmény képviselői számára nyilvános alkalom során ismerteti, ahol
közösen fogalmazzák meg a résztvevők a továbblépés irányait.

A szakmai munka fejlesztésére további inputokat a tanulóközösségeket működés
közben megfigyelő hospitálási alkalmak szolgáltatnak. Az intézmény által
megbízott szakértő mellett ezen hospitáláson másik tanulóközösség tanárai
vesznek részt.

A pedagógiai szakmai munka adminisztratív részét a my.bps rendszerben követi
nyomon az iskola. A my.bps rendszer használata minden tanár számára kötelező az
iskolában.

## TÉR

A személyre szabott teljesítmény célok meghatározásának módja:
A BPS tanárok az egyéni teljesítmény céljukat maguk határozzák meg, amelyet a tanárcsapatukkal beszélnek meg és hagynak jóvá a tanév elején. A tanárcsapat javasolhat másik célt, de fontos a közös megállapodás.  Ezt követően egyénenként kerül rögzítésre a 3 személyre szabott teljesítménycél. A tanév végi értékelő megbeszélés is a tanárcsapattal közösen történik.

Egyedi intézményi értékelési szempont kialakításának módja:
Az iskola a tanév végeztével, július első két hetében az éves szervezeti Offsite-on határozza meg szocikratikus módon a következő tanév céljait, amelyek meghatározzák a következő nevelési év irányelveit. Az itt létrejött célok megvalósításában a BPS összes tanára és dolgozója részt vállal.


## A tanulóközösségek közötti kapcsolattartás, átmenet

A tanulóközösségek közötti kapcsolattartás napi szinten az online térben valósul
meg. A teljes szervezet számára elérhető belső kommunikációs csatornán (jelenleg
Workplace) minden tanár, dolgozó jogosult hozzáférni a megosztott tartalmakhoz.
Emellett a BPS saját elektronikus rendszere, a my.bps felület is alkalmas arra,
hogy egymástól jó gyakorlatokat, megoldásokat tanuljanak a tanárok, és
segítséget, támogatást nyújtsanak, keressenek.

A valós térben történő kapcsolattartásra elsősorban az együtt tanulási alkalmak
kínálnak lehetőséget.

Egyéb közös, iskolákon átívelő tevékenység szervezése bármelyik tanárcsapat
kezdeményezésére indulhat, ehhez kizárólag a résztvevő tanulóközösségek döntése
szükséges.

## A tanárcsapatok feladatelosztása

Abból adódóan, hogy a BPS-ben a tanulóközösségek erős autonómiával rendelkeznek,
az ott dolgozók a szűken értelmezett tanári munkán felül több szerepet visznek.
A külön nevesített operátori/koordinátori szerep mellett is sok terület van,
amelyre a tanulóközösségeknek szükséges felelőst találni. Minden
tanulóközösségben van felelőse:

- a gyerek felvételnek

- az étkeztetésnek

- az ingatlan biztonságos állapotának megőrzésének

- az eszközök beszerzésének

- az SNI-s gyerekek fejlődése követésének

- a szülőkkel való kapcsolattartásnak

- a visszajelzésnek

- a mentorálásnak

Az egyes tanulóközösségek eltérő módon működtethetik ezeket a szerepeket és
választhatják ki a felelősöket. Szervezeti szinten is hatékony működéshez
szükséges, hogy a többi tanulóközösség és minden más munkatárs rendelkezzen
erről információval, ezért

## Az iskola elektronikus adminisztrációs rendszerei

Órarendet a szülők a Krétában néznek. A tanárok a mybps-ben rögzítik a modulokat és a modulórákat. A mybps-ből a Kréta Admin teszi át az adatokat a Krétába (migrálás). Tanárnak nem kell két helyre beírni. Emellett megmarad a google calendar és classroom kapcsolat. A tanárok számára a my.bps-ben továbbra is látható lesz a modulórák által kirajzolt órarend.

Ahhoz, hogy félévkor és év végén törzslapot és értesítőt tudjunk generálni az e-Krétába be kell kerülnie annak az adatnak, hogy a gyerek mikor milyen foglalkozásokon vett részt, és ha hiányzott azt igazoltan vagy igazolatlanul tette. Ahhoz, hogy ezt elérjük, a következő munkamegosztást javasoljuk a myBPS és az e-Kréta között:
- továbbra is a my.bps-ben hozzuk létre a modulokat és modulórákat
- az e-Kréta pedig átveszi a hiányzások vezetése és igazolása funkcióját.

A modulok migrációja és az egyéb adminisztratív feladatok a Kréta Admin felelőssége lesz a 2024/2025-ös tanévben - kivéve mikroiskolák által ellátandókat.

Minden tanárnak (mentortanárnak és minden modultartónak egyaránt) a saját modulóráit le kell "naplóznia" a Krétában. Ebben a rövid adminisztratív lépésben jelzi a tanár azt, hogy A) ki hiányzott az adott foglalkozáson és B) mi volt az adott foglalkozás témája.
A) A Kréta rendszer a hiányzás rögzítésekor értesítést küld a szülőknek a jelzett hiányzásokról. Ekkor tudnak a szülők igazolást beküldeni, ugyancsak a Kréta rendszerén keresztül.
B) A modulóra témáját minden tanár szabadon tölti ki. A szülő az e-Kréta rendszerben megtekintheti az ún. "haladási napló"-t, így az egyes modulórák témái láthatóak számára.

A szülők által beküldött hiányzás igazolások elfogadásának két útja lesz, amiből minden mikroiskola egy tanévre elköteleződve azt választja magának, ami a saját közösségéhez jobban illeszkedik.
1) "Saját hatáskörben" - Ebben az esetben a hiányzás igazolások elfogadásának vagy elutasításának joga és kötelezettsége a mikroiskolánál marad.
2) "Kiszervezve" - Itt a Kréta Admin az, aki a feltöltött igazolást elfogadja vagy elutasítja, és csak abban az esetben veszi fel a kapcsolatot az iskolával, amikor nem egyértelmű az eset.

A BPS a jogszabályok által előírt nyomtatványokat e-Krétában ésa  my.bps elektronikus
rendszerében tárolja és vezeti. Ezen adatokból beírási naplónak és törzslapnak
megfelelő adattartalommal minden év június 15. és július 31. között riportot
készít, melyet kinyomtat. A fenntartó erre felhatalmazott képviselője az adott
tanévre vonatkozó köteget aláírja, és az intézmény pecsétjét helyezi el rajta. A
kinyomtatott nyilvántartásokat a székhelyen zárható szekrényben tároljuk.

A Krétát és my.bps rendszert a _my.bps üzemeltető felelősségterület_ gazdája üzemelteti.
Minden BPS-sel szerződött tanár, egyéb dolgozó köteles naprakészen
adminisztrálni ebben a rendszerben az alábbiakat:

- modulokat kiírni minden gyerek részére, megfelelő számban

- a tanulás megtörténtét rögzíteni

- a hiányzásokat nyomon követni

## Dokumentumok hitelesítése, aláírsa

Az iskola működéséhez szükséges dokumentumokat, azaz jegyzőkönyveket, megállapodásokat, felhatalmazásokat az érintettek (tanárok, szülők, vagy adminisztrátorok) hitelesíthetik úgy is, hogy az egyéni, kizárólagosan használt, intézményhez kapcsolt e-mail címen (@budapestschool.org), vagy szülők esetén a my.bps rendszerben tárolt e-mail címen keresztül belépve hitelesítik azt.

## A tanulóközösségek helyszíneinek alapvető működési rendje

A tanulóközösségek területén a gyerek szülei kivételével kizárólag olyan
személyek tartózkodhatnak, akik szerződéses jogviszonyban állnak a BPS-sel.

Minden tanulóközösségnek kell lennie olyan szabályának, amely leírja, hogy az
adott helyszínre kik léphetnek be, a gyerekeket kik és hogyan vihetik el az
iskolából, a helyszín nyitvatartását, és egyéb, fizikai térhez kötődő saját
szabályokat.

## Az egészségügyi ellátás rendszere

Az iskola-egészségügyi ellátást a BPS szerződtetett iskolaorvossal és védőnővel
oldja meg. Az éves felmérések és szükséges oltások beadásának helye vagy az
iskolaorvos rendelője, vagy - amelyik helyszínen megoldható - az adott
tanulóközösség telephelye. Ha a telephelyen nem megoldható az ellátás és az
iskolaorvos rendelője másik városban van, úgy a tanulóközösség egészségnapot
szervez, és a közösséghez tartozó valamennyi gyerek a szükséges legrövidebb
idő - lehetőleg egy nap - alatt kapja meg az ellátást a rendelőben.

## A fizikai biztonsággal kapcsolatos szabályok

Az egyes telephelyek működési engedélyük megszerzése során katasztrófavédelmi és
népegészségügyi ellenőrzésen estek, esnek át. Az ellenőrzés végén kiállított
szakvéleményben foglalt előírásokat a telephelyek működésük során betartják.

Ezen felül fontos, hogy a tanárok és más dolgozók ismerjék az alapvető
biztonsági szabályokat, ezért

A teljes iskola tűzvédelmi és munkavédelmi szabályzatát és oktatási tervét
elkészítése a _tűzvédelmi és munkavédelmi felelősségterület_ gazdájának
feladata.

## Az iskolán kívüli programok szabályai

Amikor a gyerekek az iskolán kívül töltenek tanulási időt, és nem otthoni
környezetben, szükséges a megfelelő biztonságot nyújtó felnőtt kíséret. A
felnőtt kíséret szabályai eltérőek lehetnek tanulóközösségekként több okból:

- a tanulóközösségekhez tartozó gyerekek eltérő életkora miatt

- a helyszín adottságai miatt

## A gyerek-iskola kommunikáció, konfliktus

A Budapest School alapvető célja, hogy a hozzánk járó gyerekek képesek legyenek
már kisgyerekkortól sajátjukként megélni a tanulást. Ez a BPS-ben elsősorban a
saját cél állításán, annak hármas szerződésben rögzítésén, és a mentorkodáson
keresztül valósul meg.

A Modell szerint a BPS-ben a gyerek és az iskola közötti konfliktus alapvető
szereplője, felelőse a gyerek mentora. Minden gyereknek kell, hogy legyen egy
mentora mindig. Ha egy mentor elhagyja a BPS-t, köteles ezért minimum 30 nappal
előtte jelezni, hogy a tanulóközösség találjon a gyerekeknek új mentort.

A konfliktus kezelésére, feloldására a [Modell szabályai](/iskola-szervezet/konfliktusok-kezelese.md#konfliktusok-feszultsegek-kezelese) irányadók, itt most
külön kiemelve, hogy az írásbeli, tiszta megállapodások minden résztvevő számára
segítséget nyújtanak, ezért előírás, hogy a folyamat során a lehető
leggyakrabban szülessenek írásos megállapodások.

## A foglalkozások megtervezése

A foglalkozások megtervezése során a szervezet szintjén a legfontosabb
szabályok, hogy:

- minden gyereknek legyen megfelelő számú modul kiírva az adott trimeszterre,
  amivel teljesítheti az átjárhatósághoz szükséges szintet

- minden foglalkozás jelenjen meg a my.bps rendszerében, az ott leírt
  részletességgel, adatokkal

- a modulok kiírásánál tekintettel kell lennie az évfolyamok szerint
  [teljesítendő óraszámokra](/tantargyak/oraszamok-altgim.md).

- minden gyereknek a tanév összes hetében van legalább egy mentor órája.

A tanulóközösségek nagyfokú autonómiával bírnak a foglalkozások megtervezése
terén, sok szempontból, pl: tantárgyi összevonások, modulok hossza-terjedelme,
tömbösítések, témahetek, projektek, foglalkozások helyszínei. Ugyancsak a
tanulóközösségek döntése, hogy milyen eszközöket használnak a foglalkozásokon,
beleértve a tankönyveket, munkafüzeteket is.

## Az iskola napi-, heti-, éves rendje, összeállításának szabályai

A szervezet éves naptára minden tanév elején készül el, igazodva a Modell [,,tanév
ritmusa"](/tanulasi-elmeny/tanev-ritmusa.md) részéhez.

Ez a naptár legkésőbb szeptember 30-ig megosztásra kerül minden tanárral,
dolgozóval és az összes szülővel. Ez adja a tanév keretét, a teljes szervezet
számára kötelezően.

A tanulóközösségek napi és heti rendjüket maguk állítják össze. Az egyes
gyerekek és tanárok órarendje a my.bps rendszerbe felvitt modulokkal nyomonkövethető. A tanulóközösségek ugyancsak maguk alakítják ki a kirándulások,
ünnepek, kipakolások, egyéb közösségi események rendjét.

## [A hiányzás, távolmaradás nyomon követése](https://model.budapestschool.org/iskola-szervezet/hianyzasok.html)

A hiányzások rögzítése és nyomon követése az e-Kréta rendszerben történik.

Az egyéni tanulási környezetet előre egyeztetnie kell a családnak és a mikroiskola tanárcsapatának. Ezt a szülő az intézményvezetőhöz intézett kérvényben tudja jelezni. A kérvényhez csatolni kell a tanárcsapattal kötött megállapodást is. A szülő egyéb esetben is jelezheti előre, ha a gyerek adott napon nem megy iskolába.
Amennyiben egy tanár észleli, hogy a gyerek nincs jelen az adott foglalkozáson -
és a szülő nem jelezte előre a távolmaradást, -, ezt a tényt az
e-Kréta rendszerben jelzi, melyről a szülő értesítést kap.

A szülőnek az értesítésre  a Kréta rendszerben igazolnia kell, hogy mi a távolmaradás oka. Ha ez
olyan ok, mely szülői igazolás nélkül igazolható (betegség, hivatalos ügy, egyéb
ok), úgy a szülő azt jelzi válaszában. Ha nincs ilyen ok, a szülő akkor is élhet
szülői igazolással, ekkor ezt szükséges jeleznie. Amennyiben a szülő semmilyen
jelzést nem küld, úgy a foglalkozás igazolatlan.

## Évfolyamugrás rendszere

Az osztályozó és különbözeti vizsga a BPS-ben az általánosan használt
visszajelzési és értékelési rendszerhez igazodik, azaz az osztályozó vizsga is
tanulási eredmények elérésével teljesíthető, hasonlóan ahhoz, ahogy az adott
évfolyam teljesíthető. Megegyezik a dokumentálása is, a my.bps rendszerben kell
jelezni az elért tanulási eredményeket. Az osztályozó vizsga sikeres, ha az
összes - az adott évfolyamra szükséges - tantárgyból elérte a gyerek az adott
évfolyamban elérhető tanulási eredmények 41%-át.

Osztályozó vizsgát a szülő vagy a gyerek kezdeményezésére bármikor szervezhet az
iskola, a tanulóközösség.

## Akkreditációs szabályunk

A BPS kizárólag akkreditált telephelyeket és tanulási pontokat működtet. Állandó, folyamatos jelleggel más helyszíneken nem történhet tanulás.

A BPS egy épületében sem tanulhat egyidejűleg az előírtnál nagyobb létszámú gyerek.
"Előírt" =
- akkreditált helyszínnél: a működési engedélyben szereplő "egyidejűleg" létszám
- tanulási pontnál: a szakértői véleményben, vagy hatósági okiratban szereplő létszám
Ez alól a BPS közösség adhat ideiglenes felmentést maximum egy tanévre, amennyiben a felmentést kérő csapat megmutatja, hogy az ideiglenes állapot fizikailag biztonságos a gyerekek számára, valamint alátámasztott és megvalósítható tervet mutat be arra, hogy hogyan éri el az előírt állapotot egy tanéven belül. A BPS közösség a felmentést érdeklődés alapú hozzájárulásos döntésként hozza.

A tanulási pont indításának feltétele, hogy egy népegészségügyi és egy tűzvédelmi szakértő egyértelműen megállapítsa, hogy adott gyerekszámmal az ingatlan akkori állapotában oktatási tevékenység biztonsággal folytatható. Bármelyik szakértő szakvéleménye kiváltható hatósági szakvéleménnyel.

Tanulási pontokat kell akkreditálni (vagy meglévő telephelyeket bővíteni) amennyiben a teljes BPS következő tanévre tervezett összesített gyerekszáma az őszi optimista becslés szerint túllépi a működési engedélyben szereplő összes "egyidejűleg" gyerekszám 90%-át. A következő évi gyerekszám becslést, és az akkor meglévő akkreditált gyerekszámot minden év október 31-ig a teljes szervezettel megismerteti a BPS adatelemző szerepű csapattagja.

Tanulási pontból akkreditált telephellyé kell válnia egy helyszínnek az azt követő tanév kezdetére, amely tanév őszi tervezése során a reális terv szerint átlépik a 30 fős gyerekszámot.


### Telekommunikációs eszközök használata

A 245/2024. (VIII. 8.) Korm. rendeletet figyelembe véve a mikroiskola tanárcsapata saját hatáskörben szabályozza a mobiltelefonok és egyéb telekommunikációs eszközök használatát a foglalkozások során. Ennek megfelelően a tanárcsapat a következő elveket alkalmazza az eszközhasználat szabályozásában:
- Az egyes foglalkozásokon a mobiltelefonok és telekommunikációs eszközök használatának mértékét és módját a mikroiskola tanárcsapata határozza meg. A szabályok kialakításakor figyelembe veszik a gyerekek fejlődési igényeit, az oktatási célokat, valamint az adott foglalkozás jellegét.
- Az eszközök használata akkor engedélyezett, ha az közvetlenül támogatja a tanulási folyamatot, például kutatás, jegyzetelés vagy oktatási alkalmazások használata céljából.
- Amennyiben az eszközök használatára nincs szükség, a gyerekeknek a mikroiskola saját szabályzatában rögzített időben előtt ki kell kapcsolniuk vagy lenémítaniuk ezeket az eszközöket. Az eszközök tárolásnak módját a tanárcsapatok szabályozzák.
- Speciális esetekben a gyerekek vagy szülők kérhetik a mobiltelefonok használatának engedélyezését különleges körülmények (pl. egészségügyi vagy családi okok) miatt. Ezen kéréseket a tanárcsapat mérlegeli és döntést hoz.


## Összefoglaló a további szabályok megalkotásáról
A szabályok megalkotásának határideje az SZMSZ elfogadásától számított 90 nap. Összefoglalva az alábbi szabályok megalkotása szükséges:

| LAB | Valamennyi tanulóközösség |
| --- | --- |
| A LAB elkészíti a családok csatlakozásának folyamatleírását, szabályzatát | Minden tanulóközösség elkészíti és ismerteti a szervezettel és a szülőkkel a saját kapcsolattartási szabályát |
| A LAB elkészíti a mikroiskolák közötti átmenet szabályzatát | A tanulóközösségek mindegyike szabályban rögzíti az egyes szerepek elosztásának módszerét, valamint naprakész listát vezet a felelősökről, és ezeket megosztja a szervezettel |
| A LAB elkészíti és ismerteti a szervezettel a saját működését leíró szabályzatot, benne az egyes szerepekkel, amelyeknek a LAB-ban felelőse van | A tanulóközösségek saját belső szabályt hoznak a helyszínük alapvető működési rendjéről |
| A LAB elkészíti az iskola tűzvédelmi és munkavédelmi szabályzatát és oktatási tervét | A tanulóközösségek saját belső szabályt hoznak baleset, egészségügyi vészhelyzet esetére |
| A TÉR rendszer működtetésének részleteit a LAB dolgozza ki a 2024/25-ös tanév során | A tanulóközösségek saját belső szabályt hoznak iskolán kívüli programok esetére |
