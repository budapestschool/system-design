# Egyéni tanulási környezet

A Budapest School feladata, hogy olyan környezetet biztosítson a
gyerekeknek, amiben boldogak, felszabadultak, magabiztosak és hatékonyak
tudnak lenni. Budapest School családok maguk és önszántukból választják
ezt az iskolát, sok időt és energiát áldoznak arra, hogy az iskolában
tudjanak tanulni. _Ezért az iskola feltételezi, hogy a gyerekek az
iskolában akarnak tanulni önszántukból_.

Sok oka lehet annak, hogy egy gyerek mégsincs az iskolában. Például

- betegnek, fáradtnak érezheti magát, fizikailag vagy lelkileg kimerült, vagy lehet valamilyen fertőző betegsége;
- családjával tölt értékes, minőségi időt, mert fejlődését ez szolgálja a leginkább;
- előre nem tervezett esemény miatt nem tud az iskolába menni;
- utazik, felfedez, külső helyszínre szervezett tanulási programokon vesz részt;
- versenyszerűen sportol;
- tehetséggondozásban vesz részt;
- egy szakmában, tudományterületen annyira magasszintű tanulási környezetre van már szüksége, hogy azt az iskola nem tudja megadni;
- egy projektjébe úgy belemerül, hogy érdemesnek találja nem az iskolában, fókuszáltan végezni a munkát.

A fenti példák is két jól elkülöníthető kategóriába sorolhatóak. A [hiányzásoktól](hianyzasok.md) jól elkülöníthetőek azok az esetek, amikor a
gyerek, bár nem az iskolában tartozkodik, mégis szervezett, strukturált
módon biztosított a fejlődése, tanulása. Ezt az esetet _„egyéni tanulási környezetnek"_ hívja az iskola, mert ezt az időt is
tanulásra szánjuk.

Alapelv, hogy _a mentornak, gyereknek és szülőnek meg kell állapodni a
\_egyéni tanulási környezetről_. Minden félnek tudnia kell róla, meg kell előre tervezni
és nem lehet esetleges. Mindenképpen meg kell különböztetni a [hiányzástól](hianyzasok.md).

## Mit tartalmaz az ETK szerződés?

Az [ETK szerződésben](https://docs.google.com/document/d/1acDrVZW4ZX6Yyy9k30ASfbkL7qwEx_tUeV4ebyrFk6o) a felek (mentortanár, gyerek, szülők) megállapodnak az alábbiakról: 
- Milyen időintervallumot ölel fel az ETK? (Mettől meddig tart ez az időszak?)
- Hogyan jutnak el a gyerekekhez a feladatok? 
- Milyen határidővel kell a feladatokat teljesíteni? 
- Mikor és hogyan kap a gyerek visszajelzést? 
- Melyek azok a foglalkozások/modulok, amelyeket a gyerek személyesen meglátogat? 
- Mikor, milyen gyakran és hol zajlanak a mentori beszélgetések? 

## Egyéni tanulási környezet és a jelenlét

A BPS-modellben az egyéni tanulási környezet annyit jelent, hogy egy gyerek valamennyi időben nem a tanulóközösség többi tagja számára meghirdetett modulokon vesz részt, hanem az iskola tanárai által nem struktúrált tanulási utat jár be.

Ilyen esetekben a gyerek még tartozkódhat az iskola épületében, vagy épp a terv része lehet, hogy nem tartózkodik az iskola épületében. Erre az egyéni tanulási környezet tervének ki kell térnie.

## Mik az ETK feltételei? 

- A tanárcsapatnak minden esetben egyöntetűen egyet kell abban értenie, hogy javasolja az adott gyereknek az ETK-t. A tanárcsapat hozzájárulása nélkül nem lehet ETK-zni. 
- A kérelmet 10 nappal az ETK kezdete előtt be kell adni, kivéve a szeptember 1-jén induló ETK esetén, amikor a kérelmet a tanév első hetén szükséges beküldeni. 
- Az ETK szerződést mind a három (mentor, gyerek, szülők) fél betartja
- A gyerek a szerződésben megállapodottak szerint részt vesz a közösség életében a tanév során

Ha a fenti feltételek nem teljesülnek, az ETK nem indulhat el, vagy a folyamatban lévő ETK-t a tanárcsapat megszüntetheti. 

## Hogyan kezdeményezhetik a szülők az ETK-t? 

A folyamat fontosabb lépései a következők:

1. A szülők egyeztetnek a gyerek mentortanárával arról, hogy valóban megfelelő megoldás lehet-e az ETK. 
2. A mentortanár egyeztet a tanárcsapattal és ha mindenki egyetért, akkor a szülők kitöltik a mentortanárral az [ETK szerződést](https://docs.google.com/document/d/1acDrVZW4ZX6Yyy9k30ASfbkL7qwEx_tUeV4ebyrFk6o). 
3. A szülők kitöltik a Kérvényt 
4. Ezt a mentortanár eljuttatja a LAB-ba, ahol a Budapest School tanügy csapata elbírálja a kérvényt, és ha minden rendben, kiállítja a határozatot.
5. A szülők megkapják az aláírt, lepecsételt és hitelesített ETK határozatot és megkezdődik az ETK.


## Jogszabályi háttér

A 2011. évi CXC. törvény [ Köznev. tv. ] 55.§ kimondja

> Az igazgató a tanulót kérelmére - kiskorú tanuló esetében a szülő kérelmére - felmentheti - a szakmai képzés kivételével - az iskolai kötelező tanórai foglalkozásokon való részvétel alól, ha a tanuló egyéni adottságai, sajátos nevelési igénye, továbbá sajátos helyzete ezt indokolttá teszi.

A BPS–iskola _egyéni tanulási környezet_ esete megfelel az 55.§-nak azzal a megkötéssel, hogy az igazgató _akkor és csak akkor_ mentheti fel a gyereket a foglalkozásokon való részvétel alól, ha

1. megszületett a szülő, gyerek és tanárok által elfogadott megállapodás,
2. ami kitér arra, hogy a gyerek (család) mit tesz azért, hogy az egyéni tanulási környezetében folyamatos legyen a fejlődése és hogy erről hogyan ad számot,
3. az egyéni tanulási környezetben töltött idő terve _nem_ az összes tanórai foglalkozás alóli felmentésre irányul, mert akkor a szülőnek az Oktatási Hivataltól először egyéni munkarendet kell kérnie.

Ez azt is jelenti, hogy nincs más módja a BPS–iskolában az órák látogatása alóli felmentésnek, csak ha elkészül az egyéni tanulási környezetre vonatkozó megállapodás, azaz a gyerek, tanár és szülő kialakítja az egyéni tanulási környezetet.
