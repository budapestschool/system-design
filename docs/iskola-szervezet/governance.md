# Az iskola kormányzása

Az iskola szervezetét a tagjai együtt alakítják. A szervezet élő,
folyamatosan változik, a következő alapelvek mentén:

- Gyorsan és folyamatosan tanuló, agilis szervezetként az iskola mindennap jobban támogatja a gyerekek tanulását, mint az előző napon.

- Minden résztvevőnek a stabilitás, biztonság, kiszámíthatóság iránti igénye pont annyira fontos, mint a változás, a javulás, a fejlődés igénye.

- A gyerekeket leginkább ismerő, a hozzájuk legközelebb álló tanárok (a gyerekekkel és a szülőkkel erős kapcsolatban) minél több helyzetben hoznak döntéseket.

- Állandó az együttműködés, a partneri kapcsolat, a kölcsönös felhatalmazás nem csak a tanár—gyerek kapcsolatban, hanem a tanárok, adminisztrátorok között is.

Azt szeretnénk, hogy gyerekeink kreatív, környezetüket aktívan alakító,
mély partneri kapcsolatokban élő, problémamegoldó, csapatjátékos,
folyamatosan tanuló, a világot változtatni tudó felnőttekké váljanak.
Ehhez olyan iskolát építünk, ahol a tanárok kreatívak, környezetüket
aktívan alakítják, mély partneri kapcsolatban élnek, problémákat oldanak
meg, csapatban dolgoznak és folyamatosan tanulnak.

Az iskolát mint szervezetet a _szociokrácia_ (sociocracy) szervezeti
modellje alapján működtetjük, mert így tudjuk leginkább elérni, hogy
az együtműködésünk egyszerre legyen örömteli és hatékony. Ez egy olyan,
az üzleti világban is kipróbált dinamikus döntéshozatali rendszer, amely
egyszerre segíti a harmonikus és örömteli közösségi együttműködést, és
jelent garanciát arra, hogy az együttműködés hatékony lesz az egyes
csapatokon belül.

## Döntéshozás

Az iskola minden csapata (a tanulócsoportokat vezető tanulásszervezők,
a gyerekek egy csapata stb.) a _hozzájáruláson alapuló döntési
mechanizmust_ (consent based decision making) használja ahhoz, hogy a
szervezet gyorsan tudjon döntést hozni, _és_ minden tagja hallathassa a
hangját.

A csapat valamennyi tagja tehet javaslatokat, ha nem hatékony működést, feszültséget, problémát észlel, és van rá megoldása.
A javaslat értelmezése után az érintettek mindegyikét meg kell
hallgatni, hogy _elfogadhatónak_ tartja-e a javaslatot, azaz
hozzájárul-e a változáshoz, mert _„elég jónak és biztonságosnak
találja, hogy kipróbáljuk az új működést"_ (_„is this good enough for
now and safe enough to try?"_). Fontos, hogy mindenki egyenként hallassa
hangját. A javaslatot akkor tekintjük elfogadottnak, ha és amikor minden érintett
hozzájárult.

Mindenki kifejezheti a _fenntartásait_ (concern), és a csapat feladata
ezeket meghallani, és reagálni rájuk. A fenntartás azonban még nem
jelenti a javaslat elutasítását, csak fontos információt ad a döntés
végrehajtásához.

A javaslatot a csapat nem fogadja el, ha valamelyik csapattag _ellenzi_
azt (objection). Az ellenzés egy én-üzenet, valami ilyasmi: „ha a
csapat ezt a döntést meghozná, akkor mélyen sérülne a csapathoz való
elköteleződésem, mert az én igényemet, ami ... , nem elégíti ki a
javaslat. Nekem szükségem van ... , ezért inkább javaslom, hogy ... ".
Fontos, hogy az ellenvetést megfogalmazó mondja el a saját igényeit,
szükségleteit és tegyen új javaslatot, vagy kérjen segítséget, hogy
milyen új javaslatot tehetne. Ellenvetés esetén a csapat együtt dolgozik
azon, hogy új javaslatot találjon, ami az ellenvetést feloldja, és az
eredeti javaslat célja felé viszi a szervezetet.

#### A hozzájárulás nem konszenzus

A konszenzusalapú döntések esetében mindenkinek egyet kell értenie abban,
hogy a döntés a legjobb, leghelyesebb, leghelyénvalóbb. A Budapest
School iskolában azt a kérdést tesszük fel inkább, hogy van-e valakinek
ellenvetése és a javaslat kellően biztonságos-e ahhoz, hogy kipróbáljuk.
Nem azt a kérdést tesszük fel, hogy mindenki ezt a döntést hozta volna-e
és hogy mindenki egyetért-e a döntéssel hanem azt, hogy mindenki tudja-e
támogatni a csapat egy másik tagját, és nincs-e olyan ismert kockázat,
ami az egyén vagy a szervezet szempontjából nem vállalható.

#### A hozzájárulás nem szavazás

A Budapest School-iskolában nem a többség dönt, és nem az számít, hogy
hányan akarnak egy döntés mellé állni. Mindenki hozhat döntést, amit
elfogad a csapat minden tagja, azaz egyetlenegy ellenvetés sincs.

#### Az ellenvetés nem vétó

A vétójog a döntés megakadályozását jelenti. Amikor valaki
megvétóz egy döntést, akkor azzal a folyamat általában megakad. Az
iskola működésében használt ellenvetés egy beszélgetés megindítását
jelenti: „ezt én így nem tudom támogatni, helyette ezt javaslom
inkább".

#### A „van egy jobb ötletem", nem ellenvetés

A szervezetnek nem az a feladata, hogy a legjobb döntéseket hozza, hanem
hogy amikor szükséges, akkor javítson a működésén. Ezért minden
döntéskor mindenkinek azt kell mérlegelnie először, hogy elfogadható-e
neki, hogy azt a bizonyos javaslatot kipróbálja a csapat. Attól, hogy
valaki jobb, más javaslatot is tud, attól még először az eredeti
javaslatot érdemes kipróbálni és tesztelni.

#### Minden javaslat csak egy hipotézis

Amikor valamit változtatunk a szervezet működésén, akkor egy kísérletbe
vágunk bele: kipróbáljuk, hogy az új működés tényleg jobb-e, megoldja-e
a problémát, feszültséget, kielégíti-e az igényeket. A döntés
támogatásakor ezt a próbálkozást támogatjuk.

#### Nem döntünk mindenről együtt

A Budapest School-iskolában csak az iskola és a tanulóközösség működését
megváltoztató kormányzási kérdésekről döntünk együtt. A Budapest School
minden tagja szerepeiből kifolyólag fel van hatalmazva arra, hogy a
mindennapi döntéseit maga meghozhassa, ezért nem kell mindent
megbeszélnünk. A cél, hogy olyan szerepeket és rendszereket alakítsunk
ki, hogy a mindennapi döntéseket mindenki maga meg tudja hozni.

## Csapatok — az iskola szervezeti egységei

A Budapest School csapatai (a szociokrácia terminológiájában a _körök_)
önálló csoportok egy jól meghatározott céllal, felruházott
felelősséggel, döntési körrel. A csapatok maguk határozzák meg a saját
működésüket (policy making), és végzik el a saját feladatukat. Az
iskolában azok döntenek együtt, akik együtt dolgoznak, egy csapatban
(„those who associate together govern together"). És fontos, hogy akik
együtt dolgoznak, jól legyenek egymással.

Azt is tudjuk, hogy akik egy munkát elvégeznek, azok a munka szakértői,
ezért ők tudnak arról a legjobban dönteni, hogy hogyan érdemes a
munkájukat szervezni, alakítani. Nincs főnök, külső szakértő, aki
megmondja egy csapatnak, mit és hogyan csináljanak addig, amíg a rájuk
felhatalmazott kereteken belül maradnak. Az természetes, hogy minden
segítséget, támogatást, információt megkapnak, amire szükségük van. De a
kormányzás az ő kezükben van.

#### Tanulóközösségek tanulásszervező csapata

A Budapest School szervezet állandó csapatai az egy-egy tanulóközösséget
vezető tanulásszervezők csapata, ami egy _szociokratikus kör_. A
tanulóközösség gyerekeinek (családjainak) és tanárainak életét meghatározó
döntéseket maguk hozzák meg. Így például a napirend, a csoportbontások,
a szülői értekezletek tematikája a saját döntéseik alapján alakul ki.
Fontos, hogy a csapattagok maguk tudják meghatározni, kivel tudnak és
akarnak együtt dolgozni, mikor és mit akarnak csinálni.

#### Csapatok kapcsolódása

Egy-egy ember több csapatnak is tagja lehet. Egyrészt munkacsoportok
alakulhatnak egy-egy feladat elvégzésére, és a Budapest Schoolban egy
ember több részfeladatot is ellát. Másrészt a csapatokat kifejezetten
úgy alakítja a közösség, hogy legyenek köztük kapcsolódások, olyan
tagok, akik összekötik a csapatokat.

Vannak olyan csapatok, melyek elsődleges célja összekötni a kisebb
csapatokat. Például minden tanulóközösség tanárcsapata delegál egy képviselőt az
iskola közös naptárát létrehozó munkacsoportba.

#### Csapatok vezetői

Minden csapatnak van egy _vezetője_ (a szociokrácia terminológiájában a
_circle leader_). A vezető feladata, hogy mindenki ismerje a csapat
célját, a választ a _„miért létezünk?"_ kérdésre, és hogy a csapat
működjön: tiszták legyenek a szerepek és megtörténjen az, amiben
a csapat egállapodott, és működjenek és fejlődjenek a folyamatok.

A Budapest School rendszerében a csapatvezetőke nem az, aki megmondja,
ki mit csináljon; nem osztja, ellenőrzi vagy felügyeli a feladatokat,
nem rúg ki, és nem vesz fel embereket, hanem szolgálja a csapatot
(servant leadership) azzal, hogy segíti a megállapodásokat betartani:
facilitál, moderál, szintetizál, kísér, kérdez. A csapatvezető
megválasztásához, mint minden szerep megválasztásához, a csapat minden
tagjának hozzájárulása szükséges.

## Mi van, amikor egy csapat nem tud döntést hozni, együttműködni

Amikor a csapattagok úgy érzik, hogy nem tudnak mindenki számára
elfogadható döntéseket hozni, nem haladnak, vagy megjelentek a játszmák,
és ezért már nem tudják a csapat célját szolgálni, akkor konfliktus,
feszültség alakul ki, aminek feloldásához segítséget hívhatnak be a
szervezet többi részétől.

A csapat folyamatos harmóniájáért folyamatosan dolgozni kell, ahogy az
egészségünk megőrzésének és a problémák megelőzésének is napi rutinná kell
válnia. Ezért a Budapest School csapatainak erősen ajánlott a
rendszeres visszajelzés, visszatekintés (retrospektív) és a team
coaching.
