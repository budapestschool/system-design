# Hiányzások, mulasztások, igazolások, késések

Hiányzás esetén az első lépés, hogy a szülő és a tanár tudják, hogy
a gyerek hol van. Ne forduljon elő az az eset, hogy a szülő azt hiszi
a gyerek az iskolában van, a tanár pedig azt gondolja, hogy a szülővel van.
Ezért, hiányzás esetén a szülők feladata, hogy legkésőbb
reggel kilencig _írásban_ (a tanulóközösség által meghatározott csatornán)
értesítse a mentortanárt, a hiányzás tényéről.

## Igazolások kezelése

Minden hiányzást valamilyen módon igazolni kell a szülőnek akár utólag is. Tehát
nem elég, hogy szólt, írt, hogy ,,ma nem megy a gyerek", hanem az iskola
adminisztrációs rendszerében igazolnia is kell a hiányzást.

- Egy tanévben 25 nap, vagy 150 óra, de alkalmanként csak 5 nap hiányzást igazolhat önállóan a szülő.

- Orvosi igazolásként orvosi ellátást igazoló dokumentum, kórlap is elfogadható.<!-- Mybps --> A gyerekorvos által írt orvosi igazolást az EESZT rendszer automatikusan továbbítja a KRÉTÁNAK, és az igazolás kiállítása szerinti időszakra adminisztrált hiányzást ezzel igazolja is. Ha a gyerekorvos az igazolást feltölti az EESZT rendszerbe, az igazolással a szülőnek nincs feladata.


- Hatósági intézkedés és egyéb
  alapos indok esetén a 20/2012. (VIII. 31.) EMMI-rendelet 51. § (2)
  értelmében az iskola igazoltnak tekinteni a hiányzást, ha arról a szülő digitális igazolást feltölti az iskola adminsztrációs rendszerébe. <!-- Mybps -->.

## Igazolatlan hiányzás

Igazolatlan hiányzásnak tekintjük:
- ha a hiányzások száma meghaladja a szülők által igazolható órakeretet, és nem érkezik ezekhez kapcsolódó igazolás 
- vagy ha a gyerek mentortanára hosszú ideje (legalább 1 hete) nem tud a hiányzás okáról (vagyis: nem tudjuk, hol van a gyerek). 

A mentortanárnak előre jelzett, de még a KRÉTA rendszerbe fel nem töltött igazolásokat szülői igazolásként tartjuk számon az igazoló dokumentum feltöltéséig. Az órakeret elfogyása után minden 5 napon túli, nem igazolt hiányzás igazolatlan hiányzásnak minősül. Ebben az esetben az iskola szigorúbban jár el, mint általában más
iskolák. Ilyenkor a 20/2012. (VIII. 31.) EMMI-rendelet 51. § (3) pontja
értelmében minden esetben az iskola értesíti a szülőt, és 10 igazolatlan
óra után figyelmezteti, hogy a következő igazolatlan után _„az iskola a
gyermekjóléti szolgálat közreműködését igénybe véve megkeresi a tanuló
szülőjét"_. Az iskola megközelítése
egyszerű: mivel partneri viszonyban van a tanár, gyerek és szülő, ezért
az alapértelmezett az, hogy vagy előre meg lehetett volna beszélni a
hiányzásokat, vagy
betegség miatt kellett túllépni a 25 napot. Elég tág keretet enged az
iskola. Abban az esetben azonban, amikor a gyerek vagy a szülő nem
tartja be a kereteket, nem él a partneri viszonnyal, akkor ott valami
baj van. Gyorsan kell reagálni.

### Hogyan biztosítja a rendszer a visszaélések kiküszöbölését

A Budapest Schoolba járó gyerek szülei és tanárai egyetlen igazolatlan
óra hiányzás után értesítést kapnak arról, hogy a gyerek nem jelent meg
az iskolában. Tehát egy gyerek nem tud szülei tudta nélkül távolmaradni.

Egy szülő 22 napon keresztül „igazolhat" nem tervezett hiányzást, hogy
ne kelljen minden náthánál a körzeti orvosi rendszert terhelni, ahogy
azt a Házi Gyermekorvosok Egyesülete javasolja. Miért feltételezzük,
hogy nem él ezzel vissza a szülő és a gyerek? Mert az iskolában maradás
feltétele a tanulás és a folyamatosan újra felállított tanulási célok
követése. Az ezzel való visszaélés az iskola céljaival ellentétes és
legkésőbb a soron következő trimeszter tanulási szerződésekor a
felszínre kerül.

## Késések kezelése

A Budapest School tanulóközösségek maguk állítják fel a napirenddel
kapcsolatos kereteket: mikor kezdenek, meddig tartanak a strukturált
foglalkozások, mikor vannak a szünetek, és hogyan kezdődik újra a nap
folyamán a fókuszált munka. A kereteket a tanulásszervező tanárok
feladata kialakítani és trimeszterenként megkezdése előtt kihirdetni.

Fontos, megbeszélendő részlet, hogy hogyan kezeli a közösség a
késéseket: mikortól lehet érkezni, mikor kezd a közösség annyira
dolgozni, hogy zavaró, amikor valaki belép és megzavarja a folyamatot.
Megállapodást köt a közösség, hogy hogyan kívánja kezelni a késéseket,
mi segíti a csapatot leginkább a céljai elérésében.

Az iskola nem regisztrálja a késéseket, mert az iskola nem tudhatja,
hogy egy-egy késés elfogadható-e a közösségnek vagy nem. Egy színdarab
főpróbájáról 5 percet késni mást jelent a közösség számára, mint arról
az óráról, ahol mindenki egyedül füllhallgatóval böngészi egy online
tananyag számára legrelevánsabb fejezetét.

Ha egy csoportot megzavar valakinek az ismételt késése, akkor konfliktus
alakul ki a csoport és a késő vagy a tanár és a késő között. Ezt a
típusú konfliktust (is) [a közösségi lét szabályai](konfliktusok-kezelese.md#konfliktusok-feszultsegek-kezelese) szerint kell feloldani.
