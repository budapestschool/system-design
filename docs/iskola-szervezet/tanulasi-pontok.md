# Állandó külső tanulási pontok

A Budapest School iskola több telephellyel rendelkezik, amelyek otthont
adhatnak egy vagy több tanulóközösségnek, mikroiskolának.

A BPS iskolában működhetnek olyan tanulóközösségek, amik állandó jelleggel
egy külső helyszínen tartják a foglalkozásaikat.
Ezeket a helyszíneket hívjuk _tanulási pontoknak_.
Másképp fogalmazva, ha egy tanulóközösség egy trimeszternél hosszabb ideig
egy külső helyen tartja a moduljait, akkor ott tanulási pontot kell
kialakítani.

A tanulási pontok a fenntartó által kialakított, és ellenőrzött
biztonságos terek, amit egy tanulóközösség sajátjaként tud megélni. A
tanulási pontok, bár hivatalosan nem telephelyek, felszerelését és
működtetését a fenntartó és az iskola a telephelyekkel megegyező módon
végzi. A tanulási pontokban található helyiségeket a Budapest School
Általános Iskola és Gimnázium a 20/201. (VIII. 31.) EMMI rendelet
2. számú mellékletében meghatározott felhatalmazás alapján alakítja ki:
_Az eltérő pedagógiai elveket tartalmazó nevelési program az eszköz- és
felszerelési jegyzéktől eltérően határozhatja meg a nevelőmunka eszköz
és felszerelési feltételeit._

## A tanulási pontok fizikailag biztonságosak

A tanulási pontok kialakításakor ugyanazokat az előírásokat kell
betartani, mint amit a 40/2018. (XII. 4.) 16. EMMI rendelet a Tanodáknak
előír, azaz:

A tanulási pontnak vagy a tanulási pontnak helyet biztosító, más
szolgáltatást is befogadó integrált térnek meg kell felelnie a
létesítési, használati és üzemeltetési tűzvédelmi előírásoknak, amelyet
-- a fenntartó kérésére -- egy független ellenőr állapít meg és évente
ellenőriz. A tanulási pontban biztosítani kell

1. legalább egy WC-t és folyóvíz vételére is alkalmas kézmosót;
2. legalább egy teakonyhát és
3. legalább egy, a következő bekezdésben foglaltak szerint kialakított
   közösségi teret, amely
   1. legfeljebb két helyiség egybenyitásával alakítható ki, legalább
      30 négyzetméter alapterületen,
   2. jól szellőztethető, fűthető és természetes fénnyel megvilágított
      helyiségben, helyiségekben alakítható ki,
   3. a tanoda szolgáltatás nyújtásával azonos időben más célra nem
      használható, és
   4. nem alakítható ki iskola működő feladatellátási helyén;

A WC, kézmosó és teakonyha más szolgáltatással közösen használt
helyiségek is lehetnek.

## A tanulási pontok egy telephely alá tartoznak

A tanulási pontok nem alkalmasak minden iskolai feladat és szolgáltatás
biztosítására, ezért a telephely és a fenntartó biztosítja, hogy a
gyerekek érdekében előírt feltételek a tanulási pontokon lévő gyerekek
számára is elérhető legyen vagy a telephelyen, vagy más szerződött
partnernél. A tanulási pontokon akár állandó jelleggel tartózkodó
gyerekek is iskolába járással teljesítik a tankötelezettségüket Budapest
School Általános Iskola és Gimnáziumban (Nkt. 45. § (4) első fordulata).

Az élménynapokon a tanulási pontokon lévő gyerekek rendszeresen
meglátogatják a telephelyeket. Ott el fognak férni, mert a telephelyen
állandó jelleggel tanuló gyerekek épp az iskolán kívül vannak.

## A tanulási pontok ellenőrzött helyszínek

A tanulási pontokat a fenntartó évente ellenőrzi -- megfelelő
végzettséggel és jogosítvánnyal bíró hatósági vagyy nem hatósági
ellenőrök bevonásával --, hogy azok megfeleljenek a gyerekek rendszeres
ott tartózkodása miatt elvárható legszükségesebb közegészségügyi
előírásoknak, valamint a tűzvédelmi, egészségvédelmi munkavédelmi
követelményeknek.

A fenntartó minden mikroiskolát, és így a tanulási pontokon működő
tanulóközösségeket is trimeszterenként legalább egy tanítási napon
megfigyeli, és ez alapján értékelő visszajelzést ad mind a pedagógiai
munkával, mind a fenti elvárásokkal kapcsolatban.

## A tanulási pontokon megfelelő tanárok vannak

Egy tanulási pont egy telephely teljes mikroiskájának ad helyet, így a
tanulási pontokon a tanulóközösség tanulásszervezői és külső modultanárai
elérhetőek, rendszeresen jelen vannak. A tanulási pontokon dolgozó
tanárok a telephely szervezet alá tartoznak a munkaszerződésük alapján,
munkavégzésük helye módosul, kiegészül a tanulási ponttal.

## Kizárólag a székhelyen és telephelyeken végezhető feladatok

A székhely és telephelyek az iskola törvényileg elismert feladatellátási
helyei. Csak itt történhet

1. az iskolába való be- és kiiratkozás, a bizonyítvány kiállítása;

2. a trimeszterenkénti egy _nagy tudáspróba és portfólióbemutatás_,
   amikor a gyerekek más gyerekeknek és más tanároknak mutatják be,
   hogy mit tanultak, alkottak az elmúlt időszakban;

3. más tanulóközösségek számára elérhető _közös modulok_ foglalkozásai.

### Kizárólag szerződött partnernél, tanulási pontokon végezhető feladatok

- A trimeszterek teljes hosszán végigfutó modulokat olyan szerződött
  partnereknél lehet végezni, melyeknek legalább a trimeszter teljes
  hosszára van az iskolával vagy a fenntartóval bérleti vagy
  szolgáltatói szerződése.

- A speciális feltételeket, eszközöket, vagy termeket megkívánó, de
  nem egy teljes trimeszterig, hanem akár eseti jelleggel meghirdetett
  modulokat (például úszás, digitális alkotó labor, falmászás,
  csillagvizsgálat) kizárólag akkor lehet meghirdetni, ha erre van
  legalább a modul megtartását megelőző egy héttel előbb megállapodása
  az adott gyerekek tanulásáért felelő tanulásszervezőknek egy a
  feladat elvégzését hitelesen bizonyító partnerrel, és erről
  ugyaneddig a határidőig a szülőket is tájékoztatja.

### Bárhol végezhető feladatok

Eseti jelleggel a tanulásszervezők koordinátorának beleegyezésével
minden modultanár dönthet úgy, hogy valamely modulját külső helyszínen,
például parkban, erdőben, egy cégnél vagy civil szervezetnél
vendégeskedve tölti el.

Abban az esetben, ha egy modul lényeges tartalmi eleme, hogy külső
helyszíneken történik a tanulás, akkor erről előre kell a szülőket
tájékoztatni. Például egy _gyárlátogatás_ modul esetén egyben a modul
elején megadják a szülők a hozzájárulásukat a külső helyszínekhez, nem
kell minden héten erről újra megállapodni.

### Otthon végezhető feladatok

A Budapest School egész napos iskolát biztosít, mégis lehetnek olyan
esetek, amikor egy modul elvégzésének épp az a feltétele, hogy a gyerek
otthon maradhasson. Vagy azért, mert egyéni munkában akar egy projektje
végére jutni, vagy azért, mert szülői támogatásra van szüksége ahhoz,
hogy tanulmányi eredményeiben előreléphessen. Erre akkor kerülhet sor,
ha az adott modulban történő előrehaladás a gyerek mentorának
beleegyezésével történik, és nem veszélyezteti a trimeszterben felvett
további modulok elvégzését.
