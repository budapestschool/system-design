# A közösségi lét szabályai

A Budapest School egy közösségi iskola: a gyerekek együtt tanulnak és
alkotnak, a tanárok csapatokban dolgoznak, és a szülők is egy elfogadó
közösség részének érezhetik magukat. A tagok — a tanárok, a gyerekek, a
szülők és az adminisztrátorok — azért csatlakoznak a közösséghez, mert
itt szeretnének lenni, és itt érzik magukat boldognak, egészségesnek és
hasznosnak. A közösség azért fogad be új tagokat, hogy nagyobb, erősebb
közösség lehessen.

#### Partnerség

A tanulásszervezők és a családok közös célja, hogy közösségük a gyerekek fejlődését
együtt támogassa. A tanulás holisztikus szemlélete révén a gyerekeknek
egyszerre kell jó kapcsolatot ápolniuk saját társaikkal, partnerségben
lenniük tanáraikkal és meghallaniuk a szülők feléjük irányuló kéréseit. A
tanulás célját a gyerekek maguk határozzák meg, azok az értékek,
rituálék viszont, melyek a mindennapjaikat meghatározzák, túlmutatnak az
egyénen, és a közösség céljait szolgálják oly módon, hogy az az egyes
családok életvitelével is összhangban legyen.

Minden tanulóközösségnek feladata ezért, hogy az együttműködésük,
a közösségi szabályaik, a házirendjük meghatározásakor a családok
véleményét, javaslatait is kikérjék, és úgy hozzanak döntést, hogy az
mindenki számára kellően biztonságos és elég jó döntés legyen.

Ez vonatkozhat a mindennapi eszközök használatára, a közlekedésre, az
együtt, tanulással eltöltött egyéni és csoportos idő arányaira, a
közösen megtartott ünnepekre, jeles napokra, mindazokra a kérdésekre,
melyek közösségi döntések, és hatással lehetnek az egyén fejlődésére.

#### Alapcsoportok

A Budapest School iskola kisebb [tanulóközösségekből](/tanulasi-elmeny/tanulo-kozosseg.md#a-budapest-school-tanulokozossegei)
fejezetet) áll. Ez a gyerekek és tanárok elsődleges közössége: a gyerekek számára ez
az a közösség, akikkel együtt járnak iskolába, együtt alakítják az
iskolájukat, a tanárok számára ez az a csapat, akikkel együtt dolgoznak, hogy
létrehozzák, fenntartsák és fejlesszék a tanulóközösség gyerekeinek tanulási
környezetét.

A tanulás egysége a [modul](/tanulasi-elmeny/modulok.md#tanulasi-tanitasi-egysegek-a-modulok). Egy modul csoportjában a közös érdeklődésű és célú gyerekek tanulnak együtt, mert együtt boldogabban, hatékonyabban el tudják érni a céljukat.

#### Saját szabályok

A tanulóközösség és akár egy-egy modul kereteit, szabályait a résztvevők
alakítják ki. Pontosabban a tanárok felelőssége és feladata, hogy
minden tanár és gyerek számára elfogadható, betartható, releváns és
értelmes szabályok legyenek.(Mikroiskola esetén a tanulásszervező tanárok, modulok esetén a szaktanárok és a tanulásszervező tanárok közösen.)

Mind a gyerekek, mind a tanárok javasolhatnak új szabályokat vagy szabályváltoztatást. Minden egyes
gyereknek és tanárnak hozzájárulását kell adnia minden javaslathoz,
mondván: , _, it's good enough for now and safe enough to try"_, azaz:
_„elég jónak és biztonságosnak tartom, hogy kipróbáljuk"_ a szabályokat.

Ebből következik, hogy minden tanulóközösségnek saját házirendje, szabályai
lehetnek, és modulonként alakulhat, hogy mit, mikor, hogyan és kivel
csinálnak a résztvevők.

A szabályok alakításának elsődleges szándéka mindig az legyen, hogy a
gyerekek fejlődése, tanulása, alkotása és a közösség működése egyre jobb
legyen.

![A szabályok célja, hogy a közösséget szolgálja.](../pics/5b_dontes.jpg)

## Konfliktusok, feszültségek kezelése

Tudjuk, hogy a Budapest School szereplői, a gyerekek, a tanárok, a szülők, a pedagógiai program, a kerettanterv, a fenntartó, a szomszédok, az állami hivatalok között feszültségek és konfliktusok alakulhatnak ki, mert különbözőek vagyunk, különbözőek az igényeink. A feszültségekre és a konfliktusokra a Budapest School olyan lehetőségként tekint, amelynek együttműködésen alapuló megoldása építi a kapcsolatot, és segíti a fejlődést.

Az iskola partnerségen alapuló szervezetében nem autoritások, főnökök, hivatalok, bírók oldják meg a konfliktusokat, hanem egyenrangú társak. Az iskola feltételezi, hogy a felek tudnak gondolkodni, következtetni, felelősséget vállalni a döntéseikért és cselekedeteikért.

### Alapértékek

A Budapest School közösségének valamennyi tagja (a tanárok, gyerekek, szülők, adminisztrátorok, iskolát képviselő fenntartó) vállalja:
- A közösség mindennapjaival kapcsolatos konfliktusok esetén elsőként az abban érintett személynek jelez közvetlenül.
- A személyes kritikát mindig privát csatornán fogalmazza meg először, ha kell, akkor segítő bevonásával.
- Bármelyik fél jelzése esetén lehetőséget biztosít arra, hogy a vitás kérdést megbeszéljék közvetlenül, a folyamatban részt vesz.
- Az egyeztetésre elegendő időt hagy, amely legalább 30 nap, vagy (ha ennél több időre van szükség) a másik féllel megállapodott idő.
- Teljes figyelemmel, nyitottsággal, a probléma megoldására fókuszálva igyekszik feloldani a konfliktust, és közösen megoldást találni a problémára.

Ahhoz, hogy tényleg partneri kapcsolatban, egyenrangú felekként lehessen konfliktusokat, problémákat megoldani, érdemes közös értékeket elfogadni.
- Felelősséget vállalunk gondolatainkért, hiedelmeinkért, szavainkért és viselkedésünkért.
- Nem pletykálunk, szóbeszédet nem terjesztünk.
- Nem beszélünk ki embereket a hátuk mögött.
- Félreértéseket tisztázzuk, konfliktusokat felszínre hozzuk.
- A problémákat személyesen, szemtől szemb beszéljük meg, másokat nem húzunk bele a problémába.
- Nem hibáztatunk másokat a problémákért. Amikor mégis, akkor az egy jó alkalom arról gondolkozni, hogy miként vagyunk mi is része a problémának, és miként kell a megoldás részévé válnunk.
- Az erősségekre több figyelmet fordítunk, mint a gyengeségekre, és a lehetőségekről, a megoldásokról többet beszélünk, mint a problémákról.



## Kiemelt konfliktusok

### A tanárcsapat és a szülők közti konfliktusok
A konfliktusok három stádiumát különböztetjük meg, és ezeket eltérően kezeljük:
1. _Konfliktusfeloldó folyamat:_ célja az iskola és a család közti kapcsolódás, resztoráció, valamint megállapodás a tanulságokról és folytatásról
2. _Piros folyamat:_ célja, kideríteni, hogy egy tartósan fennálló probléma (ártalom, konfliktus, fizetési elmaradás) ellenére tudunk-e együttműködni, tiszta megállapodást kötni a családdal. Ebbe a folyamatba a LAB is bekapcsolódik.
3. _Búcsú folyamat:_ amikor a piros folyamat végén az kerül kimondásra, hogy elválunk a családtól, a búcsú folyamatban arról állapodunk meg, hogyan fog ez megtörténni. 


#### A konfliktusfeloldó folyamat:
Amikor a iskola és család közti együttműködést akadályozó egyet-nem-értés alakul ki, akkor konfliktusfeloldó folyamatot kell indítani. Ennek célja a szükségletek és lehetőségek megismerése annak érdekében, hogy megállapodásokat tudjunk megfogalmazni - vagyis újra létrejöhessenek az együttműködés feltételei. 

__A konfliktusfeloldó folyamat lépései:__
1. A konfliktusfeloldó folyamat kezdeményezése: a jelzés érkezhet a tanárcsapat egy/több tagjától vagy szülőktől is. 
- Ha tanár(ok) jelez(nek): a csapatuk számára egyértelműen jelzik, hogy konfliktus alakult ki, és konfliktuskezelő folyamatot szeretnének indítani. A csapat kiválasztja a folyamat érintettjeit (azokat, akiknek a tisztázó beszélgetésben szükséges részt venniük) és facilitátorát. 
- Ha a szülők jeleznek: a mentor számára email üzenetet küldenek, amiből kiderül, hogy milyen konfliktus alakult ki, és hogy konfliktusfeloldó folyamatot szeretnének kezdeményezni. A mentor ezt jelzi a tanárcsapatnak, kiválasztják a folyamat facilitátorát ill. értesítik a szülőket és a tanárcsapat többi érintettjét a folyamat indulásáról. A tisztázó beszélgetés előkészítése: a folyamat facilitátora a jelzéstől számított egy héten belül egyeztet a szülőkkel a konfliktusról egy rövid személyes vagy online meetingen, és egyeztet az iskolai érintett(ekk)el egy rövid személyes vagy online meetingen. A facilitátor megismeri a konfliktus részleteit és felkészíti az eset résztvevőit a tisztázó beszélgetésre.
2. Tisztázó beszélgetés: a facilitátor vezetésével a konfliktus résztvevői találkoznak, a beszélgetés célja a konfliktus rendezése és megállapodás. A személyesen megtartott beszélgetésről a helyszínen a résztvevők által jóváhagyott jegyzet készül (tartalmazza a résztvevők megállapodását), amit aznap mindenki megkap e-mailben is. 
3. Szükség esetén a facilitátor egy második tisztázó beszélgetést is összehív.
4. Megosztás: a tisztázó beszélgetés részeként a résztvevők a facilitátor segítségével megfogalmazzák, hogy lettek-e olyan közös tanulságok a konfliktus kapcsán, amelyeket érdemesnek tartanak a tanárcsapattal is megosztani. Ha igen, akkor ezt egy mindenkinek elküldött e-mailben a facilitátor megteszi. 

Ha a két tisztázó beszélgetés alatt nem sikerül a konfliktust megnyugtatóan lezárni, vagy ezt követően a közös megállapodások nem teljesülnek  és kiújul a konfliktus, akkor piros folyamatot lehet indítani.

#### A piros folyamat:

A piros folyamat egy protokoll, amit akkor követünk, amikor egy helyzet vagy konfliktus tartósan nehézzé vált, eszkalálódott. 

__A piros folyamat lépései:__

1. A a tanárcsapat egyértelmű hozzájárulással jelzést tesz a LAB-nak, hogy egy családdal piros folyamatba szeretne kezdeni. 
A jelzés kezdeményezésének kritériumai:
- __Amikor a gyerek nincs jól__ akkor lehet piros folyamatot indítani, ha legalább három hónappal korábban történt intervenciós jelzés a gyerekkel kapcsolatban, és fennáll egy olyan helyzet, amiben a gyerek vagy a közösség sérül (például amikor bántás éri a közösség egy/több tagját; amikor a közös tanulás súlyos akadályokba ütközik; amikor a fizikai környezetben történik károkozás; vagy ha az intervencióban érintett gyerek nem halad a tanulásban, és ennek a helyzetnek a fenntartása káros lenne a számára). 
 
- __Amikor a családnak pénzügyi elmaradása van__ akkor lehet piros folyamatot indítani, ha több mint 90 napos tagdíj elmaradása van a családnak. 

- __Amikor tartós konfliktus alakult ki a tanárcsapat és a szülők között__ akkor lehet piros folyamatot indítani, ha két tisztázó beszélgetés is megtörtént, és nem sikerült helyreállítani az együttműködést, vagy idővel kiújult a konfliktus. 

2. A LAB piros folyamat koordinátora áttekinti az iskola kezdeményezését. Ha teljesülnek a piros folyamat indításának kritériumai, akkor 
- az iskolával együtt kiválasztják a piros folyamat esetmanagert, aki
- összehívja az első piros folyamat beszélgetést, és
- dönt arról, ki vesz részt a BPS képviseletében, és
- megszervezi a találkozót, meghívja az érintetteket

3. Sor kerül az két piros folyamat beszélgetésre. Az első beszélgetés célja a szükségletek és lehetőségek feltárása. A második beszélgetés célja a megállapodás a folytatásról és a nyomonkövetésről. Mindkét beszélgetésnek személyesnek kell lennie, és készül róluk feljegyzés. 

4. A beszélgetéseket követően az esetmanager előre ütemezett, kétheti rövid follow-up egyeztetésre hívja a folyamat résztvevőit. 

Ha a piros folyamat beszélgetéseken nem sikerül az iskola és a család számára is elfogadható megállapodást kötni, akkor kimondható, hogy nem folytatjuk az együttműködést, el kell válnunk. A kezdetektől fogva világosnak kell lennie minden fél számára annak, hogy ez a piros folyamat tétje. 

#### A búcsú folyamat:

Amikor a piros folyamat eredménytelen (nem tudunk megállapodni, vagy a megállapodások nem teljesülnek, vagy a probléma fennmarad/kiújul), akkor eljön a búcsú ideje. A BPS által kezdeményezett búcsú előzménye mindig egy piros folyamat kell, hogy legyen. Az iskola nem kezdeményezheti az elválást egy családtól anélkül, hogy a konfliktuskezelés lépéseiben nem próbálta megtalálni a megoldást. 

__A búcsú folyamat lépései:__
1. A LAB-os piros folyamat koordinátornak, a mikroiskola menedzserének és az esetet gondozó piros folyamat esetmanagernek egyet kell értenie abban, hogy kimondható: végigjártuk a konfliktuskezelés szintjeit, mindent megtettünk és el kell válnunk. Eldöntik, hogy a szervezetből kit indokolt bevonni a búcsú folyamatba.
2. A piros folyamat koordinátor összehívja az első búcsú-beszélgetést. A beszélgetés célja annak megtervezése, hogyan válunk el, ki és mit fog tenni és milyen határidővel (pl. hogyan segítünk az iskola keresésben? mikor érkezik a befogadó nyilatkozat? mi a szerződésbontás folyamata, hogyan rendezzük a pénzügyeket? milyen értékelési/visszajelzési dokumentumokra van szükség? mi történik az iskolai búcsúceremónián, hogyan szeretnénk elköszönni a gyerektől?)
3. A beszélgetést követően a piros folyamat felelős előre ütemezett kétheti rövid follow-up egyeztetésre hívja a folyamat résztvevőit. 



### Gyerek-gyerek konfliktus

A mindennapokban a gyerekek akarva-akaratlanul is belecsöppennek olyan
helyzetekbe, amikor a közösségben vagy interperszonális kapcsolataik
során megborul a mindennapi egyensúly. Ezekben a konfliktushelyzetekben
leginkább a felborult egyensúly helyreállítására törekszünk _helyreállító
konfliktusfeloldási technikával_.

A helyreállító konfliktusfeloldás alaptézise az, hogy minden ilyen
megborult egyensúlyi állapot egy lehetőség valaminek a megújítására,
újragondolására. A résztvevők egy külső személy segítségével (általában
a jelen lévő tanáréval) alakítják a megoldást, egészen addig, amíg az
eredmény mindenki számára a konfliktus feloldását, azaz a
megborult egyensúly helyreállítását jelenti.

A folyamat során a konfliktusban részt vevő összes személy elmondja az
érzéseit, meglátását a felmerülő helyzettel kapcsolatban, valamint az
én-közléseken túl a szükségleteikről is beszélnek. Ezen szükségletek
képezik a megoldás alapját, azaz ezeket egy tető alá hozva feloldhatjuk
a fennálló konfliktust. Ilyenkor mindig először azokat a pontokat
keressük meg, amelyekben egyetértenek a résztvevők, hiszen ez közös
alapot szolgáltat arra, hogy a valódi feloldást megtalálhassuk.

Fontos, hogy a beszélgetésben az összes fél hallassa a hangját, és meg
is legyen hallgatva. Az értő figyelem kompetenciája is fejlődik ezen
módszer alkalmazása során, például, ha az érintett felek elmondják, hogy
mit hallottak meg abból, amit a másik elmondott.

Előfordulhat, hogy ez a folyamat nem egyből a konfliktus után indul el:
ha a résztvevők beleegyeznek, akkor a beszélgetés elhalasztható, de
lehetőleg még aznap történjen meg.


### BPS modell, egyedi program, házirend be nem tartásával kapcsolatos konfliktusok

Amikor egy tanár, egy gyerek vagy egy tanulóközösség nem tartja be az egyedi programot, a házirendet, vagy egyéb közösen
megalkotott szabályokat, akkor konfliktus alakul ki közte és az iskola
között. Ilyenkor szintén a konfliktusfeloldás folyamatát kell
alkalmazni.
