# Minőségfejlesztés, folyamatszabályozás

> Honnan tudjuk, hogy jól működik az iskola? Minden gyerek azt
> tanulja, amit szeretne, és amire neki a leginkább szüksége van?
> Megkérdezzük az érintetteket, figyeljük az adatokat és azok alapján fejlesztünk.

A Budapest School-iskolában a gyerekek tanulását egy komplex rendszer
egyik elemének tekintjük [(Barabasi, 2007)](http://barabasi.com/f/226.pdf). Iskoláink a világ felé
nyitott hálózatot alkotnak: az egy közösségben lévő gyerekek, a
családok, a velük foglalkozó tanárok, a helyi környezet, a Budapest
School további közösségei, az ország és a nemzet
állapota, valamint a globális társadalmi folyamatok is befolyásolják,
hogy mi történik egy iskolában.

Hogy egy gyerek épp mit és mennyit tanul, az nemcsak az iskola programjától vagy a NAT-tól,
hanem számos más tényezőtől is függ, melyek befolyásolják a fejlődést és
a közösség egymás fejlődésére gyakorolt hatását: többek közt függ a
gyerekek múltjától, aktuális hangulatától és vágyaitól, a tanárok
személyiségétől, a családtól, a csoportdinamikától és a társadalomban
történő változásoktól is.

Ezért a gyermekeink fejlődését, tanulását és boldogságának alakulását
egy _komplex rendszer_ működésével modellezhetjük.

A tanulási folyamataink minőségfejlesztésénél a következő szempontokat
vesszük figyelembe:

1. A történéseket, eseményeket, (rész)eredményeket folyamatosan kell monitorozni.

2. A visszajelzéseket a rendszer minden tagjától folyamatosan gyűjteni kell: a gyerekektől, tanároktól, szülőktől és az adminisztrátoroktól.

3. Anomália esetén a helyzetfelismerés, az eltérések okának felkutatása a cél.

4. A feltárt hibák alapján a rendszert folyamatosan kell javítani.

A minőségfejlesztés célja az iskola, mint tanulórendszer folyamatos
fejlesztése. A monitorozás folyamatos, így az iskola hamar felismeri az
anomáliákat, és kivizsgálás után, ha szükséges, meg tudja azokat
szüntetni, tanulni is tud belőlük, és javítja a rendszert.

A fenntartó folyamatosan monitorozza a tanulóközösségeket és
visszajelzéseket ad, aminek alapján a tanulóközösség javítja a saját működési
folyamatait. A BPS modellben leírt működést a fenntartó mérhető és
megfigyelhető indikátorokra fordítja le, és kidolgozza, üzemelteti a
metrikák 2-3 hónapnyi rendszerességű nyomon követésére alkalmas
rendszerét.

## BPS minimum indikátorai

A javaslatban szereplő lista tételei olyan minimumok, amelyekre igaznak kell lennie annak, hogy:

- hozzájáruláson alapuló szervezeti döntés született róluk
- felülbírálatuk vagy elvetésük nem tartozik a tanárcsapatok autonómiájának hatáskörébe
- teljesülésük demonstrálható,

A BPS minimumok megvalósulását a tanév során figyelemmel kísérjük, és ha egy iskolában ezekkel kapcsolatban elakadások vagy nehézségek alakulnak ki, az iskolakísérő kezdeményezésére elindul egy támogató folyamat a probléma feltárására és megoldására.
a teljesíthetőség feltételeinek megteremtése a teljes BPS közösség felelőssége (kiegészítés: mindenki a saját domainje által definiált felelősség gyakorlásával)

### Mentorálás

> Minden gyereknek van egy kitüntetett tanári kapcsolata, ami lehetővé teszi a személyre szabott tanulás támogatást, visszajelzést és reflexiót.

#### Mennyiségi kritériumok

- minden gyereknek van egy mentora
- egy mentorhoz maximum 12 gyerek tartozik
- a gyerekeknek van rendszeres mentorbeszélgetésük min. 2hetente (csoportos vagy egyéni formában)
- trimeszterenként min. 1 konzultáció a családdal (szerződő beszélgetés)
- a gyerekek minden trimeszter végén kapnak részletes írásos visszajelzést a mentoruktól (trimeszterzáró feedback)

#### Minőségi kritériumok

- High level feedback: a trimeszterzáró visszajelzés a modulzáró visszajelzések szintézisét is tartalmazza
- A mentor minőségi figyelmet ad, ismeri a mentoráltjait
- Célállításban, döntéshozatalban, reflektív gondolkodás fejlesztésében segít
- Partneri kapcsolat a szülőkkel, a mentor világos megállapodásokat köt.

#### Demonstráció módja

- Mentor feltűntetése a myBPS-ben
- Mentoridők/bandaidők benne vannak a gyerekek órarendjében
- Trimeszterzáró vissszajelzés a rubric összesítőben
- Szülői kérdőívben a szülők visszajelzései a mentor-mentorált kapcsolatról

### Értékelő-táblázatok, visszajelzés

> Rendszeres, visszakereshető, részben standardizált írásos visszajelzések segítségével nyomon követni és láthatóvá tenni a gyerekek haladását a tanulásban, fejlődésben.

#### Mennyiségi kritériumok

- trimeszterenként min. 1 modulzáró értékelő táblázat visszajelzés minden (valódi) lezárult modulról
  modul záródátuma után legkésőbb 2 héttel
- félévenként, tantárgyakhoz kapcsolódó értékelő táblázat minden tantárgyból, amiről a gyerek a tantárgyi struktúra szerint osztályzatot kaphat.

#### Minőségi kritériumok

- a modul céljaival szinkronban lévő visszajelzési szempontok
  kidolgozott szintleírások az értékelő táblázatokban
- tantárgyi standard szempontok beépítése a modulzáró viszajelzésekbe

#### Demonstráció módja

- trimeszterenként megszülető értékelő táblázat összesítők

### Saját tanulási cél

> A gyerekek gyakorolják a célállítást és autonómiát a tanulásban.

#### Mennyiségi kritériumok

- min. 1 saját cél/trimeszter, amiről szerződés is készül
- megjelenik a trimeszterzáró visszajelzésben mint szempont

#### Minőségi kritériumok

- SMART célmegfogalmazás
- tanár és szülő támogatása egyértelmű
- a cél eléréséért a gyereknek erőfeszítést kell tennie
- a gyerek portfóliójában van rá reflexió

#### Demonstráció módja

- hármas szerződésben foglalt saját célok
- trimeszterzáró visszajelzésben megjelenik a saját célok elérése, mint szempont
- szülői kérdőív visszajelzései a saját célokról

### Gyerek portfólió

> Erősíteni a gyerek autonómiáját, felelősségét, szerepét a saját tanulásában. Reflektív gondolkodás és metakogníció fejlesztése.

#### Mennyiségi kritériumok

- minden gyereknek van digitális/papíralapú/blended portfóliója

#### Minőségi kritériumok

- minden trimeszterben bővül, a gyerek tanulási útját jeleníti meg,
- a mentorvisszajelzést és a gyerek reflexióit is tartalmazza

#### Demonstráció módja

Nincs

### Moduláris tanulásszervezés

> Tanári tervezés eredményeként megszülető tanulási ívekből épül fel a gyerekek egész éves tanulási folyamata.

#### Mennyiségi kritériumok

- legfeljebb trimeszter (12 hét) hosszúságú modulok
- minden (valódi) modulnak van kitöltött modulleírása myBPS-ben

#### Minőségi kritériumok

- holisztikusan tervezett modulok: a célok, tevékenységek és visszajelzési szempontok összhangban vannak
- significant learning: tantárgyakhoz köthető és azokon túlmutató fejlesztési célokat (pl. EQ, globális nevelés, társadalmi részvétel, stb.) is magába foglal
- a modul elvégzése bővíti a gyerek portfólióját

#### Demonstráció módja

myBPS modul adatlapok

### szervezeti kapcsolódás/tanári tanulás

> Tanulószervezet vagyunk, amelyben a résztvevők folyamatosan bővülő tudását erőforrásként használjuk arra, hogy kifejlesszük a saját iskolai jó gyakorlatainkat.

#### Mennyiségi kritériumok

- a mikroiskolák képviseltetik magukat a közösségi események min. 50%-án
- senior tanárai bekapcsolódnak a fejlesztési projektbekbe (1 projekt/év)

#### Minőségi kritériumok

- alkotó részvétel a BPS modellt/szervezetet fejlesztő projektekben
- saját iskolai jó gyakorlatok megosztható tudássá transzformálása

#### Demonstráció módja

- jelenlét a közös tanulási alkalmakon
- projektek dokumentációi

### színes folyamatok

> A problémák, nehézségek gyerekfókuszú, partneri, tiszta megállapodásokon és nyomon követésen alapuló kezelése.

#### Mennyiségi kritériumok

- van felelőse az iskolában a színes folyamatoknak
- az elindult folyamatok dokumentáltak
- 22/23 tanévtől: a mikroiskola mentortanárai részt vettek színes folyamat felkészítsen

#### Minőségi kritériumok

- tiszta, partneri kommunikáció a családdal
- keretek tartása
- a jelzések időben megtörténnek

#### Demonstráció módja

- THC beszélgetés
-

### partnerség a szülőkkel

> A gyerek érdekeit előtérbe helyező, keretezett együttműködés az iskola és a család között

#### Mennyiségi kritériumok

- rendszeres (legalább 5 alkalommal a tanévben) megszervezett szülői körök
- visszakereshető hármas szerződések,
- szerződő beszélgetések minden trimeszterben
- rendszeres híradás az iskolai történésekről, tanulásról

#### Minőségi kritériumok

- világos együttműködési szabályok és egyértelmű kommunikációs csatornák,
- őszinte, asszertív, "intézményi" fordulatoktól mentes, érthető kommunikációs stílus,
- resztoratív konfliktuskezelés

#### Demonstráció módja

- szülői körök kérdőíve
- szülői kérdőív
- hármas szerződések myBPS-ben

## Gyerekek, tanárok, szülők kérdőíves monitorozása

A BPS iskolának rendszeresen kérdőíves monitorozást kell tartania.

1. A szülők biztonságban érzik gyereküket, és eleget tudnak arról, hogy mit tanulnak. Indikátor: kérdőíves vizsgálat alapján.

2. A tanárok hatékonynak tartják a munkájukat. Indikátor: kérdőíves felmérés alapján.

3. A gyerekek úgy érzik, folyamatosan tanulnak, támogatva vannak, vannak kihívásaik. Indikátor: kérdőíves felmérés alapján.
