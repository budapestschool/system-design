# Tanárok kiválasztása, tanulása, fejlődése és értékelése

Az iskola legmeghatározóbb összetevői a tanárok. Ezért a Budapest School
külön figyelmet fordít arra, hogy ki lehet tanár az iskolákban, és
hogyan segítjük az ő fejlődésüket.

## Alapelveink

1. Minden tanárnak tanulnia kell. Amit ma tudunk, az nem biztos, hogy elég arra, hogy a holnap iskoláját működtessük. És az is biztos, hogy még sokkal hatékonyabban lehetne segíteni a gyerekek tanulását, mint amilyenek a ma ismert módszereink.

2. A tanároknak csapatban kell dolgozniuk, mert összetett (interdiszciplináris) tanulást csak vegyes összetételű (diverz) csapatok tudnak támogatni.

3. A szakképesítés nem szükséges és nem elégséges feltétele annak, hogy a Budapest Schoolban valaki jól teljesítő tanár legyen.

#### Felvétel

A Budapest School tanulásszervező tanárainak felvétele egy legalább
háromlépcsős folyamat, ahol vizsgálni kell a tanár egyéniségét
(attitűdjét), felnőtt—felnőtt kapcsolatokban a viselkedésmódját (társas
kompetenciáit), és minden jelöltnek próbafoglalkozást kell tartania,
amit az erre kijelölt Budapest School-tanárok megfigyelnek. A felvételi
folyamatot a fenntartó felügyeli és irányítja.

#### Saját cél

Minden tanárnak van saját, egyéni fejlődési célja: _mitől tudok én jobb
tanár lenni, jobban támogatni a gyerekek tanulását, segíteni a
munkatársaimat és partnerként dolgozni a szülőkkel?_

#### Tanárok mentora

A gyerekekhez hasonlóan, minden tanárnak van mentora, aki segíti a
saját céljainak kialakításában, és folyamatosan támogatja ezek elérésében.

#### Tanárok értékelése

Minden tanárt évente legalább kétszer értékelnek a munkatársai. Ez az a
folyamat, amit 360 fokos értékelésnek hívnak az üzleti szférában. A
visszajelzések feldolgozása után a saját célokat frissíteni kell.

Minden tanárt értékelnek a szülők is (kifejezetten a mentorált gyerekek
szülei) és a gyerekek is legalább évente kétszer.
