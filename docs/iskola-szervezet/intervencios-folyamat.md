# Az intervenciós folyamat

A Budapest School tanulóközösségének tagjai, a gyerekek, tanárok és a szülők, mind kölcsönhatásban vagyunk egymással, a tetteink hatnak a többiekre. Az, hogy egy gyerek hogyan viselkedik a csoportban, több tényezőtől függ: személyiségétől, képességeitől, aktuális pszichés állapotától, a közvetlen és tágabb környezete, a család, a csoport, a többi gyerek és a tanárok működésétől. És a gyerek viselkedése egy tükör számunkra arról, hogy aktuálisan mennyire van jól. Például amikor egy gyerek örömmel jár iskolába, vannak saját céljai, tud erőfeszítést tenni, tud kapcsolódni, a tanulásban fejlődik - akkor feltételezhetjük, hogy a gyerek jól van. Amikor egy gyerek rendszeresen késik az iskolából, és/vagy mintázatszerűen nem teljesíti a vállalásait, és/vagy nem halad a tanulásban, és/vagy bántja a társait vagy elszigetelődik a közösségben - akkor feltételezhetjük, hogy a gyerek nincs jól. Ezek olyan nehézségek, amikor a gyerek körül lévő felnőtteknek (a szülőknek és tanároknak) felelőssége jelzést tenni, vagyis riasztást küldeni az érintetteknek arról, hogy a gyerek nincs jól. A BPS-ben ezt nevezzük intervenciós jelzésnek.


## Amikor a tanárok jeleznek

A jelzés és az azt követő intervenciós folyamat célja, hogy a gyerek körül lévő felnőttek beszéljenek a gyerek állapotáról, közös értelmezést alakítsanak ki a megfigyelt viselkedés mögötti nehézségről és megállapodjanak abban, hogy milyen lépéseket tesznek a változás érdekében. 

A folyamat állomásai, amelyeket követünk:

1. A tanárcsapat beszél a gyerekről, és az iskolai nehézségek gyorsteszt segítségével döntenek arról, hogy jelzést tesznek. 
2. A gyerek mentora (szükség esetén az iskolapszichológus, gyógypedagógiai felelős vagy a tanárcsapat további tagjainak bevonása mellett) beszél a szülőkkel a gyerekről. Az megbeszélés(ek) eredménye, hogy a szülők és az iskola 1) egyetértenek abban, hogy a gyerek nincs jól, 2) kimondják, hogy szövetkeznek a nehézségek megoldására. 
3. A szülők és az iskola megállapdnak egy tervben, ebben világosan rögzítik a szülők és az iskola vállalásait (idősebb gyerek esetében akár a gyerekét is), a következő lépéseket, egyeztetéseket, időpontokat. 
4. Megvalósulnak a következő lépések. A közösen elfogadott terv része, hogy 4-6 hetente a folyamatban érintett felnőttek egyeztetnek és reflektálnak: érzékelnek-e változást, újra kell-e definiálni a problémát, szükséges-e módosítani a terven a fejlemények,  tanulságok tükrében?
5. A terv megvalósítása után a folyamatban érintett felnőttek retrospektív megbeszélést tartanak. A megbeszélés előtt a tanárcsapat újra kitölti a gyerekre az iskolai nehézségek gyorstesztet. Ennek tükrében kétféle döntés születhet:
a. A gyerek jobban van, az intervenciós folyamatot le lehet zárni. 
b. A gyerek állapota stagnál vagy rosszabbodik - ebben az esetben újra szükség van a probléma definiálására és új intervenciós folyamat indul (a fent részletezett lépéseknek megfelelően)

## Mi történik, ha a folyamat lépései nem teljesülnek?

A riasztáAz intervenciós folyamat az iskola és a szülők együttműködésére épül, és ez több ponton sérülhet:
- az iskola és a szülők nem értenek abban egyet, hogy a gyerek nincs jól 
- az iskola és a szülők mást gondolnak a nehézség természetéről, eltérően definiálják a problémát, ezeket nem tudják egymáshoz közelíteni 
- a szülők vagy a tanárok nem tudják/szeretnék a terv megvalósításának rájuk eső részét teljesíteni 
- az iskola vagy a szülők akadályozzák a folyamat haladását (meghiúsulnak a megbeszélések, elmarad a dokumentáció, reflexió, stb.)

Ezekben a helyzetekben a bizalom és partneri működés sérül és az intervenció nem tud megvalósulni, mert ahhoz a felek egyetértése, közös tenni akarása szükséges. Ilyen esetben rögzítjük, hogy megtörtént az iskola részéről a jelzés, az intervenció azonban megáll, és elindul egy másik folyamat: az iskola és a család közti konfliktuskezelés folyamata.
sról a tanárcsapat, a szülő vagy 12 év felett a gyerek dönthet. Tanárcsapat esetén mindenkinek hozzá kell járulnia a riasztáshoz. 

## Amikor a szülő ad jelzést

Előfordulhat, hogy elsőként a szülőben merül fel az a feltételezés, hogy a gyereke az iskolával összefüggésben nincs jól: egy ideje már nem jár örömmel iskolába, tartósan szorong valamilyen iskolai elvárás miatt, a szöveges visszajelzései alapján nem halad a tanulásban, nem kapcsolódik a közösséghez, ... Mivel az intervenciós folyamat elsődleges célja, hogy a felnőttek számára témává váljon a gyerek állapota és együtt változásokat indítsanak el azért, hogy jobban legyen, mindezt a szülő is kezdeményezheti. 

Ha szülőként azt tapasztalod, hogy a gyereked nincs jól, és az iskolával közös együttműködésre van szükség a változáshoz, akkor:
1. Intervenciós jelzés címmel küldj emailt a gyereked mentorának, és egyúttal írd le, hogy mik azok a tapasztalatok, amik miatt feltételezed, hogy a gyereked nincs jól. 
2. A mentornak a jelzésed továbbítania kell a tanárcsapatnak, és egyeztetésre hívja a szülő(ke)t, ill. az esetben érintett tanárokat. Az megbeszélés(ek) eredménye, hogy a szülők és az iskola 1) egyetértenek abban, hogy a gyerek nincs jól, 2) kimondják, hogy szövetkeznek a nehézségek megoldására. 
3. A szülők és az iskola megállapdnak egy tervben, ebben világosan rögzítik a szülők és az iskola vállalásait (idősebb gyerek esetében akár a gyerekét is), a következő lépéseket, egyeztetéseket, időpontokat. 
4. Megvalósulnak a következő lépések. A közösen elfogadott terv része, hogy 4-6 hetente a folyamatban érintett felnőttek egyeztetnek és reflektálnak: érzékelnek-e változást, újra kell-e definiálni a problémát, szükséges-e módosítani a terven a fejlemények,  tanulságok tükrében?
5. A terv megvalósítása után a folyamatban érintett felnőttek retrospektív megbeszélést tartanak. Ennek tükrében kétféle döntés születhet:
a. A gyerek jobban van, az intervenciós folyamatot le lehet zárni. 
b. A gyerek állapota stagnál vagy rosszabbodik - ebben az esetben újra szükség van a probléma definiálására és új intervenciós folyamat indul (a fent részletezett lépéseknek megfelelően)
