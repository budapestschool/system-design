# Tantárgyak tartalma, a tanulási eredmények
## Állampolgári ismeretek 
### 8. évfolyamon
*  Megérti a család, mint a társadalom alapvető intézményének szerepét, és értelmezi jellemzőit.

*  Lokálpatriotizmusa megerősödik, személyiségébe beépülnek a nemzeti közösséghez tartozás, a hazaszeretet emocionális összetevői.

*  Ismeri a demokratikus jogállam működésének alapvető sajátosságait, alapvető kötelezettségeit.

*  Jártasságot szerez mindennapi ügyeinek intézésében.

*  Saját pénzügyeiben tudatos döntéseket hoz.

*  Értelmezi a család mint a társadalom alapvető intézményének szerepét és jellemzőit.

*  Értelmezi a családi kohézió alapelemeit, jellemzőit: együttműködés, szeretetközösség, kölcsönösség, tisztelet.

*  Felismeri a családi szocializációnak az ember életútját befolyásoló jelentőségét.

*  Ismeri a magyar állam alapvető intézményeinek feladatkörét és működését.

*  Értelmezi a törvényalkotás folyamatát.

*  Ismeri a saját településének, lakóhelyének alapvető jellemzőit, értelmezi a településen működő intézmények és szervezetek szerepét és működését.

*  A lakóhelyével kapcsolatos javaslatokat fogalmaz meg, tervet készít a település fejlesztésének lehetőségeiről.

*  Felismeri a jogok és kötelességek közötti egyensúly kialakításának és fenntartásának fontosságát, megismeri a haza iránti kötelezettségeit, feladatait.

*  Ismeri településének, lakóhelyének kulturális, néprajzi értékeit, a település történetének alapvető eseményeit és fordulópontjait.

*  Megfogalmazza a nemzeti identitás jelentőségét az egyén és a közösség szempontjából is.

*  Felismeri a nemzetek, nemzetállamok fontosságát a globális világban.

*  Megismeri és értelmezi a honvédelem jelentőségét, feladatait és szerepét.

*  Azonosítja a mindennapi ügyintézés alapintézményeit, az alapvető ellátó rendszerek funkcióját és működési sajátosságait.

*  Azonosítja az igazságszolgáltatás intézményeit és működésük jellemzőit, megismeri az alapvető ellátórendszereket és funkcióikat.

*  Megismeri és értelmezi a diákmunka alapvető jogi feltételeit, kereteit.

*  Információkat gyűjt és értelmez a foglalkoztatási helyzetről, a szakmaszerkezet változásairól.

*  Ismeri a családi háztartás összetevőit, értelmezi a család gazdálkodását meghatározó és befolyásoló tényezőket.

*  Felismeri a családi háztartás gazdasági-pénzügyi fenntarthatóságának és a környezettudatos életvitel kialakításának társadalmi jelentőségét.

*  Értelmezi az állam gazdasági szerepvállalásának területeit.

*  Felismeri a közteherviselés gazdasági, társadalmi és erkölcsi jelentőségét, a társadalmi felelősségvállalás fontosságát.

*  Fogyasztási szokásaiban érvényesíti a tudatosság szempontjait is.

*  Felismeri a véleménynyilvánítás, érvelés, a párbeszéd és a vita társadalmi hasznosságát.

*  Képes arra, hogy feladatai egy részét a társas tanulás keretében végezze el.

*  Önállóan vagy társaival együttműködve javaslatokat fogalmaz meg, tervet, tervezetet készít.

*  Önállóan vagy segítséggel használja az infokommunikációs eszközöket.

### 12. évfolyamon
*  Megérti a család szerepét, alapvető feladatait az egyén és a nemzet szempontjából egyaránt.

*  Értékeli a nemzeti identitás jelentőségét az egyén és a közösség szempontjából is.

*  Ismeri a választások alapelveit és a törvényhozás folyamatát.

*  Megismeri a demokratikus jogállam működésének alapvető sajátosságait.

*  Érti és vallja a haza védelmének, a nemzetért történő tenni akarás fontosságát.

*  A mindennapi életének megszervezésében alkalmazza a jogi és gazdasági-pénzügyi ismereteit.

*  Saját pénzügyeiben tudatos döntéseket hoz.

*  Felismeri az életpálya-tervezés és a munkavállalás egyéni és társadalmi jelentőségét.

*  Ismeri a munka világát érintő alapvető jogi szabályozást, a munkaerőpiac jellemzőit, tájékozódik a foglalkoztatás és a szakmaszerkezet változásairól.

*  Értelmezi a család mint a társadalom alapvető intézményének szerepét és jellemzőit.

*  Társaival megbeszéli a párválasztás, a családtervezés fontos szakaszait, szempontjait és a gyermekvállalás demográfiai jelentőségét: tájékozódás, minták, orientáló példák, átgondolt tervezés, felelősség.

*  Felismeri, hogy a családtagok milyen szerepet töltenek be a szocializáció folyamatában.

*  Értelmezi a családi szocializációnak az ember életútját befolyásoló jelentőségét.

*  Felismeri az alapvető emberi jogok egyetemes és társadalmi jelentőségét.

*  Bemutatja magyarország alaptörvényének legfontosabb részeit: alapvetés; az állam; szabadság és felelősség.

*  Érti a társadalmi normák és az egyéni cselekedetek, akaratok, célok egyeztetésének, összehangolásának követelményét. elméleti és tapasztalati úton ismereteket szerez a társadalmi felelősségvállalásról, a segítségre szorulók támogatásának lehetőségeiről.

*  Megérti a honvédelem szerepét az ország biztonságának fenntartásában, megismeri a haza védelmének legfontosabb feladatcsoportjait és területeit, az egyén kötelezettségeit.

*  Felismeri és értelmezi az igazságosság, az esélyegyenlőség biztosításának jelentőségét és követelményeit.

*  Értelmezi a választójog feltételeit és a választások alapelveit.

*  Értelmezi a törvényalkotás folyamatát.

*  Megérti a nemzeti érzület sajátosságait és a hazafiság fontosságát, lehetséges megnyilvánulási formáit.

*  Véleményt alkot a nemzetállamok és a globalizáció összefüggéseiről.

*  Felismeri a világ magyarsága mint nemzeti közösség összetartozásának jelentőségét.

*  Érti és felismeri a honvédelem mint nemzeti ügy jelentőségét.

*  Felismeri és értékeli a helyi, regionális és országos közgyűjtemények nemzeti kulturális örökség megőrzésében betöltött szerepét.

*  Azonosítja a mindennapi ügyintézés alapintézményeit.

*  Életkori sajátosságainak megfelelően jártasságot szerez a jog területének mindennapi életben való alkalmazásában.

*  Tájékozott a munkavállalással kapcsolatos szabályokban.

*  Megtervezi egy fiktív család költségvetését.

*  Saját pénzügyi döntéseit körültekintően, megalapozottan hozza meg.

*  Megismeri a megalapozott, körültekintő hitelfelvétel szempontjait, illetve feltételeit.

*  Azonosítja az állam gazdasági szerepvállalásának elemeit.

*  Felismeri és megérti a közteherviselés nemzetgazdasági, társadalmi és morális jelentőségét.

*  Életvitelébe beépülnek a tudatos fogyasztás elemei, életmódjában figyelmet fordít a környezeti terhelés csökkentésére, érvényesíti a fogyasztóvédelmi szempontokat.

*  Értelmezi a vállalkozás indítását befolyásoló tényezőket.

*  Felismeri a véleménynyilvánítás, érvelés, a párbeszéd és a vita társadalmi hasznosságát.

*  Képes arra, hogy feladatait akár önálló, akár társas tanulás révén végezze el, célorientáltan képes az együttműködésre.

*  Önállóan vagy társaival együttműködve javaslatokat fogalmaz meg.

*  Tiszteletben tartja a másik ember értékvilágát, gondolatait és véleményét, ha szükséges, kritikusan viszonyul emberi cselekedetekhez, magatartásformákhoz.

*  Megismerkedik a tudatos médiafogyasztói magatartással és a közösségi média használatával.

*  A tanulási tevékenységek szakaszaiban használja az infokommunikációs eszközöket, lehetőségeket, tisztában van azok szerepével, innovációs potenciáljával és veszélyeivel is.

## Biológia 
### 9-10. évfolyamon
*  A mindennapi élettel összefüggő problémák megoldásában alkalmazza a természettudományos gondolkodás műveleteit, rendelkezik a biológiai problémák vizsgálatához szükséges gyakorlati készségekkel.

*  Az élő rendszerek belső működése és környezettel való kapcsolataik elemzésében alkalmazza a rendszerszintű gondolkodás műveleteit.

*  Életközösségek vizsgálata alapján értelmezi a környezet és az élőlények felépítése és működése közötti összefüggést, érti az ökológiai egyensúly jelentőségét, érvel a biológiai sokféleség megőrzése mellett.

*  Az emberi test és pszichikum felépítéséről és működéséről szerzett ismereteit önismeretének fejlesztésében, egészséges életvitelének kialakításában alkalmazza.

*  Felismeri a helyi és a globális környezeti problémák összefüggését, érvel a föld és a kárpát-medence természeti értékeinek védelme mellett, döntéseket hoz és cselekszik a fenntarthatóság érdekében.

*  Ismeri a biológiai kutatások alapvető céljait, legfontosabb területeit, értékeli az élet megértésében, az élővilág megismerésében és megóvásában játszott szerepét.

*  Példákkal igazolja a biológiai ismereteknek a világképünk és a technológia fejlődésében betöltött szerepét, gazdasági és társadalmi jelentőségét.

*  Az élő rendszerek vizsgálata során felismeri az analógiákat, korrelációkat, alkalmazza a statisztikus és a rendszerszintű gondolkodás műveleteit, kritikusan és kreatívan mérlegeli a lehetőségeket, bizonyítékokra alapozva érvel, több szempontot is figyelembe vesz.

*  A vizsgált biológiai jelenségek magyarázatára előfeltevést fogalmaz meg, ennek bizonyítására vagy cáfolatára kísérletet tervez és kivitelez, azonosítja és beállítja a kísérleti változókat, megfigyeléseket és méréseket végez.

*  Biológiai vonatkozású adatokat elemez, megfelelő formába rendez, ábrázol, ezek alapján előrejelzéseket, következtetéseket fogalmaz meg, a már ábrázolt adatokat értelmezi.

*  Egyénileg és másokkal együttműködve célszerűen és biztonságosan alkalmaz biológiai vizsgálati módszereket, ismeri a fénymikroszkóp működésének alapelvét, képes azt használni.

*  Érti a biológia molekuláris szintű vizsgálati módszereinek elméleti alapjait és felhasználási lehetőségeit, értékeli a biológiai kutatásokból származó nagy mennyiségű adat feldolgozásának jelentőségét.

*  A biológiai jelenségek vizsgálata során digitális szöveget, képet, videót keres, értelmez és felhasznál, vizsgálja azok megbízhatóságát, jogszerű és etikus felhasználhatóságát.

*  Biológiai vizsgálatok során elvégzi az adatrögzítés és -rendezés műveleteit, ennek alapján tényekkel alátámasztott következtetéseket von le.

*  Ismeri a tudományos közlések lényegi jellemzőit.

*  Tájékozódik a biotechnológia és a bioetika kérdéseiben, ezekről folyó vitákban tudományosan megalapozott érveket alkot.

*  A valós és virtuális tanulási közösségekben, másokkal együttműködve megtervez és kivitelez biológiai vizsgálatokat, projekteket.

*  Tudja a biológiai problémákat és magyarázatokat a megfelelő szinttel összefüggésben értelmezni.

*  Tényekkel bizonyítja az élőlények elemi összetételének hasonlóságát, a biogén elemek, a víz, az atp és a makromolekulák élő szervezetekben betöltött alapvető szerepét.

*  Megérti, miért és hogyan mehetnek végbe viszonylag alacsony hőmérsékleten, nagy sebességgel kémiai reakciók a sejtekben, vizsgálja az enzimműködést befolyásoló tényezőket.

*  Értékeli és példákkal igazolja a különféle szintű biológiai szabályozás szerepét az élő rendszerek normál működési állapotának fenntartásában.

*  Magyarázza, hogy a sejt az élő szervezetek szerkezeti és működési egysége.

*  Ábrák, animációk alapján értelmezi, és biológiai tényekkel alátámasztja, hogy a vírusok az élő és élettelen határán állnak.

*  A felépítés és működés összehasonlítása alapján bemutatja a sejtes szerveződés kétféle formájának közös jellemzőit és alapvető különbségeit, értékeli ezek jelentőségét.

*  Tényekkel igazolja a baktériumok anyagcsere sokfélesége, gyors szaporodása és alkalmazkodóképessége közötti összefüggést.

*  Felismeri az összetett sejttípus mikroszkóppal megfigyelhető sejtalkotóit, magyarázza a sejt anyagcsere-folyamatainak lényegét.

*  Ismeri az örökítőanyag többszintű szerveződését, képek, animációk alapján értelmezi a sejtekben zajló biológiai információ tárolásának, átírásának és kifejeződésének folyamatait.

*  Tudja, hogy a sejtekben és a sejtek között bonyolult jelforgalmi hálózatok működnek, amelyek befolyásolják a génműködést, és felelősek lehetnek a normál és a kóros működésért is.

*  Összehasonlítja a sejtosztódás típusait, megfogalmazza ezek biológiai szerepét, megérti, hogy a soksejtű szervezetek a megtermékenyített petesejt és utódsejtjei meghatározott számú osztódásával és differenciálódásával alakulnak ki.

*  Felismeri az összefüggést a rák kialakulása és a sejtciklus zavarai között, megérti, hogy mit tesz a sejt és a szervezet a daganatok kialakulásának megelőzéséért.

*  Ismeri az őssejt fogalmát, különféle típusait és azok jellemzőit, különbséget tesz őssejt és daganatsejt között.

*  Fénymikroszkópban, ábrán vagy fotón felismeri és jellemzi a főbb állati és növényi szövettípusokat, elemzi, hogy milyen funkciók hatékony elvégzésére specializálódtak.

*  Ismeri és példákkal bizonyítja az élőlények szén- és energiaforrásainak különféle lehetőségeit, az anyagcseretípusok közötti különbséget.

*  Vázlatrajzok, folyamatábrák és animációk alapján azonosítja a fotoszintézis és a sejtlégzés fő szakaszainak sejten belüli helyét és struktúráit, a fontosabb anyagokat és az energiaátalakítás jellemzőit.

*  A sejtszintű anyagcsere folyamatok alapján magyarázza a növények és állatok közötti ökológiai szintű kapcsolatot, a termelő és fogyasztó szervezetek közötti anyagforgalmat.

*  A földi élet keletkezését biológiai kísérletek és elméletek alapján magyarázza.

*  Érti és tényekkel igazolja az ősbaktériumok különleges élőhelyeken való életképességét.

*  Biológiai és csillagászati tények alapján mérlegeli a földön kívüli élet valószínűsíthető feltételeit és lehetőségeit.

*  Ismeri az örökítőanyag bázissorrendjének vagy bázisainak megváltozásához vezető folyamatokat, konkrét esetekben azonosítja ezek következményeit.

*  A géntechnológia céljának és módszertani alapjainak ismeretében, kritikai szemlélettel elemzi a genetikai módosítások előnyeit és kockázatait.

*  Érti az örökítőanyagban tárolt információ és a kifejeződő tulajdonságok közötti összefüggést, megkülönbözteti a genotípust és a fenotípust.

*  Megérti a genetikai információ nemzedékek közötti átadásának törvényszerűségeit, ezeket konkrét esetek elemzésében alkalmazza.

*  Felismeri a kapcsolatot az életmód és a gének kifejeződése között, érti, hogy a sejt és az egész szervezet jellemzőinek kialakításában és fenntartásában kiemelt szerepe van a környezet általi génaktivitás-változásoknak.

*  Megérti a természetes változatosság szerveződését, az evolúciós változások eredetét és elterjedését magyarázó elemi folyamatokat, felismer és magyaráz mikro- és makroszintű evolúciós jelenségeket.

*  Példákkal igazolja, hogy a szelekció a különböző szerveződési szinteken értelmezhető tulajdonságokon keresztül egyidejűleg hat.

*  Példákkal mutatja be az élővilág főbb csoportjainak evolúciós újításait, magyarázza, hogy ezek hogyan segítették elő az adott élőlénycsoport elterjedését.

*  Érti és elfogadja, hogy a mai emberek egy fajhoz tartoznak, és az evolúció során kialakult nagyrasszok értékükben nem különböznek, a biológiai és kulturális örökségük az emberiség közös kincse.

*  Morfológiai, molekuláris biológiai adatok alapján egyszerű származástani kapcsolatokat elemez, törzsfát készít.

*  Ismeri az evolúció befolyásolásának lehetséges módjait (például mesterséges szelekció, fajtanemesítés, géntechnológia), értékeli ezek előnyeit és esetleges hátrányait.

*  Megérti a környezeti állapot és az ember egészsége közötti összefüggéseket, azonosítja az ember egészségét veszélyeztető tényezőket, felismeri a megelőzés lehetőségeit, érvényesíti az elővigyázatosság elvét.

*  Elemzi az ember mozgásképességének biokémiai, szövettani és biomechanikai alapjait, ezeket összefüggésbe hozza a mindennapi élet, a sport és a munka mozgásformáival, értékeli a rendszeres testmozgás szerepét egészségének megőrzésében.

*  Az emberi test kültakarójának, váz- és izomrendszerének elemzése alapján magyarázza az ember testképének, testalkatának és mozgásképességének biológiai alapjait.

*  A táplálkozás-, a légzés-, a keringés- és a kiválasztás szervrendszerének elemzése alapján magyarázza az emberi szervezet anyag- és energiaforgalmi működésének biológiai alapjait.

*  Az ideg-, hormon- és immunrendszer elemzése alapján magyarázza az emberi szervezet információs rendszerének biológiai alapjait.

*  Felsorolja az emberi egyedfejlődés főbb szakaszait, magyarázza hogyan és miért változik a szervezetünk az életkor előrehaladásával, értékeli a fejlődési szakaszok egészségvédelmi szempontjait, önmagát is elhelyezve ebben a rendszerben.

*  Ismeri a férfi és a női nemi szervek felépítését és működését, a másodlagos nemi jellegeket és azok kialakulási folyamatát, ismereteit összekapcsolja a szaporító szervrendszer egészségtanával.

*  Biológiai ismereteit is figyelembe véve értékeli az emberi szexualitás párkapcsolattal és a tudatos családtervezéssel összefüggő jelentőségét.

*  Megérti a fogamzásgátlók hatékonyságáról szóló információkat, a személyre szabott, orvosilag ellenőrzött fogamzásgátlás fontosságát.

*  Ismeri a fogamzás feltételeit, a terhesség jeleit, bemutatja a magzat fejlődésének szakaszait, értékeli a terhesség alatti egészséges életmód jelentőségét.

*  A biológiai működések alapján magyarázza a stressz fogalmát, felismeri a tartós stressz egészségre gyakorolt káros hatásait, igyekszik azt elkerülni, csökkenteni.

*  Ismeri a gondolkodási folyamatokat és az érzelmi és motivációs működéseket meghatározó tényezőket, értékeli az érzelmi és az értelmi fejlődés kapcsolatát.

*  Ismeri a mentális egészség jellemzőit, megérti annak feltételeit, ezek alapján megtervezi az egészségmegőrző magatartásához szükséges életviteli elemeket.

*  Megérti az idegsejtek közötti jelátviteli folyamatokat, és kapcsolatba hozza azokat a tanulás és emlékezés folyamataival, a drogok hatásmechanizmusával.

*  Az agy felépítése és funkciója alapján magyarázza az információk feldolgozásával, a tanulással összefüggő folyamatokat, értékeli a tanulási képesség jelentőségét az egyén és a közösség szempontjából.

*  Biológiai folyamatok alapján magyarázza a függőség kialakulását, felismeri a függőségekre vezető tényezőket, ezek kockázatait és következményeit.

*  Ismeri az orvosi diagnosztika, a szűrővizsgálatok és védőoltások célját, lényegét, értékeli ezek szerepét a betegségek megelőzésében és a gyógyulásban.

*  Megkülönbözteti a házi- és a szakorvosi ellátás funkcióit, ismeri az orvoshoz fordulás módját, tisztában van a kórházi ellátás indokaival, jellemzőivel.

*  Ismeri a leggyakoribb fertőző betegségek kiváltó okait, ismeri a fertőzések elkerülésének lehetőségeit és a járványok elleni védekezés módjait.

*  Ismeri a leggyakoribb népbetegségek (pl. szívinfarktus, stroke, cukorbetegség, allergia, asztma) kockázati tényezőit, felismeri ezek kezdeti tüneteit.

*  Képes a bekövetkezett balesetet, rosszullétet felismerni,

*  Képes a sérült vagy beteg személy ellátását a rendelkezésre álló eszközökkel (vagy eszköz nélkül) megkezdeni, segítséget (szükség esetén mentőt) hívni.

*  Szükség esetén alkalmazza a felnőtt alapszintű újraélesztés műveleteit (cpr), képes a félautomata defibrillátort alkalmazni.

*  Példákkal mutatja be a fontosabb hazai szárazföldi és vizes életközösségek típusait, azok jellemzőit és előfordulásait.

*  Megfigyelések, leírások és videók alapján azonosítja a populációk közötti kölcsönhatások típusait, az ezzel összefüggő etológiai jellemzőket, bemutatja ezek jellegét, jelentőségét.

*  Érti az ökológiai mutatókkal, bioindikációs vizsgálatokkal megvalósuló környezeti állapotelemzések céljait, adott esetben alkalmazza azok módszereit.

*  Ismeri a levegő-, a víz- és a talajszennyezés forrásait, a szennyező anyagok típusait és példáit, konkrét esetek alapján elemzi az életközösségekre gyakorolt hatásukat.

*  Felismeri és példákkal igazolja az állatok viselkedésének a környezethez való alkalmazkodásban játszott szerepét.

*  Felismeri a természetes élőhelyeket veszélyeztető tényezőket, kifejti álláspontját az élőhelyvédelem szükségességéről, egyéni és társadalmi megvalósításának lehetőségeiről.

*  Érti a biológiai sokféleség fogalmát, ismer a meghatározásra alkalmas módszereket, értékeli a bioszféra stabilitásának megőrzésében játszott szerepét.

*  Érti az ökológiai egyensúly fogalmát, értékeli a jelentőségét, példákkal igazolja az egyensúly felborulásának lehetséges következményeit.

*  Érti az ökológiai rendszerek működése és a biológiai sokféleség közötti kapcsolatot, konkrét életközösségek vizsgálata alapján táplálkozási piramist, hálózatot elemez.

*  Konkrét példák alapján vizsgálja a bioszférában végbemenő folyamatokat, elemzi ezek idő- és térbeli viszonyait, azonosítja az emberi tevékenységgel való összefüggésüket.

*  A kutatások adatai és előrejelzései alapján értelmezi a globális éghajlatváltozás élővilágra gyakorolt helyi és bioszféra szintű következményeit.

*  Példák alapján elemzi a levegő-, a víz- és a talajszennyeződés, az ipari és természeti katasztrófák okait és ezek következményeit, az emberi tevékenységnek az élőhelyek változásához vezető hatását, ennek alapján magyarázza egyes fajok veszélyeztetettségét.

*  Érti és elfogadja, hogy a jövőbeli folyamatokat a jelen cselekvései alakítják, tudja, hogy a folyamatok tervezése, előrejelzése számítógépes modellek alapján lehetséges.

*  Értékeli a környezet- és természetvédelem fontosságát, megérti a nemzetközi összefogások és a hazai törekvések jelentőségét, döntéshozatalai során saját személyes érdekein túl a természeti értékeket és egészség-megőrzési szempontokat is mérlegeli.

*  Történeti adatok és jelenkori esettanulmányok alapján értékeli a mezőgazdaság, erdő- és vadgazdaság, valamint a halászat természetes életközösségekre gyakorolt hatását, példák alapján bemutatja az ökológiai szempontú, fenntartható gazdálkodás technológiai lehetőségeit.

*  Megérti a biotechnológiai eljárások és a bionika eredményeinek alkalmazási lehetőségeit, értékeli az információs technológiák alkalmazásának orvosi, biológiai jelentőségét.

*  Érvel a föld, mint élő bolygó egyedisége mellett, tényekre alapozottan és kritikusan értékeli a természeti okokból és az emberi hatásokra bekövetkező változásokat.

*  Ismeri a kárpát-medence élővilágának sajátosságait, megőrzendő értékeit, ezeket összekapcsolja a hazai nemzeti parkok tevékenységével.

## Digitális kultúra 
### 4. évfolyamon
*  Elmélyülten dolgozik digitális környezetben, önellenőrzést végez.

*  Megvizsgálja és értékeli az általa vagy társai által alkalmazott, létrehozott, megvalósított eljárásokat.

*  Társaival együttműködve online és offline környezetben egyaránt megold különböző feladatokat, ötleteit, véleményét megfogalmazza, részt vesz a közös álláspont kialakításában.

*  Kiválasztja az általa ismert informatikai eszközök és alkalmazások közül azokat, melyek az adott probléma megoldásához szükségesek.

*  Eredményétől függően módosítja a problémamegoldás folyamatában az adott, egyszerű tevékenységsorokat.

*  A rendelkezésére álló eszközökkel, forrásokból meggyőződik a talált vagy kapott információk helyességéről.

*  Közvetlen otthoni vagy iskolai környezetéből megnevez néhány informatikai eszközt, felsorolja fontosabb jellemzőit.

*  Megfogalmazza, néhány példával alátámasztja, hogyan könnyíti meg a felhasználó munkáját az adott eszköz alkalmazása.

*  Egyszerű feladatokat old meg informatikai eszközökkel. esetenként tanítói segítséggel összetett funkciókat is alkalmaz.

*  Önállóan vagy tanítói segítséggel választ más tantárgyak tanulásának támogatásához applikációkat, digitális tananyagot, oktatójátékot, képességfejlesztő digitális alkalmazást.

*  Kezdetben tanítói segítséggel, majd önállóan használ néhány, életkorának megfelelő alkalmazást, elsősorban információgyűjtés, gyakorlás, egyéni érdeklődésének kielégítése céljából.

*  A feladathoz, problémához digitális eszközt, illetve alkalmazást, applikációt, felhasználói felületet választ; felsorol néhány érvet választásával kapcsolatosan.

*  Adott szempontok alapján megfigyel néhány, grafikai alkalmazással készített produktumot; személyes véleményét megfogalmazza.

*  Grafikai alkalmazással egyszerű, közvetlenül hasznosuló rajzot, grafikát, dokumentumot hoz létre.

*  Egy rajzos dokumentumot adott szempontok alapján értékel, módosít.

*  Állításokat fogalmaz meg grafikonokról, infografikákról, táblázatokról; a kapott információkat felhasználja napi tevékenysége során.

*  Információkat keres, a talált adatokat felhasználja digitális produktumok létrehozására.

*  Értelmezi a problémát, a megoldási lehetőségeket eljátssza, megfogalmazza, egyszerű eszközök segítségével megvalósítja.

*  Információt keres az interneten más tantárgyak tanulása során, és felhasználja azt.

*  Egyszerű prezentációt, ábrát, egyéb segédletet készít.

*  Felismer, eljátszik, végrehajt néhány hétköznapi tevékenysége során tapasztalt, elemi lépésekből álló, adott sorrendben végrehajtandó cselekvést.

*  Egy adott, mindennapi életből vett algoritmust elemi lépésekre bont, értelmezi a lépések sorrendjét, megfogalmazza az algoritmus várható kimenetelét.

*  Feladat, probléma megoldásához többféle algoritmust próbál ki.

*  A valódi vagy szimulált programozható eszköz mozgását értékeli, hiba esetén módosítja a kódsorozatot a kívánt eredmény eléréséig. tapasztalatait megfogalmazza, megvitatja társaival.

*  Adott feltételeknek megfelelő kódsorozatot tervez és hajtat végre, történeteket, meserészleteket jelenít meg padlórobottal vagy más eszközzel.

*  Alkalmaz néhány megadott algoritmust tevékenység, játék során, és néhány egyszerű esetben módosítja azokat.

*  Információkat keres az interneten, egyszerű eljárásokkal meggyőződik néhány, az interneten talált információ igazságértékéről.

*  Kiválasztja a számára releváns információt, felismeri a hamis információt.

*  Tisztában van a személyes adat fogalmával, törekszik megőrzésére, ismer néhány példát az e-világ veszélyeivel kapcsolatban.

*  Ismeri és használja a kapcsolattartás formáit és a kommunikáció lehetőségeit a digitális környezetben.

*  Ismeri a mobileszközök alkalmazásának előnyeit, korlátait, etikai vonatkozásait.

*  Közvetlen tapasztalatokat szerez a digitális eszközök használatával kapcsolatban.

*  Képes feladat, probléma megoldásához megfelelő applikáció, digitális tananyag, oktatójáték, képességfejlesztő digitális alkalmazás kiválasztására.

*  Ismer néhány, kisiskolások részére készített portált, információforrást, digitálistananyag-lelőhelyet.

### 5-8. évfolyamon
*  Önállóan használja a digitális eszközöket, az online kommunikáció eszközeit, tisztában van az ezzel járó veszélyekkel.

*  Elsajátítja a digitális írástudás eszközeit, azokkal feladatokat old meg.

*  Megismeri a felmerülő problémák megoldásának módjait, beleértve az adott feladat megoldásához szükséges algoritmus értelmezését, alkotását és számítógépes program készítését és kódolását a blokkprogramozás eszközeivel.

*  Digitális tudáselemek felhasználásával, társaival együttműködve különböző problémákat old meg.

*  Megismeri a digitális társadalom elvárásait, lehetőségeit és veszélyeit.

*  Célszerűen választ a feladat megoldásához használható informatikai eszközök közül.

*  Az informatikai eszközöket önállóan használja, a tipikus felhasználói hibákat elkerüli, és elhárítja az egyszerűbb felhasználói szintű hibákat.

*  Értelmezi az informatikai eszközöket működtető szoftverek hibajelzéseit, és azokról beszámol.

*  Önállóan használja az operációs rendszer felhasználói felületét.

*  Önállóan kezeli az operációs rendszer mappáit, fájljait és a felhőszolgáltatásokat.

*  Használja a digitális hálózatok alapszolgáltatásait.

*  Tapasztalatokkal rendelkezik a digitális jelek minőségével, kódolásával, tömörítésével, továbbításával kapcsolatos problémák kezeléséről.

*  Egy adott feladat kapcsán önállóan hoz létre szöveges vagy multimédiás dokumentumokat.

*  Ismeri és tudatosan alkalmazza a szöveges és multimédiás dokumentum készítése során a szöveg formázására, tipográfiájára vonatkozó alapelveket.

*  A tartalomnak megfelelően alakítja ki a szöveges vagy a multimédiás dokumentum szerkezetét, illeszti be, helyezi el és formázza meg a szükséges objektumokat.

*  Ismeri és kritikusan használja a nyelvi eszközöket (például helyesírás-ellenőrzés, elválasztás).

*  A szöveges dokumentumokat többféle elrendezésben jeleníti meg papíron, tisztában van a nyomtatás környezetre gyakorolt hatásaival.

*  Ismeri a prezentációkészítés alapszabályait, és azokat alkalmazza.

*  Etikus módon használja fel az információforrásokat, tisztában van a hivatkozás szabályaival.

*  Digitális eszközökkel önállóan rögzít és tárol képet, hangot és videót.

*  Digitális képeken képkorrekciót hajt végre.

*  Ismeri egy bittérképes rajzolóprogram használatát, azzal ábrát készít.

*  Bemutató-készítő vagy szövegszerkesztő programban rajzeszközökkel ábrát készít.

*  Érti, hogyan történik az egyszerű algoritmusok végrehajtása a digitális eszközökön.

*  Megkülönbözteti, kezeli és használja az elemi adatokat.

*  Értelmezi az algoritmus végrehajtásához szükséges adatok és az eredmények kapcsolatát.

*  Egyszerű algoritmusokat elemez és készít.

*  Ismeri a kódolás eszközeit.

*  Adatokat kezel a programozás eszközeivel.

*  Ismeri és használja a programozási környezet alapvető eszközeit.

*  Ismeri és használja a blokkprogramozás alapvető építőelemeit.

*  A probléma megoldásához vezérlési szerkezetet (szekvencia, elágazás és ciklus) alkalmaz a tanult blokkprogramozási nyelven.

*  Az adatokat táblázatos formába rendezi és formázza.

*  Cellahivatkozásokat, matematikai tudásának megfelelő képleteket, egyszerű statisztikai függvényeket használ táblázatkezelő programban.

*  Az adatok szemléltetéséhez diagramot készít.

*  Problémákat old meg táblázatkezelő program segítségével.

*  Tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes szimulációjáról.

*  Vizsgálni tudja a szabályozó eszközök hatásait a tantárgyi alkalmazásokban.

*  Ismeri az információkeresés technikáját, stratégiáját és több keresési szempont egyidejű érvényesítésének lehetőségét.

*  Önállóan keres információt, a találatokat hatékonyan szűri.

*  Az internetes adatbázis-kezelő rendszerek keresési űrlapját helyesen tölti ki.

*  Ismeri, használja az elektronikus kommunikáció lehetőségeit, a családi és az iskolai környezetének elektronikus szolgáltatásait.

*  Ismeri és betartja az elektronikus kommunikációs szabályokat.

*  Mozgásokat vezérel szimulált vagy valós környezetben.

*  Adatokat gyűjt szenzorok segítségével.

*  Tapasztalatokkal rendelkezik az eseményvezérlésről.

*  Ismeri a térinformatika és a 3d megjelenítés lehetőségeit.

*  Tapasztalatokkal rendelkezik az iskolai oktatáshoz kapcsolódó mobileszközökre fejlesztett alkalmazások használatában.

*  Tisztában van a hálózatokat és a személyes információkat érintő fenyegetésekkel, alkalmazza az adatok védelmét biztosító lehetőségeket.

*  Védekezik az internetes zaklatás különböző formái ellen, szükség esetén segítséget kér.

*  Ismeri a digitális környezet, az e-világ etikai problémáit.

*  Ismeri az információs technológia fejlődésének gazdasági, környezeti, kulturális hatásait.

*  Ismeri az információs társadalom múltját, jelenét és várható jövőjét.

*  Online gyakorolja az állampolgári jogokat és kötelességeket.

### 9-12. évfolyamon
*  Ismeri az informatikai eszközök és a működtető szoftvereik célszerű választásának alapelveit, használja a digitális hálózatok alapszolgáltatásait, az online kommunikáció eszközeit, tisztában van az ezzel járó veszélyekkel, ezzel összefüggésben ismeri a segítségnyújtási, segítségkérési lehetőségeket.

*  Gyakorlatot szerez dokumentumok létrehozását segítő eszközök használatában.

*  Megismeri az adatkezelés alapfogalmait, képes a nagyobb adatmennyiség tárolását, hatékony feldolgozását biztosító eszközök és módszerek alapszintű használatára, érti a működésüket.

*  Megismeri az algoritmikus probléma megoldásához szükséges módszereket és eszközöket, megoldásukhoz egy magas szintű formális programozási nyelv fejlesztői környezetét önállóan használja.

*  Hatékonyan keres információt; az ikt-tudáselemek felhasználásával társaival együttműködve problémákat old meg.

*  Ismeri az e-világ elvárásait, lehetőségeit és veszélyeit.

*  Ismeri és tudja használni a célszerűen választott informatikai eszközöket és a működtető szoftvereit, ismeri a felhasználási lehetőségeket.

*  Ismeri a digitális eszközök és a számítógépek fő egységeit, ezek fejlődésének főbb állomásait, tendenciáit.

*  Tudatosan alakítja informatikai környezetét, ismeri az ergonomikus informatikai környezet jellemzőit, figyelembe veszi a digitális eszközök egészségkárosító hatásait, óvja maga és környezete egészségét.

*  Önállóan használja az informatikai eszközöket, elkerüli a tipikus felhasználói hibákat, elhárítja az egyszerűbb felhasználói hibákat.

*  Céljainak megfelelően használja a mobileszközök és a számítógépek operációs rendszereit.

*  Igénybe veszi az operációs rendszer és a számítógépes hálózat alapszolgáltatásait.

*  Követi a technológiai változásokat a digitális információforrások használatával.

*  Használja az operációs rendszer segédprogramjait, és elvégzi a munkakörnyezet beállításait.

*  Tisztában van a digitális kártevők elleni védekezés lehetőségeivel.

*  Használja az állományok tömörítését és a tömörített állományok kibontását.

*  Ismeri egy adott feladat megoldásához szükséges digitális eszközök és szoftverek kiválasztásának szempontjait.

*  Speciális dokumentumokat hoz létre, alakít át és formáz meg.

*  Tapasztalatokkal rendelkezik a formanyomtatványok, a sablonok, az előre definiált stílusok használatáról.

*  Gyakorlatot szerez a fotó-, hang-, videó-, multimédia-szerkesztő, a bemutató-készítő eszközök használatában.

*  Alkalmazza az információkeresés során gyűjtött multimédiás alapelemeket új dokumentumok készítéséhez.

*  Dokumentumokat szerkeszt és helyez el tartalomkezelő rendszerben.

*  Ismeri a html formátumú dokumentumok szerkezeti elemeit, érti a css használatának alapelveit; több lapból álló webhelyet készít.

*  Létrehozza az adott probléma megoldásához szükséges rasztergrafikus ábrákat.

*  Létrehoz vektorgrafikus ábrákat.

*  Digitálisan rögzít képet, hangot és videót, azokat manipulálja.

*  Tisztában van a raszter-, a vektorgrafikus ábrák tárolási és szerkesztési módszereivel

*  Érti az egyszerű problémák megoldásához szükséges tevékenységek lépéseit és kapcsolatukat.

*  Ismeri a következő elemi adattípusok közötti különbségeket: egész, valós szám, karakter, szöveg, logikai.

*  Ismeri az elemi és összetett adattípusok közötti különbségeket.

*  Érti egy algoritmus-leíró eszköz alapvető építőelemeit, érti a típusalgoritmusok felhasználásának lehetőségeit.

*  Példákban, feladatok megoldásában használja egy formális programozási nyelv fejlesztői környezetének alapszolgáltatásait.

*  Szekvencia, elágazás és ciklus segítségével algoritmust hoz létre, és azt egy magas szintű formális programozási nyelven kódolja.

*  A feladat megoldásának helyességét teszteli.

*  Adatokat táblázatba rendez.

*  Táblázatkezelővel adatelemzést és számításokat végez.

*  A problémamegoldás során függvényeket célszerűen használ.

*  Nagy adathalmazokat tud kezelni.

*  Az adatokat diagramon szemlélteti.

*  Ismeri az adatbázis-kezelés alapfogalmait.

*  Az adatbázisban interaktív módon keres, rendez és szűr.

*  A feladatmegoldás során az adatbázisba adatokat visz be, módosít és töröl, űrlapokat használ, jelentéseket nyomtat.

*  Strukturáltan tárolt nagy adathalmazokat kezel, azokból egyedi és összesített adatokat nyer ki.

*  Tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes szimulációjáról.

*  Hétköznapi, oktatáshoz készült szimulációs programokat használ.

*  Tapasztalatokat szerez a kezdőértékek változtatásának hatásairól a szimulációs programokban.

*  Ismeri és alkalmazza az információkeresési stratégiákat és technikákat, a találati listát a problémának megfelelően szűri, ellenőrzi annak hitelességét.

*  Etikus módon használja fel az információforrásokat, tisztában van a hivatkozás szabályaival.

*  Használja a két- vagy többrésztvevős kommunikációs lehetőségeket és alkalmazásokat.

*  Ismeri és alkalmazza a fogyatékkal élők közötti kommunikáció eszközeit és formáit.

*  Az online kommunikáció során alkalmazza a kialakult viselkedési kultúrát és szokásokat, a szerepelvárásokat.

*  Ismeri és használja a mobiltechnológiát, kezeli a mobileszközök operációs rendszereit és használ mobilalkalmazásokat.

*  Céljainak megfelelő alkalmazást választ, az alkalmazás funkcióira, kezelőfelületére vonatkozó igényeit megfogalmazza.

*  Az applikációkat önállóan telepíti.

*  Az iskolai oktatáshoz kapcsolódó mobileszközökre fejlesztett alkalmazások használata során együttműködik társaival.

*  Tisztában van az e-világ -- e-szolgáltatások, e-ügyintézés, e-kereskedelem, e-állampolgárság, it-gazdaság, környezet, kultúra, információvédelem -- biztonsági és jogi kérdéseivel.

*  Tisztában van a digitális személyazonosság és az információhitelesség fogalmával.

*  A gyakorlatban alkalmazza az adatok védelmét biztosító lehetőségeket.

## Dráma és színház 
### 7-8. évfolyamon
*  Ismeri és alkalmazza a különböző verbális és nonverbális kommunikációs eszközöket.

*  Ismeri és alkalmazza a drámai és színházi kifejezés formáit.

*  Aktívan részt vesz többféle dramatikus tevékenységben tanári irányítással, önállóan, illetve társakkal való együttműködésben.

*  Saját gondolatot, témát, üzenetet fogalmaz meg a témához általa alkalmasnak ítélt dramatikus közlésformában.

*  Megfogalmazza egy színházi előadás kapcsán élményeit, gondolatait.

*  Felfedezi a tér, az idő, a tempó, a ritmus sajátosságait és összefüggéseit.

*  Megfigyeli, azonosítja és értelmezi a tárgyi világ jelenségeit.

*  Felidézi a látott, hallott, érzékelt verbális, vokális, vizuális, kinetikus hatásokat.

*  Kitalál és alkalmaz elképzelt verbális, vokális, vizuális, kinetikus hatásokat.

*  Tudatosan irányítja és összpontosítja figyelmét a környezete jelenségeire.

*  Koncentrált figyelemmel végzi a játékszabályok adta keretek között tevékenységeit.

*  Megfigyeli, azonosítja és értelmezi a környezetéből érkező hatásokra adott saját válaszait.

*  Értelmezi önmagát a csoport részeként, illetve a csoportos tevékenység alkotó közreműködőjeként.

*  Fejleszti az együttműködésre és a konszenzus kialakítására irányuló gyakorlatát.

*  Adekvát módon alkalmazza a verbális és nonverbális kifejezés eszközeit.

*  Az alkotótevékenység során használja a megismert kifejezési formákat.

*  Felfedezi a tárgyi világ kínálta eszközöket, ezek művészi formáit (pl. a bábot és a maszkot).

*  Használja a tér sajátosságaiban rejlő lehetőségeket.

*  Felfedezi a szerepbe lépésben és az együttjátszásban rejlő lehetőségeket.

*  Felismeri és alapszinten alkalmazza a kapcsolat létrehozásának és fenntartásának technikáit.

*  Felfedezi a kommunikációs jelek jelentéshordozó és jelentésteremtő erejét.

*  Felfedezi a feszültség élményét és szerepét a dramatikus tevékenységekben.

*  Felfedezi a színházi kommunikáció erejét.

*  Alkalmazza a tanult dramatikus technikákat a helyzetek megjelenítésében.

*  Felismeri a helyzetek feldolgozása során a szerkesztésben rejlő lehetőségeket.

*  Megkülönbözteti és alapszinten alkalmazza a dramaturgiai alapfogalmakat.

*  Értelmezi a megélt, a látott-hallott-olvasott, a kitalált történeteket a különböző dramatikus tevékenységek révén.

*  Felismeri a színházi élmény fontosságát.

*  A színházi előadást a dramatikus tevékenységek kiindulópontjául is használja.

*  Felismeri és azonosítja a dramatikus szituációk jellemzőit (szereplők, viszonyrendszer, cél, szándék, akarat, konfliktus, feloldás).

*  Felismeri és megvizsgálja a problémahelyzeteket és azok lehetséges megoldási alternatíváit.

*  Felismeri és azonosítja a dráma és a színház formanyelvi sajátosságait a látott előadásokban.

### 9-10. évfolyamon
*  Az élmény megélésén keresztül jusson el a megértésig.

*  Alkalmat kapjon az önmagára és a világra vonatkozó kérdések megfogalmazására és a válaszok keresésére.

*  Különböző élethelyzeteket védett környezetben, biztonságos keretek között vizsgáljon.

*  A drámán és színjátékon keresztül tanulja meg az önkifejezést.

*  Részt vegyen közösségépítésben és közösségi alkotásban.

*  Verbális és nonverbális kommunikációs készségei fejlődjenek.

*  Empátiás készségeit erősítse a dramatikus tevékenységekben való együttműködéssel.

*  Komplex látásmódot alakítson ki a dráma és a színház társadalmi, történelmi és kulturális szerepének megértésével.

*  Szabályjátékok, népi játékok

*  Dramatikus játékok (szöveggel, hanggal, bábbal, zenével, mozgással, tánccal)

*  Rögtönzés

*  Saját történetek feldolgozása

*  Műalkotások feldolgozása

*  Dramaturgiai alapfogalmak

*  A színház kifejezőeszközei (szöveg, hang, báb, zene, mozgás, tánc)

*  Színházi műfajok, stílusok

*  Színházi előadás megtekintése

*  Szabályjátékok

*  Dramatikus játékok (szöveggel, hanggal, bábbal, zenével, mozgással, tánccal)

*  Rögtönzés

*  Saját történetek feldolgozása

*  Műalkotások feldolgozása

*  Dramaturgiai ismeretek

*  A színház kifejezőeszközei (szöveg, hang, báb, zene, mozgás, tánc)

*  Dráma- és színháztörténet

*  Dráma- és színházelmélet

*  Kortárs dráma és színház

*  Színjátékos tevékenység (vers- és prózamondás, jelenet, előadás stb.)

*  Színházi előadás megtekintése

## Első élő idegen nyelv
### 4. évfolyamon
*  Megismerkedik az idegen nyelvvel, a nyelvtanulással és örömmel vesz részt az órákon.

*  Bekapcsolódik a szóbeliséget, írást, szövegértést vagy interakciót igénylő alapvető és korának megfelelő játékos, élményalapú élő idegen nyelvi tevékenységekbe.

*  Szóban visszaad szavakat, esetleg rövid, nagyon egyszerű szövegeket hoz létre.

*  Lemásol, leír szavakat és rövid, nagyon egyszerű szövegeket.

*  Követi a szintjének megfelelő, vizuális vagy nonverbális eszközökkel támogatott, ismert célnyelvi óravezetést, utasításokat.

*  Felismeri és használja a legegyszerűbb, mindennapi nyelvi funkciókat.

*  Elmondja magáról a legalapvetőbb információkat.

*  Ismeri az adott célnyelvi kultúrákhoz tartozó országok fontosabb jellemzőit és a hozzájuk tartozó alapvető nyelvi elemeket.

*  Törekszik a tanult nyelvi elemek megfelelő kiejtésére.

*  Célnyelvi tanulmányain keresztül nyitottabbá, a világ felé érdeklődőbbé válik.

*  Megismétli az élőszóban elhangzó egyszerű szavakat, kifejezéseket játékos, mozgást igénylő, kreatív nyelvórai tevékenységek során.

*  Lebetűzi a nevét.

*  Lebetűzi a tanult szavakat társaival közösen játékos tevékenységek kapcsán, szükség esetén segítséggel.

*  Célnyelven megoszt egyedül vagy társaival együttműködésben megszerzett, alapvető információkat szóban, akár vizuális elemekkel támogatva.

*  Felismeri az anyanyelvén, illetve a tanult idegen nyelven történő írásmód és betűkészlet közötti különbségeket.

*  Ismeri az adott nyelv ábécéjét.

*  Lemásol tanult szavakat játékos, alkotó nyelvórai tevékenységek során.

*  Megold játékos írásbeli feladatokat a szavak, szószerkezetek, rövid mondatok szintjén.

*  Részt vesz kooperatív munkaformában végzett kreatív tevékenységekben, projektmunkában szavak, szószerkezetek, rövid mondatok leírásával, esetleg képi kiegészítéssel.

*  Írásban megnevezi az ajánlott tématartományokban megjelölt, begyakorolt elemeket.

*  Megérti az élőszóban elhangzó, ismert témákhoz kapcsolódó, verbális, vizuális vagy nonverbális eszközökkel segített rövid kijelentéseket, kérdéseket.

*  Beazonosítja az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű szövegben a tanult nyelvi elemeket.

*  Kiszűri a lényeget az ismert nyelvi elemeket tartalmazó, nagyon rövid, egyszerű hangzó szövegből.

*  Azonosítja a célzott információt a nyelvi szintjének és életkorának megfelelő rövid hangzó szövegben.

*  Támaszkodik az életkorának és nyelvi szintjének megfelelő hangzó szövegre az órai alkotó jellegű nyelvi, mozgásos nyelvi és játékos nyelvi tevékenységek során.

*  Felismeri az anyanyelv és az idegen nyelv hangkészletét.

*  Értelmezi azokat az idegen nyelven szóban elhangzó nyelvórai szituációkat, melyeket anyanyelvén már ismer.

*  Felismeri az anyanyelve és a célnyelv közötti legalapvetőbb kiejtésbeli különbségeket.

*  Figyel a célnyelvre jellemző hangok kiejtésére.

*  Megkülönbözteti az anyanyelvi és a célnyelvi írott szövegben a betű- és jelkészlet közti különbségeket.

*  Beazonosítja a célzott információt az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű, a nyelvtanításhoz készült, illetve eredeti szövegben.

*  Csendes olvasás keretében feldolgozva megért ismert szavakat tartalmazó, pár szóból vagy mondatból álló, akár illusztrációval támogatott szöveget.

*  Megérti a nyelvi szintjének megfelelő, akár vizuális eszközökkel is támogatott írott utasításokat és kérdéseket, és ezekre megfelelő válaszreakciókat ad.

*  Kiemeli az ismert nyelvi elemeket tartalmazó, egyszerű, írott, pár mondatos szöveg fő mondanivalóját.

*  Támaszkodik az életkorának és nyelvi szintjének megfelelő írott szövegre az órai játékos alkotó, mozgásos vagy nyelvi fejlesztő tevékenységek során, kooperatív munkaformákban.

*  Megtapasztalja a közös célnyelvi olvasás élményét.

*  Aktívan bekapcsolódik a közös meseolvasásba, a mese tartalmát követi.

*  A tanórán begyakorolt, nagyon egyszerű, egyértelmű kommunikációs helyzetekben a megtanult, állandósult beszédfordulatok alkalmazásával kérdez vagy reagál, mondanivalóját segítséggel vagy nonverbális eszközökkel kifejezi.

*  Törekszik arra, hogy a célnyelvet eszközként alkalmazza információszerzésre.

*  Rövid, néhány mondatból álló párbeszédet folytat, felkészülést követően.

*  A tanórán bekapcsolódik a már ismert, szóbeli interakciót igénylő nyelvi tevékenységekbe, a begyakorolt nyelvi elemeket tanári segítséggel a tevékenység céljainak megfelelően alkalmazza.

*  Érzéseit egy-két szóval vagy begyakorolt állandósult nyelvi fordulatok segítségével kifejezi, főként rákérdezés alapján, nonverbális eszközökkel kísérve a célnyelvi megnyilatkozást.

*  Elsajátítja a tanult szavak és állandósult szókapcsolatok célnyelvi normához közelítő kiejtését tanári minta követése által, vagy autentikus hangzó anyag, digitális technológia segítségével.

*  Felismeri és alkalmazza a legegyszerűbb, üdvözlésre és elköszönésre használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és alkalmazza a legegyszerűbb, bemutatkozásra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, megszólításra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a köszönet és az arra történő reagálás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a tudás és nem tudás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a nem értés, visszakérdezés és ismétlés, kérés kifejezésére használt mindennapi nyelvi funkciókat életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Közöl alapvető személyes információkat magáról, egyszerű nyelvi elemek segítségével.

*  Új szavak, kifejezések tanulásakor ráismer a már korábban tanult szavakra, kifejezésekre.

*  Szavak, kifejezések tanulásakor felismeri, ha új elemmel találkozik és rákérdez, vagy megfelelő tanulási stratégiával törekszik a megértésre.

*  A célok eléréséhez társaival rövid feladatokban együttműködik.

*  Egy feladat megoldásának sikerességét segítséggel értékelni tudja.

*  Felismeri az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is.

*  Felhasznál és létrehoz rövid, nagyon egyszerű célnyelvi szövegeket szabadidős tevékenységek során.

*  Alapvető célzott információt megszerez a tanult témákban tudásának bővítésére.

*  Megismeri a főbb, az adott célnyelvi kultúrákhoz tartozó országok nevét, földrajzi elhelyezkedését, főbb országismereti jellemzőit.

*  Ismeri a főbb, célnyelvi kultúrához tartozó, ünnepekhez kapcsolódó alapszintű kifejezéseket, állandósult szókapcsolatokat és szokásokat.

*  Megérti a tanult nyelvi elemeket életkorának megfelelő digitális tartalmakban, digitális csatornákon olvasott vagy hallott nagyon egyszerű szövegekben is.

*  Létrehoz nagyon egyszerű írott, pár szavas szöveget szóban vagy írásban digitális felületen.

### 5-8. évfolyamon
*  Szóban és írásban megold változatos kihívásokat igénylő feladatokat az élő idegen nyelven.

*  Szóban és írásban létrehoz rövid szövegeket, ismert nyelvi eszközökkel, a korának megfelelő szövegtípusokban.

*  Értelmez korának és nyelvi szintjének megfelelő hallott és írott célnyelvi szövegeket az ismert témákban és szövegtípusokban.

*  A tanult nyelvi elemek és kommunikációs stratégiák segítségével írásbeli és szóbeli interakciót folytat, valamint közvetít az élő idegen nyelven.

*  Kommunikációs szándékának megfelelően alkalmazza a tanult nyelvi funkciókat és a megszerzett szociolingvisztikai, pragmatikai és interkulturális jártasságát.

*  Nyelvtudását egyre inkább képes fejleszteni tanórán kívüli helyzetekben is különböző eszközökkel és lehetőségekkel.

*  Használ életkorának és nyelvi szintjének megfelelő hagyományos és digitális alapú nyelvtanulási forrásokat és eszközöket.

*  Alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra, ismeretszerzésre hagyományos és digitális csatornákon.

*  Törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és intonáció megközelítésére.

*  Érti a nyelvtudás fontosságát, és motivációja a nyelvtanulásra tovább erősödik.

*  Aktívan részt vesz az életkorának és érdeklődésének megfelelő gyermek-, illetve ifjúsági irodalmi alkotások közös előadásában.

*  Egyre magabiztosabban kapcsolódik be történetek kreatív alakításába, átfogalmazásába kooperatív munkaformában.

*  Elmesél rövid történetet, egyszerűsített olvasmányt egyszerű nyelvi eszközökkel, önállóan, a cselekményt lineárisan összefűzve.

*  Egyszerű nyelvi eszközökkel, felkészülést követően röviden, összefüggően beszél az ajánlott tématartományokhoz tartozó témákban, élőszóban és digitális felületen.

*  Képet jellemez röviden, egyszerűen, ismert nyelvi fordulatok segítségével, segítő tanári kérdések alapján, önállóan.

*  Változatos, kognitív kihívást jelentő szóbeli feladatokat old meg önállóan vagy kooperatív munkaformában, a tanult nyelvi eszközökkel, szükség szerint tanári segítséggel, élőszóban és digitális felületen.

*  Megold játékos és változatos írásbeli feladatokat rövid szövegek szintjén.

*  Rövid, egyszerű, összefüggő szövegeket ír a tanult nyelvi szerkezetek felhasználásával az ismert szövegtípusokban, az ajánlott tématartományokban.

*  Rövid szövegek írását igénylő kreatív munkát hoz létre önállóan.

*  Rövid, összefüggő, papíralapú vagy ikt-eszközökkel segített írott projektmunkát készít önállóan vagy kooperatív munkaformákban.

*  A szövegek létrehozásához nyomtatott, illetve digitális alapú segédeszközt, szótárt használ.

*  Megérti a szintjének megfelelő, kevésbé ismert elemekből álló, nonverbális vagy vizuális eszközökkel támogatott célnyelvi óravezetést és utasításokat, kérdéseket.

*  Értelmezi az életkorának és nyelvi szintjének megfelelő, egyszerű, hangzó szövegben a tanult nyelvi elemeket.

*  Értelmezi az életkorának megfelelő, élőszóban vagy digitális felületen elhangzó szövegekben a beszélők gondolatmenetét.

*  Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó, élőszóban vagy digitális felületen elhangzó rövid szöveg tartalmát.

*  Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő, élőszóban vagy digitális felületen elhangzó szövegből, és azokat összekapcsolja egyéb ismereteivel.

*  Alkalmazza az életkorának és nyelvi szintjének megfelelő hangzó szöveget a változatos nyelvórai tevékenységek és a feladatmegoldás során.

*  Értelmez életkorának megfelelő nyelvi helyzeteket hallott szöveg alapján.

*  Felismeri a főbb, életkorának megfelelő hangzószöveg-típusokat.

*  Hallgat az érdeklődésének megfelelő autentikus szövegeket elektronikus, digitális csatornákon, tanórán kívül is, szórakozásra vagy ismeretszerzésre.

*  Értelmezi az életkorának megfelelő szituációkhoz kapcsolódó, írott szövegekben megjelenő összetettebb információkat.

*  Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó rövid írott szöveg tartalmát.

*  Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő szövegből, és azokat összekapcsolja más iskolai vagy iskolán kívül szerzett ismereteivel.

*  Megkülönbözteti a főbb, életkorának megfelelő írott szövegtípusokat.

*  Összetettebb írott instrukciókat értelmez.

*  Alkalmazza az életkorának és nyelvi szintjének megfelelő írott, nyomtatott vagy digitális alapú szöveget a változatos nyelvórai tevékenységek és feladatmegoldás során.

*  A nyomtatott vagy digitális alapú írott szöveget felhasználja szórakozásra és ismeretszerzésre önállóan is.

*  Érdeklődése erősödik a célnyelvi irodalmi alkotások iránt.

*  Megért és használ szavakat, szókapcsolatokat a célnyelvi, az életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel kapcsolatban

*  Kommunikációt kezdeményez egyszerű hétköznapi témában, a beszélgetést követi, egyszerű, nyelvi eszközökkel fenntartja és lezárja.

*  Az életkorának megfelelő mindennapi helyzetekben a tanult nyelvi eszközökkel megfogalmazott kérdéseket tesz fel, és válaszol a hozzá intézett kérdésekre.

*  Véleményét, gondolatait, érzéseit egyre magabiztosabban fejezi ki a tanult nyelvi eszközökkel.

*  A tanult nyelvi elemeket többnyire megfelelően használja, beszédszándékainak megfelelően, egyszerű spontán helyzetekben.

*  Váratlan, előre nem kiszámítható eseményekre, jelenségekre és történésekre is reagál egyszerű célnyelvi eszközökkel, személyes vagy online interakciókban.

*  Bekapcsolódik a tanórán az interakciót igénylő nyelvi tevékenységekbe, abban társaival közösen részt vesz, a begyakorolt nyelvi elemeket tanári segítséggel a játék céljainak megfelelően alkalmazza.

*  Üzeneteket ír,

*  Véleményét írásban, egyszerű nyelvi eszközökkel megfogalmazza, és arról írásban interakciót folytat.

*  Rövid, egyszerű, ismert nyelvi eszközökből álló kiselőadást tart változatos feladatok kapcsán, hagyományos vagy digitális alapú vizuális eszközök támogatásával.

*  Felhasználja a célnyelvet tudásmegosztásra.

*  Találkozik az életkorának és nyelvi szintjének megfelelő célnyelvi ismeretterjesztő tartalmakkal.

*  Néhány szóból vagy mondatból álló jegyzetet készít írott szöveg alapján.

*  Egyszerűen megfogalmazza személyes véleményét, másoktól véleményük kifejtését kéri, és arra reagál, elismeri vagy cáfolja mások állítását, kifejezi egyetértését vagy egyet nem értését.

*  Kifejez tetszést, nem tetszést, akaratot, kívánságot, tudást és nem tudást, ígéretet, szándékot, dicséretet, kritikát.

*  Információt cserél, információt kér, információt ad.

*  Kifejez kérést, javaslatot, meghívást, kínálást és ezekre reagálást.

*  Kifejez alapvető érzéseket, például örömöt, sajnálkozást, bánatot, elégedettséget, elégedetlenséget, bosszúságot, csodálkozást, reményt.

*  Kifejez és érvekkel alátámasztva mutat be szükségességet, lehetőséget, képességet, bizonyosságot, bizonytalanságot.

*  Értelmez és használja az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is,

*  Felhasználja a célnyelvet ismeretszerzésre.

*  Használja a célnyelvet életkorának és nyelvi szintjének megfelelő aktuális témákban és a hozzájuk tartozó szituációkban.

*  Találkozik életkorának és nyelvi szintjének megfelelő célnyelvi szórakoztató tartalmakkal.

*  Összekapcsolja az ismert nyelvi elemeket egyszerű kötőszavakkal (például: és, de, vagy).

*  Egyszerű mondatokat összekapcsolva mond el egymást követő eseményekből álló történetet, vagy leírást ad valamilyen témáról.

*  A tanult nyelvi eszközökkel és nonverbális elemek segítségével tisztázza mondanivalójának lényegét.

*  Ismeretlen szavak valószínű jelentését szövegösszefüggések alapján kikövetkezteti az életkorának és érdeklődésének megfelelő, konkrét, rövid szövegekben.

*  Alkalmaz nyelvi funkciókat rövid társalgás megkezdéséhez, fenntartásához és befejezéséhez.

*  Nem értés esetén a meg nem értett kulcsszavak vagy fordulatok ismétlését vagy magyarázatát kéri, visszakérdez, betűzést kér.

*  Megoszt alapvető személyes információkat és szükségleteket magáról egyszerű nyelvi elemekkel.

*  Ismerős és gyakori alapvető helyzetekben, akár telefonon vagy digitális csatornákon is, többnyire helyesen és érthetően fejezi ki magát az ismert nyelvi eszközök segítségével.

*  Tudatosan használ alapszintű nyelvtanulási és nyelvhasználati stratégiákat.

*  Hibáit többnyire észreveszi és javítja.

*  Ismer szavakat, szókapcsolatokat a célnyelven a témakörre jellemző, életkorának és érdeklődésének megfelelő más tudásterületen megcélzott tartalmakból.

*  Egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki magának.

*  Céljai eléréséhez megtalálja és használja a megfelelő eszközöket.

*  Céljai eléréséhez társaival párban és csoportban együttműködik.

*  Nyelvi haladását többnyire fel tudja mérni,

*  Társai haladásának értékelésében segítően részt vesz.

*  A tanórán kívüli, akár játékos nyelvtanulási lehetőségeket felismeri, és törekszik azokat kihasználni.

*  Felhasználja a célnyelvet szórakozásra és játékos nyelvtanulásra.

*  Digitális eszközöket és felületeket is használ nyelvtudása fejlesztésére,

*  Értelmez egyszerű, szórakoztató kisfilmeket

*  Megismeri a célnyelvi országok főbb jellemzőit és kulturális sajátosságait.

*  További országismereti tudásra tesz szert.

*  Célnyelvi kommunikációjába beépíti a tanult interkulturális ismereteket.

*  Találkozik célnyelvi országismereti tartalmakkal.

*  Találkozik a célnyelvi, életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel.

*  Megismerkedik hazánk legfőbb országismereti és történelmi eseményeivel célnyelven.

*  A célnyelvi kultúrákhoz kapcsolódó alapvető tanult nyelvi elemeket használja.

*  Idegen nyelvi kommunikációjában ismeri és használja a célnyelv főbb jellemzőit.

*  Következetesen alkalmazza a célnyelvi betű és jelkészletet

*  Egyénileg vagy társaival együttműködve szóban vagy írásban projektmunkát vagy kiselőadást készít, és ezeket digitális eszközök segítségével is meg tudja valósítani.

*  Találkozik az érdeklődésének megfelelő akár autentikus szövegekkel elektronikus, digitális csatornákon tanórán kívül is.

### 9-12. évfolyamon
*  Szóban és írásban is megold változatos kihívásokat igénylő, többnyire valós kommunikációs helyzeteket leképező feladatokat az élő idegen nyelven.

*  Szóban és írásban létrehoz szövegeket különböző szövegtípusokban.

*  Értelmez nyelvi szintjének megfelelő hallott és írott célnyelvi szövegeket kevésbé ismert témákban és szövegtípusokban is.

*  A tanult nyelvi elemek és kommunikációs stratégiák segítségével írásbeli és szóbeli interakciót folytat és tartalmakat közvetít idegen nyelven.

*  Kommunikációs szándékának megfelelően alkalmazza a nyelvi funkciókat és megszerzett szociolingvisztikai, pragmatikai és interkulturális jártasságát.

*  Nyelvtudását képes fejleszteni tanórán kívüli eszközökkel, lehetőségekkel és helyzetekben is, valamint a tanultakat és gimnáziumban a második idegen nyelv tanulásában is alkalmazza.

*  Felkészül az aktív nyelvtanulás eszközeivel az egész életen át történő tanulásra.

*  Használ hagyományos és digitális alapú nyelvtanulási forrásokat és eszközöket.

*  Alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra, ismeretszerzésre hagyományos és digitális csatornákon.

*  Törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és intonáció megközelítésére.

*  Beazonosítja nyelvtanulási céljait és egyéni különbségeinek tudatában, ezeknek megfelelően fejleszti nyelvtudását.

*  Első idegen nyelvéből sikeresen érettségit tesz a céljainak megfelelő szinten.

*  Visszaad tankönyvi vagy más tanult szöveget, elbeszélést, nagyrészt folyamatos és érthető történetmeséléssel, a cselekményt logikusan összefűzve.

*  Összefüggően, érthetően és nagyrészt folyékonyan beszél az ajánlott tématartományokhoz tartozó és az érettségi témákban a tanult nyelvi eszközökkel, felkészülést követően.

*  Beszámol saját élményen, tapasztalaton alapuló vagy elképzelt eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő rövid jellemzésével.

*  Ajánlott tématartományhoz kapcsolódó képi hatás kapcsán saját gondolatait, véleményét és érzéseit is kifejti az ismert nyelvi eszközökkel.

*  Összefoglalja ismert témában nyomtatott vagy digitális alapú ifjúsági tartalmak lényegét röviden és érthetően.

*  Közép- és emelt szintű nyelvi érettségi szóbeli feladatokat old meg.

*  Összefüggő, folyékony előadásmódú szóbeli prezentációt tart önállóan, felkészülést követően, az érettségi témakörök közül szabadon választott témában, ikt-eszközökkel támogatva mondanivalóját.

*  Kreatív, változatos műfajú szövegeket alkot szóban, kooperatív munkaformákban.

*  Beszámol akár az érdeklődési körén túlmutató környezeti eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

*  Összefüggően, világosan és nagyrészt folyékonyan beszél az ajánlott tématartományhoz tartozó és az idevágó érettségi témákban, akár elvontabb tartalmakra is kitérve.

*  Alkalmazza a célnyelvi normához illeszkedő, természeteshez közelítő kiejtést, beszédtempót és intonációt.

*  Írásban röviden indokolja érzéseit, gondolatait, véleményét már elvontabb témákban.

*  Leír összetettebb cselekvéssort, történetet, személyes élményeket, elvontabb témákban.

*  Információt vagy véleményt közlő és kérő, összefüggő feljegyzéseket, üzeneteket ír.

*  Alkalmazza a formális és informális regiszterhez köthető sajátosságokat.

*  Használ szövegkohéziós és figyelemvezető eszközöket.

*  Megold változatos írásbeli, feladatokat szövegszinten.

*  Papíralapú vagy ikt-eszközökkel segített írott projektmunkát készít önállóan vagy kooperatív munkaformában.

*  Összefüggő szövegeket ír önállóan, akár elvontabb témákban.

*  A szövegek létrehozásához nyomtatott vagy digitális segédeszközt, szótárt használ.

*  Beszámol saját élményen, tapasztalaton alapuló, akár az érdeklődési körén túlmutató vagy elképzelt személyes eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

*  Beszámol akár az érdeklődési körén túlmutató közügyekkel, szórakozással kapcsolatos eseményről a cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi eszközökkel történő összetettebb, részletes és világos jellemzésével.

*  A megfelelő szövegtípusok jellegzetességeit követi.

*  Értelmezi a szintjének megfelelő célnyelvi, komplexebb tanári magyarázatokat a nyelvórákon.

*  Megérti a célnyelvi, életkorának és érdeklődésének megfelelő hazai és nemzetközi hírek, események lényegét.

*  Kikövetkezteti a szövegben megjelenő elvontabb nyelvi elemek jelentését az ajánlott témakörökhöz kapcsolódó témákban.

*  Értelmezi a szövegben megjelenő összefüggéseket.

*  Megérti, értelmezi és összefoglalja az összetettebb, a tématartományhoz kapcsolódó összefüggő hangzó szöveget, és értelmezi a szövegben megjelenő összefüggéseket.

*  Megérti és értelmezi az összetettebb, az ajánlott témakörökhöz kapcsolódó összefüggő szövegeket, és értelmezi a szövegben megjelenő összefüggéseket.

*  Megérti az ismeretlen nyelvi elemeket is tartalmazó hangzó szöveg lényegi tartalmát.

*  Megérti a hangzó szövegben megjelenő összetettebb részinformációkat.

*  Megérti az elvontabb tartalmú hangzószövegek lényegét, valamint a beszélők véleményét is.

*  Alkalmazza a hangzó szövegből nyert információt feladatok megoldása során.

*  Célzottan keresi az érdeklődésének megfelelő autentikus szövegeket tanórán kívül is, ismeretszerzésre és szórakozásra.

*  A tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét számára kevésbé ismert témákban és szituációkban is.

*  A tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét akár anyanyelvi beszélők köznyelvi kommunikációjában a számára kevésbé ismert témákban és szituációkban is.

*  Megérti és értelmezi a legtöbb televíziós hírműsort.

*  Megért szokványos tempóban folyó autentikus szórakoztató és ismeretterjesztő tartalmakat, változatos csatornákon.

*  Elolvas és értelmez nyelvi szintjének megfelelő irodalmi szövegeket.

*  Megérti és értelmezi a lényeget az ajánlott tématartományokhoz kapcsolódó összefüggő, akár autentikus írott szövegekben.

*  Megérti és értelmezi az összefüggéseket és a részleteket az ajánlott tématartományokhoz kapcsolódó összefüggő, akár autentikus írott szövegekben.

*  Értelmezi a számára ismerős, elvontabb tartalmú szövegekben megjelenő ismeretlen nyelvi elemeket.

*  A szövegkörnyezet alapján kikövetkezteti a szövegben előforduló ismeretlen szavak jelentését.

*  Megérti az ismeretlen nyelvi elemeket is tartalmazó írott szöveg tartalmát.

*  Megérti és értelmezi az írott szövegben megjelenő összetettebb részinformációkat.

*  Kiszűr konkrét információkat nyelvi szintjének megfelelő szövegből, és azokat összekapcsolja egyéb ismereteivel.

*  Alkalmazza az írott szövegből nyert információt feladatok megoldása során.

*  Keresi az érdeklődésének megfelelő, célnyelvi, autentikus szövegeket szórakozásra és ismeretszerzésre tanórán kívül is.

*  Egyre változatosabb, hosszabb, összetettebb és elvontabb szövegeket, tartalmakat értelmez és használ.

*  Részt vesz a változatos szóbeli interakciót és kognitív kihívást igénylő nyelvórai tevékenységekben.

*  Szóban ad át nyelvi szintjének megfelelő célnyelvi tartalmakat valós nyelvi interakciót leképező szituációkban.

*  A társalgásba aktívan, kezdeményezően és egyre magabiztosabban bekapcsolódik az érdeklődési körébe tartozó témák esetén vagy az ajánlott tématartományokon belül.

*  Társalgást kezdeményez, a megértést fenntartja, törekszik mások bevonására, és szükség esetén lezárja azt az egyes tématartományokon belül, akár anyanyelvű beszélgetőtárs esetében is.

*  A társalgást hatékonyan és udvariasan fenntartja, törekszik mások bevonására, és szükség esetén lezárja azt, akár ismeretlen beszélgetőtárs esetében is.

*  Előkészület nélkül részt tud venni személyes jellegű, vagy érdeklődési körének megfelelő ismert témáról folytatott társalgásban,

*  Érzelmeit, véleményét változatos nyelvi eszközökkel szóban megfogalmazza és arról interakciót folytat.

*  A mindennapi élet különböző területein, a kommunikációs helyzetek széles körében tesz fel releváns kérdéseket információszerzés céljából, és válaszol megfelelő módon a hozzá intézett célnyelvi kérdésekre.

*  Aktívan, kezdeményezően és magabiztosan vesz részt a változatos szóbeli interakciót és kognitív kihívást igénylő nyelvórai tevékenységekben.

*  Társaival a kooperatív munkaformákban és a projektfeladatok megoldása során is törekszik a célnyelvi kommunikációra.

*  Egyre szélesebb körű témákban, nyelvi kommunikációt igénylő helyzetekben reagál megfelelő módon, felhasználva általános és nyelvi háttértudását, ismereteit, alkalmazkodva a társadalmi normákhoz.

*  Váratlan, előre nem kiszámítható eseményekre, jelenségekre és történésekre jellemzően célnyelvi eszközökkel is reagál tanórai szituációkban.

*  Szóban és írásban, valós nyelvi interakciók során jó nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő szinten vesz részt az egyes tématartományokban és az idetartozó érettségi témákban.

*  Informális és életkorának megfelelő formális írásos üzeneteket ír, digitális felületen is.

*  Véleményét írásban, tanult nyelvi eszközökkel megfogalmazza és arról írásban interakciót folytat.

*  Véleményét írásban változatos nyelvi eszközökkel megfogalmazza és arról interakciót folytat.

*  Írásban átad nyelvi szintjének megfelelő célnyelvi tartalmakat valós nyelvi interakciók során.

*  Írásban és szóban, valós nyelvi interakciók során jó nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő szinten vesz részt az egyes tématartományokban és az idetartozó érettségi témákban.

*  Összetett információkat ad át és cserél.

*  Egyénileg vagy kooperáció során létrehozott projektmunkával kapcsolatos kiselőadást tart önállóan, összefüggő és folyékony előadásmóddal, digitális eszközök segítségével, felkészülést követően.

*  Használ célnyelvi tartalmakat tudásmegosztásra.

*  Ismer más tantárgyi tartalmakat, részinformációkat célnyelven,

*  Összefoglal és lejegyzetel, írásban közvetít rövid olvasott vagy hallott szövegeket.

*  Környezeti témákban a kommunikációs helyzetek széles körében hatékonyan ad át és cserél információt.

*  Írott szöveget igénylő projektmunkát készít olvasóközönségnek.

*  Írásban közvetít célnyelvi tartalmakat valós nyelvi interakciót leképező szituációkban.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével további alapvető érzéseket fejez ki (pl. aggódást, félelmet, kételyt).

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez érdeklődést és érdektelenséget, szemrehányást, reklamálást.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez kötelezettséget, szándékot, kívánságot, engedélykérést, feltételezést.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez ítéletet, kritikát, tanácsadást.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez segítségkérést, ajánlást és ezekre történő reagálást.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez ok-okozat viszony vagy cél meghatározását.

*  Tanult kifejezések alkalmazásával és az alapvető nyelvi szokások követésével kifejez emlékezést és nem emlékezést.

*  Összekapcsolja a mondatokat megfelelő kötőszavakkal, így követhető leírást ad, vagy nem kronológiai sorrendben lévő eseményeket is elbeszél.

*  A kohéziós eszközök szélesebb körét alkalmazza szóbeli vagy írásbeli megnyilatkozásainak érthetőbb, koherensebb szöveggé szervezéséhez.

*  Több különálló elemet összekapcsol összefüggő lineáris szempontsorrá.

*  Képes rendszerezni kommunikációját: jelzi szándékát, kezdeményez, összefoglal és lezár.

*  Használ kiemelést, hangsúlyozást, helyesbítést.

*  Körülírással közvetíti a jelentéstartalmat, ha a megfelelő szót nem ismeri.

*  Ismert témákban a szövegösszefüggés alapján kikövetkezteti az ismeretlen szavak jelentését, megérti az ismeretlen szavakat is tartalmazó mondat jelentését.

*  Félreértéshez vezető hibáit kijavítja, ha beszédpartnere jelzi a problémát; a kommunikáció megszakadása esetén más stratégiát alkalmazva újrakezdi a mondandóját.

*  A társalgás vagy eszmecsere menetének fenntartásához alkalmazza a rendelkezésére álló nyelvi és stratégiai eszközöket.

*  Nem értés esetén képes a tartalom tisztázására.

*  Mondanivalóját kifejti kevésbé ismerős helyzetekben is nyelvi eszközök széles körének használatával.

*  A tanult nyelvi elemeket adaptálni tudja kevésbé begyakorolt helyzetekhez is.

*  Szóbeli és írásbeli közlései során változatos nyelvi struktúrákat használ.

*  A tanult nyelvi funkciókat és nyelvi eszköztárát életkorának megfelelő élethelyzetekben megfelelően alkalmazza.

*  Szociokulturális ismeretei (például célnyelvi társadalmi szokások, testbeszéd) már lehetővé teszik azt, hogy társasági szempontból is megfelelő kommunikációt folytasson.

*  Szükség esetén eltér az előre elgondoltaktól, és mondandóját a beszédpartnerekhez, hallgatósághoz igazítja.

*  Az ismert nyelvi elemeket vizsgahelyzetben is használja.

*  Megértést nehezítő hibáit önállóan javítani tudja.

*  Nyelvtanulási céljai érdekében alkalmazza a tanórán kívüli nyelvtanulási lehetőségeket.

*  Célzottan keresi az érdeklődésének megfelelő autentikus szövegeket tanórán kívül is, ismeretszerzésre és szórakozásra.

*  Felhasználja a célnyelvű, legfőbb hazai és nemzetközi híreket ismeretszerzésre és szórakozásra.

*  Használ célnyelvi elemeket más tudásterületen megcélzott tartalmakból.

*  Használ célnyelvi tartalmakat ismeretszerzésre.

*  Használ ismeretterjesztő anyagokat nyelvtudása fejlesztésére.

*  Hibáit az esetek többségében önállóan is képes javítani.

*  Hibáiból levont következtetéseire többnyire épít nyelvtudása fejlesztése érdekében.

*  Egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki magának.

*  Megfogalmaz hosszú távú nyelvtanulási célokat saját maga számára.

*  Nyelvtanulási céljai érdekében tudatosabban foglalkozik a célnyelvvel.

*  Céljai eléréséhez megtalálja és használja a megfelelő eszközöket, módokat.

*  Céljai eléréséhez társaival párban és csoportban is együttműködik.

*  Beazonosít nyelvtanulási célokat és ismeri az ezekhez tartozó nyelvtanulási és nyelvhasználati stratégiákat.

*  Használja a nyelvtanulási és nyelvhasználati stratégiákat nyelvtudása fenntartására és fejlesztésére.

*  Hatékonyan alkalmazza a tanult nyelvtanulási és nyelvhasználati stratégiákat.

*  Céljai eléréséhez önszabályozóan is dolgozik.

*  Az első idegen nyelvből sikeres érettségit tesz legalább középszinten.

*  Nyelvi haladását fel tudja mérni.

*  Használ önértékelési módokat nyelvtudása felmérésére.

*  Egyre tudatosabban használja az ön-, tanári, vagy társai értékelését nyelvtudása fenntartására és fejlesztésére.

*  Használja az ön-, tanári, vagy társai értékelését nyelvtudása fenntartására és fejlesztésére.

*  Hiányosságait, hibáit felismeri, azokat egyre hatékonyabban kompenzálja, javítja a tanult stratégiák felhasználásával.

*  Nyelvtanulási céljai érdekében él a valós nyelvhasználati lehetőségekkel.

*  Használja a célnyelvet életkorának és nyelvi szintjének megfelelő aktuális témákban és a hozzájuk tartozó szituációkban.

*  Az ismert nyelvi elemeket vizsgahelyzetben is használja.

*  Beszéd- és írásprodukcióját tudatosan megtervezi, hiányosságait igyekszik kompenzálni.

*  Nyelvi produkciójában és recepciójában önállóságot mutat, és egyre kevesebb korlát akadályozza.

*  Törekszik releváns digitális tartalmak használatára beszédkészségének, szókincsének és kiejtésének továbbfejlesztése céljából.

*  Digitális eszközöket és felületeket is magabiztosan használ nyelvtudása fejlesztésére.

*  Digitális eszközöket és felületeket is használ a célnyelven ismeretszerzésre és szórakozásra.

*  Alkalmazza a célnyelvi kultúráról megszerzett ismereteit informális kommunikációjában,

*  Ismeri a célnyelvi országok történelmének és jelenének legfontosabb vonásait,

*  Tájékozott a célnyelvi országok jellemzőiben és kulturális sajátosságaiban.

*  Tájékozott, és alkalmazni is tudja a célnyelvi országokra jellemző alapvető érintkezési és udvariassági szokásokat.

*  Ismeri és keresi a főbb hasonlóságokat és különbségeket saját anyanyelvi és a célnyelvi közösség szokásai, értékei, attitűdjei és meggyőződései között.

*  Átadja célnyelven a magyar értékeket.

*  Ismeri a célnyelvi és saját hazájának kultúrája közötti hasonlóságokat és különbségeket.

*  Interkulturális tudatosságára építve felismeri a célnyelvi és saját hazájának kultúrája közötti hasonlóságokat és különbségeket, és a magyar értékek átadására képessé válik.

*  Környezetének kulturális értékeit célnyelven közvetíti.

*  Kikövetkezteti a célnyelvi kultúrákhoz kapcsolódó egyszerű, ismeretlen nyelvi elemeket.

*  A célnyelvi kultúrákhoz kapcsolódó tanult nyelvi elemeket magabiztosan használja.

*  Interkulturális ismeretei segítségével társasági szempontból is megfelelő kommunikációt folytat írásban és szóban.

*  Felismeri a legfőbb hasonlóságokat és különbségeket az ismert nyelvi változatok között.

*  Megfogalmaz főbb hasonlóságokat és különbségeket az ismert nyelvi változatok között.

*  Alkalmazza a nyelvi változatokról megszerzett ismereteit informális kommunikációjában.

*  Megérti a legfőbb nyelvi dialektusok egyes elemeit is tartalmazó szóbeli közléseket.

*  Digitális eszközökön és csatornákon keresztül is alkot szöveget szóban és írásban.

*  Digitális eszközökön és csatornákon keresztül is megérti az ismert témához kapcsolódó írott vagy hallott szövegeket.

*  Digitális eszközökön és csatornákon keresztül is alkalmazza az ismert témához kapcsolódó írott vagy hallott szövegeket.

*  Digitális eszközökön és csatornákon keresztül is folytat célnyelvi interakciót az ismert nyelvi eszközök segítségével.

*  Digitális eszközökön és csatornákon keresztül is folytat a természeteshez közelítő célnyelvi interakciót az ismert nyelvi eszközök segítségével.

*  Digitális eszközökön és csatornákon keresztül is megfelelő nyelvi eszközökkel alkot szöveget szóban és írásban.

*  Alkalmazza az életkorának és érdeklődésének megfelelő digitális műfajok főbb jellemzőit.

## Ének-zene 
### 1-4. évfolyamon
*  Csoportosan vagy önállóan, életkorának és hangi sajátosságainak > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra > és a daloknak megfelelő tempóra.

*  Ismer legalább 180 gyermekdalt, magyar népdalt.

*  A tanult dalok, zenei részletek éneklésekor változatosan tudja > használni hangerejét a zenei kifejezésnek megfelelően.

*  Hangszerkíséretes dalokat énekel tanára vagy hangszeren játszó > osztálytársa kíséretével.

*  A tanult dalokhoz kapcsolódó játékokban, táncokban, dramatizált > előadásokban osztálytársaival aktívan részt vesz.

*  Fogalmi szinten megkülönbözteti az egyenletes lüktetést és a > ritmust.

*  Érzékeli és hangoztatja az egyenletes lüktetést, az ütemhangsúlyt a > tanult dalokban, zenei szemelvényekben.

*  Felismeri és hangoztatja a negyed, nyolcadpár, fél értékű > ritmusokat, a negyed és a fél értékű szünetet, tájékozódik a > 2/4-es ütemben, felismeri és használja az ütemvonalat, > záróvonalat, az ismétlőjelet.

*  Felismeri és hangoztatja az összetett ritmusokat (szinkópa, nyújtott > és éles ritmus), az egész értékű kottát, a pontozott fél értékű > kottát és az egyedül álló nyolcadot azok szüneteivel, valamint > tájékozódik a 4/4-es és 3/4-es ütemben.

*  Megkülönbözteti a páros és a páratlan lüktetést.

*  Ritmizálva szólaltat meg mondókákat, gyermekverseket.

*  Érzékeli a hangok magasságának változásait, különböző hangszíneket, > ellentétes dinamikai szinteket, ezeket felismeri az őt körülvevő > világ hangjaiban, tanult dalokban, zeneművekben.

*  A tanár által énekelt dalokat belső hallással követi.

*  Megismeri, énekli és alkalmazza a pentaton hangkészlet hangjait.

*  Ismeri a tanult, énekelt zenei anyaghoz köthető szolmizációs > hangokat, kézjelről énekel.

*  A dalokat tanári segítséggel szolmizálva énekli, kézjelekkel > mutatja.

*  Tanári segítséggel képes leírni és olvasni egyszerű ritmusokat, > dallamfordulatokat.

*  Érzékeli, hogy ugyanaz a dallamrészlet különböző magasságokban > írható, olvasható.

*  Reprodukálja a tanult ritmusokat mozgással, testhangszerrel, > valamint egyszerű ritmushangszerekkel.

*  A zeneművek befogadásában kreatívan használja képzeletét.

*  Adott szempontok alapján figyeli meg a hallgatott zeneművet.

*  Egyszerű ritmussorokat rögtönöz.

*  Különböző hangszíneket, hangmagasságokat, ellentétes dinamikai > szinteket hallás után megfigyel és reprodukál.

*  Rövid dallamsorokat rögtönöz.

*  Aktívan részt vesz az iskola vagy a helyi közösség hagyományos > ünnepein, tematikus projektjein.

*  Megismeri a gyermekdalokhoz kapcsolódó játékokat.

*  A tanári instrukciók alapján alakít, finomít zenei előadásmódján.

*  Életkori sajátosságának megfelelően képessé válik a zeneművek > érzelmi és intellektuális befogadására.

### 5-8. évfolyamon
*  Csoportosan vagy önállóan, életkorának és hangi sajátosságainak > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra, > kifejező, a zene stílusának megfelelő előadásra.

*  A zenei karaktereket differenciáltan tudja megszólaltatni a népi > vagy klasszikus stílusjegyeknek megfelelően egyszerű > többszólamúságban is.

*  127 új dalt ismer.

*  Emlékezetből énekli a himnuszt és a szózatot.

*  Változatosan tudja alkalmazni a tempó és dinamikai, előadási > utasításokat (tempo giusto, parlando, rubato, piano, mezzoforte, > forte).

*  Hangszerkíséretes dalokat énekel, tanára vagy hangszeren játszó > osztálytársa kíséretével.

*  A tanult dalokhoz kapcsolódó dramatizált előadásokban > osztálytársaival aktívan részt vesz.

*  Ismeri és alkalmazza az alapritmusok relációit (egész-, fél-, > negyedérték, nyolcadpár, fél- és negyedszünet) és az összetett > ritmusokat (szinkópa, kis- és nagy nyújtott, kis- és nagy éles, > triola, tizenhatodos ritmusok), grafikai jelüket és értéküket.

*  Különböző ritmusképleteket eltérő tempókban is reprodukál.

*  Érzékeli a tanult dalokban a váltakozó ütemek lüktetését.

*  Hallás útján megfigyeli a tanult zeneművekben a dúr és moll > hangzását, melyeket zenei karakterekhez, hangulatokhoz kapcsol.

*  Ismeri és jártasságot szerez a hétfokú skála szolmizációs hangjainak > írásában és olvasásában.

*  Ismeri az előjegyzésekhez kapcsolódó abszolút hangneveket.

*  Felismerő kottaolvasással követi és értelmezi a módosított hangok > szerepét a dalokban.

*  Érti a hangköz és hármashangzat fogalmát és fogalmi szinten a > hangközök harmóniaalkotó szerepét.

*  Fogalmi szinten ismeri a tiszta, kis- és nagy hangközöket (t1-t8), > és a fél és egész hangos építkezés logikáját.

*  Megnevezi és beazonosítja a kottakép alapvető elemeit, például > tempójelzés.

*  Ütemmutató, violin- és basszuskulcsok.

*  Felismerő kottaolvasással együttesen követi a ritmikai és dallami > elemeket.

*  Ismeretlen kotta esetében is vizuálisan nyomon tudja követni a > hallott ritmikai és dallami folyamatokat.

*  Érti és azonosítja a különböző formarészek zenén belüli szerepét.

*  Felismeri a homofon és polifon szerkesztést.

*  Életkori sajátosságainak megfelelően megadott szempontok > segítségével értelmezi a különböző műfajú és stílusú zenéket, > követi a lineáris és vertikális zenei folyamatokat.

*  Azonosítani tudja a zenetörténeti stílusok főbb jellemzőit, a > hozzájuk tartozó műfajokat, jellegzetes hangszereket, > hangszer-összeállításokat, ismeri történelmi, kulturális és > társadalmi hátterüket, mely segíti zeneértésüket.

*  Megfigyeli, összehasonlítja a zenét és a cselekményt, azonosítja a > témát és a szereplőket, hangulatot, műfajt, korszakot.

*  Azonosítja a tanult zenéket, megnevezi azok alkotóit és > keletkezésüknek ismert körülményeit.

*  Követni tudja a zenei elemek összetartozását, egymást kiegészítő > vagy egymástól eltérő struktúráját, a zenei kifejezésben betöltött > funkcióját.

*  Megfigyeli az egyes hangszerek hangszínének a hangszerjáték és az > előadásmód különbözőségéből adódó karakterét.

*  Kérdéseket vet fel a zenemű üzenetére és kifejezőeszközeire > vonatkozóan, többféle szempontot érvényesítve alkot véleményt.

*  Néhány mondattal összefoglalja a zenemű mondanivalóját (például > miért íródott, kinek szól, milyen gondolatokat, érzelmeket fejez > ki).

*  Megtalálja a kapcsolatot a zeneművek által közvetített élethelyzetek > és a saját élethelyzete között.

*  A tanult ritmikai és dallami elemeket alkalmazva egyszerű > ritmussorokat, dallamokat kiegészít és improvizál.

*  A zeneművektől inspirálódva produktívan használja képzeletét, > megfogalmazza a zene keltette érzéseit, gondolatait, véleményét.

*  A zeneművek befogadásának előkészítése során részt vesz olyan közös > kreatív zenélési formákban, melyek segítenek a remekművek közelébe > jutni, felhasználja énekhangját, az akusztikus környezet hangjait, > ütőhangszereket, egyszerűbb dallamhangszereket.

*  A zeneműveket műfajok és zenei korszakok szerint értelmezi, ismeri > történelmi, kulturális és társadalmi hátterüket.

*  Ismer néhány magyar és más kultúrákra jellemző hangszert.

*  Hangzás és látvány alapján felismeri a legelterjedtebb népi > hangszereket és hangszeregyütteseket.

*  Megkülönbözteti a giusto, parlando és rubato előadásmódú népdalokat.

*  Megkülönbözteti a műzenét, népzenét és a népdalfeldolgozásokat.

*  A tanult népdalokat tájegységekhez, azok ma is élő hagyományaihoz, > jellegzetes népművészeti motívumaihoz, ételeihez köti.

*  Aktívan részt vesz az iskola vagy a helyi közösség hagyományos > ünnepein és tematikus projektjeiben.

*  Képessé válik a zeneművek érzelmi és intellektuális befogadására.

*  Megfigyeli és felismeri az összefüggéseket zene és szövege, zenei > eszközök és zenei mondanivaló között.

*  A tanári instrukciók alapján alakít, finomít zenei előadásmódján.

*  Ismer és használ internetes zenei adatbázisokat, gyűjteményeket.

*  Önálló beszámolókat készít internetes és egyéb zenei adatbázisok, > gyűjtemények felhasználásával.

### 9-10. évfolyamon
*  Csoportosan vagy önállóan, életkorának és hangi sajátosságainak > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra.

*  A tanult dalokat stílusosan, kifejezően adja elő.

*  Emlékezetből és kottakép segítségével énekel régi és új rétegű > magyar népdalokat, más népek dalait, műdalokat, kánonokat.

*  43 új dalt ismer.

*  Hangszerkíséretes dalokat énekel, tanára vagy hangszeren játszó > osztálytársa kíséretével, a műdalok előadásában alkalmazkodni tud > a hangszerkísérethez.

*  A tanult dalokhoz kapcsolódó dramatizált előadásokban > osztálytársaival aktívan részt vesz.

*  Felismerő kottaolvasással követi a ritmikai és dallami elemeket.

*  Megnevezi és beazonosítja a kottakép alapvető elemeit, például > tempójelzés, ütemmutató, violin- és basszuskulcsok.

*  Ismeretlen kotta esetében is vizuálisan nyomon tudja követni a > hallott ritmikai és dallami folyamatokat.

*  Fogalmi szinten ismeri a hangközök harmóniaalkotó szerepét.

*  Érti és azonosítja a különböző formarészek zenén belüli szerepét.

*  Különbséget tud tenni korszakok, műfajok között.

*  Kezdetben tanári segítséggel, majd önállóan felismeri adott > zeneműben a stílus és korszak hatását.

*  A zeneműveket tanári segítséggel történelmi, földrajzi és társadalmi > kontextusba helyezi.

*  Kérdéseket vet fel a zenemű üzenetére és kifejezőeszközeire > vonatkozóan.

*  Kezdetben tanári segítséggel, majd önállóan azonosítja a zenében > megjelenő társadalmi, erkölcsi, vallási, és kulturális mintákat.

*  Megfigyeli a különböző zenei interpretációk közötti különbségeket, s > azokat véleményezi.

*  A dalok és a zeneművek befogadásához, azok előadásához felhasználja > eddigi ritmikai, dallami, harmóniai ismereteit.

*  A zeneműveket összekapcsolja élethelyzetekkel, melyeket saját élete > és környezete jelenségeire, problémáira tud vonatkoztatni.

*  Társítani tudja a zeneművekben megfogalmazott gondolatokat > hangszerelési, szerkesztési megoldásokkal, kompozíciós > technikákkal, formai megoldásokkal.

*  Alapszintű ismereteket alkalmaz a digitális technika zenei > felhasználásában.

*  Az elsajátított zenei anyagot élményszerűen alkalmazza tematikus > projektekben.

*  Részt vesz kreatív zenélési formákban.

*  Ismer magyar és más kultúrákra jellemző zenei sajátságokat, s ezeket > újonnan hallott zeneművekben is felfedezi.

*  Értelmezi a népdalok szövegét, mondanivalóját, megtalálja bennük > önmagát.

*  A tanult népdalokat tájegységekhez, azok ma is élő hagyományaihoz, > jellegzetes népművészeti motívumaihoz, ételeihez köti.

*  Konkrét művek példáin keresztül tanári segítséggel elkülöníti és > egymással kapcsolatba hozza az irodalomhoz és különböző művészeti > ágakhoz (film, képzőművészet, tánc) tartozó alkotások jellemző > vonásait a nyelvi, vizuális, mozgási és multimédiás művészetekben > és a zenében.

*  Konkrét, a tanár által választott műalkotásokon keresztül > összehasonlítja a különböző művészeti ágak kifejezőeszközeit, > például hogyan fejez ki azonos érzelmet, mondanivalót a zene és > más művészetek.

*  Ismeri a főbb fővárosi zenei intézményeket (például zeneakadémia, > magyar állami operaház, müpa), főbb vidéki zenei centrumokat > (például kodály központ -- pécs, kodály intézet -- kecskemét), > továbbá lakóhelye művelődési intézményeit.

*  Fogalmi szinten tájékozott a zenének a viselkedésre, fejlődésre és > agyműködésre gyakorolt hatásaiban.

*  Aktívan részt vesz az iskola vagy a helyi közösség hagyományos > ünnepein, tematikus projektjein.

*  A zeneműveket zenetörténeti kontextusba tudja helyezni, kapcsolatot > talál történelmi, irodalmi, kultúrtörténeti vonatkozásokkal, > azonosítja a műfaji jellemzőket

*  A hallott zeneműveket érzelmi és intellektuális módon közelíti meg, > érti és értékeli a művészeti alkotásokat.

*  Órai tapasztalatai és saját ismeretanyaga alapján önálló véleményt > alkot a zenemű

*  Mondanivalójáról, és azt az adott zeneműből vett példákkal > illusztrálja.

*  Értelmezi és önállóan véleményezi a zenéhez társított szöveg és a > zene kapcsolatát.

*  A tanult dalok üzenetét, saját életének, környezetének jelenségeire > tudja vonatkoztatni.

*  Önálló kutatást végez feladatai megoldásához nyomtatott és digitális > forrásokban.

*  Zenei ízlése az értékek mentén fejlődik.

## Etika
### 1-4. évfolyamon
*  Azonosítja saját helyzetét, erősségeit és fejlesztendő területeit, és ennek tudatában rövid távú célokat tartalmazó tervet tud kialakítani saját egyéni fejlődésére vonatkozóan.

*  Felismeri a hatékony kommunikációt segítő és akadályozó verbális és nonverbális elemeket és magatartásformákat.

*  Törekszik az érzelmek konstruktív és együttérző kifejezésmódjainak alkalmazására.

*  Felismeri az egyes cselekvésekre vonatkozó erkölcsi szabályokat, viselkedése szervezésénél figyelembe veszi ezeket.

*  Törekszik szabadságra, alkalmazkodásra, bizalomra, őszinteségre, tiszteletre és igazságosságra a különféle közösségekben.

*  Felismeri saját lelkiismerete működését.

*  Pozitív attitűddel viszonyul a nemzeti és a nemzetiségi hagyományok, a nemzeti ünnepek és az egyházi ünnepkörök iránt.

*  Érdeklődést tanúsít a gyermeki jogok és kötelezettségek megismerése iránt a családban, az iskolában és az iskolán kívüli közösségekben.

*  Megfogalmazza gondolatait az élet néhány fontos kérdéséről és a róluk tanított elképzelésekkel kapcsolatosan.

*  Erkölcsi fogalomkészlete tudatosodik és gazdagodik, az erkölcsi értékfogalmak, a segítség, önzetlenség, tisztelet, szeretet, felelősség, igazságosság, méltányosság, lelkiismeretesség, mértékletesség jelentésével.

*  Megismeri az élet tiszteletének és a felelősségvállalásának az elveit, hétköznapi szokásai alakításánál tekintettel van társas és fizikai környezetére.

*  A csoportos tevékenységek keretében felismeri és megjeleníti az alapérzelmeket, az alapérzelmeken kívül is felismeri és megnevezi a saját érzelmi állapotokat.

*  Felismeri, milyen tevékenységeket, helyzeteket kedvel, illetve nem kedvel, azonosítja saját viselkedésének jellemző elemeit.

*  Célokat tűz ki maga elé, és azonosítja a saját céljai eléréséhez szükséges főbb lépéseket.

*  Meggyőződése, hogy a hiányosságok javíthatók, a gyengeségek fejleszthetők, és ehhez teljesíthető rövid távú célokat tűz maga elé saját tudásának és képességeinek fejlesztése céljából.

*  Céljai megvalósítása során önkontrollt, siker esetén önjutalmazást gyakorol.

*  Ismeri a testi-lelki egészség fogalmát és főbb szempontjait , motivált a krízisek megelőzésében, és a megoldáskeresésben.

*  Rendelkezik a stresszhelyzetben keletkezett negatív érzelmek kezeléséhez saját módszerekkel.

*  Felismeri az őt ért bántalmazást, ismer néhány olyan segítő bizalmi személyt, akihez segítségért fordulhat.

*  Megfogalmazza a nehéz élethelyzettel (pl.:új családtag érkezése vagy egy családtag eltávozása) kapcsolatos érzéseit.

*  Felismeri annak fontosságát, hogy sorsfordító családi események kapcsán saját érzelmeit felismerje, megélje, feldolgozza, s azokat elfogadható módon kommunikálja a környezete felé.

*  Ismeri az életkorának megfelelő beszélgetés alapvető szabályait.

*  Mások helyzetébe tudja képzelni magát, és megérti a másik személy érzéseit.

*  Különbséget tesz verbális és nem verbális jelzések között.

*  Megkülönbözteti a felnőttekkel és társakkal folytatott társas helyzeteket.

*  Megkülönbözteti a sértő és tiszteletteljes közlési módokat fizikai és digitális környezetben egyaránt, barátsággá alakuló kapcsolatokat kezdeményez.

*  Saját érdekeit másokat nem bántó módon fejezi ki, az ehhez illeszkedő kifejezésmódokat ismeri és alkalmazza.

*  Alkalmazza az asszertív viselkedés elemeit konfliktushelyzetben és másokkal kezdeményezett interakcióban, baráti kapcsolatokat tart fenn.

*  Felismeri a „jó" és „rossz" közötti különbséget a közösen megbeszélt eseményekben és történetekben.

*  Erkölcsi érzékenységgel reagál az igazmondást, a becsületességet, a személyes és szellemi tulajdont, valamint az emberi méltóság tiszteletben tartását érintő helyzetekre fizikai és digitális környezetben is.

*  Megérti a családi szokások jelentőségét és ezek természetes különbözőségét (alkalmazkodik az éjszakai pihenéshez, az étkezéshez, a testi higiénéhez fűződő, a tanulási és a játékidőt meghatározó családi szokásokhoz).

*  Megérti az ünneplés jelentőségét, elkülöníti a családi, a nemzeti, az állami és egyéb ünnepeket, és az egyházi ünnepköröket, aktív résztvevője a közös ünnepek előkészületeinek.

*  Szerepet vállal iskolai rendezvényeken, illetve azok előkészítésében, vagy iskolán belül szervezett szabadidős programban vesz részt.

*  Képes azonosítani a szeretet és az elfogadás jelzéseit.

*  A közvetlen lakóhelyéhez kapcsolódó nevezetességeket ismeri, ezekről információkat gyűjt fizikai és digitális környezetben, társaival együtt meghatározott formában bemutatót készít.

*  Érdeklődést mutat magyarország történelmi emlékei iránt, ismer közülük néhányat.

*  Az életkorához illeszkedő mélységben ismeri a nemzeti, az állami ünnepek, egyházi ünnepkörök jelentését, a hozzájuk kapcsolódó jelképeket.

*  A lakóhelyén élő nemzetiségek tagjai, hagyományai iránt nyitott, ezekről információkat gyűjt fizikai és digitális környezetben.

*  Tájékozott a testi és érzelmi biztonságra vonatkozó gyermeki jogokról.

*  Tájékozott a képességek kibontakoztatását érintő gyermeki jogokról, ennek családi, iskolai és iskolán kívüli következményeiről, a gyermekek kötelességeiről.

*  Irodalmi szemelvények alapján példákat azonosít igazságos és igazságtalan cselekedetekre, saját élmény alapján példát hoz ilyen helyzetekre, valamint részt vesz ezek megbeszélésében, tanítói vezetéssel.

*  Magatartásával az igazságosság, a fair play elveinek betartására törekszik, és ezáltal igyekszik mások bizalmát elnyerni.

*  A bibliai szövegekre támaszkodó történetek megismerése alapján értelmezi, milyen vallási eseményhez kapcsolódik egy-egy adott ünnep.

*  Más vallások ünnepei közül ismer néhányat.

*  Azonosítja az olvasott vagy hallott bibliai tanításokban, mondákban, mesékben a megjelenő együttélési szabályokat.

*  A bibliai történetekben megnyilvánuló igazságos és megbocsátó magatartásra saját életéből példákat hoz, vagy megkezdett történetet a megadott szempont szerint fejez be.

*  Ismer néhány kihalt vagy kihalófélben lévő élőlényt, tájékozott a jelenség magyarázatában, és ezekről információt gyűjt fizikai és digitális környezetben is.

*  A szabályok jelentőségét különböző kontextusokban azonosítja és társaival megvitatja, a szabályszegés lehetséges következményeit megfogalmazza.

*  Játékvásárlási szokásaiban példát hoz olyan elemekre, amelyek révén figyelembe vehetők a környezetvédelmi szempontok, és felhívja társai figyelmét is ezekre.

*  A naponta használt csomagolóeszközök kiválasztásában megindokolja, hogy milyen elvek alkalmazása támogatja a környezetvédelmi szempontok érvényesülését, és ezekre társai figyelmét is felhívja.

### 5-8. évfolyamon
*  Fogalmi rendszerében kialakul az erkölcsi jó és rossz, mint minősítő kategória, az erkölcsi dilemma és az erkölcsi szabályok fogalma, ezeket valós vagy fiktív példákhoz tudja kötni.

*  Erkölcsi szempontok alapján vizsgálja személyes helyzetét; etikai alapelvek és az alapvető jogok szerint értékeli saját és mások élethelyzetét, valamint néhány társadalmi problémát.

*  Környezetében felismeri azokat a személyeket, akiknek tevékenysége példaként szolgálhat mások számára; ismeri néhány kiemelkedő személyiség életművét, és elismeri hozzájárulását a tudomány, a technológia, a művészetek gyarapításához, nemzeti és európai történelmünk alakításához.

*  Azonosítja saját szerepét a családi viszonyrendszerben, felismeri azokat az értékeket, amelyek a család összetartozását, harmonikus működését és az egyén egészséges fejlődését biztosítják.

*  Felismeri saját és mások érzelmi állapotát, az adott érzelmi állapotot kezelő viselkedést tud választani; a helyzethez igazodó társas konfliktus-megoldási eljárás alkalmazására törekszik.

*  Elfogadó attitűdöt tanúsít az eltérő társadalmi, kulturális helyzetű vagy különleges bánásmódot igénylő tanulók iránt.

*  Véleményt formál a zsidó keresztény, keresztyén értékrenden alapuló vallások erkölcsi értékeiről, feltárja, hogy hogyan jelennek meg ezek az értékek az emberi viselkedésben a közösségi szabályokban.

*  A személyes életben is megvalósítható tevékenységeket végez, ami összhangban van a teremtett rend megőrzésével, a fenntartható jövővel.

*  Strukturált önmegfigyelésre alapozva megismeri személyiségének egyes jellemzőit, saját érdeklődési körét, speciális pályaérdeklődését.

*  Megismeri az identitás fogalmát és jellemzőit, azonosítja saját identitásának néhány elemét.

*  Különbséget tesz a valóságos és a virtuális identitás között, megérti a virtuális identitás jellemzőit.

*  Megfelelő döntéseket hoz arról, hogy az online térben milyen információkat oszthat meg önmagáról.

*  Reflektív tanulási gyakorlatot alakít ki, önálló tanulási feladatot kezdeményez.

*  Dramatikus eszközökkel megjelenített helyzetekben különböző érzelmi állapotok által vezérelt viselkedést eljátszik, és ennek a másik személyre tett hatását a csoporttal közösen megfogalmazza.

*  Valósághűen elmondja, hogy a saját érzelmi állapotai milyen hatást gyakorolnak a társas kapcsolatai alakítására, a tanulási tevékenységére.

*  Kérdőívek használatával felismeri pályaérdeklődését és továbbtanulási céljait.

*  Ismer testi és mentális egészséget őrző tevékenységeket és felismeri a saját egészségét veszélyeztető hatásokat; megfogalmazza saját intim terének határait.

*  Megismer olyan mintákat és lehetőségeket, amelyek segítségével a krízishelyzetek megoldhatók, és tudja, hogy adott esetben hová fordulhat segítségért.

*  Azonosítja a valós és virtuális térben történő zaklatások fokozatait és módjait, van terve a zaklatások elkerülésére, kivédésére, tudja, hogy hová fordulhat segítségért.

*  Képes a saját véleményétől eltérő véleményekhez tisztelettel viszonyulni, a saját álláspontja mellett érvelni, konszenzusra törekszik.

*  Felismeri a konfliktus kialakulására utaló jelzéseket, vannak megoldási javaslatai a konfliktusok békés megoldására.

*  Azonosítja a csoportban elfoglalt helyét és szerepét, törekszik a személyiségének legjobban megfelelő feladatok ellátására.

*  Törekszik mások helyzetének megértésére, felismeri a mások érzelmi állapotára és igényeire utaló jelzéseket, a fizikai és a digitális környezetben egyaránt.

*  Nyitott és segítőkész a nehéz helyzetben lévő személyek iránt.

*  Értelmezi a szabadság és önkorlátozás, a tolerancia és a szeretet megjelenését és határait egyéni élethelyzeteiben.

*  Azonosítja a számára fontos közösségi értékeket, indokolja, hogy ezek milyen szerepet játszanak a saját életében.

*  Azonosítja, értékeli az etikus és nem etikus cselekvések lehetséges következményeit.

*  A csoporthoz való csatlakozás vagy az onnan való kiválás esetén összeveti a csoportnormákat és saját értékrendjét, mérlegeli az őt érő előnyöket és hátrányokat.

*  Azonosítja és összehasonlítja a családban betöltött szerepeket és feladatokat.

*  Érzékeli a családban előforduló, bizalmat érintő konfliktusos területeket.

*  Felismeri saját családjának viszonyrendszerét, átéli a családot összetartó érzelmeket és társas lelkületi értékeket, a különböző generációk családot összetartó szerepét.

*  Megfogalmazza, hogy a szeretetnek, hűségnek, elkötelezettségnek, bizalomnak, tiszteletnek milyen szerepe van a társas lelkületi kapcsolatokban.

*  Fizikai vagy digitális környezetben információt gyűjt és megosztja tudását a sport, a technika vagy a művészetek területén a nemzet és európa kultúráját meghatározó kiemelkedő személyiségekről és tevékenységükről.

*  Ismeri a nemzeti identitást meghatározó kulturális értékeket, indokolja miért fontos ezek megőrzése.

*  Azonosítja a nemzeti és az európai értékek közös jellemzőit, az európai kulturális szellemiség, értékrendszer meghatározó elemeit.

*  Összefüggéseket gyűjt a keresztény, keresztyén vallás és az európai, nemzeti értékvilágról, a közös jelképekről, szimbólumokról, az egyházi ünnepkörökről.

*  Ismeri a rá vonatkozó gyermekjogokat, az ezeket szabályozó főbb dokumentumokat, értelmezi kötelezettségeit, részt vesz a szabályalkotásban.

*  Részt vesz a különleges bánásmódot igénylő tanulók megértését segítő feladatokban, programokban, kifejti saját véleményét.

*  Értelmezi a norma- és szabályszegés következményeit, és etikai kérdéseket vet fel velük kapcsolatban.

*  Azonosítja az egyéni, családi és a társadalmi boldogulás, érvényesülés feltételeit.

*  Életkorának megfelelő szinten értelmezi a családi élet mindennapjait befolyásoló fontosabb jogszabályok nyújtotta lehetőségeket (például családi pótlék, családi adókedvezmény, gyermekvédelmi támogatás).

*  Feltárja, hogyan jelenik meg a hétköznapok során a vallás emberi életre vonatkozó tanítása.

*  Értelmezi a szeretetnek, az élet tisztelete elvének a kultúrára gyakorolt hatását.

*  Egyéni cselekvési lehetőségeket fogalmaz meg az erkölcsi értékek érvényesítésére.

*  Saját életét meghatározó világnézeti meggyőződés birtokában a kölcsönös tolerancia elveit valósítja meg a tőle eltérő nézetű személyekkel való kapcsolata során.

*  Folyamatosan frissíti az emberi tevékenység környezetre gyakorolt hatásaival kapcsolatos ismereteit fizikai és digitális környezetében, mérlegelő szemlélettel vizsgálja a technikai fejlődés lehetőségeit.

*  Megismeri és véleményezi a természeti erőforrások felhasználására, a környezetszennyeződés, a globális és társadalmi egyenlőtlenségek problémájára vonatkozó etikai felvetéseket.

*  Értelmezi a teremtett rend, világ, a fenntarthatóság összefüggéseit, az emberiség ökológiai cselekvési lehetőségeit.

## Fizika 
### 9-10. évfolyamon
*  Azonosítani tudja a fizika körébe tartozó problémákat, a természeti és technikai környezet leírására a megfelelő fizikai mennyiségeket használja, a jelenségek értelmezése során a megismert fizikai elveket alkalmazza.

*  A megismert jelenségek kapcsán egyszerű számolásokat végezzen, grafikus formában megfogalmazott feladatokat oldjon meg, egyszerű méréseket, megfigyeléseket tervezzen, végrehajtson, kiértékeljen, ábrákat készítsen.

*  Tudjon információkat keresni a vizsgált tudományterülethez kapcsolódóan a rendelkezésre álló információforrásokban, elektronikus adathordozókon, nyitottan közelítsen az újdonságokhoz folyamatos érdeklődés mellett.

*  Ismerje meg a fenntartható fejlődés fogalmát és fizikai vonatkozásait, elősegítve ezzel a természet és környezet, illetve a fenntartható fejlődést segítő életmód iránti felelősségteljes elköteleződés kialakulását.

*  Felismerjen és megértsen a természettudományok különböző területei között fennálló kapcsolatokat konkrét jelenségek kapcsán.

*  Eligazodjon a közvetlen természeti és technikai környezetükben, illetve a tanultakat alkalmazni tudja a mindennapokban használt eszközök működési elvének megértésére, a biztonságos eszközhasználat elsajátítására.

*  Felismerje az ember és környezetének kölcsönhatásából fakadó előnyöket és problémákat, tudatosítsa az emberiség felelősségét a környezet megóvásában.

*  Fel tudja tárni a megfigyelt jelenségek ok-okozati hátterét.

*  Képessé váljon univerzumunkat és az embert kölcsönhatásában szemlélni, az emberiség fejlődéstörténetét, jelenét és jövőjét és az univerzum történetét összekapcsolni.

*  Tisztába kerüljön azzal, hogy a tudomány művelése alapvetően társadalmi jelenség.

*  Megtanuljon különbséget tenni a valóság és az azt leképező természettudományos modellek, leírások és világról alkotott képek között.

*  Felismerje, hogy a természet egységes egész, szétválasztását résztudományokra csak a jobb kezelhetőség, áttekinthetőség indokolja, a fizika törvényei általánosak, amelyek a kémia, a biológia, a földtudományok és az alkalmazott műszaki tudományok területén is érvényesek.

*  Fizikai jelenségek megfigyelése, egyszerű értelmezése

*  Mozgások a környezetünkben, a közlekedés

*  A levegő, a víz, a szilárd anyagok

*  Fontosabb mechanikai, hőtani, elektromos és optikai eszközeink > működésének alapjai, fűtés és világítás a háztartásban

*  Az energia megjelenési formái, megmaradása, energiatermelés és > felhasználás

*  A föld, a naprendszer és a világegyetem, a föld jövője, megóvása

*  A fizikai jelenségek megfigyelése, modellalkotás, értelmezés, tudományos érvelés

*  Mozgások a környezetünkben, a közlekedés kinematikai és dinamikai vonatkozásai

*  A halmazállapotok és változásuk, a légnemű, folyékony és szilárd anyagok tulajdonságai

*  Az emberi test fizikájának elemei

*  Fontosabb mechanikai, hőtani és elektromos eszközeink működésének alapjai, fűtés és világítás a háztartásban

*  A hullámok szerepe a képek és hangok rögzítésében, továbbításában

*  Az energia megjelenési formái, megmaradása, energiatermelés és -felhasználás

*  Az atom szerkezete, fénykibocsátás, radioaktivitás

*  A föld, a naprendszer és a világegyetem, a föld jövője, megóvása, az űrkutatás eredményei

*  Ismeri a helyét a világegyetemben, látja a világegyetem időbeli > fejlődését, lehetséges jövőjét, az emberiség és a világegyetem > kapcsolatának kulcskérdéseit.

*  Tisztában van azzal, hogy a fizika átfogó törvényeket ismer fel, > melyek alkalmazhatók jelenségek értelmezésére, egyes események > minőségi és mennyiségi előrejelzésére.

*  Felismeri, hogyan jelennek meg a fizikai ismeretek a mindennapi > tevékenységek során, valamint a gyakran használt technikai > eszközök működésében.

*  Ismeri a világot leíró legfontosabb természeti jelenségeket, az > azokat leíró fizikai mennyiségeket, azok jelentését, jellemző > nagyságrendjeit.

*  Gyakorlati oldalról ismeri a tudományos megismerési folyamatot: > megfigyel, mér, adatait összeveti az egyszerű modellekkel, korábbi > ismereteivel. ennek alapján következtet, megerősít, cáfol.

*  Egyszerű fizikai rendszerek esetén a lényeges elemeket a > lényegtelenektől el tudja választani, az egyszerűbb számításokat > el tudja végezni és a helyes logikai következtetéseket le tudja > vonni, illetve táblázatokat, ábrákat, grafikonokat tud értelmezni.

*  Tájékozott a földünket és környezetünket fenyegető globális > problémákban, ismeri az emberi tevékenység szerepét ezek > kialakulásában.

*  Látja a fizikai ismeretek bővülése és a társadalmi-gazdasági > folyamatok, történelmi események közötti kapcsolatot.

*  Tud önállóan fizikai témájú ismeretterjesztő szövegeket olvasni, a > lényeget kiemelni, el tudja különíteni a számára világos, valamint > a nem érthető, további magyarázatra szoruló részeket.

*  Tudományos ismereteit érveléssel meg tudja védeni, vita során ki > tudja fejteni véleményét, érveit és ellenérveit, mérlegelni tudja > egy elképzelés tudományos megalapozottságát.

*  Egyszerű méréseket, kísérleteket végez, az eredményeket rögzíti.

*  Fizikai kísérleteket önállóan is el tud végezni.

*  Ismeri a legfontosabb mértékegységek jelentését, helyesen használja a mértékegységeket számításokban, illetve az eredmények összehasonlítása során.

*  A mérések és a kiértékelés során alkalmazza a rendelkezésre álló számítógépes eszközöket, programokat.

*  Megismételt mérések segítségével, illetve a mérés körülményeinek ismeretében következtet a mérés eredményét befolyásoló tényezőkre.

*  Egyszerű, a megértést segítő számolási feladatokat old meg, táblázatokat, ábrákat, grafikonokat értelmez, következtetést von le, összehasonlít.

*  El tudja választani egyszerű fizikai rendszerek esetén a lényeges elemeket a lényegtelenektől.

*  Gyakorlati oldalról ismeri a tudományos megismerési folyamatot: megfigyelés, mérés, a tapasztalatok, mérési adatok rögzítése, rendszerezése, ezek összevetése valamilyen egyszerű modellel vagy matematikai összefüggéssel, a modell (összefüggés) továbbfejlesztése.

*  Tudja, hogyan születnek az elismert, új tudományos felismerések, ismeri a tudományosság kritériumait.

*  Tisztában van azzal, hogy a fizika átfogó törvényeket ismer fel, melyek alkalmazhatók jelenségek értelmezésére, egyes események minőségi és mennyiségi előrejelzésére.

*  Felismeri a tudomány által vizsgálható jelenségeket, azonosítani tudja a tudományos érvelést, kritikusan vizsgálja egy elképzelés tudományos megalapozottságát.

*  Ismeri a fizika főbb szakterületeit, néhány új eredményét.

*  Kialakult véleményét mérési eredményekkel, érvekkel támasztja alá.

*  Tisztában van a különböző típusú erőművek használatának előnyeivel és környezeti kockázatával.

*  Érti az atomreaktorok működésének lényegét, a radioaktív hulladékok elhelyezésének problémáit.

*  Ismeri a környezet szennyezésének leggyakoribb forrásait, fizikai vonatkozásait.

*  Ismeri a megújuló és a nem megújuló energiaforrások használatának és az energia szállításának legfontosabb gyakorlati kérdéseit.

*  Az emberiség energiafelhasználásával kapcsolatos adatokat gyűjt, az információkat szemléletesen mutatja be.

*  Tudja, hogy a föld elsődleges energiaforrása a nap, ismeri a napenergia felhasználási lehetőségeit, a napkollektor és a napelem mibenlétét, a közöttük lévő különbséget.

*  Átlátja az ózonpajzs szerepét a földet ért ultraibolya sugárzással kapcsolatban.

*  Tisztában van az éghajlatváltozás kérdésével, az üvegházhatás jelenségével a természetben, a jelenség erőssége és az emberi tevékenység kapcsolatával.

*  Ismeri az űrkutatás történetének főbb fejezeteit, jövőbeli lehetőségeit, tervezett irányait.

*  Tisztában van az űrkutatás ipari-technikai civilizációra gyakorolt hatásával, valamint az űrkutatás tágabb értelemben vett céljaival (értelmes élet keresése, új nyersanyagforrások felfedezése).

*  Néhány konkrét példa alapján felismeri a fizika tudásrendszerének fejlődése és a társadalmi-gazdasági folyamatok, történelmi események közötti kapcsolatot.

*  El tudja helyezni lakóhelyét a földön, a föld helyét a naprendszerben, a naprendszer helyét a galaxisunkban és az univerzumban.

*  Átlátja az emberiség és a világegyetem kapcsolatának kulcskérdéseit.

*  Adatokat gyűjt és dolgoz fel a legismertebb fizikusok életével, tevékenységével, annak gazdasági, társadalmi hatásával, valamint emberi vonatkozásaival kapcsolatban (galileo galilei, michael faraday, james watt, eötvös loránd, marie curie, ernest rutherford, niels bohr, albert einstein, szilárd leó, wigner jenő, teller ede).

*  Ismeri a legfontosabb természeti jelenségeket (például légköri jelenségek, az égbolt változásai, a vízzel kapcsolatos jelenségek), azok megfelelően egyszerűsített, a fizikai mennyiségeken és törvényeken alapuló magyarázatait.

*  Tudja, hogyan jönnek létre a természet színei, és hogyan észleljük azokat.

*  Ismeri a villámok veszélyét, a villámhárítók működését, a helyes magatartást zivataros, villámcsapás-veszélyes időben.

*  Ismeri a légnyomás változó jellegét, a légnyomás és az időjárás kapcsolatát.

*  Érti a legfontosabb közlekedési eszközök -- gépjárművek, légi és vízi járművek -- működésének fizikai elveit.

*  Átlátja a korszerű lakások és házak hőszabályozásának fizikai kérdéseit (fűtés, hűtés, hőszigetelés).

*  Ismeri a háztartásban használt fontosabb elektromos eszközöket, az elektromosság szerepét azok működésében, szemléletes képe van a váltakozó áramról.

*  Tisztában van a konyhai tevékenységek (melegítés, főzés, hűtés) fizikai vonatkozásaival.

*  Átlátja a jelen közlekedése, közlekedésbiztonsága szempontjából releváns gyakorlati ismereteket, azok fizikai hátterét.

*  Ismeri az egyszerű gépek elvének megjelenését a hétköznapokban, mindennapi eszközeinkben.

*  Tisztában van az aktuálisan használt világító eszközeink működési elvével, energiafelhasználásának sajátosságaival, a korábban alkalmazott megoldásokhoz képesti előnyeivel.

*  Ismeri a mindennapi életben használt legfontosabb elektromos energiaforrásokat, a gépkocsi-, mobiltelefon-akkumulátorok legfontosabb jellemzőit.

*  Ismeri az elektromágneses hullámok szerepét az információ- (hang-, kép-) átvitelben, ismeri a mobiltelefon legfontosabb tartozékait (sim kártya, akkumulátor stb.), azok kezelését, funkcióját.

*  Ismeri a digitális fényképezőgép működésének elvét.

*  Tisztában van az elektromágneses hullámok frekvenciatartományaival, a rádióhullámok, mikrohullámok, infravörös hullámok, a látható fény, az ultraibolya hullámok, a röntgensugárzás, a gamma-sugárzás gyakorlati felhasználásával.

*  Tisztában van az elektromos áram veszélyeivel, a veszélyeket csökkentő legfontosabb megoldásokkal (gyerekbiztos csatlakozók, biztosíték, földvezeték szerepe).

*  Ismeri az elektromos fogyasztók használatára vonatkozó balesetvédelmi szabályokat.

*  Ismeri az elektromos hálózatok kialakítását a lakásokban, épületekben, az elektromos kapcsolási rajzok használatát.

*  Érti a generátor, a motor és a transzformátor működési elvét, gyakorlati hasznát.

*  Ismeri az emberi hangérzékelés fizikai alapjait, a hang mint hullám jellemzőit, keltésének eljárásait.

*  Átlátja a húros hangszerek és a sípok működésének elvét, az ultrahang szerepét a gyógyászatban, ismeri a zajszennyezés fogalmát.

*  Ismeri az emberi szemet mint képalkotó eszközt, a látás mechanizmusát, a gyakori látáshibák (rövid- és távollátás) okát, a szemüveg és a kontaktlencse jellemzőit, a dioptria fogalmát.

*  Ismeri a radioaktív izotópok néhány orvosi alkalmazását (nyomjelzés).

*  Tisztában van az elektromos áram élettani hatásaival, az emberi test áramvezetési tulajdonságaival, az idegi áramvezetés jelenségével.

*  Ismeri a szervezet energiaháztartásának legfontosabb tényezőit, az élelmiszerek energiatartalmának szerepét.

*  Átlátja a gyakran alkalmazott orvosdiagnosztikai vizsgálatok, illetve egyes kezelések fizikai megalapozottságát, felismeri a sarlatán, tudományosan megalapozatlan kezelési módokat.

*  Helyesen használja az út, a pálya és a hely fogalmát, valamint a sebesség, átlagsebesség, pillanatnyi sebesség, gyorsulás, elmozdulás fizikai mennyiségeket a mozgás leírására.

*  Tud számításokat végezni az egyenes vonalú egyenletes mozgás esetében: állandó sebességű mozgások esetén a sebesség ismeretében meghatározza az elmozdulást, a sebesség nagyságának ismeretében a megtett utat, a céltól való távolság ismeretében a megérkezéshez szükséges időt.

*  Ismeri a szabadesés jelenségét, annak leírását, tud esésidőt számolni, mérni, becsapódási sebességet számolni.

*  Egyszerű számításokat végez az állandó gyorsulással mozgó testek esetében.

*  Ismeri a periodikus mozgásokat (ingamozgás, rezgőmozgás) jellemző fizikai mennyiségeket, néhány egyszerű esetben tudja mérni a periódusidőt, megállapítani az azt befolyásoló tényezőket.

*  Ismeri az egyenletes körmozgást leíró fizikai mennyiségeket (pályasugár, kerületi sebesség, fordulatszám, keringési idő, centripetális gyorsulás), azok jelentését, egymással való kapcsolatát.

*  Érti, hogyan alakulnak ki és terjednek a mechanikai hullámok, ismeri a hullámhossz és a terjedési sebesség fogalmát.

*  Egyszerű esetekben kiszámolja a testek lendületének nagyságát, meghatározza irányát.

*  Egyszerűbb esetekben alkalmazza a lendület-megmaradás törvényét, ismeri ennek általános érvényességét.

*  Tisztában van az erő mint fizikai mennyiség jelentésével, mértékegységével, ismeri a newtoni dinamika alaptörvényeit, egyszerűbb esetekben alkalmazza azokat a gyorsulás meghatározására, a korábban megismert mozgások értelmezésére.

*  Egyszerűbb esetekben kiszámolja a mechanikai kölcsönhatásokban fellépő erőket (nehézségi erő, nyomóerő, fonálerő, súlyerő, súrlódási erők, rugóerő), meghatározza az erők eredőjét.

*  Ismeri a bolygók, üstökösök mozgásának jellegzetességeit.

*  Tudja, mit jelentenek a kozmikus sebességek (körsebesség, szökési sebesség).

*  Érti a testek súlya és a tömege közötti különbséget, a súlytalanság állapotát, a gravitációs mező szerepét a gravitációs erő közvetítésében.

*  Érti a tömegvonzás általános törvényét, és azt, hogy a gravitációs erő bármely két test között hat.

*  Ismeri a mechanikai munka fogalmát, kiszámításának módját, mértékegységét, a helyzeti energia, a mozgási energia, a rugalmas energia, a belső energia fogalmát.

*  Konkrét esetekben alkalmazza a munkatételt, a mechanikai energia megmaradásának elvét a mozgás értelmezésére, a sebesség kiszámolására.

*  Néhány egyszerűbb, konkrét esetben (mérleg, libikóka) a forgatónyomatékok meghatározásának segítségével vizsgálja a testek egyensúlyi állapotának feltételeit, összeveti az eredményeket a megfigyelések és kísérletek tapasztalataival.

*  Ismeri a celsius- és az abszolút hőmérsékleti skálát, a gyakorlat szempontjából nevezetes néhány hőmérsékletet, a termikus kölcsönhatás jellemzőit.

*  Gyakorlati példákon keresztül ismeri a hővezetés, hőáramlás és hősugárzás jelenségét, a hőszigetelés lehetőségeit, ezek anyagszerkezeti magyarázatát.

*  Értelmezi az anyag viselkedését hőközlés során, tudja, mit jelent az égéshő, a fűtőérték és a fajhő.

*  Tudja a halmazállapot-változások típusait (párolgás, forrás, lecsapódás, olvadás, fagyás, szublimáció).

*  Tisztában van a halmazállapot-változások energetikai viszonyaival, anyagszerkezeti magyarázatával, tudja, mit jelent az olvadáshő, forráshő, párolgáshő, egyszerű számításokat végez a halmazállapot-változásokat kísérő hőközlés meghatározására.

*  Ismeri a hőtágulás jelenségét, jellemző nagyságrendjét.

*  Ismeri a hőtan első főtételét, és tudja alkalmazni néhány egyszerűbb gyakorlati szituációban (palackba zárt levegő, illetve állandó nyomású levegő melegítése).

*  Tisztában van a megfordítható és nem megfordítható folyamatok közötti különbséggel.

*  Ismeri a víz különleges tulajdonságait (rendhagyó hőtágulás, nagy olvadáshő, forráshő, fajhő), ezek hatását a természetben, illetve mesterséges környezetünkben.

*  Ismeri az időjárás elemeit, a csapadékformákat, a csapadékok kialakulásának fizikai leírását.

*  Ismeri a nyomás, hőmérséklet, páratartalom fogalmát, a levegő mint ideális gáz viselkedésének legfontosabb jellemzőit. egyszerű számításokat végez az állapothatározók megváltozásával kapcsolatban.

*  Tisztában van a repülés elvével, a légellenállás jelenségével.

*  Ismeri a hidrosztatika alapjait, a felhajtóerő fogalmát, hétköznapi példákon keresztül értelmezi a felemelkedés, elmerülés, úszás, lebegés jelenségét, tudja az ezt meghatározó tényezőket, ismeri a jelenségkörre épülő gyakorlati eszközöket.

*  Ismeri az elektrosztatikus alapjelenségeket (dörzselektromosság, töltött testek közötti kölcsönhatás, földelés), ezek gyakorlati alkalmazásait.

*  Átlátja, hogy az elektromos állapot kialakulása a töltések egyenletes eloszlásának megváltozásával van kapcsolatban.

*  Érti coulomb törvényét, egyszerű esetekben alkalmazza elektromos töltéssel rendelkező testek közötti erő meghatározására.

*  Tudja, hogy az elektromos kölcsönhatást az elektromos mező közvetíti.

*  Tudja, hogy az áram a töltött részecskék rendezett mozgása, és ez alapján szemléletes elképzelést alakít ki az elektromos áramról.

*  Gyakorlati szinten ismeri az egyenáramok jellemzőit, a feszültség, áramerősség és ellenállás fogalmát.

*  Érti ohm törvényét, egyszerű esetekben alkalmazza a feszültség, áramerősség, ellenállás meghatározására, tudja, hogy az ellenállás függ a hőmérséklettől.

*  Ki tudja számolni egyenáramú fogyasztók teljesítményét, az általuk felhasznált energiát.

*  Ismeri az egyszerű áramkör és egyszerűbb hálózatok alkotórészeit, felépítését.

*  Értelmezni tud egyszerűbb kapcsolási rajzokat, ismeri kísérleti vizsgálatok alapján a soros és a párhuzamos kapcsolások legfontosabb jellemzőit.

*  Elektromágnes készítése közben megfigyeli és alkalmazza, hogy az elektromos áram mágneses mezőt hoz létre.

*  Megmagyarázza, hogyan működnek az általa megfigyelt egyszerű felépítésű elektromos motorok: a mágneses mező erőt fejt ki az árammal átjárt vezetőre.

*  Ismeri az elektromágneses indukció jelenségének lényegét, fontosabb gyakorlati vonatkozásait, a váltakozó áram fogalmát.

*  Tudja, hogy a fény elektromágneses hullám, és hogy terjedéséhez nem kell közeg.

*  Ismeri az elektromágneses hullámok jellemzőit (frekvencia, hullámhossz, terjedési sebesség), azt, hogy milyen körülmények határozzák meg ezeket. a mennyiségek kapcsolatára vonatkozó egyszerű számításokat végez.

*  Ismeri a színek és a fény frekvenciája közötti kapcsolatot, a fehér fény összetett voltát, a kiegészítő színek fogalmát, a szivárvány színeit.

*  Ismeri a fénytörés és visszaverődés törvényét, megmagyarázza, hogyan alkot képet a síktükör.

*  A fókuszpont fogalmának felhasználásával értelmezi, hogyan térítik el a fényt a domború és homorú tükrök, a domború és homorú lencsék.

*  Ismeri az optikai leképezés fogalmát, a valódi és látszólagos kép közötti különbséget. egyszerű kísérleteket tud végezni tükrökkel és lencsékkel.

*  Megfigyeli a fényelektromos jelenséget, tisztában van annak einstein által kidolgozott magyarázatával, a frekvencia (hullámhossz) és a foton energiája kapcsolatával.

*  Ismeri rutherford szórási kísérletét, mely az atommag felfedezéséhez vezetett.

*  Ismeri az atomról alkotott elképzelések változásait, a rutherford-modellt és bohr-modellt, látja a modellek hiányosságait.

*  Megmagyarázza az elektronmikroszkóp működését az elektron hullámtermészetének segítségével.

*  Átlátja, hogyan használják a vonalas színképet az anyagvizsgálat során.

*  Ismeri az atommag felépítését, a nukleonok típusait, az izotóp fogalmát, a nukleáris kölcsönhatás jellemzőit.

*  Átlátja, hogy a maghasadás és magfúzió miért alkalmas energiatermelésre, ismeri a gyakorlati megvalósulásuk lehetőségeit, az atomerőművek működésének alapelvét, a csillagok energiatermelésének lényegét.

*  Ismeri a radioaktív sugárzások típusait, az alfa-, béta- és gamma-sugárzások leírását és tulajdonságait.

*  Ismeri a felezési idő, aktivitás fogalmát, a sugárvédelem lehetőségeit.

*  Megvizsgálja a naprendszer bolygóin és holdjain uralkodó, a földétől eltérő fizikai környezet legjellemzőbb példáit, azonosítja ezen eltérések okát, a legfontosabb esetekben megmutatja, hogyan érvényesülnek a fizika törvényei a föld és a hold mozgása során.

*  Szabad szemmel vagy távcsővel megfigyeli a holdat, a hold felszínének legfontosabb jellemzőit, a holdfogyatkozás jelenségét, a látottakat fizikai ismeretei alapján értelmezi.

*  Ismeri a nap, mint csillag legfontosabb fizikai tulajdonságait, a nap várható jövőjét, a csillagok lehetséges fejlődési folyamatait.

*  Átlátja és szemlélteti a természetre jellemző fizikai mennyiségek nagyságrendjeit (atommag, élőlények, naprendszer, univerzum).

*  A legegyszerűbb esetekben azonosítja az alapvető fizikai kölcsönhatások és törvények szerepét a világegyetem felépítésében és időbeli változásaiban.

*  Használ helymeghatározó szoftvereket, a közeli és távoli környezetünket leíró adatbázisokat, szoftvereket.

*  A vizsgált fizikai jelenségeket, kísérleteket bemutató animációkat, videókat keres és értelmez.

*  Ismer magyar és idegen nyelvű megbízható fizikai tárgyú honlapokat.

*  Készségszinten alkalmazza a különböző kommunikációs eszközöket, illetve az internetet a főként magyar, illetve idegen nyelvű, fizikai tárgyú tartalmak keresésére.

*  Fizikai szövegben, videóban el tudja különíteni a számára világos, valamint nem érthető, további magyarázatra szoruló részeket.

*  Az interneten talált tartalmakat több forrásból is ellenőrzi.

*  A forrásokból gyűjtött információkat számítógépes prezentációban mutatja be.

*  Az egyszerű vizsgálatok eredményeinek, az elemzések, illetve a következtetések bemutatására prezentációt készít.

*  A projektfeladatok megoldása során önállóan, illetve a csoporttagokkal közösen különböző médiatartalmakat, prezentációkat, rövidebb-hosszabb szöveges produktumokat hoz létre a tapasztalatok, eredmények, elemzések, illetve következtetések bemutatására.

*  A vizsgálatok során kinyert adatokat egyszerű táblázatkezelő szoftver segítségével elemzi, az adatokat grafikonok segítségével értelmezi.

*  Használ mérésre, adatelemzésre, folyamatelemzésre alkalmas összetett szoftvereket (például hang és mozgókép kezelésére alkalmas programokat).

## Földrajz 
### 9-10. évfolyamon
*  Tudatosan és kritikusan használja a földrajzi tartalmú nyomtatott és elektronikus információforrásokat a tanulásban és tudása önálló bővítésekor.

*  Ismeretei alapján biztonsággal tájékozódik a valós és a digitális eszközök által közvetített virtuális földrajzi térben, földrajzi tartalmú adatokban, a különböző típusú térképeken.

*  Képes összetettebb földrajzi tartalmú szövegek értelmezésére.

*  Adott természeti, társadalmi-gazdasági témához kapcsolódóan írásbeli vagy szóbeli beszámolót készít, prezentációt állít össze.

*  Összetettebb földrajzi számítási feladatokat megold, az eredmények alapján következtetéseket fogalmaz meg.

*  Véleményt alkot aktuális társadalmi-gazdasági és környezeti kérdésekben, véleménye alátámasztására logikus érveket fogalmaz meg.

*  Földrajzi tartalmú projektfeladatokat valósít meg társaival.

*  Elkötelezett a természeti és a kulturális értékek, a kulturális sokszínűség megőrzése iránt.

*  Döntéseit a környezeti szempontok figyelembevételével mérlegeli, felelős fogyasztói magatartást tanúsít.

*  Nyitott a különböző szintű pénzügyi folyamatok és összefüggések megismerése iránt.

*  Alkalmazza a más tantárgyak tanulása során megszerzett ismereteit földrajzi problémák megoldása során.

*  Földrajzi tartalmú adatok, információk alapján következtetéseket von le, tendenciákat ismer fel, és várható következményeket (prognózist) fogalmaz meg.

*  Földrajzi megfigyelést, vizsgálatot, kísérletet tervez és valósít meg, az eredményeket értelmezi.

*  Feltárja a földrajzi folyamatok, jelenségek közötti hasonlóságokat és eltéréseket, különböző szempontok alapján rendszerezi azokat.

*  Megkülönbözteti a tényeket a véleményektől, adatokat, információkat kritikusan szemlél.

*  Önálló, érvekkel alátámasztott véleményt fogalmaz meg földrajzi kérdésekben.

*  Céljainak megfelelően kiválasztja és önállóan használja a hagyományos, illetve digitális információforrásokat, adatbázisokat.

*  Földrajzi tartalmú szövegek alapján lényegkiemelő összegzést készít szóban és írásban.

*  Digitális eszközök segítségével bemutat és értelmez földrajzi jelenségeket, folyamatokat, törvényszerűségeket, összefüggéseket.

*  Adatokat rendszerez és ábrázol hagyományos és digitális eszközök segítségével.

*  Megadott szempontok alapján alapvető földrajzi-földtani folyamatokkal, tájakkal, országokkal kapcsolatos földrajzi tartalmú szövegeket, képi információhordozókat dolgoz fel.

*  A közvetlen környezetének földrajzi megismerésére terepvizsgálódást tervez és kivitelez.

*  Tudatosan használja a földrajzi és a kozmikus térben való tájékozódást segítő hagyományos és digitális eszközöket, ismeri a légi- és űrfelvételek sajátosságait, alkalmazási területeit.

*  Képes problémaközpontú feladatok megoldására, környezeti változások összehasonlító elemzésére térképek és légi- vagy űrfelvételek párhuzamos használatával.

*  Térszemlélettel rendelkezik a csillagászati és a földrajzi térben.

*  Érti a világegyetem tér- és időbeli léptékeit, elhelyezi a földet a világegyetemben és a naprendszerben.

*  Ismeri a föld, a hold és a bolygók jellemzőit, mozgásait és ezek következményeit, összefüggéseit.

*  Értelmezi a nap és a naprendszer jelenségeit, folyamatait, azok földi hatásait.

*  Egyszerű csillagászati és időszámítással kapcsolatos feladatokat, számításokat végez.

*  Ismeri a föld felépítésének törvényszerűségeit.

*  Összefüggéseiben mutatja be a lemeztektonika és az azt kísérő jelenségek (földrengések, vulkanizmus, hegységképződés) kapcsolatát, térbeliségét, illetve magyarázza a kőzetlemez-mozgások lokális és az adott helyen túlmutató globális hatásait.

*  Felismeri a történelmi és a földtörténeti idő eltérő nagyságrendjét, ismeri a geoszférák fejlődésének időbeli szakaszait, meghatározó jelentőségű eseményeit.

*  Párhuzamot tud vonni a jelenlegi és múltbeli földrajzi folyamatok között.

*  Felismeri az alapvető ásványokat és kőzeteket, tud példákat említeni azok gazdasági és mindennapi életben való hasznosítására.

*  Ismeri a kőzetburok folyamataihoz kapcsolódó földtani veszélyek okait, következményeit, tér- és időbeli jellemzőit, illetve elemzi az alkalmazkodási, kármegelőzési lehetőségeket.

*  Érti a különböző kőzettani felépítésű területek eltérő környezeti érzékenysége, terhelhetősége közti összefüggéseket.

*  Ismeri a légkör szerkezetét, fizikai és kémiai jellemzőit, magyarázza az ezekben bekövetkező változások mindennapi életre gyakorolt hatását.

*  Összefüggéseiben mutatja be a légköri folyamatokat és jelenségeket, illetve összekapcsolja ezeket az időjárás alakulásával.

*  Tudja az időjárási térképeket és előrejelzéseket értelmezni egyszerű prognózisok készítésére.

*  Felismeri a szélsőséges időjárási helyzeteket és tud a helyzetnek megfelelően cselekedni.

*  A légkör globális változásaival foglalkozó forrásokat kritikusan elemzi, érveken alapuló véleményt fogalmaz meg a témával összefüggésben.

*  Megnevezi a légkör legfőbb szennyező forrásait és a szennyeződés következményeit, érti a lokálisan ható légszennyező folyamatok globális következményeit.

*  Magyarázza az éghajlatváltozás okait, valamint helyi, regionális, globális következményeit.

*  Ismeri a felszíni és felszín alatti vizek főbb típusait, azok jellemzőit, mennyiségi és minőségi viszonyaikat befolyásoló tényezőket, a víztípusok közötti összefüggéseket.

*  Igazolja a felszíni és felszín alatti vizek egyre fontosabbá váló erőforrásszerepét és gazdasági vonatkozásait, bizonyítja a víz társadalmi folyamatokat befolyásoló természetét, védelmének szükségességét.

*  Ismeri a vízburokkal kapcsolatos környezeti veszélyek okait, és reálisan számol a várható következményekkel.

*  Tudatában van a személyes szerepvállalások értékének a globális vízgazdálkodás és éghajlatváltozás rendszerében.

*  Összefüggéseiben, kölcsönhatásaiban mutatja be a földrajzi övezetesség rendszerének egyes elemeit, a természeti jellemzők társadalmi-gazdasági vonatkozásait.

*  Összefüggéseiben mutatja be a talajképződés folyamatát, tájékozott a talajok gazdasági jelentőségével kapcsolatos kérdésekben, ismeri magyarország fontosabb talajtípusait.

*  Bemutatja a felszínformálás többtényezős összefüggéseit, ismeri és felismeri a különböző felszínformáló folyamatokhoz (szél, víz, jég) és kőzettípusokhoz kapcsolódóan kialakuló, felszíni és felszín alatti formakincset.

*  Érti az ember környezet átalakító szerepét, ember és környezete kapcsolatrendszerét, illetve példák alapján igazolja az egyes geoszférák folyamatainak, jelenségeinek gazdasági következményeit, összefüggéseit.

*  Bemutatja a népességszám-változás időbeli és területi különbségeit, ismerteti okait és következményeit, összefüggését a fiatalodó és az öregedő társadalmak jellemző folyamataival és problémáival.

*  Különböző népességi, társadalmi és kulturális jellemzők alapján bemutat egy kontinenst, országot, országcsoportot.

*  Különböző szempontok alapján csoportosítja és jellemzi az egyes településtípusokat, bemutatja szerepkörük és szerkezetük változásait.

*  Érti és követi a lakóhelye környékén zajló település- és területfejlődési, valamint demográfiai folyamatokat.

*  Ismerteti a gazdaság szerveződését befolyásoló telepítő tényezők szerepének átalakulását, bemutatja az egyes gazdasági ágazatok jellemzőit, értelmezi a gazdasági szerkezetváltás folyamatát.

*  Értelmezi és értékeli a társadalmi-gazdasági fejlettség összehasonlítására alkalmas mutatók adatait, a társadalmi-gazdasági fejlettség területi különbségeit a föld különböző térségeiben.

*  Értékeli az eltérő adottságok, erőforrások szerepét a társadalmi-gazdasági fejlődésben.

*  Modellezi a piacgazdaság működését.

*  Megnevezi és értékeli a gazdasági integrációk és a regionális együttműködések kialakulásában szerepet játszó tényezőket.

*  Ismerteti a világpolitika és a világgazdaság működését befolyásoló nemzetközi szervezetek, együttműködések legfontosabb jellemzőit.

*  Értelmezi a globalizáció fogalmát, a globális világ kialakulásának és működésének feltételeit, jellemző vonásait.

*  Példák alapján bemutatja a globalizáció társadalmi-gazdasági és környezeti következményeit, mindennapi életünkre gyakorolt hatását.

*  Megnevezi a világgazdaság működése szempontjából tipikus térségeket, országokat.

*  Összehasonlítja az európai, ázsiai és amerikai erőterek gazdaságilag meghatározó jelentőségű országainak, országcsoportjainak szerepét a globális világban.

*  Összefüggéseiben mutatja be a perifériatérség társadalmi-gazdasági fejlődésének jellemző vonásait, a felzárkózás nehézségeit.

*  Ismerteti az európai unió működésének földrajzi alapjait, példák segítségével bemutatja az európai unión belüli társadalmi-gazdasági fejlettségbeli különbségeket, és megnevezi a felzárkózást segítő eszközöket.

*  Példák alapján jellemzi és értékeli magyarország társadalmi-gazdasági szerepét annak szűkebb és tágabb nemzetközi környezetében, az európai unióban.

*  Bemutatja a területi fejlettségi különbségek okait és következményeit magyarországon, megfogalmazza a felzárkózás lehetőségeit.

*  Értékeli hazánk környezeti állapotát, megnevezi jelentősebb környezeti problémáit.

*  Magyarázza a monetáris világ működésének alapvető fogalmait, folyamatait és azok összefüggéseit, ismer nemzetközi pénzügyi szervezeteket.

*  Bemutatja a működőtőke- és a pénztőkeáramlás sajátos vonásait, magyarázza eltérésük okait.

*  Pénzügyi döntéshelyzeteket, aktuális pénzügyi folyamatokat értelmez és megfogalmazza a lehetséges következményeket.

*  Pénzügyi lehetőségeit mérlegelve egyszerű költségvetést készít, értékeli a hitelfelvétel előnyeit és kockázatait.

*  Alkalmazza megszerzett ismereteit pénzügyi döntéseiben, belátja a körültekintő, felelős pénzügyi tervezés és döntéshozatal fontosságát.

*  Felismeri és azonosítja a földrajzi tartalmú természeti, társadalmi-gazdasági és környezeti problémákat, megnevezi kialakulásuk okait, és javaslatokat fogalmaz meg megoldásukra.

*  Rendszerezi a geoszférákat ért környezetkárosító hatásokat, bemutatja a folyamatok kölcsönhatásait.

*  Példákkal igazolja a természetkárosítás és a természeti, illetve környezeti katasztrófák társadalmi következményeit, a környezetkárosodás életkörülményekre, életminőségre gyakorolt hatását, a lokális szennyeződés globális következményeit.

*  Globális problémákhoz vezető, földünkön egy időben jelenlévő, különböző természeti és társadalmi-gazdasági eredetű problémákat elemez, feltárja azok összefüggéseit, bemutatja mérséklésük lehetséges módjait és azok nehézségeit.

*  Megfogalmazza az energiahatékony, nyersanyag-takarékos, illetve „zöld" gazdálkodás lényegét, valamint példákat nevez meg a környezeti szempontok érvényesíthetőségére a termelésben és a fogyasztásban.

*  Megkülönbözteti a fogyasztói társadalom és a tudatos fogyasztói közösség jellemzőit.

*  A lakóhely adottságaiból kiindulva értelmezi a fenntartható fejlődés társadalmi, természeti, gazdasági, környezetvédelmi kihívásait.

*  Megnevez a környezet védelmében, illetve humanitárius céllal tevékenykedő hazai és nemzetközi szervezeteket, példákat említ azok tevékenységére, belátja és igazolja a nemzetközi összefogás szükségességét.

*  Értelmezi a fenntartható gazdaság, a fenntartható gazdálkodás fogalmát, érveket fogalmaz meg a fenntarthatóságot szem előtt tartó gazdaság, illetve gazdálkodás fontossága mellett.

*  Bemutatja az egyén társadalmi szerepvállalásának lehetőségeit, a tevékeny közreműködés példáit a környezet védelme érdekében, illetve érvényesíti saját döntéseiben a környezeti szempontokat.

## Hon- és népismeret 
### 6. évfolyamon
*  Képessé válik a nemzedékek közötti párbeszédre.

*  Megismeri magyarország és a kárpát-medence kulturális hagyományait, tiszteletben tartja más kultúrák értékeit.

*  Ismeri és adekvát módon használja az ismeretkör tartalmi kulcsfogalmait.

*  Az önálló ismeretszerzés során alkalmazni tudja az interjúkészítés, a forráselemzés módszerét.

*  Képes az együttműködésre csoportmunkában, alkalmazza a tevékenységek lezárásakor az önértékelést és a társak értékelését.

*  Megismeri a közvetlen környezetében található helyi értékeket, felhasználva a digitálisan elérhető adatbázisokat is.

*  Megbecsüli szűkebb lakókörnyezetének épített örökségét, természeti értékeit, helyi hagyományait.

*  Megéli a közösséghez tartozást, nemzeti önazonosságát, kialakul benne a haza iránti szeretet és elköteleződés.

*  Megtapasztalja a legszűkebb közösséghez, a családhoz, a lokális közösséghez tartozás érzését.

*  Nyitottá válik a hagyományos családi és közösségi értékek befogadására.

*  Felismeri a néphagyományok közösségformáló, közösségmegtartó erejét.

*  Tiszteletben tartja más kultúrák értékeit.

*  Érdeklődő attitűdjével erősíti a nemzedékek közötti párbeszédet.

*  Megbecsüli és megismeri az idősebb családtagok tudását, tapasztalatait, nyitott a korábbi nemzedékek életmódjának, normarendszerének megismerésére.

*  Megérti a természeti környezet meghatározó szerepét a más tájakon élő emberek életmódbeli különbségében.

*  Belátja, hogy a természet kínálta lehetőségek felhasználásának elsődleges szempontja a szükségletek kielégítése, a mértéktartás alapelvének követése.

*  Megismeri a gazdálkodó életmódra jellemző újrahasznosítás elvét, és saját életében is megpróbálja alkalmazni.

*  Önálló néprajzi gyűjtés során, digitális archívumok, írásos dokumentumok tanulmányozásával ismereteket szerez családja, lakóhelye múltjáról, hagyományairól.

*  Szöveges és képi források, digitalizált archívumok elemzése, feldolgozása alapján bővíti tudását.

*  Felismeri a csoportban elfoglalt helyét és szerepét, törekszik a személyiségének, készségeinek és képességeinek, érdeklődési körének legjobban megfelelő feladatok vállalására.

*  Tevékenyen részt vesz a csoportmunkában zajló együttműködő alkotási folyamatban, digitális források közös elemzésében.

*  Meghatározott helyzetekben önálló véleményt tud alkotni, a társak véleményének meghallgatását követően álláspontját felül tudja bírálni, döntéseit át tudja értékelni.

## Komplex természettudomány
### 9. évfolyamon
* Ismeri a tudományos kutatás alapszabályait és azokat alkalmazza
* Önálló tudományos kutatást tervez meg és végez el
* Önálló kutatása összeállításakor tudományos modelleket használ
* Tudományos kutatások során elvégzi a megszerzett adatok feldolgozását és értelmezését
* Érti a tudomány szerepét és szükségszerűségét a társadalmi folyamatok alakításában
* Hiteles források felhasználásával egy tudományos probléma kritikus elemzését adja a megszerzett információk alapján
* Elméleti és gyakorlati eszközöket választ és alkalmaz egy adott tudományos probléma ismertetéséhez
* Tudományos kutatási eredményeket érthetően mutat be digitális eszközök segítségével
* Önállóan és kiscsoportban biztonságosan végez természettduományos kísérleteket
* Felismeri a saját és társai által végzett tudományos kísérletek etikai és társadalmi vonatkozásait
* Ismeri és alkalmazza az energia felhasználásának és átalakulásának elméleti és gyakorlati lehetőségeit (energiaáramláson alapuló ökoszisztémák, a föld saját energiaforrásai stb.)
* Felismeri és kísérletei során alkalmazza azt a tudást, hogy az anyag atomi természete határozza meg a fizikai és kémiai tulajdonságokat és azok kölcsönhatásából eredő módosulását. (Példák kísérletekre: kémiai reakciók, molekuláris biológiai, anyagok körforgása a különböző ökoszisztémákban)
* Felismeri és kísérletei során alkalmazza azt a tudást, hogy a természet ismert rendszerei előrejelezhető módon változnak (evolúció, klímaváltozás, földtörténeti korok, Föld felszíni változása, tektonikus mozgások, ember környezeti hatásai stb.)
* Felismeri és kísérletei során alkalmazza azt a tudást, hogy a tárgyak mozgása előrejelezhető (erők, égitestek mozgása, molekuláris mozgások, hangok, fények mozgása)
* Felismeri és kísérletei során alkalmazza azt a tudást, hogy a világ megismerésének egyik alapja a szerveződési szintek és típusok megértése (periódusos rendszer, sjetszintű szerveződések, állat és növényvilág rendszertana, a világegyetem szervező elvei) -
* 
* Ismeri a föld népességgének aktuális kihívásait beleértve azok társadalmi és egészségügyi kockázatait (betegség-megelőzés, járványok, élelmezés)
* Ismeri a hulladékgazdálkodás aktuális kihívásait
* Ismeri az energia fogalmát és az egyéni és társadalmi energiafelhasználás különböző lehetőségeit
* Megkülönbözteti egymástól a természetes és mesterséges anyagokat és felismeri, hogy miként állapítható egy anyag összetétele
* Ismeri a városi és falusi életmód és életterek közötti különbségeket, azok környezetre gyakorolt hatását
* Használja a regionalitás fogalmát és érti annak szerepét a gazdasági folyamatok alakulásában
* Ismeri a levegő és a víz fizikai és kémiai jellemzőit, felismeri ezek élettani hatásait
* Tudja, hogy milyen vízkészletekkel rendelkezik a Föld és azok felhasználásának milyen hatása van a környezeti, ipari, turisztikai és szállítmányozási folyamatokra
* Ismeri a növények tápanyagigényét és fejlődésük alapjait
* Ismeri a vadon élő állatközösségeket fenyegető veszélyeket és azt a kihívást, amit ez az emberekre gyakorol
* Ismeri a különböző kultúrák eltérő gazdasági termelési szokásait (növénytermesztés, állattartás)
* Ismeri a talaj természetét és annak megművelésének különböző formáit
* Ismeri a föld légkörét befolyásoló globális folyamatokat (tengerek és szelek áramlása, klímaváltozás, üvegházhatású gázok)
* Ismeri az emberi táplálkozás során hasznos ételeket, megkülönbözteti a gyógyító és a káros anyagokat, önállóan tud egészséges ételt készíteni
* Ismeri az emberi agy alapvető működési szabályait és az azt befolyásoló tényezőket
* Tisztában van az Univerzum létrejöttének ma ismert elméletével, valamint a Naprendszer kialakulásának folyamatával
* Ismeri a tanulás és az emberi kommunikáció biológiai alapjait
* Ismeri az emberi szervezet egészségét alapvetően befolyásoló tényezőket, a stressz, az öröklött hajlamok és genetikai tulajdonságok, valamint a környezeti hatások szerepét
* Ismeri az emberi szexualitás kulturális, társadalmi és biológiai alapjait. Önálló véleménye van a nemi szerepek fontosságáról, érti a nemi identitás komplex jellegét.
* Ismeri a hálózatok és a hálózatkutatás szerepét modern világunkban, az életközösségek, a sejtszintű gondolkodás és az információs technológiák területén.
* Ismeri a genetikai információ átadásának alapvető szabályait.
## Kémia 
### 9-10. évfolyamon
*  Érdeklődését felkeltse a környezetben zajló fizikai és kémiai változások okai, magyarázata, komplexitása, elméleti háttere iránt.

*  Ismerje a mindennapi életben előforduló alapvető vegyülettípusokat, legyen tisztában alapvető kémiai fogalmakkal, jelenségekkel és reakciókkal, legyen anyagismerete.

*  Önálló ismeretszerzési, illetve összefüggés-felismerési készségei fejlődjenek a kísérletek, laboratóriumi vizsgálatok, nyomtatott vagy digitális információforrások önálló vagy csoportban történő elemzése révén, ami megalapozza az értő, önálló munkavégzés lehetőségét.

*  Problémaorientált, elemző és mérlegelő gondolkodása alakuljon ki, ami nélkülözhetetlen az információs társadalomra jellemző hír- és információdömpingben történő eligazodáshoz, a felelős és tudatos állampolgári szerepvállaláshoz.

*  Tanulmányozza a természetben lejátszódó folyamatokat, valamint átgondolja a várható következményeket, cselekedni képes, a környezetért felelősséggel tenni akaró magatartást alakítson ki, ezzel is hangsúlyozva, hogy az ember egyénként és egy nagyobb közösség részeként egyaránt felelős a természeti környezetéért, annak jövőbeni állapotáért, felismeri és megérti, hogy a környezettudatos, a fenntarthatóságot szem előtt tartó gondolkodás az élhető jövő záloga.

*  A köznapi életben használt vegyi anyagok és az azokkal végzett felelősségteljes munka alapvető ismereteinek elsajátítása mellett tanulja meg a mindennapi életben hasznosítható kémiai ismereteket, és alakuljon ki benne az értő, felelős döntési képesség készsége.

*  A kísérleti megfigyeléstől a modellalkotásig

*  Az anyagi halmazok

*  Atomok, molekulák, ionok

*  Kémiai reakciók

*  Kémia a természetben

*  Kémia a mindennapokban

*  Az anyagok szerkezete és tulajdonságai

*  A kémiai átalakulások

*  A szén egyszerű szerves vegyületei

*  Az életműködések kémiai alapjai

*  Elemek és szervetlen vegyületeik

*  Kémia az ipari termelésben és a mindennapokban

*  Környezeti kémia és környezetvédelem

*  Ismeri a mindennapi életben előforduló fontosabb vegyülettípusokat, tisztában van az élettelen és élő természet legfontosabb kémiai fogalmaival, jelenségeivel és az azokat működtető reakciótípusokkal.

*  Önállóan vagy csoportban el tud végezni egyszerű kémiai kísérleteket és megbecsüli azok várható eredményét.

*  Alkalmazza a természettudományos problémamegoldás lépéseit egyszerű kémiai problémák megoldásában.

*  Képes az analógiás, a korrelatív és a mérlegelő gondolkodásra kémiai kontextusban.

*  Képes számítógépes prezentáció formájában kémiával kapcsolatos eredmények, információk bemutatására, megosztására, a mérési adatok számítógépes feldolgozására, szemléltetésére.

*  Tudja használni a részecskemodellt az anyagok tulajdonságainak és átalakulásainak értelmezésére.

*  Ismeri a kémiának az egyén és a társadalom életében betöltött szerepét.

*  Tisztában van a háztartásban leggyakrabban előforduló anyagok felhasználásának előnyeivel és veszélyeivel, a biztonságos vegyszerhasználat szabályaival.

*  Egyedül vagy csoportban elvégez egyszerű kémiai kísérleteket leírás vagy szóbeli útmutatás alapján és értékeli azok eredményét.

*  Egyedül vagy csoportban elvégez összetettebb, halmazállapot-változással és oldódással kapcsolatos kísérleteket és megbecsüli azok várható eredményét.

*  Kémiai vizsgálatainak tervezése során alkalmazza az analógiás gondolkodás alapjait és használja az „egyszerre csak egy tényezőt változtatunk" elvet.

*  Ismeri az anyagmennyiség és a mól fogalmát, érti bevezetésük szükségességét és egyszerű számításokat végez m, n és m segítségével.

*  Analógiás gondolkodással következtet a szerves vegyület tulajdonságára a funkciós csoportja ismeretében.

*  Használja a fémek redukáló sorát a fémek tulajdonságainak megjóslására, tulajdonságaik alátámasztására.

*  Ismer megbízható magyar és idegen nyelvű internetes forrásokat kémiai tárgyú, elemekkel és vegyületekkel kapcsolatos képek és szövegek gyűjtésére.

*  Magabiztosan használ magyar és idegen nyelvű mobiltelefonos, táblagépes applikációkat kémiai tárgyú információk keresésére.

*  A különböző, megbízható forrásokból gyűjtött információkat számítógépes prezentációban mutatja be.

*  Mobiltelefonos, táblagépes alkalmazások segítségével médiatartalmakat, illetve bemutatókat hoz létre.

*  Alapvető szinten ismeri a természetes környezetet felépítő légkör, vízburok, kőzetburok és élővilág kémiai összetételét.

*  Érti a környezetünk megóvásának a jelentőségét az emberi civilizáció fennmaradása szempontjából.

*  Ismeri a zöld kémia lényegét, a környezetbarát folyamatok előtérbe helyezését, példákat mond újonnan előállított, az emberiség jólétét befolyásoló anyagokra (pl. új gyógyszerek, lebomló műanyagok, intelligens textíliák).

*  Ismeri a legfontosabb környezetszennyező forrásokat és anyagokat, valamint ezeknek az anyagoknak a környezetre gyakorolt hatását.

*  Példákkal szemlélteti egyes kémiai technológiák, illetve bizonyos anyagok felhasználásának környezetre gyakorolt pozitív és negatív hatásait.

*  Ismeri a bioüzemanyagok legfontosabb típusait.

*  Példákkal szemlélteti az emberiség legégetőbb globális problémáit (globális éghajlatváltozás, ózonlyuk, ivóvízkészlet csökkenése, energiaforrások kimerülése), és azok kémiai vonatkozásait.

*  Ismeri az emberiség előtt álló legnagyobb kihívásokat, kiemelten azok kémiai vonatkozásai (energiahordozók, környezetszennyezés, fenntarthatóság, új anyagok előállítása).

*  Példákon keresztül szemlélteti az antropogén tevékenységek kémiai vonatkozású környezeti következményeit.

*  Kiselőadás vagy projektmunka keretében mutatja be a 20. század néhány nagy környezeti katasztrófáját és azt, hogy milyen tanulságokat vonhatunk le azok megismeréséből.

*  Ismeri a légkör kémiai összetételét és az azt alkotó gázok fontosabb tulajdonságait, példákat mond a légkör élőlényekre és élettelen környezetre gyakorolt hatásaira, ismeri a legfontosabb légszennyező gázokat, azok alapvető tulajdonságait, valamint a környezetszennyezésének hatásait, ismeri a légkört érintő globális környezeti problémák kémiai hátterét és ezen problémák megoldására tett erőfeszítéseket.

*  Ismeri a természetes vizek típusait, azok legfontosabb kémiai összetevőit a víz körforgásának és tulajdonságainak tükrében, példákat mond vízszennyező anyagokra, azok forrására, a szennyezés lehetséges következményeire, ismeri a víztisztítás folyamatának alapvető lépéseit, valamint a tiszta ivóvíz előállításának a módját.

*  Érti a kőzetek és a környezeti tényezők talajképző szerepét, példát mond alapvető kőzetekre, ásványokra, érti a különbséget a hulladék és a szemét fogalmi megkülönböztetése között, ismeri a hulladékok típusait, kezelésük módját, környezetre gyakorolt hatásait.

*  Érti a különbséget a tudományos és áltudományos információk között, konkrét példát mond a köznapi életből tudományos és áltudományos ismeretekre, információkra.

*  Ismeri a tudományos megközelítés lényegét (objektivitás, reprodukálhatóság, ellenőrizhetőség, bizonyíthatóság).

*  Látja az áltudományos megközelítés lényegét (feltételezés, szubjektivitás, bizonyítatlanság), felismeri az áltudományosságra utaló legfontosabb jeleket.

*  Ismeri az élelmiszereink legfontosabb összetevőinek, a szénhidrátoknak, a fehérjéknek, valamint a zsíroknak és olajoknak a molekulaszerkezetét és a tulajdonságait, felsorolja a háztartásban megtalálható legfontosabb élelmiszerek tápanyagait, példát mond bizonyos összetevők (fehérjék, redukáló cukrok, keményítő) kimutatására, ismeri a legfontosabb élelmiszeradalék-csoportokat, alapvető szinten értelmezi egy élelmiszer tájékoztató címkéjét.

*  Ismeri a gyógyszer fogalmát és a gyógyszerek fontosabb csoportjait hatásuk alapján, alapvető szinten értelmezi a gyógyszerek mellékelt betegtájékoztatóját.

*  Ismeri a leggyakrabban használt élvezeti szerek (szeszes italok, dohánytermékek, kávé, energiaitalok, drogok) hatóanyagát, ezen szerek használatának veszélyeit, érti az illegális drogok használatával kapcsolatos alapvető problémákat, példákat mond illegális drogokra, ismeri a doppingszer fogalmát, megérti és értékeli a doppingszerekkel kapcsolatos információkat.

*  Ismeri a méreg fogalmának jelentését, érti az anyagok mennyiségének jelentőségét a mérgező hatásuk tekintetében, példát mond növényi, állati és szintetikus mérgekre, ismeri a mérgek szervezetbe jutásának lehetőségeit (tápcsatorna, bőr, tüdő), ismeri és felismeri a különböző anyagok csomagolásán a mérgező anyag piktogramját, képes ezeknek az anyagoknak a felelősségteljes használatára, ismeri a köznapi életben előforduló leggyakoribb mérgeket, mérgezéseket (pl. szén-monoxid, penészgomba-toxinok, gombamérgezések, helytelen égetés során keletkező füst anyagai, drogok, nehézfémek), tudja, hogy a mérgező hatás nem az anyag szintetikus eredetének a következménye.

*  Ismeri a természetben megtalálható legfontosabb nyersanyagokat.

*  Érti az anyagok átalakításának hasznát, valamint konkrét példákat mond vegyipari termékek előállítására.

*  Ismeri a különböző nyersanyagokból előállítható legfontosabb termékeket.

*  Érti, hogy az ipari (vegyipari) termelés során különféle, akár a környezetre vagy szervezetre káros anyagok is keletkezhetnek, amelyek közömbösítése, illetve kezelése fontos feladat.

*  Képes az ismeretein alapuló tudatos vásárlással és tudatos életvitellel a környezetének megóvására.

*  Érti a mészkőalapú építőanyagok kémiai összetételét és átalakulásait (mészkő, égetett mész, oltott mész), ismeri a beton alapvető összetételét, előállítását és felhasználásának lehetőségeit, ismeri a legfontosabb hőszigetelő anyagokat.

*  Érti, hogy a fémek többsége a természetben vegyületek formájában van jelen, ismeri a legfontosabb redukciós eljárásokat (szenes, elektrokémiai redukció), ismeri a legfontosabb ötvözeteket, érti az ötvözetek felhasználásának előnyeit.

*  Ismeri a fosszilis energiahordozók fogalmát és azok legfontosabb képviselőit, érti a kőolaj ipari lepárlásának elvét, ismeri a legfontosabb párlatok nevét, összetételét és felhasználási lehetőségeit, példát mond motorhajtó anyagokra, ismeri a töltőállomásokon kapható üzemanyagok típusait és azok felhasználását.

*  Ismeri a műanyag fogalmát és a műanyagok csoportosításának lehetőségeit eredetük, illetve hővel szemben mutatott viselkedésük alapján, konkrét példákat mond műanyagokra a környezetéből, érti azok felhasználásának előnyeit, ismeri a polimerizáció fogalmát, példát ad monomerekre és polimerekre, ismeri a műanyagok felhasználásának előnyeit és hátrányait, környezetre gyakorolt hatásukat.

*  Ismeri a mosó- és tisztítószerek, valamint a fertőtlenítőszerek fogalmi megkülönböztetését, példát mond a környezetéből gyakran használt mosó-és tisztítószerre és fertőtlenítőszerre, ismeri a szappan összetételét és a szappangyártás módját, ismeri a hypo kémiai összetételét és felhasználási módját, érti a mosószerek mosóaktív komponenseinek (a felületaktív részecskéknek) a mosásban betöltött szerepét.

*  Ismeri a kemény víz és a lágy víz közötti különbséget, érti a kemény víz és egyes mosószerek közötti kölcsönhatás (kicsapódás) folyamatát.

*  Ismeri a mindennapi életben előforduló növényvédő szerek használatának alapvető szabályait, értelmezi a növényvédő szereknek a leírását, felhasználási útmutatóját, példát mond a növényvédő szerekre a múltból és a jelenből (bordói lé, korszerű peszticidek), ismeri ezek hatásának elvi alapjait.

*  Ismeri a legfontosabb (n-, p-, k-tartalmú) műtrágyák kémiai összetételét, előállítását és felhasználásának szükségszerűségét.

*  Ismeri az atom felépítését, az elemi részecskéket, valamint azok jellemzőit, ismeri az izotópok legfontosabb tulajdonságait, érti a radioaktivitás lényegét és példát mond a radioaktív izotópok gyakorlati felhasználására.

*  Ismeri az atom elektronszerkezetének kiépülését a bohr-féle atommodell szintjén, tisztában van a vegyértékelektronok kémiai reakciókban betöltött szerepével.

*  Értelmezi a periódusos rendszer fontosabb adatait (vegyjel, rendszám, relatív atomtömeg), alkalmazza a periódusszám és a (fő)csoportszám jelentését a héjak és a vegyértékelektronok szempontjából, ismeri a periódusos rendszer fontosabb csoportjainak a nevét, és az azokat alkotó elemek vegyjelét.

*  Ismeri a molekulaképződés szabályait, ismeri az elektronegativitás fogalmát, és érti a kötéspolaritás lényegét, a kovalens kötést jellemzi száma és polaritása szerint, megalkotja egyszerű molekulák szerkezeti képletét, ismeri a legalapvetőbb molekulaalakokat (lineáris, síkháromszög, tetraéder, piramis, v-alak), valamint ezek meghatározó szerepét a molekulák polaritása szempontjából.

*  Meghatározza egyszerű molekulák polaritását, és ennek alapján következtet a közöttük kialakuló másodrendű kémiai kötésekre, valamint oldhatósági jellemzőikre, érti, hogy a moláris tömeg és a molekulák között fellépő másodrendű kötések hogyan befolyásolják az olvadás- és forráspontot, ezeket konkrét példákkal támasztja alá.

*  Érti a részecske szerkezete és az anyag fizikai és kémiai tulajdonságai közötti alapvető összefüggéseket.

*  Ismeri az egyszerű ionok atomokból való létrejöttének módját, ezt konkrét példákkal szemlélteti, ismeri a fontosabb összetett ionok molekulákból való képződésének módját, tudja a nevüket, összegképletüket, érti egy ionvegyület képletének a megszerkesztését az azt alkotó ionok képlete alapján, érti az ionrács felépülési elvét, az ionvegyület képletének jelentését, konkrét példák segítségével jellemzi az ionvegyületek fontosabb tulajdonságait.

*  Ismeri a fémek helyét a periódusos rendszerben, érti a fémes kötés kialakulásának és a fémek kristályszerkezetének a lényegét, érti a kapcsolatot a fémek kristályszerkezete és fontosabb tulajdonságai között, konkrét példák segítségével (pl. fe, al, cu) jellemzi a fémes tulajdonságokat, összehasonlításokat végez.

*  Ismeri a fémrács szerkezetét és az ebből adódó alapvető fizikai tulajdonságokat.

*  Ismeri az anyagok csoportosításának a módját a kémiai összetétel alapján, ismeri ezeknek az anyagcsoportoknak a legfontosabb közös tulajdonságait, példákat mond minden csoport képviselőire, tudja, hogy az oldatok a keverékek egy csoportja.

*  Érti a „hasonló a hasonlóban jól oldódik" elvet, ismeri az oldatok töménységével és az oldhatósággal kapcsolatos legfontosabb ismereteket, egyszerű számítási feladatokat old meg az oldatok köréből (tömegszázalék, anyagmennyiség-koncentráció).

*  Adott szempontok alapján összehasonlítja a három halmazállapotba (gáz, folyadék, szilárd) tartozó anyagok általános jellemzőit, ismeri avogadro gáztörvényét és egyszerű számításokat végez gázok térfogatával standard körülmények között, érti a halmazállapot-változások lényegét és energiaváltozását.

*  Érti a fizikai és kémiai változások közötti különbségeket.

*  Ismeri a kémiai reakciók végbemenetelének feltételeit, ismeri, érti és alkalmazza a tömegmegmaradás törvényét a kémiai reakciókra.

*  Ismeri a kémiai reakciók csoportosítását többféle szempont szerint: a reagáló és a képződő anyagok száma, a reakció energiaváltozása, időbeli lefolyása, iránya, a reakcióban részt vevő anyagok halmazállapota szerint.

*  A kémiai reakciókat szimbólumokkal írja le.

*  Konkrét reakciókat termokémiai egyenlettel is felír, érti a termokémiai egyenlet jelentését, ismeri a reakcióhő fogalmát, a reakcióhő ismeretében megadja egy reakció energiaváltozását, energiadiagramot rajzol, értelmez, ismeri a termokémia főtételét és jelentőségét a többlépéses reakciók energiaváltozásának a meghatározásakor.

*  Érti a katalizátorok hatásának az elvi alapjait.

*  Ismer egyirányú és egyensúlyra vezető kémiai reakciókat, érti a dinamikus egyensúly fogalmát, ismeri és alkalmazza az egyensúly eltolásának lehetőségeit le châtelier elve alapján.

*  Ismeri a fontosabb savakat, bázisokat, azok nevét, képletét, brønsted sav-bázis elmélete alapján értelmezi a sav és bázis fogalmát, ismeri a savak és bázisok erősségének és értékűségének a jelentését, konkrét példát mond ezekre a vegyületekre, érti a víz sav-bázis tulajdonságait, ismeri az autoprotolízis jelenségét és a víz autoprotolízisének a termékeit.

*  Konkrét példákon keresztül értelmezi a redoxireakciókat oxigénfelvétel és oxigénleadás alapján, ismeri a redoxireakciók tágabb értelmezését elektronátmenet alapján is, konkrét példákon bemutatja a redoxireakciót, eldönti egy egyszerű redoxireakció egyenlete ismeretében az elektronátadás irányát, az oxidációt és redukciót, megadja az oxidálószert és a redukálószert.

*  Ismeri az anyagok jellemzésének logikus szempontrendszerét: anyagszerkezet -- fizikai tulajdonságok -- kémiai tulajdonságok -- előfordulás -- előállítás -- felhasználás.

*  Ismeri a hidrogén, a halogének, a kalkogének, a nitrogén, a szén és fontosabb vegyületeik fizikai és kémiai sajátságait, különös tekintettel a köznapi életben előforduló anyagokra.

*  Alkalmazza az anyagok jellemzésének szempontjait a hidrogénre, kapcsolatot teremt az anyag szerkezete és tulajdonságai között.

*  Ismeri a halogének képviselőit, jellemzi a klórt, ismeri a hidrogén-klorid és a nátrium-klorid tulajdonságait.

*  Ismeri az oxigént és a vizet, ismeri az ózont, mint az oxigén allotróp módosulatát, ismeri mérgező hatását (szmogban) és uv-elnyelő hatását (ózonpajzsban).

*  Ismeri a ként, a kén-dioxidot és a kénsavat.

*  Ismeri a nitrogént, az ammóniát, a nitrogén-dioxidot és a salétromsavat.

*  Ismeri a vörösfoszfort és a foszforsavat, fontosabb tulajdonságaikat és a foszfor gyufagyártásban betöltött szerepét.

*  Összehasonlítja a gyémánt és a grafit szerkezetét és tulajdonságait, különbséget tesz a természetes és mesterséges szenek között, ismeri a természetes szenek felhasználását, ismeri a koksz és az aktív szén felhasználását, példát mond a szén reakcióira (pl. égés), ismeri a szén oxidjainak (co, co2) a tulajdonságait, élettani hatását, valamint a szénsavat és sóit, a karbonátokat.

*  Ismeri a fontosabb fémek (na, k, mg, ca, al, fe, cu, ag, au, zn) fizikai és kémiai tulajdonságait.

*  Kísérletek tapasztalatainak ismeretében értelmezi a fémek egymáshoz viszonyított reakciókészségét oxigénnel, sósavval, vízzel és más fémionok oldatával, érti a fémek redukáló sorának felépülését, következtet fémek reakciókészségére a sorban elfoglalt helyük alapján.

*  Ismeri a fémek helyét a periódusos rendszerben, megkülönbözteti az alkálifémeket, az alkáliföldfémeket, ismeri a vas, az alumínium, a réz, valamint a nemesfémek legfontosabb tulajdonságait.

*  Ismeri a fémek köznapi szempontból legfontosabb vegyületeit, azok alapvető tulajdonságait (nacl, na2co3, nahco3, na3po4, caco3, ca3(po4)2, al2o3, fe2o3, cuso4).

*  Tisztában van az elektrokémiai áramforrások felépítésével és működésével, ismeri a daniell-elem felépítését és az abban végbemenő folyamatokat, az elem áramtermelését.

*  Érti az elektromos áram és a kémiai reakciók közötti összefüggéseket: a galvánelemek áramtermelésének és az elektrolízisnek a lényegét.

*  Ismeri az elektrolizáló cella felépítését és az elektrolízis lényegét a hidrogén-klorid-oldat grafitelektródos elektrolízise kapcsán, érti, hogy az elektromos áram kémiai reakciók végbemenetelét segíti, példát ad ezek gyakorlati felhasználására (alumíniumgyártás, galvanizálás).

*  Ismer eljárásokat fémek ércekből történő előállítására (vas, alumínium).

*  Ismeri a szerves vegyületeket felépítő organogén elemeket, érti a szerves vegyületek megkülönböztetésének, külön csoportban tárgyalásának az okát, az egyszerűbb szerves vegyületeket szerkezeti képlettel és összegképlettel jelöli.

*  Ismeri a telített szénhidrogének homológ sorának felépülési elvét és fontosabb képviselőiket, ismeri a metán fontosabb tulajdonságait, jellemzi az anyagok szempontrendszere alapján, ismeri a homológ soron belül a forráspont változásának az okát, valamint a szénhidrogének oldhatóságát, ismeri és egy-egy kémiai egyenlettel leírja az égés, a szubsztitúció és a hőbontás folyamatát.

*  Érti az izoméria jelenségét, példákat mond konstitúciós izomerekre.

*  Ismeri a telítetlen szénhidrogének fogalmát, az etén és az acetilén szerkezetét és fontosabb tulajdonságait, ismeri és reakcióegyenletekkel leírja a telítetlen szénhidrogének jellemző reakciótípusait, az égést, az addíciót és a polimerizációt.

*  Ismeri a legegyszerűbb szerves kémiai reakciótípusokat.

*  Felismeri az aromás szerkezetet egy egyszerű vegyületben, ismeri a benzol molekulaszerkezetét és fontosabb tulajdonságait, tudja, hogy számos illékony aromás szénhidrogén mérgező.

*  Példát mond közismert halogéntartalmú szerves vegyületre (pl. kloroform, vinil-klorid, freonok, ddt, tetrafluoretén), és ismeri felhasználásukat.

*  Ismeri, és vegyületek képletében felismeri a legegyszerűbb oxigéntartalmú funkciós csoportokat: a hidroxilcsoportot, az oxocsoportot, az étercsoportot.

*  Ismeri az alkoholok fontosabb képviselőit (metanol, etanol, glikol, glicerin), azok fontosabb tulajdonságait, élettani hatásukat és felhasználásukat.

*  Felismeri az aldehidcsoportot, ismeri a formaldehid tulajdonságait, az aldehidek kimutatásának módját, felismeri a ketocsoportot, ismeri az aceton tulajdonságait, felhasználását.

*  Ismeri, és vegyületek képletében felismeri a karboxilcsoportot és az észtercsoportot, ismeri az egyszerűbb és fontosabb karbonsavak (hangyasav, ecetsav, zsírsavak) szerkezetét és lényeges tulajdonságaikat.

*  Az etil-acetát példáján bemutatja a kis szénatomszámú észterek jellemző tulajdonságait, tudja, hogy a zsírok, az olajok, a foszfatidok, a viaszok egyaránt az észterek csoportjába tartoznak.

*  Szerkezetük alapján felismeri az aminok és az amidok egyszerűbb képviselőit, ismeri az aminocsoportot és az amidcsoportot.

*  Ismeri a biológiai szempontból fontos szerves vegyületek építőelemeit (kémiai összetételét, a nagyobbak alkotó molekuláit).

*  Ismeri a lipid gyűjtőnevet, tudja, hogy ebbe a csoportba hasonló oldhatósági tulajdonságokkal rendelkező vegyületek tartoznak, felsorolja a lipidek legfontosabb képviselőit, felismeri azokat szerkezeti képlet alapján, ismeri a lipidek csoportjába tartozó vegyületek egy-egy fontos szerepét az élő szervezetben.

*  Ismeri a szénhidrátok legalapvetőbb csoportjait, példát mond mindegyik csoportból egy-két képviselőre, ismeri a szőlőcukor képletét, összefüggéseket talál a szőlőcukor szerkezete és tulajdonságai között, ismeri a háztartásban található szénhidrátok besorolását a megfelelő csoportba, valamint köznapi tulajdonságaikat (ízük, oldhatóságuk) és felhasználásukat, összehasonlítja a keményítő és a cellulóz molekulaszerkezetét és tulajdonságait, valamint szerepüket a szervezetben és a táplálékaink között.

*  Tudja, hogy a fehérjék aminosavakból épülnek fel, ismeri az aminosavak általános szerkezetét és azok legfontosabb tulajdonságait, ismeri a fehérjék elsődleges, másodlagos, harmadlagos és negyedleges szerkezetét, érti e fajlagos molekulák szerkezetének a kialakulását, példát mond a fehérjék szervezetben és élelmiszereinkben betöltött szerepére, ismeri a fehérjék kicsapásának módjait és ennek jelentőségét a mérgezések kapcsán.

## Környezetismeret 
### 4. évfolyamon
*  Ismeretei bővítéséhez nyomtatott és digitális forrásokat használ.

*  Megfigyelés, mérés és kísérletezés közben szerzett tapasztalatairól szóban, rajzban, írásban beszámol.

*  Projektmunkában, csoportos tevékenységekben vesz részt.

*  Felismeri a helyi természet- és környezetvédelmi problémákat.

*  Szöveggel, táblázattal és jelekkel adott információkat értelmez.

*  Adott szempontok alapján algoritmus szerint élettelen anyagokon és élőlényeken megfigyeléseket végez.

*  Felismeri az élőlényeken, élettelen dolgokon az érzékelhető tulajdonságokat.

*  Összehasonlítja az élőlényeket és az élettelen anyagokat.

*  Adott szempontok alapján képes élettelen anyagokat összehasonlítani, csoportosítani.

*  Adott szempontok alapján képes élőlényeket összehasonlítani, csoportosítani.

*  Megfigyeléseinek, összehasonlításainak és csoportosításainak tapasztalatait szóban, rajzban, írásban rögzíti, megfogalmazza.

*  Figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat (például olvadás, forrás, fagyás, párolgás, lecsapódás, égés, ütközés).

*  A megfigyelésekhez, összehasonlításokhoz és csoportosításokhoz kapcsolódó ismereteit felidézi.

*  Felismeri az élettelen anyagokon és az élőlényeken a mérhető tulajdonságokat.

*  Algoritmus szerint, előzetes viszonyítás, majd becslés után méréseket végez, becsült és mért eredményeit összehasonlítja.

*  A méréshez megválasztja az alkalmi vagy szabvány mérőeszközt, mértékegységet.

*  Az adott alkalmi vagy szabvány mérőeszközt megfelelően használja.

*  A mérésekhez kapcsolódó ismereteit felidézi.

*  A méréseket és azok tapasztalatait a mindennapi életben alkalmazza.

*  Tanítói segítséggel egyszerű kísérleteket végez.

*  A kísérletezés elemi lépéseit annak algoritmusa szerint megvalósítja.

*  A tanító által felvetett problémákkal kapcsolatosan hipotézist fogalmaz meg, a vizsgálatok eredményét összeveti hipotézisével.

*  Az adott kísérlethez választott eszközöket megfelelően használja.

*  A kísérletek tapasztalatait a mindennapi életben alkalmazza.

*  Figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat (például a természet változásai, időjárási elemek).

*  A vizsgálatok tapasztalatait megfogalmazza, rajzban, írásban rögzíti.

*  A feladatvégzés során társaival együttműködik.

*  Megfelelően eligazodik az időbeli relációkban, ismeri és használja az életkorának megfelelő időbeli relációs szókincset.

*  Megfelelő sorrendben sorolja fel a napszakokat, a hét napjait, a hónapokat, az évszakokat, ismeri ezek időtartamát, relációit.

*  Felismeri a napszakok, évszakok változásai, valamint a föld mozgásai közötti összefüggéseket.

*  Az évszakokra vonatkozó megfigyeléseket végez, tapasztalatait rögzíti, és az adatokból következtetéseket von le.

*  Figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat (például víz körforgása, emberi élet szakaszai, növények csírázása, növekedése).

*  Naptárt használ, időintervallumokat számol, adott eseményeket időrend szerint sorba rendez.

*  Napirendet tervez és használ.

*  Analóg és digitális óráról leolvassa a pontos időt.

*  Ismeri és használja az életkorának megfelelő térbeli relációs szókincset.

*  Megnevezi és iránytű segítségével megállapítja a fő- és mellékvilágtájakat.

*  Irányokat ad meg viszonyítással.

*  Megkülönböztet néhány térképfajtát: domborzati, közigazgatási, turista, autós.

*  Felismeri és használja az alapvető térképjeleket: felszínformák, vizek, települések, útvonalak, államhatárok.

*  A tanterméről, otthona valamely helyiségéről egyszerű alaprajzot készít és leolvas.

*  Tájékozódik az iskola környékéről és településéről készített térképvázlattal és térképpel, az iskola környezetéről egyszerű térképvázlatot készít.

*  Felismeri a különböző domborzati formákat, felszíni vizeket, ismeri jellemzőiket, ezeket terepasztalon vagy saját készítésű modellen előállítja.

*  Felismeri lakóhelyének jellegzetes felszínformáit.

*  Domborzati térképen felismeri a felszínformák és vizek jelölését.

*  Térkép segítségével megnevezi hazánk szomszédos országait, megyéit, saját megyéjét, megyeszékhelyét, környezetének nagyobb településeit, hazánk fővárosát, és ezeket megtalálja a térképen is.

*  Térkép segítségével megnevezi magyarország jellemző felszínformáit (síkság, hegy, hegység, domb, dombság), vizeit (patak, folyó, tó), ezeket terepasztalon vagy saját készítésű modellen előállítja.

*  Térkép segítségével megmutatja hazánk nagytájait, felismeri azok jellemző felszínformáit.

*  Felismeri, megnevezi és megfigyeli az életfeltételeket, életjelenségeket.

*  Felismeri, megnevezi és megfigyeli egy konkrét növény választott részeit, algoritmus alapján a részek tulajdonságait, megfogalmazza, mi a növényi részek szerepe a növény életében.

*  Növényt ültet és gondoz, megfigyeli a fejlődését, tapasztalatait rajzos formában rögzíti.

*  Felismeri, megnevezi és megfigyeli egy konkrét állat választott részeit, algoritmus alapján a részek tulajdonságait, megfogalmazza, mi a megismert rész szerepe az állat életében.

*  Algoritmus alapján megfigyeli és összehasonlítja a saját lakókörnyezetében fellelhető, jellemző növények és állatok jellemzőit, a megfigyelt tulajdonságok alapján csoportokba rendezi azokat.

*  Ismeri a lakóhelyéhez közeli életközösségek (erdő, mező-rét, víz-vízpart) főbb jellemzőit.

*  Algoritmus alapján megfigyeli és összehasonlítja hazánk természetes és mesterséges élőhelyein, életközösségeiben élő növények és állatok jellemzőit, a megfigyelt jellemzőik alapján csoportokba rendezi azokat.

*  Megnevezi a megismert életközösségekre jellemző élőlényeket, használja az életközösségekhez kapcsolódó kifejezéseket.

*  Konkrét példán keresztül megfigyeli és felismeri az élőhely, életmód és testfelépítés kapcsolatát.

*  Felismeri a lakóhelyéhez közeli életközösségek és az ott élő élőlények közötti különbségeket (pl. természetes -- mesterséges életközösség, erdő -- mező, rét -- víz, vízpart -- park, díszkert -- zöldséges, gyümölcsöskert esetében).

*  Megfigyeléseit mérésekkel (például időjárási elemek, testméret), modellezéssel, egyszerű kísérletek végzésével (például láb- és csőrtípusok) egészíti ki.

*  Felismeri, hogy az egyes fajok környezeti igényei eltérőek.

*  Felismeri az egyes életközösségek növényei és állatai közötti jellegzetes kapcsolatokat.

*  Példákkal mutatja be az emberi tevékenység természeti környezetre gyakorolt hatását, felismeri a természetvédelem jelentőségét.

*  Egyéni és közösségi környezetvédelmi cselekvési formákat ismer meg és gyakorol közvetlen környezetében.

*  Megnevezi az ember életszakaszait.

*  Ismeri az emberi szervezet fő életfolyamatait.

*  Megnevezi az emberi test részeit, fő szerveit, ismeri ezek működését, szerepét.

*  Megnevezi az érzékszerveket és azok szerepét a megismerési folyamatokban.

*  Belátja az érzékszervek védelmének fontosságát, és ismeri ezek eszközeit, módjait.

*  Ismer betegségeket, felismeri a legjellemzőbb betegségtüneteket, a betegségek megelőzésének alapvető módjait.

*  Felismeri az egészséges, gondozott környezet jellemzőit, megfogalmazza, milyen hatással van a környezet az egészségére.

*  Tisztában van az egészséges életmód alapelveivel, összetevőivel, az emberi szervezet egészséges testi és lelki fejlődéséhez szükséges szokásokkal,azokat igyekszik betartani.

*  Felismeri, mely anyagok szennyezhetik környezetünket a mindennapi életben, mely szokások vezetnek környezetünk károsításához, egyéni és közösségi környezetvédelmi cselekvési formákat ismer meg és gyakorol közvetlen környezetében (pl. madárbarát kert, iskolakert kiépítésében, fenntartásában való részvétel, iskolai környezet kialakításában, rendben tartásában való részvétel, települési természet- és környezetvédelmi tevékenységben való részvétel).

*  Elsajátít olyan szokásokat és viselkedésformákat, amelyek a károsítások megelőzésére irányulnak (pl. hulladékminimalizálás -- anyagtakarékosság, újrahasználat és -felhasználás, tömegközlekedés, gyalogos vagy kerékpáros közlekedés előnyben részesítése, energiatakarékosság).

*  Felelősségtudattal rendelkezik a szűkebb, illetve tágabb környezete iránt.

*  Felismeri az élőlényeken, élettelen anyagokon az érzékelhető és mérhető tulajdonságokat.

*  Adott szempontok alapján élettelen anyagokat és élőlényeket összehasonlít, csoportosít.

*  Azonosítja az anyagok halmazállapotait, megnevezi és összehasonlítja azok alapvető jellemzőit.

*  Egyszerű kísérletek során megfigyeli a halmazállapot-változásokat: fagyás, olvadás, forrás, párolgás, lecsapódás.

*  Megismeri és modellezi a víz természetben megtett útját, felismeri a folyamat ciklikus jellegét.

*  Tanítói segítséggel égéssel kapcsolatos egyszerű kísérleteket végez, csoportosítja a megvizsgált éghető és éghetetlen anyagokat.

*  Megfogalmazza a tűz és az égés szerepét az ember életében.

*  Megnevezi az időjárás fő elemeit, időjárási megfigyeléseket tesz, méréseket végez.

*  Megfigyeli a mozgások sokféleségét, csoportosítja a mozgásformákat: hely- és helyzetváltoztató mozgás.

*  Megfigyeli a növények csírázásának és növekedésének feltételeit, ezekre vonatkozóan egyszerű kísérletet végez.

## Magyar nyelv és irodalom 
### 1-4. évfolyamon
*  Az életkorának és egyéni adottságainak megfelelő, hallott és olvasott szövegeket megérti.

*  Felkészülés után tagolt szöveget érthetően és pontosan olvas hangosan.

*  Életkorának megfelelően és adottságaihoz mérten kifejezően, érthetően, az élethelyzethez igazodva kommunikál.

*  Az életkorának és egyéni képességeinek megfelelően alkot szövegeket szóban és írásban.

*  Segítséggel egyéni érdeklődésének megfelelő olvasmányt választ, amelyről beszámol.

*  Érdeklődésének megfelelően, hagyományos és digitális szövegek által bővíti ismereteit.

*  Megfogalmazza saját álláspontját, véleményét.

*  Egyéni sajátosságaihoz mérten törekszik a rendezett írásképre, esztétikus füzetvezetésre.

*  A tanult nyelvi, nyelvtani, helyesírási ismereteket képességeihez mérten alkalmazza.

*  Élményeket és tapasztalatokat szerez változatos irodalmi szövegek megismerésével, olvasásával.

*  Részt vesz a testséma-tudatosságot fejlesztő tevékenységekben (szem-kéz koordináció, térérzékelés, irányok, arányok, jobb-bal oldal összehangolása, testrészek tudatosítása) és érzékelő játékokban.

*  Megérti és használja a tér- és időbeli tájékozódáshoz szükséges szókincset.

*  Észleli, illetve megérti a nyelv alkotóelemeit, hangot, betűt, szótagot, szót, mondatot, szöveget, és azokra válaszokat fogalmaz meg.

*  Beszédlégzése és artikulációja megfelelő; figyelmet fordít a hangok időtartamának helyes ejtésére, a beszéd helyes ritmusára, hangsúlyára, tempójára, az élethelyzetnek megfelelő hangerőválasztásra.

*  A szavakat hangokra, szótagokra bontja.

*  Hangokból, szótagokból szavakat épít.

*  Biztosan ismeri az olvasás jelrendszerét.

*  Felismeri, értelmezi a szövegben a számára ismeretlen szavakat, kifejezéseket; digitális forrásokat is használ.

*  Egyszerű magyarázat, szemléltetés (szóbeli, képi, dramatikus tevékenység) alapján megérti az új kifejezés jelentését.

*  A megismert szavakat, kifejezéseket a nyelvi fejlettségi szintjén alkalmazza.

*  Használ életkorának megfelelő digitális és hagyományos szótárakat.

*  Adottságaihoz mérten, életkorának megfelelően szöveget hangos vagy néma olvasás útján megért.

*  Részt vesz népmesék és műmesék, regék, mondák, történetek közös olvasásában és feldolgozásában.

*  Rövid meséket közösen olvas, megért, feldolgoz.

*  Néma olvasás útján megérti az írott utasításokat, közléseket, kérdéseket, azokra adekvát módon reflektál.

*  Megérti a közösen olvasott rövid szövegeket, részt vesz azok olvasásában, feldolgozásában.

*  Önállóan, képek, grafikai szervezők (kerettörténet, történettérkép, mesetáblázat, karakter-térkép, történetpiramis stb.) segítségével vagy tanítói segédlettel a szöveg terjedelmétől függően összefoglalja a történetet.

*  Értő figyelemmel követi a tanító, illetve társai felolvasását.

*  Felkészülés után tagolt szöveget érthetően olvas hangosan.

*  A szöveg megértését igazoló feladatokat végez.

*  Önállóan, képek vagy tanítói segítség alapján a szöveg terjedelmétől függően kiemeli annak lényeges elemeit, összefoglalja azt.

*  Alkalmaz alapvető olvasási stratégiákat.

*  Az olvasott szöveghez illusztrációt készít, a hiányos illusztrációt kiegészíti, vagy a meglévőt társítja a szöveggel.

*  Az olvasott szövegekben kulcsszavakat azonosít, a főbb szerkezeti egységeket önállóan vagy segítséggel elkülöníti.

*  Egyszerű, játékos formában megismerkedik a szövegek különböző modalitásával, médiumok szövegalkotó sajátosságainak alapjaival.

*  Megérti a szóbeli utasításokat, kérdéseket, az adottságainak és életkorának megfelelő szöveg tartalmát.

*  Mozgósítja a hallott szöveg tartalmával kapcsolatos ismereteit, élményeit, tapasztalatait, és összekapcsolja azokat.

*  Megérti az életkorának megfelelő nyelvi és nem nyelvi üzeneteket, és azokra a kommunikációs helyzetnek megfelelően reflektál.

*  Részt vesz nagymozgást és finommotorikát fejlesztő tevékenységekben, érzékelő játékokban.

*  Tér- és síkbeli tájékozódást fejlesztő feladatokat megold.

*  Saját tempójában elsajátítja az anyanyelvi írás jelrendszerét.

*  Szavakat, szószerkezeteket, 3-4 szavas mondatokat leír megfigyelés, illetve diktálás alapján.

*  Az egyéni sajátosságaihoz mérten olvashatóan ír, törekszik a rendezett írásképre, esztétikus füzetvezetésre.

*  Adottságaihoz mérten, életkorának megfelelően érthetően, az élethelyzethez igazodva kommunikál.

*  Részt vesz a kortársakkal és felnőttekkel való kommunikációban, beszélgetésben, vitában, és alkalmazza a megismert kommunikációs szabályokat.

*  Használja a kapcsolat-felvételi, kapcsolattartási, kapcsolatlezárási formákat: köszönés, kérés, megszólítás, kérdezés; testtartás, testtávolság, tekintettartás, hangsúly, hanglejtés, hangerő, hangszín, megköszönés, elköszönés.

*  Élményeiről segítséggel vagy önállóan beszámol.

*  Megadott szempontok alapján szóban mondatokat és 3-4 mondatos szöveget alkot.

*  Bekapcsolódik párbeszédek, dramatikus helyzetgyakorlatok, szituációs játékok megalkotásába.

*  A tanult verseket, mondókákat, rövidebb szövegeket szöveghűen, érthetően tolmácsolja.

*  A hallás és olvasás alapján megfigyelt szavakat, szószerkezeteket, mondatokat önállóan leírja.

*  Egyéni képességeinek megfelelően alkot szövegeket írásban.

*  Gondolatait, érzelmeit, véleményét a kommunikációs helyzetnek megfelelően, néhány mondatban írásban is megfogalmazza.

*  A szövegalkotáskor törekszik a megismert helyesírási szabályok alkalmazására, meglévő szókincsének aktivizálására.

*  Tanítói segítséggel megadott rímpárokból, különböző témákban 2--4 soros verset alkot.

*  Megadott szempontok alapján rövid mesét ír, kiegészít vagy átalakít.

*  A megismert irodalmi szövegekhez, iskolai eseményekhez plakátot, meghívót, saját programjaihoz meghívót készít hagyományosan és digitálisan.

*  Alapvető hagyományos és digitális kapcsolattartó formákat alkalmaz.

*  Nyitott az irodalmi művek befogadására.

*  Könyvet kölcsönöz a könyvtárból, és azt el is olvassa, élményeit, gondolatait megosztja.

*  Ajánlással, illetve egyéni érdeklődésének és az életkori sajátosságainak megfelelően választott irodalmi alkotást ismer meg.

*  Részt vesz az adott közösség kultúrájának megfelelő gyermekirodalmi mű közös olvasásában, és nyitott annak befogadására.

*  Verbális és vizuális módon vagy dramatikus eszközökkel reflektál a szövegre, megfogalmazza a szöveg alapján benne kialakult képet.

*  Részt vesz népmesék és műmesék közös olvasásában, feldolgozásában.

*  Különböző célú, rövidebb tájékoztató, ismeretterjesztő szövegeket olvas hagyományos és digitális felületen.

*  Ismer és használ az életkorának megfelelő nyomtatott és digitális forrásokat az ismeretei bővítéséhez, rendszerezéséhez.

*  A mesék, történetek szereplőinek cselekedeteiről kérdéseket fogalmaz meg, véleményt alkot.

*  Megfogalmazza, néhány érvvel alátámasztja saját álláspontját; meghallgatja társai véleményét.

*  Különbséget tesz mesés és valószerű történetek között.

*  Megfigyeli és összehasonlítja a történetek tartalmát és a saját élethelyzetét.

*  Részt vesz dramatikus játékokban.

*  A feladatvégzéshez szükséges személyes élményeit, előzetes tudását felidézi.

*  Képzeletét a megértés érdekében mozgósítja.

*  Ismer és alkalmaz néhány alapvető tanulási technikát.

*  Gyakorolja az ismeretfeldolgozás egyszerű technikáit.

*  Információkat, adatokat gyűjt a szövegből, kiemeli a bekezdések lényegét; tanítói segítséggel vagy önállóan megfogalmazza azt.

*  Bővíti a témáról szerzett ismereteit egyéb források feltárásával, gyűjtőmunkával, könyvtárhasználattal, filmek, médiatermékek megismerésével.

*  Írásbeli munkáját segítséggel vagy önállóan ellenőrzi és javítja.

*  Megfigyeli, és tapasztalati úton megkülönbözteti egymástól a magánhangzókat és a mássalhangzókat, valamint időtartamukat.

*  Különbséget tesz az egyjegyű, a kétjegyű és a háromjegyű betűk között.

*  A hangjelölés megismert szabályait jellemzően helyesen alkalmazza a tanult szavakban.

*  A mondatot nagybetűvel kezdi, alkalmazza a mondat hanglejtésének, a beszélő szándékának megfelelő mondatvégi írásjeleket.

*  Biztosan ismeri a kis- és nagybetűs ábécét, azonos és különböző betűkkel kezdődő szavakat betűrendbe sorol; a megismert szabályokat alkalmazza digitális felületen való kereséskor is.

*  Felismeri, jelentésük alapján csoportosítja, és önállóan vagy segítséggel helyesen leírja az élőlények, tárgyak, gondolati dolgok nevét.

*  A több hasonló élőlény, tárgy nevét kis kezdőbetűvel írja.

*  A személyneveket, állatneveket és a lakóhelyhez kötődő helyneveket nagy kezdőbetűvel írja le.

*  Törekszik a tanult helyesírási ismeretek alkalmazására.

*  Kérdésre adott válaszában helyesen toldalékolja a szavakat.

*  Önállóan felismeri és elkülöníti az egytövű ismert szavakban a szótövet és a toldalékot.

*  Biztosan szótagol, alkalmazza az elválasztás szabályait.

*  Helyesen alkalmazza a szóbeli és írásbeli szövegalkotásában az idő kifejezésének nyelvi eszközeit.

*  A kiejtéssel megegyező rövid szavak leírásában követi a helyesírás szabályait.

*  A kiejtéstől eltérő ismert szavakat megfigyelés, szóelemzés alkalmazásával megfelelően leírja.

*  Ellentétes jelentésű és rokon értelmű kifejezéseket gyűjt, azokat a beszédhelyzetnek megfelelően használja az írásbeli és szóbeli szövegalkotásban.

*  Megkülönbözteti a szavak egyes és többes számát.

*  Felismeri és önállóan vagy segítséggel helyesen leírja a tulajdonságot kifejező szavakat és azok fokozott alakjait.

*  Felismeri, önállóan vagy segítséggel helyesen leírja az ismert cselekvést kifejező szavakat.

*  Megkülönbözteti a múltban, jelenben és jövőben zajló cselekvéseket, történéseket.

*  Ismer és ért számos egyszerű közmondást és szólást, szóláshasonlatot, közmondást, találós kérdést, nyelvtörőt, kiszámolót, mondókát.

*  Megérti és használja az ismert állandósult szókapcsolatokat.

*  Különféle módokon megjeleníti az ismert szólások, közmondások jelentését.

*  Élményeket és tapasztalatokat szerez változatos irodalmi szövegtípusok és műfajok -- magyar klasszikus, kortárs magyar alkotások -- megismerésével.

*  Élményeket és tapasztalatokat szerez néhány szövegtípusról és műfajról, szépirodalmi és ismeretközlő szövegről.

*  Részt vesz különböző műfajú és megjelenésű szövegek olvasásában és feldolgozásában.

*  A közös olvasás, szövegfeldolgozás során megismer néhány életkorának megfelelő mesét, elbeszélést.

*  Megtapasztalja az életkorának, érdeklődésének megfelelő szövegek befogadásának és előadásának élményét és örömét.

*  Megfigyeli a költői nyelv sajátosságait; élményeit az általa választott módon megfogalmazza, megjeleníti.

*  Részt vesz ismert szövegek (magyar népi mondókák, kiszámolók, nyelvtörők, népdalok, klasszikus és kortárs magyar gyerekversek, mesék) mozgásos-játékos feldolgozásában, dramatikus elemekkel történő élményszerű megjelenítésében, érzületileg, lelkületileg átérzi azokat.

*  Segítséggel vagy önállóan előad ritmuskísérettel verseket.

*  Olvas és megért rövidebb nép- és műköltészeti alkotásokat, rövidebb epikai műveket, verseket.

*  Megtapasztalja a vershallgatás, a versmondás, a versolvasás örömét és élményét.

*  Érzékeli és átéli a vers ritmusát és hangulatát.

*  A versek hangulatát kifejezi különféle érzékszervi tapasztalatok segítségével (színek, hangok, illatok, tapintási élmények stb.).

*  A tanító vagy társai segítségével, együttműködésével verssorokat, versrészleteket memorizál.

*  Felismeri, indokolja a cím és a szöveg közötti összefüggést, azonosítja a történetekben, elbeszélő költeményekben a helyszínt, a szereplőket, a konfliktust és annak megoldását.

*  Szövegszerűen felidézi kölcsey ferenc: himnusz, vörösmarty mihály: szózat, petőfi sándor: nemzeti dal című verseinek részleteit.

*  Részt vesz rövid mesék, történetek dramatikus, bábos és egyéb vizuális, digitális eszközökkel történő megjelenítésében, saját gondolkodási és nyelvi szintjén megfogalmazza a szöveg hatására benne kialakult képet.

*  Megismer néhány mesét és történetet a magyar és más népek irodalmából.

*  Megismer néhány klasszikus verset a magyar irodalomból.

*  Élményt és tapasztalatot szerez különböző ritmikájú lírai művek megismerésével a kortárs és a klasszikus magyar gyermeklírából és a népköltészeti alkotásokból.

*  Segítséggel, majd önállóan szöveghűen felidéz néhány könnyen tanulható, rövidebb verset, mondókát, versrészletet, prózai és dramatikus szöveget, szövegrészletet.

*  A tanult verseket, mondókákat, rövidebb szövegeket szöveghűen, érthetően tolmácsolja.

*  Részt vesz legalább két hosszabb terjedelmű magyar gyermekirodalmi alkotás feldolgozásában.

*  Jellemző és ismert részletek alapján azonosítja a nemzeti ünnepeken elhangzó költemények részleteit, szerzőjüket megnevezi.

*  Megéli és az általa választott formában megjeleníti a közösséghez tartozás élményét.

*  Megismer a jeles napokhoz, ünnepekhez kapcsolódó szövegeket, dalokat, szokásokat, népi gyermekjátékokat.

*  Megfigyeli az ünnepek, hagyományok éves körforgását.

*  Nyitottá válik a magyarság értékeinek megismerésére, megalapozódik nemzeti identitástudata, történelmi szemlélete.

*  Képes családjából származó közösségi élményeit megfogalmazni, összevetni az iskolai élet adottságaival, a témakört érintő beszélgetésekben aktívan részt venni.

*  Törekszik a világ tapasztalati úton történő megismerésére, értékeinek tudatos megóvására.

*  Ismeri a keresztény, keresztyén ünnepköröket (karácsony, húsvét, pünkösd), jelképeket, nemzeti és állami ünnepeket (március 15., augusztus 20., október 23.), népszokásokat (márton-nap, luca-nap, betlehemezés, húsvéti locsolkodás, pünkösdölés).

*  Ismerkedik régi magyar mesterségekkel, irodalmi művek olvasásával és gyűjtőmunkával.

*  Megismer gyermekirodalmi alkotás alapján készült filmet, médiaterméket.

*  Részt vesz gyerekeknek szóló kiállítások megismerésében, alkotásaival hozzájárul létrehozásukhoz.

*  Megismer a szűkebb környezetéhez kötődő irodalmi és kulturális emlékeket, emlékhelyeket.

*  Megismeri saját lakóhelyének irodalmi és kulturális értékeit.

### 5-8. évfolyamon
*  Elkülöníti a nyelv szerkezeti egységeit, megnevezi a tanult elemeket.

*  Ismeri a magyar hangrendszer főbb jellemzőit és a hangok kapcsolódási szabályait. írásban helyesen jelöli.

*  Funkciójuk alapján felismeri és megnevezi a szóelemeket és szófajokat.

*  A szövegben felismer és funkciójuk alapján azonosít alapvető és gyakori szószerkezeteket (alanyos, határozós, jelzős, tárgyas).

*  Szerkezetük alapján megkülönbözteti az egyszerű és összetett mondatokat.

*  Felismeri és elemzi a főbb szóelemek mondat- és szövegbeli szerepét, törekszik helyes alkalmazásukra.

*  Megfigyeli és elemzi a mondat szórendjét, a szórendi változatok, valamint a környező szöveg kölcsönhatását.

*  Érti és megnevezi a tanult nyelvi egységek szövegbeli szerepét.

*  Felismeri és megnevezi a főbb szóalkotási módokat: szóösszetétel, szóképzés, néhány ritkább szóalkotási mód.

*  Tanári irányítással, néhány szempont alapján összehasonlítja az anyanyelv és a tanult idegen nyelv sajátosságait.

*  Megfigyeli, elkülöníti és funkciót társítva értelmezi a környezetében előforduló nyelvváltozatokat (nyelvjárások, csoportnyelvek, rétegnyelvek).

*  Felismeri és megnevezi a nyelvi és nem nyelvi kommunikáció elemeit.

*  Használja a digitális kommunikáció eszközeit, megnevezi azok főbb jellemző tulajdonságait.

*  Megfigyeli és értelmezi a tömegkommunikáció társadalmat befolyásoló szerepét.

*  Felismeri a kommunikáció főbb zavarait, alkalmaz korrekciós lehetőségeket.

*  Alkalmazza az általa tanult nyelvi, nyelvtani, helyesírási, nyelvhelyességi ismereteket.

*  Ismeri és alkalmazza helyesírásunk alapelveit: kiejtés, szóelemzés, hagyomány, egyszerűsítés.

*  Társai és saját munkájában a tanult formáktól eltérő, gyakran előforduló helyesírási hibákat felismeri és javítja.

*  A tanuló etikusan és kritikusan használja a hagyományos papíralapú, illetve a világhálón található és egyéb digitális adatbázisokat.

*  Elolvassa a kötelező olvasmányokat, és saját örömére is olvas.

*  Megérti az irodalmi mű szövegszerű és elvont jelentéseit.

*  A tanult fogalmakat használva beszámol a megismert műről.

*  Megismeri és elkülöníti a műnemeket, illetve a műnemekhez tartozó főbb műfajokat, felismeri és megnevezi azok poétikai és retorikai jellemzőit.

*  Összekapcsol irodalmi műveket különböző szempontok alapján (téma, műfaj, nyelvi kifejezőeszközök).

*  Elkülöníti az irodalmi művek főbb szerkezeti egységeit.

*  Összehasonlít egy adott irodalmi művet annak adaptációival (film, festmény, stb.).

*  Felismeri, és bemutatja egy adott műfaj főbb jellemzőit.

*  Az irodalmi szövegek befogadása során felismer és értelmez néhány alapvető nyelvi-stilisztikai eszközt: szóképek, alakzatok stb..

*  A megismert epikus művek cselekményét összefoglalja, fordulópontjait önállóan ismerteti.

*  Elkülöníti és jellemzi a fő- és mellékszereplőket, megkülönbözteti a helyszíneket, az idősíkokat, azonosítja az előre- és visszautalásokat.

*  Felismeri és a szövegből vett példával igazolja az elbeszélő és a szereplő nézőpontja közötti eltérést.

*  A szövegből vett idézetekkel támasztja alá, és saját szavaival fogalmazza meg a lírai szöveg hangulati jellemzőit, a befogadás során keletkezett érzéseit és gondolatait.

*  Azonosítja a versben a lírai én különböző megszólalásait, és elkülöníti a mű szerkezeti egységeit.

*  Felismeri a tanult alapvető rímképleteket (páros rím, keresztrím, bokorrím), és azonosítja az olvasott versek ritmikai, hangzásbeli hasonlóságait és különbségeit.

*  Ismer és felismer néhány alapvető lírai műfajt.

*  Szöveghűen, értőn mondja el a memoriterként tanult lírai műveket.

*  Drámai műveket olvas és értelmez, előadásokat, feldolgozásokat megnéz (pl.: mesedráma, vígjáték, ifjúsági regény adaptációja).

*  A nemzeti hagyomány szempontjából meghatározó néhány mű esetén bemutatja a szerzőhöz és a korszakhoz kapcsolódó legfőbb jellemzőket.

*  Megérti az életkorának megfelelő hallott és olvasott szövegeket.

*  Kifejezően tudja olvasni és értelmezni az életkorának megfelelő különböző műfajú és megjelenésű szövegeket. a tanuló felismeri és a tanár segítségével értelmezi a számára ismeretlen kifejezéseket.

*  Felismeri és szükség szerint a tanár segítségével értelmezi a szövegben számára ismeretlen kifejezéseket.

*  A lábjegyzetek, a digitális és nyomtatott szótárak használatával önállóan értelmezi az olvasott szöveget.

*  A pedagógus irányításával kiválasztja a rendelkezésre álló digitális forrásokból a megfelelő információkat.

*  Különbséget tesz a jelentésszerkezetben a szó szerinti és metaforikus értelmezés között.

*  Alkalmazza a különböző olvasási típusokat és szöveg-feldolgozási módszereket.

*  Összekapcsolja ismereteit a szöveg tartalmával, és reflektál azok összefüggéseire.

*  Az olvasott szövegeket szerkezeti egységekre tagolja.

*  Szóbeli vagy képi módszerekkel megfogalmazza, megjeleníti a szöveg alapján kialakult érzéseit, gondolatait.

*  Az életkorának megfelelő szöveg alapján jegyzetet, vázlatot készít.

*  A tanulási tevékenységében hagyományos és digitális forrásokat használ, ezt mérlegelő gondolkodással és etikusan teszi.

*  Érthetően, a kommunikációs helyzetnek megfelelően beszél.

*  Gondolatait, érzelmeit, véleményét a kommunikációs helyzetnek megfelelően, érvekkel alátámasztva fogalmazza meg, és mások véleményét is figyelembe veszi.

*  A tanult szövegeket szöveghűen és mások számára követhetően tolmácsolja.

*  Az általa tanult hagyományos és digitális szövegtípusok megfelelő tartalmi és műfaji követelményeinek megfelelően alkot szövegeket.

*  A szövegalkotás során alkalmazza a tanult helyesírási és szerkesztési szabályokat, használja a hagyományos és a digitális helyesírási szabályzatot és szótárt.

*  Az egyéni sajátosságaihoz mérten tagolt, rendezett, áttekinthető írásképpel, egyértelmű javításokkal alkot szöveget.

*  Tanári segítséggel kreatív szöveget alkot a megismert műhöz kapcsolódóan hagyományos és digitális formában.

*  Egyszerű rímes és rímtelen verset alkot.

*  Életkorának megfelelő irodalmi szövegeket olvas.

*  Érthetően, kifejezően és pontosan olvas.

*  Egy általa elolvasott művet ajánl kortársainak.

*  A tanult szövegeket szöveghűen és mások számára követhetően tolmácsolja.

*  Megfogalmazza vagy társaival együttműködve drámajátékban megjeleníti egy mű megismerése során szerzett tapasztalatait, élményeit.

*  Személyes véleményt alakít ki a szövegek és művek által felvetett problémákról (pl. döntési helyzetek, motivációk, konfliktusok), és véleményét indokolja.

*  Megérti mások álláspontját, elfogadja azt, vagy a sajátja mellett érveket fogalmaz meg.

*  Személyes tapasztalatait összeköti a művekben megismert konfliktusokkal, érzelmi állapotokkal.

*  A feladatvégzés során hatékony közös munkára, együttműködésre törekszik.

### 9-12. évfolyamon
*  Az anyanyelvről szerzett ismereteit alkalmazva képes a kommunikációjában a megfelelő nyelvváltozat kiválasztására, használatára.

*  Felismeri a kommunikáció zavarait, kezelésükre stratégiát dolgoz ki.

*  Felismeri és elemzi a tömegkommunikáció befolyásoló eszközeit, azok céljait és hatásait.

*  Reflektál saját kommunikációjára, szükség esetén változtat azon.

*  Ismeri az anyanyelvét, annak szerkezeti felépítését, nyelvhasználata tudatos és helyes.

*  Ismeri a magyar nyelv hangtanát, alaktanát, szófajtanát, mondattanát, ismeri és alkalmazza a tanult elemzési eljárásokat.

*  Felismeri és megnevezi a magyar és a tanult idegen nyelv közötti hasonlóságokat és eltéréseket.

*  Ismeri a szöveg fogalmát, jellemzőit, szerkezeti sajátosságait, valamint a különféle szövegtípusokat és megjelenésmódokat.

*  Felismeri és alkalmazza a szövegösszetartó grammatikai és jelentésbeli elemeket, szövegépítése arányos és koherens.

*  Ismeri a stílus fogalmát, a stíluselemeket, a stílushatást, a stíluskorszakokat, stílusrétegeket, ismereteit a szöveg befogadása és alkotása során alkalmazza.

*  Szövegelemzéskor felismeri az alakzatokat és a szóképeket, értelmezi azok hatását, szerepét, megnevezi típusaikat.

*  Ismeri a nyelvhasználatban előforduló különféle nyelvváltozatokat (nyelvjárások, csoportnyelvek, rétegnyelvek), összehasonlítja azok főbb jellemzőit.

*  Alkalmazza az általa tanult nyelvi, nyelvtani, helyesírási, nyelvhelyességi ismereteket.

*  A retorikai ismereteit a gyakorlatban is alkalmazza.

*  Ismeri és érti a nyelvrokonság fogalmát, annak kritériumait.

*  Ismeri a magyar nyelv eredetének hipotéziseit, és azok tudományosan megalapozott bizonyítékait.

*  Érti, hogy nyelvünk a történelemben folyamatosan változik, ismeri a magyar nyelvtörténet nagy korszakait, kiemelkedő jelentőségű nyelvemlékeit.

*  Ismeri a magyar nyelv helyesírási, nyelvhelyességi szabályait.

*  Tud helyesen írni, szükség esetén nyomtatott és digitális helyesírási segédleteket használ.

*  Etikusan és kritikusan használja a hagyományos, papír alapú, illetve a világhálón található és egyéb digitális adatbázisokat.

*  Elolvassa a kötelező olvasmányokat, és saját örömére is olvas.

*  Felismeri és elkülöníti a műnemeket, illetve a műnemekhez tartozó műfajokat, megnevezi azok poétikai és retorikai jellemzőit.

*  Megérti, elemzi az irodalmi mű jelentésszerkezetének szintjeit.

*  Értelmezésében felhasználja irodalmi és művészeti, történelmi, művelődéstörténeti ismereteit.

*  Összekapcsolja az irodalmi művek szerkezeti felépítését, nyelvi sajátosságait azok tartalmával és értékszerkezetével.

*  Az irodalmi mű értelmezése során figyelembe veszi a mű keletkezéstörténeti hátterét, a műhöz kapcsolható filozófiai, eszmetörténeti szempontokat is.

*  Összekapcsolja az irodalmi művek szövegének lehetséges értelmezéseit azok társadalmi-történelmi szerepével, jelentőségével.

*  Összekapcsol irodalmi műveket különböző szempontok alapján (motívumok, történelmi, erkölcsi kérdésfelvetések, művek és parafrázisaik).

*  Összehasonlít egy adott irodalmi művet annak adaptációival (film, festmény, zenemű, animáció, stb.), összehasonlításkor figyelembe veszi az adott művészeti ágak jellemző tulajdonságait.

*  Epikai és drámai művekben önállóan értelmezi a cselekményszálak, a szerkezet, az időszerkezet (lineáris, nem lineáris), a helyszínek és a jellemek összefüggéseit.

*  Epikai és drámai művekben rendszerbe foglalja a szereplők viszonyait, valamint összekapcsolja azok motivációját és cselekedeteit.

*  Epikai művekben értelmezi a különböző elbeszélésmódok szerepét (tudatábrázolás, egyenes és függő beszéd, mindentudó és korlátozott elbeszélő stb.).

*  A drámai mű értelmezésében alkalmazza az általa tanult drámaelméleti és drámatörténeti fogalmakat (pl. analitikus és abszurd dráma, epikus színház, elidegenedés).

*  A líra mű értelmezésében alkalmazza az általa tanult líraelméleti és líratörténeti fogalmakat (pl. lírai én, beszédhelyzetek, beszédmódok, ars poetica, szereplíra).

*  A tantárgyhoz kapcsolódó fogalmakkal bemutatja a lírai mű hangulati és hangnemi sajátosságait, hivatkozik a mű verstani felépítésére.

*  Szükség esetén a mű értelmezéséhez felhasználja történeti ismereteit.

*  A mű értelmezésében összekapcsolja a szöveg poétikai tulajdonságait a mű nemzeti hagyományban betöltött szerepével.

*  Tájékozottságot szerez régiója magyar irodalmáról.

*  Tanulmányai során ismereteket szerez a kulturális intézmények (múzeum, könyvtár, színház) és a nyomtatott, illetve digitális formában megjelenő kulturális folyóiratok, adatbázisok működéséről.

*  Különböző megjelenésű, típusú, műfajú, korú és összetettségű szövegeket olvas, értelmez.

*  A különböző olvasási típusokat és a szövegfeldolgozási stratégiákat a szöveg típusának és az olvasás céljának megfelelően választja ki és kapcsolja össze.

*  A megismert szöveg tartalmi és nyelvi minőségéről érvekkel alátámasztott véleményt alkot.

*  Hosszabb terjedelmű szöveg alapján többszintű vázlatot vagy részletes gondolattérképet készít.

*  Azonosítja a szöveg szerkezeti elemeit, és figyelembe veszi azok funkcióit a szöveg értelmezésekor.

*  Egymással összefüggésben értelmezi a szöveg tartalmi elemeit és a hozzá kapcsolódó illusztrációkat, ábrákat.

*  Különböző típusú és célú szövegeket hallás alapján értelmez és megfelelő stratégia alkalmazásával értékel és összehasonlít.

*  Összefüggő szóbeli szöveg (előadás, megbeszélés, vita) alapján önállóan vázlatot készít.

*  Felismeri és értelmezésében figyelembe veszi a hallott és az írott szövegek közötti funkcionális és stiláris különbségeket.

*  Folyamatos és nem folyamatos, hagyományos és digitális szövegeket olvas és értelmez maga által választott releváns szempontok alapján.

*  Feladatai megoldásához önálló kutatómunkát végez nyomtatott és digitális forrásokban, ezek eredményeit szintetizálja.

*  Felismeri és értelmezi a szövegben a kétértelműséget és a félrevezető információt, valamint elemzi és értelmezi a szerző szándékát.

*  Megtalálja a közös és eltérő jellemzőket a hagyományos és a digitális technikával előállított, tárolt szövegek között, és véleményt formál azok sajátosságairól.

*  Törekszik arra, hogy a különböző típusú, stílusú és regiszterű szövegekben megismert, számára új kifejezéseket beépítse szókincsébe, azokat adekvát módon használja.

*  Önállóan értelmezi az ismeretlen kifejezéseket a szövegkörnyezet vagy digitális, illetve nyomtatott segédeszközök használatával.

*  Ismeri a tanult tantárgyak, tudományágak szakszókincsét, azokat a beszédhelyzetnek megfelelően használja.

*  Megadott szempontrendszer alapján szóbeli feleletet készít.

*  Képes eltérő műfajú szóbeli szövegek alkotására: felelet, kiselőadás, hozzászólás, felszólalás.

*  Rendelkezik korának megfelelő retorikai ismeretekkel.

*  Felismeri és megnevezi a szóbeli előadásmód hatáskeltő eszközeit, hatékonyan alkalmazza azokat.

*  Írásbeli és szóbeli nyelvhasználata, stílusa az adott kommunikációs helyzetnek megfelelő. írásképe tagolt, beszéde érthető, artikulált.

*  A tanult szövegtípusoknak megfelelő tartalommal és szerkezettel önállóan alkot különféle írásbeli szövegeket.

*  Az írásbeli szövegalkotáskor alkalmazza a tanult szerkesztési, stilisztikai ismereteket és a helyesírási szabályokat.

*  Érvelő esszét alkot megadott szempontok vagy szövegrészletek alapján.

*  Ismeri, érti és etikusan alkalmazza a hagyományos, digitális és multimédiás szemléltetést.

*  Különböző, a munka világában is használt hivatalos szövegeket alkot hagyományos és digitális felületeken (pl. kérvény, beadvány, nyilatkozat, egyszerű szerződés, meghatalmazás, önéletrajz, motivációs levél).

*  Megadott vagy önállóan kiválasztott szempontok alapján az irodalmi művekről elemző esszét ír.

*  A kötelező olvasmányokat elolvassa, és saját örömére is olvas.

*  Tudatosan keresi a történeti és esztétikai értékekkel rendelkező olvasmányokat, műalkotásokat.

*  Olvasmányai kiválasztásakor figyelembe veszi az alkotások kulturális regiszterét.

*  Társai érdeklődését figyelembe véve ajánl olvasmányokat.

*  Választott olvasmányaira is vonatkoztatja a tanórán megismert kontextusteremtő eljárások tanulságait.

*  Önismeretét irodalmi művek révén fejleszti.

*  Részt vesz irodalmi mű kreatív feldolgozásában, bemutatásában (pl. animáció, dramaturgia, átirat).

*  A környező világ jelenségeiről, szövegekről, műalkotásokról véleményt alkot, és azt érvekkel támasztja alá.

*  Megnyilvánulásaiban, a vitákban alkalmazza az érvelés alapvető szabályait.

*  Vitahelyzetben figyelembe veszi mások álláspontját, a lehetséges ellenérveket is.

*  Feladatai megoldásához önálló kutatómunkát végez nyomtatott és digitális forrásokban, a források tartalmát mérlegelő módon gondolja végig.

*  A feladatokat komplex szempontoknak megfelelően oldja meg, azokat kiegészíti saját szempontjaival.

*  A kommunikációs helyzetnek és a célnak megfelelően tudatosan alkalmazza a beszélt és írott nyelvet, reflektál saját és társai nyelvhasználatára.

## Matematika 
### 1-4. évfolyamon
*  Tudatos megfigyeléseket tesz a környező világ tárgyaira, ezek viszonyára vonatkozóan.

*  Tájékozódik a környező világ mennyiségi és formai világában.

*  Megérti a tanult ismereteket és használja azokat a feladatok megoldása során.

*  A környezetében lévő dolgokat szétválogatja, összehasonlítja és rendszerezi egy-két szempont alapján.

*  Jártas a mérőeszközök használatában, a mérési módszerekben.

*  Helyes képzete van a természetes számokról, érti a számnevek és számjelek épülésének rendjét.

*  Helyesen értelmezi az alapműveleteket tevékenységekkel, szövegekkel, és jártas azok elvégzésében fejben és írásban.

*  Megfigyeli jelenségek matematikai tartalmát, és le tudja ezeket írni számokkal, műveletekkel vagy geometriai alakzatokkal.

*  Életkorának megfelelően eligazodik környezetének térbeli és időbeli viszonyaiban.

*  Érti a korának megfelelő, matematikai tartalmú hallott és olvasott szövegeket.

*  Megszerzett ismereteit digitális eszközökön is alkalmazza.

*  Megkülönböztet, azonosít egyedi konkrét látott, hallott, mozgással, tapintással érzékelhető tárgyakat, dolgokat, helyzeteket, jeleket.

*  Válogatásokat végez saját szempont szerint személyek, tárgyak, dolgok, számok között.

*  Felismeri a mások válogatásában együvé kerülő dolgok közös és a különválogatottak eltérő tulajdonságát.

*  Folytatja a megkezdett válogatást felismert szempont szerint.

*  Személyek, tárgyak, dolgok, szavak, számok közül kiválogatja az adott tulajdonsággal rendelkező összes elemet.

*  Azonosítja a közös tulajdonsággal rendelkező dolgok halmazába nem való elemeket.

*  Megnevezi egy adott tulajdonság szerint ki nem válogatott elemek közös tulajdonságát a tulajdonság tagadásával.

*  Barkochbázik valóságos és elképzelt dolgokkal is, kerüli a felesleges kérdéseket.

*  Halmazábrán is elhelyez elemeket adott címkék szerint.

*  Adott, címkékkel ellátott halmazábrán elhelyezett elemekről eldönti, hogy a megfelelő helyre kerültek-e; a hibás elhelyezést javítja.

*  Talál megfelelő címkéket halmazokba rendezett elemekhez.

*  Megfogalmaz adott halmazra vonatkozó állításokat; értelemszerűen használja a „mindegyik", „nem mindegyik", „van köztük...", „"egyik sem..." és a velük rokon jelentésű szavakat.

*  Két szempontot is figyelembe vesz egyidejűleg.

*  Két meghatározott tulajdonság egyszerre történő figyelembevételével szétválogat adott elemeket: tárgyakat, személyeket, szavakat, számokat, alakzatokat.

*  Felsorol elemeket konkrét halmazok közös részéből.

*  Megfogalmazza a halmazábra egyes részeibe kerülő elemek közös, meghatározó tulajdonságát; helyesen használja a logikai „nem" és a logikai „és" szavakat, valamint velük azonos értelmű kifejezéseket.

*  Megítéli, hogy adott halmazra vonatkozó állítás igaz-e, vagy hamis.

*  Keresi az okát annak, ha a halmazábra valamelyik részébe nem kerülhet egyetlen elem sem.

*  Hiányos állításokat igazzá tevő elemeket válogat megadott alaphalmazból.

*  Összehasonlít véges halmazokat az elemek száma szerint.

*  Ismeri két halmaz elemeinek kölcsönösen egyértelmű megfeleltetését (párosítását) az elemszámok szerinti összehasonlításra.

*  Helyesen alkalmazza a feladatokban a több, kevesebb, ugyanannyi fogalmakat 10 000-es számkörben.

*  Helyesen érti és alkalmazza a feladatokban a „valamennyivel" több, kevesebb fogalmakat.

*  Adott elemeket elrendez választott és megadott szempont szerint is.

*  Sorba rendezett elemek közé elhelyez további elemeket a felismert szempont szerint.

*  Két, három szempont szerint elrendez adott elemeket többféleképpen is; segédeszközként használja a táblázatos elrendezést és a fadiagramot.

*  Megkeresi egyszerű esetekben a két, három feltételnek megfelelő összes elemet, alkotást.

*  Megfogalmazza a rendezés felismert szempontjait.

*  Megkeresi két, három szempont szerint teljes rendszert alkotó, legfeljebb 48 elemű készlet hiányzó elemeit, felismeri az elemek által meghatározott rendszert.

*  A tevékenysége során felmerülő problémahelyzetben megoldást keres.

*  Megfogalmazott problémát tevékenységgel, megjelenítéssel, átfogalmazással értelmez.

*  Az értelmezett problémát megoldja.

*  A problémamegoldás során a sorrendben végzett tevékenységeket szükség szerint visszafelé is elvégzi.

*  Megoldását értelmezi, ellenőrzi.

*  Kérdést tesz fel a megfogalmazott probléma kapcsán.

*  Értelmezi, elképzeli, megjeleníti a szöveges feladatban megfogalmazott hétköznapi szituációt.

*  Szöveges feladatokban megfogalmazott hétköznapi problémát megold matematikai ismeretei segítségével.

*  Tevékenység, ábrarajzolás segítségével megold egyszerű, következtetéses, szöveges feladatokat.

*  Megkülönbözteti az ismert és a keresendő (ismeretlen) adatokat.

*  Megkülönbözteti a lényeges és a lényegtelen adatokat.

*  Az értelmezett szöveges feladathoz hozzákapcsol jól megismert matematikai modellt.

*  A megválasztott modellen belül meghatározza a keresett adatokat.

*  A modellben kapott megoldást értelmezi az eredeti problémára; arra vonatkoztatva ellenőrzi a megoldást.

*  Választ fogalmaz meg a felvetett kérdésre.

*  Tudatosan emlékezetébe vési az észlelt tárgyakat, személyeket, dolgokat, és ezek jellemző tulajdonságait, elrendezését, helyzetét.

*  Tudatosan emlékezetébe vés szavakat, számokat, utasítást, adott helyzetre vonatkozó megfogalmazást.

*  Kérésre, illetve problémahelyzetben felidézi a kívánt, szükséges emlékképet.

*  Alkalmazza a felismert törvényszerűségeket analógiás esetekben.

*  Összekapcsolja az azonos matematikai tartalmú tevékenységek során szerzett tapasztalatait.

*  Példákat gyűjt konkrét tapasztalatai alapján matematikai állítások alátámasztására.

*  Egy állításról ismeretei alapján eldönti, hogy igaz vagy hamis.

*  Ismeretei alapján megfogalmaz önállóan is egyszerű állításokat.

*  Egy- és többszemélyes logikai játékban döntéseit mérlegelve előre gondolkodik.

*  Érti és helyesen használja a több, kevesebb, ugyanannyi relációkat halmazok elemszámával kapcsolatban, valamint a kisebb, nagyobb, ugyanakkora relációkat a megismert mennyiségekkel (hosszúság, tömeg, űrtartalom, idő, terület, pénz) kapcsolatban 10 000-es számkörben.

*  Használja a kisebb, nagyobb, egyenlő kifejezéseket a természetes számok körében.

*  Kis darabszámokat ránézésre felismer többféle rendezett alakban.

*  Megszámlál és leszámlál; adott (alkalmilag választott vagy szabványos) egységgel meg- és kimér a 10 000-es számkörben; oda-vissza számlál kerek tízesekkel, százasokkal, ezresekkel.

*  Ismeri a következő becslési módszereket: közelítő számlálás, közelítő mérés, mérés az egység többszörösével; becslését finomítja újrabecsléssel.

*  Nagyság szerint sorba rendez számokat, mennyiségeket.

*  Megadja és azonosítja számok sokféle műveletes alakját.

*  Megtalálja a számok helyét, közelítő helyét egyszerű számegyenesen, számtáblázatokban, a számegyenesnek ugyanahhoz a pontjához rendeli a számokat különféle alakjukban a 10 000-es számkörben.

*  Megnevezi a 10 000-es számkör számainak egyes, tízes, százas, ezres szomszédjait, tízesekre, százasokra, ezresekre kerekített értékét.

*  Számokat jellemez tartalmi és formai tulajdonságokkal.

*  Számot jellemez más számokhoz való viszonyával.

*  Helyesen írja az arab számjeleket.

*  Ismeri a római számjelek közül az i, v, x jeleket, hétköznapi helyzetekben felismeri az ezekkel képzett számokat.

*  Összekapcsolja a tízes számrendszerben a számok épülését a különféle számrendszerekben végzett tevékenységeivel.

*  Érti a számok ezresekből, százasokból, tízesekből és egyesekből való épülését, ezresek, százasok, tízesek és egyesek összegére való bontását.

*  Érti a számok számjegyeinek helyi, alaki, valódi értékét.

*  Helyesen írja és olvassa a számokat a tízes számrendszerben 10 000-ig.

*  Megbecsül, mér alkalmi és szabványos mértékegységekkel hosszúságot, tömeget, űrtartalmat és időt.

*  Helyesen alkalmazza a mérési módszereket, használ skálázott mérőeszközöket, helyes képzete van a mértékegységek nagyságáról.

*  Helyesen használja a hosszúságmérés, az űrtartalommérés és a tömegmérés szabványegységei közül a következőket: mm, cm, dm, m, km; ml, cl, dl, l; g, dkg, kg.

*  Ismeri az időmérés szabványegységeit: az órát, a percet, a másodpercet, a napot, a hetet, a hónapot, az évet.

*  Ismer hazai és külföldi pénzcímleteket 10 000-es számkörben.

*  Alkalmazza a felváltást és beváltást különböző pénzcímletek között.

*  Összeveti azonos egységgel mért mennyiség és mérőszáma nagyságát, összeveti ugyanannak a mennyiségnek a különböző egységekkel való mérésekor kapott mérőszámait.

*  Megméri különböző sokszögek kerületét különböző egységekkel.

*  Területet mér különböző egységekkel lefedéssel vagy darabolással.

*  Ismer a terület és kerület mérésére irányuló tevékenységeket.

*  Helyesen értelmezi a 10 000-es számkörben az összeadást, a kivonást, a szorzást, a bennfoglaló és az egyenlő részekre osztást.

*  Helyesen használja a műveletek jeleit.

*  Hozzákapcsolja a megfelelő műveletet adott helyzethez, történéshez, egyszerű szöveges feladathoz.

*  Értelmezi a műveleteket megjelenítéssel, modellezéssel, szöveges feladattal.

*  Megérti a következő kifejezéseket: tagok, összeg, kisebbítendő, kivonandó, különbség, tényezők, szorzandó, szorzó, szorzat, osztandó, osztó, hányados, maradék.

*  Számolásaiban felhasználja a műveletek közti kapcsolatokat, számolásai során alkalmazza konkrét esetekben a legfontosabb műveleti tulajdonságokat.

*  Megold hiányos műveletet, műveletsort az eredmény ismeretében, a műveletek megfordításával is.

*  Alkalmazza a műveletekben szereplő számok (kisebbítendő, kivonandó és különbség; tagok és összeg; tényezők és szorzat; osztandó, osztó és hányados) változtatásának következményeit.

*  Szöveghez, valós helyzethez kapcsolva zárójelet tartalmazó műveletsort értelmez, elvégez.

*  Alkalmazza a számolást könnyítő eljárásokat.

*  Fejben pontosan összead és kivon a 100-as számkörben.

*  Érti a szorzó- és bennfoglaló táblák kapcsolatát.

*  Emlékezetből tudja a kisegyszeregy és a megfelelő bennfoglalások, egyenlő részekre osztások eseteit a számok tízszereséig.

*  Fejben pontosan számol a 100-as számkörben egyjegyűvel való szorzás és maradék nélküli osztás során.

*  Fejben pontosan számol a 10 000-es számkörben a 100-as számkörben végzett műveletekkel analóg esetekben.

*  Érti a 10-zel, 100-zal, 1000-rel való szorzás, osztás kapcsolatát a helyiérték-táblázatban való jobbra, illetve balra tolódással, fejben pontosan számol a 10 000-es számkörben a számok 10-zel, 100-zal, 1000-rel történő szorzásakor és maradék nélküli osztásakor.

*  Elvégzi a feladathoz szükséges észszerű becslést, mérlegeli a becslés során kapott eredményt.

*  Teljes négyjegyűek összegét, különbségét százasokra kerekített értékekkel megbecsüli, teljes kétjegyűek két- és egyjegyűvel való szorzatát megbecsüli.

*  Helyesen végzi el az írásbeli összeadást, kivonást.

*  Helyesen végzi el az írásbeli szorzást egy- és kétjegyű szorzóval, az írásbeli osztást egyjegyű osztóval.

*  Tevékenységekkel megjelenít egységtörteket és azok többszöröseit különféle mennyiségek és többféle egységválasztás esetén.

*  A kirakást, mérést és a rajzot mint modellt használja a törtrészek összehasonlítására.

*  A negatív egész számokat irányított mennyiségként (hőmérséklet, tengerszint alatti magasság, idő) és hiányként (adósság) értelmezi.

*  Nagyság szerint összehasonlítja a természetes számokat és a negatív egész számokat a használt modellen belül.

*  Szabadon épít, kirak formát, mintát adott testekből, síklapokból.

*  Minta alapján létrehoz térbeli, síkbeli alkotásokat.

*  Sormintát, síkmintát felismer, folytat.

*  Alkotásában követi az adott feltételeket.

*  Testeket épít élekből, lapokból; elkészíti a testek élvázát, hálóját; testeket épít képek, alaprajzok alapján; elkészíti egyszerű testek alaprajzát.

*  Síkidomokat hoz létre különféle eszközök segítségével.

*  Alaklemezt, vonalzót, körzőt használ alkotáskor.

*  Megtalálja az összes, több feltételnek megfelelő építményt, síkbeli kirakást.

*  Megfogalmazza az alkotásai közti különbözőséget.

*  Megkülönbözteti és szétválogatja szabadon választott vagy meghatározott geometriai tulajdonságok szerint a gyűjtött, megalkotott testeket, síkidomokat.

*  Megfigyeli az alakzatok közös tulajdonságát, megfelelő címkéket talál megadott és halmazokba rendezett alakzatokhoz.

*  Megtalálja a közös tulajdonsággal nem rendelkező alakzatokat.

*  Megnevezi a tevékenységei során előállított, válogatásai során előkerülő alakzatokon megfigyelt tulajdonságokat.

*  Különbséget tesz testek és síkidomok között.

*  Megnevezi a sík és görbült felületeket, az egyenes és görbe vonalakat, szakaszokat tapasztalati ismeretei alapján.

*  Kiválasztja megadott síkidomok közül a sokszögeket.

*  Megnevezi a háromszögeket, négyszögeket, köröket.

*  Megkülönböztet tükrösen szimmetrikus és tükrösen nem szimmetrikus síkbeli alakzatokat.

*  Megszámlálja az egyszerű szögletes test lapjait.

*  Megnevezi a téglatest lapjainak alakját, felismeri a téglatesten az egybevágó lapokat, megkülönbözteti a téglatesten az éleket, csúcsokat.

*  Tudja a téglalap oldalainak és csúcsainak számát, összehajtással megmutatja a téglalap szögeinek egyenlőségét.

*  Megmutatja a téglalap azonos hosszúságú oldalait és elhelyezkedésüket, megmutatja és megszámlálja a téglalap átlóit és szimmetriatengelyeit.

*  Megfigyeli a kocka mint speciális téglatest és a négyzet mint speciális téglalap tulajdonságait.

*  Megnevezi megfigyelt tulajdonságai alapján a téglatestet, kockát, téglalapot, négyzetet.

*  Megfigyelt tulajdonságaival jellemzi a létrehozott síkbeli és térbeli alkotást, mintázatot.

*  Tapasztalattal rendelkezik mozgással, kirakással a tükörkép előállításáról.

*  Szimmetrikus alakzatokat hoz létre térben, síkban különböző eszközökkel; felismeri a szimmetriát valóságos dolgokon, síkbeli alakzatokon.

*  Megépíti, kirakja, megrajzolja hálón, jelölés nélküli lapon sablonnal, másolópapír segítségével alakzat tükörképét, eltolt képét.

*  Ellenőrzi a tükrözés, eltolás helyességét tükör vagy másolópapír segítségével.

*  Követi a sormintában vagy a síkmintában lévő szimmetriát.

*  Térben, síkban az eredetihez hasonló testeket, síkidomokat alkot nagyított vagy kicsinyített elemekből; az eredetihez hasonló síkidomokat rajzol hálón.

*  Helyesen használja az irányokat és távolságokat jelölő kifejezéseket térben és síkon.

*  Tájékozódik lakóhelyén, bejárt terepen: bejárt útvonalon visszatalál adott helyre, adott utca és házszám alapján megtalál házat.

*  Térképen, négyzethálón megtalál pontot két adat segítségével.

*  Részt vesz memóriajátékokban különféle tulajdonságok szerinti párok keresésében.

*  Megfogalmazza a személyek, tárgyak, dolgok, időpontok, számok, testek, síklapok közötti egyszerű viszonyokat, kapcsolatokat.

*  Érti a problémákban szereplő adatok viszonyát.

*  Megfogalmazza a felismert összefüggéseket.

*  Összefüggéseket keres sorozatok elemei között.

*  Megadott szabály szerint sorozatot alkot; megértett probléma értelmezéséhez, megoldásához sorozatot, táblázatot állít elő modellként.

*  Tárgyakkal, logikai készletek elemeivel kirakott periodikus sorozatokat folytat.

*  Elsorolja az évszakokat, hónapokat, napokat, napszakokat egymás után, tetszőleges kezdőponttól is.

*  Ismert műveletekkel alkotott sorozat, táblázat szabályát felismeri; ismert szabály szerint megkezdett sorozatot, táblázatot helyesen, önállóan folytat.

*  Tárgyakkal, számokkal kapcsolatos gépjátékhoz szabályt alkot; felismeri az egyszerű gép megfordításával nyert gép szabályát.

*  Felismer kapcsolatot elempárok, elemhármasok tagjai között.

*  Szabályjátékok során létrehoz a felismert kapcsolat alapján további elempárokat, elemhármasokat.

*  A sorozatban, táblázatban, gépjátékokban felismert összefüggést megfogalmazza saját szavaival, nyíljelöléssel vagy nyitott mondattal.

*  Adatokat gyűjt a környezetében.

*  Adatokat rögzít későbbi elemzés céljából.

*  Gyűjtött adatokat táblázatba rendez, diagramon ábrázol.

*  Adatokat gyűjt ki táblázatból, adatokat olvas le diagramról.

*  Jellemzi az összességeket.

*  Részt vesz olyan játékokban, kísérletekben, melyekben a véletlen szerepet játszik.

*  Különbséget tesz tapasztalatai alapján a „biztos", „lehetetlen", „lehetséges, de nem biztos" események között.

*  Megítéli „biztos", „lehetetlen", „lehetséges, de nem biztos" eseményekkel kapcsolatos állítások igazságát.

*  Tapasztalatai alapján tippet fogalmaz meg arról, hogy két esemény közül melyik esemény valószínűbb olyan véletlentől függő szituációk során, melyekben a két esemény valószínűsége között jól belátható a különbség.

*  Tetszőleges vagy megadott módszerrel összeszámlálja az egyes kimenetelek előfordulásait olyan egyszerű játékokban, kísérletekben, amelyekben a véletlen szerepet játszik.

*  A valószínűségi játékokban, kísérletekben megfogalmazott előzetes sejtését, tippjét összeveti a megfigyelt előfordulásokkal.

*  Önállóan értelmezi a hallott, olvasott matematikai tartalmú szöveget.

*  Helyesen használja a mennyiségi viszonyokat kifejező szavakat, nyelvtani szerkezeteket.

*  Szöveges feladatokban a különböző kifejezésekkel megfogalmazott műveleteket megérti.

*  Megfelelő szókincset és jeleket használ mennyiségi viszonyok kifejezésére szóban és írásban.

*  Megfelelően használja szóban és írásban a nyelvtani szerkezeteket matematikai tartalmuk szerint.

*  Szöveget, ábrát alkot matematikai jelekhez, műveletekhez.

*  Játékos feladatokban személyeket, tárgyakat, számokat, formákat néhány meghatározó tulajdonsággal jellemez.

*  Kérdést fogalmaz meg, ha munkája során nehézségbe ütközik.

*  Nyelvi szempontból megfelelő választ ad a feladatokban megjelenő kérdésekre.

*  Használ matematikai képességfejlesztő számítógépes játékokat, programokat.

*  Alkalmazza a tanult infokommunikációs ismereteket matematikai problémák megoldása során.

### 5-8. évfolyamon
*  Rendelkezik a matematikai problémamegoldáshoz szükséges eszközrendszerrel, melyet az adott problémának megfelelően tud alkalmazni.

*  Felismeri a hétköznapi helyzetekben a matematikai vonatkozásokat, és ezek leírására megfelelő modellt használ.

*  Megfogalmaz sejtéseket, és logikus érveléssel ellenőrzi azokat.

*  Helyesen használja a matematikai jelöléseket írásban.

*  Olvassa és érti az életkorának megfelelő matematikai tartalmú szövegeket.

*  Tanulási módszerei változatosak: szóbeli közlés, írott szöveg és digitális csatornák útján egyaránt képes az ismeretek elsajátítására.

*  Matematikai ismereteit össze tudja kapcsolni más tanulásterületeken szerzett tapasztalatokkal.

*  Matematikai ismereteit alkalmazza a pénzügyi tudatosság területét érintő feladatok megoldásában.

*  Különböző szövegekhez megfelelő modelleket készít.

*  Számokat, számhalmazokat, halmazműveleti eredményeket számegyenesen ábrázol.

*  Konkrét szituációkat szemléltet gráfok segítségével.

*  Egyismeretlenes elsőfokú egyenletet lebontogatással és mérlegelvvel megold.

*  Állítások logikai értékét (igaz vagy hamis) megállapítja.

*  Igaz és hamis állításokat fogalmaz meg.

*  Tanult minták alapján néhány lépésből álló bizonyítási gondolatsort megért és önállóan összeállít.

*  A logikus érvelésben a matematikai szaknyelvet következetesen alkalmazza társai meggyőzésére.

*  Elemeket halmazba rendez több szempont alapján.

*  Részhalmazokat konkrét esetekben felismer és ábrázol.

*  Véges halmaz kiegészítő halmazát (komplementerét), véges halmazok közös részét (metszetét), egyesítését (unióját) képezi és ábrázolja konkrét esetekben.

*  A természetes számokat osztóik száma alapján és adott számmal való osztási maradékuk szerint csoportosítja.

*  Síkbeli tartományok közül kiválasztja a szögtartományokat, nagyság szerint összehasonlítja, méri, csoportosítja azokat.

*  Csoportosítja a háromszögeket szögeik és oldalaik szerint.

*  Ismeri a speciális négyszögek legfontosabb tulajdonságait, ezek alapján elkészíti a halmazábrájukat.

*  Valószínűségi játékokat, kísérleteket végez, ennek során az adatokat tervszerűen gyűjti, rendezi és ábrázolja digitálisan is.

*  Összeszámlálási feladatok megoldása során alkalmazza az összes eset áttekintéséhez szükséges módszereket.

*  A háromszögek és a speciális négyszögek tulajdonságait alkalmazza feladatok megoldásában.

*  A kocka, a téglatest, a hasáb, a gúla, a gömb tulajdonságait alkalmazza feladatok megoldásában.

*  Konkrét adatsor esetén átlagot számol, megállapítja a leggyakoribb adatot (módusz), a középső adatot (medián), és ezeket összehasonlítja.

*  Matematikából, más tantárgyakból és a mindennapi életből vett egyszerű szöveges feladatokat következtetéssel vagy egyenlettel megold.

*  Gazdasági, pénzügyi témájú egyszerű szöveges feladatokat következtetéssel vagy egyenlettel megold.

*  Gyakorlati problémák megoldása során előforduló mennyiségeknél becslést végez.

*  Megoldását ellenőrzi.

*  Ismeri a százalék fogalmát, gazdasági, pénzügyi és mindennapi élethez kötődő százalékszámítási feladatokat megold.

*  Érti és alkalmazza a számok helyi értékes írásmódját nagy számok esetén.

*  Érti és alkalmazza a számok helyi értékes írásmódját tizedes törtek esetén.

*  Ismeri a római számjelek közül az l, c, d, m jeleket, felismeri az ezekkel képzett számokat a hétköznapi helyzetekben.

*  Ismeri az egész számokat.

*  Meghatározza konkrét számok ellentettjét, abszolút értékét.

*  Ábrázol törtrészeket, meghatároz törtrészeknek megfelelő törtszámokat.

*  Megfelelteti egymásnak a racionális számok közönséges tört és tizedes tört alakját.

*  Ismeri a racionális számokat, tud példát végtelen nem szakaszos tizedes törtre.

*  Meghatározza konkrét számok reciprokát.

*  Ismeri és helyesen alkalmazza a műveleti sorrendre és a zárójelezésre vonatkozó szabályokat fejben, írásban és géppel számolás esetén is a racionális számok körében.

*  Ismeri és alkalmazza a 2-vel, 3-mal, 4-gyel, 5-tel, 6-tal, 9-cel, 10-zel, 100-zal való oszthatóság szabályait.

*  Ismeri a prímszám és az összetett szám fogalmakat; el tudja készíteni összetett számok prímtényezős felbontását 1000-es számkörben.

*  Meghatározza természetes számok legnagyobb közös osztóját és legkisebb közös többszörösét.

*  Pozitív egész számok pozitív egész kitevőjű hatványát kiszámolja.

*  Négyzetszámok négyzetgyökét meghatározza.

*  Egyszerű betűs kifejezésekkel összeadást, kivonást végez, és helyettesítési értéket számol.

*  Egy- vagy kéttagú betűs kifejezést számmal szoroz, két tagból közös számtényezőt kiemel.

*  Írásban összead, kivon és szoroz.

*  Gyakorlati feladatok megoldása során legfeljebb kétjegyű egész számmal írásban oszt. a hányadost megbecsüli.

*  Gyakorlati feladatok megoldása során tizedes törtet legfeljebb kétjegyű egész számmal írásban oszt. a hányadost megbecsüli.

*  A műveleti szabályok ismeretében ellenőrzi számolását. a kapott eredményt észszerűen kerekíti.

*  A gyakorlati problémákban előforduló mennyiségeket becsülni tudja, feladatmegoldásához ennek megfelelő tervet készít.

*  Elvégzi az alapműveleteket a racionális számok körében, eredményét összeveti előzetes becslésével.

*  Ismeri az idő, a tömeg, a hosszúság, a terület, a térfogat és az űrtartalom szabványmértékegységeit, használja azokat mérések és számítások esetén.

*  Egyenes hasáb, téglatest, kocka alakú tárgyak felszínét és térfogatát méréssel megadja, egyenes hasáb felszínét és térfogatát képlet segítségével kiszámolja; a képleteket megalapozó összefüggéseket érti.

*  Idő, tömeg, hosszúság, terület, térfogat és űrtartalom mértékegységeket átvált helyi értékes gondolkodás alapján, gyakorlati célszerűség szerint.

*  Meghatározza háromszögek és speciális négyszögek kerületét, területét.

*  A kocka, a téglatest, a hasáb és a gúla hálóját elkészíti.

*  Testeket épít képek, nézetek, alaprajzok, hálók alapján.

*  Ismeri a gömb tulajdonságait.

*  Ismeri a kocka, a téglatest, a hasáb és a gúla következő tulajdonságait: határoló lapok típusa, száma, egymáshoz viszonyított helyzete; csúcsok, élek száma; lapátló, testátló.

*  Ismeri a tengelyesen szimmetrikus háromszöget.

*  Ismeri a speciális négyszögeket: trapéz, paralelogramma, téglalap, deltoid, rombusz, húrtrapéz, négyzet.

*  Tapasztalatot szerez a síkbeli mozgásokról gyakorlati helyzetekben.

*  Felismeri a síkban az egybevágó alakzatokat.

*  Felismeri a kicsinyítést és a nagyítást hétköznapi helyzetekben.

*  Ismeri a kör részeit; különbséget tesz egyenes, félegyenes és szakasz között.

*  Ismeri a háromszögek tulajdonságait: belső és külső szögek összege, háromszög-egyenlőtlenség.

*  Ismeri a pitagorasz-tételt és alkalmazza számítási feladatokban.

*  Ismeri a négyszögek tulajdonságait: belső és külső szögek összege, konvex és konkáv közti különbség, átló fogalma.

*  A szerkesztéshez tervet, előzetes ábrát készít.

*  Ismeri az alapszerkesztéseket: szakaszfelező merőlegest, szögfelezőt, merőleges és párhuzamos egyeneseket szerkeszt, szöget másol.

*  Megszerkeszti alakzatok tengelyes és középpontos tükörképét.

*  Geometriai ismereteinek felhasználásával pontosan szerkeszt több adott feltételnek megfelelő ábrát.

*  Konkrét halmazok elemei között megfeleltetést hoz létre.

*  Felismeri az egyenes és a fordított arányosságot konkrét helyzetekben.

*  Tájékozódik a koordináta-rendszerben: koordinátáival adott pontot ábrázol, megadott pont koordinátáit leolvassa.

*  Értéktáblázatok adatait grafikusan ábrázolja.

*  Egyszerű grafikonokat jellemez.

*  Felismeri és megalkotja az egyenes arányosság grafikonját.

*  Sorozatokat adott szabály alapján folytat.

*  Néhány tagjával adott sorozat esetén felismer és megfogalmaz képzési szabályt.

*  Valószínűségi játékokban érti a lehetséges kimeneteleket, játékában stratégiát követ.

*  Ismeri a gyakoriság és a relatív gyakoriság fogalmát. ismereteit felhasználja a „lehetetlen", a „biztos" és a „kisebb, nagyobb eséllyel lehetséges" kijelentések megfogalmazásánál.

*  Helyesen használja a tanult matematikai fogalmakat megnevező szakkifejezéseket.

*  Adatokat táblázatba rendez, diagramon ábrázol hagyományos és digitális eszközökkel is.

*  Különböző típusú diagramokat megfeleltet egymásnak.

*  Megadott szempont szerint adatokat gyűjt ki táblázatból, olvas le hagyományos vagy digitális forrásból származó diagramról, majd rendszerezés után következtetéseket fogalmaz meg.

*  Konkrét esetekben halmazokat felismer és ábrázol.

*  Értelmezi a táblázatok adatait, az adatoknak megfelelő ábrázolási módot kiválasztja, és az ábrát elkészíti.

*  Ismer táblázatkezelő programot, tud adatokat összehasonlítani, elemezni.

*  A fejszámoláson és az írásban végzendő műveleteken túlmutató számolási feladatokhoz és azok ellenőrzéséhez számológépet használ.

*  Ismer és használ dinamikus geometriai szoftvereket, tisztában van alkalmazási lehetőségeikkel.

*  Ismer és használ digitális matematikai játékokat, programokat.

*  Alkalmazza a tanult infokommunikációs ismereteket matematikai problémák megoldása során.

### 9-12. évfolyamon
*  Ismeretei segítségével, a megfelelő modell alkalmazásával megold hétköznapi és matematikai problémákat, a megoldást ellenőrzi és értelmezi.

*  Megérti a környezetében jelen lévő logikai, mennyiségi, függvényszerű, térbeli és statisztikai kapcsolatokat.

*  Sejtéseket fogalmaz meg és logikus lépésekkel igazolja azokat.

*  Adatokat gyűjt, rendez, ábrázol, értelmez.

*  A matematikai szakkifejezéseket és jelöléseket helyesen használja írásban és szóban egyaránt.

*  Megérti a hallott és olvasott matematikai tartalmú szövegeket.

*  Felismeri a matematika különböző területei közötti kapcsolatokat.

*  A matematika tanulása során digitális eszközöket és különböző információforrásokat használ.

*  A matematikát más tantárgyakhoz kapcsolódó témákban is használja.

*  Matematikai ismereteit alkalmazza a pénzügyi tudatosság területét érintő feladatok megoldásában.

*  Adott halmazt diszjunkt részhalmazaira bont, osztályoz.

*  Matematikai vagy hétköznapi nyelven megfogalmazott szövegből a matematikai tartalmú információkat kigyűjti, rendszerezi.

*  Felismeri a matematika különböző területei közötti kapcsolatot.

*  Látja a halmazműveletek és a logikai műveletek közötti kapcsolatokat.

*  Halmazokat különböző módokon megad.

*  Halmazokkal műveleteket végez, azokat ábrázolja és értelmezi.

*  Véges halmazok elemszámát meghatározza.

*  Alkalmazza a logikai szita elvét.

*  Adott állításról eldönti, hogy igaz vagy hamis.

*  Alkalmazza a tagadás műveletét egyszerű feladatokban.

*  Ismeri és alkalmazza az „és", a (megengedő és kizáró) „vagy" logikai jelentését.

*  Megfogalmazza adott állítás megfordítását.

*  Megállapítja egyszerű „ha \... , akkor \..." és „akkor és csak akkor" típusú állítások logikai értékét.

*  Helyesen használja a „minden" és „van olyan" kifejezéseket.

*  Tud egyszerű állításokat indokolni és tételeket bizonyítani.

*  Megold sorba rendezési és kiválasztási feladatokat.

*  Konkrét szituációkat szemléltet és egyszerű feladatokat megold gráfok segítségével.

*  Adott problémához megoldási stratégiát, algoritmust választ, készít.

*  A problémának megfelelő matematikai modellt választ, alkot.

*  A kiválasztott modellben megoldja a problémát.

*  A modellben kapott megoldását az eredeti problémába visszahelyettesítve értelmezi, ellenőrzi és az észszerűségi szempontokat figyelembe véve adja meg válaszát.

*  Geometriai szerkesztési feladatoknál vizsgálja és megállapítja a szerkeszthetőség feltételeit.

*  Ismeri és alkalmazza a következő egyenletmegoldási módszereket: mérlegelv, grafikus megoldás, szorzattá alakítás.

*  Megold elsőfokú egyismeretlenes egyenleteket és egyenlőtlenségeket, elsőfokú kétismeretlenes egyenletrendszereket.

*  Megold másodfokú egyismeretlenes egyenleteket és egyenlőtlenségeket; ismeri és alkalmazza a diszkriminánst, a megoldóképletet és a gyöktényezős alakot.

*  Megold egyszerű, a megfelelő definíció alkalmazását igénylő exponenciális egyenleteket, egyenlőtlenségeket.

*  Egyenletek megoldását behelyettesítéssel, értékkészlet-vizsgálattal ellenőrzi.

*  Ismeri a mérés alapelvét, alkalmazza konkrét alap- és származtatott mennyiségek esetén.

*  Ismeri a hosszúság, terület, térfogat, űrtartalom, idő mértékegységeit és az átváltási szabályokat. származtatott mértékegységeket átvált.

*  Sík- és térgeometriai feladatoknál a problémának megfelelő mértékegységben adja meg válaszát.

*  Ismeri és alkalmazza az oszthatóság alapvető fogalmait.

*  Összetett számokat felbont prímszámok szorzatára.

*  Meghatározza két természetes szám legnagyobb közös osztóját és legkisebb közös többszörösét, és alkalmazza ezeket egyszerű gyakorlati feladatokban.

*  Ismeri és alkalmazza az oszthatósági szabályokat.

*  Érti a helyi értékes írásmódot 10-es és más alapú számrendszerekben.

*  Ismeri a számhalmazok épülésének matematikai vonatkozásait a természetes számoktól a valós számokig.

*  A kommutativitás, asszociativitás, disztributivitás műveleti azonosságokat helyesen alkalmazza különböző számolási helyzetekben.

*  Racionális számokat tizedes tört és közönséges tört alakban is felír.

*  Ismer példákat irracionális számokra.

*  Ismeri a valós számok és a számegyenes kapcsolatát.

*  Ismeri és alkalmazza az abszolút érték, az ellentett és a reciprok fogalmát.

*  A számolással kapott eredményeket nagyságrendileg megbecsüli, és így ellenőrzi az eredményt.

*  Valós számok közelítő alakjaival számol, és megfelelően kerekít.

*  Ismeri és alkalmazza a négyzetgyök fogalmát és azonosságait.

*  Ismeri és alkalmazza az n-edik gyök fogalmát.

*  Ismeri és alkalmazza a normálalak fogalmát.

*  Ismeri és alkalmazza az egész kitevőjű hatvány fogalmát és a hatványozás azonosságait.

*  Ismeri és alkalmazza a racionális kitevőjű hatvány fogalmát és a hatványozás azonosságait.

*  Ismeri és alkalmazza a logaritmus fogalmát.

*  Műveleteket végez algebrai kifejezésekkel.

*  Ismer és alkalmaz egyszerű algebrai azonosságokat.

*  Átalakít algebrai kifejezéseket összevonás, szorzattá alakítás, nevezetes azonosságok alkalmazásával.

*  Ismeri és alkalmazza az egyenes és a fordított arányosságot.

*  Ismeri és alkalmazza a százalékalap, -érték, -láb, -pont fogalmát.

*  Ismeri és használja a pont, egyenes, sík (térelemek) és szög fogalmát.

*  Ismeri és feladatmegoldásban alkalmazza a térelemek kölcsönös helyzetét, távolságát és hajlásszögét.

*  Ismeri és alkalmazza a nevezetes szögpárok tulajdonságait.

*  Ismeri az alapszerkesztéseket, és ezeket végre tudja hajtani hagyományos vagy digitális eszközzel.

*  Ismeri és alkalmazza a háromszögek oldalai, szögei, oldalai és szögei közötti kapcsolatokat; a speciális háromszögek tulajdonságait.

*  Ismeri és alkalmazza a háromszög nevezetes vonalaira, pontjaira és köreire vonatkozó fogalmakat és tételeket.

*  Ismeri és alkalmazza a pitagorasz-tételt és megfordítását.

*  Kiszámítja háromszögek területét.

*  Ismeri és alkalmazza speciális négyszögek tulajdonságait, területüket kiszámítja.

*  Ismeri és alkalmazza a szabályos sokszög fogalmát; kiszámítja a konvex sokszög belső és külső szögeinek összegét.

*  Átdarabolással kiszámítja sokszögek területét.

*  Ki tudja számolni a kör és részeinek kerületét, területét.

*  Ismeri a kör érintőjének fogalmát, kapcsolatát az érintési pontba húzott sugárral.

*  Ismeri és alkalmazza a thalész-tételt és megfordítását.

*  Ismer példákat geometriai transzformációkra.

*  Ismeri és alkalmazza a síkbeli egybevágósági transzformációkat és tulajdonságaikat; alakzatok egybevágóságát.

*  Ismeri és alkalmazza a középpontos hasonlósági transzformációt, a hasonlósági transzformációt és az alakzatok hasonlóságát.

*  Ismeri és alkalmazza a hasonló síkidomok kerületének és területének arányára vonatkozó tételeket.

*  Megszerkeszti egy alakzat tengelyes, illetve középpontos tükörképét, pont körüli elforgatottját, párhuzamos eltoltját hagyományosan és digitális eszközzel.

*  Ismeri a vektorokkal kapcsolatos alapvető fogalmakat.

*  Ismer és alkalmaz egyszerű vektorműveleteket.

*  Alkalmazza a vektorokat feladatok megoldásában.

*  Ismeri hegyesszögek szögfüggvényeinek definícióját a derékszögű háromszögben.

*  Ismeri tompaszögek szögfüggvényeinek származtatását a hegyesszögek szögfüggvényei alapján.

*  Ismeri a hegyes- és tompaszögek szögfüggvényeinek összefüggéseit.

*  Alkalmazza a szögfüggvényeket egyszerű geometriai számítási feladatokban.

*  A szögfüggvény értékének ismeretében meghatározza a szöget.

*  Ismeri és alkalmazza a szinusz- és a koszinusztételt.

*  Ismeri és alkalmazza a hasáb, a henger, a gúla, a kúp, a gömb, a csonkagúla, a csonkakúp (speciális testek) tulajdonságait.

*  Lerajzolja a kocka, téglatest, egyenes hasáb, egyenes körhenger, egyenes gúla, forgáskúp hálóját.

*  Kiszámítja a speciális testek felszínét és térfogatát egyszerű esetekben.

*  Ismeri és alkalmazza a hasonló testek felszínének és térfogatának arányára vonatkozó tételeket.

*  Megad pontot és vektort koordinátáival a derékszögű koordináta-rendszerben.

*  Koordináta-rendszerben ábrázol adott feltételeknek megfelelő ponthalmazokat.

*  Koordináták alapján számításokat végez szakaszokkal, vektorokkal.

*  Ismeri és alkalmazza az egyenes egyenletét.

*  Egyenesek egyenletéből következtet az egyenesek kölcsönös helyzetére.

*  Kiszámítja egyenesek metszéspontjainak koordinátáit az egyenesek egyenletének ismeretében.

*  Megadja és alkalmazza a kör egyenletét a kör sugarának és a középpont koordinátáinak ismeretében.

*  Megad hétköznapi életben előforduló hozzárendeléseket.

*  Adott képlet alapján helyettesítési értékeket számol, és azokat táblázatba rendezi.

*  Táblázattal megadott függvény összetartozó értékeit ábrázolja koordináta-rendszerben.

*  Képlettel adott függvényt hagyományosan és digitális eszközzel ábrázol.

*  Adott értékkészletbeli elemhez megtalálja az értelmezési tartomány azon elemeit, amelyekhez a függvény az adott értéket rendeli.

*  A grafikonról megállapítja függvények alapvető tulajdonságait.

*  Számtani és mértani sorozatokat adott szabály alapján felír, folytat.

*  A számtani, mértani sorozat n-edik tagját felírja az első tag és a különbség (differencia)/hányados (kvóciens) ismeretében.

*  A számtani, mértani sorozatok első n tagjának összegét kiszámolja.

*  Mértani sorozatokra vonatkozó ismereteit használja gazdasági, pénzügyi, természettudományi és társadalomtudományi problémák megoldásában.

*  Adott cél érdekében tudatos adatgyűjtést és rendszerezést végez.

*  Hagyományos és digitális forrásból származó adatsokaság alapvető statisztikai jellemzőit meghatározza, értelmezi és értékeli.

*  Adatsokaságból adott szempont szerint oszlop- és kördiagramot készít hagyományos és digitális eszközzel.

*  Ismeri és alkalmazza a sodrófa (box-plot) diagramot adathalmazok jellemzésére, összehasonlítására.

*  Felismer grafikus manipulációkat diagramok esetén.

*  Tapasztalatai alapján véletlen jelenségek jövőbeni kimenetelére észszerűen tippel.

*  Ismeri és alkalmazza a klasszikus valószínűségi modellt és a laplace-képletet.

*  Véletlen kísérletek adatait rendszerezi, relatív gyakoriságokat számol, nagy elemszám esetén számítógépet alkalmaz.

*  Konkrét valószínűségi kísérletek esetében az esemény, eseménytér, elemi esemény, relatív gyakoriság, valószínűség, egymást kizáró események, független események fogalmát megkülönbözteti és alkalmazza.

*  Ismeri és egyszerű esetekben alkalmazza a valószínűség geometriai modelljét.

*  Meghatározza a valószínűséget visszatevéses, illetve visszatevés nélküli mintavétel esetén.

*  A megfelelő matematikai tankönyveket, feladatgyűjteményeket, internetes tartalmakat értőn olvassa, a matematikai tartalmat rendszerezetten kigyűjti és megérti.

*  A matematikai fogalmakat és jelöléseket megfelelően használja.

*  Önállóan kommunikál matematika tartalmú feladatokkal kapcsolatban.

*  Matematika feladatok megoldását szakszerűen prezentálja írásban és szóban a szükséges alapfogalmak, azonosságok, definíciók és tételek segítségével.

*  Szöveg alapján táblázatot, grafikont készít, ábrát, kapcsolatokat szemléltető gráfot rajzol, és ezeket kombinálva prezentációt készít és mutat be.

*  Ismer a tananyaghoz kapcsolódó matematikatörténeti vonatkozásokat.

*  Számológép segítségével alapműveletekkel felírható számolási eredményt; négyzetgyököt; átlagot; szögfüggvények értékét, illetve abból szöget; logaritmust; faktoriálist; binomiális együtthatót; szórást meghatároz.

*  Digitális környezetben matematikai alkalmazásokkal dolgozik.

*  Megfelelő informatikai alkalmazás segítségével szöveget szerkeszt, táblázatkezelő programmal diagramokat készít.

*  Ismereteit digitális forrásokból kiegészíti, számítógép segítségével elemzi és bemutatja.

*  Prezentációhoz informatív diákat készít, ezeket logikusan és következetesen egymás után fűzi és bemutatja.

*  Kísérletezéshez, sejtés megfogalmazásához, egyenlet grafikus megoldásához és ellenőrzéshez dinamikus geometriai, grafikus és táblázatkezelő szoftvereket használ.

*  Szerkesztési feladatok euklideszi módon történő megoldásához dinamikus geometriai szoftvert használ.

## Második idegen nyelv
### 9-12. évfolyamon
*  Megismerkedik az idegen nyelvvel, a nyelvtanulással és örömmel vesz részt az órákon.

*  Bekapcsolódik a szóbeliséget, írást, szövegértést vagy interakciót igénylő alapvető és korának megfelelő játékos, élményalapú élő idegen nyelvi tevékenységekbe.

*  Szóban visszaad szavakat, esetleg rövid, nagyon egyszerű szövegeket hoz létre.

*  Lemásol, leír szavakat és rövid, nagyon egyszerű szövegeket.

*  Követi a szintjének megfelelő, vizuális vagy nonverbális eszközökkel támogatott, ismert célnyelvi óravezetést, utasításokat.

*  Felismeri és használja a legegyszerűbb, mindennapi nyelvi funkciókat.

*  Elmondja magáról a legalapvetőbb információkat.

*  Ismeri az adott célnyelvi kultúrákhoz tartozó országok fontosabb jellemzőit és a hozzájuk tartozó alapvető nyelvi elemeket.

*  Törekszik a tanult nyelvi elemek megfelelő kiejtésére.

*  Célnyelvi tanulmányain keresztül nyitottabbá, a világ felé érdeklődőbbé válik.

*  Megismétli az élőszóban elhangzó egyszerű szavakat, kifejezéseket játékos, mozgást igénylő, kreatív nyelvórai tevékenységek során.

*  Lebetűzi a nevét.

*  Lebetűzi a tanult szavakat társaival közösen játékos tevékenységek kapcsán, szükség esetén segítséggel.

*  Célnyelven megoszt egyedül vagy társaival együttműködésben megszerzett, alapvető információkat szóban, akár vizuális elemekkel támogatva.

*  Felismeri az anyanyelvén, illetve a tanult idegen nyelven történő írásmód és betűkészlet közötti különbségeket.

*  Ismeri az adott nyelv ábécéjét.

*  Lemásol tanult szavakat játékos, alkotó nyelvórai tevékenységek során.

*  Megold játékos írásbeli feladatokat a szavak, szószerkezetek, rövid mondatok szintjén.

*  Részt vesz kooperatív munkaformában végzett kreatív tevékenységekben, projektmunkában szavak, szószerkezetek, rövid mondatok leírásával, esetleg képi kiegészítéssel.

*  Írásban megnevezi az ajánlott tématartományokban megjelölt, begyakorolt elemeket.

*  Megérti az élőszóban elhangzó, ismert témákhoz kapcsolódó, verbális, vizuális vagy nonverbális eszközökkel segített rövid kijelentéseket, kérdéseket.

*  Beazonosítja az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű szövegben a tanult nyelvi elemeket.

*  Kiszűri a lényeget az ismert nyelvi elemeket tartalmazó, nagyon rövid, egyszerű hangzó szövegből.

*  Azonosítja a célzott információt a nyelvi szintjének és életkorának megfelelő rövid hangzó szövegben.

*  Támaszkodik az életkorának és nyelvi szintjének megfelelő hangzó szövegre az órai alkotó jellegű nyelvi, mozgásos nyelvi és játékos nyelvi tevékenységek során.

*  Felismeri az anyanyelv és az idegen nyelv hangkészletét.

*  Értelmezi azokat az idegen nyelven szóban elhangzó nyelvórai szituációkat, melyeket anyanyelvén már ismer.

*  Felismeri az anyanyelve és a célnyelv közötti legalapvetőbb kiejtésbeli különbségeket.

*  Figyel a célnyelvre jellemző hangok kiejtésére.

*  Megkülönbözteti az anyanyelvi és a célnyelvi írott szövegben a betű- és jelkészlet közti különbségeket.

*  Beazonosítja a célzott információt az életkorának megfelelő szituációkhoz kapcsolódó, rövid, egyszerű, a nyelvtanításhoz készült, illetve eredeti szövegben.

*  Csendes olvasás keretében feldolgozva megért ismert szavakat tartalmazó, pár szóból vagy mondatból álló, akár illusztrációval támogatott szöveget.

*  Megérti a nyelvi szintjének megfelelő, akár vizuális eszközökkel is támogatott írott utasításokat és kérdéseket, és ezekre megfelelő válaszreakciókat ad.

*  Kiemeli az ismert nyelvi elemeket tartalmazó, egyszerű, írott, pár mondatos szöveg fő mondanivalóját.

*  Támaszkodik az életkorának és nyelvi szintjének megfelelő írott szövegre az órai játékos alkotó, mozgásos vagy nyelvi fejlesztő tevékenységek során, kooperatív munkaformákban.

*  Megtapasztalja a közös célnyelvi olvasás élményét.

*  Aktívan bekapcsolódik a közös meseolvasásba, a mese tartalmát követi.

*  A tanórán begyakorolt, nagyon egyszerű, egyértelmű kommunikációs helyzetekben a megtanult, állandósult beszédfordulatok alkalmazásával kérdez vagy reagál, mondanivalóját segítséggel vagy nonverbális eszközökkel kifejezi.

*  Törekszik arra, hogy a célnyelvet eszközként alkalmazza információszerzésre.

*  Rövid, néhány mondatból álló párbeszédet folytat, felkészülést követően.

*  A tanórán bekapcsolódik a már ismert, szóbeli interakciót igénylő nyelvi tevékenységekbe, a begyakorolt nyelvi elemeket tanári segítséggel a tevékenység céljainak megfelelően alkalmazza.

*  Érzéseit egy-két szóval vagy begyakorolt állandósult nyelvi fordulatok segítségével kifejezi, főként rákérdezés alapján, nonverbális eszközökkel kísérve a célnyelvi megnyilatkozást.

*  Elsajátítja a tanult szavak és állandósult szókapcsolatok célnyelvi normához közelítő kiejtését tanári minta követése által, vagy autentikus hangzó anyag, digitális technológia segítségével.

*  Felismeri és alkalmazza a legegyszerűbb, üdvözlésre és elköszönésre használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és alkalmazza a legegyszerűbb, bemutatkozásra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, megszólításra használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a köszönet és az arra történő reagálás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a tudás és nem tudás kifejezésére használt mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Felismeri és használja a legegyszerűbb, a nem értés, visszakérdezés és ismétlés, kérés kifejezésére használt mindennapi nyelvi funkciókat életkorának és nyelvi szintjének megfelelő, egyszerű helyzetekben.

*  Közöl alapvető személyes információkat magáról, egyszerű nyelvi elemek segítségével.

*  Új szavak, kifejezések tanulásakor ráismer a már korábban tanult szavakra, kifejezésekre.

*  Szavak, kifejezések tanulásakor felismeri, ha új elemmel találkozik és rákérdez, vagy megfelelő tanulási stratégiával törekszik a megértésre.

*  A célok eléréséhez társaival rövid feladatokban együttműködik.

*  Egy feladat megoldásának sikerességét segítséggel értékelni tudja.

*  Felismeri az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is.

*  Felhasznál és létrehoz rövid, nagyon egyszerű célnyelvi szövegeket szabadidős tevékenységek során.

*  Alapvető célzott információt megszerez a tanult témákban tudásának bővítésére.

*  Megismeri a főbb, az adott célnyelvi kultúrákhoz tartozó országok nevét, földrajzi elhelyezkedését, főbb országismereti jellemzőit.

*  Ismeri a főbb, célnyelvi kultúrához tartozó, ünnepekhez kapcsolódó alapszintű kifejezéseket, állandósult szókapcsolatokat és szokásokat.

*  Megérti a tanult nyelvi elemeket életkorának megfelelő digitális tartalmakban, digitális csatornákon olvasott vagy hallott nagyon egyszerű szövegekben is.

*  Létrehoz nagyon egyszerű írott, pár szavas szöveget szóban vagy írásban digitális felületen.

*  Szóban és írásban megold változatos kihívásokat igénylő feladatokat az élő idegen nyelven.

*  Szóban és írásban létrehoz rövid szövegeket, ismert nyelvi eszközökkel, a korának megfelelő szövegtípusokban.

*  Értelmez korának és nyelvi szintjének megfelelő hallott és írott célnyelvi szövegeket az ismert témákban és szövegtípusokban.

*  A tanult nyelvi elemek és kommunikációs stratégiák segítségével írásbeli és szóbeli interakciót folytat, valamint közvetít az élő idegen nyelven.

*  Kommunikációs szándékának megfelelően alkalmazza a tanult nyelvi funkciókat és a megszerzett szociolingvisztikai, pragmatikai és interkulturális jártasságát.

*  Nyelvtudását egyre inkább képes fejleszteni tanórán kívüli helyzetekben is különböző eszközökkel és lehetőségekkel.

*  Használ életkorának és nyelvi szintjének megfelelő hagyományos és digitális alapú nyelvtanulási forrásokat és eszközöket.

*  Alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra, ismeretszerzésre hagyományos és digitális csatornákon.

*  Törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és intonáció megközelítésére.

*  Érti a nyelvtudás fontosságát, és motivációja a nyelvtanulásra tovább erősödik.

*  Aktívan részt vesz az életkorának és érdeklődésének megfelelő gyermek-, illetve ifjúsági irodalmi alkotások közös előadásában.

*  Egyre magabiztosabban kapcsolódik be történetek kreatív alakításába, átfogalmazásába kooperatív munkaformában.

*  Elmesél rövid történetet, egyszerűsített olvasmányt egyszerű nyelvi eszközökkel, önállóan, a cselekményt lineárisan összefűzve.

*  Egyszerű nyelvi eszközökkel, felkészülést követően röviden, összefüggően beszél az ajánlott tématartományokhoz tartozó témákban, élőszóban és digitális felületen.

*  Képet jellemez röviden, egyszerűen, ismert nyelvi fordulatok segítségével, segítő tanári kérdések alapján, önállóan.

*  Változatos, kognitív kihívást jelentő szóbeli feladatokat old meg önállóan vagy kooperatív munkaformában, a tanult nyelvi eszközökkel, szükség szerint tanári segítséggel, élőszóban és digitális felületen.

*  Megold játékos és változatos írásbeli feladatokat rövid szövegek szintjén.

*  Rövid, egyszerű, összefüggő szövegeket ír a tanult nyelvi szerkezetek felhasználásával az ismert szövegtípusokban, az ajánlott tématartományokban.

*  Rövid szövegek írását igénylő kreatív munkát hoz létre önállóan.

*  Rövid, összefüggő, papíralapú vagy ikt-eszközökkel segített írott projektmunkát készít önállóan vagy kooperatív munkaformákban.

*  A szövegek létrehozásához nyomtatott, illetve digitális alapú segédeszközt, szótárt használ.

*  Megérti a szintjének megfelelő, kevésbé ismert elemekből álló, nonverbális vagy vizuális eszközökkel támogatott célnyelvi óravezetést és utasításokat, kérdéseket.

*  Értelmezi az életkorának és nyelvi szintjének megfelelő, egyszerű, hangzó szövegben a tanult nyelvi elemeket.

*  Értelmezi az életkorának megfelelő, élőszóban vagy digitális felületen elhangzó szövegekben a beszélők gondolatmenetét.

*  Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó, élőszóban vagy digitális felületen elhangzó rövid szöveg tartalmát.

*  Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő, élőszóban vagy digitális felületen elhangzó szövegből, és azokat összekapcsolja egyéb ismereteivel.

*  Alkalmazza az életkorának és nyelvi szintjének megfelelő hangzó szöveget a változatos nyelvórai tevékenységek és a feladatmegoldás során.

*  Értelmez életkorának megfelelő nyelvi helyzeteket hallott szöveg alapján.

*  Felismeri a főbb, életkorának megfelelő hangzószöveg-típusokat.

*  Hallgat az érdeklődésének megfelelő autentikus szövegeket elektronikus, digitális csatornákon, tanórán kívül is, szórakozásra vagy ismeretszerzésre.

*  Értelmezi az életkorának megfelelő szituációkhoz kapcsolódó, írott szövegekben megjelenő összetettebb információkat.

*  Megérti a nem kizárólag ismert nyelvi elemeket tartalmazó rövid írott szöveg tartalmát.

*  Kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő szövegből, és azokat összekapcsolja más iskolai vagy iskolán kívül szerzett ismereteivel.

*  Megkülönbözteti a főbb, életkorának megfelelő írott szövegtípusokat.

*  Összetettebb írott instrukciókat értelmez.

*  Alkalmazza az életkorának és nyelvi szintjének megfelelő írott, nyomtatott vagy digitális alapú szöveget a változatos nyelvórai tevékenységek és feladatmegoldás során.

*  A nyomtatott vagy digitális alapú írott szöveget felhasználja szórakozásra és ismeretszerzésre önállóan is.

*  Érdeklődése erősödik a célnyelvi irodalmi alkotások iránt.

*  Megért és használ szavakat, szókapcsolatokat a célnyelvi, az életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel kapcsolatban

*  Kommunikációt kezdeményez egyszerű hétköznapi témában, a beszélgetést követi, egyszerű, nyelvi eszközökkel fenntartja és lezárja.

*  Az életkorának megfelelő mindennapi helyzetekben a tanult nyelvi eszközökkel megfogalmazott kérdéseket tesz fel, és válaszol a hozzá intézett kérdésekre.

*  Véleményét, gondolatait, érzéseit egyre magabiztosabban fejezi ki a tanult nyelvi eszközökkel.

*  A tanult nyelvi elemeket többnyire megfelelően használja, beszédszándékainak megfelelően, egyszerű spontán helyzetekben.

*  Váratlan, előre nem kiszámítható eseményekre, jelenségekre és történésekre is reagál egyszerű célnyelvi eszközökkel, személyes vagy online interakciókban.

*  Bekapcsolódik a tanórán az interakciót igénylő nyelvi tevékenységekbe, abban társaival közösen részt vesz, a begyakorolt nyelvi elemeket tanári segítséggel a játék céljainak megfelelően alkalmazza.

*  Üzeneteket ír,

*  Véleményét írásban, egyszerű nyelvi eszközökkel megfogalmazza, és arról írásban interakciót folytat.

*  Rövid, egyszerű, ismert nyelvi eszközökből álló kiselőadást tart változatos feladatok kapcsán, hagyományos vagy digitális alapú vizuális eszközök támogatásával.

*  Felhasználja a célnyelvet tudásmegosztásra.

*  Találkozik az életkorának és nyelvi szintjének megfelelő célnyelvi ismeretterjesztő tartalmakkal.

*  Néhány szóból vagy mondatból álló jegyzetet készít írott szöveg alapján.

*  Egyszerűen megfogalmazza személyes véleményét, másoktól véleményük kifejtését kéri, és arra reagál, elismeri vagy cáfolja mások állítását, kifejezi egyetértését vagy egyet nem értését.

*  Kifejez tetszést, nem tetszést, akaratot, kívánságot, tudást és nem tudást, ígéretet, szándékot, dicséretet, kritikát.

*  Információt cserél, információt kér, információt ad.

*  Kifejez kérést, javaslatot, meghívást, kínálást és ezekre reagálást.

*  Kifejez alapvető érzéseket, például örömöt, sajnálkozást, bánatot, elégedettséget, elégedetlenséget, bosszúságot, csodálkozást, reményt.

*  Kifejez és érvekkel alátámasztva mutat be szükségességet, lehetőséget, képességet, bizonyosságot, bizonytalanságot.

*  Értelmez és használja az idegen nyelvű írott, olvasott és hallott tartalmakat a tanórán kívül is,

*  Felhasználja a célnyelvet ismeretszerzésre.

*  Használja a célnyelvet életkorának és nyelvi szintjének megfelelő aktuális témákban és a hozzájuk tartozó szituációkban.

*  Találkozik életkorának és nyelvi szintjének megfelelő célnyelvi szórakoztató tartalmakkal.

*  Összekapcsolja az ismert nyelvi elemeket egyszerű kötőszavakkal (például: és, de, vagy).

*  Egyszerű mondatokat összekapcsolva mond el egymást követő eseményekből álló történetet, vagy leírást ad valamilyen témáról.

*  A tanult nyelvi eszközökkel és nonverbális elemek segítségével tisztázza mondanivalójának lényegét.

*  Ismeretlen szavak valószínű jelentését szövegösszefüggések alapján kikövetkezteti az életkorának és érdeklődésének megfelelő, konkrét, rövid szövegekben.

*  Alkalmaz nyelvi funkciókat rövid társalgás megkezdéséhez, fenntartásához és befejezéséhez.

*  Nem értés esetén a meg nem értett kulcsszavak vagy fordulatok ismétlését vagy magyarázatát kéri, visszakérdez, betűzést kér.

*  Megoszt alapvető személyes információkat és szükségleteket magáról egyszerű nyelvi elemekkel.

*  Ismerős és gyakori alapvető helyzetekben, akár telefonon vagy digitális csatornákon is, többnyire helyesen és érthetően fejezi ki magát az ismert nyelvi eszközök segítségével.

*  Tudatosan használ alapszintű nyelvtanulási és nyelvhasználati stratégiákat.

*  Hibáit többnyire észreveszi és javítja.

*  Ismer szavakat, szókapcsolatokat a célnyelven a témakörre jellemző, életkorának és érdeklődésének megfelelő más tudásterületen megcélzott tartalmakból.

*  Egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki magának.

*  Céljai eléréséhez megtalálja és használja a megfelelő eszközöket.

*  Céljai eléréséhez társaival párban és csoportban együttműködik.

*  Nyelvi haladását többnyire fel tudja mérni,

*  Társai haladásának értékelésében segítően részt vesz.

*  A tanórán kívüli, akár játékos nyelvtanulási lehetőségeket felismeri, és törekszik azokat kihasználni.

*  Felhasználja a célnyelvet szórakozásra és játékos nyelvtanulásra.

*  Digitális eszközöket és felületeket is használ nyelvtudása fejlesztésére,

*  Értelmez egyszerű, szórakoztató kisfilmeket

*  Megismeri a célnyelvi országok főbb jellemzőit és kulturális sajátosságait.

*  További országismereti tudásra tesz szert.

*  Célnyelvi kommunikációjába beépíti a tanult interkulturális ismereteket.

*  Találkozik célnyelvi országismereti tartalmakkal.

*  Találkozik a célnyelvi, életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb hírekkel, eseményekkel.

*  Megismerkedik hazánk legfőbb országismereti és történelmi eseményeivel célnyelven.

*  A célnyelvi kultúrákhoz kapcsolódó alapvető tanult nyelvi elemeket használja.

*  Idegen nyelvi kommunikációjában ismeri és használja a célnyelv főbb jellemzőit.

*  Következetesen alkalmazza a célnyelvi betű és jelkészletet

*  Egyénileg vagy társaival együttműködve szóban vagy írásban projektmunkát vagy kiselőadást készít, és ezeket digitális eszközök segítségével is meg tudja valósítani.

*  Találkozik az érdeklődésének megfelelő akár autentikus szövegekkel elektronikus, digitális csatornákon tanórán kívül is.

## Mozgóképkultúra és médiaismeret 
### 12. évfolyamon
*  Rendelkezik átfogó egyetemes és magyar médiatörténeti ismeretekkel. ismeri a különböző médiumok specifikumait, a fontosabb műfajokat, szerzőket, műveket.

*  Képes különböző médiatermékek értő befogadására. ismeri a különböző médiumok formanyelvének alapvető eszköztárát, érzékeli ezek hatását az értelmezés folyamatára, valamint képes ezeknek az eszközöknek alapszintű alkalmazására is saját környezetében.

*  Rövid média-alkotások tervezése és kivitelezése révén szert tesz a különböző médiumok szövegalkotási folyamatainak elemi tapasztalataira, érti a különféle szövegtípusok eltérő működési elvét, s tud azok széles spektrumával kreatív befogadói viszonyt létrehozni.

*  Ismereteket szerez a filmkultúra területéről: érti a szerzői kultúra és a tömegkultúra eltérő működésmódját; felismeri az elterjedtebb filmműfajok jegyeit; különbséget tud tenni a különböző stílusirányzatokhoz tartozó alkotások között.

*  Ismeri a magyar filmművészet főbb szerzőit, jellegzetességeit és értékeit.

*  Tisztában van a sztárfogalom kialakulásával és módosulásával. azonosítani tudja a sztárjelenséget a filmen és a médiában. képes ismert filmszereplők és médiaszemélyiségek imázsának elemzésére, a háttérben fellelhető archetípusok meghatározására.

*  Ismeri a modern tömegkommunikáció fő működési elveit, jellemző vonásait, érti, milyen társadalmi és kulturális következményekkel járnak a kommunikációs rendszerben bekövetkező változások, ezek hatásait saját környezetében is észreveszi.

*  Értelmezni tudja a valóság és a média által a valóságról kialakított „kép" viszonyát, ismeri a reprezentáció fogalmát, és rendelkezik a médiatudatosság képességével.

*  Ismeri a médiareprezentáció, valószerűség és hitelesség kritériumait, a fikciós műfajok illetve a dokumentumjelleg különbségeit, a sztereotípia, tematizáció, valóságábrázolás, hitelesség, hír fogalmait. képes saját értékelő viszonyt kialakítani ezekkel kapcsolatban.

*  Tisztában van a médiaipar, médiafogyasztás és -befogadás jellemzőivel. ismeri a kereskedelmi, közszolgálati és nonprofit média, az alkotói szándék, célcsoport, a közönség, mint vevő és áru, a médiafogyasztási szokások jellegzetességeit, a médiafüggőség tüneteit.

*  Ismeri az internet-világ jelenségeit, a globális kommunikáció adta lehetőségeket. érti a hálózati kommunikáció és a közösségi média működési módját, képes abban felelősen részt venni, ismeri és megfelelően használja annak alapvető szövegtípusait. képes igényes önálló tartalmak alkotására és részt venni a lokális nyilvánosságban.

*  Ismeretekkel rendelkezik a médiaetika és a médiaszabályozás főbb kérdéseit illetően, saját álláspontot tud megfogalmazni azokkal kapcsolatban. érti a média etikai környezetének és jogi szabályozásának tétjeit.

*  Tisztában van a média véleményformáló szerepével, az alkotók és felhasználók felelősségével, az egyének és közösségek jogaival. ismeri a norma és normaszegés fogalmait a médiában.

*  Tudatosítja magában a nyilvános megszólalás szabadságával és felelősségével járó lehetőségeket és lehetséges következményeket, tisztában van a digitális zaklatás veszélyeivel. tudatos médiahasználóként állást tud foglalni a média által közvetített értékek minőségével kapcsolatban.

## Pénzügyi és vállalkozói ismeretek
### 10. évfolyamon
* A tanuló érti a nemzetgazdaság szereplőinek (háztartások, vállalatok, állam, pénzintézetek) feladatait, a köztük lévő kapcsolatrendszer sajátosságait.
* Tudja értelmezni az állam gazdasági szerepvállalásának jelentőségét, ismeri főbb feladatait, azok hatásait.
* Tisztában van azzal, hogy az adófizetés biztosítja részben az állami feladatok ellátásnak pénzügyi fedezetét.
* Ismeri a mai bankrendszer felépítését, az egyes pénzpiaci szereplők főbb feladatait.
* Képes választani az egyes banki lehetőségek közül.
* Tisztában van az egyes banki ügyletek előnyeivel, hátrányaival, kockázataival.
* A bankok kínálatából bankot, bankszámla csomagot tud választani.
* Tud érvelni a családi költségvetés mellett, a tudatos, hatékony pénzgazdálkodás érdekében.
* Önismereti tesztek, játékok segítségével képes átgondolni milyen foglalkozások, tevékenységek illeszkednek személyiségéhez.
* Tisztában van az álláskeresés folyamatával, a munkaviszonnyal kapcsolatos jogaival, kötelezettségeivel.
* Ismer vállalkozókat, vállalatokat, össze tudja hasonlítani az alkalmazotti, és a vállalkozói személyiségjegyeket.
* Érti a leggyakoribb vállalkozási formák jellemzőit, előnyeit, hátrányait.
* Tisztában van a nem nyereségérdekelt szervezetek gazdaságban betöltött szerepével.
* Ismeri a vállalkozásalapítás, -működtetés legfontosabb lépéseit, képes önálló vállalkozói ötlet kidolgozására.
* Meg tudja becsülni egy vállalkozás lehetséges költségeit, képes adott időtartamra költségkalkulációt tervezni.
* Tisztában van az üzleti tervezés szükségességével, mind egy új vállalkozás alapításakor, mind már meglévő vállalkozás működése esetén.
* Tájékozott az üzleti terv tartalmi elemeiről.
* Megismeri a nem üzleti (társadalmi, kulturális, egyéb civil) kezdeményezések pénzügyi-gazdasági igényeit, lehetőségeit.
* Felismeri a kezdeményezőkészség jelentőségét az állampolgári felelősségvállalásban.
* Felismeri a sikeres vállalkozás jellemzőit, képes azonosítani az esetleges kudarc okait, javaslatot tud tenni a problémák megoldására.
## Szoftverfejlesztés és -tesztelés
### 9-10. évfolyamon
* Adott kapcsolási rajz alapján egyszerűbb áramköröket épít próbapanel segítségével vagy forrasztásos technológiával.
* Ismeri az elektronikai alapfogalmakat, kapcsolódó fizikai törvényeket, alapvető alkatrészeket és kapcsolásokat.
* A funkcionalitás biztosítása mellett törekszik az esztétikus kialakításra (pl. minőségi forrasztás, egyenletes alkatrész sűrűség, olvashatóság).
* Az elektromos berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
* Alapvető villamos méréseket végez önállóan a megépített áramkörökön.
* Ismeri az elektromos mennyiségek mérési metódusait, a mérőműszerek használatát.
* Elvégzi a számítógépen és a mobil eszközökön az operációs rendszer (pl. Windows, Linux, Android, iOS), valamint az alkalmazói szoftverek telepítését, frissítését és alapszintű beállítását. Grafikus felületen, valamint parancssorban használja a Windows, és Linux operációs rendszerek alapszintű parancsait és szolgáltatásait (pl. állomány- és könyvtárkezelési műveletek, jogosultságok beállítása, szövegfájlokkal végzett műveletek, folyamatok kezelése).
* Ismeri a számítógépen és a mobil informatikai eszközökön használt operációs rendszerek telepítési és frissítési módjait, alapvető parancsait és szolgáltatásait, valamint alapvető beállítási lehetőségeit.
* Törekszik a felhasználói igényekhez alkalmazkodó szoftverkörnyezet kialakítására.
* Önállóan elvégzi a kívánt szoftverek telepítését, szükség esetén gondoskodik az eszközön korábban tárolt adatok biztonsági mentéséről.
* Elvégzi a PC perifériáinak csatlakoztatását, szükség esetén új alkatrészt szerel be vagy alkatrészt cserél egy számítógépben.
* Ismeri az otthoni és irodai informatikai környezetet alkotó legáltalánosabb összetevők (PC, nyomtató, mobiltelefon, WiFi router stb.) szerepét, alapvető működési módjukat. Ismeri a PC és a mobil eszközök főbb alkatrészeit (pl. alaplap, CPU, memória) és azok szerepét.
* Törekszik a végrehajtandó műveletek precíz és előírásoknak megfelelő elvégzésére.
* Az informatikai berendezésekre vonatkozó munka- és balesetvédelmi szabályokat a saját és mások testi épsége érdekében betartja és betartatja.
* Alapvető karbantartási feladatokat lát el az általa megismert informatikai és távközlési berendezéseken (pl. szellőzés és csatlakozások ellenőrzése, tisztítása).
* Tisztában van vele, hogy miért szükséges az informatikai és távközlési eszközök rendszeres és eseti karbantartása. Ismeri legalapvetőbb karbantartási eljárásokat.
* A hibamentes folyamatos működés elérése érdekében fontosnak tartja a megelőző karbantartások elvégzését.
* Otthoni vagy irodai hálózatot alakít ki WiFi router segítségével, elvégzi WiFi router konfigurálását, a vezetékes- és vezeték nélküli eszközök (PC, mobiltelefon, set-top box stb.), csatlakoztatását és hálózati beállítását.
* Ismeri az informatikai hálózatok felépítését, alapvető technológiáit (pl. Ethernet), protokolljait (pl. IP, HTTP) és szabványait (pl. 802.11-es WiFi szabványok). Ismeri az otthoni és irodai hálózatok legfontosabb összetevőinek (kábelezés, WiFi router, PC, mobiltelefon stb.) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
* Törekszik a felhasználói igények megismerésére, megértésére, és szem előtt tartja azokat a hálózat kialakításakor.
* Néhány alhálózatból álló kis- és közepes vállalati hálózatot alakít ki forgalomirányító és kapcsoló segítségével, elvégzi az eszközök alapszintű hálózati beállításait (pl. forgalomirányító interfészeinek IP-cím beállítása, alapértelmezett átjáró beállítása).
* Ismeri a kis- és közepes vállalati hálózatok legfontosabb összetevőinek (pl. kábelrendező szekrény, kapcsoló, forgalomirányító) szerepét, jellemzőit, csatlakozási módjukat és alapszintű hálózati beállításait.
* Alkalmazza a hálózatbiztonsággal kapcsolatos legfontosabb irányelveket (pl. erős jelszavak használata, vírusvédelem alkalmazása, tűzfal használat).
* Ismeri a fontosabb hálózatbiztonsági elveket, szabályokat, támadás típusokat, valamint a szoftveres és hardveres védekezési módszereket.
* Megkeresi és elhárítja az otthoni és kisvállalati informatikai környezetben jelentkező hardveres és szoftveres hibákat.
* Ismeri az otthoni és kisvállalati informatikai környezetben leggyakrabban felmerülő hibákat (pl. hibás IP-beállítás, kilazult csatlakozó) és azok elhárításának módjait.
* Önállóan behatárolja a hibát. Egyszerűbb problémákat önállóan, összetettebbeket szakmai irányítással hárít el.
* Internetes források és tudásbázisok segítségével követi, valamint feladatainak elvégzéséhez lehetőség szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
* Naprakész információkkal rendelkezik a legmodernebb információs technológiákkal és trendekkel kapcsolatban.
* Nyitott és érdeklődő a legmodernebb információs technológiák és trendek iránt.
* Önállóan szerez információkat a témában releváns szakmai platformokról.
* Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével.
* Ismeri a HTML5, a CSS3 alapvető elemeit, a stíluslapok fogalmát, felépítését. Érti a reszponzív megjelenítéshez használt módszereket, keretrendszerek előnyeit, a reszponzív webdizájn alapelveit.
* A felhasználói igényeknek megfelelő funkcionalitás és design összhangjára törekszik.
* Önállóan létrehozza és megformázza a weboldalt.
* Munkája során jelentkező problémák kezelésére vagy folyamatok automatizálására egyszerű alkalmazásokat készít Python programozási nyelv segítségével.
* Ismeri a Python nyelv elemeit, azok céljait (vezérlési szerkezetek, adatszerkezetek, változók, aritmetikai és logikai kifejezések, függvények, modulok, csomagok). Ismeri az algoritmus fogalmát, annak szerepét.
* Jól átlátható kódszerkezet kialakítására törekszik.
* Önállóan készít egyszerű alkalmazásokat.
* Git verziókezelő rendszert, valamint fejlesztést és csoportmunkát támogató online eszközöket és szolgáltatásokat (pl.: GitHub, Slack, Trello, Microsoft Teams, Webex Teams) használ.
* Ismeri a Git, valamint a csoportmunkát támogató eszközök és online szolgáltatások célját, működési módját, legfontosabb funkcióit.
* Törekszik a feladatainak megoldásában a hatékony csoportmunkát támogató online eszközöket kihasználni.
* A Git verziókezelőt, valamint a csoportmunkát támogató eszközöket és szolgáltatásokat önállóan használja.
* Társaival hatékonyan együttműködve, csapatban dolgozik egy informatikai projekten. A projektek végrehajtása során társaival tudatosan és célirányosan kommunikál.
* Ismeri a projektmenedzsment lépéseit (kezdeményezés, követés, végrehajtás, ellenőrzés, dokumentáció, zárás).
* Más munkáját és a csoport belső szabályait tiszteletben tartva, együttműködően vesz részt a csapatmunkában.
* A projektekben irányítás alatt, társaival közösen dolgozik. A ráosztott feladatrészt önállóan végzi el.
* Munkája során hatékonyan használja az irodai szoftvereket.
* Ismeri az irodai szoftverek főbb funkcióit, felhasználási területeit.
* Az elkészült termékhez prezentációt készít és bemutatja, előadja azt munkatársainak, vezetőinek, ügyfeleinek.
* Ismeri a hatékony prezentálás szabályait, a prezentációs szoftverek lehetőségeit.
* Törekszik a tömör, lényegre törő, de szakszerű bemutató összeállítására.
* A projektcsapat tagjaival egyeztetve, de önállóan elkészíti az elvégzett munka eredményét bemutató prezentációt.
### 11-13. évfolyamon
* Használja a Git verziókezelő rendszert, valamint a fejlesztést támogató csoportmunkaeszközöket és szolgáltatásokat (pl. GitHub, Slack, Trello, Microsoft Teams, Webex Teams).
* Ismeri a legelterjedtebb csoportmunkaeszközöket, valamint a Git verziókezelőrendszer szolgáltatásait.
* Igyekszik munkatársaival hatékonyan, igazi csapatjátékosként együtt dolgozni. Törekszik a csoporton belül megkapott feladatok precíz, határidőre történő elkészítésére, társai segítésére.
* Szoftverfejlesztési projektekben irányítás alatt dolgozik, a rábízott részfeladatok megvalósításáért felelősséget vállal.
* Az általa végzett szoftverfejlesztési feladatok esetében kiválasztja a legmegfelelőbb technikákat, eljárásokat és módszereket.
* Elegendő ismerettel rendelkezik a meghatározó szoftverfejlesztési technológiák (programozási nyelvek, keretrendszerek, könyvtárak stb.), illetve módszerek erősségeiről és hátrányairól.
* Nyitott az új technológiák megismerésére, tudását folyamatosan fejleszti.
* Önállóan dönt a fejlesztés során használt technológiákról és eszközökről.
* A megfelelő kommunikációs forma (e-mail, chat, telefon, prezentáció stb.) kiválasztásával munkatársaival és az ügyfelekkel hatékonyan kommunikál műszaki és egyéb információkról magyarul és angolul.
* Ismeri a különböző kommunikációs formákra (e-mail, chat, telefon, prezentáció stb.) vonatkozó etikai és belső kommunikációs szabályokat.
* Angol nyelvismerettel rendelkezik (KER B1 szint). Ismeri a gyakran használt szakmai kifejezéseket angolul.
* Kommunikációjában konstruktív, együttműködő, udvarias. Feladatainak a felhasználói igényeknek leginkább megfelelő, minőségi megoldására törekszik.
* Felelősségi körébe tartozó feladatokkal kapcsolatban a vállalati kommunikációs szabályokat betartva, önállóan kommunikál az ügyfelekkel és munkatársaival.
* Szabványos, reszponzív megjelenítést biztosító weblapokat hoz létre és formáz meg stíluslapok segítségével. Kereső optimalizálási beállításokat alkalmaz.
* Ismeri a HTML5 és a CSS3 szabvány alapvető nyelvi elemeit és eszközeit (strukturális és szemantikus HTML-elemek, attribútumok, listák, táblázatok, stílus jellemzők és függvények). Ismeri a a reszponzív webdizájn alapelveit és a Bootstrap keretrendszer alapvető szolgáltatásait.
* Törekszik a weblapok igényes és a használatot megkönnyítő kialakítására.
* Kisebb webfejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
* Egyszerűbb webhelyek dinamikus viselkedését (eseménykezelés, animáció stb.) biztosító kódot, készít JavaScript nyelven.
* Alkalmazási szinten ismeri a JavaScript alapvető nyelvi elemeit, valamint az aszinkron programozás és az AJAX technológia működési elvét. Tisztában van a legfrissebb ECMAScript változatok (ES6 vagy újabb) hatékonyság növelő funkcióival.
* Egyszerűbb JavaScript programozási feladatokat önállóan végez el.
* RESTful alkalmazás kliens oldali komponensének fejlesztését végzi JavaScript nyelven.
* Tisztában van a REST szoftverarchitektúra elvével, alkalmazás szintjén ismeri az AJAX technológiát.
* A tiszta kód elveinek megfelelő, megfelelő mennyiségű megjegyzéssel ellátott, kellőképpen tagolt, jól átlátható, kódot készít.
* Ismeri a tiszta kód készítésének alapelveit.
* Törekszik arra, hogy az elkészített kódja jól átlátható, és mások számára is értelmezhető legyen.
* Adatbázis-kezelést is végző konzolos vagy grafikus felületű asztali alkalmazást készít magas szintű programozási nyelvet (C#, Java) használva.
* Ismeri a választott magas szintű programozási nyelv alapvető nyelvi elemeit, illetve a hozzá tartozó fejlesztési környezetet.
* Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
* Kisebb asztali alkalmazás-fejlesztési projekteken önállóan, összetettebbekben részfeladatokat megvalósítva, irányítás mellett dolgozik.
* Adatkezelő alkalmazásokhoz relációs adatbázist tervez és hoz létre, többtáblás lekérdezéseket készít.
* Tisztában van a relációs adatbázis-tervezés és -kezelés alapelveivel. Haladó szinten ismeri a különböző típusú SQL lekérdezéseket, azok nyelvi elemeit és lehetőségeit.
* Törekszik a redundanciamentes, világos szerkezetű, legcélravezetőbb kialakítású adatbázis szerkezet megvalósítására.
* Kisebb projektekhez néhány táblás adatbázist önállóan tervez meg, nagyobb projektekben a biztosított adatbáziskörnyezetet használva önállóan valósít meg lekérdezéseket.
* Önálló- vagy komplex szoftverrendszerek részét képző kliens oldali alkalmazásokat fejleszt mobil eszközökre.
* Ismeri a választott mobil alkalmazás fejlesztésére alkalmas nyelvet és fejlesztői környezetet. Tisztában van a mobil alkalmazásfejlesztés alapelveivel.
* Törekszik a felhasználó számára minél könnyebb használatot biztosító felhasználói felület és működési mód kialakítására.
* Kisebb projektek mobil eszközökre optimalizált kliens oldali alkalmazását önállóan megvalósítja meg.
* Webes környezetben futtatható kliens oldali (frontend) alkalmazást készít JavaScript keretrendszer (pl. React, Vue, Angular) segítségével.
* Érti a frontend fejlesztésre szolgáló JavaScript keretrendszerek célját. Meg tudja nevezni a 3-4 legelterjedtebb keretrendszert. Alkalmazás szintjén ismeri a könyvtárak és modulok kezelését végző csomagkezelő rendszereket (package manager, pl. npm, yarn). Ismeri a választott JavaScript keretrendszer működési elvét, nyelvi és strukturális elemeit.
* Törekszik maximálisan kihasználni a választott keretrendszer előnyeit, követi az ajánlott fejlesztési mintákat.
* Kisebb frontend alkalmazásokat önállóan készít el, nagyobb projektekben irányítás mellett végzi el a kijelölt komponensek fejlesztését.
* RESTful alkalmazás adatbázis-kezelési feladatokat is ellátó szerveroldali komponensének (backend) fejlesztését végzi erre alkalmas nyelv vagy keretrendszer segítségével (pl. Node.js, Spring, Laravel).
* Érti a RESTful szoftverarchitektúra lényegét. Tisztában van legalább egy backend készítésére szolgáló nyelv vagy keretrendszer működési módjával, nyelvi és strukturális elemeivel. Alkalmazás szintjén ismeri az objektum-relációs leképzés technológiát (ORM).
* Igyekszik backend működését leíró precíz, a frontend fejlesztők számára könnyen értelmezhető dokumentáció készítésére.
* Kisebb backend alkalmazásokat önállóan készít el, nagyobb projektekben részletes specifikációt követve, irányítás mellett végzi el a kijelölt komponensek fejlesztését.
* Objektum orientált (OOP) programozási módszertant alkalmazó asztali, webes és mobil alkalmazást készít.
* Ismeri az objektumorientált programozás elvét, tisztában van az öröklődés, a polimorfizmus, a metódus/konstruktor túlterhelés fogalmával.
* Törekszik az OOP technológia nyújtotta előnyök kihasználására, valamint igyekszik követni az OOP irányelveket és ajánlásokat.
* Kisebb projektekben önállóan tervezi meg a szükséges osztályokat, nagyobb projektekben irányítás mellett, a projektben a projektcsapat által létrehozott osztálystruktúrát használva, illetve azt kiegészítve végzi a fejlesztést.
* Tartalomkezelő rendszer (CMS, pl. WordPress) segítségével webhelyet készít, egyéni problémák megoldására saját beépülőket hoz létre.
* Ismeri a tartalomkezelő-rendszerek célját és alapvető szolgáltatásait. Ismeri a beépülők célját és alkalmazási területeit.
* Törekszik az igényes kialakítású és a felhasználók számára könnyű használatot biztosító webhelyek kialakításra.
* Kevésbé összetett portálokat igényes vizuális megjelenést biztosító sablonok, valamint magas funkcionalitást biztosító beépülők használatával önállóan valósít meg. Összetettebb projekteken irányítás mellett, grafikus tervezőkkel, UX szakemberekkel és más fejlesztőkkel együttműködve dolgozik.
* Manuális és automatizált szoftvertesztelést végezve ellenőrzi a szoftver hibátlan működését, dokumentálja a tesztek eredményét.
* Ismeri a unit tesztelés, valamint más tesztelési, hibakeresési technikák alapelveit és alapvető eszközeit.
* Törekszik a mindenre kiterjedő, az összes lehetséges hibát felderítő tesztelésre, valamint a tesztek körültekintő dokumentálására.
* Saját fejlesztésként megvalósított kisebb projektekben önállóan végzi a tesztelést, tesztelői szerepben nagyobb projektekben irányítás mellett végez meghatározott tesztelési feladatokat.
* Szoftverfejlesztés vagy -tesztelés során felmerülő problémákat old meg és hibákat hárít el webes kereséssel és internetes tudásbázisok használatával (pl. Stack Overflow).
* Ismeri a hibakeresés szisztematikus módszereit, a problémák elhárításának lépéseit.
* Ismeri a munkájához kapcsolódó internetes keresési módszereket és tudásbázisokat.
* Törekszik a hibák elhárítására, megoldására, és arra, hogy azokkal lehetőség szerint ne okozzon újabb hibákat.
* Internetes információszerzéssel önállóan old meg problémákat és hárít el hibákat.
* Munkája során hatékonyan használja az irodai szoftvereket, műszaki tartalmú dokumentumokat és bemutatókat készít.
* Ismeri az irodai szoftverek haladó szintű szolgáltatásait.
* Precízen készíti el a műszaki tartalmú dokumentációkat, prezentációkat. Törekszik arra, hogy a dokumentumok könnyen értelmezhetők és mások által is szerkeszthetők legyenek.
* Felelősséget vállal az általa készített műszaki tartalmú dokumentációkért.
* Munkája során cél szerint alkalmazza a legmodernebb információs technológiákat és trendeket (virtaulizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
* Alapszintű alkalmazási szinten ismeri a legmodernebb információs technológiákat és trendeket (virtualizáció, felhőtechnológia, IoT, mesterséges intelligencia, gépi tanulás stb.).
* Nyitott az új technológiák megismerésére, és törekszik azok hatékony, a felhasználói igényeknek és a költséghatékonysági elvárásoknak megfelelő felhasználására a szoftverfejlesztési feladatokban.
* Részt vesz szoftverrendszerek ügyfeleknél történő bevezetésében, a működési környezetet biztosító IT-környezet telepítésében és beállításában.
* Ismeri a számítógép és a mobil informatikai eszközök felépítését (főbb komponenseket, azok feladatait) és működését. Ismeri az eszközök operációs rendszerének és alkalmazói szoftvereinek telepítési és beállítási lehetőségeit.
* A szoftverrendszerek bevezetése és a működési környezet kialakítása során törekszik az ügyfelek elvárásainak megfelelni, valamint tiszteletben tartja az ügyfél vállalati szabályait.
* Az elvégzett eszköz- és szoftvertelepítésekért felelősséget vállal.
* A szoftverfejlesztés és tesztelési munkakörnyezetének kialakításához beállítja a hálózati eszközöket, elvégzi a vezetékes és vezetéknélküli eszközök csatlakoztatását és hálózatbiztonsági beállítását. A fejlesztett szoftverben biztonságos, HTTPS protokollt használó webes kommunikációt valósít meg.
* Ismeri az IPv4 és IPv6 címzési rendszerét és a legalapvetőbb hálózati protokollok szerepét és működési módját (IP, TCP, UDP, DHCP, HTTP, HTTPS, telnet, ssh, SMTP, POP3, IMAP4, DNS, TLS/SSL stb.). Ismeri a végponti berendezések IP-beállítási és hibaelhárítási lehetőségeit. Ismeri az otthoni és kisvállalati hálózatokban működő multifunkciós forgalomirányítók szolgáltatásait, azok beállításának módszereit.
## Technika és tervezés 
### 1-4. évfolyamon
*  Elkülöníti a természeti és mesterséges környezet jellemzőit.

*  Felismeri, hogy tevékenységei során változtatni tud a közvetlen környezetén.

*  Kitartó a munkavégzésben, szükség esetén segítséget kér, segítséget ad.

*  Szöveg vagy rajz alapján épít, tárgyakat készít, alkalmazza a tanult munkafolyamatokat, terveit megosztja.

*  Munkafolyamatokat, technológiákat segítséggel algoritmizál.

*  Megadott szempontok mentén értékeli saját, a társak, a csoport munkáját, viszonyítja a kitűzött célokhoz.

*  Alkotótevékenysége során megéli, megismeri a jeles napokat, ünnepeket, hagyományokat mint értékeket.

*  Tevékenysége során munkatapasztalatot szerez, megéli az alkotás örömét, az egyéni és csapatsiker élményét.

*  Felismeri az egymásért végzett munka fontosságát, a munkamegosztás értékét.

*  Az anyagok tulajdonságairól érzékszervi úton, önállóan szerez ismereteket -- szín, alak, átlátszóság, szag, keménység, rugalmasság, felületi minőség.

*  Alkotótevékenysége során figyelembe veszi az anyag tulajdonságait, felhasználhatóságát.

*  Adott szempontok alapján egyszerűbb tárgyakat önállóan tervez, készít, alkalmazza a tanult munkafolyamatokat.

*  Egyszerű szöveges, rajzos és képi utasításokat hajt végre a tevékenysége során.

*  Alkotótevékenysége során előkészítő, alakító, szerelő és felületkezelő műveleteket végez el.

*  Saját és társai tevékenységét a kitűzött célok mentén, megadott szempontok szerint reálisan értékeli.

*  Értékelés után megfogalmazza tapasztalatait, következtetéseket von le a későbbi eredményesebb munkavégzés érdekében.

*  Felismeri, hogy tevékenysége során tud változtatni közvetlen környezetén, megóvhatja, javíthat annak állapotán.

*  Rendet tart a környezetében.

*  Törekszik a takarékos anyagfelhasználásra.

*  Szelektíven gyűjti a hulladékot.

*  Rendelkezik az életkorának megfelelő szintű probléma-felismerési, probléma-megoldási képességgel.

*  Ismeri a családellátó tevékenységeket, melyek keretében vállalt feladatait az iskolai önellátás során munkamegosztásban végzi -- terítés, rendrakás, öltözködés, növények, állatok gondozása stb..

*  Otthoni és iskolai környezetének, tevékenységeinek balesetveszélyes helyzeteit felismeri, és ismeri megelőzésük módját.

*  Takarékosan gazdálkodik az anyaggal, energiával, idővel.

*  Ismeri a tudatos vásárlás néhány fontos elemét.

*  Ismeri az egészségmegőrzés tevékenységeit.

*  Ismeri és használni, alkalmazni tudja a legfontosabb közlekedési lehetőségeket, szabályokat, viselkedési elvárásokat.

*  Tudatosan megtartja az egészséges és biztonságos munkakörnyezetét.

*  Az elvárt feladatokban önállóan dolgozik -- elvégzi a műveletet.

*  Társaival munkamegosztás szerint együttműködik a csoportos munkavégzés során.

*  Felismeri az egymásért végzett munka fontosságát, a munkamegosztás értékét.

*  Ismeri a környezetében fellelhető, megfigyelhető szakmák, hivatások jellemzőit.

### 5-7. évfolyamon
*  Ismeri a felhasznált anyagok vizsgálati lehetőségeit és módszereit, tulajdonságait, és céljainak megfelelően választ a rendelkezésre álló anyagokból.

*  Tevékenységét megtervezi, terveit másokkal megosztja.

*  Ismeri és betartja az alapvető munkavédelmi szabályokat.

*  Tervek mentén folytatja alkotótevékenységét.

*  Célszerűen választja ki és rendeltetésszerűen használja a szükséges szerszámokat, eszközöket, digitális alkalmazásokat.

*  Törekszik a balesetmentes tevékenységre, a munkaterületen rendet tart.

*  Munkavégzéskor szabálykövető, kooperatív magatartást követ.

*  Ismeri az egyes műveletek jelentőségét a munka biztonságának, eredményességének vonatkozásában.

*  A tevékenység során társaival együttműködik, feladatmegosztás szerint tevékenykedik.

*  Az elkészült produktumot használatba veszi, a tervhez viszonyítva értékeli saját, mások és a csoport munkáját.

*  Értékeli az elvégzett munkákat, az értékelésben elhangzottakat felhasználja a későbbi munkavégzés során.

*  Értékként tekint saját és mások alkotásaira, a létrehozott produktumokra.

*  Felismeri az emberi cselekvés jelentőségét és felelősségét a környezetalakításban.

*  Önállóan szerez információt megfigyelés, vizsgálat, adatgyűjtés útján.

*  Környezeti, fenntarthatósági szempontokat is mérlegelve, céljainak megfelelően választ a rendelkezésre álló anyagokból.

*  Tevékenységét önállóan vagy társakkal együttműködve tervezi.

*  Terveit a műszaki kommunikáció alkalmazásával osztja meg.

*  A terv szerinti lépések megtartásával, önellenőrzéssel halad alkotótevékenységében.

*  Alkalmazza a forma és funkció összefüggéseit, önállóan választ szerszámot, eszközt.

*  A megismert szerszámokat és eszközöket önállóan, az újakat tanári útmutatással használja.

*  Részt vesz a munkavégzési szabályok megalkotásában, betartja azokat.

*  Csoportmunkában feladatot vállal, részt vesz a döntéshozatalban, és a döntésnek megfelelően tevékenykedik.

*  Felméri és tervezi a tevékenység munkavédelmi szabályait.

*  A csoportban feladata szerint tevékenykedik, tudását megosztja.

*  Adott szempontok mentén értékeli saját és mások munkáját.

*  A használatbavétel során, az eltéréseket kiindulópontként alkalmazva javaslatot tesz produktuma továbbfejlesztésére.

*  Megérti az egyén felelősségét a közös értékteremtésben.

*  Szempontokat határoz meg a környezeti állapot felméréséhez, bizonyos eltéréseket számszerűsít.

*  Érti és értékeli a globális változásokat érintő lehetséges megoldások és az emberi tevékenység szerepét, jelentőségét.

*  Tevékenységének tervezésénél és értékelésénél figyelembe vesz környezeti szempontokat.

*  Felismeri a technikai fejlődés és a társadalmi, gazdasági fejlődés kapcsolatát.

*  A problémamegoldás során önállóan vagy társakkal együtt fogalmaz meg megoldási alternatívákat.

*  Komplex szempontrendszer mentén választ stratégiát, optimalizál.

*  Holisztikus szemlélettel rendelkezik, az összefüggések megértésére törekszik.

*  Döntéseit tudatosság jellemzi, alternatívákat mérlegel.

*  Felismeri a személyes cselekvés jelentőségét a globális problémák megoldásában.

*  Felismeri saját felelősségét életvezetése megtervezésében és megszervezésében, tudatosan gazdálkodik a rendelkezésre álló anyagi és nem anyagi erőforrásokkal.

*  Rendszerszinten végzi az elemzést és az alkalmazást.

*  Tisztában van a saját, a családi és a társadalmi erőforrásokkal és az azokkal való hatékony és tudatos gazdálkodás módjaival.

*  Egészség- és környezettudatosan dönt és tevékenykedik.

*  Terv szerint tevékenykedik, probléma esetén észszerű kockázatokat felvállal.

*  Önismeretére építve vállal feladatokat, szem előtt tartva a csapat eredményességét.

*  Alkalmazkodik a változó munkafeladatokhoz, szerepelvárásokhoz; vezetőként tudatosan vezeti a csoport döntési folyamatát.

*  Alkalmazza a döntés-előkészítés, döntéshozatal eljárásait, hibás döntésein változtat.

*  Az egyes részfeladatokat rendszerszinten szemléli.

*  Érti a társadalmi munkamegosztás lényegét, az egyes foglalkoztatási ágazatok jelentőségét.

*  Ismeri az egyes modulokhoz kapcsolódó foglalkozások jellemzőit, ezekkel kapcsolatban megfogalmazza saját preferenciáit.

*  A fizikai és digitális környezetből információt gyűjt a számára vonzó foglalkozások alkalmassági és képesítési feltételeiről, keresi a vállalkozási lehetőségeket, a jövedelmezőséget és a jellemző tanulási utakat.

## Természettudomány 
### 4. évfolyamon
*  Ráébredjen a természeti rendszerek és a természetben zajló folyamatok komplexitására, alapvető okaira és magyarázataira.

*  Képessé váljon az önálló ismeretszerzésre, az összefüggések felismerésére és az egyszerű elemzések elvégzésére a tanulói kísérletek, terepi megfigyelések és vizsgálatok révén, azzal, hogy a távlati cél a felsőbb évfolyamokon való értő és önálló munkavégzés lehetőségének megalapozása.

*  Elsajátítsa a természettudományok egységét szem előtt tartó szintetizáló gondolkodásmódot, legyen képes folyamatokat rendszerben szemlélni.

*  Tudjon kritikusan gondolkodni az adott természeti, környezeti problémáról, illetve hogy felismerje az áltudományos információkat, amely nagyban hozzájárul a felelős és tudatos állampolgári szerepvállalás kialakításához.

*  Hozzáférjen a mindennapi életben hasznosítható természettudományos tudáshoz, amelynek révén a mindennapi életükben előforduló egyszerűbb problémákat tudjon megoldani, és kialakuljon benne az értő, felelős döntéshozás képessége.

*  A természetben lejátszódó folyamatok vizsgálatával, a várható következmények megértésével cselekvőképes, a környezetért felelősséggel tenni akaró állampolgárrá váljon, ezzel is hangsúlyozva, hogy az ember egyénként és egy nagyobb közösség részeként egyaránt felelős természeti környezetéért, annak jövőbeni állapotáért.

*  Felismerje és megértse, hogy az élhető jövő záloga a környezettudatos, fenntarthatóságot szem előtt tartó gondolkodás.

*  Tudatos eszközhasználóvá váljon az infokommunikációs eszközök használata és a digitális kompetenciák fejlesztése révén.

*  Segítséget kapjon a későbbi műszaki vagy természettudományos pályaválasztáshoz.

*  Megfigyelési és mérési módszerek

*  Tájékozódás az időben

*  Tájékozódás a térben

*  Élő környezetünk

*  Anyagok és folyamatok

### 5-6. évfolyamon
*  Megfigyeléseket, összehasonlításokat, csoportosításokat, méréseket és kísérleteket végez.

*  Megfigyeléseiről, tapasztalatairól szóban, írásban, rajzban beszámol.

*  A szöveggel, táblázattal és jelekkel kapott információt önállóan értelmezi, azokból következtetéseket von le.

*  Ismeretei bővítéséhez tanári útmutatás mellett kutatásokat végez a nyomtatott és digitális források felhasználásával.

*  Kialakul benne a szűkebb, illetve tágabb környezet iránti felelősségtudat.

*  Kialakul benne a természettudomány iránti érdeklődés.

*  Felismeri és megfigyeli a környezetben előforduló élő és élettelen anyagokat, megadott vagy önállóan kitalált szempontok alapján csoportosítja azokat.

*  Felismer és megfigyel különböző természetes és mesterséges anyagokat, ismeri azok tulajdonságait, felhasználhatóságukat, ismeri a természetes és mesterséges környezetre gyakorolt hatásukat.

*  Önállóan végez becsléseket, méréseket és használ mérőeszközöket különféle fizikai paraméterek meghatározására.

*  Önállóan végez egyszerű kísérleteket.

*  Korábbi tapasztalatai és megfigyelései révén felismeri a víz különböző tulajdonságait, különböző szempontok alapján rendszerezi a vizek fajtáit.

*  Bizonyítja, és hétköznapi példákkal alátámasztja a víz fagyásakor történő térfogat-növekedést.

*  Megfigyeli a különböző halmazállapot-változásokhoz (olvadás, fagyás, párolgás, forrás, lecsapódás) kapcsolódó folyamatokat, példákat gyűjt hozzájuk a természetben, a háztartásban és az iparban.

*  Kísérletek során megfigyeli a különböző halmazállapotú anyagok vízben oldódásának folyamatát.

*  Felismeri az olvadás és az oldódás közötti különbséget kísérleti tapasztalatok alapján.

*  Elsajátítja a tűzveszélyes anyagokkal való bánásmódot, ismeri tűz esetén a szükséges teendőket.

*  Megfigyeli a talaj élő és élettelen alkotóelemeit, tulajdonságait, összehasonlít különböző típusú talajféleségeket, valamint következtetések révén felismeri a talajnak, mint rendszernek a komplexitását.

*  Korábbi tapasztalatai és megfigyelései révén felismeri a levegő egyes tulajdonságait.

*  Vizsgálat révén azonosítja a tipikus lágyszárú és fásszárú növények részeit.

*  Megkülönbözteti a hely- és helyzetváltoztatást és példákat keres ezekre megadott szempontok alapján.

*  Önállóan végez becsléseket, méréseket és használ mérőeszközöket a hőmérséklet, a hosszúság, a tömeg, az űrtartalom és az idő meghatározására.

*  Észleli, méri az időjárási elemeket, a mért adatokat rögzíti, ábrázolja.

*  Magyarországra vonatkozó adatok alapján kiszámítja a napi középhőmérsékletet, a napi és évi közepes hőingást.

*  Leolvassa és értékeli a magyarországra vonatkozó éghajlati diagramok és éghajlati térképek adatait.

*  Megfigyeli a mágneses kölcsönhatásokat, kísérlettel igazolja a vonzás és a taszítás jelenségét, példákat ismer a mágnesesség gyakorlati életben való felhasználására.

*  Megfigyeli a testek elektromos állapotát és a köztük lévő kölcsönhatásokat, ismeri ennek gyakorlati életben való megjelenését.

*  Megfigyeléseken és kísérleten keresztül megismeri az energiatermelésben szerepet játszó anyagokat és az energiatermelés folyamatát.

*  Kísérletekkel igazolja a növények életfeltételeit.

*  Kísérleti úton megfigyeli az időjárás alapvető folyamatait, magyarázza ezek okait és következményeit.

*  Felismeri az idő múlásával bekövetkező változásokat és ezek összefüggéseit az élő és élettelen környezet elemein.

*  Tudja értelmezni az időt különböző dimenziójú skálákon.

*  Tervet készít saját időbeosztására vonatkozóan.

*  Megfigyeli a természet ciklikus változásait.

*  Megérti a föld mozgásai és a napi, évi időszámítás közötti összefüggéseket.

*  Modellezi a nap és a föld helyzetét a különböző napszakokban és évszakokban.

*  Meghatározza az irányt a valós térben.

*  Érti a térkép és a valóság közötti viszonyt.

*  Tájékozódik a térképen és a földgömbön.

*  Mágneses kölcsönhatásként értelmezi az iránytű működését.

*  Felismeri a felszínformák ábrázolását a térképen.

*  Megérti a méretarány és az ábrázolás részletessége közötti összefüggéseket.

*  Fő- és mellékégtájak segítségével meghatározza különböző földrajzi objektumok egymáshoz viszonyított helyzetét.

*  Felismeri és használja a térképi jelrendszert és a térképfajtákat (domborzati térkép, közigazgatási térkép, autós térkép, turista térkép).

*  Felismeri a földrészeket és az óceánokat a különböző méretarányú és ábrázolásmódú térképeken.

*  Felismeri a nevezetes szélességi köröket a térképen.

*  Megfogalmazza európa és magyarország tényleges és viszonylagos földrajzi fekvését.

*  Ismeri a főfolyó, a mellékfolyó és a torkolat térképi ábrázolását.

*  Felismeri és megnevezi a legjelentősebb hazai álló- és folyóvizeket.

*  Bejelöli a térképen budapestet és a saját lakóhelyéhez közeli fontosabb nagyvárosokat és a szomszédos országokat.

*  A valóságban megismert területről egyszerű, jelrendszerrel ellátott útvonaltervet, térképet készít.

*  Tájékozódik a terepen térképvázlat, iránytű és gps segítségével.

*  Meghatározott szempontok alapján útvonalat tervez a térképen.

*  Használni tud néhány egyszerű térinformatikai alkalmazást.

*  Komplex rendszerként értelmezi az élő szervezeteket és az ezekből felépülő élőlénytársulásokat.

*  Tisztában van az életfeltételek és a testfelépítés közti kapcsolattal.

*  Tisztában van azzal, hogy az élő rendszerekbe történő beavatkozás káros hatásokkal járhat.

*  Felismeri és megnevezi a növények életfeltételeit, életjelenségeit.

*  Összehasonlít ismert hazai termesztett vagy vadon élő növényeket adott szempontok (testfelépítés, életfeltételek, szaporodás) alapján.

*  Felismeri és megnevezi a növények részeit, megfigyeli jellemzőiket, megfogalmazza ezek funkcióit.

*  Összehasonlítja ismert hazai termesztett vagy vadon élő növények részeit megadott szempontok alapján.

*  Ismert hazai termesztett vagy vadon élő növényeket különböző szempontok szerint csoportosít.

*  Azonosítja a lágyszárú és a fás szárú növények testfelépítése közötti különbségeket.

*  Felismeri és megnevezi az állatok életfeltételeit és életjelenségeit.

*  Összehasonlít ismert hazai házi vagy vadon élő állatokat adott szempontok (testfelépítés, életfeltételek, szaporodás) alapján.

*  Felismeri és megnevezi az állatok testrészeit, megfigyeli jellemzőiket, megfogalmazza ezek funkcióit.

*  Az állatokat különböző szempontok szerint csoportosítja.

*  Azonosítja a gerinctelen és a gerinces állatok testfelépítése közötti különbségeket.

*  Mikroszkóp segítségével megfigyel egysejtű élőlényeket.

*  Megfigyeli hazánk erdei élőlénytársulásainak főbb jellemzőit.

*  Életközösségként értelmezi az erdőt, mezőt, vizes élőhelyeket.

*  Felismeri és magyarázza az élőhely -- életmód - testfelépítés összefüggéseit az erdők életközössége esetén.

*  Példákkal bizonyítja, rendszerezi és következtetéseket von le az erdei élőlények környezethez történő alkalmazkodására vonatkozóan.

*  Táplálékláncokat és azokból táplálékhálózatot állít össze a megismert erdei növény- és állatfajokból.

*  Példákon keresztül bemutatja az erdőgazdálkodási tevékenységek életközösségre gyakorolt hatásait.

*  Tisztában van az erdő természetvédelmi értékével, fontosnak tartja annak védelmét.

*  Megfigyeli hazánk fátlan élőlénytársulásainak főbb jellemzőit.

*  Megadott szempontok alapján összehasonlítja a rétek és a szántóföldek életközösségeit.

*  Felismeri és magyarázza az élőhely - életmód - testfelépítés összefüggéseit a rétek életközössége esetén.

*  Példákkal bizonyítja, rendszerezi és következtetéseket von le a mezei élőlények környezethez történő alkalmazkodására vonatkozóan.

*  Táplálékláncokat és azokból táplálékhálózatot állít össze a megismert mezei növény- és állatfajokból.

*  Példákon keresztül mutatja be a mezőgazdasági tevékenységek életközösségre gyakorolt hatásait.

*  Tisztában van a fátlan társulások természetvédelmi értékével, fontosnak tartja annak védelmét.

*  Megfigyeli hazánk vízi és vízparti élőlénytársulásainak főbb jellemzőit.

*  Összehasonlítja a vízi és szárazföldi élőhelyek környezeti tényezőit.

*  Felismeri és magyarázza az élőhely -- életmód - testfelépítés összefüggéseit a vízi és vízparti életközösségek esetén.

*  Példákkal bizonyítja, rendszerezi és következtetéseket von le a vízi élőlények környezethez történő alkalmazkodására vonatkozóan.

*  Táplálékláncokat és ezekből táplálékhálózatot állít össze a megismert vízi és vízparti növény- és állatfajokból.

*  Példákon keresztül bemutatja a vízhasznosítás és a vízszennyezés életközösségre gyakorolt hatásait.

*  Tisztában van a vízi társulások természetvédelmi értékével, fontosnak tartja annak védelmét.

*  Érti, hogy a szervezet rendszerként működik.

*  Tisztában van a testi és lelki egészség védelmének fontosságával.

*  Tisztában van az egészséges környezet és az egészségmegőrzés közti összefüggéssel.

*  Felismeri és megnevezi az emberi test fő részeit, szerveit.

*  Látja az összefüggéseket az egyes szervek működése között.

*  Érti a kamaszkori testi és lelki változások folyamatát, élettani hátterét.

*  Tisztában van az egészséges életmód alapelveivel, azokat igyekszik betartani.

*  Összetett rendszerként értelmezi az egyes földi szférák működését.

*  Ismeri a természeti erőforrások energiatermelésben betöltött szerepét.

*  Tisztában van a természeti erők szerepével a felszínalakításban.

*  Csoportosítja az energiahordozókat különböző szempontok alapján.

*  Példákat hoz a megújuló és a nem megújuló energiaforrások felhasználására.

*  Megismeri az energiatermelés hatását a természetes és a mesterséges környezetre.

*  Megállapítja, összehasonlítja és csoportosítja néhány jellegzetes hazai kőzet egyszerűen vizsgálható tulajdonságait.

*  Példákat hoz a kőzetek tulajdonságai és a felhasználásuk közötti összefüggésekre.

*  Tisztában van azzal, hogy a talajpusztulás világméretű probléma.

*  Ismer olyan módszereket, melyek a talajpusztulás ellen hatnak (tápanyag-visszapótlás, komposztkészítés, ökológiai kertművelés).

*  Felismeri és összehasonlítja a gyűrődés, a vetődés, a földrengés és a vulkáni tevékenység hatásait.

*  Magyarázza a felszín lejtése, a folyó vízhozama, munkavégző képessége és a felszínformálás közti összefüggéseket.

*  Magyarázza az éghajlat és a folyók vízjárása közötti összefüggéseket.

*  Megnevezi az éghajlat fő elemeit.

*  Jellemezi és összehasonlítja az egyes éghajlati övezeteket (forró, mérsékelt, hideg).

*  Értelmezi az évszakok változását.

*  Értelmezi az időjárás-jelentést.

*  Piktogramok alapján megfogalmazza a várható időjárást.

## Testnevelés és egészségfejlesztés 
### 1-4. évfolyamon
*  Életkorának és testi adottságának megfelelően fejlődött motoros teljesítőképessége a hozzá kapcsolódó ismeretekkel olyan mérvű, hogy képes a saját teljesítménye tudatos befolyásolására.

*  Mozgáskultúrája olyan szintre fejlődött, hogy képes a hatékony mozgásos cselekvéstanulásra, testedzésre.

*  Ismeri a testnevelés életkorához igazodó elméleti ismeretanyagát, szakkifejezéseit, helyes terminológiáját, érti azok szükségességét.

*  Megismeri az elsősegélynyújtás jelentőségét, felismeri a baleseti és egészségkárosító veszélyforrásokat, képes azonnali segítséget kérni.

*  Önismerete, érzelmi-akarati készségei és képességei a testmozgás, a testnevelés és a sport eszközei által megfelelően fejlődtek.

*  A tanult mozgásformákat összefüggő cselekvéssorokban, jól koordináltan kivitelezi.

*  A tanult mozgásforma könnyed és pontos kivitelezésének elsajátításáig fenntartja érzelmi-akarati erőfeszítéseit.

*  A sportjátékok, a testnevelési és népi játékok művelése során egyaránt törekszik a szabályok betartására.

*  Nyitott az alapvető mozgásformák újszerű és alternatív környezetben történő alkalmazására, végrehajtására.

*  Megfelelő motoros képességszinttel rendelkezik az alapvető mozgásformák viszonylag önálló és tudatos végrehajtásához.

*  Olyan szintű relatív erővel rendelkezik, amely lehetővé teszi összefüggő cselekvéssorok kidolgozását, az elemek közötti összhang megteremtését.

*  Megfelelő általános állóképesség-fejlődést mutat.

*  Az alapvető mozgásformákat külsőleg meghatározott ritmushoz, a társak mozgásához igazított sebességgel és dinamikával képes végrehajtani.

*  Mozgásműveltsége szintjénél fogva pontosan hajtja végre a keresztező mozgásokat.

*  Futását összerendezettség, lépésszabályozottság, ritmusosság jellemzi.

*  Különböző mozgásai jól koordináltak, a hasonló mozgások szimultán és egymást követő végrehajtása jól tagolt.

*  Az egyszerűbb mozgásformákat jól koordináltan hajtja végre, a hasonló mozgások szimultán és egymást követő végrehajtásában megfelelő szintű tagoltságot mutat.

*  A különböző ugrásmódok alaptechnikáit és előkészítő mozgásformáit a vezető műveletek ismeretében tudatosan és koordináltan hajtja végre.

*  A különböző dobásmódok alaptechnikáit és előkészítő mozgásformáit a vezető műveletek ismeretében tudatosan és koordináltan hajtja végre.

*  A támasz- és függésgyakorlatok végrehajtásában a testtömegéhez igazodó erő- és egyensúlyozási képességgel rendelkezik.

*  A funkcionális hely- és helyzetváltoztató mozgásformáinak kombinációit változó feltételek között koordináltan hajtja végre.

*  Labdás ügyességi szintje lehetővé teszi az egyszerű taktikai helyzetekre épülő folyamatos, célszerű játéktevékenységet.

*  A megtanultak birtokában örömmel, a csapat teljes jogú tagjaként vesz részt a játékokban.

*  Vállalja a társakkal szembeni fizikai kontaktust, sportszerű test-test elleni küzdelmet valósít meg.

*  Ismeri és képes megnevezni a küzdőfeladatok, esések, tompítások játék- és baleset-megelőzési szabályait.

*  Ellenőrzött tevékenység keretében mozog a szabad levegőn, egyúttal tudatosan felkészül az időjárás kellemetlen hatásainak elviselésére sportolás közben.

*  Az elsajátított egy (vagy több) úszásnemben helyes technikával úszik.

*  A testnevelési és népi játékokban tudatosan, célszerűen alkalmazza az alapvető mozgásformákat.

*  Játék közben az egyszerű alaptaktikai elemek tudatos alkalmazására törekszik, játék- és együttműködési készsége megmutatkozik.

*  Célszerűen alkalmaz sportági jellegű mozgásformákat sportjáték-előkészítő kisjátékokban.

*  Játéktevékenysége közben a tanult szabályokat betartja.

*  A versengések és a versenyek közben toleráns a csapattársaival és az ellenfeleivel szemben.

*  A versengések és a versenyek tudatos szereplője, a közösséget pozitívan alakító résztvevő.

*  A szabályjátékok közben törekszik az egészséges versenyszellem megőrzésére.

*  Felismeri a sportszerű és sportszerűtlen magatartásformákat, betartja a sportszerű magatartás alapvető szabályait.

*  Felismeri a különböző veszély- és baleseti forrásokat, elkerülésükhöz tanári segítséget kér.

*  Ismeri a keringési, légzési és mozgatórendszerét fejlesztő alapvető mozgásformákat.

*  Tanári segítséggel megvalósít a biomechanikailag helyes testtartás kialakítását elősegítő gyakorlatokat.

*  Tanári irányítással, ellenőrzött formában végzi a testnevelés -- számára nem ellenjavallt -- mozgásanyagát.

*  Aktívan vesz részt az uszodában végzett mozgásformák elsajátításában, gyakorlásában.

*  A családi háttere és a közvetlen környezete adta lehetőségeihez mérten rendszeresen végez testmozgást.

*  Az öltözködés és a higiéniai szokások terén teljesen önálló, adott esetben segíti társait.

*  Megismeri az életkorának megfelelő sporttáplálkozás alapelveit, képes különbséget tenni egészséges és egészségtelen tápanyagforrások között.

*  A szabadban végzett foglalkozások során nem csupán ügyel környezete tisztaságára és rendjére, hanem erre felhívja társai figyelmét is.

*  Ismeri a helyes testtartás egészségre gyakorolt pozitív hatásait.

### 5-8. évfolyamon
*  Életkorának és testi adottságának megfelelően fejlődött motoros teljesítőképessége olyan mérvű, hogy képes a saját teljesítménye és fittségi szintje tudatos befolyásolására.

*  Sokoldalú mozgásműveltségének birtokában eredményesen tanul összetett mozgásformákat.

*  Ismeri és használja a testnevelés életkorához igazodó elméleti ismeretanyagát, szakkifejezéseit, helyes terminológiáját.

*  Önismerete, érzelmi-akarati készségei és képességei a testmozgás, a testnevelés és a sport eszközei által megfelelően fejlődtek.

*  Képes értelmezni az életben adódó baleseti forrásokat és az egészséget károsító, veszélyes szokásokat, tevékenységeket.

*  A tanult alapvető mozgásformák kombinációiból álló cselekvéssorokat változó térbeli, időbeli, dinamikai feltételek mellett készségszinten kivitelezi.

*  A tanult mozgásforma készségszintű kivitelezése közben fenntartja érzelmi-akarati erőfeszítéseit.

*  Minden sporttevékenységében forma- és szabálykövető attitűddel rendelkezik, ez tevékenységének automatikus részévé válik.

*  Nyitott az alapvető mozgásformák újszerű és alternatív környezetben történő felhasználására, végrehajtására.

*  A motoros képességeinek fejlődési szintje révén képes az összhang megteremtésére a cselekvéssorainak elemei között.

*  Relatív erejének birtokában képes a sportágspecifikus mozgástechnikák koordinált, készségszintű kivitelezésére.

*  Az alapvető mozgásainak koordinációjában megfelelő begyakorlottságot mutat, és képes a változó környezeti feltételekhez célszerűen illeszkedő végrehajtásra.

*  A (meg)tanult erő-, gyorsaság-, állóképesség- és ügyességfejlesztő eljárásokat tanári irányítással tudatosan alkalmazza.

*  Futótechnikája -- összefüggő cselekvéssor részeként -- eltérést mutat a vágta- és a tartós futás közben.

*  A rajttechnikákat a játékok, a versengések és a versenyek közben készségszinten használja.

*  Magabiztosan alkalmazza a távol- és magasugrás, valamint a kislabdahajítás és súlylökés -- számára megfelelő -- technikáit.

*  Segítségadással képes egy-egy általa kiválasztott tornaelem bemutatására és a tanult elemekből önállóan alkotott gyakorlatsor kivitelezésére.

*  A torna, a ritmikus gimnasztika, tánc és aerobik jellegű mozgásformákon keresztül tanári irányítás mellett fejleszti esztétikai-művészeti tudatosságát és kifejezőképességét.

*  A testnevelési és sportjáték közben célszerű, hatékony játék- és együttműködési készséget mutat.

*  A tanári irányítást követve, a mozgás sebességét növelve hajt végre önvédelmi fogásokat, ütéseket, rúgásokat, védéseket és ellentámadásokat.

*  Ellenőrzött tevékenység keretében rendszeresen mozog, edz, sportol a szabad levegőn, egyúttal tudatosan felkészül az időjárás kellemetlen hatásainak elviselésére sportolás közben.

*  Az elsajátított egy (vagy több) úszásnemben helyes technikával, készségszinten úszik.

*  A tanult testnevelési és népi játékok mellett folyamatosan, jól koordináltan végzi a választott sportjátékokat.

*  A sportjátékok előkészítő kisjátékaiban tudatosan és célszerűen alkalmazza a technikai és taktikai elemeket.

*  A küzdő jellegű feladatokban életkorának megfelelő asszertivitást mutatva tudatosan és célszerűen alkalmazza a támadó és védő szerepeknek megfelelő technikai és taktikai elemeket.

*  A versengések és a versenyek közben toleráns a csapattársaival és az ellenfeleivel szemben, ezt tőlük is elvárja.

*  A versengések és a versenyek közben közösségformáló, csapatkohéziót kialakító játékosként viselkedik.

*  Egészséges versenyszellemmel rendelkezik, és tanári irányítás vagy ellenőrzés mellett képes a játékvezetésre.

*  Megoldást keres a különböző veszély- és baleseti források elkerülésére.

*  Tanári segítséggel, egyéni képességeihez mérten, tervezetten, rendezetten és rendszeresen fejleszti keringési, légzési és mozgatórendszerét.

*  Tervezetten, rendezetten és rendszeresen végez a biomechanikailag helyes testtartás kialakítását elősegítő gyakorlatokat.

*  A mindennapi sporttevékenységébe tudatosan beépíti a korrekciós gyakorlatokat.

*  Tudatosan, összehangoltan végzi a korrekciós gyakorlatait és uszodai tevékenységét, azok megvalósítása automatikussá, mindennapi életének részévé válik.

*  Rendszeresen végez számára megfelelő vízi játékokat, és hajt végre úszástechnikákat.

*  Ismeri a tanult mozgásformák gerinc- és ízületvédelmi szempontból helyes végrehajtását.

*  Megnevez és bemutat egyszerű relaxációs gyakorlatokat.

*  A családi háttere és a közvetlen környezete adta lehetőségeihez mérten tervezetten, rendezetten és rendszeresen végez testmozgást.

*  A higiéniai szokások terén teljesen önálló, adott esetben segíti társait.

*  Az életkorának és alkati paramétereinek megfelelően tervezett, rendezett és rendszeres, testmozgással összefüggő táplálkozási szokásokat alakít ki.

*  A szabadban végzett foglalkozások során nem csupán ügyel környezete tisztaságára és rendjére, hanem erre felhívja társai figyelmét is.

*  A helyes testtartás egészségre gyakorolt pozitív hatásai ismeretében önállóan is kezdeményez ilyen tevékenységet.

### 9-12. évfolyamon
*  Alkotó módon használja a testnevelés életkorához igazodó ismeretanyagát, szakkifejezéseit, helyes terminológiáját.

*  Megismeri és mindennapjai részévé teszi a mozgáshoz kapcsolódó helyes attitűdöket, a fizikailag aktív életmód és a társas-érzelmi jóllét élethosszig tartó jótékony hatásait.

*  Képes elhárítani a baleseti és veszélyforrásokat, magabiztosan segíteni és elsősegélyt nyújtani embertársainak.

*  Társas-közösségi kapcsolatai, valamint stressztűrő és -kezelő képességei megfelelő szintre fejlődtek.

*  Toleráns a testi és más fogyatékossággal élő személyek iránt, megismeri és tiszteletben tartja a szexuális kultúra alapelveit, elfogadja az egészségügyi szűrések és a környezetvédelem fontosságát.

*  A tanult mozgásformákat alkotó módon, a testedzés és a sportolás minden területén használja.

*  A testedzéshez, a sportoláshoz kívánatosnak tartott jellemzőknek megfelelően (fegyelmezetten, határozottan, lelkiismeretesen, innovatívan és kezdeményezően) törekszik végrehajtani az elsajátított mozgásformákat.

*  Sporttevékenységében spontán, automatikus forma- és szabálykövető attitűdöt követ.

*  Nyitott az alapvető és sportágspecifikus mozgásformák újszerű és alternatív környezetben történő felhasználására, végrehajtására.

*  Olyan szintű motoros képességekkel rendelkezik, amelyek lehetővé teszik a tanult mozgásformák alkotó módon történő végrehajtását.

*  Relatív erejének birtokában a tanult mozgásformákat változó környezeti feltételek mellett, hatékonyan és készségszinten kivitelezi.

*  A különböző sportágspecifikus mozgásformákat változó környezeti feltételek mellett, hatékonyan és készségszinten hajtja végre.

*  A (meg)tanult erő-, gyorsaság-, állóképesség- és ügyességfejlesztő eljárásokat önállóan, tanári ellenőrzés nélkül alkalmazza.

*  Tanári ellenőrzés mellett digitálisan méri és értékeli a kondicionális és koordinációs képességeinek változásait, ezekből kiindulva felismeri saját motoros képességbeli hiányosságait, és ezeket a képességeket tudatosan és rendszeresen fejleszti.

*  A korábbi évfolyamokon elért eredményeihez képest folyamatosan javítja futóteljesítményét, amelyet önmaga is tudatosan nyomon követ.

*  A rajtolási módokat a játékok, versenyek, versengések közben hatékonyan, kreatívan alkalmazza.

*  Képes a kiválasztott ugró- és dobótechnikákat az ilyen jellegű játékok, versengések és versenyek közben, az eredményesség érdekében, egyéni sajátosságaihoz formálva hatékonyan alkalmazni.

*  Önállóan képes az általa kiválasztott elemkapcsolatokból tornagyakorlatot összeállítani, majd bemutatni.

*  A torna, ritmikus gimnasztika, aerobik és tánc jellegű mozgásformákon keresztül fejleszti esztétikai-művészeti tudatosságát és kifejezőképességét.

*  A zenei ütemnek megfelelően, készségszintű koordinációval végzi a kiválasztott ritmikus gimnasztika, illetve aerobik mozgásformákat.

*  Önállóan képes az életben adódó, elkerülhetetlen veszélyhelyzetek célszerű hárítására.

*  A különböző eséstechnikák készségszintű elsajátítása mellett a választott küzdősport speciális mozgásformáit célszerűen alkalmazza.

*  Rendszeresen mozog, edz, sportol a szabad levegőn, erre − lehetőségeihez mérten − társait is motiválja.

*  Az elsajátított egy (vagy több) úszásnemben vízbiztosan, készségszinten úszik, a természetes vizekben is.

*  Önállóan képes az elkerülhetetlen vízi veszélyhelyzetek célszerű kezelésére.

*  A tanult testnevelési, népi és sportjátékok összetett technikai és taktikai elemeit kreatívan, az adott játékhelyzetnek megfelelően, célszerűen, készségszinten alkalmazza.

*  Játéktevékenységét kreativitást mutató játék- és együttműködési készség jellemzi.

*  A versengések és a versenyek közben toleráns a csapattársaival és az ellenfeleivel szemben, ezt tőlük is elvárja.

*  A versengések és a versenyek közben közösségformáló, csapatkohéziót kialakító játékosként viselkedik.

*  A szabályjátékok alkotó részese, képes szabálykövető játékvezetésre.

*  Megoldást keres a különböző veszély- és baleseti források elkerülésére, erre társait is motiválja.

*  Az egyéni képességeihez mérten, mindennapi szokásrendszerébe építve fejleszti keringési, légzési és mozgatórendszerét.

*  Belső igénytől vezérelve rendszeresen végez a biomechanikailag helyes testtartás kialakítását elősegítő gyakorlatokat.

*  Mindennapi tevékenységének tudatos részévé válik a korrekciós gyakorlatok végzése.

*  A szárazföldi és az uszodai korrekciós gyakorlatait készségszinten sajátítja el, azokat tudatosan rögzíti.

*  Önállóan, de tanári ellenőrzés mellett végez számára megfelelő uszodai tevékenységet.

*  A családi háttere és a közvetlen környezete adta lehetőségeihez mérten, belső igénytől vezérelve, alkotó módon, rendszeresen végez testmozgást.

*  Ismer és alkalmaz alapvető relaxációs technikákat.

*  Mindennapi életének részeként kezeli a testmozgás, a sportolás közbeni higiéniai és tisztálkodási szabályok betartását.

*  Az életkorának és alkati paramétereinek megfelelő pozitív, egészégtudatos, testmozgással összefüggő táplálkozási szokásokat alakít ki.

*  A szabadban végzett foglalkozások során nem csupán ügyel környezete tisztaságára és rendjére, hanem erre felhívja társai figyelmét is.

*  Megoldást keres a testtartási rendellenesség kialakulásának megakadályozására, erre társait is motiválja.

## Történelem 
### 5-8. évfolyamon
*  Alapvető ismeretekkel rendelkezik a magyar nemzet, magyarország és az európai civilizáció és földünk legfontosabb régióinak múltjáról és jelenéről.

*  Képes a múlt és jelen alapvető társadalmi, gazdasági, politikai és kulturális folyamatairól, jelenségeiről véleményt alkotni.

*  Megérti, hogy minden történelmi eseménynek és folyamatnak okai és következményei vannak.

*  Ismeri a közös magyar nemzeti, az európai, valamint az egyetemes emberi civilizáció kulturális örökségének, kódrendszerének legalapvetőbb elemeit.

*  Különbséget tud tenni a múltról szóló fiktív történetek és a történelmi tények között.

*  Megérti és méltányolja, hogy a múlt viszonyai, az emberek gondolkodása, értékítélete eltért a maitól.

*  Alapvető ismereteket szerez a demokratikus államszervezetről, a társadalmi együttműködés szabályairól és a piacgazdaságról.

*  Kialakul a múlt iránti érdeklődés.

*  Megerősödik benne a nemzeti identitás és hazaszeretet érzése; büszke népe múltjára, ápolja hagyományait, és méltón emlékezik meg hazája nagyjairól.

*  Kialakulnak az európai civilizációs identitás alapelemei.

*  Kialakulnak a társadalmi felelősség és normakövetés, az egyéni kezdeményezőkészség és a hazája, közösségei és embertársai iránti felelősségvállalás, az aktív állampolgárság, valamint a demokratikus elkötelezettség alapelemei.

*  Ismeri és fel tudja idézni a magyar és az európai történelmi hagyományhoz kapcsolódó legfontosabb mítoszokat, mondákat, történeteket, elbeszéléseket.

*  Be tudja mutatni a különböző korok életmódjának és kultúrájának főbb vonásait és az egyes történelmi korszakokban élt emberek életét befolyásoló tényezőket.

*  Tisztában van a kereszténység kialakulásának főbb állomásaival, ismeri a legfontosabb tanításait és hatását az európai civilizációra és magyarországra.

*  Ismeri a magyar történelem kiemelkedő alakjait, cselekedeteiket, illetve szerepüket a magyar nemzet történetében.

*  Fel tudja idézni a magyar történelem legfontosabb eseményeit, jelenségeit, folyamatait és fordulópontjait a honfoglalástól napjainkig.

*  Képes felidézni a magyar nemzet honvédő és szabadságharcait, példákat hoz a hazaszeretet, önfeláldozás és hősiesség megnyilvánulásaira.

*  Tisztában van a középkor és újkor világképének fő vonásaival, a 19. és 20. század fontosabb politikai eszméivel és azok hatásaival.

*  Ismeri és be tudja mutatni a 19. és 20. századi modernizáció gazdasági, társadalmi és kulturális hatásait magyarországon és a világban.

*  Ismeri a különböző korok hadviselési szokásait, az első és a második világháború legfontosabb eseményeit, jellemzőit, valamint napjainkra is hatással bíró következményeit.

*  Fel tudja idézni az első és második világháború borzalmait, érveket tud felsorakoztatni a békére való törekvés mellett.

*  Ismeri a nemzetiszocialista és a kommunista diktatúrák főbb jellemzőit, az emberiség ellen elkövetett bűneiket, ellentmondásaikat és ezek következményeit, továbbá a velük szembeni ellenállás példáit.

*  Felismeri a különbségeket a demokratikus és a diktatórikus berendezkedések között, érvel a demokrácia értékei mellett.

*  Példákat tud felhozni arra, hogy a történelem során miként járultak hozzá a magyarok európa és a világ kulturális, tudományos és politikai fejlődéséhez.

*  Ismeri a magyarság, illetve a kárpát-medence népei együttélésének jellemzőit néhány történelmi korszakban, beleértve a határon kívüli magyarság sorsát, megmaradásáért folytatott küzdelmét, példákat hoz a magyar nemzet és a közép-európai régió népeinek kapcsolatára és együttműködésére.

*  Valós képet alkotva képes elhelyezni magyarországot a globális folyamatokban a történelem során és napjainkban.

*  Ismeri hazája államszervezetét.

*  Képes ismereteket szerezni személyes beszélgetésekből, tárgyak, épületek megfigyeléséből, olvasott és hallott, valamint a különböző médiumok által felkínált szöveges és képi anyagokból.

*  Kiemel lényeges információkat (kulcsszavakat, tételmondatokat) elbeszélő vagy leíró, illetve rövidebb magyarázó írott és hallott szövegekből és az ezek alapján megfogalmazott kérdésekre egyszerű válaszokat adni.

*  Megadott szempontok alapján, tanári útmutatás segítségével történelmi információkat gyűjt különböző médiumokból és forrásokból (könyvek, atlaszok, kronológiák, könyvtárak, múzeumok, médiatárak, filmek; nyomtatott és digitális, szöveges és vizuális források).

*  Képes élethelyzetek, magatartásformák megfigyelése által értelmezni azokat.

*  Megadott szempontok alapján tudja értelmezni és rendszerezni a történelmi információkat.

*  Felismeri, hogy melyik szöveg, kép, egyszerű ábra, grafikon vagy diagram kapcsolódik az adott történelmi témához.

*  Képen, egyszerű ábrán, grafikonon, diagramon ábrázolt folyamatot, jelenséget saját szavaival le tud írni.

*  Képes egyszerű esetekben forráskritikát végezni, valamint különbséget tenni források között típus és szövegösszefüggés alapján.

*  Össze tudja vetni a forrásokban található információkat az ismereteivel, párhuzamot tud vonni különböző típusú (pl. szöveges és képi) történelmi források tartalma között.

*  Meg tudja vizsgálni, hogy a történet szerzője résztvevője vagy kortársa volt-e az eseményeknek.

*  Egyszerű következtetéseket von le, és véleményt tud alkotni különböző források hitelességéről és releváns voltáról.

*  Ismeri a nagy történelmi korszakok elnevezését és időhatárait, néhány kiemelten fontos esemény, jelenség és történelmi folyamat időpontját.

*  Biztonsággal használja az idő tagolására szolgáló kifejezéseket, történelmi eseményre, jelenségre, folyamatra, korszakra való utalással végez időmeghatározást.

*  Ismeretei segítségével időrendbe tud állítani történelmi eseményeket, képes az idő ábrázolására pl. időszalag segítségével.

*  A tanult történelmi eseményeket, jelenségeket, személyeket, ikonikus szimbólumokat, tárgyakat, képeket hozzá tudja rendelni egy adott történelmi korhoz, régióhoz, államhoz.

*  Biztonsággal használ különböző történelmi térképeket a fontosabb történelmi események helyszíneinek azonosítására, egyszerű jelenségek, folyamatok leolvasására, értelmezésére, vaktérképen való elhelyezésére.

*  Egyszerű alaprajzokat, modelleket, térképvázlatokat (pl. települések, épületek, csaták) tervez és készít.

*  Önállóan, folyamatos beszéddel képes eseményeket, történeteket elmondani, történelmi személyeket bemutatni, saját véleményt megfogalmazni.

*  Össze tudja foglalni saját szavaival hosszabb elbeszélő vagy leíró, valamint rövidebb magyarázó szövegek tartalmát.

*  Az általa gyűjtött történelmi adatokból, szövegekből rövid tartalmi ismertetőt tud készíteni.

*  Képes önálló kérdések megfogalmazására a tárgyalt történelmi témával, eseményekkel, folyamatokkal kapcsolatban.

*  Képes rövid fogalmazások készítésére egy-egy történetről, történelmi témáról.

*  Különböző történelmi korszakok, történelmi és társadalmi kérdések tárgyalása során szakszerűen alkalmazza az értelmező és tartalmi kulcsfogalmakat, továbbá használja a témához kapcsolódó történelmi fogalmakat.

*  Tud egyszerű vizuális rendezőket készíteni és kiegészíteni hagyományos vagy digitális módon (táblázatok, ábrák, tablók, rajzok, vázlatok) egy történelmi témáról.

*  Egyszerű történelmi témáról tanári útmutatás segítségével kiselőadást és digitális prezentációt állít össze és mutat be.

*  Egyszerű történelmi kérdésekről tárgyilagos véleményt tud megfogalmazni, állításait alátámasztja.

*  Meghallgatja és megérti -- adott esetben elfogadja -- mások véleményét, érveit.

*  Tanári segítséggel dramatikusan, szerepjáték formájában tud megjeleníteni történelmi eseményeket, jelenségeket, személyiségeket.

*  Adott történetben különbséget tud tenni fiktív és valós, irreális és reális elemek között.

*  Képes megfigyelni, értelmezni és összehasonlítani a történelemben előforduló különböző emberi magatartásformákat és élethelyzeteket.

*  A történelmi eseményekkel, folyamatokkal és személyekkel kapcsolatban önálló kérdéseket fogalmaz meg.

*  Feltételezéseket fogalmaz meg történelmi személyek cselekedeteinek mozgatórugóiról, és adatokkal, érvekkel alátámasztja azokat.

*  A történelmi szereplők megnyilvánulásainak szándékot tulajdonít, álláspontjukat azonosítja.

*  Önálló véleményt képes megfogalmazni történelmi szereplőkről, eseményekről, folyamatokról.

*  Felismeri és értékeli a különböző korokra és régiókra jellemző tárgyakat, alkotásokat, életmódokat, szokásokat, változásokat, képes azokat összehasonlítani egymással, illetve a mai korral.

*  Társadalmi és erkölcsi problémákat azonosít adott történetek, történelmi események, különböző korok szokásai alapján.

*  Példákat hoz a történelmi jelenségekre, folyamatokra.

*  Feltételezéseket fogalmaz meg néhány fontos történelmi esemény és folyamat feltételeiről, okairól és következményeiről, és tényekkel alátámasztja azokat.

*  Több szempontból képes megkülönböztetni a történelmi jelenségek és események okait és következményeit (pl. hosszú vagy rövid távú, gazdasági, társadalmi vagy politikai).

*  Felismeri, hogy az emberi cselekedet és annak következménye között szoros kapcsolat van.

### 9-12. évfolyamon
*  Megbízható ismeretekkel bír az európai, valamint az egyetemes történelem és mélyebb tudással rendelkezik a magyar történelem fontosabb eseményeiről, történelmi folyamatairól, fordulópontjairól.

*  Képes a múlt és jelen társadalmi, gazdasági, politikai és kulturális folyamatairól, jelenségeiről többszempontú, tárgyilagos érveléssel alátámasztott véleményt alkotni, ezekkel kapcsolatos problémákat megfogalmazni.

*  Ismeri a közös magyar nemzeti és európai, valamint az egyetemes emberi civilizáció kulturális örökségének, kódrendszerének lényeges elemeit.

*  Különbséget tud tenni történelmi tények és történelmi interpretáció, illetve vélemény között.

*  Képes következtetni történelmi események, folyamatok és jelenségek okaira és következményeire.

*  Képes a tanulási célhoz megfelelő információforrást választani, a források között szelektálni, azokat szakszerűen feldolgozni és értelmezni.

*  Kialakul a hiteles és tárgyilagos forráshasználat és kritika igénye.

*  Képes a múlt eseményeit és jelenségeit a saját történelmi összefüggésükben értelmezni, illetve a jelen viszonyait kapcsolatba hozni a múltban történtekkel.

*  Ismeri a demokratikus államszervezet működését, a társadalmi együttműködés szabályait, a piacgazdaság alapelveit; autonóm és felelős állampolgárként viselkedik.

*  Kialakul és megerősödik a történelmi múlt, illetve a társadalmi, politikai, gazdasági és kulturális kérdések iránti érdeklődés.

*  Kialakulnak a saját értékrend és történelemszemlélet alapjai.

*  Elmélyül a nemzeti identitás és hazaszeretet, büszke népe múltjára, ápolja hagyományait, és méltón emlékezik meg hazája nagyjairól.

*  Megerősödnek az európai civilizációs identitás alapelemei.

*  Megerősödik és elmélyül a társadalmi felelősség és normakövetés, az egyéni kezdeményezőkészség, a hazája, közösségei és embertársai iránt való felelősségvállalás, valamint a demokratikus elkötelezettség.

*  Ismeri az ókori civilizációk legfontosabb jellemzőit, valamint az athéni demokrácia és a római állam működését, hatásukat az európai civilizációra.

*  Felidézi a monoteista vallások kialakulását, legfontosabb jellemzőiket, tanításaik főbb elemeit, és bemutatja terjedésüket.

*  Bemutatja a keresztény vallás civilizációformáló hatását, a középkori egyházat, valamint a reformáció és a katolikus megújulás folyamatát és kulturális hatásait; érvel a vallási türelem, illetve a vallásszabadság mellett.

*  Képes felidézni a középkor gazdasági és kulturális jellemzőit, világképét, a kor meghatározó birodalmait és bemutatni a rendi társadalmat.

*  Ismeri a magyar nép őstörténetére és a honfoglalásra vonatkozó tudományos elképzeléseket és tényeket, tisztában van legfőbb vitatott kérdéseivel, a különböző tudományterületek kutatásainak főbb eredményeivel.

*  Értékeli az államalapítás, valamint a kereszténység felvételének jelentőségét.

*  Felidézi a középkori magyar állam történetének fordulópontjait, legfontosabb uralkodóink tetteit.

*  Ismeri a magyarság törökellenes küzdelmeit, fordulópontjait és hőseit; felismeri, hogy a magyar és az európai történelem alakulását meghatározóan befolyásolta a török megszállás.

*  Be tudja mutatni a kora újkor fő gazdasági és társadalmi folyamatait, ismeri a felvilágosodás eszméit, illetve azok kulturális és politikai hatását, valamint véleményt formál a francia forradalom európai hatásáról.

*  Összefüggéseiben és folyamatában fel tudja idézni, miként hatott a magyar történelemre a habsburg birodalomhoz való tartozás, bemutatja az együttműködés és konfrontáció megnyilvánulásait, a függetlenségi törekvéseket és értékeli a rákóczi-szabadságharc jelentőségét.

*  Ismeri és értékeli a magyar nemzetnek a polgári átalakulás és nemzeti függetlenség elérésére tett erőfeszítéseit a reformkor, az 1848/49-es forradalom és szabadságharc, illetve az azt követő időszakban; a kor kiemelkedő magyar politikusait és azok nézeteit, véleményt tud formálni a kiegyezésről.

*  Fel tudja idézni az ipari forradalom szakaszait, illetve azok gazdasági, társadalmi, kulturális és politikai hatásait; képes bemutatni a modern polgári társadalom és állam jellemzőit és a 19. század főbb politikai eszméit, valamint felismeri a hasonlóságot és különbséget azok mai formái között.

*  Fel tudja idézni az első világháború előzményeit, a háború jellemzőit és fontosabb fordulópontjait, értékeli a háborúkat lezáró békék tartalmát, és felismeri a háborúnak a 20. század egészére gyakorolt hatását.

*  Bemutatja az első világháború magyar vonatkozásait, a háborús vereség következményeit; példákat tud hozni a háborús helytállásra.

*  Képes felidézni azokat az okokat és körülményeket, amelyek a történelmi magyarország felbomlásához vezettek.

*  Tisztában van a trianoni békediktátum tartalmával és következményeivel, be tudja mutatni az ország talpra állását, a horthy-korszak politikai, gazdasági, társadalmi és kulturális viszonyait, felismeri a magyar külpolitika mozgásterének korlátozottságát.

*  Össze tudja hasonlítani a nemzetiszocialista és a kommunista ideológiát és diktatúrát, példák segítségével bemutatja a rendszerek embertelenségét és a velük szembeni ellenállás formáit.

*  Képes felidézni a második világháború okait, a háború jellemzőit és fontosabb fordulópontjait, ismeri a holokausztot és a hozzávezető okokat.

*  Bemutatja magyarország revíziós lépéseit, a háborús részvételét, az ország német megszállását, a magyar zsidóság tragédiáját, a szovjet megszállást, a polgári lakosság szenvedését, a hadifoglyok embertelen sorsát.

*  Össze tudja hasonlítani a nyugati demokratikus világ és a kommunista szovjet blokk politikai és társadalmi berendezkedését, képes jellemezni a hidegháború időszakát, bemutatni a gyarmati rendszer felbomlását és az európai kommunista rendszerek összeomlását.

*  Bemutatja a kommunista diktatúra magyarországi kiépítését, működését és változatait, az 1956-os forradalom és szabadságharc okait, eseményeit és hőseit, összefüggéseiben szemléli a rendszerváltoztatás folyamatát, felismerve annak történelmi jelentőségét.

*  Bemutatja a gyarmati rendszer felbomlásának következményeit, india, kína és a közel-keleti régió helyzetét és jelentőségét.

*  Ismeri és reálisan látja a többpólusú világ jellemzőit napjainkban, elhelyezi magyarországot a globális világ folyamataiban.

*  Bemutatja a határon túli magyarság helyzetét, a megmaradásért való küzdelmét trianontól napjainkig.

*  Ismeri a magyar cigányság történetének főbb állomásait, bemutatja jelenkori helyzetét.

*  Ismeri a magyarság, illetve a kárpát-medence népei együttélésének jellemzőit, példákat hoz a magyar nemzet és a közép-európai régió népeinek kapcsolatára, különös tekintettel a visegrádi együttműködésre.

*  Ismeri hazája államszervezetét, választási rendszerét.

*  Önállóan tud használni általános és történelmi, nyomtatott és digitális információforrásokat (tankönyv, kézikönyvek, szakkönyvek, lexikonok, képzőművészeti alkotások, könyvtár és egyéb adatbázisok, filmek, keresők).

*  Önállóan információkat tud gyűjteni, áttekinteni, rendszerezni és értelmezni különböző médiumokból és írásos vagy képi forrásokból, statisztikákból, diagramokból, térképekről, nyomtatott és digitális felületekről.

*  Tud forráskritikát végezni és különbséget tenni a források között hitelesség, típus és szövegösszefüggés alapján.

*  Képes azonosítani a különböző források szerzőinek a szándékát, bizonyítékok alapján értékeli egy forrás hitelességét.

*  Képes a szándékainak megfelelő információkat kiválasztani különböző műfajú forrásokból.

*  Összehasonlítja a forrásokban talált információkat saját ismereteivel, illetve más források információival és megmagyarázza az eltérések okait.

*  Képes kiválasztani a megfelelő forrást valamely történelmi állítás, vélemény alátámasztására vagy cáfolására.

*  Ismeri a magyar és az európai történelem tanult történelmi korszakait, időszakait, és képes azokat időben és térben elhelyezni.

*  Az egyes események, folyamatok idejét konkrét történelmi korhoz, időszakhoz kapcsolja vagy viszonyítja, ismeri néhány kiemelten fontos esemény, jelenség időpontját, kronológiát használ és készít.

*  Össze tudja hasonlítani megadott szempontok alapján az egyes történelmi korszakok, időszakok jellegzetességeit az egyetemes és a magyar történelem egymáshoz kapcsolódó eseményeit.

*  Képes azonosítani a tanult egyetemes és magyar történelmi személyiségek közül a kortársakat.

*  Felismeri, hogy a magyar történelem az európai történelem része, és példákat tud hozni a magyar és európai történelem kölcsönhatásaira.

*  Egyszerű történelmi térképvázlatot alkot hagyományos és digitális eljárással.

*  A földrajzi környezet és a történeti folyamatok összefüggéseit példákkal képes alátámasztani.

*  Képes különböző időszakok történelmi térképeinek összehasonlítására, a történelmi tér változásainak és a történelmi mozgások követésére megadott szempontok alapján a változások hátterének feltárásával.

*  Képes a történelmi jelenségeket általános és konkrét történelmi fogalmak, tartalmi és értelmező kulcsfogalmak felhasználásával értelmezni és értékelni.

*  Fel tud ismerni fontosabb történelmi fogalmakat meghatározás alapján.

*  Képes kiválasztani, rendezni és alkalmazni az azonos korhoz, témához kapcsolható fogalmakat.

*  Össze tudja foglalni rövid és egyszerű szaktudományos szöveg tartalmát.

*  Képes önállóan vázlatot készíteni és jegyzetelni.

*  Képes egy-egy korszakot átfogó módon bemutatni.

*  Történelmi témáról kiselőadást, digitális prezentációt alkot és mutat be.

*  Történelmi tárgyú folyamatábrákat, digitális táblázatokat, diagramokat készít, történelmi, gazdasági, társadalmi és politikai modelleket vizuálisan is meg tud jeleníteni.

*  Megadott szempontok alapján történelmi tárgyú szerkesztett szöveget (esszét) tud alkotni, amelynek során tételmondatokat fogalmaz meg, állításait több szempontból indokolja és következtetéseket von le.

*  Társaival képes megvitatni történelmi kérdéseket, amelynek során bizonyítékokon alapuló érvekkel megindokolja a véleményét, és választékosan reflektál mások véleményére, árnyalja saját álláspontját.

*  Képes felismerni, megfogalmazni és összehasonlítani különböző társadalmi és történelmi problémákat, értékrendeket, jelenségeket, folyamatokat.

*  A tanult ismereteket problémaközpontúan tudja rendezni.

*  Hipotéziseket alkot történelmi személyek, társadalmi csoportok és intézmények viselkedésének mozgatórugóiról.

*  Önálló kérdéseket fogalmaz meg történelmi folyamatok, jelenségek és események feltételeiről, okairól és következményeiről.

*  Önálló véleményt tud alkotni történelmi eseményekről, folyamatokról, jelenségekről és személyekről.

*  Képes különböző élethelyzetek, magatartásformák megfigyelése által következtetések levonására, erkölcsi kérdéseket is felvető történelmi helyzetek felismerésére és megítélésére.

*  A változás és a fejlődés fogalma közötti különbséget ismerve képes felismerni és bemutatni azokat azonos korszakon belül, vagy azokon átívelően.

*  Képes összevetni, csoportosítani és súlyozni az egyes történelmi folyamatok, jelenségek, események okait, következményeit, és ítéletet alkotni azokról, valamint a benne résztvevők szándékairól.

*  Összehasonlít különböző, egymáshoz hasonló történeti helyzeteket, folyamatokat, jelenségeket.

*  Képes felismerni konkrét történelmi helyzetekben, jelenségekben és folyamatokban valamely általános szabályszerűség érvényesülését.

*  Összehasonlítja és kritikusan értékeli az egyes történelmi folyamatokkal, eseményekkel és személyekkel kapcsolatos eltérő álláspontokat.

*  Feltevéseket fogalmaz meg, azok mellett érveket gyűjt, illetve mérlegeli az ellenérveket.

*  Felismeri, hogy a jelen társadalmi, gazdasági, politikai és kulturális viszonyai a múltbeli események, tényezők következményeiként alakultak ki.

## Vizuális kultúra 
### 1-4. évfolyamon
*  Alkotó tevékenység közben bátran kísérletezik.

*  Különböző eszközöket rendeltetésszerűen használ.

*  Megérti és végrehajtja a feladatokat.

*  Példák alapján különbséget tesz a hétköznapi és a művészi között.

*  Gondolatait vizuálisan is érthetően megmagyarázza.

*  Példák alapján azonosítja a médiafogyasztás mindennapi jelenségeit.

*  Önálló döntéseket hoz a környezet alakításának szempontjából.

*  Csoportban végzett feladatmegoldás során részt vállal a feladatmegoldásban, és figyelembe veszi társai álláspontját.

*  Csoportban végzett feladatmegoldás közben képes érvelésre.

*  Feladatmegoldás során betartja az előre ismertetett szabályokat.

*  Egyszerű, begyakorolt feladatokat önállóan is elvégez.

*  Véleményét önállóan megfogalmazza.

*  Érzékszervi tapasztalatokat pontosan megfogalmaz mérettel, formával, színnel, felülettel, illattal, hanggal, mozgással kapcsolatban.

*  Közvetlen vizuális megfigyeléssel leolvasható egyszerű jellemzők alapján vizuális jelenségeket, képeket méret, irány, elhelyezkedés, mennyiség, szín szempontjából azonosít, kiválaszt, rendez, szövegesen pontosan leír és összehasonlít.

*  Látványt, vizuális jelenségeket, képeket viszonylagos pontossággal emlékezetből azonosít, kiválaszt, megnevez, különböző szempontok szerint rendez.

*  Vizuális jelenségeket, egyszerű látott képi elemeket különböző vizuális eszközökkel megjelenít: rajzol, fest, nyomtat, formáz, épít.

*  Élmények, elképzelt vagy hallott történetek, szövegek részleteit különböző vizuális eszközökkel egyszerűen megjeleníti: rajzol, fest, nyomtat, fotóz, formáz, épít.

*  Rövid szövegekhez, egyéb tananyagtartalmakhoz síkbeli és térbeli vizuális illusztrációt készít különböző vizuális eszközökkel: rajzol, fest, nyomtat, fotóz, formáz, épít és a képet, tárgyat szövegesen értelmezi.

*  Egyszerű eszközökkel és anyagokból elképzelt teret rendez, alakít, egyszerű makettet készít egyénileg vagy csoportmunkában, és az elképzelést szövegesen is bemutatja, magyarázza.

*  Elképzelt történeteket, irodalmi alkotásokat bemutat, dramatizál, ehhez egyszerű eszközöket: bábot, teret, díszletet, kelléket, egyszerű jelmezt készít csoportmunkában, és élményeit szövegesen megfogalmazza.

*  Saját és társai vizuális munkáit szövegesen értelmezi, kiegészíti, magyarázza.

*  Saját munkákat, képeket, műalkotásokat, mozgóképi részleteket szereplők karaktere, szín-, fényhatás, kompozíció, kifejezőerő szempontjából szövegesen elemez, összehasonlít.

*  Képek, műalkotások, mozgóképi közlések megtekintése után önállóan megfogalmazza és indokolja tetszésítéletét.

*  Képek, műalkotások, mozgóképi közlések megtekintése után adott szempontok szerint következtetést fogalmaz meg, megállapításait társaival is megvitatja.

*  Különböző alakzatokat, motívumokat, egyszerű vizuális megjelenéseket látvány alapján, különböző vizuális eszközökkel, viszonylagos pontossággal megjelenít: rajzol, fest, nyomtat, formáz, épít.

*  Adott cél érdekében fotót vagy rövid mozgóképet készít.

*  Alkalmazza az egyszerű tárgykészítés legfontosabb technikáit: vág, ragaszt, tűz, varr, kötöz, fűz, mintáz.

*  Adott cél érdekében alkalmazza a térbeli formaalkotás különböző technikáit egyénileg és csoportmunkában.

*  Gyűjtött természeti vagy mesterséges formák egyszerűsítésével, vagy a magyar díszítőművészet általa megismert mintakincsének felhasználásával mintát tervez.

*  A tanulás során szerzett tapasztalatait, saját céljait, gondolatait vizuális megjelenítés segítségével magyarázza, illusztrálja egyénileg és csoportmunkában.

*  Saját és mások érzelmeit, hangulatait segítséggel megfogalmazza és egyszerű dramatikus eszközökkel eljátssza, vizuális eszközökkel megjeleníti.

*  Korábban átélt eseményeket, tapasztalatokat, élményeket különböző vizuális eszközökkel, élményszerűen megjelenít: rajzol, fest, nyomtat, formáz, épít, fotóz és magyarázza azt.

*  Valós vagy digitális játékélményeit vizuálisan és dramatikusan feldolgozza: rajzol, fest, formáz, nyomtat, eljátszik, elmesél.

*  A vizuális nyelv elemeinek értelmezésével és használatával kísérletezik.

*  Az adott életkornak megfelelő rövid mozgóképi közléseket segítséggel elemez a vizuális kifejezőeszközök használatának tudatosítása érdekében.

*  Egyszerű, mindennapok során használt jeleket felismer.

*  Pontosan ismeri államcímerünk és nemzeti zászlónk felépítését, összetevőit, színeit.

*  Az adott életkornak megfelelő tájékoztatást, meggyőzést, figyelemfelkeltést szolgáló, célzottan kommunikációs szándékú vizuális közléseket segítséggel értelmez.

*  Azonosítja a gyerekeknek szóló vagy fogyasztásra ösztönző, célzottan kommunikációs szándékú vizuális közléseket.

*  Azonosítja a nonverbális kommunikáció eszközeit: mimika, gesztus, > ezzel kapcsolatos tapasztalatait közlési és kifejezési > helyzetekben használja.

*  Adott cél érdekében egyszerű vizuális kommunikációt szolgáló > megjelenéseket -- jel, meghívó, plakát -- készít egyénileg vagy > csoportmunkában.

*  Saját kommunikációs célból egyszerű térbeli tájékozódást segítő > ábrát -- alaprajz, térkép -- készít.

*  Időbeli történéseket egyszerű vizuális eszközökkel, segítséggel > megjelenít.

*  Saját történetet alkot, és azt vizuális eszközökkel is tetszőlegesen > megjeleníti.

*  Adott álló- vagy mozgóképi megjelenéseket egyéni elképzelés szerint > átértelmez.

*  Különböző egyszerű anyagokkal kísérletezik, szabadon épít, saját > célok érdekében konstruál.

### 5-8. évfolyamon
*  Alkotó tevékenység közben önállóan kísérletezik, különböző megoldási utakat keres.

*  Eszközhasználata begyakorlott, szakszerű.

*  Anyaghasználata gazdaságos, átgondolt.

*  Feladatokat önállóan megold, eredményeit érthetően bemutatja.

*  Adott tanulási helyzetben a tanulási előrehaladás érdekében adekvát kérdést tesz fel.

*  Feladatmegoldásai során felidézi és alkalmazza a korábban szerzett ismereteket, illetve kapcsolódó információkat keres.

*  Felismeri a vizuális művészeti műfajok példáit.

*  Használja és megkülönbözteti a különböző vizuális médiumok kifejezőeszközeit.

*  Megkülönböztet művészettörténeti korszakokat, stílusokat.

*  Felismeri az egyes témakörök szemléltetésére használt műalkotásokat, alkotókat az ajánlott képanyag alapján.

*  Használja a térbeli és az időbeli viszonyok megjelenítésének különböző vizuális lehetőségeit.

*  Példák alapján megérti a képmanipuláció és befolyásolás összefüggéseit.

*  Példák alapján meg tudja magyarázni a tervezett, épített környezet és a funkció összefüggéseit.

*  Érti a magyar és egyetemes kulturális örökség és hagyomány jelentőségét.

*  Csoportban végzett feladatmegoldás során részt vállal a feladatmegoldásban, önállóan megtalálja saját feladatát, és figyelembe veszi társai álláspontját is.

*  Feladatmegoldás során megállapodásokat köt, szabályt alkot, és betartja a közösen meghatározott feltételeket.

*  Önállóan véleményt alkot, és azt röviden indokolja.

*  Látványt, vizuális jelenségeket, műalkotásokat önállóan is pontosan, részletgazdagon szövegesen jellemez, bemutat.

*  Látványok, vizuális jelenségek, alkotások lényeges, egyedi jellemzőit kiemeli, bemutatja.

*  Alkotómunka során felhasználja a már látott képi inspirációkat.

*  Adott témával, feladattal kapcsolatos vizuális információkat és képi inspirációkat keres többféle forrásból.

*  Megfigyeléseit, tapasztalatait, gondolatait vizuálisan rögzíti, mások számára érthető vázlatot készít.

*  Különböző érzetek kapcsán belső képeinek, képzeteinek megfigyelésével tapasztalatait vizuálisan megjeleníti.

*  Elvont fogalmakat, művészeti tartalmakat belső képek összekapcsolásával bemutat, magyaráz és különböző vizuális eszközökkel megjelenít.

*  Adott tartalmi keretek figyelembevételével karaktereket, tereket, tárgyakat, helyzeteket, történeteket részletesen elképzel, fogalmi és vizuális eszközökkel bemutat és megjelenít, egyénileg és csoportmunkában is.

*  A valóság vagy a vizuális alkotások, illetve azok elemei által felidézett asszociatív módon generált képeket, történeteket szövegesen megfogalmaz, vizuálisan megjelenít, egyénileg és csoportmunkában is.

*  Szöveges vagy egyszerű képi inspiráció alapján elképzeli és megjeleníti a látványt, egyénileg és csoportmunkában is.

*  Látványok, képek részeinek, részleteinek alapján elképzeli a látvány egészét, fogalmi és vizuális eszközökkel bemutatja és megjeleníti, rekonstruálja azt.

*  A látványokkal kapcsolatos objektív és szubjektív észrevételeket pontosan szétválasztja.

*  Vizuális problémák vizsgálata során összegyűjtött információkat, gondolatokat különböző szempontok szerint rendez és összehasonlít, a tapasztalatait különböző helyzetekben a megoldás érdekében felhasználja.

*  Vizuális megjelenések, képek, mozgóképek, médiaszövegek vizsgálata, összehasonlítása során feltárt következtetéseit megfogalmazza, és alkotó tevékenységében, egyénileg és csoportmunkában is felhasználja.

*  Különböző korok és kultúrák szimbólumai és motívumai közül adott cél érdekében gyűjtést végez, és alkotó tevékenységében felhasználja a gyűjtés eredményeit.

*  Különböző művészettörténeti korokban, stílusokban készült alkotásokat, építményeket összehasonlít, megkülönböztet és összekapcsol más jelenségekkel, fogalmakkal, alkotásokkal, melyek segítségével alkotótevékenysége során újrafogalmazza a látványt.

*  Adott koncepció figyelembevételével, tudatos anyag- és eszközhasználattal tárgyakat, tereket tervez és hoz létre, egyénileg vagy csoportmunkában is.

*  Adott cél és szempontok figyelembevételével térbeli, időbeli viszonyokat, változásokat, eseményeket, történeteket egyénileg és csoportmunkában is rögzít, megjelenít.

*  Ismeri a térábrázolás alapvető módszereit (pl. axonometria, perspektíva) és azokat alkotómunkájában felhasználja.

*  Gondolatait, terveit, észrevételeit, véleményét változatos vizuális eszközök segítségével prezentálja.

*  Tetszésítélete alapján alkotásokról információkat gyűjt, kifejezőerő és a közvetített hatás szempontjából csoportosítja, és megállapításait felhasználja más szituációban.

*  Megfogalmazza személyes viszonyulását, értelmezését adott vagy választott művész alkotásai, társadalmi reflexiói kapcsán.

*  Látványok, képek, médiaszövegek, történetek, szituációk feldolgozása kapcsán személyes módon kifejezi, megjeleníti felszínre kerülő érzéseit, gondolatait, asszociációit.

*  Vizuális megjelenítés során egyénileg és csoportmunkában is használja a kiemelés, figyelemirányítás, egyensúlyteremtés vizuális eszközeit.

*  Egyszerű tájékoztató, magyarázó rajzok, ábrák, jelek, szimbólumok tervezése érdekében önállóan információt gyűjt.

*  Célzottan vizuális kommunikációt szolgáló megjelenéseket értelmez és tervez a kommunikációs szándék és a hatáskeltés szempontjait kiemelve.

*  A helyzetek, történetek ábrázolása, dokumentálása során egyénileg vagy csoportmunkában is felhasználja a kép és szöveg, a kép és hang viszonyában rejlő lehetőségeket.

*  Adott témát, időbeli, térbeli folyamatokat, történéseket közvetít újabb médiumok képírási formáinak segítségével egyénileg vagy csoportmunkában is.

*  Nem vizuális információkat (pl. számszerű adat, absztrakt fogalom) különböző célok (pl. tudományos, gazdasági, turisztikai) érdekében vizuális, képi üzenetté alakít.

*  Adott téma vizuális feldolgozása érdekében problémákat vet fel, megoldási lehetőségeket talál, javasol, a probléma megoldása érdekében kísérletezik.

*  Nem konvencionális feladatok kapcsán egyéni elképzeléseit, ötleteit rugalmasan alkalmazva megoldást talál.

### 9-10. évfolyamon
*  Feladatmegoldás közben kísérletezik, különböző megoldási utakat keres, és törekszik az egyéni megoldás igényes kivitelezésére.

*  Eszköz- és anyaghasználat során adekvát döntést hoz.

*  Adott tanulási helyzetben a tanulási előrehaladás érdekében problémákat keres, kérdéseket tesz fel, és ezekre önállóan is keresi a megoldásokat és válaszokat.

*  Feladatmegoldásai során a felhasználás érdekében hatékonyan szelektálja a korábban szerzett ismereteket.

*  Feladatmegoldásai során saját ötleteit és eredményeit bátran bemutatja.

*  Példák alapján érti a művészet kultúraközvetítő szerepét.

*  Példák alapján művészettörténeti korszakokat, stílusokat felismer és egymáshoz képest időben viszonylagos pontossággal elhelyez.

*  Példák alapján felismeri és értelmezi a kortárs művészetben a társadalmi reflexiókat.

*  Érti és meg tudja magyarázni a célzott vizuális közlések hatásmechanizmusát.

*  Ismeri néhány példáját a digitális képalkotás közösségi médiában használt lehetőségének.

*  Felismeri a designgondolkodás sajátosságait az őt körülvevő tárgy- és környezetkultúra produktumaiban.

*  A fenntarthatóság érdekében felelős döntéseket hoz a saját tervezett, épített környezetével kapcsolatban.

*  Csoportban végzett feladatmegoldás során részt vállal a feladatmegoldásban, önállóan megtalálja saját feladatát, figyelembe veszi társai álláspontját, de az optimális eredmény érdekében hatékonyan érvényesíti érdekeit.

*  Önálló véleményt alkot, és azt meggyőzően indokolja.

*  Adott területen megtalálja a számára érdekes és fontos kihívásokat.

*  A látható világ vizuális összefüggéseinek megfigyeléseit ok-okozati viszonyoknak megfelelően rendszerezi.

*  Alkotó és befogadó tevékenységei során érti, és komplex módon használja a vizuális nyelv eszközeit.

*  A vizuális megjelenések mintáinak önálló megfigyelése és felismerése által konstrukciókat alkot, e megfigyelések szempontjainak összekapcsolásával definiál és következtet, mindezt társaival együttműködve alkotótevékenységébe is beilleszti.

*  Adott feladatmegoldás érdekében meglévő vizuális ismeretei között megfelelően szelektál, a további szakszerű információszerzés érdekében adekvátan keres.

*  Az alkotótevékenység során szerzett tapasztalatait önálló feladatmegoldás során beépíti, és az eredményes feladatmegoldás érdekében szükség szerint továbbfejleszti.

*  Alkotó feladatmegoldásai során az elraktározott, illetve a folyamatosan újraalkotott belső képeit, képzeteit szabadon párosítja a felkínált tartalmi elemek és látványok újrafogalmazásakor, amelyet indokolni is tud.

*  Új ötleteket is felhasznál képek, tárgyak, terek megjelenítésének, átalakításának, rekonstruálásának megvalósításánál síkbeli, térbeli és időbeli produktumok létrehozása esetében.

*  A vizuális megjelenések elemzése és értelmezése során a befogadó és az alkotó szerepkört egyaránt megismerve reflexióit szemléletesen és szakszerűen fogalmazza meg szövegesen és képi megjelenítéssel is.

*  A művészi hatás megértése és magyarázata érdekében összehasonlít, és következtetéseket fogalmaz meg a különböző művészeti ágak kifejezési formáival kapcsolatban.

*  Adott és választott vizuális művészeti témában önállóan gyűjtött képi és szöveges információk felhasználásával részletesebb helyzetfeltáró, elemző, összehasonlító projektmunkát végez.

*  Megfelelő érvekkel alátámasztva, mérlegelő szemlélettel viszonyul az őt körülvevő kulturális környezet vizuális értelmezéseinek mediális csatornáihoz, amit társaival is megvitat.

*  Különböző mediális produktumokat vizuális jelrendszer, kommunikációs szándék és hatáskeltés szempontjából elemez, összehasonlít, és következtetéseit társaival is megvitatja.

*  Érti és megkülönbözteti a klasszikus és a modern művészet kultúrtörténeti összetevőit, közlésformáinak azonosságait és különbségeit.

*  Adott vagy választott kortárs művészeti üzenetet személyes viszonyulás alapján, a társadalmi reflexiók kiemelésével értelmez.

*  Adott szempontok alapján érti és megkülönbözteti a történeti korok és a modern társadalmak tárgyi és épített környezetének legfontosabb jellemzőit, miközben értelmezi kulturális örökségünk jelentőségét is.

*  Személyes élményei alapján elemzi a tárgy- és környezetkultúra, valamint a fogyasztói szokások mindennapi életre gyakorolt hatásait és veszélyeit, és ezeket társaival megvitatja.

*  Az adott vagy a választott célnak megfelelően környezetátalakítás érdekében, társaival együttműködésben, környezetfelméréssel alátámasztva tervet készít, amelyet indokolni is tud.

*  Bemutatás, felhívás, történetmesélés céljából térbeli és időbeli > folyamatokat, történéseket, cselekményeket különböző eszközök > segítségével rögzít.

*  Adott feladatnak megfelelően alkalmazza az analóg és a digitális > prezentációs technikákat, illetve az ezekhez kapcsolható álló- és > mozgóképi lehetőségeket.

*  Tervezési folyamat során a gondolkodás szemléltetése érdekében > gondolatait mások számára is érthetően, szövegesen és képpel > dokumentálja.

*  Képalkotás és tárgyformálás során autonóm módon felhasználja > személyes tapasztalatait a hiteles kifejezési szándék érdekében a > választott médiumnak is megfelelően.

*  Saját munkáit bátran újraértelmezi és felhasználja további > alkotótevékenység során.

*  Vizuális megjelenéseket, alkotásokat újraértelmez, áttervez és > módosított kifejezési szándék vagy funkció érdekében újraalkot.

*  Valós célokat szolgáló, saját kommunikációs helyzetnek megfelelő, > képes és szöveges üzenetet felhasználó vizuális közlést hoz létre > társaival együttműködésben is.

*  Szabadon választott témában, társaival együtt ok-okozati > összefüggéseken alapuló történetet alkot, amelynek részleteit > vizuális eszközökkel is magyarázza, bemutatja.

*  Adott téma újszerű megjelenítéséhez illő technikai lehetőségeket > kiválaszt, és adott vizuális feladatmegoldás érdekében megfelelően > felhasznál.

*  Technikai képnél és a számítógépes környezetben felismeri a > manipuláció lehetőségét, és érti a befolyásolás vizuális > eszközeinek jelentőségét.

*  Adott feladatmegoldás érdekében ötleteiből rendszert alkot, a célok > érdekében alkalmas kifejezési eszközöket és technikákat választ, > az újszerű ötletek megvalósítása érdekében szabályokat újraalkot.

*  Egyéni munkáját hajlandó a közösségi alkotás érdekei alá rendelni, a > hatékonyság érdekében az együttműködésre törekszik.

*  A leghatékonyabb megoldás megtalálása érdekében felméri a > lehetőségeket és azok feltételeit, amelyek komplex mérlegelésével > döntést hoz az adott feladatokban.

