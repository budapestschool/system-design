# Emberkép

A világ és benne az egyén állandóan változik, és a változás mégis
számos állandóságot jelöl ki. Korábban a technológiai változás
évszázadokban volt mérhető, az általunk használt eszközök száma és
komplexitása jóval kisebb volt, mára néhány év, vagy egy pár órás
repülőút elegendő ahhoz, hogy olyan környezetbe, olyan emberek, eszközök
közé kerüljünk, ahol másként kell alkalmazkodnunk, mint ahogy azt
tanultuk. Ebben a világban a saját belső harmóniánk megtalálása, a saját
közösségünkhöz, közösségeinkhez való kapcsolódás, a világ működésének
megértése és a törekvés arra, hogy tegyünk a fenntartásáért, különösen
fontossá vált. Ahhoz, hogy ezeknek a változásoknak meg tudjunk felelni, a tanári, szülői és a
tanulói szerepekben is folyamatos fejlődésre van szükségünk. A jövőbe
nem látunk, de egy dolgot biztosan tudunk: bármilyenek lesznek is a jövő
kihívásai, nekünk az a fontos, hogy ez a gyerekek számára ne félelmetes
és szorongást keltő legyen, hanem lehetőséget, kihívást és örömet
okozzon.

Azt szeretnénk, hogy a fiatalok az iskolában és utána is könnyen
megtalálják a saját útjukat. Képesek legyenek önmaguknak célokat
állítani, azokat elérni. Képesek legyenek már kisgyerekkortól
sajátjukként megélni a tanulást és ahhoz kapcsolódóan célokat elérni, és
fokozatosan tanulják meg azt, hogy egyénileg és csoportosan is tudjanak
nagyszabású projekteket véghezvinni. A tanulás három rétege, a tudás
megszerzése, az annak felhasználását segítő önálló gondolkodás és az
alkotás egyszerre jelenik meg a mindennapokban. Ezek egyensúlyban
tartása éppoly fontos, mint az, hogy a gyerekek a kerettantervben
szereplő tanulási eredményeket és a további célkitűzéseiket egyaránt
sajátjukként éljék meg ezzel is biztosítva, hogy személyes céljaik és a
társadalmi elvárások folyamatos egyensúlyban lehessenek.

Ezért _legfontosabb fejlődési dimenziónak az önálló, célorientált
tanulási és stratégiai gondolkodást tekintjük_, melynek ütemeit tanulási
szakaszokra bontjuk. A társas kapcsolatok tanulási folyamatában a
születéstől a felnőttkorig a teljes magára hagyatottság és énközpontúság
csecsemőkori állapotából fejlődik az ember önállóan és kreatívan
gondolkodó, önmagával és közösségével integránsan élő, érett nagykorúig.
Ezt az utat Erik Erikson pszichoszociális fejlődési modelljében
rögzítette [(Erikson, 1991)](https://psycnet.apa.org/record/1959-15012-000). A Budapest School ezt a modellt integrálja
tanulási struktúrájába, melynek eredményeképp az iskolakezdőtől az
érettségizőig négy tanulási szakaszt különít el. Ezen tanulási szakaszok
határait az önállósodás során tapasztalt egyéni mérföldkövek jelölik ki,
melyek fokmérője a saját cél állításának képessége, illetve célkitűzés időbeli és tartalmi realitása, valamint a közösséghez való
kapcsolódása. Amellett, hogy az egyes tanulási szakaszok a célállításban
elkülönülnek egymástól, a szintugrás abban is mérhető, hogy mennyire
képes egy gyerek az alatta lévő szinten lévő társait segíteni az
előrelépésben. A folyamatos kortárs támogatás a szintekben való
előrehaladás valós fokmérője a mentor és a szülő visszajelzései mellett.

Az egyes szintek közötti életkori határok elmosódnak, kontúrjaik nem
élesen válnak el egymástól, így inkább a fejlődés iránya mintsem a
korosztályi tagolás a fontos. Számos kutatás bizonyítja, hogy a gyerekek
mentális életkora és az egyes területeken való fejlettsége között jelentős
különbségek lehetnek. Vagyis előfordulhat, hogy egy gyerek
zongorázásából már jóval előrehaladottabb célokat tud kitűzni maga elé,
mint olvasásból vagy épp robotikából, és ez rendben van így.

#### Első szakasz, 5 — 10 év

Ebben a korban alakul ki a gyerekek logikus gondolkodása, melynek
részeként feladatokat tudnak rendszerezni, sorrendeket képesek
felállítani és azokat fogalmakhoz társítani. A gondolkodásuk elkezd a
befelé fordulóból a társas kapcsolatok irányába nyílni, ezáltal
megnyílik a közösségi problémamegoldás lehetősége. Igényük nő a belső és
a külső rendezettségre, így elkezdhetnek önmaguk szabályokat alakítani a
saját tanulási igényeik kapcsán. Az első szakaszban a gyerekek
pszichoszociális fejlődésükkel összhangban a saját maguknak állított cél
jelentőségének fogalmát tanulják rövid, eleinte néhány órás, majd a
szakasz vége felé néhány napos tervezéssel és erre adott önreflexiókkal
és külső megerősítésekkel. Megtanulják a ma, a tegnap és a holnap
fogalmát. Tanulási szerződésük tartalmát eleinte főleg a mentor és a
szülő segítségével állítják össze, hogy annak a szakasznak végére már
teljesen egyedi, önállóan kiválasztott elemei is lehessenek.

Ebben a szakaszban a szabad játéknak nagy szerepe van, a belső világ
tágassága fokozatosan nyílik meg a külvilág felé, és kezd lényegessé
válni az azokkal való kommunikáció kiérlelése. Ekkor tanulnak meg a
gyerekek írni, olvasni, és megismerik a matematikai alapműveleteket,
valamint a mindennapjaikban használatos geometriai és kombinatorikai
alapfogalmakat. Célunk, hogy önmagukhoz
és környezetükhöz érzékenyen, odafigyeléssel és empátiával forduljanak,
megtanulják tiszteletben tartani, hogy társaik másmilyenek, akiket más
dolgok is érdekelhetnek, másfajta megoldásaik is lehetnek.

_A szakasz
végére biztonsággal meg tudnak maguknak tervezni egy néhány napos
projektet._

#### Második szakasz, 9—15 év

A gyerek testi és pszichoszociális fejlődése szempontjából egyaránt
kiemelten fontos időszak következik. A korai serdülőkorban alakulnak ki
a másodlagos nemi jellegek, melyek ütemükben mind a fiúk és lányok, mind
az egyének között jelentős eltéréseket mutathatnak. Az egyéni tanulási
tervek összeállításának ezért ebben az időszakban megnő a szerepe.

A sajátként megélt tanulási célok eddigi alapozása ebben a szakaszban
nyeri el mélyebb funkcióját. A tinédzserkor minden szempontból a gyerek
kivirágzásának időpontja, melynek során a helyét kereső, befelé, saját
változásaira fókuszáló gyerekből egy, a jövő felé nyitott kamasz válhat.
A gondolkodási struktúrák mellett megnő a családon túli társas
kapcsolatok szerepe, és ekkor fontos, hogy a családias jelleg, a
biztonságos tanulás, a szülővel való konzultáció mint a gyerek számára
egyaránt fontos érték, és nem mint kényszer jelenjen meg. Az érvelés, a
hipotézis-alapú problémamegoldás, valamint a jelen fókuszált megélése
ebben a korban alakul ki, ahogy annak a tudata is, hogy tetteinknek a
jövőre nézve nagyobb mértékű következményei lehetnek.

Az agyi struktúrák jelentős átalakulása is erre az időszakra tehető, a
gondolkodás, a figyelem, az emlékezet területén is lehetnek ezen
átalakulások miatt nehézségek, amelyeket később, — az átmeneti visszaesést
követően — az agy fejlettebb működése követ. Ekkor alapozhatja meg
egy gyerek a tervezés, a saját tanulási célok komplexebb fogalmait.

Kiemelten fontos
ebben a szakaszban a különböző vélemények, információk, megoldási
lehetőségek számbavétele, annak lehetősége, hogy a gyerek egyénileg
ismerhesse fel az eltérő utak közötti különbségeket, és ha teheti,
kérdőjelezzen meg akár tudományos hipotéziseket, vagy írjon újra
művészeti, kulturális tartalmakat. Ha a gyerek ezen határok megértésében
szabadon fejlődik, akkor lehetősége lesz arra, hogy a megszerzett
alapképességeit olyan kreatív irányokba fordíthassa, melyek saját élete és
társadalmunk jövője formálásához egyaránt hasznosak lehetnek.

Aki
ebbe a szakaszba lép, már hetekre előre ki tudja jelölni a saját
feladatait. Három év alatt oda kell eljutni, hogy a szakasz végére
önállóan képes legyen egy teljes trimeszternyi tanulási tervet
összeállítani. Ehhez a hetek számát folyamatosan növelnie kell, miközben
a problémák komplexitása is változik. A szülő még mindig megjelenik a
célalkotásban, de egyre inkább átalakul a szerepe tanácsadóvá. A mentor
az egyéni fejlődés mintázataira figyelve segíti a gyereket abban, hogy
fejlettségi szintjének, aktuális testi, lelki változásainak megfelelő
módon legyen terhelve.

_A szakasz végén önállóan be tudja mutatni társainak egy pár hetes
projekt eredményeit, és mind szövegértése, mind logikus gondolkodása és
matematikai alapismeretei olyan szinten állnak, hogy elmélyült alkotó,
kutató feladatokat vállalhat a következő szakaszban._ Biztonságos
nyelvhasználata a sajátja mellett egy másik nyelvben is elindul, és
értékrendjében önmaga és társai megismerésén túl a környezet és a világ
fogalma is tágulni kezd.

#### Harmadik szakasz, 13—19 év

Ebben a szakaszban a gyerek fizikai értelemben teljesen felnőtté válik, ehhez kiemelten szüksége van érzelmi és szociális biztonságra. A
szerelem, a szexuális identitás erősödése, az önállósodásra való igény,
az addikciók veszélye konfliktusokat szülhet, amelyek kezelése különösen
fontos ebben az időszakban. Az iskola (elsősorban a mentor) a szülővel együttműködve tudja megteremteni ezt a biztonságos közeget.

A szakasz célja, hogy a gyerek képessé váljon a saját életét meghatározó,
egy-két éves távlatokban mérhető felelős döntések meghozatalára, melyek
akár sorsfordító jelentőségűek is lehetnek. Ezek a döntések
vonatkozhatnak egy emelt szintű érettségire való felkészülésre, egy
nemzetközi egyetemre való felkészülésre, egy nagyobb komplexitású
kutatási vagy alkotói projektre. Fontos azonban a gyerek támogatása
abban is, hogy beláthassa, nem kell örök érvényű döntéseket hoznia.

Ebben az időszakban a saját tanulási célokat egy teljes trimeszterre vonatkozóan
egyénileg határozza meg a gyerek, a szülő és a mentor ebben a folyamatban mint
konzulens jelennek meg. Meghozza első komolyabb döntéseit arról, hogy
milyen területekkel szeretne elmélyültebben foglalkozni, és ehhez
kapcsolódóan választott érettségi tantárgyait is kijelöli a szakasz
közepére. Önállóan készül az érettségire,
tanárai segítik, hogy folyamatosan megtalálja a kihívást ebben. Saját
céljai szűkebb környezetén túl könnyedén hatással lehetnek már a világra
is.

Előadásmódját, kutatási és alkotói munkáját felnőtt jegyek
jellemzik.
