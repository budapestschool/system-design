# A Budapest School Modell

A Budapest School Modellben azt mutatjuk be, amit az elmúlt években a tanulásról megtanultunk. Annak az útnak a tapasztalatait összegezzük, amely során az álomból az első csoportjaink létrejöttek, majd újabbak és újabbak alakultak. Ahogy nőtt a Budapest School, úgy kezdtük egyre jobban megérteni, mit is csinálunk és miként kell ezt a tanárok, a családok, a gyerekek, valamint a társadalom és a jogalkotó elvárásaihoz igazítanunk.

Az elmúlt években azon dolgoztunk, hogy magántanulók közösségéből államilag elfogadott iskolává válhasson az, amit mi Budapest Schoolnak hívunk. Arra a kérdésre kerestük a választ, hogy létrehozhatunk-e egy személyre szabott, önvezérelt tanulási modellt úgy, hogy az mindenben megfeleljen a törvényi előírásoknak. Ezért írtunk először egy _kerettantervet_, ami arra a kérdésre válaszol, hogy mit tanulnak a gyerekek. Amikor ezzel elkészültünk, leírtuk azt is, hogyan tanulnak, és mire a kérdés minden részlete kibomlott előttünk, elkészült a _pedagógiai programunk_ is. Menet közben ráébredtünk, hogy a mit és a hogyan kérdése közötti határok jóval elmosódottabbak, mint azt elsőre gondolnánk. Egy Budapest School Modell alapján működő iskolába vagy osztályba járó gyerekek ugyanis éppannyira dönthetnek a tartalomról, mint amennyire formálói lehetnek a mindennapok működésének is.

Így jött létre a Budapest School Modell, ami azt írja le, hogy a mai tudásunk szerint hogyan szervezünk tanulási környezeteket. Ugyanazt a modellt használhatjuk írni, olvasni, számolni tanuló kisiskolások mindennapjainak szervezéséhez (alsó tagozat), egy mély szakmai képességre fókuszáló középiskola megszervezéséhez (technikum) vagy egy általános gimnázium működtetéséhez.

A BPS modell a tanulás szervezését, azaz a pedagógiáját a motiváció [(Pink, 2011)](https://www.amazon.com/Drive-Surprising-Truth-About-Motivates/dp/1594484805), a fejlődési szemlélet [(Dweck, 2006)](https://psycnet.apa.org/record/2006-08575-000) és az elmélyült gyakorlás [(Ericsson, 2016)](http://peakthebook.com/index.html)
pszichológiai kutatási eredményei és a
modern tanulásszervezési paradigmák, mint az önvezérelt
[(Mitra, 2012)](https://books.google.hu/books?id=ENfanQAACAAJ) és a személyre szabott [(Khan, 2012)](https://books.google.hu/books?id=xz-gkDYm4UUC) tanulás alapján
határozza meg.

Az iskola a gyerekek számára egy önvezérelt és személyre szabott tanulási környezetet biztosít. Az igényekre agilisan reagál, és közben stabil, biztonságos és kiszámítható rendszerként biztosítja az iskola és más iskolák közötti átjárhatóságot, a továbbtanulás lehetőségét, a jogszabályok betartását és az iskola elszámoltathatóságát.

## BPS Modell több iskolában

A BPS modell egy iskola működési rendszerét írja le. Rendszer, mert definiálja az iskola szereplőit, azok interakcióját és együttműködésüket. Mindeközben amit ebben a dokumentumban leírunk, csak egy modell, mert minden iskolában kicsit másként működnek majd a dolgok, hiszen emberek a modellt a saját céljaik szerint alakíthatják.

BPS modell alapján több különböző iskola tud működni. A BPS modell ugyanúgy érvényes tud lenni egy általános iskolában, egy gimnáziumban, egy technikumban, vagy akárcsak egy iskola kísérleti osztályában.
