# Miért létezik a BPS iskola?

Célunk, hogy a gyerekeink boldog, egészséges felnőttekké váljanak, akik hozzájárulnak környezetük jóllétéhez.

A BPS-ben a gyerekek biztonságos és támogató közösségben fedezik fel és fejlesztik önbizalmukat, képességeiket. Tudatosul bennük, hogyan hozhatják összhangba törekvéseiket a lehetőségeikkel. Megtanulják használni saját erősségeiket, és örömet találni az önmaguk elé tűzött kihívások teljesítésében. Folyamatosan haladnak a céljaik felé, miközben az egészségükre, kapcsolataikra és a boldogságukra is odafigyelnek.

A BPS-gyerekek belülről fakadó érdeklődéssel és empátiával nyitnak a világra, és konstruktív módon alakítják mind a saját életüket, mind a körülöttük zajló folyamatokat.

A jószándék mellett tudatosan is tesznek azért, hogy pozitív változásokat teremtsenek a környezetükben.
