# Egyéni és közösségi szempontok

![Egyéni és közösségi szempontok a Budapest School iskolában.](../pics/celok_egyensulya.jpg)

A Budapest School modellben az iskolának két rétege van. A gyerek
igényeire gyorsan reagáló tanulóközösség egy burok a gyerek körül. Ezt a gyermekközpontú réteget veszi körül a társadalomhoz való kapcsolódást biztosító külső réteg.

A belső burokban a Budapest School Modell a tanulás folyamatát szabályozza, a
_Mit tanulunk?_ kérdés helyett a _Hogyan szervezzük meg a tanulást?_
kérdésre ad választ. A tanulás során a gyerekek a Budapest School
Modellben részletezett módon az érdeklődésüknek megfelelően specifikus
tanulási egységeket, vagyis modulokat végeznek el, melyek eredményeit a
saját portfóliójukban gyűjtik össze. Így a gyerekek tanulási útja a
portfólió fejlődésével nyomon követhető, és a portfólió tartalma alapján
megállapítható a gyerek aktuális tudása, képessége. Az iskola fő
funkciója emellett mindvégig az aktív tanulás, saját fejlődésük
kereteinek megtalálása, a folyamatosan újragondolt saját célok állítása,
és e célok irányába történő haladás marad.

A belső réteg a személyre szabott és önvezérelt tanuláshoz szükséges célkitűzés-tervezés, tanulás,
és az arra történő reflektálás módját írja le, vagyis a tanulás
folyamatát rögzíti, míg annak pontos tartalmában szabadságot enged.

## NAT-hoz, a kerettantervekhez és más követelményekhez való kapcsolódás

Ezt a szabadságot keretezik a társadalmi normák és jogszabályi
elvárások, hogy biztosítva legyen a gyerekek boldogulása a BPS iskolán kívül is. Ezért a BPS modell alapján működő iskola programja a magyar Nemzeti alaptanterv [(Nat, 2020)](http://www.kozlonyok.hu/nkonline/MKPDF/hiteles/MK20017.pdf), a miniszter által közreadott kerettantervek [(Kerettanterv, 2020)](https://www.oktatas.hu/kozneveles/kerettantervek/2020_nat) ismeretanyagát, azok tantárgyi struktúráját és tanulási eredményeit teljes mértékben tartalmazza. A BPS technikumok a szakmának megfelelő _Képzési és Kimeneti Követelményeket_ veszik alapul.

Az iskolában sok mindent és sokféleképpen tanulhatnak a gyerekek. Azonban a NAT, KKK által definiált tanulási eredmények elérése egy alap minimumnak tekintendő, azok elérése mindenki számára kitűzött cél. Az iskola szabadságot ad a gyerekeknek, tanároknak és szülőknek abban, hogy a gyerekek _hogyan_ érik el ezeket az elvárt eredményeket és ezen kívül fontos célnak tekinti, hogy a NAT ismeretanyagán túli világot is megismerhessék a gyerekek.

## Tantárgyak és osztályzatok

A NAT és a miniszter által közreadott kerettantervekkel 100%-ban megegyező tantárgyi struktúra, a tanulási
eredmények elérését célzó moduláris tanulásszervezés, és a többszempontú szöveges visszajelzési rendszer biztosítják az átláthatóságot a tanulás nyomon követésében, és az átjárhatóságot a BPS és más oktatási intézmények között.

Technikumi képzés esetén a Képzési és Kimeneti Követelmények alapján készült tantárgyak definiálják a szakmai képzés tartalmát.

A mindennapokban a tantárgyi érdemjegyeknél és osztályzatoknál sokkal részletesebb, több szempontot figyelembe vevő szöveges, értékelőtáblázat- (rubric-) alapú visszajelzést kapnak a gyerekek. Ezek adják az osztályzás alapját: azokban a tantárgyakban és évfolyamokon, amikor erre szükség van, a tanulóközösség tanárcsapata a gyerek dokumentált visszajelzései és portfóliója alapján alátámasztott döntést hoz a tantárgyi osztályzatokról (/tanulasi-elmeny/bizonyitvany-evfolyamok.md#evfolyam-osztalyzatok-bizonyitvany).

A Budapest School Modell transzparenssé teszi az iskolában működő
két színtű struktúrát: a gyerekközpontú, személyre szabott, saját
tervezésen alapuló mindennapi tanulási élményt leképezzük a
a NAT és a kerettantervek tantárgyi struktúrájára. Az
osztályzatok biztosítani képesek az átjárhatóságot és a
továbbtanuláshoz szükséges feltételeket. A kettős rendszer, a gyerekekre szabott belső tanulási környezet és a kiszámíthatóságot adó külső kapcsolódások biztosítják,
hogy az iskola végén a gyerekek sikeres záróvizsgát (érettségi és technikusi) tegyenek.

## Pedagógiai módszersemlegesség

A Budapest School Modell a tanár feladatának tekinti, hogy mindig az optimálisnak
tűnő tanulási, tanítási, gyakorlási módszert válassza. Ezért ez a modell
nem beszél arról, hogy a modulokat (foglalkozásokat, tanórákat) milyen
pedagógiai módszer alapján szervezi a tanár. Van, amikor egy frontális előadás a megfelelő, és van, amikor a kooperatív tanulásszervezés adja a legjobb eredményt.

A Budapest School Modell az iskolába járókat gyerekeknek hívja, a tanulók és diákok szinonímájaként. Azok a gyerekek, akik tanulmányaik vége felé felnőtté érnek
a Budapest Schoolban, tanulók is maradnak, és talán egy kicsit gyerekek
is, ezért a szóhasználaton miattuk sem változtatunk. Az ő esetükben a
gyerek az iskolába járó tanulót jelenti.
