# Kapcsolat a hazai és nemzetközi oktatási irányzatokkal

A Budapest School programja a hazai és nemzetközi oktatási reformok
kontextusában és a pszichológia, a szociálpszichológia, valamint a
szervezetfejlesztés terén elvégzett kortárs kutatások tükrében válik
könnyebben értelmezhetővé.

Magyarországról több iskola története, működése is nagy hatással volt
ránk. A 90-es évektől induló alternatív iskolák világát mi a [_Rogers
Személyközpontú Általános Iskola_](https://www.rogersiskola.hu/), a [_Lauder Javne Iskola_](https://www.lauder.hu/wp/), a
[_Kincskereső Iskola_](https://www.kincskereso-iskola.hu/) és a [_Gyermekek Háza_](https://gyermekekhaza.hu/) alapján ismertük meg. A
megújuló középiskolák modelljének mi az [_Alternatív Közgazdasági
Gimnáziumot_](https://www.akg.hu/) és a [_Közgazdasági Politechnikumot_](https://poli.hu/wp/) tartjuk. Ezek az
iskolák a személyközpontúság, a gyerekközpontúság hangsúlyozása mellett
elkezdték a gyakorlatban alkalmazni a partnerség alapú kommunikációt, a
differenciálás, a kooperatív technikák módszerét és egyes
projektmódszertanokat.

Programunk kidolgozásában nagy szerepe volt annak, hogy ezek az iskolák
olyan szemléletmódbeli alapokat fektettek le, amelyek mára
alapelvárásként fogalmazódnak meg a szülők oldaláról az iskolákkal
szemben.

Gyakorlati tapasztalatokat a világ más részein is gyűjtöttünk. A 21.
században a Budapest Schoolhoz hasonló kezdeményezések sorra indulnak a
világban. Ezek egyes jegyei a Budapest School modelljével összhangban
vannak:

A [_Wildflower School_](https://wildflowerschools.org/) tanulóközösségek hálózatát működteti kisebb
üzlethelyiségekben. A Budapest Schoolhoz hasonlóan célja, hogy falakat
romboljon a gyerekek és a világ között: az egyéni tanulás és az intézményes
tanulás, a tanár és a tudós szerepe, valamint az iskola és környezete
közötti határok elmosása az egyik fő üzenete.

Hasonlóan az otthon tanulás és az _unschooling_ strukturált formáját
keresi az amerikai Texasban alapított [_Acton Academy_](https://www.actonacademy.org/), amely a
szokratikus módszereket (azaz, hogy megbeszéljük közösen), a valós
projektből való tanulást, és a gyakornokoskodáshoz hasonló munka
közbeni tanulást („learning on the job") teszi a megközelítésének
középpontjába.

A [_High Tech High_](https://www.hightechhigh.org/) iskoláiban a gyerekek elsősorban
projektmódszertan alapján tanulnak. A tanulási jogokban való egyenlőség
mellett az egyéni célokra szabott tanulás, a világ alakulásához
kapcsolódó tartalmi elemek, valamint az együttműködés-alapú tanulás is
megjelenik pedagógiájukban a Budapest School által is alkalmazott
jegyekből.

A [_School21_](https://www.school21.org.uk/) brit iskola 21. századi képességek fejlesztését tűzte
ki célul. Ezért a prezentációs, előadói skillek kiemelt jelentőségűek.
Az iskola egyensúlyt akar teremteni a tudásbéli (akadémiai), a szívbéli
(személyiség és jóllét) és a kézzel fogható (problémamegoldó, alkotó)
között. A Budapest School iskoláinak hasonló módon célja, hogy a tanulás
három rétegét, a tudást, a gondolkodást és az alkotást folyamatos
harmóniában tartsa.

A [_Khan Lab School_](https://khanlabschool.org/) a Montessori-módszert keveri az online
tanulással. Kevert korosztályú csoportokban, személyre szabott
módszerekkel segítik a képességfejlesztést és a projektalapú munkát. A BPS modell négy tanulási szakasza tulajdonképpen megfelel a Khan Lab School [függetlenségi szintjeinek](https://khanlabschool.org/independence-levels).
