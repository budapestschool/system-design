# A tanulni tanulás hét pillére

A tanulás egyénenként változó, és ha vegyes korosztályokban is, mégis
egy közösségben valósul meg. _A tanulni tanulás hét pillére_ minden
korosztályban meghatározza a Budapest School
működését.

## Tanulni tanulunk — fejlődésközpontúak vagyunk

A BPS iskolában mindig ott, akkor és annak kell
történnie a gyerekekkel, ami őket a fejlődésükben a leginkább támogatja.
Minden, ami az iskolában történik, újra és újra erre az alapkérdésre
kell, hogy visszatérjen. Az segíti a leginkább a gyerekek fejlődését,
amit most csinálunk, vagy változtatnunk kell rajta? A Budapest School
tehát rugalmas és integratív, a gyerekek fejlődéséhez igazodó.

A gyerekek tanulása [fejlődésközpontú szemléletben](https://pszichoforyou.hu/carol-s-dweck-minden-hiba-ujabb-lehetoseg-tanulasra/)
(angolul: growth mindset) történik. Erőfeszítéseik segítségével képességeik fejleszthetők, megváltoztathatóak, ha kellő támogatást kapnak. Számukra inspiráló a kihívás, és a hibázás kevésbé töri le lelkesedésüket. A gyerekek és a tanárok számára ezáltal nagyobb fontossággal bír a tanulásba fektetett erőfeszítés és a
fejlődés, mint az aktuális teljesítmény.

A hibázás inkább válik a gyakorlás és az új megismerésének jelzőjévé.
Ennek feszültségmentes kezelése kulcsfontosságú abban, hogy a gyerekek
merjék feszegetni a saját határaikat, hogy magabiztosan dolgozzanak
azon, hogy képességeiket, ismereteiket vagy gyakorlataikat folyamatosan
fejlesszék. A hibázásból való tanulás fő célja, hogy mindig új hibákat
ismerjenek meg, és a korábbiakra minél jobb megoldásokat találjanak a
gyerekek.

## Tanulni tanulunk — saját célokat állítunk

A gyerekek saját erősségeiket fejlesztve saját célokat állítanak, közben
céljaikat folyamatosan igazítják a világ adta lehetőségekhez és
szükségletekhez.

A tanulás egésze egy olyan folyamatként írható le, amely különböző
állomásokra, rövid célokra bontható. A tanulási célok állításának
folyamata, annak minősége az évek alatt folyamatosan változik, egyre
tudatosabbá, pontosabbá, komplexebbé válik. Ennek feltétele, hogy már az első évektől el kell kezdeni — az életkornak és személyes lehetőségeknek megfelelő — célállítás gyakorlását.

A gyerekek jellemzője a kíváncsiság, az igény a felfedezésre,
tapasztalásra. BPS-gyerekek számára _a tanulás egy önvezérelt aktív
folyamat_, melynek megtartása és folyamatos fejlesztése a Budapest
School tanárainak legfőbb feladata. Nem csak azokra a képességekre fókuszálunk, amelyekre ma szükségük van, mert így tudásuk veszítene a világ változásával a korszerűségéből. Az iskola abban segíti őket, hogy megtaníthassák maguknak azokat a képességeket, amelyekre épp az adott élethelyzetükben szükségük lesz. A tanulás így élményszerűvé válik, ismeretszerző jellege csökken, és nő az önálló felfedezés lehetősége.

## Tanulni tanulunk — mindig és mindenhol

A tanulásra a Budapest Schoolban mindig van lehetőség. A tanulási környezet pontos kialakítása a gyerekek igényeitől, fejlettségi szintjétől és
korosztályától is függ. A tanulási rend meghatározásáért a BPS tanulásszervezői felelnek.

A tanulás során jut idő egyéni és csoportos, gyakorló,
ismeretszerző és alkotó foglalkozásokra is. A tanulási egységek között van idő
fellélegezni és felkészülni az újabb modulokra. Van, amikor az is megoldható,
hogy a gyerekek és tanárok újratervezzék az időrendjüket, ha egy tanulási egység nagyon magával ragadja a gyerekeket, és nagyon benne maradnának abban a tevékenységben.

A tanulás az iskolában nem ér véget. A tanulás szeretetének
kialakulásával folyamatossá válik az ezzel való foglalkozás, így a
Budapest School tanulásnak tekinti az otthon vagy a szünetekben tanulással eltöltött időt is, ahol néha hatékonyabb módon tud egy gyerek gyakorolni, kutatni, alkotni, mint az iskolában, amikor társaival van egy közösségben és
ezáltal számos más inger is éri.

Az iskolában történő tanulással egyenrangúnak tekintjük az otthon
tanulást, az iskolától független iskola utáni programokat, a (nyári)
táborokat, a családi utazásokat, a vállalatoknál töltött gyakornoki
időt, az egyéni tanulást és projekteket. A tanulás bárhol és bármikor
történhet, amíg az eléri a célját. Célunk, hogy a gyerekek mindenhol és mindig tanuljanak.

## Tanulni tanulunk — együtt, egymástól

A tanulás egyénileg és csoportokban is történhet. A csoportok
megszervezése mindig azon múlik, hogy az adott tanulási célt mi
szolgálja a legjobban. Ennek megfelelően a gyerekek nem állandó, hanem a
tanulási célokhoz, az érdeklődéshez, a képességi szintekhez alkalmazkodó
rugalmas csoportokban tanulnak. A tanulás ezáltal kevert korcsoportokban
is történhet, akár egy nagy családban. Együtt, egymástól tanulnak a
gyerekek, egymást segítik a fejlődésben. Az egymásnak adott
visszajelzések, megerősítések révén folyamatosan alakul ki a
tanulás tisztelete és a képességek fejlesztésébe vetett hit, az együttműködésre és az alkalmazkodásra való képesség.

A közösségben tanulás módja nagyban függhet attól, hogy egy gyerek
mennyire zárkózott, mennyire tud és akar önállóan tanulni. A csoportos
munkák során alapelv, hogy a zárkózott gyerekek is lehetőséget kapjanak,
hogy csöndesen, vagy kisebb csoportban végezhessék a munkájukat,
mondhassák el ötleteiket. Az egyéni tanulásban minden gyereknek
lehetőséget kell adni arra és segíteni kell abban, hogy önállóan,
fókuszáltan tudjon tanulni.

## Tanulni tanulunk — alkotunk és felfedezünk

A tanulás három rétege, az ismeretszerzés, a gondolkodás fejlesztése és
az alkotás egyszerre jelenik meg a Budapest School
mindennapjaiban. Az alkotó munka rugalmas időkereteket, változó
csoportbontásokat, és a projektmódszerek sokszínű alkalmazását igényli.
A tanulás ilyenkor sokszor inkább alkotássá válik, az ismeret pedig termékké
változik.

A gyerekek önmaguk és a világ számára releváns kérdésekkel foglalkoznak,
amihez külső szakértőket is bevonnak, ha szükséges. A tanulás tehát
célokhoz, nem pedig tárgyakhoz kötött. A tanulás tartalmát igazítjuk a
tanulás céljához, ezáltal az egyes tudományterületek, művészetek, vagy
épp mesterségek gyakran keverednek egymással egy-egy modulon belül.
Szintén a célhoz igazított tanuláshoz kötődik a kutató-felfedező
attitűd, ami az ismeretlen
felfedezésére, a megválaszolhatatlan megválaszolására irányul.

## Tanulni tanulunk — egymásra figyelünk

A gyerekek tanulását családi hátterük változása, egyéni problémák,
számos mindennapi esemény befolyásolhatja. Ezek figyelembevétele a
mindennapokban, a _mentortanárral_ való bizalmi viszonynak köszönhetően
válik lehetségessé. Ennek a kapcsolatnak az alapjait ezért a partnerség,
az értő figyelem adja.

A Budapest School emellett kiemelt figyelmet fordít arra, hogy a sajátos
nevelési igényű tanulók is lehetőséget kapjanak a csoportban való
munkára, amennyiben az a közösség számára is hasznos. Tanulásukat, ha
szükséges, külső szakember segíti. A Budapest School a hátrányos
helyzetű gyerekek számára is biztosítani kívánja az elfogadó, fejlesztő
környezetet. Az egyenlő bánásmód megvalósulása érdekében olyan
differenciált tanulási környezetet alakít ki, ami biztosítja a minél
nagyobb mértékű inkluzivitást.

## Tanulni tanulunk — tanulva tanítunk

A Budapest School _tanulásszervezői_ partnerként, a tanulás folyamán
segítő társként vannak jelen a gyerekek életében. A tanulás tanórák
helyett pontos tanulási célokat tartalmazó tanulási modulokból épül fel,
melyek során a tanár az adott cél eléréséhez szükséges eszközöket,
tanulási segédleteket biztosítja.

A tanár akkor és annyira segíti a gyerekeket a saját céljuk elérésében,
amennyire az épp szükséges, és folyamatosan tekintettel van arra,
amit a gyerek saját fejlődési üteme megkíván. Ehhez tudatosan
kell kezelnie nemcsak a gyerekeket érintő fejlesztési lehetőségeket,
hanem azt is pontosan látnia kell, hogy egy adott tanulási cél
elérésének milyen készségszintű vagy gyakorlatias alapfeltételei vannak.
Ezért a gyerekek tanulási célját támogatandó segítenie kell abban, hogy
a gyakorló idő, az alkotó idő és az új ismeret
megszerzésének ideje folyamatos egyensúlyban legyen. A tanár a
gyerekekkel együtt fejlődik, saját tanulása a segítői szerepben
folytonos.
