---
meta:
  - name: "og:title"
    content: BPS ált iskola és gimnázium COVID protokoll V6
  - name: "og:description"
    content: Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.
  - name: description
    content: Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.
---

# BPS COVID protokoll

Már két éve közösen dolgozunk az alábbi szabályok, a BPS covid-protokoll alakításán. Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át és módosítjuk időről-időre a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.

Három feladatunk van:

1. A vírus terjedésének nehezítése akkor, amikor senkiről nem tudjuk, hogy fertőzött-e.
2. A vírus továbbterjedésének megakadályozása, ha kiderül, hogy egy adott mikroiskola egy tagja megfertőződött.
3. A közösségi élmény, az együtt töltött idő maximalizálása úgy, hogy eközben mindenki biztonságban tudhassa a gyerekét,

A BPS mikroiskola struktúrája nagyban növeli a családok és a tanárok biztonságát, mert egy nagyságrenddel kevesebb emberrel kerül egy BPS-gyerek kapcsolatba, mint egy normál városi iskolában. Néhány alapvető szabállyal ezt a szociális távolságtartást tovább tudjuk növelni, miközben a mikroiskolák felkészülhetnek arra a szintre, amikor még kisebb karanténcsoportokban találkoznak a gyerekek, és elindul a hibrid oktatás, vagy akár arra is, ha újra át kell állnunk a teljes online oktatásra.

Az alábbi szabályok betartása annak feltétele, hogy gyereked tanulni tudjon a mikroiskolában, és tanárként végezhesd  a munkád. Minden gyerek megismeri, tanárok, és minden családból legalább egy szülő aláírja, hogy az alábbi szabályokat elfogadja, betartja.


##  Általános szabályok



1. Minden tanárnak javasoljuk a három oltást.
2. A BPS-ben az oltás a norma és közös célunk az iskola védelme. 

    Ennek értelmében, a covid felelősök megírják a szülőknek a következőket: 

      * Adott iskolában hány tanár van 3X / 2X  beoltva, és 
      * Aki nincs az miért (egészségügyi ok, saját döntés).  

    A covid felelősök az összesített adatokat HP-nak elküldik, aki a teljes szervezet számára láthatóvá teszi. 

3. Oltható korú gyerekek iskoláiban (azaz 12 év felettieket fogadó iskolákban:  középiskola, felsők)
    1. Felmérjük, hogy ki van oltva azok közül, akik felnőttként 10 órát hetente bent vannak és a közösséggel megosztjuk az összesített adatokat 
    2. Felmérjük, hogy a gyerekek hány százaléka vette fel az oltást 2X, és az összesített adatot megosztjuk.
    3. Kötelező lesz a - minimum kettő, 6 hónapon belüli - oltás a mentorok és a heti 10 óránál többet tartó modultartók, admin, menedzser, dodó, konyhás tehát sokat jelenlévő (tehát heti 10 órát vagy többet) staff számára dec 1 -től.
    4. A mentor csapat és a menedzser egyetértésével indokkal felmenthetnek egyéneket a kötelező oltás alól, és ezt le kell dokumentálni (HP-nak küldeni), és meg kell írni a szülőknek
    5. A mikroiskola dönthet úgy, hogy fertőzés esetén nem zár be 10 napra, csak 6 napra. Akkor, ha heti rendszeres tesztelés, maszkviselés  és 80% fölötti átoltottság van a közösségben és, amikor  az iskolában egy fertőzöttet jelentenek
        1. Az utolsó kontakttól számított 6 napra bezárnak
        2. A fertőzöttre a karanténszabályok vonatkoznak (10 nap, utolsó 2 tünetmentesen)
        3. A 7. napi visszatérés előtt teljes antigén teszt a közösségnek, 
            1. aki poztiív, otthon marad (10 napig) 
            2. Aki negatív: másnaptól visszatér az iskolába
4. 12 év alatti gyerekeket fogadó iskolákban mentoroknak nem kötelező az oltás, de nagyon javasolt és a 2. pont miatt nyilvánosságra kell hozni a szülőknek és a szervezetnek.
5. A LAB-ban mindenkinek kötelező az oltás. Ahol kötelező az oltás, ott nem veszünk fel olyan embert 6 hónapig, aki nem oltott.
6. A protocol betartásáért továbbra is az iskolák covid-felelősei és managerei a felelősek. A teljes szervezet szintjén is megjelenik ezentúl a "BPS covid-felelős" szerepe.

## Készültségi szintek


Négy készültségi szintet különböztetünk meg. Szeptemberben az első szinten kezdjük az iskolát.


### Első szint: minden olyan, mint a régi szép időkben, csak tudatosabban készülünk lehetséges pandémiára

Minden mikroiskola tanulászervezői választanak egy covid-felelőst, aki első az egyenlők között, Az a dolga, hogy mindenről tudjon, és ha gyorsan kell, akkor döntsön. A covid-felelős elszámoltatható (accountable) azért, hogy



* a mikroiskola saját szabályait kidolgozza, elfogadja, és a szülőkkel megossza;
* megállapítsa és hangosan, tanárok, gyerekek, szülők és a LAB számára hallható módon megmondja, hogy milyen szinten van a mikroiskola, és azonnal jelezze, ha ebben változás történik;
* szükség esetén krízismenedzserként döntéseket hozzon;
* döntsön a harmadik szintre való lépésről;
* elérhető legyen mindenki számára, ha kérdése, fenntartása van. Szülőként a covid-felelősnek jelezd, ha úgy gondolod, valamin változtatni kell.

Emellett alapvető higéniai szabályokra figyelünk, azaz



* mindenki könyökhajlatba köhög, tüsszent. Papírzsebkendő minden teremben mindenkinek mindig elérhető;
* folyékony szappant és papírtörölközőt, kézszárítót, vagy névre szóló textiltörölközőt biztosítunk a kézmosáshoz;
* a mikroiskolák bejáratánál vírusölő hatású kézfertőtlenítőt biztosítunk.

### Általános védekezési fázis - még járunk iskolába, és a fentiek mellett további szabályokat hozunk a vírus terjedésének megakadályozására

* Lecsökkentjük a kontaktszemélyek számát (gyerekekek csak a saját mikroiskolájuk gyerekeivel és tanáraival találkoznak)
    * Nem szervezünk olyan programot, amin különböző mikroiskolák gyerekei egymással találkoznak
    * Minden mikroiskolába csak a mentortanárok, és az adott trimeszterre szerződött modulvezető szaktanárok, szerződött helyettesítők jöhetnek, valamint más BPS iskola oltott tanára maszkban hospitálhat. (Azaz: nincs például vendégelőadó, egyszeri speciális foglalkozás.)
    * E szinten lévő iskola oltott mentortanára mehet hospitálni másik BPS iskolába. 
* A szülők és a tanárok az iskolában nem kerülnek kontaktusba, nem találkoznak.
    * A szülői köröket, szülő-tanár konzultációkat, egyéb programokat online vagy szabadtéren tartjuk.
    * A szülők, és a gyerekekért érkező kísérők csak maszkban, egy meghatározott pontig (pl. előtér) jöhetnek be az iskolába. Ezt a határt, és hogy náluk melyik térben egyszerre hány szülő tartózkodhat, a mikroiskolák határozzák meg.
* Minimalizáljuk a külső kapcsolatokat
    * A külső helyszíni zárt terek látogatása (pl: könyvtár) csak maszkban (lehetőleg ffp2) elfogadható . A mikroiskolák döntenek arról, hogy használják-e a városi tömegközlekedést és a vasutat, vagy nem. Ha igen, akkor utazás közben mindenki maszkot fog viselni.
* Az iskolában csak akut betegségtüneteket nem mutató gyerekek és felnőttek tartózkodhatnak.
    * Amennyiben a gyerekednél 48 órán belül a következő _rá nem jellemző_ tünetek legalább egyikét észlelted, ne gyertek iskolába.
        * Köhögés, fejfájás, torokfájás, orrfolyás, tüsszögés
        * bágyadtság és levertség
        * 37,5 C fok feletti testhőmérséklet
        * nehézlégzés, légszomj
        * szaglásvesztés, ízérzés hiánya vagy ízérzés zavara
        * hallással kapcsolatos panaszok
        * szemirritáció, szemváladékozás
        * izomfájdalom
        * hányinger, hasmenés

A delta variáns tünetei miatt könnyen [összetéveszthető az allergiával, megbetegedés esetén könnyen tűnhet súlyosabb lefolyású náthának is](https://koronavirus.gov.hu/cikkek/gyorsabban-terjed-es-allergiaszeru-tuneteket-okoz-delta-virusmutans).



* Érvényesnek tartjuk a még tavaly kiadott [tájékoztatót](https://koronavirus.gov.hu/cikkek/fokozott-szulok-es-tanarok-felelossege-jarvany-miatt-tunetes-gyereket-ne-engedjunk-kozossegbe), ami szerint “A legenyhébb tüneteinek jelentkezése esetén se engedd közösségbe a gyermeked!”
* Ha mi látunk az adott gyerekre nem jellemző tünetet a fentiek közül, felhívunk, és megkérünk, hogy haladéktalanul vidd haza.
* Amikor gyereked a fenti tüneteket mutatja, telefonon hívd fel a háziorvost, vagy gyermekorvost, és ne vidd a rendelőbe a gyereked. Az orvos dönti el, hogy “a gyermeknél fennáll-e a fertőzés gyanúja”.  Értesítsd a mentortanárt és a mikroiskola covid-felelősét arról, hogy az orvos mit javasolt.
* Amennyiben a gyereked, vagy a vele egy háztartásban élő bármely családtagja igazolt covid-fertőzötté, illetve kontaktkutatásba bevont személlyé válik, kövessétek a háziorvos előírásait, és kérünk, hogy azonnal értesítsd a mentortanárt és a  mikroiskolád covid-felelősét.
* Ha egy gyerekkel egy háztartásban élő (szülő, nagyszülő, testvér) _gyanítja_, hogy fertőzött, mert tünetei vannak (lásd fent), a akkor a gyerek ne jöjjön addig az iskolába, amíg
    * Nincs egy negatív tesztje
    * Vagy amíg nem telt el 2 nap tünet mentesen a másik embernél is.
* Az iskola megszervezi, és a covid-felelős nyomonköveti a közösen használt felületek napi takarítását és fertőtlenítését.
* Ha egy gyerek vagy tanár COVID miatt hiányzik, akkor a tanárok minden szülőnek küldenek egy értesítést arról, hogy valami felmerült.
* Az ételek tálalásánál a konyhai segítő maszkot és gumikesztyűt visel.

#### Mikroiskola online átállása

Amint egy gyerekről vagy a gyerekekkel közvetlen kapcsolatban dolgozó felnőttről igazolódik a fertőzés, úgy óvodában és alsóban 10 napra negyedik szintre kerül az iskola, azaz átmegy teljes online működésre. Minden iskola csak akkor zárhat be, ha a kormányzat így döntött, vagy ha volt igazolt (PCR, vagy gyorsteszt) fertőzött az iskolában, aki szoros kontaktban volt. 

Felsőben és középiskolában, mikroiskola dönthet úgy, hogy fertőzés esetén nem zár be 10 napra, csak 6 napra. Akkor, ha heti rendszeres tesztelés, maszkviselés  és 80% fölötti átoltottság van a közösségben és, amikor  az iskolában egy fertőzöttet jelentenek

A (z akkreditált telephellyel rendelkező) mikroiskola jelenti a fertőzést a területileg illetékes ántsz-nek.

Amennyiben adott mikroiskola előzőleg úgy dönt, a 6 vagy 10 napos zárás alól az iskola (nem a fertőzött személy!) fejenként 2 db, 48 órás különbséggel elvégzett negatív pcr-teszteredménnyel (mely közül az első legkorábban az 4. napon végezhető el) mentesülhet és kinyithat.

Amennyiben a területileg illetékes ántsz számítása és határozata alapján a lehetséges visszatérés a 10 naptól eltérő (pl. 9 nap), az adott mikroiskola dönthet úgy, hogy a határozatban szereplő dátumot tekinti irányadónak a nyitásnál.

Ha csak fertőzöttség gyanúja merült fel, akkor az iskolának nem kell teljes online működésre áttérni. 

Ha bármely iskolában van igazolt fertőzött, az iskola covid felelőse azonnal megosztja a tényt a BPS belső kommunikációs rendszerében a Covid csoportban, és jelzi, hogy mely sulik lehetnek még érintettek (testvérek, tanárok kapcsán).


### Harmadik szint: karanténcsoportokra bontás

Ezen a szinten már a mikroiskola gyerekeit karanténcsoportokra osztjuk, és ezekben a kisebb létszámú csoportokban találkozunk csak. Második szintről harmadik szintre akkor lép a mikroiskola, amikor ezt a mikroiskola covid-felelőse kimondja.

* Egyidőben csak az egy csoportba tartozó gyerekek tartózkodhatnak az iskolában, így bizonyos modulok foglalkozásait online kell megtartani.
    * Megoldási ötletek merültek fel: 3 nap online és 2 nap találkoznak a karanténcsoportok külön, vagy A és B hét szerint dolgozunk.
* Minden tanár (szaktanár, modulvezető, műhelytartó, mentortanár, zenebohóc) csak egy karanténcsoporttal találkozik személyesen.
* Két karanténcsoport között kitakarítjuk, szellőztetjük és fertőtlenítjük a megosztott tereket.

Ennek a szintnek az az előnye, hogy ha fertőzés igazolódik egy gyereknél, akkor csak az ő karanténcsoportja nem találkozik 10 napig.


### Negyedik szint: Nem járunk iskolába

Nem járnak iskolába a gyerekek, mert a kormányzat vagy a tanulásszervezők így döntöttek. Amennyiben más előírások vonatkoznak majd az eltérő életkorú gyerekekre, azokat fogjuk követni. Pl. ovis jöhet, sulis online lesz, ügyelettel.

Ilyenkor két lehetőségünk még van:



1. A karanténcsoportok dönthetnek a saját találkozásaikról, akár családoknál összejönni.
2. A tanárcsapat, vagy a szülők szervezhetnek szabadtéri programokat, kirándulásokat, táborokat.

## Általános kérések


Ebben a furcsa helyzetben természetes, hogy mindannyiunk igényei és határai máshol vannak. Azt is fogadd el, kérlek, ha valaki már egy köhintésnél nem akarja a gyerekét az iskolába vinni, és azt is, ha egy szülő tudja, hogy a gyerekének csak a szokásos allergiás köhögése, orrfolyása jött elő. Ha a tanárok úgy látják, hogy valakinek nem kellene az iskolában lennie, akkor mindenki fogadja el, hogy a tanulóközösség biztonságáért ők a felelősek, és a továbbiakban a döntésüknek megfelelően járjunk el.

Ne feledjétek: a konfliktusokra mi mindig fejlődési lehetőségként tekintünk. Fontos, hogy időt és teret találjunk arra, hogy meghalljuk és meghallgassuk egymás kéréseit, kérdéseit, javaslatait és fenntartásait, és kialakítsuk azt a szokás- és szabályrendszert, ami egy mikroiskola közössége számára az adott helyzetben elfogadható megoldást jelent.

Ebben az esetben különösen fontos, hogy időt és teret adjunk a vírus-helyzetből fakadó nézet- és határkülönbségek átbeszélésének. A végső döntést mindig a tanárcsapatok és, amikor úgy látja, akkor a covid-felelős egyedül hozza meg.
