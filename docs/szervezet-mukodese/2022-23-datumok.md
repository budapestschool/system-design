# A 2022/23-as tanév fontosabb dátumai

## Szünetek és tanítási napok

- szeptember 1. első nap az iskolában
- őszi szünet: október 28. - november 6. (első nap az iskolában: november 7. hétfő)
- téli szünet: december 22. - január 4. (első nap az iskolában: január 5. csütörtök)
- síszünet: február 17-26. (első nap az iskolában: február 27. hétfő)
- tavaszi szünet: április 6-11. (első nap az iskolában: április 12. szerda)
- utolsó tanítási nap: június 15.

Ezenkívül zárva lesz az iskola a következő napokon (ezek a napok, amikor a tanárcsapatok terveznek):

- szeptember 26. hétfő
- november 28. péntek (az őszi szünethez kapcsolva)
- december 9. péntek
- február 17. péntek (a síszünethez kapcsolva)
- március 20. hétfő

## A trimeszterek eleje és vége:

- első trimeszter: szeptember 1. - november 25.
- második trimeszter: november 28. - március 3.
- harmadik trimeszter: március 6. - május 26.
