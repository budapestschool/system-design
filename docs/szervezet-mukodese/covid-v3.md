---
meta:
  - name: "og:title"
    content: BPS ált iskola és gimnázium COVID protokoll V2
  - name: "og:description"
    content: Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.
  - name: description
    content: Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.
---

# BPS COVID protokoll

Több napja közösen dolgozunk az alábbi szabályok, a BPS covid-protokoll kialakításán. Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át a következőkben olvasható szabályokat, amelyek a Budapest School minden mikroközösségére vonatkoznak.

Három feladatunk van:

1. A vírus terjedésének nehezítése akkor, amikor senkiről nem tudjuk, hogy fertőzött-e.
2. A vírus továbbterjedésének megakadályozása, ha kiderül, hogy egy adott mikroiskola egy tagja megfertőződött.
3. A közösségi élmény, az együtt töltött idő maximalizálása úgy, hogy eközben mindenki biztonságban tudhassa a gyerekét,

A BPS mikroiskola struktúrája nagyban növeli a családok és a tanárok biztonságát, mert egy nagyságrenddel kevesebb emberrel kerül egy BPS-gyerek kapcsolatba, mint egy normál városi iskolában. Néhány alapvető szabállyal ezt a szociális távolságtartást tovább tudjuk növelni, miközben a mikroiskolák felkészülhetnek arra a szintre, amikor még kisebb karanténcsoportokban találkoznak a gyerekek, és elindul a hibrid oktatás, vagy akár arra is, ha újra át kell állnunk a teljes online oktatásra.

Az alábbi szabályok betartása annak feltétele, hogy szeptember 2-án gyereked megkezdje a tanulást a mikroiskolában, és tanárként elkezd a munkát. Minden gyerek megismeri, tanárok, és minden családból legalább egy szülő aláírja, hogy az alábbi szabályokat elfogadja, betartja.

## Szabályok

Négy készültségi szintet különböztetünk meg. Szeptemberben a második szinten kezdjük az iskolát.

### Első szint: minden olyan, mint a régi szép időkben, csak komolyan vesszük a vírusokat

- Mindenki könyökhajlatba köhög, tüsszent. Papírzsebkendő minden teremben mindenkinek mindig elérhető
- Folyékony szappant és papírtörölközőt, kézszárítót, vagy névre szóló textiltörölközőt biztosítunk a kézmosáshoz.
- A mikroiskolák bejáratánál vírusölő hatású kézfertőtlenítőt biztosítunk.

### Második szint: Általános védekezési fázis - még járunk iskolába, és a fentiek mellett további szabályokat hozunk a vírus terjedésének megakadályozására

- Minden mikroiskola tanulászervezői választanak egy covid-felelőst, aki első az egyenlők között, Az a dolga, hogy mindenről tudjon, és ha gyorsan kell, akkor döntsön. A covid-felelős elszámoltatható (accountable) azért, hogy

  - a mikroiskola saját szabályait kidolgozza, elfogadja, és a szülőkkel megossza;
  - megállapítsa és hangosan, tanárok, gyerekek, szülők és a LAB számára hallható módon megmondja, hogy milyen szinten van a mikroiskola, és azonnal jelezze, ha ebben változás történik;
  - szükség esetén krízismenedzserként döntéseket hozzon;
  - döntsön a harmadik szintre való lépésről;
  - elérhető legyen mindenki számára, ha kérdése, fenntartása van. Szülőként a covid-felelősnek jelezd, ha úgy gondolod, valamin változtatni kell.

- Lecsökkentjük a kontaktszemélyek számát (gyerekekek csak a saját mikroiskolájuk gyerekeivel és tanáraival találkoznak)
  - Nem szervezünk olyan programot, amin különböző mikroiskolák gyerekei egymással találkoznak
  - Minden mikroiskolába csak a mentortanárok, és az adott trimeszterre szerződőtt modulvezető szaktanárok, szerződött helyettesítők jöhetnek, azaz: nincs például vendégelőadó, egyszeri speciális foglalkozás.
- A szülők és a tanárok az iskolában nem kerülnek kontaktusba, nem találkoznak.
  - A szülői köröket, szülő-tanár konzultációkat, egyéb programokat online vagy szabadtéren tartjuk.
  - A szülők, és a gyerekekért érkező kísérők csak maszkban, egy meghatározott pontig (pl. Előtér) jöhetnek be az iskolába. Ezt a határt, és hogy náluk melyik térben egyszerre hány szülő tartózkodhat, a mikroiskolák határozzák meg.
- Minimalizáljuk a külső kapcsolatokat
  - Felfüggesztjük a külső helyszíni zárt terek látogatását (pl: könyvtár), ahol másokkal lehetnek egy időben a gyerekek. Ha van olyan külső tér, amit a tanulásszervező tanárok biztonságosnak tartanak, és csak a mikroiskola gyerekei vannak ott egy adott idősávban, akkor oda szervezhetnek programot.
  - A mikroiskolák döntenek arról, hogy használják-e a városi tömegközlekedést és a vasutat, vagy nem. Ha igen, akkor utazás közben mindenki maszkot fog viselni.
- Az iskolában akut betegségtüneteket nem mutató gyerekek és felnőttek tartózkodhatnak

  - Amennyiben a gyerekednél 48 órán belül a következő _rá nem jellemző_ tünetek legalább egyikét észlelted, ne gyertek iskolába.

    - köhögés
    - 37,5 C fok feletti testhőmérséklet
    - nehézlégzés, légszomj
    - hirtelen kezdetű szaglásvesztés, ízérzés hiánya vagy ízérzés zavara
    - izomfájdalom
    - Illetve a koronavírus nem gyakori tűnetei esetén is: izomfájdalom, orrdugulás, orrfolyás, torokfájás, hasmenés, légszomj.

  - Az aktuális kiadott [tájékoztató](https://koronavirus.gov.hu/cikkek/fokozott-szulok-es-tanarok-felelossege-jarvany-miatt-tunetes-gyereket-ne-engedjunk-kozossegbe) szerint “A legenyhébb tüneteinek jelentkezése esetén se engeddd közösségbe a gyermeked!”
  - Ha mi látunk az adott gyerekre nem jellemző tünetet a fentiek közül, felhívunk, és megkérünk, hogy haladéktalanul vidd haza.
  - Amikor gyereked a fenti tüneteket mutatja, telefonon hívd fel a háziorvost, vagy gyermekorvost, és ne vidd a rendelőbe a gyereked. Az orvos dönti el, hogy “a gyermeknél fennáll-e a fertőzés gyanúja,” Értesítsd a mentortanárt és a mikroiskola covid-felelősét arról, hogy az orvos mit javasolt.
  - Amennyiben a gyereked, vagy a vele egy háztartásban élő bármely családtagja igazolt covid-fertőzötté, illetve kontaktkutatásba bevont személlyé válik, kövessétek a háziorvos előírásait, és kérünk, hogy azonnal értesítsd a mentortanárt, a mikroiskolád covid-felelősét, és írj a [covid@budapestschool.org](mailto:covid@budapestschool.org) e-mail címre.

- Ha egy gyerekkel egy háztartásban élő (szülő, nagyszülő, testvér) _gyanítja_, hogy fertőzött, mert az elsődleges tűnetei (láz, száraz köhögés, nehézlégzés, légszomj, szaglásvesztés, ízérzés hiánya vagy ízérzés zavara vagy izomfájdalom) vannak, a akkor a gyerek ne jöjjön addig az iskolába, amíg

  - Nincs egy negatív tesztje
  - Vagy amíg nem telt el 2 nap tünet mentesen a másik embernél is.

- Az iskola megszervezi, és a covid-felelős nyomonköveti a közösen használt felületek napi takarítását és fertőtlenítését.
- Ha egy gyerek vagy tanár COVID miatt hiányzik, akkor a tanárok minden szülőnek küldenek egy értesítést arról, hogy valami felmerült.

- Az ételek tálalásánál a konyhai segítő maszkot és gumikesztyűt visel.

Amint egy gyerekről vagy a gyerekekkel közvetlen kapcsolatban dolgozó felnőttről igazolódik a fertőzés, úgy 10 napra negyedik szintre kerül az iskola, azaz átmegy teljes online működésre

- akkor ha a népegészségügyi központ ezt tartja helyénvalónak,
- vagy a mikroiskola tanárcsapata ezt tartja biztonságosnak, mert a fertőzött a többséggel szoros kontakban volt és valószínűsíthető a terjedés.

Ha csak gyanú merült fel, akkor az iskolának nem kell teljes online működésre áttérni. Ha a népegészségügyi központ nem rendel el távoktatást, akkor a tanárcsapat mérlegeli, hogy online lesz-e a teljes mikroiskola, vagy nem.

### Harmadik szint: karanténcsoportokra bontás

Ezen a szinten már a mikroiskola gyerekeit karanténcsoportokra osztjuk, és ezekben a kisebb létszámú csoportokban találkozunk csak. Második szintről harmadik szintre akkor lép a mikroiskola, amikor ezt a mikroiskola covid-felelőse kimondja.

- A mentortanárok szeptember 6-ig elkészítenek egy csoportbeosztást, és összeállítják a csoportok saját heti rendjét.
- Egyidőben csak az egy csoportba tartozó gyerekek tartózkodhatnak az iskolában, így bizonyos modulok foglalkozásait online kell megtartani.
  - Megoldási ötletek merültek fel: 3 nap online és 2 nap találkoznak a karanténcsoportok külön, vagy A és B hét szerint dolgozunk.
- Minden tanár (szaktanár, modulvezető, műhelytartó, mentortanár, zenebohóc) csak egy karanténcsoporttal találkozik személyesen.
- Két karanténcsoport között kitakarítjuk, szellőztetjük és fertőtlenítjük a megosztott tereket.

Ennek a szintnek az az előnye, hogy ha fertőzés igazolódik egy gyereknél, akkor csak az ő karanténcsoportja nem találkozik 10 napig.

### Negyedik szint: Nem járunk iskolába

Nem járnak iskolába a gyerekek, mert a kormányzat vagy a tanulásszervezők így döntöttek.Amennyiben más előírások vonatkoznak majd az eltérő életkorú gyerekekre, azokat fogjuk követni. Pl. ovis jöhet, sulis online lesz, ügyelettel.

Az ügyelettel kapcsolatos szabályainkat később dolgozzuk ki.

Ilyenkor két lehetőségünk még van:

1. A karanténcsoportok dönthetnek a saját találkozásaikról, akár családoknál összejönni.
2. A tanárcsapat, vagy a szülők szervezhetnek szabadtéri programokat, kirándulásokat, táborokat.

## Általános kérések

Ebben a furcsa helyzetben természetes, hogy mindannyiunk igényei és határai máshol vannak. Azt is fogadd el, kérlek, ha valaki már egy köhintésnél nem akarja a gyerekét az iskolába vinni, és azt is, ha egy szülő tudja, hogy a gyerekének csak a szokásos allergiás köhögése, orrfolyása jött elő. Ha a tanárok úgy látják, hogy valakinek nem kellene az iskolában lennie, akkor mindenki fogadja el, hogy a tanulóközösség biztonságáért ők a felelősek, és a továbbiakban a döntésüknek megfelelően járjunk el.

Ne feledjétek: a konfliktusokra mi mindig fejlődési lehetőségként tekintünk. Fontos, hogy időt és teret találjunk arra, hogy meghalljuk és meghallgassuk egymás kéréseit, kérdéseit, javaslatait és fenntartásait, és kialakítsuk azt a szokás- és szabályrendszert, ami egy mikroiskola közössége számára az adott helyzetben elfogadható megoldást jelent.

Ebben az esetben különösen fontos, hogy időt és teret adjunk a vírus-helyzetből fakadó nézet- és határkülönbségek átbeszélésének. A végső döntést mindig a tanárcsapatok és, amikor úgy látja, akkor a covid-felelős egyedül hozza meg.

Szerzők: Pintér Zsuzsa, Halácsy Péter, Szabady Helga

Utoljára átnézve: 2020.08.30

<!-- Footnotes themselves at the bottom. -->

### Források

- Az érvényben lévő népegészségügyi protokoll [klinikai tünetdefiníciója](https://www.nnk.gov.hu/attachments/article/567/2_sz_mell%C3%A9klet_Esetdefin%C3%ADci%C3%B3_es_vizsg%C3%A1land%C3%B3k_k%C3%B6re_2020.06.11.pdf)
- Kormány tájékoztatója: [_Fokozott a szülők és a tanárok felelőssége a járvány miatt – Tünetes gyereket ne engedjünk közösségbe!_](https://koronavirus.gov.hu/cikkek/fokozott-szulok-es-tanarok-felelossege-jarvany-miatt-tunetes-gyereket-ne-engedjunk-kozossegbe)
