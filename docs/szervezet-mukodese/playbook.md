# Playbook

Egy csapat akkor lesz csak igazán egészséges, ha egy irányba húz, ha van pár alap kérdés, amiben a tagok ugyanúgy gondolkoznak (angolul ezt hívják úgy, hogy "_alignment_"). Ennek kialakulását több módon segíthetjük. Egyik módszer, amit nagyon kedvelünk, hogy a csapat elkészíti a "playbookját". A playbook hat kérdést tartalmaz és a csapat válaszait ezekre a kérdésekre. Ez a módszer [Patrick Lencioni - The Advantage](https://www.goodreads.com/book/show/12975375-the-advantage) című könyvéből jön.

Egy jó 1-2 napos workshop tud lenni, hogy egyenként végigmegy a csapat a kérdéseken. Mindegyik kérdésre végigcsinálhatjuk a következő folyamatot:

1. Először mindenki csendben, pár perc alatt leírja a saját válaszát.
2. Körbemegyünk, mindenki megosztja, amit leírt és tisztázó kérdéseket teszünk fel. Ebben a körben még nem kezdjük közösen értelmezni, alakítani a válaszokat, csak a megértés a cél.
3. Álbeszéljük, amit hallottunk és próbálunk egy olyan választ találni, amivel az egész csapat egyet tud érteni. Nem kell tökéletes mondatokat találni, lesz még alkalom javítgatni.

Amikor ezt megcsináltuk mind a 6 kérdéssel, még egyszer végigmehetünk és megnézhetjük, hol lehetne pontosítani. Gyakran a későbbi kérdéseknél lefolytatott beszélgetések segítenek egy korábbi kérdést jobban megérteni.

## Why do we exist?

- The Purpose. It is idealistic. It answers the question: "How do we contribute to a better world?"
- It has to be true. It has to reflect the real motivation of the team. It isn’t for external marketing.
- It never changes. This is why the team exists, it can't change.
- Read [this article](https://hbr.org/2011/07/the-fundamental-purpose-of-you.html#:~:targetText=A%20clear%20and%20compelling%20purpose,seek%20in%20what%20they%20do.) about why purpose really matters for teams.

## How do we behave?

It never changes. There are 4 big categories:

![values](../pics/playbook/values.png)

### Core Values

These are the few—just two or three—behavioral traits that are inherent in an organization. Core values lie at the heart of the organization’s identity, do not change over time, and must already exist. In other words, they cannot be contrived. An organization knows that it has identified its core values correctly when it will allow itself to be punished for living those values and when it accepts the fact that employees will sometimes take those values too far. Core values are not a matter of convenience. They cannot be extracted from an organization any more than a human being’s conscience can be extracted from his or her person. As a result, they should be used to guide every aspect of an organization, from hiring and firing to strategy and performance management.

### Aspirational Values

These are the characteristics that an organization wants to have, wishes it already had, and believes it must develop in order to maximize its success in its current market environment. Aspirational values are the qualities that an organization is aspiring to adopt and will do its best to manage intentionally into the organization. However, they are neither natural nor inherent, which is why they must be purposefully inserted into the culture. But they should not be confused with core values, which, again, do not change over time and do not come and go with the needs of the business.

### Permission-to-Play Values

These values are the minimum behavioral standards that are required in an organization. Although they are extremely important, permissionto-play values don’t serve to clearly define or differentiate an organization from others. Values that commonly fit into this category include honesty, integrity, and respect for others. If those sound generic, something you’ve seen on virtually all of the values statements plastered on the walls of every mediocre company you’ve ever visited, then you understand the problem. Permission-to-play values must be delineated from the core to avoid dilution and genericism (I don’t think that’s a word, but you get the point).

### Accidental Values

These values are the traits that are evident in an organization but have come about unintentionally and don’t necessarily serve the good of the organization. In many companies, behavioral tendencies develop over time because of history, or because people start to hire employees who come from similar backgrounds. One day everyone looks around and realizes that just about every employee who works in the organization shares some quality: socioeconomic status, introversion, or good looks. The question that needs to be asked is whether being middle class, introverted, or good looking is something that the company has cultivated for a purpose, or whether it came about accidentally. It’s important that leaders guard against accidental values taking root because they can prevent new ideas and people from flourishing in an organization. Sometimes they even sabotage its success by shutting out new perspectives and even potential customers.

### Isolating the Core

The key to sifting core values from the others, especially aspirational and permission-to-play values, is to ask a few difficult questions. For instance, separating core from aspirational values can be done by asking the questions, Is this trait inherent and natural for us, and has it been apparent in the organization for a long time? Or, is it something that we have to work hard to cultivate? A core value will have been apparent for a long time and requires little intentional provocation.

Permission-to-play values are also often confused with core. The best way to differentiate them is to ask, Would our organization be able to credibly claim that we are more committed to this value than 99 percent of the companies in our industry? If so, then maybe it really is core. If not, then it’s probably a candidate for permission-to-play; it’s still important and should be used as a filter in hiring, but it’s not what sets the organization apart and uniquely defines it.

It’s worth restating that the reason organizations need to understand the various kinds of values is to prevent them from getting confused with and diluting the core. Core values are what matters most.

## What do we do?

- It only changes when the market changes. (Once every few years at most.)
- A power company: “We generate and deliver electrical and natural gas products and services to people throughout the state.”
- A credit card company: “We provide payment products and extend credit to consumers.”
- A technology hardware company: “We develop, manufacture, and market hard drives, solid-state drives, and storage subsystems for consumers, OEMs, and enterprises.”
- A biopharmaceutical company: “We discover, develop, make, and commercialize better medicines through integrated sciences.”
- A Catholic church: “We provide Sacraments, outreach services, counseling, and religious education for people in our parish.”

## How will we succeed?

[Download a nice pdf version of this section](https://www.tablegroup.com/imo/media/doc/AdvantageThematicGoal(8).pdf).

- “Strategy.” It helps making day-to-day decisions. For example if your strategy is to sell high quality produce and you can’t get any, you shouldn’t sell anything or sell the lower quality produce under a different brand.
- Process: first collect tons of post its about what we have to do, than find 3 higher level patterns: 3 strategic anchors.
- “We worked with an organization that runs charter schools. As is true in many mission-driven organizations, there is a real temptation in schools for leaders to want to be all things to all people. Of course, with limited resources and high stakes, the cost of not being strategic is great. The team started by creating an exhaustive list of everything that was currently true about the organization: focus on kindergarten through fifth grade, standardize core processes across all schools, headquarters in Texas, slightly lower staff pay than average public schools, emphasis on student safety, no transportation services provided, performance driven, data driven, no special education programs, emphasis on parent volunteerism and involvement, internal promotion of leaders, formative assessments, focus on benefit to kids, low cost, minimal branding and marketing, character-focused education, state-controlled pricing, distributed leadership model, local principal autonomy, no frills, employees passionate about mission. After an hour of brainstorming and passionate debate, they arrived at the following strategic anchors: standardization of operations, selective marketing, performance and measurement driven. They decided that the way to ensure their success and differentiate from their competition was to ensure that every decision they made reflected (1) the ability to leverage standardized processes for efficiency and low cost, (2) to do only cost-effective, targeted marketing to parents in the micromarkets they served, and (3) to focus relentlessly on student achievement and parents’ return on investment. Those anchors also gave them the clarity about what they shouldn’t do like provide transportation services and special education. As unhappy as they initially felt about those decisions, the leaders of the company knew that their ability to succeed in a competitive world meant they had to make difficult, strategic trade-offs.”

## What is most important right now?

A **thematic goal** provides a rallying cry for an organization–a clear direction for the entire organization for a fixed period of time determined by answering the question, *“What is most important right now?”*

The temporary, qualitative components that serve to clarify exactly what is meant by the thematic goal; shared by all members of the team (and usually varying in number from four to six). **Defining objectives** provide a level of specificity so that the thematic goal isn’t merely a slogan but rather a specific and understandable call to action.

**Standard Operating Objectives**: These are the ongoing and relatively straightforward metrics and areas of responsibility that any leadership team must maintain to keep the organization afloat. These objectives do not go away from period to period and often include topics such as revenue, expenses, customer satisfaction, quality, etc.

### Thematic Goal exercise instruction

1. Ask every member of the team to individually answer this question: “If we accomplish one thing during the next x months, what would it be?” Share and debate answers. The goal should be to rally the troops, but more than anything else, it should give the leadership team clarity where to spend its time, energy and resources. Leadership team members may need to temporarily abandon their departmental objectives for the good of the organization as a whole. To help determine the thematic goal, consider these facilitation suggestions:
   1. Have every team member write down their answer.
   2. Estimate time frame (between 6-9 months).
   3. Consider this supplemental statement: “If we do not accomplish ____________, we have failed.”
   4. If there is push-back over the need for only one goal, recite the adage, “If everything is important than nothing is.”
2. Once everyone has committed to answer, white board the team’s answers. Facilitation tips:
   1. Encourage everyone not to hold back; ask the leader to go last.
   2. Write down all goals and any needed clarifying statements.
3. Discuss the list and determine if some of the answers might actually be a standard operating objective. To determine this distinction, consider the following ideas:
   1. Ask the questions: “Is this something that is always important?” “When are we not worried about that?” “How is that different from last period, or next period or next year?”
   2. Remind the team that a thematic goal is only for a specific period of time, and then it goes away. If something is always important, it is more likely than not a standard operating objective.
4.  Ask the team to review the newly sorted list to identify which goal rises to the top as the most important.
5. If there are discrepancies, ask team members to take 60 seconds to convince the team why their suggested goal is most important. Consider all suggestions.
6. Team must then put a stake in the ground to select the thematic goal.
7. Review the list of suggestions; many of these activities/concepts will likely populate the defining objectives and standard operating objectives.
8. Continue the discussion until you have 4-6 defining objectives (activities/components that define the goal) and outline several of your team’s standard operating objectives (areas of focus that don’t change from period to period).

### A story

 “We were working with the leadership team of a large freight and logistics company that was doing very well. However, among the various challenges that the leaders were discussing was a problem with having no excess capacity to take on the growing volume of business that was coming their way. After discussing this and various other topics competing for their attention, we posed the big question: If you accomplish just one thing in the next nine months, what should it be? Within just a few minutes, the team agreed that “if we don’t solve the capacity issue, we’re in huge trouble.” So “solve the capacity problem” became their thematic goal. Not sexy, but clear and correct. And just as important, there was no ambiguity that this thematic goal would become the top priority of every team member, regardless of their specific functional responsibilities. The next step was to define exactly what they would need to do to address the problem and achieve the thematic goal. After less than an hour of discussion and debate, they came up with the following defining objectives: (1) Accurately assess and forecast the capacity shortfall (2) Hire and train more employees (3) Acquire additional equipment (4) Expand and modify current facilities (5) Better utilize systems and software. As obvious as this may seem in retrospect, had the executive team not had this conversation, they would have gone back to work and continued working on whatever projects and responsibilities they usually attended to, treating the capacity problem as just another of a long list of important objectives. Instead, they ended the conversation ready to talk about what they should stop working on, and how they would reallocate less-critical resources in the organization to accomplish the thematic goal.”

#### Application

The thematic goal should now be the focus of the team’s Weekly Staff Meetings. Teams should grade their progress across all areas, using the green-yellow-red stoplight scale. Those categories with a yellow or red grade deserve the team’s focus first. Once the goal is near completion, a new singular, time-bound, thematic goal should be established.

## Who must do what?

Break down the goals into action items owned by team members.
