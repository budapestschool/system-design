# 360 fokos visszajelzés (peer feedback)

Egy tanuló szervezetben fontos néha megállni és ránézni, hogy hol tartunk most és merre mehetünk tovább. Ebben segít az, ha rendszeresen visszajelzést adunk egymásnak. Ezért évente háromszor szervezünk teljes 360 fokos visszajelzést a csapatoknak (november-december, február-március és május) -- ebből az elsőben és utolsóban kötelező minden iskolának részt vennie, a középső opcionális.

## A folyamat

Mindenki adhat mindenkinek visszajelzést a csapatában. Magunknak is érdemes "visszajelzést adni" (a saját kérdőívünket kitölteni), így mi is végiggondoljuk adott területeken hol tartunk .

Néha nehéz egyedül feldolgozni a visszajelzést, mert heves érzelmeket válthat ki mikor kollégáink érzékeny területen olyan dolgot látnak rólunk, ami meglepő számunkra. Ezért minden esetben egy ember tolmácsolja nekünk a visszajelzést, mielőtt megkapnánk a nyers, írott visszajelzéseket. Ezt az embert _interpretálónak_ hívjuk. Ez lehet a csapatunk vezetője, a mentorunk, akárki, aki úgy gondoljuk, hogy ismer minket eléggé és jól ad visszajelzést.

1. Minden tanár kiválasztja, hogy ki interpretálja a visszajelzését.
2. Mindenki kap egy emailt, amiben az összes csapattársához tartozó visszajelző kérdőívet megkapja. Nagyjából két hete van ezeket kitölteni.
3. A beérkezett visszajelzéseket elküldjük az adott ember visszajelzését interpretáló kollégának. Ő átolvassa, esetleg tisztázó kérdéseket tesz fel a csapattagoknak, hogy jobban értse a szituációt. Érdemes jegyzetet készíteni a visszatérő mintákról, az erős üzenetekről, konkrét idézetekkel.
4. Leül az interpretáló és a visszajelzést kapó ember és átbeszélik a beérkezett kérdőíveket. A beszélgetés során egyéni célokról is beszélnek. A találkozó után az interpretáló megosztja az adott emberrel a visszajelzéseket, amit kapott.

A visszajelzés nem névtelen. Minden visszajelzésnél látszik majd a kolléga neve, aki a visszajelzést adta.

Ha valaki nem tud egy csapattársának visszajelzést adni, mert nem lát eleget a munkájából, akkor nem ad visszajelzést. Ha keveset tud, akkor keveset ad. Ha valaki szeretne olyan embernek is visszajelzést adni, aki nincs a csapatában, erre is van lehetőség. A visszajelzés ajándék, ami segít a fejlődésben.

## Tippek

- Többség imád visszajelzést kapni, de adni túl sok időnek tűnik. Sosem érezzük, hogy lenne rá időnk, mert mindig van valami ami sürgősebb. Éppen ezért előre kihirdetjük a visszajelzések időpontját minden évben. Érdemes a naptárban időt elkülőníteni arra, hogy megírjuk a visszajelzéseket. Egy módszer, ami segíthet, ha először mindenkin "végigrohanunk" és csak rövid pointokban leírjuk az első pár gondolatot, ami eszünkbe jut. Utána pár nap múlva visszamegyünk, átnézzük a piszkozatot és kiegészítjük. Aztán megint pár nap múlva letisztázzuk és elküldjük. Így egyben nem sok idő, az agyunk a háttérben dolgozik rajta és ha valami 3 alkalommal sem jut eszünkbe, akkor valószínűleg nem fontos. Ha valakihez kevés visszajelzés jut eszünkbe, az önmagában egy jel (például az illető keveset oszt meg arról, hogy mit csinál), nem kell rosszul éreznünk magunkat miatta, a kevés visszajelzés is jobb, mint a semmi.
- Teljesen rendben van, ha valaki nem jó valamiben. Néha ez tudatos döntés. A fókuszálás jó dolog, nem kell mindenkinek mindenben jónak lennie, ha diverz csapat vagyunk. Mindenki tud fejlődni, ha akar, de nem mindig kell akarni. Ezért nem kell rosszul éreznünk magunkat, ha valakit ma egy területen gyengének látunk.
- Ha belefér az időbe, sokat szokott segíteni, ha személyesen is tudjuk a feedbacket adni. Írásban könnyű félreérteni dolgokat és nehezebb visszakérdezni. Érdemes arra figyelni, hogy a mádik olyan állapotban van-e, aki szeretne visszajelzést kapni. Néha "visszavonulunk" és nem szeretnénk kimozdulni a komfortzónánkból, ezt jó tiszteletben tartani.
- Konkrétan érdemes fogalmazni, a hatásról beszélni és nem csak címkézni:
  - rossz: _"X nagyon szeretetteljes a gyerekekkel"_
  - jó: _"Amikor Y felpörög, X felemeli és megöleli. Ettől Y lenyugszik. Az év első pár hetében nagy probléma volt, hogy Y-tól nem tudtunk a délutáni foglalkozásokon haladni, mert elvitte a figyelmet. Mióta X rájött, hogy az ölelés mennyire működik, sokkal könyebb a kereteket tartani ezeken a foglalkozásokon."_
- Visszajelzést adni az igazán nagy hatású dolgokról érdemes. Apróságok csökkentik a visszajelzés súlyát:
  - rossz: _"imádtam, hogy X megköszönte, hogy vegán ebédet vettem"_
  - jó: _"X figyel a csapattársaira. Rendszeresen ad visszajelzést amiből kiderül, hogy figyel az érzelmeimre és segít fejlődni. Például imádom, hogy bevezette, hogy a heti meetingünket kezdjük azzal, hogy mindenki ad egy pozitív visszajelzést az összes többi csapattagnak. Ettől úgy érzem, hogy jelentősen közelebb kerültünk és könyebb kapcsolódnom a többiekhez."_
- Segíthet, ha azon gondolkozunk, hogy mi olyat csinál az illető, amit mi még nem tudunk, de szeretnénk megtanulni tőle:
  - rossz: _"X remek vega ebéd rendelésében"_
  - jó: _"Szeptember vége felé Y anyukája írt nekünk egy dühös emailt, amiben azt mondta, hogy nem figyelünk egyáltalán Y érzelmeire. Amikor olvastam, elveszettnek éreztem magam. 10 perccel később X-től kaptunk egy google docot, amiben leírta, hogy ő mit válaszolna. A levél empatikus volt, leírta konkrétan, hogy miket szoktunk csinálni, mit tervezünk még javítani és kérte a szülőt, hogy beszélgessünk erről személyesen, hogy neki milyen egyéb ötletei lennének. Megkérdezte a véleményünket róla, még hozzáírtunk 2-3 dolgot és még aznap este ki tudtuk küldeni a levelet. Másnap telefonon is felhívta a szülőt majd 1 hónap múlva megint rákérdezett, hogy minden OK-e. Az anyuka azóta az egyik lagnagyobb rajongónk, a szülőin például ő védett meg minket, amikor Z szóba került."_
- Hasznos arról beszélni, hogy hogyan hat ránk valami, amit az illető csinál. Például, hogy milyen érzelmeket vált ki belőlünk.
  - rossz: _"X teljesen megbízhatatlan"_
  - jó: _"Félek, hogy túl lassan haladunk és elveszítjük a szülők bizalmát, ha nem csináljuk meg időre amit bevállalunk. A múltkori szülői után X bevállalt 3 tennivalót, aztán azóta sem hallottunk róla. Ki kéne találnunk, hogy mikor tudunk ezekről a tennivalókról beszélgetni és hogyan tudnánk jobban segíteni egymást, ha rosszul haladunk."_
- Segíthet, ha azon gondolkozunk, hogy mi az, amiben már most is jó az illető és ha még többet csinálná, segíthetne a csapatnak: _"Sokat segített, hogy kitalálta, hogy mi az ölelés a kulcs X-hez. Azt érzem, hogy kimondott erőssége, hogy ki tudja találni mi segít egy-egy gyereknek. Ma talán Y okozza a legnagyobb fejfájást a csapatnak, sokat segítene, ha több időt töltene vele is, hátha rájön, hogy mire van szüksége."_
- Segíthet, ha azon gondolkozunk, hogy mi lehet az illető számára a következő lépés a karrierjében: _"A csapatunkat már nagyon magabiztosan vezeti. Talán megpróbálhatna több csapatnak is segíteni."_
- Magunknak visszajelzést adni jó dolog. Segít, ha néha leülünk és végiggondoljuk, hogy mit értünk el az előző pár hónapban és mit szeretnénk máshogy csinálni a jövőben. Érdekes trendeket lehet látni. Például, ha azt látjuk, hogy kevés dolgot fejeztünk be 3 hónap alatt, de 10 dolog folyamatban van, akkor érdemes lehet arra fókuszálni, hogy kevesebb folyamat fusson párhuzamosan és csak akkor kezdjünk új feladatba, ha az előzőt lezártuk. Érdekes visszanézni a korábbi visszajelzéseinket, hogy lássuk, mennyire válzotik és mi az, ami kb állandó.
