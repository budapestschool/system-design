# Csapat szemle (team deep dive)

A Budapest School tanulóközösségek hálózata. Mindegyik csapat nagy szabadságot kap, egyedül fejlődik. Emellett fontos, hogy felismerjünk rendszer-szintű mintákat és valamilyen minőségbiztosítással garantáljuk, hogy egyik iskola se okozzon túl nagy rizikót a teljes hálózat számára. Egyik eszközünk, hogy trimeszterenként legalább egyszer mindegyik iskolára közösen "ránézünk".

## A folyamat

Egy-egy csapatra külön-külön megbeszélésen nézünk rá.

A megbeszélés előtt:

- A megbeszélés előtti két hétben a csapat közösen végigcsinál egy team health check workshopot, ahol közösen ránéznek, hogy főbb területeken hogyan haladnak, mennyire elégedettek a jelenlegi helyzetükre. Erre a csapatnak szabaddá kell tennie 2 órát.
- Az adott hét előtti két hétben a marketing csapatunk frissíti az iskola weboldalát, ha ők elküldik az adatokat (új képek, bemutatkozók tanárokról, elérhetősegek).
- Mindenki átnézi a csapatról elérhető adatokat.

A csapat szemle megbeszélés menete:

- Az adatokon közösen végigszaladunk (gyerekek, tanárok, felvételi folyamatok, pénzügyi helyzet, stb), tisztázó kérdéseket teszünk fel.
- Végigmegyünk a team health check eredményein és tisztázó kérdéseket teszünk fel.
- Ránézünk a Budapest School model fontosabb elemeire: mentorok, hármas szerződések (célok, vizsgák), modulok (órarend, mindennapok), piros/sárga folyamatok
- Szabadon beszélgetünk arról, hogy most hol tart a csapat, mik a fő lehetőségek. Milyen mintákat látunk. A megbeszélés végére leírunk egy [Thematic Goalt](peer-feedback.md) a csapatnak. Azt is leírjuk, hogy ki, mit vállal ezért a célért.
- A csapat elmondhatja, hogy a Labtol mit szeretne kérni és beszélgetünk róla.
