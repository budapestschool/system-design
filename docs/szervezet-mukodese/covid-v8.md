# BPS COVID protokoll

Már két éve közösen dolgozunk az alábbi szabályok, a BPS covid-protokoll alakításán. Iskolaként, közösségként, egymás iránt felelősséget vállalva gondoltuk át és módosítjuk időről-időre a szabályainkat. 

A BPS mikroiskola struktúrája nagyban növeli a családok és a tanárok biztonságát, mert egy nagyságrenddel kevesebb emberrel kerül egy BPS-gyerek kapcsolatba, mint egy normál városi iskolában. Egy olyan mátrixot készítettünk, amely elemeinek felhasználásával minden mikroiskola elkészíti a saját protokolját, a tanárok, a gyerekek és a szülők által megkívánt biztonsági szinten.

A következő paraméterek mentén minden mikroiskola kialakítja január 6-ig a saját közösségére vonatkozó biztonsági szabályokat. 

A betűkkel jelölt sorok közül minden mikroiskolának ki kell választania, hogy mi az ő protokollja.

## Mikor zár be az iskola?

Amikor egy gyerekről vagy tanárról kiderül, hogy fertőzött (PCR vagy pozitív antigén teszt), akkor



* (A) bezár az iskola 5 napra - online oktatás 5 napig. A visszatérés feltétele
    * (a) visszatérés fejenként 1 db, az 5. napon elvégzett negatív antigén teszttel - és tünetmentesen. Pozitív teszt esetén a fertőzött a pozitív teszt dátumától 7 napra otthon marad, ha a pozitív teszttől számított 4. napon láztalan és tünetmentes, 5. napi negatív antigén teszttel érkezhet vissza a 6. napon.
    * (b) visszatérés fejenként 2 db, 48 óra különbséggel elvégzett negatív antigén teszttel (mely közül az első legkorábban a 3. napon végezhető el) - és tünetmentesen
    
* (B) nyitva marad az iskola (a fertőzöttek otthon maradnak), amíg a fertőzöttek száma nem haladja meg a teljes iskolai közösség (gyerekek+mentortanárok és 10 óránál többet modultartó tanárok) - közösség létszámának 7%-át (ha meghaladja, akkor a tömeges fertőzés napjától az iskola 5 napra online tanulásra áll át)
    * (a) az iskola nyitva marad, míg a fertőzöttek aránya nem éri el a 7%-ot – kötelező a maszkviselés (ffp2 vagy orvosi maszk - már az első fertőzés megjelenése előtt) és heti gyorstesztelés legalább az első fertőzéstől kezdődően.
    * (b) az iskola nyitva marad, míg a fertőzöttek aránya nem éri el a 7%-ot – heti gyorstesztelés szükséges legalább az első fertőzéstől kezdődően.

* (C) mivel a mikroiskolában _izolált kiscsoportok_ vannak, ezért csak a fertőzőtt gyerek izolált kiscsoportja zár 5 napra.

    Izolált kiscsoport azt jelenti, hogy egy gyerek csak a saját csoportjával találkozik napi 20 percnél tovább (és akkor is csupán maszkviseléssel). A többi gyerekkel csak maszkkal találkozik. 

* (D) a fertőzöttek otthon maradnak, nincs maszkviselés és tesztelés - és az iskola nyitva marad.

### Szelektív jelenlét

Ha adott iskolában fertőzés miatti zárás történik és a közösségben Covid fertőzésen 3 hónapon belül _igazoltan_ átesett gyermekek és tanárok vannak,  


* (A) akkor számukra a mikroiskola választhatóan személyes jelenlétű ügyeletet/ oktatást biztosíthat az online működés idejére. (Amennyiben nincs ezzel ellentétes tartalmú ÁNTSZ határozat az érintett személyekre vonatkozóan.
* (B) akkor is bezár a mikroiskola mindenkinek. 

__Igazolt átesettség:__ az, hogy mit tekintünk igazolt átesettségnek (EESZT igazolás, otthoni gyorsteszt, etc - a mikroiskola saját döntése)



## Fertőzöttség, jelzés egymás felé

### Ha fertőzött a gyereked

Pozitív antigén vagy PCR teszt esetén a fertőzött a pozitív teszt dátumától 7 napra otthon marad, amennyiben a pozitív teszttől számított 4. napon láztalan és tünetmentes, 5. napi negatív antigén teszttel a 6. napon érkezhet vissza. 

### Fertőzés tényének megosztása 
Ha bármely iskolában van igazolt fertőzött, az iskola covid felelőse azonnal megosztja a tényt a BPS belső kommunikációs rendszerében a Covid csoportban, és jelzi, hogy mely sulik lehetnek még érintettek (testvérek, tanárok kapcsán). Adott iskolában megjelenő fertőzés (vagy kontakt-kapcsolat) esetén a covid felelős – anonim módon vagy a szülő beleegyezésével névvel  – megosztja a tényt a szülői közösséggel.

   
### Maradjon otthon a gyereked, ha
* ha bármilyen tünete van
* ha fertőzött (tünetmentes fertőzött esetében is - a pozitív teszt dátumától 7 napra otthon marad, amennyiben a pozitív teszttől számított 4. napon láztalan és tünetmentes, 5. napi negatív antigén teszttel a 6. napon érkezhet vissza.
* ha a szűk családjában (életközösség, háztartás) fertőzött van (a fertőzött karanténjának 7 (ha rövidít, akkor 5) napja után + 5 nap és neg antigén teszt után jöhet)
* Oltott/3 hónapon belül a fertőzésen igazoltan átesett gyerek: ha szoros kontakt a gyereked (és nem egy háztartásban van a fertőzöttel) és nem kerül ántsz karanténba, választhatsz: a 3. napon elvégzett negatív antigén teszt után bejöhet további 2 nap ffp2 maszkhasználattal - ha tünetmentes
* Oltatlan gyerek: ha szoros kontakt a gyereked (és nem az egy háztartásban van a fertőzöttel) és nem kerül ántsz karanténba, választhatsz: otthon marad 7 napig VAGY az 5. napon elvégzett antigén teszttel a 6. napon bejöhet - ha tünetmentes

### Elvárt jelzések egymás és a közösség felé
Kérlek jelezt a mikroiskola covid-felelősnek és a gyereked mentortanárának, ha 

* (A) ha a gyerek fertőzött
* (B) ha az életközösségben élő bármely családtag fertőzött vagy karanténba kerül vagy szoros kontakt
* (C) ha releváns helyen (edzés, családi összejövetel, különóra) maszk nélkül fertőzöttel került kapcsolatba a gyerek
* (D) ha a gyereknek, tanárnak bármilyen tünete van, azonnal antigén tesztet elvégzését várjuk el. 

## Általános szabályok

A protokoll betartásáért továbbra is az iskolák _covid-felelősei_  a felelősek. A teljes szervezet szintjén is megjelenik ezentúl a "BPS covid-felelős" szerepe.

Satellite tanároknak, egyéb bejáró felnőtteknek (pl ételhozó) állandó maszkviselés kötelező. 

Az össz-bps tanárcsapat oltottsági státuszáról HP tájékoztatást kap mikroiskolánként január 20-ig (tanárok hány %-a oltott a maximális, adható számú oltással, ki az, akit felmentett a tanárcsapat az oltás alól, okkal). 

A teljes BPS tanárcsapat össz-oltottsági státuszát %-os arányban minden mikroiskola megosztja a szülői közösségével. 

###  Higiénia
* mindenki könyökhajlatba köhög, tüsszent. Papírzsebkendő minden teremben mindenkinek mindig elérhető;
* folyékony szappant és papírtörölközőt, kézszárítót, vagy névre szóló textiltörölközőt biztosítunk a kézmosáshoz;
* a mikroiskolák bejáratánál vírusölő hatású kézfertőtlenítőt biztosítunk
* napi fertőtlenítő takarítás 
* Szellőztetés legalább 2 óránként

### Betegség-tünetek
Az iskolában csak akut betegségtüneteket nem mutató gyerekek és felnőttek tartózkodhatnak. Ha mi látunk az adott gyerekre nem jellemző tünetet, felhívunk, és megkérünk, hogy haladéktalanul vidd haza.

A szülők kötelesek az iskola covid-felelősét és a gyerek mentortanárát azonnal tájékoztatni, ha a gyermek környezetében (családban, edzésen, különórán) koronavírus-fertőzött személy van. 

Az iskolát nem látogathatja tanár és diák, amennyiben bármilyen betegségtünete vagy kifejezetten Covid vírusfertőzés tünete jelenik meg nála. 


## Tanárok, dolgozók oltottsága

A BPS-ben az oltás a norma, közös célunk az iskola védelme. Minden tanárnak javasoljuk a 3 oltást. A covid-felelősök 2022. Január 20-ig megírják a szülőknek, a 
a teljes BPS-ben a tanárok oltottságának %-át.

A mentortanár-csapat és a menedzser egyetértésével elfogadják a csapatok, ha egyes tagjaik nem rendelkeznek oltással. 

### Tanárok tesztelése (mikroiskola döntés): 
  * (A) Oltással nem rendelkező tanárok hetente rendszeresen tesztelnek.
  * (B) Teljes tanárcsapat hetente rendszeresen teszteli magát.
  * (C) Nincs heti tesztelés, csak tünetek esetén tesztelnek a tanárok.

A covid felelősök az összesített oltottsági adatokat HP-nak elküldik, aki a teljes szervezet számára láthatóvá teszi.

## Készültségi szintek

### Első szint: minden olyan, mint a régi szép időkben, csak tudatosabban készülünk lehetséges pandémiára

Minden iskolában van covid-felelős. A covid-felelős elszámoltatható (accountable) azért, hogy

* a mikroiskola saját szabályait kidolgozza, elfogadja, és a szülőkkel megossza;
* megállapítsa és hangosan, tanárok, gyerekek, szülők és a LAB számára hallható módon megmondja, hogy milyen szinten van a mikroiskola, és azonnal jelezze, ha ebben változás történik;
* szükség esetén krízismenedzserként döntéseket hozzon;
* elérhető legyen mindenki számára, ha kérdése, fenntartása van. Szülőként a covid-felelősnek jelezd, ha úgy gondolod, valamin változtatni kell.

### Második szint: Általános védekezési fázis - még járunk iskolába, és további szabályokat hozunk a vírus terjedésének megakadályozására

Lecsökkentjük a kontaktszemélyek számát (gyerekekek csak a saját mikroiskolájuk gyerekeivel és tanáraival találkoznak)
    * Nem szervezünk olyan programot, amin különböző mikroiskolák gyerekei egymással találkoznak
    * Minden mikroiskolába csak a mentortanárok, és az adott trimeszterre szerződött modulvezető szaktanárok, szerződött helyettesítők jöhetnek, valamint más BPS iskola oltott tanára maszkban hospitálhat. (Azaz: nincs például vendégelőadó, egyszeri speciális foglalkozás.)
    * Bármely  iskola oltott mentortanára mehet hospitálni másik BPS iskolába.

A szülők és a tanárok az iskolában nem kerülnek kontaktusba, nem találkoznak.
    * A szülői köröket, szülő-tanár konzultációkat, egyéb programokat online vagy szabadtéren tartjuk.
    * A szülők, és a gyerekekért érkező kísérők csak maszkban, egy meghatározott pontig (pl. előtér) jöhetnek be az iskolába. Ezt a határt, és hogy náluk melyik térben egyszerre hány szülő tartózkodhat, a mikroiskolák határozzák meg.

Minimalizáljuk a külső kapcsolatokat
    * A külső helyszíni zárt terek látogatása (pl: könyvtár) csak maszkban (lehetőleg ffp2) elfogadható . A mikroiskolák döntenek arról, hogy használják-e a városi tömegközlekedést és a vasutat, vagy nem. Ha igen, akkor utazás közben mindenki maszkot fog viselni.

### Harmadik szint: Nem járunk iskolába — mert a kormány így döntött, vagy mert fertőzés jelent meg az iskolában
