# A 2024/25-ös tanév fontosabb dátumai

## Szünetek és tanítási napok

- szeptember 2. első nap az iskolában
- őszi szünet: október 26. - november 3. (első nap az iskolában: november 4. hétfő)
- téli szünet: december 21. - január 5. (első nap az iskolában: január 6. hétfő)
- síszünet: február 8-12. (első nap az iskolában: február 13. szerda)
- tavaszi szünet: április 18-27. (első nap az iskolában: április 28. hétfő)
- utolsó tanítási nap: június 19.


Ezenkívül zárva lesz az iskola a következő napokon (ezek a napok, amikor a tanárcsapatok terveznek):

- október 4. (választható a tanárcsapat számára)
- október 7. (választható a tanárcsapat számára)
- december 20. (a téli szünethez kapcsolva - választható a tanárcsapat számára)
- január 6. (a téli szünethez kapcsolva - választható a tanárcsapat számára)
- április 17. (a tavaszi szünethez kapcsolva)
- június 20.


## A trimeszterek eleje és vége:

- első trimeszter: szeptember 2. - november 29.
- második trimeszter: december 2. - március 7.
- harmadik trimeszter: március 10. - június 6.
