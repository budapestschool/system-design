---
meta:
  - name: "og:title"
    content: Budapest School akkreditációjával kapcsolatos dokumentumok
  - name: "og:description"
    content: A BPS Modell a magyar állam két minisztériuma, és a nemzetközi Council of Internation School szervezet is elfogadta és elismerte 2020-ban.
  - name: description
    content: A BPS Modell elfogadottságát bizonyító dokumentumok
---

# Magyar és nemzetközi akkreditáció

## A Budapest School Általános Iskola és Gimnázium

Az Emberi Erőforrások Minisztériuma (EMMI) 2020-ban engedélyezte, hogy a Budapest School Általános Iskola és Gimnázium a BPS modell alapján készült egyedi pedagógia programjáben szereplő egyedi megoldásokat alkalmazza, majd 2021-ben engedélyezett további eltéréseket is.

- Az [egyedi megoldások alkamazását engedélyező határozat](/emmi-bps-elfodas.pdf), illetve annak 2021-es [kiegészítése](/emmi-bps-elfogadas-2021.pdf).
- A [kérelem](miben-terunk-el.md), ami összefoglalja, hogy miben egyedi a programunk.
- [Összefoglaló](miben-nem-terunk-el.md), arról, hogy miben _nem_ tér el az iskola a jogszabályok által feltételezett működéstől.
- Annak kifejtése, hogy [miért](miert-terunk-el.md) szükséges egyedi megoldásokat alkalmaznunk a programunkban.
- Az eredetileg leadott egyedi megoldás [kérelemhez csatolt egyedi pedagógia program](/egyedi-program-altgim-jovahagyott.pdf) (azóta frissült a program).
- A [legfrisebb pedagógia program és szmsz egyben](/egyedi-program-altgim.pdf)
## BPS Code Technikum

A szakképzésért felelős miniszter, Palkovics László 2020. május 5-n engedélyezte, hogy a BPS Modell alapján egy szoftverfejlesztésre fókuszáló tanulóközösséget indítson a Budapest School.

- [Engedély](/bps-code-technikum-kiserleti-program-jovahagyas.pdf)
- A [BPS Code Szakmai program](/bps-code-jovahagyott.pdf)
