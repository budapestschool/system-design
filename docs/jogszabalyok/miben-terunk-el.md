# Eltérések részletes bemutatása

Alábbiakban részletes bemutatjuk, hogy a 2011. évi CXC. törvény 9. § (8) értelmében mennyiben és miben tér el a Budapest School programja a Nemzeti alaptantervben (NAT) és az oktatásért felelős miniszter által közzétett kerettantervekben leírtaktól és más jogszabályokban, szabályozókban foglaltaktól.

## Nevelés-oktatás tartalmi kérdései

Az iskola programja a Nemzeti alaptanterv tantárgyait veszi alapul, tehát
nem tér el a Nat-ban és az oktatásért felelős miniszter által kiadott kerettantervben foglalt _tantárgyi struktúrától_.

Az is belátható, hogy a helyi tanterv a Nemzeti alaptanterv és az oktatásért felelős miniszter által közzétett kerettantervekben meghatározott _ismeretanyagot teljes mértékben tartalmazza_, hiszen azok tanulási eredményeit száz százalékban átvette.

Óraszámokban nincs eltérés a helyi tanterv és a Nat óraszám ajánlásai között, kivéve hogy

1. 1-4. évfolyamon megjelenik a kötelező _Mentoridő_ heti egy órában;
2. 5-12. évfolyamon a _Közösségi nevelés (osztályfőnöki)_ helyébe heti egy órában a mentoridő lép;
3. 9-10. évfolyamon a biológia, fizika, kémia, földrajz tantárgyak helyett egy összevont, természettudományi tantárgyat tanít az iskola heti 5-5 órában.
4. 9. és 10. évfolyamon 3-3 óra projekt óra jelenik meg.

Minden gyerek egy órát hetente a mentorával tölt, amikor a mentor a gyerek számára egyéni, minőségi figyelmet ad. A mentor ilyenkor a gyereket segíti a saját céljainak
megfogalmazásában és hogy legyen lehetősége reflektálni a saját fejlődésére. Ez az óra a gyerekek és a mentorok számára kötelező foglalkozás.

Ez azt is eredményezi, hogy az iskola átlagban magasabb tanár/gyerek aránnyal tudja csak elvégezni az egyedi pedagógia programban előírtakat mert nem csak tanítókra és szaktanárokra van szüksége, hanem mentorokra is.

### Komplex természettudomány óra

A BPS iskolában a 9-10. évfolyamon a biológia, fizika, kémia, földrajz tantárgyak helyett egy összevont, természettudományi tantárgyat tanít az iskola heti 5-5 órában. A tantárgy keretében a diszciplináris tantárgyak kerettantervi tartalmai megtanításra kerülnek. A tantárgy keretében kitűzőtt tanulási eredményeket a helyi tanterv tartalmazza. Garantáljuk, hogy minden tanuló megszerzi a Nat-ban és a kerettantervekben előírt természettudományos tudást, ismereteket legalább heti 5 természettudományos órán.

Abban az esetben, ha egy gyereknek továbbtanulás vagy érettségi miatt szüksége van a diszciplináris tantárgyakból osztályzatokra, az iskola megszervezi számára az osztályozó vizsgákat és biztosítja számára az egyéni felkészülés lehetőségét.

### Projektórát vezetünk be

A BPS iskolában a 9. és 10. évfolyamon 3-3 óra projekt óra jelenik meg, ahol projektoktatás során a téma meghatározása, a a munkamenet megtervezése és megszervezése, a témák feldolgozása és a feladat megoldása a gyerekek érdeklődésére, a gyerekek és a tanárok közös tevékenysége és együttműködése alapján alakul.

A projekt órák célja, hogy a gyerekek projekteken csapatban dolgozzanak (jó termékeket és szolgáltatásokat hozzanak létre) és, hogy a projekthez szükséges, a pedagógia program által amúgy is előírt tantárgyi tanulási eredményekből minél több relevánsat a gyerekek elérjenek.

A projekt órák témáját a pedagógia program előre nem rögzíti, hiszen az a tanárok egy a gyerekek együttműködésére épül, azonban a projekteket a tanároknak és a gyerekeknek trimeszterenként le kell dokumentálni.

#### Projektórák értékelése

Projektórákon való részvételt, a munkát és az elkészült eredményeket a tanárok és a gyerekek többszempontú értekelőtáblázatok segítségével értékelik.

Projektórából osztályozó vizsga nem szervezhető. Amennyiben egy gyerek valamilyen okból a projektórákon nem tudott résztvenni, akkor következő évfolyamba akkor léphet, ha a tantárgyi tanulási eredményeket elérte. Magyarul fogalmazva: a projektórák a tantárgyi tananyagon túlmutató tevékenységek.

## A vezetési modell

Az intézmény vezetését intézményvezető látja el, amely szerepkör ellátása elsősorban a tanügyigazgatási rendszer követelményeinek való megfelelés miatt szükséges. A Budapest Schoolban az intézményvezető nem köteles órát, foglalkozást tartani.

Az iskola belső működéséből és szervezeti kultúrájából következik, hogy iskolában állandó intézményvezető-helyettes megbízása nem szükséges. Az intézményvezető munkája a gyerekek létszámával nem növekszik, feladatai elvégzésében segítik más tanárok, megbízott szakértők, ideiglenes intézményvezető-helyettesek és a fenntartó is.

## A nevelés- és oktatásszervezési módszerek

### Mentor

Osztályfőnök feladatkörét az adott gyerek tekintetében a mentor látja el.

### Folyamatos tervezés, megvalósítás, mérés, fejlesztés

A program a tanárok autonómiájára kívánja bízni, hogy a gyerekek tanulási céljai, képességei alapján trimeszterenként alakítsák ki

1. a foglalkozások rendjét és idejét;
2. a csoportbontások rendszerét, a konkrétan kialakításra került csoportok összetételét, a bennük résztvevő tanulók nevét;
3. az aktív tanulási alapelvek szerint szerveződő, több tantárgy, tanulási terület ismereteinek integrálását igénylő témákat, jelenségeket; feldolgozó tanórák, foglalkozások, témanapok, témahetek, tematikus hetek és projektek alkalmazását;ű
4. 20/2012. (VIII. 31.) EMMI 12.§ (1-3) pontjaiban említett, a rendelet szerint a helyi tanterv kompentenciájába tartozó,
   1. a gyerekek, az egyes évfolyamok, ezen belül az egyes osztályok, valamint az osztályokon belüli csoportok tanítási óráit;
   2. különböző évfolyamok, különböző osztályok tanulóiból álló csoportok kialakításának rendjét;
   3. a kötelező, kötelezően választandó és szabadon választható foglalkozások rendszerét;
   4. azon kötelező tanítási foglalkozásokat, amelyeken egy adott osztály valamennyi tanulója köteles részt venni;
   5. azon kötelező tanítási foglalkozásokat, amelyeken a gyerekeknek a választásra felkínált tantárgyak közül kötelezően választva, az a tanárok által megadott óraszámban kell résztvenni.
5. a tantárgyi blokkosítások, és tömbösítések rendjét;

### Értékelés

#### Érdemjegyek helyett struktúrált szöveges visszajelzés és tanulási eredmények elszámolása

A BPS iskolában az érdemjegyek helyett a gyerekek folyamatosan kapnak visszajelzést teljesítményükről és előrehaladásukról:

1. Tanulási egységek, modulok, foglalkozásák, szakaszok, feladatok, projektek végén értékelő a tanárok értékelő táblázatokat (angolul rubric) alkalmaznak. Az értékelő táblázatban szerepelnek az értékelés szempontjai és szempontonkénti szintek, rövid leírásokkal. A táblázatok formája minden visszajelzés esetén (értsd foglalkozásonként, trimeszterenként, célonként) változtatható. Az értékelőtáblázat tekinthető struktúrált szöveges értékelésnek.

2. A gyerekek folyamatosan „gyűjtik" a tanulási eredményeket, ami folyamatosan visszajelzést ad nekik arról, hogy az évfolyamhoz tartozó tantárgyi követelmények hány százalékánál tartanak.

Ez alapján tanév közben nemcsak a gyerek, hanem tanárai és családja is mindig nyomon követheti, hogy hol tart a gyerek és mit kell tenni azért, hogy a tanév végére elérje a az évfolyamhoz tartozó követelményeket.

#### Osztályozás

Az iskola az érdemjegyek helyett használt folyamatos, többszempontú, személyes visszajelzéseit csak abban az esetben váltja osztályzatokká, azaz végzi el a félévi és az év végi minősítést osztályzatokkal, amikor _iskolaváltás vagy továbbtanulás miatt erre szükség van, vagy a szülő vagy a gyerek ezt kéri_. Az osztályzatok kialakítása csak a gyerek által elért eredmények, alkotásai, demonstrált tanulási eredményei, azaz a portfóliója alapján történhet. Az osztályzatok kialakításához szükséges minden adat egy online elérhető adatbázisként tartalmaz mindent, ami a döntéshez szükséges lehet.

Az osztályozás egyedi módja a következő:

A gyerekek portfóliója alapján egyértelműen megállapítható a teljesítésre került tanulási eredmények. Egy tanulási eredményt vagy teljesített a gyerek, vagy nem, részben teljesíteni nem lehetséges.

- az elért tanulási eredmények száma: a gyerek által adott félévben/évben teljesített tanulási eredményeinek számossága
- a félévre/évre számított összes tanulási eredmény: a Nat. adott tantárgyhoz rögzített valamennyi tanulási eredményének száma, osztva az adott időszak számosságával (tehát pl. etika tantárgyban a NAT 1-4. osztályra 50 tanulási eredményt ír elő, az egy félévre számított összes tanulási eredmény 7)
- az elért tanulási eredmények számát elosztjuk a félévre/évre számított összes tanulási eredmény számával, és megszorozzuk 100-zal
- a kapott százalékos mutatót az alábbi táblázat alapján osztályzattá alakítjuk:

| Teljesítmény | Osztályzat    |
| ------------ | ------------- |
| 0 - 40%      | 1 (elégtelen) |
| 41 - 50%     | 2 (elégséges) |
| 51 - 60%     | 3 (közepes)   |
| 61 - 70%     | 4 (jó)        |
| 71 - 100%    | 5 (példás)    |

#### Magatartás és szorgalom szöveges értékelése

Az Ntk. 54. § (1) előírja a tanuló magatartásának és szorgalmának értékelését és minősítését. Ezt a gyerek mentora trimeszterenként szövegesen vagy értékelő táblázatokban végzi el. Tehát nem az osztályfőnök végzi és nem érdemjegyekkel és osztályzatokkal.

## A minőségpolitika, a minőséggondozás rendszere

Az iskola és fenntartója a teljes szervezet minőségfejlesztését PDCA-ciklusként (plan - tervezés, do – cselekvés, check – ellenőrzés, act - beavatkozás) végzi. A gyerekek élményét leginkább befolyásoló foglalkozások tervezése trimeszterenként történik, a többi esetben a jogszabályok által megengedett legrövidebb csiklust alkalmazza az iskola. Ellenőrzésként több metrikát vesz figyelembe a folyamat, de mindenképp figyelembe kell venni a következőket:

1. A szülő, a gyerek és a tanár közötti saját célokat megfogalmazó hármas megállapodások időben megszülettek, nincs olyan gyerek, akinek nincs elfogadott saját tanulási célja. Indikátor: elkészült szerződések száma.
2. A modulok végén a portfóliók bővülnek, és azok tartalma a tantárgyakhoz kapcsolódik. Indikátor: a portfólió elemeinek száma és kapcsolhatósága.
3. A tanulási eredményekre vonatkozó megkötések időben teljesülnek. Indikátor: elért tanulási eredmények száma gyerekekre bontva.
4. A szülők biztonságban érzik gyereküket, és eleget tudnak arról, hogy mit tanulnak. Indikátor: kérdőíves vizsgálat alapján.
5. A tanárok hatékonynak tartják a munkájukat. Indikátor: kérdőíves felmérés alapján.
6. A gyerekek úgy érzik, folyamatosan tanulnak, támogatva vannak, vannak kihívásaik. Indikátor: kérdőíves felmérés alapján.

## A tanuló heti kötelező óraszáma

A NAT óraszámait tartjuk, a jogszabályokban előírtak szerint.

## A pedagógus végzettség és -szakképzettség

A BPS iskola egyedisége, hogy mentorok pedagógus munkakörben alkalmaz. A program a mentorok esetén kötelezően előírja, hogy csak olyan felnőtt alkalmazható mentorként, aki a köznevelési jogszabályok által pedagógus munkakörben alkalmazható és elvégezte a BPS saját 30 órás mentorképzését.

## A működéshez szükséges feltételek

## Helyiségek

A telephelyeken található helyiségeket az iskola a 20/2012.(VIII.31.) EMMI rendelet 2.számú mellékletében meghatározott felhatalmazás alapján alakítja ki: _Az eltérő pedagógiai elveket tartalmazó nevelési program az eszköz- és felszerelési jegyzéktől eltérően határozhatja meg a nevelőmunka eszköz és felszerelési feltételeit._

BPS modellben részletezett strukturális, szervezeti és tanulásszervezési módok miatt az iskola az iskolában az alábbi helyiségek megléte szükséges és elégséges:

| **helyiség megnevezése**        | **mennyiségi mutató**                                         | **megjegyzés**                                                                                                          |
| :------------------------------ | ------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| tanterem (1)                    | a telephelyen egyidőben jelenlévő gyerekek mind elférjenek    | min 15nm - max 120nm, jogszabályok által szabályozott gyerek / nm                                                       |
| laboratóriumok és szertárak (2) | iskolánként egy                                               | laboratóriumi tevékenység végzésére alkalmas létesítmény üzemeltetőjével kötött megállapodással is teljesíthető         |
| tornaterem                      | iskolánként egy                                               | kiváltható szerződéssel                                                                                                 |
| tornaszoba                      | székhelyen, telephelyeken egy                                 | csak ha ha a székhelynek v.telephelynek nincs saját tornaterme és akkor is kiváltható szerződéssel                      |
| sportudvar                      | székhelyen, telephelyeken egy                                 | kiváltható szerződéssel, vagy helyettesíthető alkalmas szabad területtel                                                |
| sportszertár (4)                | székhelyen, telephelyeken egy                                 | tornateremhez kapcsolódóan (kiváltható szerződéssel)                                                                    |
| általános szertár (4)           | székhelyen, telephelyeken egy                                 | tornateremhez kapcsolódóan (kiváltható szerződéssel)                                                                    |
| iskolatitkári iroda (5)         | iskolánként egy                                               | teljesen külön épületben, irodában is kialakítható                                                                      |
| nevelőtestületi szoba           | iskolánként (telephelyenként) egy                             |                                                                                                                         |
| könyvtár                        | iskolánként egy                                               | nyilvános könyvtár elláthatja a funkcót, megállapodás alapján                                                           |
| orvosi szoba                    | iskolánként egy                                               | amennyiben külső egészségügyi intézményben a gyerekek ellátása megoldható, nem kötelező; lehet ideiglenesen kialakított |
| ebédlő (6)                      | székhelyen, telephelyeken egy                                 | gyerek- és felnőttétkező közös helyiségben; tanteremmel közös helységben is kialakítható, de egyidőben csak egy funkció |
| főzőkonyha                      | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| melegítőkonyha (7)              | székhelyen, telephelyeken egy                                 | ha nem helyben főznek, de helyben étkeznek                                                                              |
| tálaló-mosogató                 | székhelyen, telephelyeken egy                                 | ha nem helyben főznek, de helyben étkeznek; melegítőkonyhával közös helyiségben is kialakítható                         |
| szárazáru raktár                | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| földesáru raktár                | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| éléskamra                       | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| élelmiszerhulladék-tároló (8)   | székhelyen, telephelyeken egy                                 | helyben étkeznek; nem kell külön helységben, ha biztonságosan eltávolítható a hulladék                                  |
| személyzeti WC                  | székhelyen, telephelyeken egy                                 | ha fülkés, különálló WC kerül kialakításra, akkor a gyerek WC-t és személyzeti WC-t nem kell megkülönbözteti            |
| gyerek WC                       | székhelyen, telephelyeken a gyereklétszám figyelembevételével | ha fülkés, különálló WC kerül kialakításra, akkor nem kell elkülöníteni a fiú és lány WC-t                              |
| mozgássérült WC                 | a mozgássérült gyereklétszám figyelembevételével              |                                                                                                                         |

1. Tanterem esetén az iskola azért nem osztályonként, hanem gyereklétszám arányában határozza meg a szükséges tantermek számát, mert osztálybontások esetén egy osztályra két tanteremnek kell jutnia, összevont foglalkozások esetén pedig fele annyi tanteremre van szükség. Ahhoz, hogy a tanárok meg tudják választani a megfelelő tanulásszervezési módszert, csoportbontást, ahhoz rugalmas terekre van szükség, és nem egyelő méretű tantermekre.
2. Amennyiben a 9-12. évfolyam tanulási eredményeinek eléréséhez szükséges laboratóriumi tevékenységek az iskolában található eszközökkel és termekben nem végezhetők el, az iskola és a fenntartó is kötelességet vállal arra, hogy a tanulási eredmény eléréséhez szükséges tárgyi feltételeket megállapodás, szerződés alapján biztosítsa.
3. A székhely és a telephelyek általánostól eltérően nagyobb száma és kisebb mérete más típusú intézmény vezetést tesz szükségessé, melynek része az egyes feladatellátási helyek rendszeres felkeresése. Az intézményvezető tanórát, foglalkozást nem, illetve minimális számban tart, így nem szükséges folyamatos jelenléte az adott székhelyen, telephelyen.
4. Az egyértelműség okán célszerű rögzíteni, hogy a sportszertár és általános szertár osztja a tornaterem sorsát.
5. A székhely és a telephelyek általánostól eltérően nagyobb száma és kisebb mérete miatt az adminisztráció működtetése célszerűen és hatékonyan megoldható a székhelyen kívül.
6. Technikailag megoldható hordozható hideg-meleg kézmosó alkalmazásával (népegészségügyi szerv által megerősített megoldás).
7. A melegítőkonyha funkciója az ebédlő részeként is kialakítható, ezen termek funkciója nem zárja ki az egy helyiségben történő kialakítást - különös tekintettel az Intézmény feladatellátási helyeinek általánostól eltérően nagyobb számára és kisebb méretére.
8. 852/2004/EK rendelet alapján az élelmiszer-hulladékot, a nem ehető melléktermékeket és egyéb hulladékot a felgyülemlésük elkerülése érdekében a lehető leggyorsabban el kell távolítani azokból a helyiségekből, amelyekben élelmiszer található. A rendelet nem írja elő, hogy ezt külön helységben kell tárolni.

A 20/2012. (VIII. 31.) EMMI rendelettől néhány terem esetén oly módon kívánunk eltérni, hogy az adott terem ne legyen kötelező a telephelyeken:

- **Aula**: Kevés gyereket befogadó épületben a legnagyobb tanterem általában elegendő a közösségi programokhoz. Nagyobb épületekben pedig a rendelet is kiválthatónak határozza meg.

- **Csoportterem**: A tantermek csoportteremként is funkcionálnak. Tekintettel arra, hogy az iskolában az osztályok gyerekszáma tipikus esetben 10 fő alatti, és az egyes foglalkozásokra létrejött csoportokban sok esetben több osztály tanulói is részt vesznek, a tanulásra kialakított terek megkülönböztetése (tan-, csoport-, szakterem) nem szükséges.
- **Egyéb raktár**: Amennyiben nem helyben étkeznek, a tanulói létszám feladatellátási helyenként alacsony mértéke okán nem szükséges egyéb helyiség kialakítása, közelebbről meg nem határozott tárgyak számára.
- **Felnőtt étkező**: Gyermekvédelmi szempontból nem megoldható, hogy a gyerekek tanár jelenléte nélkül tartózkodjanak külön helyiségben az étkezés idejére. Ezért a felnőttek együtt étkeznek a gyerekkel.
- **Intézményvezetői iroda**: A székhely és a telephelyek általánostól eltérően nagyobb száma és kisebb mérete más típusú intézmény vezetést tesz szükségessé, melynek része az egyes feladatellátási helyek rendszeres felkeresése. Az intézményvezető tanórát, foglalkozást nem, illetve minimális számban tart, így nem szükséges folyamatos jelenléte az adott székhelyen, telephelyen. Az iskolatitkári iroda olyan módon kerül kialakításra, hogy az az intézményvezető számára is használható.
- **Iskolapszichológusi szoba**: Az iskolapszichológusi tevékenységek megoldhatók a tantermekben, különös tekintettel arra, hogy egész napos iskolaként működik az iskola.
- **Logopédiai foglalkoztató, egyéni fejlesztő szoba**: A logopédiai foglalkozások megoldhatók a tantermekben, különös tekintettel arra, hogy egész napos iskolaként működik az iskola.
- **Porta**: Funkciója, a gyerekek biztonságának biztosítása, szabályzatokkal, beléptető rendszerekkel megoldható.
- **Szaktanterem a hozzá tartozó szertárral**: A szaktantermek funkciói a legtöbb esetben a tanteremben is megvalósíthatóak. Így számítástechnikai terem, társadalomtudományi szaktanterem, művészeti nevelés szaktanterem, technikai szaktanterem és gyakorló tanterem eszközigénye mobil eszközökkel is megoldható, úgy mint pl. laptop, rajztábla, satu, elektromos zongora. A természettudományi szaktanterem funkcióit pedig a szerződött laboratóriumokban elérhető a gyerekek számára. Amennyiben az adott tantárgyhoz, tantárgycsoporthoz tartozó tanulási eredmény az Intézményben található eszközökkel és termekben nem érhető el, az Intézmény és a fenntartó is kötelességet vállal arra, hogy a tanulási eredmény eléréséhez szükséges tárgyi feltételeket megállapodás, szerződés alapján biztosítsa.
- **Személyzeti öltöző** és **személyzeti mosdó-zuhanyzó**: Az intézmény a tanárokon és óraadókon kívül állandó egyéb alkalmazottat egy feladatellátási helyszínen nem foglalkoztat. Az egyéb alkalmazottak (pl. karbantartó, takarító, kisegítők) ideiglenes, a feladatuk ellátásához szükséges ideig tartózkodnak az intézmény területén, ezért ilyen terem kialakítása szükségtelen.
- **Teakonyha**: Tálaló-mosogató, ebédlő, és melegítőkonyha mellett külön teakonyha terem létesítése nem célszerű, az említett három ellátja a teakonyha funkcióját Intézményünkben, tekintettel az egy feladatellátási helyen egyszerre jelen lévő tanárok alacsony számára.
- **Technikai alkalmazotti mosdó-zuhanyzó, WC helyiség**: Az intézmény a tanárokon és óraadókon kívül állandó egyéb alkalmazottat egy feladatellátási helyszínen nem foglalkoztat. Az egyéb alkalmazottak (pl. karbantartó, takarító, kisegítők) ideiglenes, a feladatuk ellátásához szükséges ideig tartózkodnak az intézmény területén, a tanári WC-t használata számukra elégséges.

## Helyiségek bútorzata és egyéb berendezési tárgyai

A Budapest School telephelyein az alábbi feltételek megléte szükséges és elégséges:

- fenntartónak stabil szélessávú internethozzáférést kell biztosítania minden telephelyen;
- minden tanteremben minden órán elérhetőnek kell lennie legalább egy internethez kötött számítógépnek vagy tabletnek;
- tanulói asztalok, székek a gyerekek számának megfelelően;
- eszköztároló szekrény;
- tábla vagy flipchart legalább három különböző színű tollal;
- szeméttároló.

Amennyiben a tanároknak többletigénye
merül fel, akkor a szervezeti és működési szabályzatban lefektett módon
tudják az iskola és a fenntartó segítségét kérni.
