# Pedagógusokra vonatkozó előírások

Az iskolában többféle tanárszerepben lehet valaki pedagógus-munkakörben.

- A gyerekek [mentora](/tanulasi-elmeny/tanari-szerepek.md#mentor), az a felnőtt, aki segíti céljaik elérésében, figyel jóllétükre és kitüntetett figyelmet ad nekik.
- A [szaktanárok](/tanulasi-elmeny/tanari-szerepek.md#szaktanarok) azok a tanárok, akik valamilyen specifikus képesség, tudás elsajátításában, így például a tanulási eredmények elérésében és tevékenységben kísérik a gyerekekek.
- _Mentortanárnak_ hívja az iskola azokat a pedagógusokat, akik mentorok és szaktanárok is.
- Az iskola mentora és szaktanárai közül kerülnek ki a tanulóközösségeket vezető tanárcsapatok tagjai, a [tanulásszervezők](/tanulasi-elmeny/tanari-szerepek.md#tanulasszervezo).

## Biztonsági előírások

Ahhoz, hogy az iskola törekedjen a gyermekek számára biztonságos környezet kialakítására minden pedagógus-munkakörben alkalmazott tanár, valamint minden, a gyerekek körül és az épületekben általában feladatot ellátó személy esetén meg kell bizonyosodni, hogy

- cselekvőképes,
- büntetlen előéletű, és nem áll a tevékenység folytatását kizáró foglalkozástól eltiltás hatálya alatt és
- referenciát adó személy vagy munkáltató igazolta képességeit és a feladatra való rátermettségét.

## Elfogadott végzettségek és szakképzettségek

Fontos alapelv, hogy a tanárok személyisége, tudása, képességei és
kompetenciái határozzák meg a gyerekek élményét. Ezért az és csak az
lehet tanár, aki képes segíteni a gyerekeket a tanulásban. A végzettség
a tanárok hatékonyságának egyik indikátora. A különböző tanári szerepekhez különböző képességekre van szükség és így különböző végzettségek lehetnek a képesség indikátorai.

| Pedagógus-munkakör    | Az alkalmazáshoz szükséges feltétetel                                                       |
| --------------------- | ------------------------------------------------------------------------------------------- |
| Mentor                | A köznevelési jogszabályok által pedagógus munkakörben alkalmazható + 30 órás mentor képzés |
| Szaktanár             | Nkt. előírása alapján                                                                       |
| Mentortanár           | Mentor + szaktanár                                                                          |
| Tanulásszervező tanár | A köznevelési jogszabályok által pedagógus munkakörben alkalmazható + 30 órás mentor képzés |

## Intézményvezető és -helyettes

Az intézmény vezetését intézményvezető látja el, amely szerepkör ellátása elsősorban a tanügyigazgatási rendszer követelményeinek való megfelelés miatt szükséges. A Budapest Schoolban az intézményvezető nem köteles órát, foglalkozást tartani.

Az iskola belső működéséből és szervezeti kultúrájából következik, hogy iskolában állandó intézményvezető-helyettes megbízása nem szükséges. Az intézményvezető munkája a gyerekek létszámával nem növekszik, feladatainak elvégzésében segíthetik más tanárok, megbízott szakértők, ideiglenesen az intézményvezetésére kijelölt munkatársak és a fenntartó is.

Amennyiben az intézményvezető akadályoztatva van, vagy munkaköre megürült, a fenntartó jogosult olyan személy ideiglenes megbízására, aki pedagógus végzettséggel és legalább 2 éves, igazolt vezetői tapasztalattal rendelkezik. A fenntartónak ebben az esetben azonnal ki kell neveznie az ideiglenes intézményvezetőt.

## Kötött és kötetlen munkaidő szabályozása

A Budapest School-iskolákban alkalmazott tanárok munkaviszonyban vagy megbízási jogviszonyban állnak. A munkaviszonyban álló tanárok rendes vagy kötetlen munkaidőben dolgoznak.

Az iskola megközelítése az, hogy a
tanárok a NAT, a saját maguk és közösségi célok által kialakított
eredmények elérésére vállalnak kötelezettséget. Ezért az Nkt. és a kapcsolódó jogszabályok által előírt munkaidő-beosztási és -nyilvántartási szabályoktól a jelen modell eltérést enged. Az eltérésekben az iskolának és a tanároknak meg kell egyezniük, és a részleteknek a munka-, illetve megbízási szerződésekben meg kell jelenniük.

A tanulóközösség tanárcsapatának a feladata a szükséges óraszámok,
beosztások kialakítása és a megfelelő szaktanárok megtalálása.

## Tanár - gyerek arány

A mentor egyik legnagyobb ajándéka a mentoráltja számára a minőségi figyelme. Odafigyel rá, meghallgatja, egyénileg támogatja, együttérez vele, kapcsolatait és tudását átadja számára. Hogy ennek a kapcsolatnak a minőségét ne veszélyeztessük, a BPS modell megköti, hogy egy tanár maximum hány mentorálttal dolgozhat. Az első négy évfolyamon maximum 15, a felső négy évfolyamon 20 és 8–12. évfolyamon 25 gyerek tartozhat maximum egy mentorhoz.

A tanulásszervező tanárcsapat minimális mérete a tanulóközösség méretétől függ. Egy-egy közösségben 20 gyerekenként legalább egy tanulásszervezőnek kell dolgoznia.

Szaktanárok esetén ilyen arányszámot nehéz megadni. Egy kevert (blended) tanulási környezetben egyetlen szaktanár akár több száz tanulóval tud egyszerre foglalkozni foglalkozás keretében. De például kémiai kisérletező modulok során nem ajánlott, hogy 15 embernél több legyen jelen a laborban.
