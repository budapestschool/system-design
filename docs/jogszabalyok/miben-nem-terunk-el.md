# A Budapest School Modell kapcsolódása a közoktatáshoz

A rendszerünk jobb átláthatósága miatt az alábbiakban kivonatosan összefoglaljuk, miben nem különbözik a BPS Általános Iskola és Gimnázium egyedi pedagógiai programja a közoktatás működési szabályaitól, azaz intézményünk miben nem kérelmez eltérést az Nkt. előírásaitól. Mivel sok esetben inkább nem egyedi az iskola működése, ezért nem célunk egy teljes lista összeállítása, a legfőbb pontokat emeltük ki:

1. Minden gyerek minden félévben minden tantárgyból **osztályzat**ot kap.
2. Az iskolában **a NAT tantárgyai mind** megjelennek, céljukat és tartalmukat az iskola nem változtatja, csak lehetőséget ad azok kiegészítésére.
3. Az iskola minden gyerek számára célul tűzi ki a **NAT minden tanulási eredmény**ének elérését.
4. Az iskolának vannak **teljesítmény-elvárásai és fontos az előírt tananyag átadása**, a követelmények teljesítése, a tanulói terhelés korlátozására vonatkozó rendelkezések megtartása.
5. Az iskolában tudomásul vesszük, hogy a gyerekek egyéni teljesítőképessége változó. Lesznek olyanok, akik **elégséges szinten** érik el a tanulási eredményeket, és lesznek, akik **példásan teljesítenek**.
6. Az iskola működése biztosítja az **iskolák közötti átjárhatóság**ot, mivel az iskola helyi tanterve száz százalékban követi a NAT-ot, ugyanolyan bizonyítványt kapnak, ugyanolyan osztályzatokkal, mint a többi köznevelési intézményben.
7. Az iskolában **az Nkt. szerinti pedagógus végzettség**nek elfogadott végzettségű pedagógusok tanítanak.
8. Az iskolában **a tanítás alapegységei a foglalkozások**, amelyeknek a kereteit, témáját a tanárok alakítják, a folyamatokat ők irányítják az elérni kívánt célok alapján.
9. Az iskola **felkészít az érettségire**, megszervezi azt, és cél, hogy a gyerekek leérettségizzenek.
10. Az iskolának - székhelye mellett - vannak **telephelyei, amelyek alkalmasak oktatás és nevelés ellátására**. A telephelyeket az illetékes hatóság engedélyezi.
11. Az Nkt.-ban lefektetett **mindennapos testmozgás** elvét és előírásait változatlan formában követi az iskola.
