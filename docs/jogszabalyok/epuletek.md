# Épületekre vonatkozó előírások

## Telephelyek

A Budapest School több telephellyel rendelkezik. Minden telephely egy intézményhez tartozik, az egységes iskola működéséhez nincs szükség tagintézmények létrehozására (azaz a BPS nem hoz létre tagintézményeket, kizárólag telephelyeket), tekintettel arra, hogy az iskolák szervezeti irányítása a BPS modell és azt leképező szoftverrendszer segítségével megoldható anélkül, hogy az egyes helyszíneket ne telephelyként, hanem tagintézményként kelljen működtetni.

A telephelyek nem csak nevelési, oktatási célú ingatlanokban alakíthatók ki.

## Helyiségek

A telephelyeken található helyiségeket az iskola a 20/2012.(VIII.31.) EMMI rendelet 2. számú mellékletében meghatározott felhatalmazás alapján alakítja ki: _Az eltérő pedagógiai elveket tartalmazó nevelési program az eszköz- és felszerelési jegyzéktől eltérően határozhatja meg a nevelőmunka eszköz és felszerelési feltételeit._

A BPS modellben részletezett strukturális, szervezeti és tanulásszervezési módok miatt az iskolában az alábbi helyiségek megléte szükséges és elégséges:

| **Helyiség megnevezése**        | **Mennyiségi mutató**                                         | **Megjegyzés**                                                                                                          |
| :------------------------------ | ------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| tanterem (1)                    | a telephelyen egyidőben jelenlévő gyerekek mind elférjenek    | 15–120nm , jogszabályok által szabályozott gyerek / nm                                                                  |
| laboratóriumok és szertárak (2) | iskolánként egy                                               | laboratóriumi tevékenység végzésére alkalmas létesítmény üzemeltetőjével kötött megállapodással is teljesíthető         |
| tornaterem                      | iskolánként egy                                               | kiváltható szerződéssel                                                                                                 |
| tornaszoba                      | székhelyen, telephelyeken egy                                 | csak ha ha a székhelynek v.telephelynek nincs saját tornaterme és akkor is kiváltható szerződéssel                      |
| sportudvar                      | székhelyen, telephelyeken egy                                 | kiváltható szerződéssel, vagy helyettesíthető alkalmas szabad területtel                                                |
| sportszertár (4)                | székhelyen, telephelyeken egy                                 | tornateremhez kapcsolódóan (kiváltható szerződéssel)                                                                    |
| általános szertár (4)           | székhelyen, telephelyeken egy                                 | tornateremhez kapcsolódóan (kiváltható szerződéssel)                                                                    |
| iskolatitkári iroda (5)         | iskolánként egy                                               | teljesen külön épületben, irodában is kialakítható                                                                      |
| nevelőtestületi szoba           | iskolánként (telephelyenként) egy                             |                                                                                                                         |
| könyvtár                        | iskolánként egy                                               | nyilvános könyvtár elláthatja a funkcót, megállapodás alapján                                                           |
| orvosi szoba                    | iskolánként egy                                               | amennyiben külső egészségügyi intézményben a gyerekek ellátása megoldható, nem kötelező; lehet ideiglenesen kialakított |
| ebédlő (6)                      | székhelyen, telephelyeken egy                                 | gyerek- és felnőttétkező közös helyiségben; tanteremmel közös helységben is kialakítható, de egyidőben csak egy funkció |
| főzőkonyha                      | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| melegítőkonyha (7)              | székhelyen, telephelyeken egy                                 | ha nem helyben főznek, de helyben étkeznek                                                                              |
| tálaló-mosogató                 | székhelyen, telephelyeken egy                                 | ha nem helyben főznek, de helyben étkeznek; melegítőkonyhával közös helyiségben is kialakítható                         |
| szárazáru raktár                | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| földesáru raktár                | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| éléskamra                       | székhelyen, telephelyeken egy                                 | ha helyben főznek                                                                                                       |
| élelmiszerhulladék-tároló (8)   | székhelyen, telephelyeken egy                                 | helyben étkeznek; nem kell külön helységben, ha biztonságosan eltávolítható a hulladék                                  |
| személyzeti WC                  | székhelyen, telephelyeken egy                                 | ha fülkés, különálló WC kerül kialakításra, akkor a gyerek-WC-t és személyzeti WC-t nem kell megkülönbözteti            |
| gyerek WC                       | székhelyen, telephelyeken a gyereklétszám figyelembevételével | ha fülkés, különálló WC kerül kialakításra, akkor nem kell elkülöníteni a fiú- és lány-WC-t                             |
| mozgássérült WC                 | a mozgássérült gyereklétszám figyelembevételével              |                                                                                                                         |

1. Tanterem esetén az iskola azért nem osztályonként, hanem gyereklétszám arányában határozza meg a szükséges tantermek számát, mert osztálybontások esetén egy osztályra két tanteremnek kell jutnia, összevont foglalkozások esetén pedig feleannyi tanteremre van szükség. Ahhoz, hogy a tanárok meg tudják választani a megfelelő tanulásszervezési módszert, csoportbontást, ahhoz rugalmas terekre van szükség, és nem pedig egyelő méretű tantermekre.
2. Amennyiben a 9–12. évfolyam tanulási eredményeinek eléréséhez szükséges laboratóriumi tevékenységek az iskolában található eszközökkel és termekben nem végezhetők el, az iskola és a fenntartó is kötelezettséget vállal arra, hogy a tanulási eredmény eléréséhez szükséges tárgyi feltételeket megállapodás, szerződés alapján biztosítsa.
3. A székhely és a telephelyek általánostól eltérően nagyobb száma és kisebb mérete más típusú intézményvezetést tesz szükségessé, melynek része az egyes feladatellátási helyek rendszeres felkeresése. Az intézményvezető tanórát, foglalkozást nem, illetve minimális számban tart, így nem szükséges folyamatos jelenléte az adott székhelyen, telephelyen.
4. Az egyértelműség okán célszerű rögzíteni, hogy a sportszertárra és az általános szertár osztja a tornaterem sorsát.
5. A székhelynek és a telephelyeknek általánostól eltérően nagyobb száma és kisebb mérete miatt az adminisztráció működtetése célszerűen és hatékonyan megoldható a székhelyen kívül.
6. Technikailag megoldható hordozható hideg-meleg kézmosó alkalmazásával (népegészségügyi szerv által megerősített megoldás).
7. A melegítőkonyha funkciója az ebédlő részeként is kialakítható, ezeknek a termeknek a funkciója nem zárja ki az egy helyiségben történő kialakítást — különös tekintettel az intézmény feladatellátási helyeinek az általánostól eltérően nagyobb számára és kisebb méretére.
8. A 852/2004/EK rendelet alapján az élelmiszer-hulladékot, a nem ehető melléktermékeket és egyéb hulladékot a felgyülemlésük elkerülése érdekében a lehető leggyorsabban el kell távolítani azokból a helyiségekből, amelyekben élelmiszer található. A rendelet nem írja elő, hogy ezeket külön helységben kell tárolni.

A 20/2012. (VIII. 31.) EMMI-rendelettől néhány terem esetén oly módon kívánunk eltérni, hogy az adott terem ne legyen kötelező a telephelyeken:

- **Aula**: Kevés gyereket befogadó épületben a legnagyobb tanterem általában elegendő a közösségi programokhoz. Nagyobb épületekben pedig a rendelet is kiválthatónak határozza meg.

- **Csoportterem**: A tantermek csoportteremként is funkcionálnak. Tekintettel arra, hogy az iskolában az osztályok gyerekszáma tipikus esetben 10 fő alatti, és az egyes foglalkozásokra létrejött csoportokban sok esetben több osztály tanulói is részt vesznek, a tanulásra kialakított terek megkülönböztetése (tan-, csoport-, szakterem) nem szükséges.
- **Egyéb raktár**: Amennyiben nem helyben étkeznek, a tanulói létszám feladatellátási helyenként alacsony mértéke okán nem szükséges egyéb helyiség kialakítása, közelebbről meg nem határozott tárgyak számára.
- **Felnőtt étkező**: Gyermekvédelmi szempontból nem megoldható, hogy a gyerekek tanár jelenléte nélkül tartózkodjanak külön helyiségben az étkezés idejére. Ezért a felnőttek együtt étkeznek a gyerekkel.
- **Intézményvezetői iroda**: A székhelynek és a telephelyeknek általánostól eltérően nagyobb száma és kisebb mérete más típusú intézményvezetést tesz szükségessé, melynek része az egyes feladatellátási helyek rendszeres felkeresése. Az intézményvezető tanórát, foglalkozást nem, illetve minimális számban tart, így nem szükséges folyamatos jelenléte az adott székhelyen, telephelyen. Az iskolatitkári iroda olyan módon kerül kialakításra, hogy az intézményvezető számára is használható legyen.
- **Iskolapszichológusi szoba**: Az iskolapszichológusi tevékenységek megoldhatók a tantermekben, különös tekintettel arra, hogy egész napos iskolaként működik az iskola.
- **Logopédiai foglalkoztató, egyéni fejlesztő szoba**: A logopédiai foglalkozások megoldhatók a tantermekben, különös tekintettel arra, hogy egész napos iskolaként működik az iskola.
- **Porta**: Funkciója, a gyerekek biztonságának biztosítása, szabályzatokkal, beléptető rendszerekkel megoldható.
- **Szaktanterem a hozzá tartozó szertárral**: A szaktantermek funkciói a legtöbb esetben a tanteremben is megvalósíthatók. Így számítástechnikai terem, társadalomtudományi szaktanterem, művészeti nevelés szaktanterem, technikai szaktanterem és gyakorló tanterem eszközigénye mobil eszközökkel is megoldható, úgy mint pl. laptop, rajztábla, satu, elektromos zongora. A természettudományi szaktanterem funkciói pedig a szerződött laboratóriumokban elérhetők a gyerekek számára. Amennyiben az adott tantárgyhoz, tantárgycsoporthoz tartozó tanulási eredmény az intézményben található eszközökkel és termekben nem érhető el, az intézmény és a fenntartó is kötelezettséget vállal arra, hogy a tanulási eredmény eléréséhez szükséges tárgyi feltételeket megállapodás, szerződés alapján biztosítja.
- **Személyzeti öltöző** és **személyzeti mosdó-zuhanyzó**: Az intézmény a tanárokon és óraadókon kívül állandó egyéb alkalmazottat egy feladatellátási helyszíneken nem foglalkoztat. Az egyéb alkalmazottak (pl. karbantartó, takarító, kisegítők) ideiglenes, a feladatuk ellátásához szükséges ideig tartózkodnak az intézmény területén, ezért ilyen terem kialakítása szükségtelen.
- **Teakonyha**: Tálaló-mosogató, ebédlő, és melegítőkonyha mellett külön teakonyha-terem létesítése nem célszerű, az említett három ellátja a teakonyha funkcióját intézményünkben, tekintettel az egy feladatellátási helyen egyszerre jelen lévő tanárok alacsony számára.
- **Technikai alkalmazotti mosdó-zuhanyzó, WC helyiség**: Az intézmény a tanárokon és óraadókon kívül állandó egyéb alkalmazottat a feladatellátási helyszíneken nem foglalkoztat. Az egyéb alkalmazottak (pl. karbantartó, takarító, kisegítők) ideiglenes, a feladatuk ellátásához szükséges ideig tartózkodnak az intézmény területén, a tanári WC használata számukra elégséges.

## Helyiségek bútorzata és egyéb berendezési tárgyai

A Budapest School telephelyein az alábbi feltételek megléte szükséges és elégséges:

- a fenntartónak stabil szélessávú internethozzáférést kell biztosítania minden telephelyen;
- minden tanteremben minden órán elérhetőnek kell lennie legalább egy internethez kötött számítógépnek vagy tabletnek;
- tanulói asztalok, székek a gyerekek számának megfelelően;
- eszköztároló szekrény;
- tábla vagy flipchart legalább három különböző színű tollal;
- szeméttároló.

Amennyiben a tanároknak többletigényük
merül fel, akkor a szervezeti és működési szabályzatban lefektett módon
tudják az iskola és a fenntartó segítségét kérni.
