**MAGYARORSZÁG KORMÁNYA**

[Közli:]{.underline} Magyar Közlöny

**A [K]{.smallcaps}ORMÁNY**

**\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--**

**rendelete**

**a Nemzeti alaptanterv kiadásáról, bevezetéséről és alkalmazásáról
szóló**

**110/2012. (VI. 4.) Korm. rendelet módosításáról**

A Kormány a nemzeti köznevelésről szóló 2011. évi CXC. törvény 94. § (4)
bekezdés b) pontjában,

a 7. § (2) bekezdése tekintetében a jogalkotásról szóló 2010. évi CXXX.
törvény 31. § (1) bekezdés *a)* pont *aa)* alpontjában

kapott felhatalmazás alapján, az Alaptörvény 15. cikk (1) bekezdésében
meghatározott feladatkörében eljárva a következőket rendeli el:

**1. §**

**A Nemzeti alaptanterv kiadásáról, bevezetéséről és alkalmazásáról
szóló 110/2012. (VI. 4.) Korm. rendelet (a továbbiakban: Rendelet) 5. §
(1) bekezdése helyébe a következő rendelkezés lép:**

„(1) A kerettantervek meghatározzák

*a)* a nevelés-oktatás céljait,

*b)* a tantárgyi rendszert,

*c)* az egyes tantárgyak témaköreit, tartalmát,

*d)* a tantárgyak követelményeit,

*e)* a tantárgyközi tudás- és képességterületek fejlesztésének
feladatait,

*f)* a követelmények teljesítéséhez rendelkezésre álló időkeretet."

**2. §**

A Rendelet 3. alcíme a következő 5/A. §-sal és 5/B. §-sal egészül ki:

„5/A. § Az oktatásért felelős miniszter kerettantervet, alapprogramot,
irányelvet ad ki és tesz közzé.

5/B. § A nemzeti köznevelésről szóló törvény 9. § (8) bekezdésében
rögzített egyedi megoldásoknak a Nat-ban és a kerettantervekben
meghatározott ismeretanyagot tartalmazniuk kell."

**3. §**

\(1) A Rendelet 8. § (3) bekezdése helyébe a következő rendelkezés lép:

„(3) A tanuló kötelező és választható tanítási óráinak összege -- a
(4)-(7) bekezdésben foglalt eltérésekkel -- egy tanítási héten
legfeljebb

*a)* az első-harmadik évfolyamon huszonnégy,

*b)* a negyedik évfolyamon huszonöt,

*c)* az ötödik és a hatodik évfolyamon huszonnyolc,

*d)* a hetedik és a nyolcadik évfolyamon harminc,

*e)* a kilencedik-tizenharmadik évfolyamon harmincnégy

óra lehet."

\(2) A Rendelet 8. §-a a következő (4)-(7) bekezdéssel egészül ki:

„(4) A két tanítási nyelvű iskolai oktatásban, vagy ha az iskola a
nemzeti köznevelésről szóló törvény 7. § (6) bekezdése szerint
sportiskola feladatait látja el, továbbá ha az iskola emelt szintű
oktatást szervez, a tanuló kötelező és választható tanítási óráinak
összege a (3) bekezdésben egy tanítási hétre meghatározott időkerethez
képest legfeljebb kettő tanítási órával emelkedhet.

\(5) A nemzetiségi nevelést-oktatást folytató iskolában a tanuló kötelező
és választható tanítási óráinak összege a (3) bekezdésben egy tanítási
hétre meghatározott időkerethez képest az 1--8. évfolyamon legfeljebb
három tanítási órával, a 9--12. évfolyamon legfeljebb négy tanítási
órával emelkedhet.

\(6) A (4) és (5) bekezdésben meghatározott óratöbbletek nem
összevonhatóak.

\(7) A nyelvi előkészítő évfolyamon

*a)* a heti tanórai foglalkozások száma legalább harminc, legfeljebb
harminckettő lehet,

*b)* idegen nyelv tantárgy esetében heti tizennyolc, a digitális kultúra
tantárgy esetében heti három, a testnevelés tantárgy esetében heti öt
tanórai foglalkozást kell biztosítani, továbbá heti négy-hat tanórát
képességfejlesztésre kell fordítani."

**4. §**

A Rendelet a következő 12. §-sal egészül ki:

„12. § (1) A Nemzeti alaptanterv kiadásáról, bevezetéséről és
alkalmazásáról szóló 110/2012. (VI. 4.) Korm. rendelet módosításáról
szóló .../2020. (...) Korm. rendelettel (a továbbiakban: Módr.)
módosított 1. § (1) és (2) bekezdését, 3. § (2), (5) és (6) bekezdését,
5. § (1), (3) és (4) bekezdését, 8. §-át és Mellékletét első alkalommal
a 2020/2021. tanévben

*a)* az iskolák első évfolyamán,

*b)* az iskolák ötödik, − a hat- és nyolcévfolyamos gimnáziumok
kivételével − kilencedik évfolyamán,

*c)* hatévfolyamos gimnázium esetében a hetedik évfolyamán,

majd ezt követően minden tanévben felmenő rendszerben kell alkalmazni.

\(2) Az iskolák a Módr. alapján 2020. április 30-ig felülvizsgálják a
pedagógiai programjukat.

\(3) Az iskolák az (1) bekezdés által érintett évfolyamokon a (2)
bekezdés szerint felülvizsgált pedagógiai program szerint kezdik meg a
2020/2021. tanévben a nevelő-oktató munkát.

\(4) Az iskoláknak az (1) bekezdésben fel nem sorolt évfolyamain a
nevelő-oktató munkát a Módr. 7. § (2) bekezdésében meghatározott
miniszteri rendeleteknek a Módr. hatálybalépésekor hatályos
rendelkezései alkalmazásával kell megszervezni."

**5. §**

A Rendelet Melléklete az *1. melléklet* szerint módosul.

**6. §**

A Rendelet

*a)* 1. §

*aa)* (1) bekezdés *a)* pontjában a „*b)*-*e)* és *g)*" szövegrész
helyébe a „*b)*-*f) és h)-i)*",

*ab)* (2) bekezdésében a „*b)-c)*" szövegrész helyébe a „*b)*-*d)*",

*b)* 3. §

*ba)* (2) bekezdésében a *„fejlesztési feladatok és közműveltségi
tartalmak"* szövegrész helyébe a *„tantárgyaknál megadott fő témakörök
és tanulási eredmények",*

*bb)* (5) bekezdésében a *„középiskolai nevelés-oktatás szakaszában
folyó"* szövegrész helyébe a *„középfokú nevelés-oktatás és a szakmai
oktatás szakaszában folyó",*

*bc)* (6) bekezdésében a *„szakiskolában"* szövegrész helyébe a
*„szakképző iskolában"*, a *„szakképzés"* szövegrész helyébe a
*„munkaerőpiac*" szöveg, a *„szakiskolai"* szövegrész helyébe a
*„szakképző iskolai"*,

*c)* 5. §

*ca)* (3) bekezdésében a *„szakképző iskolák"* szövegrész helyébe a
*„szakképző intézmény, szakgimnázium, szakiskola"*,

*cb)* (4) bekezdésében az *„az Ember és társadalom, az Ember és
természet"* szövegrész helyébe az „*a Történelem és állampolgári
ismeretek, a Természettudomány és földrajz"*,

*d)* 8. § (1) bekezdés

*da)* *a)* pontjában az *„első-harmadik"* szövegrész helyébe az
*„első-negyedik"*,

*db)* *c)* pontjában az *„ötödik-nyolcadik évfolyamon"* szövegrész
helyébe az *„ötödik-nyolcadik és a nyelvi előkészítő évfolyamon"*,

*dc)* *d)* pontjában a *„kilencedik-tizenkettedik"* szövegrész helyébe a
*„kilencedik-tizenharmadik"*

szöveg lép.

**7. §**

\(1) Hatályát veszti a Rendelet

*a)* 3. § (7) bekezdése,

*b)* 3/A. alcíme,

*c)* 6. § (4) bekezdésében a „Hídprogramok," szövegrész,

*d)* 8. §

*da)* (1) bekezdés *b)* pontja,

*db)* (2) bekezdése.

\(2) Hatályát veszti

*a)* az alapfokú művészetoktatás követelményei és tantervi programjának
bevezetéséről és kiadásáról szóló 27/1998. (VI. 10.) MKM rendelet,

*b)* a Sajátos nevelési igényű gyermekek óvodai nevelésének irányelve és
a Sajátos nevelési igényű tanulók iskolai oktatásának irányelve
kiadásáról szóló 32/2012. (X. 8.) EMMI rendelet,

*c)* a kerettantervek kiadásának és jóváhagyásának rendjéről szóló
51/2012. (XII. 21.) EMMI rendelet,

*d)* a két tanítási nyelvű iskolai oktatás irányelvének kiadásáról szóló
4/2013. (I. 11.) EMMI rendelet,

*e)* a nemzetiség óvodai nevelésének irányelve és a nemzetiség iskolai
oktatásának irányelve kiadásáról szóló 17/2013. (III. 1.) EMMI rendelet,

*f)* a kerettantervek kiadásának és jóváhagyásának rendjéről szóló
51/2012. (XII. 21.) EMMI rendelet módosításáról szóló 22/2016. (VIII.
25.) EMMI rendelet,

*g)* a Kollégiumi nevelés országos alapprogramjának kiadásáról szóló
59/2013. (VIII. 9.) EMMI rendelet.

**8. §**

Ez a rendelet a kihirdetését követő nyolcadik napon lép hatályba.

> ( Orbán Viktor )
>
> miniszterelnök

*[1. melléklet a(z) ....../2020. (... ...) Korm.
rendelethez]{.underline}*

1\. A Rendelet Melléklet I. Része az I.1. pontot megelőzően a következő
rendelkezéssel egészül ki:

„Alapvetés

A Nat a magyar kulturális és pedagógiai örökség gyökereiből táplálkozik,
annak hagyományaira épül. Meghatározza azokat a nevelési-oktatási
alapelveket, amelyek a nemzeti köznevelésről szóló törvény 5. § (4)
bekezdésében foglaltaknak megfelelően biztosítják az iskolai
nevelés-oktatás tartalmi egységét, az iskolák közötti átjárhatóságot. A
Nat emellett meghatározza az elsajátítandó tanulási tartalmakat,
valamint kötelező rendelkezéseket állapít meg az oktatásszervezés
körében. A Nat lefekteti a köznevelés elvi és tartalmi alapjait és
kereteit, azaz meghatározza az alapműveltség kötelezően közvetítendő
tartalmait az alap- és középfokú oktatási intézmények számára, beleértve
a különleges bánásmódot igénylő tanulókat ellátó intézményeket is. A Nat
a köznevelés szemléleti alapjainak meghatározásával kiegészíti a
gyermekek, tanulók családban megvalósuló nevelését, erősíti ezzel a
hazához és a nemzet történelméhez való kötődést, a generációk közötti
kapcsolatot, a közös kulturális gyökereket, az anyanyelv használatát.
Így rögzíti azt a minden magyar emberben közös tudást, amely megalapozza
a nemzeti identitást. A Nat elsődleges felhasználója a nevelési-oktatási
intézményben dolgozó pedagógus, valamint az intézményvezető. Ez a
dokumentum az ő munkájukhoz ad iránymutatást, keretet."

2\. A Rendelet Melléklet I. Rész I.1.2. pontja helyébe a következő
rendelkezés lép:

„*I.1.2. Egységesség és differenciálás, módszertani alapelvek*

Az aktív tanulás a tanulónak a tanulási tevékenységekben történő
részvételét hangsúlyozza. A tanulási tevékenység legfőbb célja olyan
tanulói kompetenciák fejlesztése, amelyek lehetővé teszik az
ismereteknek különböző helyzetekben történő kreatív alkalmazását. A
tevékenységekre épülő tanulásszervezési formák segítik a tanulót a
tanulási eredmények által kijelölt ismeretek megszerzésében, és ezen
keresztül a kompetenciák fejlesztésében. Lehetőség szerint ki kell
használni a tanulás társas természetéből adódó előnyöket, a
differenciált egyéni munka adta lehetőségeket. Segíteni kell a párban
vagy csoportban végzett felfedező, tevékeny és jól szervezett,
együttműködésen alapuló tanulást. A tanulási eredmények elérését segítik
elő az olyan differenciáló módszerek, mint a minden szempontból
akadálymentes és minden tanuló számára egyformán hozzáférhető tanulási
környezet biztosítása, a tanulói különbségekhez illeszkedő,
differenciált célkijelölés, a többszintű tervezés és
tananyag-alkalmazás, a fejlesztő, tanulást támogató értékelés. A
differenciált tanulásszervezés jellegzetességeit képviselik az olyan
eljárások, mint az egyéni rétegmunka vagy az adaptált szövegváltozatok
felhasználása, melyek kiterjeszthetik és elmélyíthetik a tankönyvek
tartalmát.

A pedagógus a probléma-megoldási és a jelenségértelmezési folyamatot --
a tanuló szükségleteinek megfelelően -- közvetett, illetve közvetlen
eszközökkel segíti. A pedagógus az aktív tanulói tevékenységek
megvalósítása során lehetővé teszi iskolán kívüli szakemberek bevonását,
valamint a külső helyszínek nyújtotta pedagógiai lehetőségek
felhasználását (könyvtár, múzeum, levéltár, színház, koncert). A
pedagógus együttműködik más tantárgyakat tanító pedagógusokkal azért,
hogy a tanulóknak lehetőségük legyen a tanórákon vagy a témahetek,
tematikus hetek, projektnapok, témákhoz szervezett események, tanulmányi
kirándulások, iskolai táborok alkalmával a tantárgyak szervezett,
összefüggő, illetve kapcsolódó tartalmainak integrálására.

Az iskoláknak tanítási évenként több olyan tanóra megszervezését
ajánlott beilleszteniük a helyi tantervbe, amelyben több tantárgy
ismereteinek integrálását igénylő (multidiszciplináris) téma kerül a
középpontba, a tanóra céljának, tartalmának és megvalósítási
módszereinek megjelölésével. A különleges bánásmódot igénylő tanulók
esetében a tananyag feldolgozásánál a pedagógusnak figyelembe kell
vennie a tantárgyi tartalmaknak a tanulói sajátosságokhoz való
illesztését. A különleges bánásmódot igénylő tanulók esetében ez az
adaptálás lehetővé teszi az egyéni haladási ütem biztosítását, valamint
a differenciált (optimális esetben személyre szabott) nevelés, oktatás
során az egyéni módszerek alkalmazását. Az aktív tanulás segítése a
tanuló tehetségének, különleges nevelési-oktatási szükségleteinek vagy
fogyatékosságának típusához igazodó szakképzettséggel rendelkező
szakember támogatásával történik.

**Az eredményes tanulás segítésének elvei**

*Tanulási környezet*

A tanulás közvetlen helyszíneként használt helyiségeket (kiemelten
osztálytermeket) lehetőség szerint úgy kell biztosítani, hogy a
különböző tanulásszervezési eljárások alkalmazásához a berendezések
rugalmasan és gyorsan átalakíthatók legyenek, illeszkedjenek az
osztályba járó tanulók korosztályi és egyéni szükségleteihez, valamint
nyugodt, biztonságos és támogató tanulási környezetet teremtsenek
valamennyi tanuló számára. Lehetőség szerint biztosítani kell, hogy a
tanulók a foglalkozásokon IKT és digitális eszközöket (számítógép, más
iskolai vagy saját eszköz), internetkapcsolatot és prezentációs
eszközöket vehessenek igénybe, valamint hozzáférhetővé váljanak a
hagyományos iskolai és az elektronikus könyvtárak egyaránt.

A tanulók, a pedagógusok, a szülők és a pedagógiai munkát támogató
minden szereplő kapcsolata -- a közös célt szem előtt tartva -- a
kölcsönös tiszteleten és nyílt párbeszéden alapul.

A tanulók értékelését egyéni fejlődésük és sikeres tanulási
teljesítményük érdekében az igazságosság, az esélyteremtés és a
méltányosság alapelveit szem előtt tartva, emberi méltóságuk
tiszteletben tartásával, az értékelés személyes jellegének
figyelembevételével szükséges megvalósítani.

A tanulók tanulási tevékenységekben való aktív részvétele
kulcsfontosságú, ezért ennek előmozdítása érdekében a pedagógusoknak
mindvégig a tevékenységközpontú tanulásszervezési formákat kívánatos
előnyben részesíteniük. A tanulás társas természetéből adódó előnyök, a
differenciált egyéni munka adta lehetőségek kihasználása, valamint a
párban vagy csoportban végzett kutatásalapú, felfedező, tevékeny és jól
szervezett, együttműködő tanulás támogatása szintén hozzájárul a
korszerű tanulási környezet megteremtéséhez.

Fontos, hogy a tanulóval szemben támasztott elvárások egyértelműek
legyenek, az azokhoz igazodó mérési stratégiákkal együtt, és már a
tanulási folyamat elején ismertté váljanak. Az iskolai légkör bizalmi
jellege elsődleges feltétele annak, hogy a tanulási problémákra és a
személyes nehézségekre időben fény derüljön. Ennek a bizalomnak a
megteremtése és fenntartása minden intézményvezető és pedagógus állandó
felelőssége.

Az aktív tanulási alapelvek szerint szerveződő, több tantárgy, tanulási
terület ismereteinek integrálását igénylő témákat, jelenségeket
feldolgozó tanórák, foglalkozások, témanapok, témahetek, tematikus hetek
és projektek alkalmazása segíti a tanulót a jelenségek megértésében, a
problémák komplex módon történő vizsgálatában.

*Egyénre szabott tanulási lehetőségek*

Az iskola a tanulók tanulmányi előmenetelét a képességeiknek megfelelő,
egyénre szabott tanulási lehetőségek biztosításával tudja a
leghatékonyabban támogatni.

Fontos alapelv, hogy a tanulók közti különbözőségeket az iskola a
különféle környezeti feltételek és az egyénenként eltérő idegrendszeri
érés, valamint az egyéni képességek kölcsönhatása eredményének tekintse.
E szemlélet gyakorlatban történő alkalmazásához olyan fejlesztési
célokat kell kijelölni, amelyek nemcsak a tanulótól várnak illeszkedést
a tanulási környezethez, hanem a tanulási környezettől is alkalmazkodást
igényelnek a tanuló egyedi jellemzőihez. Ennek értelmében a
képességtartományok mindkét határán − tehetség és fejlődési késés,
fejlődési zavar, esetleg ezek együttes megléte − kihívást jelentő
feladatok megtervezése kívánatos. Különösen fontos, hogy az iskola
biztosítsa a tanulás egyéni lehetőségeit és a személyre szabott
nevelés-oktatás során megszerezhető tanulási tapasztalatokat, enyhítse a
hátrányok hatásait, optimális esetben képes legyen kiküszöbölni azokat.
Ezek közül kiemelten fontos a családi és településszerkezeti
hátrányokból eredő, az eltérő kulturális és nyelvi elsajátítási
lehetőségekhez köthető, valamint a különleges bánásmódot igénylő
tanulókhoz illeszkedő fejlesztő tevékenység.

Minden gyermek, tanuló fejlődésében lényeges szerepet játszik a
pedagógus fejlesztő tevékenysége. Különösen igaz ez a kiemelkedően
kreatív, egy vagy több területen tehetséges, a hátrányos és halmozottan
hátrányos helyzetű, a sajátos nevelési igényű (SNI), valamint a szakmai
besorolásukat tekintve heterogén, az ok-okozati összefüggéseket tekintve
fel nem tárt, ám tanulási-tanítási szempontból kihívást jelentő,
beilleszkedési, tanulási és magatartási nehézséggel (BTMN) küzdő tanulók
fejlesztésének területén. Ennek a feladatnak az ellátásában
felértékelődik a segítő szakterületek (iskolapszichológia,
gyógypedagógia, fejlesztő pedagógia) szerepe, valamint a különböző
tantárgyakat tanító pedagógusok tudásmegosztásra épülő, egymást segítő
szakmai tevékenysége.

A különleges bánásmódot igénylő tanulók esetében a közös
felelősségvállalásnak lényeges szerepe van a nevelés sikerességében. Ez
nem csak az együttnevelést megvalósító iskolában tanító pedagógus
kizárólagos felelőssége. A pedagógus a tanulást-tanítást speciális
szakmai kompetenciák alapján segíteni tudó különböző szakemberekkel
együtt (például gyógypedagógus, gyógytestnevelő, iskolapszichológus,
szociális munkás, fejlesztő pedagógus, tehetséggondozó pedagógus,
gyermekvédelmi jelzőrendszer szakembere) kialakított foglalkozások
keretében, valamint a szülők és a tanuló folyamatos bevonásával, a
pedagógiai tevékenység részeként elismert konzultációs tevékenységet
folytatva végzi pedagógiai munkáját.

Előremutató folyamatként értékelhető a teamtanításnak olyan alkalmazása,
amely a több tantárgy ismereteit integráló témákat feldolgozó
foglalkozásokat közös tanítás keretében valósítja meg, az együttnevelést
megvalósító iskolában pedig a tanulót tanító pedagógus és a
gyógypedagógus közösen tervezett tanulási-tanítási programjára, a közös
tanításra, valamint az e tevékenységet követő közös értékelésre épül.

A differenciálás speciális megvalósulása lehet az együttnevelés során a
habilitációs, rehabilitációs szemlélet érvényesülése. A
hátránykompenzáció biztosítása érdekében (SNI, BTMN, HH, HHH) a tanuló
szükségleteihez, képességeihez, készségeihez illeszkedő módszertani
eljárások (eszközök, módszerek, terápiák, a tanulást-tanítást segítő
speciális eszközök, a gyógypedagógus módszertani iránymutatásainak
beépítése, egyéni fejlesztési terv készítése és rendszeres ellenőrzése)
alkalmazása szükséges a különböző pedagógiai színtereken. A tanulói
szükségletek ismeretében az egységes gyógypedagógiai módszertani
intézmények, a pedagógiai szakszolgálati, illetve pedagógiai-szakmai
szolgáltatást nyújtó intézmények, valamint az utazó gyógypedagógiai
hálózatok működtetésére kijelölt intézmények és más szakemberek
szolgáltatásainak igénybevételével egészítendő ki a tanulók és
pedagógusok szakmai támogatása. Ez elősegíti az integrációt, illetve
ennek magasabb szintjét, az inklúziót is annak érdekében, hogy a Nat-ban
foglalt nevelési -- oktatási alapelvek és célok minden tanuló esetében
megvalósulhassanak. A tehetséges tanulók tekintetében ezek a szempontok
kiemelten fontosak lehetnek, mivel az ő esetükben a tanulási fókuszt és
az érdeklődést jellemzően a megfelelően motiváló feladatokkal lehet
fenntartani. A tehetséggondozást valamennyi nevelési-oktatási
szakaszban, minden neveléssel, oktatással foglalkozó intézménynek
alkalmaznia kell, és a pedagógiai programjába szükséges beillesztenie.

A különleges bánásmódot igénylő tanulók integrációja és hatékony
együttnevelése olyan pedagógusokat kíván, akik rendelkeznek az ehhez
szükséges szemléletmóddal és kompetenciákkal.

A technológiai fejlődés nyújtotta lehetőségek alkalmazása sokféle
módszertani lehetőséget biztosítva segíti a tanulás-tanítás folyamatát.
A XXI. századi tanulási környezet nélkülözhetetlen elemét képezi az
iskolai tanuláshoz kapcsolódó digitális technológiával támogatott
oktatási módszerek sokfélesége, ezért különösen fontos, hogy a
pedagógusok ismerjék és alkalmazzák azokat. Olyan tanulási folyamatra
adnak lehetőséget, amely nemcsak a pedagógus-diák együttműködést, hanem
ennek következtében a hagyományos tanulási folyamatot is jelentősen
megváltoztathatja, ennek következtében új típusú szerepben jelenik meg a
folyamatban mind a tanuló, mind pedig a pedagógus. Ennek gyakorlati
leképezése azonban csak úgy valósulhat meg hatékonyan, ha ennek az
intézmények pedagógiai gyakorlatában és szemléletmódjában is elfogadott,
elismert helye és szerepe lesz.

**Képesség-kibontakoztatás támogatása**

A tanulói teljesítmény értékelésének egyik célja, hogy segítse a tanuló
és a szülő objektív tájékoztatását, továbbá hozzájáruljon ahhoz, hogy a
pedagógus folyamatosan meggyőződjön a tanulási folyamat hatékonyságáról,
lehetőséget biztosítson a pedagógiai munka nyomon követésére, és az
újratervezésére, a célok újra definiálására és továbbiak
meghatározására, amennyiben ezt a tanulók fejlődése megkívánja. A
tanulást és annak eredményességét befolyásoló pedagógiai tevékenység
során végzett értékelésnek adatokra és tényekre kell támaszkodnia. Az
erre alapozott értékelés segíti a tanulót további tanulási módszereinek,
technikáiknak meghatározásában.

Az értékelési folyamatokat megalapozó tervező munka figyelembe veszi a
tanuló előzetes tudását, aktuális fejlettségi szintjét, egyéni fejlődési
lehetőségeit, életkori sajátosságait, az értékelés
személyiségfejlődésére gyakorolt hatását és a pedagógiai célokat. Ennek
érdekében a kiinduló állapot értékelése (diagnosztikus mérés) egy
tanítási óra, tanulási egység, téma vagy program megkezdése előtt
végzett adatgyűjtés. A diagnosztikus értékelés kiterjedhet a tanulók
meglévő tartalmi tudására, aktuális készség- és képességfejlődési
szintjére, hozzáállására, viszonyulására. Az értékelés során figyelembe
kell venni a tanuló életkori sajátosságait és a tanulás korábbi és
aktuális környezeti tényezőiről rendelkezésre álló információkat,
továbbá a pedagógiai célokat. Az eredmények visszajelzésével a pedagógus
útmutatást tud adni a tanulónak a tanulást várhatóan leghatékonyabban
segítő tanulási módokról.

A tanulás folyamatában több alkalommal, tájékozódó jelleggel végzett
információgyűjtés, a fejlesztő, tanulást segítő értékelés és ennek
visszajelzése akkor éri el a kívánt hatást, ha az a tanuló számára az
értékelést követően rövid időn belül megismerhető. A tanulási folyamat
rendszeres értékelése és visszajelzése teszi lehetővé a tanuló
fejlődésének folyamatos nyomon követését.

Mindkét értékelési típus (a kiinduló állapot értékelése és a fejlesztő,
segítő támogató értékelés) önfejlesztésre és kitartásra ösztönzi a
tanulót fejlődésének, valamint tudásának gyakori, interaktív módon
történő visszajelzésével. Célja a tanuló erősségeinek és hiányosságainak
felmérése, valamint az éppen kihívást jelentő célok meghatározása, és
ezzel a nevelési-oktatási gyakorlatnak a célok eléréséhez történő
igazítása. Az összegző értékelés célja annak megállapítása, hogy a
tanulók tudásának, ezen belül a stabil ismeretek kialakításának és a
készségek elsajátításának szintje milyen mértékben felel meg a célként
kitűzött tanulási eredményeknek. Az összegző értékelés minősítő jellegű,
de lehet részletes szöveges értékelés is, amely rendelkezik a fejlesztő,
tanulástámogató értékelés jellemzőivel. Az első évfolyam végén a tanuló
összegző értékelése szöveges formában történik.

Az első nevelési-oktatási szakasz fő feladatainak megvalósítását
leghatékonyabban a fejlesztő, tanulást támogató értékelés szolgálja,
összekapcsolva a diagnosztikus mérésekkel, amelyek segítik a tanulók
hatékony fejlesztését, az aktuális pedagógiai tevékenység
meghatározását, szükség esetén felülvizsgálatát. Az iskolakezdést követő
első félévet minden szempontból bevezető, fejlesztő szakasznak szükséges
tekinteni, ezért a pedagógus ebben az időszakban a szöveges formában
megfogalmazott fejlesztő, tanulást segítő értékeléseket elsősorban
szakmailag megalapozott megfigyeléseire építheti. A szöveges értékelés
lehetőséget biztosít arra, hogy a tanuló és a szülő részére a tantárgyi
előrehaladásról és a kompetenciák fejlődéséről a pedagógus a
tapasztalatain és a követő méréseken alapuló, részletes tájékoztatást
nyújtson.

Az értékelés az 1. évfolyamot követően az iskola pedagógiai programjában
rögzített módon, a jogszabályoknak megfelelően történik.

Az iskola pedagógiai programjában egyértelműen meg kell határozni és
nyilvánossá kell tenni az értékelés minden formájának -- beleértve az
osztályozást is -- szempontjait, az ehhez kapcsolódó eljárásokat. A
nyilvánosságra hozatal időpontját úgy kell megválasztani, hogy a tanulók
és a szülők, gondviselők előzetesen, már az iskolaválasztáskor
megismerhessék azokat. A mérésekkel és az értékeléssel kapcsolatos
feladatokat, adminisztratív tevékenységeket, továbbá a tanulók és a
szülők, gondviselők értesítésével kapcsolatos szabályokat is meg kell
jeleníteni az intézmény pedagógiai programjában."

3\. A Rendelet Melléklet I. Rész I.2.1. pont Az erkölcstan oktatása című
alpontja helyébe a következő rendelkezés lép:

„**Az etika / hit- és erkölcstan tantárgyak oktatására vonatkozó
szabályok**

**A nemzeti köznevelésről szóló törvény rendelkezései szerint az
általános iskola 1-8. évfolyamán az etika tantárgy oktatása kötelező
tanórai keretben történik.** A hit- és erkölcstan oktatás tartalmát az
egyházi jogi személy határozza meg."

4\. A Rendelet Melléklet I. Rész I.2.1. pont Természettudományos nevelés
című alpontja helyébe a következő rendelkezés lép:

„**Természettudományos nevelés**

A természettudományos gondolkodás megalapozása az alapfokú képzés első
szakaszában a magyar nyelv és irodalom tanulási területének
tudásbővítést és olvasásfejlődést segítő olvasmányaiba (1-2. évfolyam)
ágyazva kezdődik, és a Természettudomány és földrajz tanulási terület
környezetismeret (3-4. évfolyam) és természettudomány (5-6. évfolyam)
tantárgyainak keretében folytatódik.

A természettudomány oktatása a 7-8. évfolyamon a biológia, a kémia és a
fizika, valamint hozzájuk kapcsolódva a földrajz tantárgyak keretében
valósul meg. Ezeken az évfolyamokon a diszciplináris tartalmak egy
integrált természettudomány tantárgy részeként is oktathatók. A
gimnáziumokban a 9-12. évfolyamokon a természettudományos oktatás
diszciplináris bontásban valósul meg.

A természettudományos ismeretek és kiemelten a matematikai,
természettudományos, mérnöki-műszaki és informatikai (a továbbiakban:
MTMI) készségek fejlesztése érdekében a gimnáziumban a 11. évfolyamon
azon tanulóknak, akik nem tanulnak emelt óraszámban vagy fakultáción
természettudományos tantárgyat, egy jelenségek vizsgálatán alapuló,
komplex szemléletmóddal oktatott, a természettudományos műveltséget
bővítő tantárgyat kell felvennie, illetve a fenti elvek mentén oktatott
természettudomány integrált, fizika, kémia, biológia, földrajz moduljai
közül az egyiket heti két óra időkeretben."

5\. A Rendelet Melléklet I. Rész I.2.1. pont Az idegennyelv-oktatás című
alpontja helyébe a következő rendelkezés lép:

„**Az idegennyelv-oktatás**

Az első idegen nyelv oktatása legkésőbb az általános iskola 4.
évfolyamán kezdődik. Amennyiben az 1-3. évfolyam idegennyelv-oktatásában
képzett pedagógus alkalmazása megoldható, és az iskola pedagógiai
programja erre lehetőséget ad, az első idegen nyelv oktatása ezen
évfolyamokon is megkezdhető. Az első idegen nyelv megválasztásakor
biztosítani kell, hogy azt a felsőbb évfolyamokon is folyamatosan
tanulhassák. A második idegen nyelv oktatása a 8. évfolyam után
kezdődhet. A tanuló a középiskolai tanulmányai végére képes elérni a KER
szerinti B2 szintet, de legalább a középszintű nyelvi érettségit (B1
szint) teljesíti. A gimnáziumban a második idegen nyelvre meghatározott
órakeret egy részét a vonatkozó szabályozásnak megfelelően fel lehet
használni az első idegen nyelv oktatására, az intézmény pedagógiai
programjában rögzített keretek között. A középiskolákban második idegen
nyelvként szabad választás szerint oktathatók a különböző nyelvek."

6\. A Rendelet Melléklet I. Rész I.2.1. pont Az emelt szintű képzési
forma című alpontja a következő mondattal egészül ki:

„Az emelt szintű oktatás esetében 5. évfolyamtól a Nat-ban meghatározott
órakeret legfeljebb heti két órával megnövelhető."

7\. A Rendelet Melléklet I. Rész I.2.1. pont A szakközépiskolai oktatás
című alpontja helyébe a következő rendelkezés lép:

„**A technikumi és szakgimnáziumi oktatás**

A technikumnak és a szakgimnáziumnak szakmai érettségi végzettséget adó
érettségire, szakirányú felsőfokú iskolai továbbtanulásra, szakirányú
munkába állásra felkészítő, valamint általános műveltséget megalapozó öt
középiskolai évfolyama van, ahol szakmai oktatás is folyik. A képzés
óraterve párhuzamosan biztosítja a felkészülést az érettségi vizsgákra,
valamint a szakmai ismeretek elsajátítását"

8\. A Rendelet Melléklet I. Rész I.2.1. pont A szakiskolai nevelés című
alpontja helyébe a következő rendelkezés lép, és a pont a következő
alpontokkal egészül ki:

„**A szakképző iskolai oktatás**

A szakképző iskolai nevelés-oktatásban a képzési és kimeneti
követelményben, valamint a programtantervben meghatározott időkeretet
kell biztosítani a Nat követelményeinek megvalósítására. A
programtantervek egyrészt a Nat kiemelt fejlesztési területeire,
nevelési céljaira, a kulcskompetenciákra épülnek, másrészt a szakképző
iskola közismereti és szakmai tárgyai együttesét figyelembe véve
érvényesítik a műveltségterületek alapelveit, céljait és fejlesztési
követelményeit.

**A szakiskolai oktatás**

A szakiskola a sajátos nevelési igényük miatt a többi tanulóval együtt
nem nevelhető - oktatható tanulót készíti fel a szakma elsajátítására, a
szakmai vizsgára. A közismereti oktatás a szakiskolában a sajátos
nevelési igény típusa szerinti, a sajátos nevelési igényű tanulók
iskolai oktatásának irányelveit figyelembe vevő, ahhoz igazodó
közismereti kerettantervek alapján folyik. A szakmai oktatás -- a
sajátos nevelési igény típusától, jellemzőitől függően -- általános
szakképzési programtantervek vagy speciális kerettantervek
alkalmazásával valósulhat meg. A szakiskolák kiemelt feladata a
választott szakma, részszakma vagy szakképesítés jellegzetességeinek és
a tanuló egyéni erősségeinek való megfelelés együttes figyelembevétele a
tanulás-tanítás folyamatában.

**A készségfejlesztő iskolai oktatás**

A készségfejlesztő iskolák alapvető feladata az enyhe és középsúlyos
értelmi fogyatékos tanuló életkezdésre történő felkészítésének
elősegítése, a munkába állást lehetővé tevő, egyszerű betanulást igénylő
munkafolyamatok elsajátításának biztosítása, továbbá a szakképzésben
részt venni nem tudó, enyhe értelmi fogyatékos tanulóban a munkába
álláshoz és az életkezdéshez szükséges készségek kialakítása. A
közismereti oktatás a készségfejlesztő iskolában a sajátos nevelési
igény jellegéhez igazodó közismereti kerettanterv alapján folyik. A
készségfejlesztő iskola gyakorlati évfolyamainak képzését a
készségfejlesztő kerettantervek határozzák meg."

9\. A Rendelet Melléklet II. Rész II.1. pontja helyébe a következő
rendelkezés lép:

„II.1. A KULCSKOMPETENCIÁK

A Nat az Európai Unió által ajánlott kulcskompetenciákból kiindulva,
arra építve, de a hazai sajátosságokat figyelembe véve az alábbiak
szerint határozza meg a tanulási területeken átívelő általános
kompetenciákat, továbbá azokat, amelyek jellemzői, hogy egyetlen
tanulási területhez sem köthetők kizárólagosan, hanem változó mértékben
és összetételben épülnek a megszerzett tudásra, fejlődnek a
tanulási-tanítási folyamatban.

1\. A tanulás kompetenciái

2\. A kommunikációs kompetenciák (anyanyelvi és idegen nyelvi)

3\. A digitális kompetenciák

4\. A matematikai, gondolkodási kompetenciák

5\. A személyes és társas kapcsolati kompetenciák

6\. A kreativitás, a kreatív alkotás, önkifejezés és kulturális
tudatosság kompetenciái

7\. Munkavállalói, innovációs és vállalkozói kompetenciák"

10\. A Rendelet Melléklet II. Rész II.2.1. pontja a következő
rendelkezéssel egészül ki:

„**II.2.1.1. táblázat:** Alapóraszámok tanulási területenként kétéves
bontásban

+--------+--------+--------+--------+--------+--------+--------+--------+
|        | A      | B      | C      | D      | E      | F      | G      |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 1      | Tanulá | Nevelé |        |        |        |        |        |
|        | si     | si-okt |        |        |        |        |        |
|        | terüle | atási  |        |        |        |        |        |
|        | tek    | szakas |        |        |        |        |        |
|        | *(tant | zok    |        |        |        |        |        |
|        | árgyak | évfoly |        |        |        |        |        |
|        | )*     | amai   |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 2      |        | 1--2.  | 3--4.  | 5--6.  | 7--8.  | 9-10.  | 11--12 |
|        |        |        |        |        |        |        | .      |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 3      | Magyar | 14     | 10     | 8      | 6      | 7      | 8      |
|        | nyelv  |        |        |        |        |        |        |
|        | és     |        |        |        |        |        |        |
|        | irodal |        |        |        |        |        |        |
|        | om     |        |        |        |        |        |        |
|        | (iroda |        |        |        |        |        |        |
|        | lom,   |        |        |        |        |        |        |
|        | magyar |        |        |        |        |        |        |
|        | nyelv) |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 4      | Matema | 8      | 8      | 8      | 6      | 6      | 6      |
|        | tika   |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (m |        |        |        |        |        |        |
|        | atemat |        |        |        |        |        |        |
|        | ika)   |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 5      | Történ |        |        | 5      | 5      | 4      | 7      |
|        | elem   |        |        |        |        |        |        |
|        | és     |        |        |        |        |        |        |
|        | államp |        |        |        |        |        |        |
|        | olgári |        |        |        |        |        |        |
|        | ismere |        |        |        |        |        |        |
|        | tek    |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (t |        |        |        |        |        |        |
|        | örténe |        |        |        |        |        |        |
|        | lem,   |        |        |        |        |        |        |
|        |     ál |        |        |        |        |        |        |
|        | lampol |        |        |        |        |        |        |
|        | gári   |        |        |        |        |        |        |
|        |     is |        |        |        |        |        |        |
|        | merete |        |        |        |        |        |        |
|        | k,     |        |        |        |        |        |        |
|        |     ho |        |        |        |        |        |        |
|        | n-     |        |        |        |        |        |        |
|        |     és |        |        |        |        |        |        |
|        |     né |        |        |        |        |        |        |
|        | pismer |        |        |        |        |        |        |
|        | et)    |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 6      | Etika  | 2      | 2      | 2      | 2      |        |        |
|        | / hit- |        |        |        |        |        |        |
|        | és     |        |        |        |        |        |        |
|        | erkölc |        |        |        |        |        |        |
|        | stan   |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 7      | Termés | 0      | 2      | 4      | 12     | 16     |        |
|        | zettud |        |        |        |        |        |        |
|        | omány  |        |        |        |        |        |        |
|        | és     |        |        |        |        |        |        |
|        | földra |        |        |        |        |        |        |
|        | jz     |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (k |        |        |        |        |        |        |
|        | örnyez |        |        |        |        |        |        |
|        | etisme |        |        |        |        |        |        |
|        | ret,   |        |        |        |        |        |        |
|        |     te |        |        |        |        |        |        |
|        | rmésze |        |        |        |        |        |        |
|        | ttudom |        |        |        |        |        |        |
|        | ány,   |        |        |        |        |        |        |
|        |     in |        |        |        |        |        |        |
|        | tegrál |        |        |        |        |        |        |
|        | t      |        |        |        |        |        |        |
|        |     te |        |        |        |        |        |        |
|        | rmésze |        |        |        |        |        |        |
|        | ttudom |        |        |        |        |        |        |
|        | ány,   |        |        |        |        |        |        |
|        |     bi |        |        |        |        |        |        |
|        | ológia |        |        |        |        |        |        |
|        | ,      |        |        |        |        |        |        |
|        |     ké |        |        |        |        |        |        |
|        | mia,   |        |        |        |        |        |        |
|        |     fi |        |        |        |        |        |        |
|        | zika,  |        |        |        |        |        |        |
|        |     fö |        |        |        |        |        |        |
|        | ldrajz |        |        |        |        |        |        |
|        | )      |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 8      | Idegen | 0      | 2      | 6      | 6      | 12     | 14     |
|        | nyelv  |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (e |        |        |        |        |        |        |
|        | lső    |        |        |        |        |        |        |
|        |     és |        |        |        |        |        |        |
|        |     má |        |        |        |        |        |        |
|        | sodik  |        |        |        |        |        |        |
|        |     id |        |        |        |        |        |        |
|        | egen   |        |        |        |        |        |        |
|        |     ny |        |        |        |        |        |        |
|        | elv)   |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 9      | Művész | 8      | 7      | 5      | 5      | 4      | 2      |
|        | etek   |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (é |        |        |        |        |        |        |
|        | nek-ze |        |        |        |        |        |        |
|        | ne,    |        |        |        |        |        |        |
|        |     dr |        |        |        |        |        |        |
|        | áma    |        |        |        |        |        |        |
|        |     és |        |        |        |        |        |        |
|        |     sz |        |        |        |        |        |        |
|        | ínház, |        |        |        |        |        |        |
|        |     vi |        |        |        |        |        |        |
|        | zuális |        |        |        |        |        |        |
|        |     ku |        |        |        |        |        |        |
|        | ltúra, |        |        |        |        |        |        |
|        |     mo |        |        |        |        |        |        |
|        | zgókép |        |        |        |        |        |        |
|        | kultúr |        |        |        |        |        |        |
|        | a      |        |        |        |        |        |        |
|        |     és |        |        |        |        |        |        |
|        |     mé |        |        |        |        |        |        |
|        | diaism |        |        |        |        |        |        |
|        | eret)  |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 10     | Techno | 2      | 4      | 4      | 3      | 3      | 2      |
|        | lógia  |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (t |        |        |        |        |        |        |
|        | echnik |        |        |        |        |        |        |
|        | a      |        |        |        |        |        |        |
|        |     és |        |        |        |        |        |        |
|        |     te |        |        |        |        |        |        |
|        | rvezés |        |        |        |        |        |        |
|        | ,      |        |        |        |        |        |        |
|        |     di |        |        |        |        |        |        |
|        | gitáli |        |        |        |        |        |        |
|        | s      |        |        |        |        |        |        |
|        |     ku |        |        |        |        |        |        |
|        | ltúra) |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 11     | Testne | 10     | 10     | 10     | 10     | 10     | 10     |
|        | velés  |        |        |        |        |        |        |
|        | és     |        |        |        |        |        |        |
|        | egészs |        |        |        |        |        |        |
|        | égfejl |        |        |        |        |        |        |
|        | esztés |        |        |        |        |        |        |
|        |        |        |        |        |        |        |        |
|        | -   (t |        |        |        |        |        |        |
|        | estnev |        |        |        |        |        |        |
|        | elés)  |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 12     | Közöss | 0      | 0      | 2      | 2      | 2      | 2      |
|        | égi    |        |        |        |        |        |        |
|        | nevelé |        |        |        |        |        |        |
|        | s      |        |        |        |        |        |        |
|        | (osztá |        |        |        |        |        |        |
|        | lyfőnö |        |        |        |        |        |        |
|        | ki)    |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 13     | Kötele | 44     | 45     | 54     | 57     | 64     | 51     |
|        | ző     |        |        |        |        |        | (+8)   |
|        | alapór |        |        |        |        |        |        |
|        | aszám  |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 14     | Szabad | 4      | 4      | 2      | 3      | 4      | 9      |
|        | on     |        |        |        |        |        |        |
|        | tervez |        |        |        |        |        |        |
|        | hető   |        |        |        |        |        |        |
|        | óraker |        |        |        |        |        |        |
|        | et     |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+
| 15     | Maximá | 48     | 49     | 56     | 60     | 68     | 68     |
|        | lis    |        |        |        |        |        |        |
|        | óraker |        |        |        |        |        |        |
|        | et     |        |        |        |        |        |        |
+--------+--------+--------+--------+--------+--------+--------+--------+

A magyar nyelv és irodalom, a matematika és a történelem és állampolgári
ismeretek műveltségi területekre a 11--12. évfolyamra meghatározott
óraszámok és a Nat tartalma érvényes a szakgimnáziumban és a szakképzési
törvényben meghatározott eltérésekkel a technikumban is.

A hon- és népismeret tantárgy a felső tagozat bármelyik évfolyamán
választható. A hon- és népismeret tantárgy oktatásához a történelem és
állampolgári ismeretek műveltségi terület alapóraszámában a II.2.1.1.
táblázat D:5 mezőjében, az 5--6. évfolyamra meghatározott érték egy
tanórát tartalmaz, amely átcsoportosítható az E:5 mezőben, a 7--8.
évfolyamra vonatkozó alapóraszámba.

A dráma és színház a felső tagozat valamely évfolyamán kötelezően
választandó, valamint a 11. évfolyamon a művészetek tantárgy keretében
önálló tárgyként is választható. A dráma és színház tantárgy oktatásához
a II.2.1.1. Táblázat E:9 mezőjében a 7--8. évfolyamra meghatározott
érték egy tanórát tartalmaz, amely átcsoportosítható a D:9 mezőben, az
5--6. évfolyamra vonatkozó alapóraszámba.

A dráma és színház vagy a mozgóképkultúra és médiaismeret tantárgy a 12.
évfolyamon kötelezően választandó, amelyhez a II.2.1.1. táblázat G:9
mezőjében, a 11--12. évfolyamra meghatározott érték egy tanórát
tartalmaz.

A 11--12. évfolyamokon a kötelező óraszám 59 óra, amelyből mindkét
évfolyamon 4-4 órát az érettségire történő felkészítésre kell fordítani.
A természettudományos ismeretek és kiemelten az MTMI készségek
fejlesztése érdekében a gimnáziumban a 11. évfolyamon azon tanulóknak,
akik nem tanulnak emelt óraszámban vagy fakultáción természettudományos
tantárgyat, egy jelenségek vizsgálatán alapuló, komplex szemléletmóddal
oktatott, a természettudományos műveltséget bővítő integrált
természettudomány tantárgyat vagy a fizika, kémia, biológia, földrajz
tantárgyak egyikét kell tanulniuk heti két óra időkeretben. A választott
tantárgyak ismeretanyaga nem képezi részét e tárgyak középszintű
érettségi követelményeinek, ugyanakkor az érettségi vizsga letételéhez
az itt megszerzett tudás és készségek segítséget jelentenek.

A nemzetiségi nevelést-oktatást folytató iskolában a 8. § (5) bekezdése
alapján biztosított óratöbbletek mellett a szabadon tervezhető órakeret
is felhasználható a nemzetiségi nyelv és irodalom és a nemzetiségi
népismeret tantárgyak oktatására.

**II.2.1.2. táblázat**

A tanulók heti kötelező alapóraszáma és maximális óraszáma
évfolyamonként

  --- ----------------------- ---- ---- ---- ---- ------ ------ ------ ------ ---- ----- ----- -----
      A                       B    C    D    E    F      G      H      I      J    K     L     M
  1                           1.   2.   3.   4.   5.     6.     7.     8.     9.   10.   11.   12.
  2   alapóraszám             22   22   22   23   27\*   26\*   28\*   28\*   32   32    30    29
  3   maximális óraszám\*\*   24   24   24   25   28     28     30     30     34   34    34    34
  --- ----------------------- ---- ---- ---- ---- ------ ------ ------ ------ ---- ----- ----- -----

\* Az 5-8. évfolyam alapóraszámai a hon- és népismeret, valamint a dráma
és színház tantárgyak 1-1 tanórájával növekednek az intézményi döntés
alapján meghatározott évfolyamokon. Az 5-8. évfolyamok esetében a hon-és
népismeret, illetve a dráma és színház tárgyak közül egy évfolyamra
egyszerre csak az egyik tervezhető.

\*\* A tanulóknak ettől magasabb óraszáma csak a két tanítási nyelvű
iskolai oktatásban, a nemzetiségi nevelésben-oktatásban, vagy ha az
iskola a nemzeti köznevelésről szóló törvény 7. § (6) bekezdése szerint
sportiskola feladatait látja el, továbbá ha az iskola emelt szintű
oktatást szervez.

**II.2.1.3. táblázat**

*Ajánlás* az alapóraszámok tanulási területekhez tartozó tantárgyanként,
képzési szakaszok és évfolyamok szerinti arányos tanóraelosztására a 8
évfolyamos általános iskolák és a 4 évfolyamos gimnáziumok számára. Két
évfolyamos ciklusokon belül az ajánlásban megfogalmazott óraszámok
átcsoportosíthatók a helyi tantervben meghatározott módon, azzal a
megkötéssel, hogy az egyes évfolyamok maximális óraszáma nem lehet
magasabb az arra meghatározott legmagasabb óraszámnál. A 11. és a 12.
évfolyamon mind a kötött célú, mind a szabadon tervezhető órakeret
terhére az érettségi tantárgyak közül a tanuló szabadon választhat.

  ---- --------------------------------------------------------- --------------------------------------------- ------------------------------------------------ -------- -------- -------- -------- -------- -------- -------- -------- -------- --------
       A                                                         B                                             C                                                D        E        F        G        H        I        J        K        L        M
  1    Tantárgyak **műveltségi terület** szerinti felosztásban   Alapfokú képzés nevelési-oktatási szakaszai   A gimnáziumi képzés nevelési-oktatási szakasza
  2                                                              *1*                                           *2*                                              *3*      *4*      *5*      *6*      *7*      *8*      *9*      *10*     *11*     *12*
  3    **Magyar nyelv és irodalom**
  4    magyar nyelv és irodalom                                  **7**                                         **7**                                            **5**    **5**    **4**    **4**    **3**    **3**    **3**    **4**    **4**    **4**
  5    **Matematika**
  6    matematika                                                **4**                                         **4**                                            **4**    **4**    **4**    **4**    **3**    **3**    **3**    **3**    **3**    **3**
  7    **Történelem és állampolgári ismeretek**
  8    történelem                                                                                                                                                                 **2**    **2**    **2**    **2**    **2**    **2**    **3**    **3**
  9    állampolgári ismeretek                                                                                                                                                                                **1**                               **1**
  10   hon- és népismeret\*                                                                                                                                                                **1**
  11   **Etika / hit- és erkölcstan**                            **1**                                         **1**                                            **1**    **1**    **1**    **1**    **1**    **1**
  12   **Természettudomány és földrajz**
  13   környezetismeret                                                                                                                                         **1**    **1**
  14   természettudomány\*\*                                                                                                                                                      **2**    **2**    **4**    **5**                      **2**
  15   kémia                                                                                                                                                                                        **1**    **2**    **1**    **2**
  16   fizika                                                                                                                                                                                       **1**    **2**    **2**    **3**
  17   biológia                                                                                                                                                                                     **2**    **1**    **3**    **2**
  18   földrajz                                                                                                                                                                                     **2**    **1**    **2**    **1**
  19   **Idegen nyelv**
  20   első élő idegen nyelv                                                                                                                                             **2**    **3**    **3**    **3**    **3**    **3**    **3**    **4**    **4**
  21   második idegen nyelv                                                                                                                                                                                           **3**    **3**    **3**    **3**
  22   **Művészetek\*\*\*\*\***                                                                                                                                                                                                         **1**
  23   ének-zene                                                 **2**                                         **2**                                            **2**    **2**    **2**    **1**    **1**    **1**    **1**    **1**
  24   vizuális kultúra                                          **2**                                         **2**                                            **2**    **1**    **1**    **1**    **1**    **1**    **1**    **1**
  25   dráma és színház\*                                                                                                                                                                           **1**
  26   mozgóképkultúra és médiaismeret\*                                                                                                                                                                                                         **1**
  27   **Technológia**
  28   technika és tervezés                                      **1**                                         **1**                                            **1**    **1**    **1**    **1**    **1**
  29   digitális kultúra                                                                                                                                        **1**    **1**    **1**    **1**    **1**    **1**    **2**    **1**    **2**
  30   **Testnevelés és egészségfejlesztés**
  31   testnevelés                                               **5**                                         **5**                                            **5**    **5**    **5**    **5**    **5**    **5**    **5**    **5**    **5**    **5**
  32   közösségi nevelés (osztályfőnöki)                                                                                                                                          **1**    **1**    **1**    **1**    **1**    **1**    **1**    **1**
  33   kötött célú órakeret\*\*\*                                **-**                                         **-**                                            **-**    **-**    **-**    **-**    **-**    **-**    **-**    **-**    **4**    **4**
  34   **Kötelező alapóraszám**                                  **22**                                        **22**                                           **22**   **23**   **27**   **26**   **28**   **28**   **32**   **32**   **30**   **29**
  35   Szabadon tervezhető órakeret\*\*\*\*                      **2**                                         **2**                                            **2**    **2**    **1**    **2**    **2**    **2**    2        **2**    **4**    **5**
  36   **Maximális órakeret**                                    **24**                                        **24**                                           **24**   **25**   **28**   **28**   **30**   **30**   **34**   **34**   **34**   **34**
  ---- --------------------------------------------------------- --------------------------------------------- ------------------------------------------------ -------- -------- -------- -------- -------- -------- -------- -------- -------- --------

\* A hon- és népismeret tantárgyat heti 1 órában, intézményi döntés
alapján, az 5--8. évfolyamok egyikén kell tanítani. A dráma és színház
tantárgyat heti 1 órában az 5-8. évfolyamok egyikén kell tanítani. A
dráma és színház tantárgy szervezése megvalósulhat projektnapok, témahét
vagy tematikus hét keretében, továbbá tömbösítve is. A dráma és színház
tantárgy órakeretben történő oktatását csak szakirányú végzettséggel
rendelkező személy végezheti. A dráma és színház vagy a mozgóképkultúra
és médiaismeret tantárgy a 12. évfolyamon kötelezően választandó.

\*\* A természettudomány tantárgyak a 7--8. évfolyamon diszciplináris
bontásban vagy egy integrált természettudomány tantárgy moduljaiként is
oktathatók. Diszciplináris bontás esetében az egyes tantárgyak heti 3-3
órában taníthatók a két évfolyamra vetítve. A fizika, kémia, biológia
tantárgyak óraszámait a kerettantervek szabályozzák. Gimnáziumokban a
9--10. évfolyamon diszciplináris bontásban folyik a természettudományi
tantárgyak tanulása, tanítása. A természettudományi tantárgyak emelt
óraszámban 11--12. évfolyamon folytathatók.

\*\*\* A kötelező érettségi tantárgyakhoz rendelendő órakeretet a
II.2.1.1. táblázat G:13 mezőjében a zárójelben megadott szám jelzi a
kötelező alapóraszámon belül megadva. A 11. és 12. évfolyamon legalább
4-4 tanórát bármely érettségi tantárgy oktatására kell felhasználni. Ha
a tanuló a 11. évfolyamon a természettudomány tantárgyat tanulja, akkor
legalább további 2 órát kell hetente egyéb érettségi tantárgyból
felvennie.

\*\*\*\*A szabadon tervezhető órakeret terhére beépíthetők a helyi
tantervbe azok a tantárgyak, amelyek az oktatásért felelős miniszter
által közzétett kerettantervvel rendelkeznek, mint például a pénzügyi és
vállalkozási ismeretek, honvédelmi ismeretek. Ezen akkreditált
tantárgyak érettségi vizsgatárgyként történő megjelenéséről az érettségi
vizsga vizsgaszabályzatának kiadásáról szóló kormányrendelet
rendelkezhet. A szabadon tervezhető órakeret terhére kerettantervvel
rendelkező tantárgyak tanítása szervezhető. Ez az órakeret lehetővé
teszi az alapóraszámban biztosított tantárgyak óraszámának az iskola
helyi tantervében meghatározandó emelését is. A hon- és népismeret
tantárgy az 5--8. évfolyamok valamelyikén összesen egyéves időtartamban,
heti egy órában a szabadon tervezhető órakeret terhére kötelezően
választandó.

\*\*\*\*\*A művészetre tervezett heti 1 óra a 11. évfolyamon
megvalósulhat a dráma és színház, a vizuális kultúra, az ének-zene,
valamint a mozgóképkultúra és médiaismeret tantárggyal is.

A szakgimnáziumok, technikumok esetében a közismereti tárgyakra
vonatkozó kerettantervek megegyeznek a gimnáziumokra vonatkozókkal."

11\. A Rendelet Melléklet II. Rész II.3. pontja helyébe a következő
rendelkezés lép:

„**II.3. A MŰVELTSÉGI TERÜLETEK ANYAGAI**

*II.3.1.* *Magyar nyelv és irodalom*

***A) ALAPELVEK, CÉLOK***

A Kárpát-medencei magyarság kultúrájának, nemzeti identitásának egyik
legfontosabb alapja az anyanyelve és az ezen a nyelven megszólaló
irodalma. Nyelv és irodalom: nemcsak hagyományt teremtenek, hanem
folyamatos változásukkal jelent és jövőt is alakítanak. A magyar nyelv
és irodalom tantárgyak kiemelten fontos területei a nemzeti öntudatra,
önazonosságtudatra nevelésnek. Nyelvünk, közös történelmünk, keresztény
alapú vallási és művészeti hagyományaink összekötnek bennünket: korokat,
alkotókat, befogadókat és műveket. Egy kulturális hagyományhoz
tartozunk, egy nemzet vagyunk. Ezért a magyar nyelv és irodalom
tantárgyak a Kárpát-medencei magyarság irodalmát, szellemi örökségét
egységesen és egységben kezelik.

Az irodalomtanításban kiemelt szerep jut azoknak az alkotóknak, akik
igazodási pontként erkölcsi magatartásukkal, kiemelkedően magas szintű
életművükkel alapvetően határozták és határozzák meg a magyar
közgondolkodást. Az irodalmi alkotásoknak értékközvetítő funkciójuk van.
A magyar irodalom tantárgy tananyaga olyan normatív értékeket közvetít,
amelyek a társadalom döntő többségének értékvilágát tükrözik.

A magyar irodalom oktatása folyamatosságában, az egyetemes irodalom
pedig szigetszerűen, a legjelentősebb alkotók és alkotások bemutatásával
történik.

Az irodalmi művek nyelvileg megformált esztétikai alkotások, melyek
beágyazódnak a magyar és az európai kultúrába, így egymással is
párbeszédbe tudnak lépni, létrehozva a közös gondolkodást és
motívumkincset. Az irodalom azonban nem csak szöveg, és a nyelv sem
azonosítható csupán szövegalkotó elemeivel és hatásaival. Az irodalmi
alkotások morális, kulturális értékeket örökítenek és teremtenek.
Történelmi, személyes tapasztalatokat, bölcseleti felismeréseket
hagyományoznak. Létük és hatásuk messze meghaladja a kommunikációs
eszköz és a fikciós esztétikai teljesítmény funkcionálisan értelmezett
szerepét. Gondolkodásunk, önkifejezésünk, személyes és nemzeti
identitásunk kialakításának feltételei és eszközei: *„... nemzeti
hagyomány és nemzeti poézis szoros függésben állanak egymással."*
(Kölcsey).

Az irodalmi művek olyan erkölcsi, történelmi, érzelmi konfliktusokkal
szembesítik az olvasót, melyekben saját jelenüket, benne közösségi és
személyes konfliktusaikat is felismerhetik, és amely felismerések a
tanulók morális, esztétikai és érzelmi fejlődésének is eszközei. Az
anyanyelvi kommunikáció fejlettségének meghatározó szerepe van a nyelvi,
a kulturális és a szociális kompetenciák alakításában, fejlesztésében,
az érzelmi nevelésben, a tanulás teljes folyamatában.

A magyar nyelv és irodalom kiemelt szerepet tölt be a tantárgyak
sorában: az olvasottság, a nyelv rendszerszerű ismerete, tudatos
alkalmazása a differenciált szövegértés alapja, az irodalmi szövegek
elemzése a szövegek jelentésszerkezetének megértéséig vezet el, ezek
pedig lehetővé teszik az összetett, elvont gondolkodási műveletek
elsajátítását, majd alkalmazását. Ezáltal a többi tantárgy tanulásának,
később a társadalmi beilleszkedésnek és boldogulásnak is feltételei,
segítői. Az irodalmi művek az egyetemes emberi értékeket és normákat
(közjó -- egyéni boldogság; hazafiság --, individualizmus, igazság,
szépség, jóság, stb.) közvetítik, ezért az irodalom, mint tantárgy
lehetőséget ad a tanulóknak arra, hogy ezeket az értékeket, azok
állandóságát, illetve a koronként bekövetkező átértelmezését
megismerjék.

A sikeres anyanyelvi és irodalmi oktatás kihagyhatatlan szereplője a
tanár. Viselkedése, a szakma iránti elkötelezettsége, személyes
példamutatása önmagában modell a tanulók számára. A tanár tanít, nevel,
fejleszt, irányít és segít. Hagyományt és tudást ad át, segíti a
tanulókat, hogy azokat maguk is felfedezzék, megteremtsék saját kognitív
struktúráikat, elkészítsék önálló olvasataikat. Ne csak befogadók
legyenek, hanem mérlegelő, problémaérzékeny gondolkodású, kreatív
értelmezők is. A műveltség, a strukturált tudás átadása alapvető
feltétele annak, hogy a tanulók megtanuljanak önállóan gondolkodni és
tanulni. Az értékközvetítés pedig elengedhetetlenül szükséges ahhoz,
hogy a mérlegelő gondolkodás képességének segítségével a saját
értékvilágukat megalkossák, és ez számukra intellektuális és emocionális
élményt jelentsen.

A magyar nyelv- és irodalomtanítás egyidejűleg műveltségközvetítést,
kompetencia- és személyiségfejlesztést, morális és érzelmi nevelést is
jelent. A kompetenciafejlesztés az önálló tudás kialakításában, a
közösségbe való beilleszkedésben nyújt segítséget. A morális és érzelmi
nevelés -- a kompetenciák fejlesztésével együtt -- lehetőséget teremt
arra, hogy a tanuló átgondolt ítéleteket alkosson, képes legyen
sokoldalúan megindokolni véleményét vagy éppen változtasson azon, hogy
művelt, kiegyensúlyozott, harmonikus személyiségként szűkebb
közösségének felelős tagja legyen.

A magyar nyelv és irodalom tantárgyak nevelési-oktatási struktúrája
követi a tanulók kognitív, érzelmi és szociális fejlődését. Az első
szakaszban, az 1--4. évfolyamon döntően a jártasságok, készségek,
képességek fejlesztése, a második szakaszban, az 5--8. évfolyamon a
műveltségalapozás a leghangsúlyosabb, a harmadik képzési szakaszban, a
9--12. évfolyamon a műveltségátadás, illetve a továbbtanulásra és a
felnőtt életre való felkészítés kerül középpontba.

A tananyagok ennek megfelelően egészítik ki egymást, a „megtartva
továbbfejleszteni" elv alapján épülnek egymásra. Az egyes témák --
elősegítve a tudás bevésését, a megértés mélyítését --
vissza-visszatérnek, de újabb tartalmakkal, más kérdések középpontba
állításával bővülnek. A magyar nyelv és irodalom oktatásának alapelvei
és követelményei közé tartoznak: a differenciálás és a tehetséggondozás,
a személyes haladást figyelembe vevő foglalkozások, a tanórán kívüli
tevékenységek bevonása a tanulási folyamatba.

A magyartanítás alapelve, hogy nincs kitüntetett didaktikai modell,
tanítási, értékelési mód. A tanár szabad döntése, szakmai felelőssége,
hogy az adott tananyaghoz adekvát módszertani, értékelési módot
válasszon, hogy órái élményt adók legyenek.

Az óraszámok 80%-át a kerettantervben meghatározott törzsanyag
tanítására kell fordítani. Ha a szaktanár úgy ítéli meg, hogy a
törzsanyag elsajátításához tanulóinak az órakeret akár 100%-ára szüksége
van, rendelkezhet ezekkel az órákkal. Ha elegendő a törzsanyag
elsajátításához az órák 80%-a, akkor az órakeret fennmaradó 20%-át
szabadon választott témák feldolgozására fordíthatja a szaktanár, melyek
kiválasztásába bevonhatja a tanulóit. A választást segítő javaslatokat a
részletesen szabályozott kötelező törzsanyag mellett szintén a
kerettanterv tartalmazza. Az 5--6. évfolyamon a tematikus közelítés,
gyengéd átmenet, a 7--8. évfolyamon a kronológia elve érvényesül. A
kronológia elvének figyelembevételével az egyes témakörök sorrendjét a
szaktanár megváltoztathatja.

A magyar nyelv és irodalom tantárgyak kötelező törzsanyagában csak
lezárt, biztosan értékelhető életművek szerepelnek. Ezen felül, a
választható órakeret terhére a tanár szabadon beilleszthet kortárs
alkotókat, műveket a tananyagba.

A magyar nyelv és irodalom tantárgyi követelményei igazodnak a Nat
alapelveihez és céljaihoz. A Nat-ban meghatározott, illetve a
kerettantervben részletesen szabályozott követelmények a középszintű
érettségi vizsgához szükséges szaktárgyi elvárásokat rögzítik. A magyar
nyelv és irodalom tantárgyak tanítása öt nagy szempont köré
csoportosítható: a kompetenciafejlesztés, a műveltségközvetítés, a
személyiségfejlesztés, a morális és az érzelmi nevelés.

> **Célok**

Alapvető cél a Kárpát-medencei magyarság által létrehozott nyelvi,
irodalmi kultúra legkiemelkedőbb alkotásainak megismertetésével olyan
műveltségsztenderd kialakítása a tanulókban, amely biztosítja a nemzeti
kultúra generációkon átívelő megmaradását és fejlődését.

A magyar nyelv és irodalom tanításának kiemelt célja a harmonikus,
sokoldalúan felkészült, olvasó, az anyanyelvüket tudatosan használó,
biztos szövegértéssel, illetve szövegalkotási kompetenciával rendelkező
tanulók képzése, akik a nyelv tudatos és reflektív alkalmazásával
eredményesen kommunikálnak, írásban és szóban is képesek önmagukat
pontosan, az adott helyzetnek, illetve műfajnak megfelelően kifejezni,
képesek a kulturált viselkedésre, nyelvhasználatra. Nyelvi ismereteik és
kompetenciáik lehetővé teszik a mérlegelő gondolkodást, az élethosszig
tartó folyamatos tanulást, mely által boldogulnak a munka világában,
tudnak önállóan és csapatban hatékonyan dolgozni. Nemzetünk kulturális
hagyományait ismerik, értik és tisztelik, kötődnek azokhoz.

A magyar nyelv és irodalom tanításának további céljai:

1. A tanulók szövegértési és szövegalkotási képességeinek folyamatos
    fejlesztése. Ezáltal azonosítani és alkalmazni tudják a verbális és
    non-verbális kommunikáció jeleit, megértik mások véleményét, ki
    tudják fejezni a sajátjukat.

2. A tanulók nyelvi megnyilatkozásai megfeleljenek a magyar
    nyelvhelyesség, illetve helyesírás szabályainak.

3. A hagyományos és digitális szövegfeldolgozások révén fejlődjék a
    tanulók íráskészsége, digitális kompetenciája, ismerjék meg a
    hagyományos és digitális információforrásokat, tanulják meg azok
    kritikus és etikus használatát.

4. A tanulók rendelkezzenek megfelelő retorikai ismeretekkel, tagolt,
    arányos szöveget tudjanak alkotni. Ismerjék az érvek, a cáfolatok
    fajtáit, a nyelvek főbb típusait, az anyanyelvük eredetéről szóló
    tudományos hipotéziseket, bizonyítékokat, a nyelvtörténetünk nagy
    korszakait és fontosabb nyelvemlékeinket. Érettségüknek megfelelő
    szinten tanulják meg nyelvünk földrajzi és társadalmi tagozódását,
    értsék meg, hogy a nyelv folyamatosan változó rendszer, és ezért a
    változásért felelősséggel tartoznak.

5. Tanulmányaik alatt ismerjék meg a magyar irodalom korszakait,
    alkotóit, irodalmunk történetét, az európai irodalom korszakváltást
    hozó nagy szakaszait, alkotásait. A magyar irodalom kiemelkedő
    jelentőségű műveit tanári irányítással, majd önállóan értelmezzék,
    elemezzék. Az irodalmi művek elemzése segíti az összetett
    gondolkodási műveletek kialakítását: elvonatkoztatás, jelentéssíkok
    elkülönítése, elemzés, szintetizálás.

6. Az irodalmi művek befogadása által fejlődjék a tanulók szövegértése,
    szépérzéke, alakuljon ki az irodalomról szóló laikus szaknyelvük.

7. A könyv nélkül megtanulandó művek segítségével fejlődjék a tanulók
    memóriája, előadókészsége. A memorizálás tartós bevésést jelent,
    mely egyszerre gazdagítja ismereteiket és jelent bármikor előhívható
    tudást.

8. A magyar nyelv és irodalom tantárgyak nemzetünk
    gondolkodástörténeti, művészeti hagyományának egy meghatározó
    szeletét ismertetik meg a tanulókkal. Ezért kiemelten fontos cél,
    hogy a tanulók számára ez felfedezés, intellektuális és emocionális
    élmény legyen, olyan hatás, mely gazdagítja műveltségüket, és
    bevonja őket kulturális örökségünkbe. Az irodalomoktatás a fejlett
    érzelmi intelligencia kialakításának egyik legfontosabb eszköze.

9. Cél továbbá, hogy az iskolai tanulmányok végére a már fiatal
    felnőttek készen álljanak ismereteik, műveltségük, elsajátított
    készségeik révén arra, hogy bekapcsolódjanak a munka világába,
    továbbtanuljanak, művelt, nemzetünk iránt elkötelezett emberekké
    váljanak. Olyan személyiségekké, akik tudatos kultúrabefogadók,
    műértők lesznek, képesek az értelmező gondolkodásra, az
    önreflexióra. A szépirodalmi és gyakorlati szövegek, drámajátékok,
    helyzetgyakorlatok segítenek a saját nézőpont kialakításában és más
    nézőpontok megértésében, a problémamegoldás fejlesztésében, valamint
    az empátia kialakításában. Az irodalomoktatás a fejlett érzelmi
    intelligencia kialakításának egyik legfontosabb eszköze. Az európai
    és azon belül a magyar kultúra nagy történeteinek, szövegeinek
    megismerése lehetővé teszi a közösség erkölcsi értékeinek átadását,
    valamint társadalmi, etikai, lélektani kérdések értelmezését.

***A tantárgyak tanításának specifikus jellemzői az 1--4. évfolyamon***

Az alapfokú képzés első nevelési-oktatási szakaszának négy évfolyamán a
legfontosabb cél a beszéd és az anyanyelven történő kifejezőkészség
fejlesztése, az olvasás és az írás stabil megalapozása. A fő hangsúly az
alapkészségek fejlesztésén, az értő olvasás kialakításán, a szövegértés
és -értelmezés megalapozásán van. A sikeres óvoda--iskola átmenet,
gyengéd átmenet érdekében szükséges a nyelvi, a mozgáskoordinációs és
egyéb hátrányok iskolakezdéskor történő felmérése, és a hátrányok
mértéke szerint a kompenzációs és fejlesztő gyakorlatok:
beszédgyakorlatok, téri tájékozódást és mozgáskoordinációt fejlesztő
gyakorlatok beépítése a tanórákba. Az olvasás és az írás sikeres
elsajátításához elengedhetetlen képességek folyamatos fejlesztése és
diagnosztizáló értékelése a köznevelési intézmény feladata.

Az anyanyelvi készségek és képességek, kompetenciák valamennyi tanulási
területen meghatározzák a tanuló későbbi előmenetelét. Ezek elégtelen
szintje az egyes tantárgyakban megmutatkozó alulteljesítésben, valamint
a lemorzsolódásban, a végzettség nélküli iskolaelhagyásban nyilvánulhat
meg. A készségfejlesztésnek és felzárkóztatásnak ugyanakkor összhangban
kell lennie a korosztályt tipikusan jellemző kognitív fejlődési
szinttel, az egyéni képességeknek megfelelő terhelhetőséggel, és a
differenciálást is ennek megfelelően kell tervezni.

Az alapozás első szakasza, amelyben hangsúlyos szerepet kap az olvasás-
és íráskészség, valamint a szövegértés fejlesztése, a második évfolyam
végén zárul. Ebben a szakaszban folyamatos diagnosztizáló és fejlesztő,
tanulást támogató értékeléssel kell követni az eltérő beszédkészséggel,
nyelvi adottságokkal iskolát kezdő tanulók képességeinek alakulását. A
késleltetett írástanítás lehetővé teszi a hosszabb és alaposabb
írás-előkészítést és az ehhez kapcsolódó differenciált fejlesztő
tevékenységeket.

A tanulási terület legfontosabb tanulási eredménye az olvasás és az
írás, mint alaptechnika elsajátítása, valamint a szövegértéshez és
-értelmezéshez szükséges alapkészségek, kompetenciák kialakítása. Az
olvasóvá nevelést támogatja a tanulócsoport érdeklődésének megfelelő
gyermekirodalmi alkotások közös olvasása, élményszerű feldolgozása.

Az anyanyelvi kompetenciák fejlesztése érdekében a nyelvtani szabályok
megismertetése helyett a tanuló kreatív játékos, alkotó jellegű vagy
kísérletező feladatokkal, változatos munkaformákban tapasztalja meg a
nyelv összefüggéseit, működését.

A tantárgyi készségfejlesztés komplex rendszert alkot azokkal a
tantárgyakkal, amelyek a zenei hallást, a ritmuskészséget, a mozgást, a
mozgásügyességet, a manuális készségeket, az idő- és térbeli szekvenciák
azonosítását és követését, a téri-vizuális és időbeli tájékozódást
fejlesztik.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

A magyar nyelv és irodalom tanulási terület két tantárgy
összekapcsolásával jött létre. Nyelvi meghatározottságuk,
tudományterületi összekapcsolódásuk, közös fejlesztési céljaik ellenére
önálló diszciplínák, saját szerkezeti és tartalmi elemekkel. Mindkettő
saját logikája mentén szerveződik, így a kerettantervben is külön
jelennek meg, tananyagtartalmuk, fejlesztési területeik, tanulási
eredményeik szerint.

A felső tagozatos tananyag szervesen kapcsolódik az alsó tagozatban
elsajátítottakhoz, valamint a tanulók életkori sajátosságaihoz. A
képzési szakasz első felében motívumokra épülő témakörök uralják a
tananyagot. Ezek biztosítják a tanulási szakaszok közötti átmenetet, az
alapkompetenciák és a problémamegoldó gondolkodás fejlesztését.

A képzés második szakaszában a fő tananyagszervező elv a kronológia.
Ennek célja, hogy a tanulók megismerjék a magyar és a világirodalom
néhány nagy korszakát, művelődéstörténeti korszakát, az irodalmat a
történelmi-társadalmi folyamatok részeként lássák, és ismereteiket össze
tudják kötni más tantárgyak tananyagtartalmaival. Ebben a képzési
szakaszban válik feladattá az irodalmi műfajok megismerése és a műfaji
sajátosságok felismerése. Ekkor ismerkednek meg az alapvető verstani és
stilisztikai jellegzetességekkel is. Az irodalmi ismeretek elsősorban az
olvasás öröméhez, a különféle befogadási, értelmezési stratégiákhoz, az
irodalmi művek kontextusaihoz vezető utakat mutatják meg, ezzel
előkészítve az irodalmi művek eszme-, stílus- és hatástörténeti,
valamint történelmi nézőpontú értelmezését.

Az anyanyelvi ismeretek és készségek alapvetően a tanuló
nyelvhasználatának fejlesztését szolgálják.

A tanórai és az iskolán kívüli tevékenységeket úgy kell megtervezni,
hogy alkalmazkodjanak a tanuló társadalmi, kulturális és életkori
sajátosságaihoz, és segítsék az adott esetben szükségessé váló
differenciálást (felzárkóztatás, tehetséggondozás). Az iskolai munka
során törekedni kell a módszertani sokszínűségre, az aktív tanulói
tevékenységekre épülő módszerek alkalmazására (gyűjtőmunka, projektek,
kooperatív, együttműködésen alapuló változatos és ötletgazdag írásbeli
és szóbeli feladatok).

A tantárgyak fejlesztési céljai csak úgy valósíthatók meg, ha a tanuló a
magyar órák aktív alakítójává válik, és nem csupán passzív befogadóként
vesz azon részt. A tanulói tevékenységekre és együttműködésre épülő
módszerek erősítik a tanuló problémamegoldó képességét, és képessé
teszik őt arra, hogy összekapcsolja a különböző forrásokból nyert
sokféle információt.

Az 5--8. évfolyamon a tanítás célja az is, hogy megfelelően előkészítse
a középiskolai tanulmányokat. A tervezéskor figyelembe kell venni a
középiskolai felvételi vizsgák elvárásait, azaz a középiskolák ismert
bemeneti követelményeit.

Az értékelési eljárások alkalmazásakor fontos a tanuló teljesítményének
kiegyensúlyozott, szóban és írásban történő megítélése.

***A tantárgy tanításának specifikus jellemzői a 9--12. évfolyamon***

A középiskolai nevelési-oktatási szakaszban a magyartanítás öt elemből
álló célrendszere (kompetenciafejlesztés, műveltségközvetítés,
személyiségfejlesztés, morális és érzelmi nevelés) változatlanul jelen
van. A tanulók felkészültsége, érettsége alapján a megelőző
nevelési-oktatási szakaszhoz képest meghatározóbbá válik a
műveltségközvetítés, az anyanyelvi és az irodalmi ismeretek átadása.
Azonban a műveltségátadás és a kompetenciafejlesztés egymást erősítő
tevékenységek, a műveltségátadással egy időben a tanulói kompetenciákat
továbbra is fejleszteni kell. Ismeretek, műveltség, képességek,
készségek együttese révén válnak a diákok önállóan gondolkodó emberekké,
akik a képzés befejezése után be tudnak illeszkedni a munka világába
vagy felsőfokú intézményben folytatják tanulmányaikat. Olvasó, a
kultúrára nyitott emberekké válnak, képesek lesznek felelős döntéseket
hozni.

Hasonlóan az 5--8. évfolyamhoz, a két tantárgy, azaz a magyar nyelv és
az irodalom egymásra épülnek. A két tantárgy fejlesztési céljai
egybefonódnak, ám szerkezeti és tartalmi elemeikben önállóak. Mindkettő
saját logikája mentén szerveződik, így a kerettantervben is külön
jelennek meg, tananyagtartalmuk, fejlesztési területeik, tanulási
eredményeik szerint.

E nevelési-oktatási szakasz döntő fontosságú feladata a differenciálás
lehetőségének biztosítása, elősegítése, mivel ekkorra alakulhatnak ki a
legnagyobb különbségek egyének, csoportok és intézmények között. Ennek
oka lehet az eltérő motiváció és érdeklődés, a továbbtanulási szándékok
különbsége. A tananyag összeállításakor ezért tekintettel kell lenni a
különböző tanulócsoportok igényeire, képességeire, céljaira. A
tanulócsoport adottságainak ismeretében lehetőséget kell biztosítani a
felzárkóztatásra és a tehetséggondozásra.

A magyar nyelv és irodalom általános célrendszerébe középfokon két
gyakorlati feladat illeszkedik. Az egyik az érettségire történő
felkészülés, ami a középiskola utolsó két évében kiemelt hangsúlyt kap,
ezért ebben a szakaszban a Nat-on kívül az érettségi vizsgakövetelmény
is fontos szabályozó dokumentum. A másik fontos feladat a munka
világához és az önálló felnőtt élethez kapcsolódó anyanyelvi
kompetenciák fejlesztése, különös tekintettel a szövegalkotási,
szövegértési képességekre.

Az óraszámok 80%-át a kerettantervben meghatározott törzsanyag
tanítására kell fordítani. Ha a szaktanár úgy ítéli meg, hogy a
törzsanyag elsajátításához tanulóinak az órakeret akár 100%-ára szüksége
van, rendelkezhet ezekkel az órákkal. Ha elegendő a törzsanyag
elsajátításához az órák 80%-a, akkor az órakeret fennmaradó 20%-át
szabadon választott témák feldolgozására fordíthatja a szaktanár, melyek
kiválasztásába bevonhatja a tanulóit. A választást segítő javaslatokat a
részletesen szabályozott kötelező törzsanyag mellett szintén a
kerettanterv tartalmazza.

Az összetett tantárgyi célrendszer megvalósításához szükséges a
tudatosan választott, változatos és sokszínű tanulói tevékenységre
ösztönző módszerek alkalmazása. A módszerek kiválasztásakor a
tanulócsoport sajátosságait úgy kell figyelembe venni, hogy azok
segítsék a differenciálást és az órák élményt adók legyenek. A tanulói
tevékenységekre és együttműködésre épülő módszerek erősítik a tanuló
problémamegoldó képességét, és képessé teszi őt arra, hogy
összekapcsolja, együtt értelmezze a különböző forrásokból nyert sokféle
információt.

Az értékelési eljárások alkalmazásakor fontos a tanuló teljesítményének
kiegyensúlyozott, szóban és írásban történő megítélése.

***B) FŐ TÉMAKÖRÖK***

***Fő témakörök az 1--4. évfolyamon***

1. Beszéd és kommunikáció

2. Olvasás, szövegértés

3. Írás, helyesírás

4. Nyelvi tudatosság, anyanyelvi ismeretek

***Fő témakörök az 5--8. Évfolyamon***

MAGYAR NYELV

1. A kommunikáció alapjai

2. Szövegértés és szövegalkotás a gyakorlatban

3. Állandósult szókapcsolatok, közmondások, szóláshasonlatok,
    szállóigék

4. Helyesírás, nyelvhelyesség

5. A nyelvi szintek: hangok, szóelemek, szavak, szószerkezetek, mondat,
    szöveg

6. A szóalkotás módjai

7. Nyelvtörténet, nyelvrokonság; a legújabb kutatások eredményei

8. A magyar nyelv földrajzi, társadalmi változatai

9. A könyv- és könyvtárhasználat, a kultúra helyszínei

IRODALOM

1. Család, otthon, nemzet -- mese, monda, mítosz

2. Szülőföld, táj

3. Szeretet, hazaszeretet, szerelem

4. Hősök az irodalomban

5. A 19. századi magyar irodalom elbeszélő költeményei: Petőfi Sándor:
    János vitéz, Arany János: Toldi

6. Prózai nagyepika: Molnár Ferenc: A Pál utcai fiúk és Gárdonyi Géza:
    Egri csillagok

7. A kerettantervben ajánlottak alapján egy szabadon választott mese
    vagy ifjúsági regény a magyar irodalomból

8. A kerettantervben ajánlottak alapján egy szabadon választott
    ifjúsági regény a világirodalomból

9. Szabadon választott ifjúsági regény a magyar vagy a világirodalomból

10. Korok és portrék: szabadság, szerelem

11. Kárpát-medencei irodalmunk a 20. században

12. A 20. századi történelem az irodalomban

13. Szórakoztató irodalom

***Fő témakörök a 9--12. Évfolyamon***

MAGYAR NYELV

1. A kommunikáció -- fogalma, típusai, zavarai, eszközei; a digitális
    kommunikáció; a munka világa

2. Általános nyelvi ismeretek: a nyelv és a nyelvi rendszer -- a nyelv
    szerkezeti jellemzői, a magyar és idegen nyelvek

3. A szöveg -- fogalma, szerkezete, típusai; a szövegkohézió, a
    szövegértelem összetevői, a szövegalkotás

4. Stilisztika -- stílusrétegek, stílushatás, stíluseszközök (szóképek,
    zeneiség stb.)

5. Jelentéstan -- jel és jelentés

6. A nyelv változása -- nyelvjárások, nyelvi tervezés, nyelvi norma

7. Retorika -- a beszédfajták, a beszéd felépítése, az érvtípusok, a
    hatékony tárgyalástechnika

8. Pragmatika -- a megnyilatkozás fogalma, társalgási forduló,
    beszédaktus, együttműködési elv

9. Nyelvtörténet -- nyelvrokonság, nyelvemlékek

10. Szótárhasználat

IRODALOM

1. Bevezetés az irodalomba, az irodalom és hatása

2. Az irodalom ősi formái: mágia, mítosz, mitológia

3. A görög és római irodalom

4. A Biblia világa és hatása

5. Portrék, életművek, metszetek, szemelvények a magyar és
    világirodalomból a középkortól napjainkig -- művelődéstörténeti
    korok, korstílusok, stíluskorszakok alapján

6. A XX. századi történelem az irodalomban

7. Az irodalom határterületei

***C) TANULÁSI EREDMÉNYEK***

***Átfogó célként kitűzött, valamint a fejlesztési területekhez
kapcsolódó tanulási eredmények (általános követelmények) az 1--4.
évfolyamon***

A nevelési-oktatási szakasz végére a tanuló:

1. az életkorának és egyéni adottságainak megfelelő, hallott és
    olvasott szövegeket megérti;

2. felkészülés után tagolt szöveget érthetően és pontosan olvas
    hangosan;

3. életkorának megfelelően és adottságaihoz mérten kifejezően,
    érthetően, az élethelyzethez igazodva kommunikál;

4. az életkorának és egyéni képességeinek megfelelően alkot szövegeket
    szóban és írásban;

5. segítséggel egyéni érdeklődésének megfelelő olvasmányt választ,
    amelyről beszámol;

6. érdeklődésének megfelelően, hagyományos és digitális szövegek által
    bővíti ismereteit;

7. megfogalmazza saját álláspontját, véleményét;

8. egyéni sajátosságaihoz mérten törekszik a rendezett írásképre,
    esztétikus füzetvezetésre;

9. a tanult nyelvi, nyelvtani, helyesírási ismereteket képességeihez
    mérten alkalmazza;

10. élményeket és tapasztalatokat szerez változatos irodalmi szövegek
    megismerésével, olvasásával.

SZÖVEGÉRTÉS

OLVASÁSI KÉSZSÉGET MEGALAPOZÓ KÉPESSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. részt vesz a testséma-tudatosságot fejlesztő tevékenységekben
    (szem-kéz koordináció, térérzékelés, irányok, arányok, jobb-bal
    oldal összehangolása, testrészek tudatosítása) és érzékelő
    játékokban;

2. megérti és használja a tér- és időbeli tájékozódáshoz szükséges
    szókincset;

3. észleli, illetve megérti a nyelv alkotóelemeit, hangot, betűt,
    szótagot, szót, mondatot, szöveget, és azokra válaszokat fogalmaz
    meg;

4. beszédlégzése és artikulációja megfelelő; figyelmet fordít a hangok
    időtartamának helyes ejtésére, a beszéd helyes ritmusára,
    hangsúlyára, tempójára, az élethelyzetnek megfelelő
    hangerőválasztásra;

5. a szavakat hangokra, szótagokra bontja;

6. hangokból, szótagokból szavakat épít;

7. biztosan ismeri az olvasás jelrendszerét.

SZÓKINCSFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri, értelmezi a szövegben a számára ismeretlen szavakat,
    kifejezéseket; digitális forrásokat is használ;

2. egyszerű magyarázat, szemléltetés (szóbeli, képi, dramatikus
    tevékenység) alapján megérti az új kifejezés jelentését;

3. a megismert szavakat, kifejezéseket a nyelvi fejlettségi szintjén
    alkalmazza;

4. használ életkorának megfelelő digitális és hagyományos szótárakat.

SZÖVEGÉRTÉS, OLVASÁSI STRATÉGIÁK

A nevelési-oktatási szakasz végére a tanuló:

1. adottságaihoz mérten, életkorának megfelelően szöveget hangos vagy
    néma olvasás útján megért;

2. részt vesz népmesék és műmesék, regék, mondák, történetek közös
    olvasásában és feldolgozásában;

3. rövid meséket közösen olvas, megért, feldolgoz;

4. néma olvasás útján megérti az írott utasításokat, közléseket,
    kérdéseket, azokra adekvát módon reflektál;

5. megérti a közösen olvasott rövid szövegeket, részt vesz azok
    olvasásában, feldolgozásában;

6. önállóan, képek, grafikai szervezők (kerettörténet, történettérkép,
    mesetáblázat, karakter-térkép, történetpiramis stb.) segítségével
    vagy tanítói segédlettel a szöveg terjedelmétől függően
    összefoglalja a történetet;

7. értő figyelemmel követi a tanító, illetve társai felolvasását;

8. felkészülés után tagolt szöveget érthetően olvas hangosan;

9. a szöveg megértését igazoló feladatokat végez;

10. önállóan, képek vagy tanítói segítség alapján a szöveg terjedelmétől
    függően kiemeli annak lényeges elemeit, összefoglalja azt;

11. alkalmaz alapvető olvasási stratégiákat;

12. az olvasott szöveghez illusztrációt készít, a hiányos illusztrációt
    kiegészíti, vagy a meglévőt társítja a szöveggel;

13. az olvasott szövegekben kulcsszavakat azonosít, a főbb szerkezeti
    egységeket önállóan vagy segítséggel elkülöníti;

14. egyszerű, játékos formában megismerkedik a szövegek különböző
    modalitásával, médiumok szövegalkotó sajátosságainak alapjaival.

HALLÁS UTÁNI MEGÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a szóbeli utasításokat, kérdéseket, az adottságainak és
    életkorának megfelelő szöveg tartalmát;

2. mozgósítja a hallott szöveg tartalmával kapcsolatos ismereteit,
    élményeit, tapasztalatait, és összekapcsolja azokat;

3. megérti az életkorának megfelelő nyelvi és nem nyelvi üzeneteket, és
    azokra a kommunikációs helyzetnek megfelelően reflektál.

SZÖVEGALKOTÁS

ÍRÁSKÉSZSÉGET MEGALAPOZÓ KÉPESSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. részt vesz nagymozgást és finommotorikát fejlesztő tevékenységekben,
    érzékelő játékokban;

2. tér- és síkbeli tájékozódást fejlesztő feladatokat megold;

3. saját tempójában elsajátítja az anyanyelvi írás jelrendszerét;

4. szavakat, szószerkezeteket, 3-4 szavas mondatokat leír megfigyelés,
    illetve diktálás alapján;

5. az egyéni sajátosságaihoz mérten olvashatóan ír, törekszik a
    rendezett írásképre, esztétikus füzetvezetésre.

SZÓBELI SZÖVEGALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. adottságaihoz mérten, életkorának megfelelően érthetően, az
    élethelyzethez igazodva kommunikál;

2. részt vesz a kortársakkal és felnőttekkel való kommunikációban,
    beszélgetésben, vitában, és alkalmazza a megismert kommunikációs
    szabályokat;

3. használja a kapcsolat-felvételi, kapcsolattartási, kapcsolatlezárási
    formákat: köszönés, kérés, megszólítás, kérdezés; testtartás,
    testtávolság, tekintettartás, hangsúly, hanglejtés, hangerő,
    hangszín, megköszönés, elköszönés;

4. élményeiről segítséggel vagy önállóan beszámol;

5. megadott szempontok alapján szóban mondatokat és 3-4 mondatos
    szöveget alkot;

6. bekapcsolódik párbeszédek, dramatikus helyzetgyakorlatok, szituációs
    játékok megalkotásába;

7. a tanult verseket, mondókákat, rövidebb szövegeket szöveghűen,
    érthetően tolmácsolja.

ÍRÁSBELI SZÖVEGALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. a hallás és olvasás alapján megfigyelt szavakat, szószerkezeteket,
    mondatokat önállóan leírja;

2. egyéni képességeinek megfelelően alkot szövegeket írásban;

3. gondolatait, érzelmeit, véleményét a kommunikációs helyzetnek
    megfelelően, néhány mondatban írásban is megfogalmazza;

4. a szövegalkotáskor törekszik a megismert helyesírási szabályok
    alkalmazására, meglévő szókincsének aktivizálására.

KREATÍV ÉS DIGITÁLIS SZÖVEGALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. tanítói segítséggel megadott rímpárokból, különböző témákban 2--4
    soros verset alkot;

2. megadott szempontok alapján rövid mesét ír, kiegészít vagy átalakít;

3. a megismert irodalmi szövegekhez, iskolai eseményekhez plakátot,
    meghívót, saját programjaihoz meghívót készít hagyományosan és
    digitálisan;

4. alapvető hagyományos és digitális kapcsolattartó formákat alkalmaz.

OLVASÓVÁ NEVELÉS

IRODALMI ALKOTÁSOK BEFOGADÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. nyitott az irodalmi művek befogadására;

2. könyvet kölcsönöz a könyvtárból, és azt el is olvassa, élményeit,
    gondolatait megosztja;

3. ajánlással, illetve egyéni érdeklődésének és az életkori
    sajátosságainak megfelelően választott irodalmi alkotást ismer meg;

4. részt vesz az adott közösség kultúrájának megfelelő gyermekirodalmi
    mű közös olvasásában, és nyitott annak befogadására;

5. verbális és vizuális módon vagy dramatikus eszközökkel reflektál a
    szövegre, megfogalmazza a szöveg alapján benne kialakult képet;

6. részt vesz népmesék és műmesék közös olvasásában, feldolgozásában.

HAGYOMÁNYOS ÉS DIGITÁLIS SZÖVEGEK OLVASÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. különböző célú, rövidebb tájékoztató, ismeretterjesztő szövegeket
    olvas hagyományos és digitális felületen;

2. ismer és használ az életkorának megfelelő nyomtatott és digitális
    forrásokat az ismeretei bővítéséhez, rendszerezéséhez.

GONDOLKODÁS, VÉLEMÉNYALKOTÁS

KULTURÁLT VÉLEMÉNYNYILVÁNÍTÁS, VITAKULTÚRA

A nevelési-oktatási szakasz végére a tanuló:

1. a mesék, történetek szereplőinek cselekedeteiről kérdéseket fogalmaz
    meg, véleményt alkot;

2. megfogalmazza, néhány érvvel alátámasztja saját álláspontját;
    meghallgatja társai véleményét;

3. különbséget tesz mesés és valószerű történetek között;

4. megfigyeli és összehasonlítja a történetek tartalmát és a saját
    élethelyzetét;

5. részt vesz dramatikus játékokban.

ÖNÁLLÓ FELADATVÉGZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a feladatvégzéshez szükséges személyes élményeit, előzetes tudását
    felidézi;

2. képzeletét a megértés érdekében mozgósítja;

3. ismer és alkalmaz néhány alapvető tanulási technikát;

4. gyakorolja az ismeretfeldolgozás egyszerű technikáit;

5. információkat, adatokat gyűjt a szövegből, kiemeli a bekezdések
    lényegét; tanítói segítséggel vagy önállóan megfogalmazza azt;

6. bővíti a témáról szerzett ismereteit egyéb források feltárásával,
    gyűjtőmunkával, könyvtárhasználattal, filmek, médiatermékek
    megismerésével;

7. írásbeli munkáját segítséggel vagy önállóan ellenőrzi és javítja.

ANYANYELVI KULTÚRA, ANYANYELVI ISMERETEK

HANGTANI, ALAKTANI ÉS HELYESÍRÁSI TUDATOSSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeli, és tapasztalati úton megkülönbözteti egymástól a
    magánhangzókat és a mássalhangzókat, valamint időtartamukat;

2. különbséget tesz az egyjegyű, a kétjegyű és a háromjegyű betűk
    között;

3. a hangjelölés megismert szabályait jellemzően helyesen alkalmazza a
    tanult szavakban;

4. a mondatot nagybetűvel kezdi, alkalmazza a mondat hanglejtésének, a
    beszélő szándékának megfelelő mondatvégi írásjeleket;

5. biztosan ismeri a kis- és nagybetűs ábécét, azonos és különböző
    betűkkel kezdődő szavakat betűrendbe sorol; a megismert szabályokat
    alkalmazza digitális felületen való kereséskor is;

6. felismeri, jelentésük alapján csoportosítja, és önállóan vagy
    segítséggel helyesen leírja az élőlények, tárgyak, gondolati dolgok
    nevét;

7. a több hasonló élőlény, tárgy nevét kis kezdőbetűvel írja;

8. a személyneveket, állatneveket és a lakóhelyhez kötődő helyneveket
    nagy kezdőbetűvel írja le;

9. törekszik a tanult helyesírási ismeretek alkalmazására.

A NYELVI, NYELVTANI EGYSÉGEK, NYELVI ELEMZŐKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. kérdésre adott válaszában helyesen toldalékolja a szavakat;

2. önállóan felismeri és elkülöníti az egytövű ismert szavakban a
    szótövet és a toldalékot;

3. biztosan szótagol, alkalmazza az elválasztás szabályait;

4. helyesen alkalmazza a szóbeli és írásbeli szövegalkotásában az idő
    kifejezésének nyelvi eszközeit;

5. a kiejtéssel megegyező rövid szavak leírásában követi a helyesírás
    szabályait;

6. a kiejtéstől eltérő ismert szavakat megfigyelés, szóelemzés
    alkalmazásával megfelelően leírja;

7. ellentétes jelentésű és rokon értelmű kifejezéseket gyűjt, azokat a
    beszédhelyzetnek megfelelően használja az írásbeli és szóbeli
    szövegalkotásban;

8. megkülönbözteti a szavak egyes és többes számát;

9. felismeri és önállóan vagy segítséggel helyesen leírja a
    tulajdonságot kifejező szavakat és azok fokozott alakjait;

10. felismeri, önállóan vagy segítséggel helyesen leírja az ismert
    cselekvést kifejező szavakat;

11. megkülönbözteti a múltban, jelenben és jövőben zajló cselekvéseket,
    történéseket.

ÁLLANDÓSULT SZÓKAPCSOLATOK, SZÓLÁSOK, KÖZMONDÁSOK

A nevelési-oktatási szakasz végére a tanuló:

1. ismer és ért számos egyszerű közmondást és szólást,
    szóláshasonlatot, közmondást, találós kérdést, nyelvtörőt,
    kiszámolót, mondókát;

2. megérti és használja az ismert állandósult szókapcsolatokat;

3. különféle módokon megjeleníti az ismert szólások, közmondások
    jelentését.

IRODALMI KULTÚRA, IRODALMI ISMERETEK

AZ IRODALMI NYELV SAJÁTOSSÁGAI, IRODALMI MŰFAJOK

A nevelési-oktatási szakasz végére a tanuló:

1. élményeket és tapasztalatokat szerez változatos irodalmi
    szövegtípusok és műfajok -- magyar klasszikus, kortárs magyar
    alkotások -- megismerésével;

2. élményeket és tapasztalatokat szerez néhány szövegtípusról és
    műfajról, szépirodalmi és ismeretközlő szövegről;

3. részt vesz különböző műfajú és megjelenésű szövegek olvasásában és
    feldolgozásában;

4. a közös olvasás, szövegfeldolgozás során megismer néhány életkorának
    megfelelő mesét, elbeszélést;

5. megtapasztalja az életkorának, érdeklődésének megfelelő szövegek
    befogadásának és előadásának élményét és örömét;

6. megfigyeli a költői nyelv sajátosságait; élményeit az általa
    választott módon megfogalmazza, megjeleníti;

7. részt vesz ismert szövegek (magyar népi mondókák, kiszámolók,
    nyelvtörők, népdalok, klasszikus és kortárs magyar gyerekversek,
    mesék) mozgásos-játékos feldolgozásában, dramatikus elemekkel
    történő élményszerű megjelenítésében, érzületileg, lelkületileg
    átérzi azokat;

8. segítséggel vagy önállóan előad ritmuskísérettel verseket;

9. olvas és megért rövidebb nép- és műköltészeti alkotásokat, rövidebb
    epikai műveket, verseket;

10. megtapasztalja a vershallgatás, a versmondás, a versolvasás örömét
    és élményét;

11. érzékeli és átéli a vers ritmusát és hangulatát;

12. a versek hangulatát kifejezi különféle érzékszervi tapasztalatok
    segítségével (színek, hangok, illatok, tapintási élmények stb.);

13. a tanító vagy társai segítségével, együttműködésével verssorokat,
    versrészleteket memorizál;

14. felismeri, indokolja a cím és a szöveg közötti összefüggést,
    azonosítja a történetekben, elbeszélő költeményekben a helyszínt, a
    szereplőket, a konfliktust és annak megoldását;

15. szövegszerűen felidézi Kölcsey Ferenc: Himnusz, Vörösmarty Mihály:
    Szózat, Petőfi Sándor: Nemzeti dal című verseinek részleteit;

16. részt vesz rövid mesék, történetek dramatikus, bábos és egyéb
    vizuális, digitális eszközökkel történő megjelenítésében, saját
    gondolkodási és nyelvi szintjén megfogalmazza a szöveg hatására
    benne kialakult képet.

ALKOTÓK, MŰVEK A GYERMEKIRODALOMBÓL

A nevelési-oktatási szakasz végére a tanuló:

1. megismer néhány mesét és történetet a magyar és más népek
    irodalmából;

2. megismer néhány klasszikus verset a magyar irodalomból;

3. élményt és tapasztalatot szerez különböző ritmikájú lírai művek
    megismerésével a kortárs és a klasszikus magyar gyermeklírából és a
    népköltészeti alkotásokból;

4. segítséggel, majd önállóan szöveghűen felidéz néhány könnyen
    tanulható, rövidebb verset, mondókát, versrészletet, prózai és
    dramatikus szöveget, szövegrészletet;

5. a tanult verseket, mondókákat, rövidebb szövegeket szöveghűen,
    érthetően tolmácsolja;

6. részt vesz legalább két hosszabb terjedelmű magyar gyermekirodalmi
    alkotás feldolgozásában.

A KULTURÁLIS EMLÉKEZETHEZ ÉS NEMZETI HAGYOMÁNYHOZ KAPCSOLÓDÓ SZÖVEGEK

A nevelési-oktatási szakasz végére a tanuló:

1. jellemző és ismert részletek alapján azonosítja a nemzeti ünnepeken
    elhangzó költemények részleteit, szerzőjüket megnevezi;

2. megéli és az általa választott formában megjeleníti a közösséghez
    tartozás élményét;

3. megismer a jeles napokhoz, ünnepekhez kapcsolódó szövegeket,
    dalokat, szokásokat, népi gyermekjátékokat;

4. megfigyeli az ünnepek, hagyományok éves körforgását;

5. nyitottá válik a magyarság értékeinek megismerésére, megalapozódik
    nemzeti identitástudata, történelmi szemlélete;

6. képes családjából származó közösségi élményeit megfogalmazni,
    összevetni az iskolai élet adottságaival, a témakört érintő
    beszélgetésekben aktívan részt venni;

7. törekszik a világ tapasztalati úton történő megismerésére,
    értékeinek tudatos megóvására;

8. ismeri a keresztény, keresztyén ünnepköröket (karácsony, húsvét,
    pünkösd), jelképeket, nemzeti és állami ünnepeket (március 15.,
    augusztus 20., október 23.), népszokásokat (Márton-nap, Luca-nap,
    betlehemezés, húsvéti locsolkodás, pünkösdölés);

9. ismerkedik régi magyar mesterségekkel, irodalmi művek olvasásával és
    gyűjtőmunkával.

A KULTÚRA HELYSZÍNEI

A nevelési-oktatási szakasz végére a tanuló:

1. megismer gyermekirodalmi alkotás alapján készült filmet,
    médiaterméket;

2. részt vesz gyerekeknek szóló kiállítások megismerésében,
    alkotásaival hozzájárul létrehozásukhoz;

3. megismer a szűkebb környezetéhez kötődő irodalmi és kulturális
    emlékeket, emlékhelyeket;

4. megismeri saját lakóhelyének irodalmi és kulturális értékeit.

***Átfogó célként kitűzött, valamint a fejlesztési területekhez
kapcsolódó tanulási eredmények (általános követelmények) az 5--8.
évfolyamon***

ANYANYELVI KULTÚRA, ANYANYELVI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. elkülöníti a nyelv szerkezeti egységeit, megnevezi a tanult
    elemeket;

2. ismeri a magyar hangrendszer főbb jellemzőit és a hangok
    kapcsolódási szabályait. Írásban helyesen jelöli;

3. funkciójuk alapján felismeri és megnevezi a szóelemeket és
    szófajokat;

4. a szövegben felismer és funkciójuk alapján azonosít alapvető és
    gyakori szószerkezeteket (alanyos, határozós, jelzős, tárgyas);

5. szerkezetük alapján megkülönbözteti az egyszerű és összetett
    mondatokat;

6. felismeri és elemzi a főbb szóelemek mondat- és szövegbeli szerepét,
    törekszik helyes alkalmazásukra;

7. megfigyeli és elemzi a mondat szórendjét, a szórendi változatok,
    valamint a környező szöveg kölcsönhatását;

8. érti és megnevezi a tanult nyelvi egységek szövegbeli szerepét;

9. felismeri és megnevezi a főbb szóalkotási módokat: szóösszetétel,
    szóképzés, néhány ritkább szóalkotási mód;

10. tanári irányítással, néhány szempont alapján összehasonlítja az
    anyanyelv és a tanult idegen nyelv sajátosságait;

11. megfigyeli, elkülöníti és funkciót társítva értelmezi a
    környezetében előforduló nyelvváltozatokat (nyelvjárások,
    csoportnyelvek, rétegnyelvek);

12. felismeri és megnevezi a nyelvi és nem nyelvi kommunikáció elemeit;

13. használja a digitális kommunikáció eszközeit, megnevezi azok főbb
    jellemző tulajdonságait;

14. megfigyeli és értelmezi a tömegkommunikáció társadalmat befolyásoló
    szerepét;

15. felismeri a kommunikáció főbb zavarait, alkalmaz korrekciós
    lehetőségeket;

16. alkalmazza az általa tanult nyelvi, nyelvtani, helyesírási,
    nyelvhelyességi ismereteket;

17. ismeri és alkalmazza helyesírásunk alapelveit: kiejtés, szóelemzés,
    hagyomány, egyszerűsítés;

18. társai és saját munkájában a tanult formáktól eltérő, gyakran
    előforduló helyesírási hibákat felismeri és javítja;

19. A tanuló etikusan és kritikusan használja a hagyományos papíralapú,
    illetve a világhálón található és egyéb digitális adatbázisokat.

IRODALMI KULTÚRA, IRODALMI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. elolvassa a kötelező olvasmányokat, és saját örömére is olvas;

2. megérti az irodalmi mű szövegszerű és elvont jelentéseit;

3. a tanult fogalmakat használva beszámol a megismert műről;

4. megismeri és elkülöníti a műnemeket, illetve a műnemekhez tartozó
    főbb műfajokat, felismeri és megnevezi azok poétikai és retorikai
    jellemzőit;

5. összekapcsol irodalmi műveket különböző szempontok alapján (téma,
    műfaj, nyelvi kifejezőeszközök);

6. elkülöníti az irodalmi művek főbb szerkezeti egységeit;

7. összehasonlít egy adott irodalmi művet annak adaptációival (film,
    festmény, stb.);

8. felismeri, és bemutatja egy adott műfaj főbb jellemzőit;

9. az irodalmi szövegek befogadása során felismer és értelmez néhány
    alapvető nyelvi-stilisztikai eszközt: szóképek, alakzatok stb.;

10. a megismert epikus művek cselekményét összefoglalja, fordulópontjait
    önállóan ismerteti;

11. elkülöníti és jellemzi a fő- és mellékszereplőket, megkülönbözteti a
    helyszíneket, az idősíkokat, azonosítja az előre- és
    visszautalásokat;

12. felismeri és a szövegből vett példával igazolja az elbeszélő és a
    szereplő nézőpontja közötti eltérést;

13. a szövegből vett idézetekkel támasztja alá, és saját szavaival
    fogalmazza meg a lírai szöveg hangulati jellemzőit, a befogadás
    során keletkezett érzéseit és gondolatait;

14. azonosítja a versben a lírai én különböző megszólalásait, és
    elkülöníti a mű szerkezeti egységeit;

15. felismeri a tanult alapvető rímképleteket (páros rím, keresztrím,
    bokorrím), és azonosítja az olvasott versek ritmikai, hangzásbeli
    hasonlóságait és különbségeit;

16. ismer és felismer néhány alapvető lírai műfajt;

17. szöveghűen, értőn mondja el a memoriterként tanult lírai műveket;

18. drámai műveket olvas és értelmez, előadásokat, feldolgozásokat
    megnéz (pl.: mesedráma, vígjáték, ifjúsági regény adaptációja);

19. a nemzeti hagyomány szempontjából meghatározó néhány mű esetén
    bemutatja a szerzőhöz és a korszakhoz kapcsolódó legfőbb
    jellemzőket.

MŰISMERET

A.

A tanuló olvassa és önálló, illetve osztálytermi munka keretében
feldolgozza a kerettantervben kötelezően és részletesen meghatározott
műveket, illetve műrészleteket a B) pontban felsorolt szerzőktől.

A tanuló olvassa és önálló, illetve osztálytermi munka keretében
feldolgozza a kerettantervben kötelezően és részletesen meghatározott
bibliai történeteket, görög mítoszokat, magyar mítoszokat, hősöket,
mondákat.

A tanuló olvassa és önálló, illetve osztálytermi munka keretében
feldolgozza a kerettantervben felsoroltak közül választható magyar
ifjúsági és meseregényt, illetve világirodalmi ifjúsági regényt.

B. ALKOTÓK

A tanuló olvassa és önálló, illetve osztálytermi munka keretében
feldolgozza az alábbi szerzők kerettantervben részletesen meghatározott
egyes műveit:

Ady Endre, Áprily Lajos, Arany János, Babits Mihály; Balassi Bálint;
Berzsenyi Dániel, Choli Daróczy József; Agatha Christie, Csokonai Vitéz
Mihály, Csukás István, Daniel Defoe, Tonke Dragt, Dsida Jenő, Fazekas
Mihály, Fekete István, Gárdonyi Géza, Herczeg Ferenc, Illyés Gyula,
Janus Pannonius, Jókai Mór, József Attila, Juhász Gyula, Kányádi Sándor,
Karinthy Frigyes; Kittenberger Kálmán, Kós Károly, Kosztolányi Dezső,
Kölcsey Ferenc„Lázár Ervin, Mándy Iván, Márai Sándor, Mikes Kelemen,
Mikszáth Kálmán, Molière, Molnár Ferenc, Móra Ferenc, Móricz Zsigmond,
Nagy László, Nyirő József, George Orwell, Örkény István, Petőfi Sándor,
Pilinszky János; Radnóti Miklós, Reményik Sándor, Antoine Saint-Exupéry,
William Shakespeare, Sütő András, Szabó Lőrinc; Szabó Magda, Széchenyi
Zsigmond, Tamási Áron, Tóth Árpád, Jules Verne, Vörösmarty Mihály,Wass
Albert, Weöres Sándor, Zrínyi Miklós

A memoriterek és a kötelező olvasmányok (C, D) a közös irodalmi
műveltség (kulturális kód) legfontosabb elemeit tartalmazzák.

C. MEMORITEREK

Ady Endre: Őrizem a szemed; Arany János: Rege a csodaszarvasról
(részletek); Családi kör (részletek); A walesi bárdok (részletek); Toldi
részletek; Csokonai Vitéz Mihály: Tartózkodó kérelem, A Reményhez;
József Attila: Születésnapomra, Mama; Kányádi Sándor: Két nyárfa;
Kölcsey Ferenc: Himnusz -- teljes szöveg, Huszt, Emléklapra; Petőfi
Sándor: János vitéz (részletek); Nemzeti dal, Szeptember végén,
Szabadság, szerelem; Radnóti Miklós: Nem tudhatom; Reményik Sándor:
Templom és iskola (részletek); Vörösmarty Mihály: Szózat; Weöres Sándor:
Ó ha cinke volnék.

D. KÖTELEZŐ OLVASMÁNYOK

Arany János: Toldi; Gárdonyi Géza: Egri csillagok; Jókai Mór: A
nagyenyedi két fűzfa; Mikszáth Kálmán: Szent Péter esernyője vagy A két
koldusdiák; A néhai bárány; Molnár Ferenc: A Pál utcai fiúk; Móricz
Zsigmond: Légy jó mindhalálig vagy Pillangó; Hét krajcár; Petőfi Sándor:
János vitéz;

Shakespeare, William: Szentivánéji álom vagy Molière: A képzelt beteg;
Szabó Magda: Abigél.

Szabadon választható magyar ifjúsági vagy meseregény; szabadon
választható világirodalmi ifjúsági regény.

SZÖVEGÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megérti az életkorának megfelelő hallott és olvasott szövegeket;

2. kifejezően tudja olvasni és értelmezni az életkorának megfelelő
    különböző műfajú és megjelenésű szövegeket. A tanuló felismeri és a
    tanár segítségével értelmezi a számára ismeretlen kifejezéseket;

3. felismeri és szükség szerint a tanár segítségével értelmezi a
    szövegben számára ismeretlen kifejezéseket;

4. a lábjegyzetek, a digitális és nyomtatott szótárak használatával
    önállóan értelmezi az olvasott szöveget;

5. a pedagógus irányításával kiválasztja a rendelkezésre álló digitális
    forrásokból a megfelelő információkat;

6. különbséget tesz a jelentésszerkezetben a szó szerinti és
    metaforikus értelmezés között;

7. alkalmazza a különböző olvasási típusokat és szöveg-feldolgozási
    módszereket;

8. összekapcsolja ismereteit a szöveg tartalmával, és reflektál azok
    összefüggéseire;

9. az olvasott szövegeket szerkezeti egységekre tagolja;

10. szóbeli vagy képi módszerekkel megfogalmazza, megjeleníti a szöveg
    alapján kialakult érzéseit, gondolatait;

11. az életkorának megfelelő szöveg alapján jegyzetet, vázlatot készít;

12. a tanulási tevékenységében hagyományos és digitális forrásokat
    használ, ezt mérlegelő gondolkodással és etikusan teszi.

SZÖVEGALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. érthetően, a kommunikációs helyzetnek megfelelően beszél;

2. gondolatait, érzelmeit, véleményét a kommunikációs helyzetnek
    megfelelően, érvekkel alátámasztva fogalmazza meg, és mások
    véleményét is figyelembe veszi;

3. a tanult szövegeket szöveghűen és mások számára követhetően
    tolmácsolja;

4. az általa tanult hagyományos és digitális szövegtípusok megfelelő
    tartalmi és műfaji követelményeinek megfelelően alkot szövegeket;

5. a szövegalkotás során alkalmazza a tanult helyesírási és
    szerkesztési szabályokat, használja a hagyományos és a digitális
    helyesírási szabályzatot és szótárt;

6. az egyéni sajátosságaihoz mérten tagolt, rendezett, áttekinthető
    írásképpel, egyértelmű javításokkal alkot szöveget;

7. tanári segítséggel kreatív szöveget alkot a megismert műhöz
    kapcsolódóan hagyományos és digitális formában;

8. egyszerű rímes és rímtelen verset alkot.

OLVASÓVÁ NEVELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. életkorának megfelelő irodalmi szövegeket olvas;

2. érthetően, kifejezően és pontosan olvas;

3. egy általa elolvasott művet ajánl kortársainak;

4. a tanult szövegeket szöveghűen és mások számára követhetően
    tolmácsolja;

5. megfogalmazza vagy társaival együttműködve drámajátékban megjeleníti
    egy mű megismerése során szerzett tapasztalatait, élményeit.

MÉRLEGELŐ GONDOLKODÁS, VÉLEMÉNYALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. személyes véleményt alakít ki a szövegek és művek által felvetett
    problémákról (pl. döntési helyzetek, motivációk, konfliktusok), és
    véleményét indokolja;

2. megérti mások álláspontját, elfogadja azt, vagy a sajátja mellett
    érveket fogalmaz meg;

3. személyes tapasztalatait összeköti a művekben megismert
    konfliktusokkal, érzelmi állapotokkal;

4. a feladatvégzés során hatékony közös munkára, együttműködésre
    törekszik.

***Átfogó célként kitűzött, valamint a fejlesztési területekhez
kapcsolódó tanulási eredmények (általános követelmények) a 9--12.
évfolyamon***

ANYANYELVI KULTÚRA, ANYANYELVI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. az anyanyelvről szerzett ismereteit alkalmazva képes a
    kommunikációjában a megfelelő nyelvváltozat kiválasztására,
    használatára;

2. felismeri a kommunikáció zavarait, kezelésükre stratégiát dolgoz ki;

3. felismeri és elemzi a tömegkommunikáció befolyásoló eszközeit, azok
    céljait és hatásait;

4. reflektál saját kommunikációjára, szükség esetén változtat azon;

5. ismeri az anyanyelvét, annak szerkezeti felépítését, nyelvhasználata
    tudatos és helyes;

6. ismeri a magyar nyelv hangtanát, alaktanát, szófajtanát,
    mondattanát, ismeri és alkalmazza a tanult elemzési eljárásokat;

7. felismeri és megnevezi a magyar és a tanult idegen nyelv közötti
    hasonlóságokat és eltéréseket;

8. ismeri a szöveg fogalmát, jellemzőit, szerkezeti sajátosságait,
    valamint a különféle szövegtípusokat és megjelenésmódokat;

9. felismeri és alkalmazza a szövegösszetartó grammatikai és
    jelentésbeli elemeket, szövegépítése arányos és koherens;

10. ismeri a stílus fogalmát, a stíluselemeket, a stílushatást, a
    stíluskorszakokat, stílusrétegeket, ismereteit a szöveg befogadása
    és alkotása során alkalmazza;

11. szövegelemzéskor felismeri az alakzatokat és a szóképeket, értelmezi
    azok hatását, szerepét, megnevezi típusaikat;

12. ismeri a nyelvhasználatban előforduló különféle nyelvváltozatokat
    (nyelvjárások, csoportnyelvek, rétegnyelvek), összehasonlítja azok
    főbb jellemzőit;

13. alkalmazza az általa tanult nyelvi, nyelvtani, helyesírási,
    nyelvhelyességi ismereteket;

14. a retorikai ismereteit a gyakorlatban is alkalmazza;

15. ismeri és érti a nyelvrokonság fogalmát, annak kritériumait;

16. ismeri a magyar nyelv eredetének hipotéziseit, és azok tudományosan
    megalapozott bizonyítékait;

17. érti, hogy nyelvünk a történelemben folyamatosan változik, ismeri a
    magyar nyelvtörténet nagy korszakait, kiemelkedő jelentőségű
    nyelvemlékeit;

18. ismeri a magyar nyelv helyesírási, nyelvhelyességi szabályait;

19. tud helyesen írni, szükség esetén nyomtatott és digitális
    helyesírási segédleteket használ;

20. etikusan és kritikusan használja a hagyományos, papír alapú, illetve
    a világhálón található és egyéb digitális adatbázisokat.

IRODALMI KULTÚRA, IRODALMI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. elolvassa a kötelező olvasmányokat, és saját örömére is olvas;

2. felismeri és elkülöníti a műnemeket, illetve a műnemekhez tartozó
    műfajokat, megnevezi azok poétikai és retorikai jellemzőit;

3. megérti, elemzi az irodalmi mű jelentésszerkezetének szintjeit;

4. értelmezésében felhasználja irodalmi és művészeti, történelmi,
    művelődéstörténeti ismereteit;

5. összekapcsolja az irodalmi művek szerkezeti felépítését, nyelvi
    sajátosságait azok tartalmával és értékszerkezetével;

6. az irodalmi mű értelmezése során figyelembe veszi a mű
    keletkezéstörténeti hátterét, a műhöz kapcsolható filozófiai,
    eszmetörténeti szempontokat is;

7. összekapcsolja az irodalmi művek szövegének lehetséges értelmezéseit
    azok társadalmi-történelmi szerepével, jelentőségével;

8. összekapcsol irodalmi műveket különböző szempontok alapján
    (motívumok, történelmi, erkölcsi kérdésfelvetések, művek és
    parafrázisaik);

9. összehasonlít egy adott irodalmi művet annak adaptációival (film,
    festmény, zenemű, animáció, stb.), összehasonlításkor figyelembe
    veszi az adott művészeti ágak jellemző tulajdonságait;

10. epikai és drámai művekben önállóan értelmezi a cselekményszálak, a
    szerkezet, az időszerkezet (lineáris, nem lineáris), a helyszínek és
    a jellemek összefüggéseit;

11. epikai és drámai művekben rendszerbe foglalja a szereplők
    viszonyait, valamint összekapcsolja azok motivációját és
    cselekedeteit;

12. epikai művekben értelmezi a különböző elbeszélésmódok szerepét
    (tudatábrázolás, egyenes és függő beszéd, mindentudó és korlátozott
    elbeszélő stb.);

13. a drámai mű értelmezésében alkalmazza az általa tanult drámaelméleti
    és drámatörténeti fogalmakat (pl. analitikus és abszurd dráma,
    epikus színház, elidegenedés);

14. a líra mű értelmezésében alkalmazza az általa tanult líraelméleti és
    líratörténeti fogalmakat (pl. lírai én, beszédhelyzetek,
    beszédmódok, ars poetica, szereplíra);

15. a tantárgyhoz kapcsolódó fogalmakkal bemutatja a lírai mű hangulati
    és hangnemi sajátosságait, hivatkozik a mű verstani felépítésére;

16. szükség esetén a mű értelmezéséhez felhasználja történeti
    ismereteit;

17. a mű értelmezésében összekapcsolja a szöveg poétikai tulajdonságait
    a mű nemzeti hagyományban betöltött szerepével;

18. tájékozottságot szerez régiója magyar irodalmáról;

19. tanulmányai során ismereteket szerez a kulturális intézmények
    (múzeum, könyvtár, színház) és a nyomtatott, illetve digitális
    formában megjelenő kulturális folyóiratok, adatbázisok működéséről.

MŰISMERET

A. ÉLETMŰVEK

A tanuló ismeri az alábbi magyar szerzők életművét. A kerettantervben
részletezett művek ismerete alapján értelmezi a szerzők életművének
fontosabb összefüggését, azok etikai, történeti, lélektani vagy
társadalmi vonatkozásait. Érti a szerzők nemzeti-kulturális
jelentőségét, helyét a magyar irodalom történetében. Memoriterként
felidéz néhány szövegrészt a szerzők életművéből.

Ady Endre, Arany János, Babits Mihály, Herczeg Ferenc, Jókai Mór, József
Attila, Kosztolányi Dezső, Mikszáth Kálmán, Petőfi Sándor, Vörösmarty
Mihály

B. PORTRÉK:

A tanuló ismeri az alábbi magyar szerzők kerettantervben részletezett
néhány rövidebb (lírai, kisprózai) alkotását vagy hosszabb művéből
választott részletet, valamint azok történeti, etikai, lélektani vagy
társadalmi vonatkozását. Ezek ismerete alapján értelmezi a szerzők
írásmódjának és világlátásának jellegzetességeit, érti a szerzők
nemzeti-kulturális jelentőségét, valamint ismeri helyüket a magyar
irodalom történetében: portrét készít róluk.

Balassi Bálint, Berzsenyi Dániel, Csokonai Vitéz Mihály, Janus
Pannonius, Kányádi Sándor, Kölcsey Ferenc, Móricz Zsigmond, Örkény
István, Szabó Magda, Wass Albert, Zrínyi Miklós

C. METSZETEK:

A magyar irodalomtörténettel és a nemzeti hagyományokkal kapcsolatos
ismereteinek elmélyítése során a tanuló különböző lehetséges
kontextusokba illesztve olvas és értelmez, a kerettantervben
részletezett szöveget vagy szövegrészletet. Ezek alapján készít egy
korszakról, szerzőről irodalmi metszetet.

Áprily Lajos; Dsida Jenő; Juhász Gyula; Karinthy Frigyes; Krúdy Gyula;
Mikes Kelemen; Nagy László; Pilinszky János; Radnóti Miklós; Reményik
Sándor; Szabó Dezső, Szabó Lőrinc; Tóth Árpád; Weöres Sándor.

D. SZEMELVÉNYEK:

A tanuló megismeri a magyar irodalom kerettantervben részletezett néhány
szemelvényét, alkotókat, műveket, melyek segítségével irodalomtörténeti,
művelődéstörténeti korokat tud bemutatni.

Anonymus; Apáczai Csere János; Arany László; Bessenyei György; Gárdonyi
Géza; Gyergyai (Gergei) Albert; Gyóni Géza; Hajnóczy Péter; Halotti
beszéd és könyörgés; Heltai Gáspár; Kassák Lajos; Károli Gáspár;
Kazinczy Ferenc; Kecskeméti Vég Mihály; Kisfaludy Károly; Kuruc-kori
költészet; Márai Sándor; II. Rákóczi Ferenc; Nagy Gáspár; Ómagyar
Mária-siralom; Örkény István; Pázmány Péter; Romhányi József; Sylvester
János; Szenczi Molnár Albert; Szent István király legendája Hartvik
püspöktől; Margit-legenda; Szép Ernő; Sztárai Mihály; Tompa Mihály;
Vajda János

E. VILÁGIRODALMI SZÖVEGEK ÉS ALKOTÓK:

A tanuló megismeri a világirodalom kerettantervben rögzített néhány
fontos alkotóját és szövegét. Tanulmányai során megismeri az európai
kultúra különböző korszakainak és népeinek alapvető irodalmi és
kulturális hagyományait, azokat kapcsolatba hozza a magyar kultúra
sajátosságaival.

Történetek és szövegek a görög mitológiából; babiloni teremtésmítosz;
részletek a Bibliából

Aiszóposz; Alkaiosz; Anakreón; Balzac, Honoré de vagy Stendhal;
Boccaccio; Borowski, Tadeusz; Burns, Robert; Byron; Catullus, Caius
Valerius; Dante, Alighieri; Defoe, Daniel; Dosztojevszkij, Fjodor
Mihajlovics; Eliot Thomas Stearns; francia szimbolisták; García Márquez,
Gabriel; Goethe, Johann Wolfgang von; Gogol, Nyikolaj Vaszilijevics;
Heine, Heinrich; Homérosz; Horatius Flaccus, Quintus; Hrabal, Bohumil;
Hugo, Victor; Kafka, Franz; La Fontaine, Jean de; Mann, Thomas;
Mickiewicz, Adam; Orwell, George; Ovidius Naso, Publius; Petrarca; Poe,
Edgar Allan; Pound, Ezra; Puskin, Alexszandr Szergejevics; Swift,
Jonathan; Szapphó; Szent Ágoston; Szent Ferenc; Theokritosz; Todi,
Jacopone da; Tolsztoj, Lev Nyikolajevics Vergilius Maro, Publius;
Villon, François; Vogelweide, Walter von der; Voltaire

F. A SZÍNHÁZ- ÉS DRÁMATÖRTÉNET NAGY ALKOTÓI ÉS ALKOTÁSAI

A tanuló megismeri a színház- és drámatörténet kerettantervben rögzített
nagy alkotóit és alkotásait. A műveket elhelyezi az irodalomtörténetben.
Kiemeli a művek alaphelyzetét jelentő konfliktusokat, azokat értelmezi.
Megérti a szereplők egymáshoz való viszonyát, a mű szerkezetét.

Beckett, Samuel Barclay; Brecht, Bertolt; Csehov, Anton Pavlovics vagy
Ibsen, Henrik; Dürrenmatt, Friedrich; Katona József; Madách Imre;
Molière; Örkény István; Shakespeare, William; Szabó Magda; Szophoklész

G.

A tanuló megismeri régiója, szűkebb hazája irodalmát, illetve
alkotásokat az irodalom határterületeiről.

A kötelező olvasmányok és a memoriterek (H, I) a közös irodalmi
műveltség (kulturális kód) legfontosabb elemeit tartalmazzák.

H. KÖTELEZŐ OLVASMÁNYOK

Biblia (a kerettantervben meghatározott részletek);

Boccaccio, Giovanni: Dekameron, (részletek); Dante Alighieri: Isteni
színjáték -- Pokol (részletek); Homérosz: Odüsszeia (részletek); Jókai
Mór: A huszti beteglátogatók (novella); Az arany ember; Katona József:
Bánk bán; Mikes Kelemen: Törökországi levelek (részletek);

Molière: A fösvény; Petőfi Sándor: A helység kalapácsa, Az apostol
(részlet); Shakespeare, William: Romeo és Júlia vagy Hamlet, dán
királyfi; Szent Margit legendája (részlet); Szophoklész: Antigoné;
Villon, François: A nagy testamentum (részletek); Vörösmarty Mihály:
Csongor és Tünde; Zrínyi Miklós: Szigeti veszedelem (részletek). Arany
János: Toldi estéje; Babits Mihály: Jónás könyve; Jónás imája; Balzac,
Honoré de: Goriot apó (részletek) vagy Stendhal: Vörös és fekete
(részletek); Beckett, Samuel Barclay: Godot-ra várva vagy Dürrenmatt,
Friedrich: A fizikusok; Herczeg Ferenc: Az élet kapuja; Ibsen, Henrik:
Nóra/A vadkacsa vagy Csehov, Anton Pavlovics: A sirály/Ványa bácsi;
Madách Imre: Az ember tragédiája; Mikszáth Kálmán: Beszterce ostroma;
Móricz Zsigmond: Úri muri, Tragédia; Örkény István: Tóték; Szabó Magda:
Az ajtó; Tolsztoj, Lev Nyikolajevics: Ivan Iljics halála; Wass Albert:
Adjátok vissza a hegyeimet!

I. MEMORITEREK

Anakreón: Gyűlölöm azt...; Balassi Bálint: Egy katonaének (részlet); Adj
már csendességet (részlet); Berzsenyi Dániel: A közelítő tél (1.
versszak); A magyarokhoz (I.) (1. versszak); Osztályrészem (1.
versszak); Catullus: Gyűlölök és szeretek; Csokonai Vitéz Mihály:
Tartózkodó kérelem (általános iskolai memoriter felújítása); A
Reményhez; Halotti beszéd és könyörgés (részlet); Homérosz: Odüsszeia
(részlet); Janus Pannonius: Pannonia dicsérete; Kölcsey Ferenc: Himnusz
(általános iskolai memoriter felújítása); Zrínyi második éneke
(részletek); Ómagyar Mária-siralom (részlet); Petőfi Sándor: Fa leszek,
ha...; Nemzeti dal (általános iskolai memoriter felújítása), A bánat?
Egy nagy oceán, Szeptember végén (általános iskolai memoriter
felújítása); Vörösmarty Mihály: Szózat (általános iskolai memoriter
felújítása); Gondolatok a könyvtárban (részlet); Előszó (részlet).

Ady Endre: Góg és Magóg fia vagyok én...; Kocsi-út az éjszakában; Áprily
Lajos: Március; Arany János: Toldi estéje (részletek); egy szabadon
választott balladája a nagykőrösi korszakból; Epilógus (részlet); Babits
Mihály: A lírikus epilógja; Jónás imája; József Attila: Reménytelenül
(Lassan, tűnődve); Óda (részlet); Kányádi Sándor: Valaki jár a fák
hegyén; Kosztolányi Dezső: Hajnali részegség részlet); Nagy László: Ki
viszi át a Szerelmet; Radnóti Miklós: Hetedik ecloga (részlet); Reményik
Sándor: Halotti vers a hulló leveleknek (részlet).

SZÖVEGÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. különböző megjelenésű, típusú, műfajú, korú és összetettségű
    szövegeket olvas, értelmez;

2. a különböző olvasási típusokat és a szövegfeldolgozási stratégiákat
    a szöveg típusának és az olvasás céljának megfelelően választja ki
    és kapcsolja össze;

3. a megismert szöveg tartalmi és nyelvi minőségéről érvekkel
    alátámasztott véleményt alkot;

4. hosszabb terjedelmű szöveg alapján többszintű vázlatot vagy
    részletes gondolattérképet készít;

5. azonosítja a szöveg szerkezeti elemeit, és figyelembe veszi azok
    funkcióit a szöveg értelmezésekor;

6. egymással összefüggésben értelmezi a szöveg tartalmi elemeit és a
    hozzá kapcsolódó illusztrációkat, ábrákat;

7. különböző típusú és célú szövegeket hallás alapján értelmez és
    megfelelő stratégia alkalmazásával értékel és összehasonlít;

8. összefüggő szóbeli szöveg (előadás, megbeszélés, vita) alapján
    önállóan vázlatot készít;

9. felismeri és értelmezésében figyelembe veszi a hallott és az írott
    szövegek közötti funkcionális és stiláris különbségeket;

10. folyamatos és nem folyamatos, hagyományos és digitális szövegeket
    olvas és értelmez maga által választott releváns szempontok alapján;

11. feladatai megoldásához önálló kutatómunkát végez nyomtatott és
    digitális forrásokban, ezek eredményeit szintetizálja;

12. felismeri és értelmezi a szövegben a kétértelműséget és a
    félrevezető információt, valamint elemzi és értelmezi a szerző
    szándékát;

13. megtalálja a közös és eltérő jellemzőket a hagyományos és a
    digitális technikával előállított, tárolt szövegek között, és
    véleményt formál azok sajátosságairól;

14. törekszik arra, hogy a különböző típusú, stílusú és regiszterű
    szövegekben megismert, számára új kifejezéseket beépítse
    szókincsébe, azokat adekvát módon használja;

15. önállóan értelmezi az ismeretlen kifejezéseket a szövegkörnyezet
    vagy digitális, illetve nyomtatott segédeszközök használatával;

16. ismeri a tanult tantárgyak, tudományágak szakszókincsét, azokat a
    beszédhelyzetnek megfelelően használja.

SZÖVEGALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. megadott szempontrendszer alapján szóbeli feleletet készít;

2. képes eltérő műfajú szóbeli szövegek alkotására: felelet,
    kiselőadás, hozzászólás, felszólalás;

3. rendelkezik korának megfelelő retorikai ismeretekkel;

4. felismeri és megnevezi a szóbeli előadásmód hatáskeltő eszközeit,
    hatékonyan alkalmazza azokat;

5. írásbeli és szóbeli nyelvhasználata, stílusa az adott kommunikációs
    helyzetnek megfelelő. Írásképe tagolt, beszéde érthető, artikulált;

6. a tanult szövegtípusoknak megfelelő tartalommal és szerkezettel
    önállóan alkot különféle írásbeli szövegeket;

7. az írásbeli szövegalkotáskor alkalmazza a tanult szerkesztési,
    stilisztikai ismereteket és a helyesírási szabályokat;

8. érvelő esszét alkot megadott szempontok vagy szövegrészletek
    alapján;

9. ismeri, érti és etikusan alkalmazza a hagyományos, digitális és
    multimédiás szemléltetést;

10. különböző, a munka világában is használt hivatalos szövegeket alkot
    hagyományos és digitális felületeken (pl. kérvény, beadvány,
    nyilatkozat, egyszerű szerződés, meghatalmazás, önéletrajz,
    motivációs levél);

11. megadott vagy önállóan kiválasztott szempontok alapján az irodalmi
    művekről elemző esszét ír.

OLVASÓVÁ NEVELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a kötelező olvasmányokat elolvassa, és saját örömére is olvas;

2. tudatosan keresi a történeti és esztétikai értékekkel rendelkező
    olvasmányokat, műalkotásokat;

3. olvasmányai kiválasztásakor figyelembe veszi az alkotások kulturális
    regiszterét.

4. társai érdeklődését figyelembe véve ajánl olvasmányokat;

5. választott olvasmányaira is vonatkoztatja a tanórán megismert
    kontextusteremtő eljárások tanulságait;

6. önismeretét irodalmi művek révén fejleszti;

7. részt vesz irodalmi mű kreatív feldolgozásában, bemutatásában (pl.
    animáció, dramaturgia, átirat).

MÉRLEGELŐ GONDOLKODÁS, VÉLEMÉNYALKOTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. a környező világ jelenségeiről, szövegekről, műalkotásokról
    véleményt alkot, és azt érvekkel támasztja alá;

2. megnyilvánulásaiban, a vitákban alkalmazza az érvelés alapvető
    szabályait;

3. vitahelyzetben figyelembe veszi mások álláspontját, a lehetséges
    ellenérveket is;

4. feladatai megoldásához önálló kutatómunkát végez nyomtatott és
    digitális forrásokban, a források tartalmát mérlegelő módon gondolja
    végig;

5. a feladatokat komplex szempontoknak megfelelően oldja meg, azokat
    kiegészíti saját szempontjaival;

6. a kommunikációs helyzetnek és a célnak megfelelően tudatosan
    alkalmazza a beszélt és írott nyelvet, reflektál saját és társai
    nyelvhasználatára.

*II.3.2. Idegen nyelv*

A tudásalapú társadalomban a nyelvtudásnak kiemelt társadalmi,
kulturális és gazdasági jelentősége van. A nevelési-oktatási rendszer
feladata ezért az egyén fejlődésében is kulcsfontosságú nyelvtudás és
ezáltal a személyes és a szakmai fejlődés tágabb terének biztosítása. Az
idegennyelv-tanítás és -tanulás a nyelvtudás megszerzésén túl hozzájárul
a megismerő folyamatok fejlődéséhez, más tanulási területek fejlesztési
céljainak és nevelési feladatainak megvalósításához. A nyelvtanulás
során közvetített ismeretek segítik az adott nyelvet használó emberek
megismerését, az egyes kultúrák, valamint az azok közötti eltérések
megértését, a nyitott és befogadó életszemlélet kialakítását. Az idegen
nyelvek ismerete hozzájárul továbbá az érdeklődő, tájékozott
nyelvtanulói magatartás kialakításához, és fejleszti a nyelvet tanuló és
nyelvet használó anyanyelvi kompetenciáit is. A nyelvtudás és a
nyelvhasználat segítségével elért tudás erősítheti a saját nemzeti
azonosságtudatot, valamint az európai közösséghez tartozás érzését. Az
idegennyelv-tudás lehetővé teszi a társadalmi és tanulási célú
mobilitást, valamint növeli az információhoz való hozzáférés
lehetőségét, és csökkenti az esélyhátrányokat. Az egyéni előnyök mellett
a használható nyelvtudásnak számos társadalmi haszna is van. Az idegen
nyelvek ismerete a tudásalapú társadalomban elengedhetetlen. Jelentős
szerepe van a körülöttünk lévő világ múltjának, jelenének és jövőjének
megismerésében, hozzájárul a tanuló jövőbeni munkavállalói
kompetenciáinak sokrétű fejlesztéséhez.

A mai kor egyik fontos nyelv- és oktatáspolitikai célkitűzése az egyéni
többnyelvűség fejlesztése, amelyhez nagymértékben hozzájárul az
intézményes idegennyelv-oktatás. Fontos, hogy a tanított nyelvek ne
különálló tantárgyakként jelenjenek meg, hanem a többnyelvűség
kontextusában, azaz a nyelvtanárok építsenek a nyelvtanulók már korábban
megszerzett nyelvi kompetenciáira, nyelvtanulási stratégiáira, valamint
a nyelvek közötti hasonlóságokra. Ily módon lehetővé válhat az
idegennyelv-oktatásban az integrált, nyelveken és kultúrákon átívelő
szemlélet, elősegítve ezzel a nyelvi tudatosságot és előkészítve további
idegen nyelvek későbbi elsajátítását.

Az idegen nyelvek tanítása során figyelembe kell venni a nyelvtanulók
más tantárgyak tanulása során szerzett ismereteit, már meglévő
anyanyelvi és idegen nyelvi tudását, valamint tanulási és nyelvtanulási
stratégiáit. Ezek az egyes tantárgyak között átvihetők, megerősítve
ezzel a tudásintegrációt, valamint a tantárgyakon átívelő komplex
gondolkodás fejlesztését. Mind az élő, mind a klasszikus idegen nyelvek
ismerete olyan tartalmakhoz nyitja meg az utat, amelyek más tanulási
területek elmélyülését segítik, és a hatás a másik irányban is döntő
fontosságú: az új, a tanulók számára motiváló célnyelvi tartalmak
fejlesztik a nyelvtudást és előremozdítják a nyelvtanulást.

A sikerben fontos szerepet játszik az önálló nyelvtanulásra való tudatos
felkészítés is. A hatékony és eredményes idegennyelv-tanításhoz és --
tanuláshoz, az egyénre szabott oktatáshoz elengedhetetlen a 21. századi
modern eszközök és tartalmak bevonása.

Az idegen nyelv tanulási terület két tantárgyat ölel fel: az élő és a
klasszikus idegen nyelvet.

**Élő idegen nyelv:** Az első idegen nyelv oktatását 4. évfolyamon, a
másodikat a 9. évfolyamon kell megkezdeni. A középiskolákban második
idegen nyelvként szabad választás szerint oktathatók a klasszikus vagy
az élő idegen nyelvek.

**Klasszikus idegen nyelv**: Második idegen nyelvként kerettantervvel
szabályozott klasszikus nyelvet is oktathatnak az intézmények.

***II.3.2.1. ÉLŐ IDEGEN NYELV***

***A) ALAPELVEK, CÉLOK***

A modern élő idegennyelv-oktatás elsődleges célkitűzése a nyelvtanulók
nyelvi cselekvőképességének fejlesztése. Ennek értelmében a nyelvtanuló
képessé válik arra, hogy a nyelvet kommunikációs céljainak és igényeinek
megfelelően valódi szituációkban tudja használni. A tevékenységközpontú
élő idegennyelv-oktatás a tanuló-központúságot szem előtt tartva a
tanulók számára életkorukkal, illetve érdeklődésükkel összhangban lévő
helyzeteket teremt. A cél az, hogy a tanulók olyan valós, a gyakorlatban
is alkalmazható nyelvtudást érjenek el intézményes keretek között, mely
lehetővé teszi a felsőfokú intézményben való továbbtanulást,
felhasználható ismeretszerzésre, szórakozásra, személyes és szakmai
céljaik elérésére mind természetes, mind pedig digitális térben. Az
idegennyelv-tanítás során a tanulók tehát olyan nyelvhasználók, akik
azért tanulják az idegen nyelvet, hogy azt később személyes és
választott szakmájukkal összefüggő helyzetekben alkalmazni tudják.

**Az élő idegen nyelv ismerete által** a tanulók megismerik és megértik
az adott nyelvet használó embereket és kultúrákat, és erre építve
nyitottabbá, megértőbbé, érdeklődőbbé és tájékozottabbá válnak. Ez
hozzájárul személyes és társas kompetenciájuk fejlődéséhez, amely mind
az iskolában, mind magánéletükben segíti majd őket további nyelvtanulási
és egyéb céljaik elérésében. A nyelvtanulás emellett hatékonyan
fejleszti a tanulási és gondolkodási stratégiák beépülését a tanulók
tanulási kompetenciájába, valamint jelentős szerepet játszik a
tantárgyak közötti tudásintegrációban is. A tantárgyakon átívelő,
interdiszciplináris szemlélet segítségével a tanulók az idegen nyelv
tanulása során fel tudják használni a más tantárgyak keretében szerzett
ismereteiket, valamint építhetnek nyelvtudásukra más tantárgyak
tudásanyagának bővítése során.

A köznevelési intézményekben elsajátítható élő idegen nyelvek tanítása
és tanulása során figyelembe kell venni, hogy az idegen nyelveket a
tanulók nem kizárólag a nyelvórákon tanulják, hanem az iskolán kívüli,
mindennapi tevékenységeiken keresztül is elsajátítanak nyelvi elemeket,
önállóan vagy társaikkal együttműködve. Az önszabályozó, hosszú távon is
fenntartható nyelvi fejlődés érdekében elengedhetetlen a tanórán kívüli
tevékenységekre is építeni a nyelvtanításban, ezzel élővé és
megfoghatóvá téve a nyelvtanulási célokat.

A célok megvalósításához szükséges a tanulók nyelvtanulási
tapasztalatainak és igényeinek folyamatos feltárása, egyéni különbségeik
megismerése, valamint a 21. századi, akár digitális eszközök és
tartalmak beépítése a nyelvtanulás folyamatába. A hatékonyság és
eredményesség érdekében feltétlenül szükséges a fejlesztési területek
integrált megközelítése, beleértve a nyelvtani ismeretek életkornak és
nyelvi szintnek megfelelő, funkcionális átadását. A kommunikációs
lehetőségek növeléséhez javasolt a mindenkori csoportbontás
megteremtése.

Társadalmi szinten az élő idegen nyelvek használható tudása és az idegen
nyelvi kommunikáció csökkenti az esélyhátrányokat, és növeli az
információk egyenlő elérésének esélyét. Az élő idegen nyelvek mindezeken
túl lehetőséget kínálnak a nemzetközi kapcsolatépítésre és a tanulási
célú mobilitásra is.

Az élő idegen nyelv tanításának célja, hogy a tanuló:

1. a jelen és a jövő valós és életszerű igényeinek megfelelő,
    hagyományos és digitális csatornákon is alkalmazható nyelvtudást
    szerez intézményes keretek között legalább egy idegen nyelvből;

2. a nyelvi ismeretek felépítésén, valamint a nyelvi alapkészségeken
    túl elsajátít interkulturális, szociolingvisztikai és pragmatikai
    készségeket is, melyek segítségével nyelvi eszköztárát valódi
    kommunikációs helyzetekben hatékonyan és megfelelően tudja
    alkalmazni;

3. felhasználja az adott idegen nyelven szerzett tudást, a nyelvtanulás
    során megismert stratégiákat ismeretszerzésre, szórakozásra,
    személyes és szakmai céljai elérésére;

4. pozitívan viszonyul a nyelvekhez és a nyelvtanuláshoz;

5. megismeri és megérti az adott nyelvet használó embereket és
    kultúrákat, és erre építve nyitottabb, érdeklődőbb és tájékozottabb
    lesz;

6. célnyelven közvetíti hazája kulturális értékeit, a magyar nép
    történetének legfőbb állomásait;

7. elsajátít nyelvtanulási stratégiákat, és aktív, önálló, önszabályozó
    nyelvtanulóvá válik;

8. megérti, hogy mivel minden élő idegen nyelv folyamatosan változik,
    megszerzett ismereteit és használatukat folyamatosan fejlesztenie
    kell ahhoz, hogy megfeleljen a mindenkori kommunikáció során fellépő
    igényeknek;

9. eléri a 6. évfolyam végére a KER szerinti A1, a 8. évfolyam végére
    pedig az A2 nyelvi szintet a tanult első idegen nyelvből;

10. középiskolai tanulmányai végére képes lehet elérni a KER szerinti B2
    szintet, de legalább a középszintű nyelvi érettségit (B1 szint)
    teljesíti;

11. a második (élő) idegen nyelvből a gimnázium végére eléri a KER A2
    szintet.

***A tantárgy tanításának specifikus jellemzői a 4. évfolyamon***

A kisgyermekkori idegen nyelvi fejlesztés legfontosabb céljai közé a
kedvező nyelvtanulási attitűd kialakítása, illetve a motiváció
megteremtése és fenntartása tartozik. A nyelvtanulás első szakaszában a
gyermek megismerkedik az idegen nyelvek létezésével, a nyelvtudás
fontosságával, és bepillantást nyer a célnyelvi kultúrákba. A szelíd
nyelvi nevelés élményalapú és tevékenységközpontú, fő célja a nyelv és a
nyelvtanulás megszerettetésén túl a nyelvtanulás mindennapokban
betöltött szerepének megértése és a célnyelvi kultúrák megismerése. Az
önálló nyelvtanulóvá válás során a tanuló megismerkedik olyan alapvető
tanulási stratégiákkal, amelyek segítségével képessé válik nyelvtudását
folyamatosan fejleszteni és fenntartani, valamint az aktív nyelvtanulást
megalapozni. A tanulási folyamat során szerzett siker növeli és
fejleszti az önbizalmat, önismeretet, önértékelést és az együttműködési
hajlandóságot.

Az idegen nyelv tanulásának kezdeti szakasza alapvetően nem
elvárás-központú, hanem tevékenység- és élményalapú. A hangsúly a
szóbeliségen, a játékosságon és az életkornak, illetve nyelvi szintnek
megfelelő kommunikáció megvalósulásán van. A gyermekbarát, vizuális
elemekben gazdag, szemléletes tanulási környezet, valamint az irányító
pozitív tanári viszonyulás segítségével a tanuló megérti, hogy már kevés
nyelvtudását is fel tudja használni valós helyzetekben.

Az idegen nyelvi órákon a tanuló életkori sajátosságainak és fejlettségi
szintjének megfelelő, érdekes, változatos és kihívást jelentő
tevékenységek által kerül közel az idegen nyelvhez. A korosztály
sajátosságainak megfelelően a beszédértés és a beszédkészség, valamint
az interakció komplex fejlesztésén van a hangsúly. A nyelvi tartalmakat
minden esetben kontextusba ágyazva, konkrét beszédhelyzetek során
javasolt feldolgozni. A nyelvórákat az örömteli játékosság, mozgással,
dramatizálással összekapcsolt daltanulás, mondókázás, mesélés és
változatos munkaformák kell, hogy jellemezzék. A tanulás tartalmát,
tananyagait a nyelvtanuló igényeinek és egyéni különbségeinek
megfelelően kell folyamatosan tervezni és alakítani, minél inkább szem
előtt tartva a 21. századi eszközök nyújtotta lehetőségeket. A tanuló
ismerjen meg az életkorának és érdeklődésének megfelelő autentikus
anyagokat is, amelyek tovább erősítik a nyelvtanulási motivációját, és
legyen alkalma találkozni a következő szövegtípusokkal: gyermekirodalmi
szövegek, a populáris kultúra szövegei, ismeretterjesztő és tényközlő,
valamint személyes interakcióhoz, élményekhez, játékos tanuláshoz
kapcsolódó szövegek.

Az adott nevelési-oktatási szakaszban egy élő idegen nyelv kötelező,
melynek tanulási eredményeit, követelményeit a Nat szabályozza, és
amelynek kimenetére nem határozható meg KER szerinti nyelvi szint. A
nyelvtanulás ugyanakkor már ebben a képzési szakaszban is szorosan
kapcsolódik más tanulási területekhez, ezzel tudatosítva a tanulókban
azt, hogy a nyelvtudás nem önmagáért való cél, hanem eszköz a világ
megismerésére és egyéni céljaik elérésére.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

Ebben a nevelési-oktatási szakaszban az egyik fő cél a nyelvtanulási
motiváció fenntartása és erősítése, valamint a valós nyelvi helyzetekben
egyre inkább használható nyelvtudás fejlesztése. A korábbi tanulási
szakaszban megalapozott önbizalom további erősítése, együttesen az újabb
stratégiák elsajátításával és az alapkészségek integrált fejlődésével
magabiztosabb nyelvhasználóvá teszi a tanulót. A nyelvórát egyre inkább
a valódi nyelvi szituációkhoz közelítés, a nyelvtanulás és a
nyelvhasználat lehetőségeinek bővülése kell, hogy jellemezze. Ebben a
nevelési-oktatási szakaszban tudatosan kell felépíteni a nyelvtanuló
nyelvi ismereteit és képessé kell tenni őt a szövegek világában való
eligazodásra, a következő szövegtípusokkal való találkozásra: gyermek-
és ifjúsági irodalmi szövegek, a populáris kultúra szövegei,
ismeretterjesztő és tényközlő, valamint személyes interakcióhoz,
élményekhez, játékos tanuláshoz kapcsolódó szövegek. A reflexióra és
önreflexióra építő tanulási és értékelési formákkal együtt ezek
megfelelő alapot biztosítanak az önálló nyelvtanulóvá váláshoz.

A kommunikatív érték, a nyelvhasználói szerep megerősítése ebben a
szakaszban kiemelkedő jelentőséggel bír. Az egyéni különbségek tovább
erősödhetnek, ezért az eltérő nyelvi szinten lévő tanulók különbségeit
fel kell tárni és a tartalmakat ennek figyelembevételével
differenciáltan kell meghatározni. A segítő, biztató tanári magatartás,
valamint a jó hangulatú, stresszmentes, motiváló tanulási környezet
ebben az életkorban is nagyban hozzájárul a nyelvhasználat
aktiválásához.

Az ajánlott témakörök és ezek elemei ebben a nevelési-oktatási
szakaszban bővülnek, továbbá az adott témákat a célnyelvet tanulók egyre
mélyebben és árnyaltabban dolgozzák fel. Ezeken az évfolyamokon tovább
erősödik az az elvárás, hogy a hagyományos témakörök mellett hangsúlyt
kapjanak az éppen aktuális témák, hírek, melyek tárgyalása tovább
csökkenti a távolságot az osztálytermi és a valós nyelvhasználat között.
A nyelvtanulási motiváció fenntartásához nagyban hozzájárulnak az
életkornak megfelelő autentikus anyagok. A tanulás tartalmát,
tananyagait a nyelvtanulók igényeinek megfelelően kell folyamatosan
tervezni és fejleszteni, fokozottan szem előtt tartva a 21. századi
lehetőségeket, különös tekintettel az infokommunikációs eszközökre és a
modern nyelvpedagógiai technológiákra.

Ebben a nevelési-oktatási szakaszban egy élő idegen nyelv tanulása
kötelező, melyből a 6. évfolyam végére a KER szerinti A1, a 8. évfolyam
végére az A2 nyelvi szint a kimeneti elvárás, és melynek tanulási
eredményeit, követelményeit a Nat részletezi. A tanuló a
nevelési-oktatási szakasz végére ismer és tudatosan használ alapszintű
nyelvtanulási és nyelvhasználati stratégiákat, valamint ezeket más
tanulási területeken is alkalmazza kompetenciáinak elmélyítésére.
Életkorának és nyelvi szintjének megfelelő hagyományos és digitális
nyelvtanulási forrásokat, eszközöket és mobilalkalmazásokat használ,
továbbá kiaknázza a tanórán kívüli nyelvtanulási lehetőségeket,
nyelvtudását kapcsolatépítésre használja személyes interakciókban.

***A tantárgy tanításának specifikus jellemzői a 9--12. évfolyamon***

Az élő idegen nyelv oktatásának célja a 9--12. évfolyamon is a tanuló
idegen nyelvi kommunikatív kompetenciájának továbbfejlesztése, a többi
kulcskompetencia és általános nevelési cél erősítése mellett. A
nyelvtanulás a középiskolában is kiemelkedő szerepet játszik olyan
fontos fejlesztési területeken, mint a körülöttünk lévő világ
megismerése és megértése, az élethosszig tartó, önszabályozó tanulási
folyamatok, stratégiák és attitűdök megalapozása, a kreatív és logikus
gondolkodás fejlesztése, a társadalmi felelősségvállalás kialakítása, az
együttműködési képességek, a nemzeti és interkulturális tudatosság,
valamint a digitális kompetenciák és önkifejezés erősítése.

A nyelvtanuló valójában nyelvhasználó. Köznevelési tanulmányai
befejezése után a nyelvórán megtanultakat valós élethelyzetekben is
tudnia kell használni, ezért a fejlesztési területek az alapkészségeken
túl szükségszerűen magukban foglalják a mindennapi nyelvhasználat, az
interkulturalitás, a szociolingvisztika és a pragmatika nézőpontjait,
valamint a nyelvtanításnak élnie kell a hagyományos mellett a digitális
eszközök és tartalmak nyújtotta lehetőségekkel is.

A megszerzett nyelvtudás valós célnyelvi környezetben való bővítéséhez,
finomításához és a nyelvtanulási motiváció erősítéséhez nagyban
hozzájárulnak majd a középiskolásoknak szervezett külföldi tanulmányutak
a 9. és 11. évfolyamon. A célnyelvi országban lehetőségük lesz a
nyelvtanulóknak a kultúra és az emberek megismerésére. Az utakra történő
felkészülés pozitív hatással lesz a nyelvórákra is, melyek keretében a
várható nyelvi és kulturális helyzetekre való gyakorlás elengedhetetlen
lesz.

Az ajánlott témakörök és ezek elemei ebben a nevelési-oktatási
szakaszban még tovább bővülnek, feldolgozásuk egyre mélyebben és
árnyaltabban történik. Egyre hangsúlyosabbá válnak a kereszttantervi,
interkulturális és célnyelvi vonatkozások, valamint a tudásmegosztással
és ismeretszerzéssel kapcsolatos tartalmak. Az osztálytermi témakör a
9-10. évfolyamon az iskola és a tanulás témáit, 11-12. évfolyamon pedig
a vizsgafelkészítést, illetve az érettségire való felkészítést állítja
fókuszba, utóbbi segítségével lehetővé téve a tanuló felkészülését a
kimeneti követelmények teljesítésére is.

Ebben a nevelési-oktatási szakaszban tovább folytatódik a nyelvi
ismeretek alapkészségekbe integrált felépítése, amely képessé teszi a
nyelvtanulót az egyre összetettebb és absztraktabb szövegek tartalmának
befogadására. A nyelvi ismeretek felépítésekor fontos kiemelni a nyelvi
eszközök szövegekben betöltött funkcionális szerepét. A nyelvtanulót
képessé kell tenni arra, hogy az értés során a nyelvi eszközök szövegben
betöltött funkcióját felismerje, valamint a produkció során azokat
figyelembe vegye. A nevelési-oktatási szakasz egyik fontos célkitűzése a
szövegértés, a szövegalkotás, valamint a szövegekkel való munka tudatos
fejlesztése változatos szövegtípusokon keresztül: irodalmi, populáris
kultúra szövegei, ismeretterjesztő, tényközlő, valamint személyes
interakcióhoz, élményekhez kapcsolódó szövegek.

Az egyéni különbségeknek, köztük az életkori sajátosságoknak kiemelkedő
szerep jut ebben az időszakban is, hiszen ezek minden esetben alapvetően
meghatározzák a nyelvtanulás hatékonyságát és a kimeneti követelmények
sikeres teljesülését. A változatos, tevékenységközpontú, élményszerű és
kognitív kihívást jelentő tanórai tevékenységek továbbra is
meghatározóak, de az iskolán kívüli informális és nem formális tanulási
lehetőségek, egyéni utak is kiemelten fontosak az érintett korosztály
számára. Ezek mind hatékonyan segítik az idegennyelv-tudást és a valós
nyelvhasználatot, egyszerre képezve motiváló célokat, eszközöket és
bővítve a nyelvtanulási lehetőségeket. Az internetnek köszönhetően a
felhasználóképes nyelvtudás megszerzésében a tanuló már aktív, önálló
szerepet játszik -- a nyelvóráknak erre fel kell készíteniük a
középiskolásokat. Meghatározó a nyelvórák jelentősége abból a
szempontból is, hogy támogassák a tanulót saját egyedi nyelvtanulási
céljai, erősségei felismerésében, nyelvtanulási stratégiái
kialakításában, motivációja és az idegen nyelvek iránti pozitív
attitűdje megőrzésében. A korszerű idegennyelv-tanulás a nyelvhasználó
valós szükségleteire építve olyan életszerű kommunikatív helyzetekre
készíti fel a nyelvtanulót az órákon, amelyekkel tanulmányai és későbbi
felnőtt élete, munkavállalása, utazásai során nagy valószínűséggel
találkozik majd.

A tanuló a nevelési-oktatási szakasz végére tudatosan használ
nyelvtanulási és nyelvhasználati stratégiákat, és az aktív nyelvtanulás
életkorának megfelelő eszközeivel készül az egész életen át történő
tanulásra. A célnyelvi és célnyelvű kereszttantervi tartalmakon,
témakörökön keresztül további ismereteket szerez környezete
fenntartásáról, megóvásáról. Idegen nyelvi tanulmányai eredményeként
megfogalmazza a saját és más népek kultúrája közötti különbségeket és
értékként kezeli azokat.

Ennek az időszaknak a végére a tanuló elérheti a KER szerinti B2
szintet, és felkészülhet az emelt szintű nyelvi érettségire. A tanulónak
az első idegen nyelvből kötelezően legalább a középszintű nyelvi
érettségit (B1-es szint) kell teljesítenie. Az első idegen nyelvhez
tartozó tanulási eredményeket a Nat fogalmazza meg, melyeket a
nyelv-specifikus kerettantervek témakörök köré rendezve részleteznek.
Második idegen nyelvből a minimum kimeneti követelmény a KER szerinti A2
szint, melyhez élő és klasszikus idegen nyelvekből is a tanulási
eredményeket és nyelvi példákat a kerettantervek összegzik.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK ÉS SZÖVEGTÍPUSOK A 4. ÉVFOLYAMON***

1. Személyes témák és szituációk

2. Közvetlen környezeti és természeti témák és szituációk

3. Iskolai témák és szituációk

4. Kereszttantervi témák és szituációk

5. Interkulturális, országismereti témák

6. Célnyelvi vonatkozások

7. Szórakozás

8. Ismeretszerzés, tudásmegosztás

9. Gyermekirodalmi szövegek

10. Ismeretterjesztő szövegek

11. Tényközlő szövegek

12. Személyes interakcióhoz, élményekhez kapcsolódó szövegek

13. Játékos tanuláshoz kapcsolódó szövegek

***FŐ TÉMAKÖRÖK ÉS SZÖVEGTÍPUSOK AZ 5--8. ÉVFOLYAMON***

1. Személyes témák és szituációk

2. Közvetlen környezeti és természeti témák és szituációk

3. Közéleti témák és szituációk

4. Osztálytermi iskolai témák és szituációk

5. Kereszttantervi témák és szituációk

6. Interkulturális, országismereti témák

7. Aktuális témák

8. Célnyelvi vonatkozások

9. Szórakozás

10. Ismeretszerzés, tudásmegosztás

11. Gyermek-, ifjúsági irodalmi szövegek

12. Populáris kultúra szövegei

13. Ismeretterjesztő szövegek

14. Tényközlő szövegek

15. Személyes interakcióhoz, élményekhez kapcsolódó szövegek

16. Játékos tanuláshoz kapcsolódó szövegek

***FŐ TÉMAKÖRÖK ÉS SZÖVEGTÍPUSOK A 9--12. ÉVFOLYAMON***

1. Személyes témák és szituációk

2. Közvetlen környezeti és természeti témák és szituációk

3. Közéleti témák és szituációk

4. Osztálytermi témák és szituációk

5. Kereszttantervi témák és szituációk

6. Interkulturális, országismereti témák

7. Aktuális témák

8. Célnyelvi vonatkozások

9. Szórakozás

10. Ismeretszerzés, tudásmegosztás

11. Ifjúsági irodalmi szövegek

12. Populáris kultúra szövegei

13. Ismeretterjesztő szövegek

14. Tényközlő szövegek

15. Személyes interakcióhoz, élményekhez kapcsolódó szövegek

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. megismerkedik az idegen nyelvvel, a nyelvtanulással és örömmel vesz
    részt az órákon;

2. bekapcsolódik a szóbeliséget, írást, szövegértést vagy interakciót
    igénylő alapvető és korának megfelelő játékos, élményalapú élő
    idegen nyelvi tevékenységekbe;

3. szóban visszaad szavakat, esetleg rövid, nagyon egyszerű szövegeket
    hoz létre;

4. lemásol, leír szavakat és rövid, nagyon egyszerű szövegeket;

5. követi a szintjének megfelelő, vizuális vagy nonverbális eszközökkel
    támogatott, ismert célnyelvi óravezetést, utasításokat;

6. felismeri és használja a legegyszerűbb, mindennapi nyelvi
    funkciókat;

7. elmondja magáról a legalapvetőbb információkat;

8. ismeri az adott célnyelvi kultúrákhoz tartozó országok fontosabb
    jellemzőit és a hozzájuk tartozó alapvető nyelvi elemeket;

9. törekszik a tanult nyelvi elemek megfelelő kiejtésére;

10. célnyelvi tanulmányain keresztül nyitottabbá, a világ felé
    érdeklődőbbé válik.

SZÖVEGALKOTÁS IDEGEN NYELVEN

BESZÉDKÉSZSÉG: SZÓBELISÉG

A nevelési-oktatási szakasz végére a tanuló:

1. megismétli az élőszóban elhangzó egyszerű szavakat, kifejezéseket
    játékos, mozgást igénylő, kreatív nyelvórai tevékenységek során;

2. lebetűzi a nevét;

3. lebetűzi a tanult szavakat társaival közösen játékos tevékenységek
    kapcsán, szükség esetén segítséggel;

4. célnyelven megoszt egyedül vagy társaival együttműködésben
    megszerzett, alapvető információkat szóban, akár vizuális elemekkel
    támogatva.

ÍRÁSKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az anyanyelvén, illetve a tanult idegen nyelven történő
    írásmód és betűkészlet közötti különbségeket;

2. ismeri az adott nyelv ábécéjét;

3. lemásol tanult szavakat játékos, alkotó nyelvórai tevékenységek
    során;

4. megold játékos írásbeli feladatokat a szavak, szószerkezetek, rövid
    mondatok szintjén;

5. részt vesz kooperatív munkaformában végzett kreatív
    tevékenységekben, projektmunkában szavak, szószerkezetek, rövid
    mondatok leírásával, esetleg képi kiegészítéssel;

6. írásban megnevezi az ajánlott tématartományokban megjelölt,
    begyakorolt elemeket.

SZÖVEGÉRTÉS IDEGEN NYELVEN

BESZÉDÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megérti az élőszóban elhangzó, ismert témákhoz kapcsolódó, verbális,
    vizuális vagy nonverbális eszközökkel segített rövid kijelentéseket,
    kérdéseket;

2. beazonosítja az életkorának megfelelő szituációkhoz kapcsolódó,
    rövid, egyszerű szövegben a tanult nyelvi elemeket;

3. kiszűri a lényeget az ismert nyelvi elemeket tartalmazó, nagyon
    rövid, egyszerű hangzó szövegből;

4. azonosítja a célzott információt a nyelvi szintjének és életkorának
    megfelelő rövid hangzó szövegben;

5. támaszkodik az életkorának és nyelvi szintjének megfelelő hangzó
    szövegre az órai alkotó jellegű nyelvi, mozgásos nyelvi és játékos
    nyelvi tevékenységek során;

6. felismeri az anyanyelv és az idegen nyelv hangkészletét;

7. értelmezi azokat az idegen nyelven szóban elhangzó nyelvórai
    szituációkat, melyeket anyanyelvén már ismer;

8. felismeri az anyanyelve és a célnyelv közötti legalapvetőbb
    kiejtésbeli különbségeket;

9. figyel a célnyelvre jellemző hangok kiejtésére.

OLVASÁSÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti az anyanyelvi és a célnyelvi írott szövegben a betű-
    és jelkészlet közti különbségeket;

2. beazonosítja a célzott információt az életkorának megfelelő
    szituációkhoz kapcsolódó, rövid, egyszerű, a nyelvtanításhoz
    készült, illetve eredeti szövegben;

3. csendes olvasás keretében feldolgozva megért ismert szavakat
    tartalmazó, pár szóból vagy mondatból álló, akár illusztrációval
    támogatott szöveget;

4. megérti a nyelvi szintjének megfelelő, akár vizuális eszközökkel is
    támogatott írott utasításokat és kérdéseket, és ezekre megfelelő
    válaszreakciókat ad;

5. kiemeli az ismert nyelvi elemeket tartalmazó, egyszerű, írott, pár
    mondatos szöveg fő mondanivalóját;

6. támaszkodik az életkorának és nyelvi szintjének megfelelő írott
    szövegre az órai játékos alkotó, mozgásos vagy nyelvi fejlesztő
    tevékenységek során, kooperatív munkaformákban;

7. megtapasztalja a közös célnyelvi olvasás élményét;

8. aktívan bekapcsolódik a közös meseolvasásba, a mese tartalmát
    követi.

INTERAKCIÓ IDEGEN NYELVEN

BESZÉDKÉSZSÉG: SZÓBELI INTERAKCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. a tanórán begyakorolt, nagyon egyszerű, egyértelmű kommunikációs
    helyzetekben a megtanult, állandósult beszédfordulatok
    alkalmazásával kérdez vagy reagál, mondanivalóját segítséggel vagy
    nonverbális eszközökkel kifejezi;

2. törekszik arra, hogy a célnyelvet eszközként alkalmazza
    információszerzésre;

3. rövid, néhány mondatból álló párbeszédet folytat, felkészülést
    követően;

4. a tanórán bekapcsolódik a már ismert, szóbeli interakciót igénylő
    nyelvi tevékenységekbe, a begyakorolt nyelvi elemeket tanári
    segítséggel a tevékenység céljainak megfelelően alkalmazza;

5. érzéseit egy-két szóval vagy begyakorolt állandósult nyelvi
    fordulatok segítségével kifejezi, főként rákérdezés alapján,
    nonverbális eszközökkel kísérve a célnyelvi megnyilatkozást;

6. elsajátítja a tanult szavak és állandósult szókapcsolatok célnyelvi
    normához közelítő kiejtését tanári minta követése által, vagy
    autentikus hangzó anyag, digitális technológia segítségével.

MINDENNAPI IDEGENNYELV-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és alkalmazza a legegyszerűbb, üdvözlésre és elköszönésre
    használt mindennapi nyelvi funkciókat az életkorának és nyelvi
    szintjének megfelelő, egyszerű helyzetekben;

2. felismeri és alkalmazza a legegyszerűbb, bemutatkozásra használt
    mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének
    megfelelő, egyszerű helyzetekben;

3. felismeri és használja a legegyszerűbb, megszólításra használt
    mindennapi nyelvi funkciókat az életkorának és nyelvi szintjének
    megfelelő, egyszerű helyzetekben;

4. felismeri és használja a legegyszerűbb, a köszönet és az arra
    történő reagálás kifejezésére használt mindennapi nyelvi funkciókat
    az életkorának és nyelvi szintjének megfelelő, egyszerű
    helyzetekben;

5. felismeri és használja a legegyszerűbb, a tudás és nem tudás
    kifejezésére használt mindennapi nyelvi funkciókat az életkorának és
    nyelvi szintjének megfelelő, egyszerű helyzetekben;

6. felismeri és használja a legegyszerűbb, a nem értés, visszakérdezés
    és ismétlés, kérés kifejezésére használt mindennapi nyelvi
    funkciókat életkorának és nyelvi szintjének megfelelő, egyszerű
    helyzetekben;

7. közöl alapvető személyes információkat magáról, egyszerű nyelvi
    elemek segítségével.

ÖNÁLLÓ NYELVTANULÁS

A nevelési-oktatási szakasz végére a tanuló:

1. új szavak, kifejezések tanulásakor ráismer a már korábban tanult
    szavakra, kifejezésekre;

2. szavak, kifejezések tanulásakor felismeri, ha új elemmel találkozik
    és rákérdez, vagy megfelelő tanulási stratégiával törekszik a
    megértésre;

3. a célok eléréséhez társaival rövid feladatokban együttműködik;

4. egy feladat megoldásának sikerességét segítséggel értékelni tudja;

5. felismeri az idegen nyelvű írott, olvasott és hallott tartalmakat a
    tanórán kívül is;

6. felhasznál és létrehoz rövid, nagyon egyszerű célnyelvi szövegeket
    szabadidős tevékenységek során;

7. alapvető célzott információt megszerez a tanult témákban tudásának
    bővítésére.

INTERKULTURALITÁS, ORSZÁGISMERET

A nevelési-oktatási szakasz végére a tanuló:

1. megismeri a főbb, az adott célnyelvi kultúrákhoz tartozó országok
    nevét, földrajzi elhelyezkedését, főbb országismereti jellemzőit;

2. ismeri a főbb, célnyelvi kultúrához tartozó, ünnepekhez kapcsolódó
    alapszintű kifejezéseket, állandósult szókapcsolatokat és
    szokásokat.

DIGITÁLIS ESZKÖZÖK ÉS FELÜLETEK HASZNÁLATA IDEGEN NYELVEN

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a tanult nyelvi elemeket életkorának megfelelő digitális
    tartalmakban, digitális csatornákon olvasott vagy hallott nagyon
    egyszerű szövegekben is;

2. létrehoz nagyon egyszerű írott, pár szavas szöveget szóban vagy
    írásban digitális felületen.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. szóban és írásban megold változatos kihívásokat igénylő feladatokat
    az élő idegen nyelven;

2. szóban és írásban létrehoz rövid szövegeket, ismert nyelvi
    eszközökkel, a korának megfelelő szövegtípusokban;

3. értelmez korának és nyelvi szintjének megfelelő hallott és írott
    célnyelvi szövegeket az ismert témákban és szövegtípusokban;

4. a tanult nyelvi elemek és kommunikációs stratégiák segítségével
    írásbeli és szóbeli interakciót folytat, valamint közvetít az élő
    idegen nyelven;

5. kommunikációs szándékának megfelelően alkalmazza a tanult nyelvi
    funkciókat és a megszerzett szociolingvisztikai, pragmatikai és
    interkulturális jártasságát;

6. nyelvtudását egyre inkább képes fejleszteni tanórán kívüli
    helyzetekben is különböző eszközökkel és lehetőségekkel;

7. használ életkorának és nyelvi szintjének megfelelő hagyományos és
    digitális alapú nyelvtanulási forrásokat és eszközöket;

8. alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra,
    ismeretszerzésre hagyományos és digitális csatornákon;

9. törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és
    intonáció megközelítésére;

10. érti a nyelvtudás fontosságát, és motivációja a nyelvtanulásra
    tovább erősödik.

SZÖVEGALKOTÁS IDEGEN NYELVEN

BESZÉDKÉSZSÉG: ÖSSZEFÜGGŐ BESZÉD

A nevelési-oktatási szakasz végére a tanuló:

1. aktívan részt vesz az életkorának és érdeklődésének megfelelő
    gyermek-, illetve ifjúsági irodalmi alkotások közös előadásában;

2. egyre magabiztosabban kapcsolódik be történetek kreatív alakításába,
    átfogalmazásába kooperatív munkaformában;

3. elmesél rövid történetet, egyszerűsített olvasmányt egyszerű nyelvi
    eszközökkel, önállóan, a cselekményt lineárisan összefűzve;

4. egyszerű nyelvi eszközökkel, felkészülést követően röviden,
    összefüggően beszél az ajánlott tématartományokhoz tartozó témákban,
    élőszóban és digitális felületen;

5. képet jellemez röviden, egyszerűen, ismert nyelvi fordulatok
    segítségével, segítő tanári kérdések alapján, önállóan;

6. változatos, kognitív kihívást jelentő szóbeli feladatokat old meg
    önállóan vagy kooperatív munkaformában, a tanult nyelvi eszközökkel,
    szükség szerint tanári segítséggel, élőszóban és digitális
    felületen.

ÍRÁSKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló

1. megold játékos és változatos írásbeli feladatokat rövid szövegek
    szintjén;

2. rövid, egyszerű, összefüggő szövegeket ír a tanult nyelvi
    szerkezetek felhasználásával az ismert szövegtípusokban, az ajánlott
    tématartományokban;

3. rövid szövegek írását igénylő kreatív munkát hoz létre önállóan;

4. rövid, összefüggő, papíralapú vagy IKT-eszközökkel segített írott
    projektmunkát készít önállóan vagy kooperatív munkaformákban;

5. a szövegek létrehozásához nyomtatott, illetve digitális alapú
    segédeszközt, szótárt használ;

6. a részletes tanulási eredményekben foglalt szövegtípusok
    jellegzetességeit követi.

SZÖVEGÉRTÉS IDEGEN NYELVEN

BESZÉDÉRTÉS

A nevelési-oktatási szakasz végére a tanuló

1. megérti a szintjének megfelelő, kevésbé ismert elemekből álló,
    nonverbális vagy vizuális eszközökkel támogatott célnyelvi
    óravezetést és utasításokat, kérdéseket;

2. értelmezi az életkorának és nyelvi szintjének megfelelő, egyszerű,
    hangzó szövegben a tanult nyelvi elemeket;

3. értelmezi az életkorának megfelelő, élőszóban vagy digitális
    felületen elhangzó szövegekben a beszélők gondolatmenetét;

4. megérti a nem kizárólag ismert nyelvi elemeket tartalmazó, élőszóban
    vagy digitális felületen elhangzó rövid szöveg tartalmát;

5. kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő,
    élőszóban vagy digitális felületen elhangzó szövegből, és azokat
    összekapcsolja egyéb ismereteivel;

6. alkalmazza az életkorának és nyelvi szintjének megfelelő hangzó
    szöveget a változatos nyelvórai tevékenységek és a feladatmegoldás
    során;

7. értelmez életkorának megfelelő nyelvi helyzeteket hallott szöveg
    alapján;

8. felismeri a főbb, életkorának megfelelő hangzószöveg-típusokat;

9. hallgat az érdeklődésének megfelelő autentikus szövegeket
    elektronikus, digitális csatornákon, tanórán kívül is, szórakozásra
    vagy ismeretszerzésre.

OLVASÁSÉRTÉS

A nevelési-oktatási szakasz végére a tanuló

1. értelmezi az életkorának megfelelő szituációkhoz kapcsolódó, írott
    szövegekben megjelenő összetettebb információkat;

2. megérti a nem kizárólag ismert nyelvi elemeket tartalmazó rövid
    írott szöveg tartalmát;

3. kiemel, kiszűr konkrét információkat a nyelvi szintjének megfelelő
    szövegből, és azokat összekapcsolja más iskolai vagy iskolán kívül
    szerzett ismereteivel;

4. megkülönbözteti a főbb, életkorának megfelelő írott szövegtípusokat;

5. összetettebb írott instrukciókat értelmez;

6. alkalmazza az életkorának és nyelvi szintjének megfelelő írott,
    nyomtatott vagy digitális alapú szöveget a változatos nyelvórai
    tevékenységek és feladatmegoldás során;

7. a nyomtatott vagy digitális alapú írott szöveget felhasználja
    szórakozásra és ismeretszerzésre önállóan is;

8. érdeklődése erősödik a célnyelvi irodalmi alkotások iránt;

9. megért és használ szavakat, szókapcsolatokat a célnyelvi, az
    életkorának és érdeklődésének megfelelő hazai és nemzetközi legfőbb
    hírekkel, eseményekkel kapcsolatban

INTERAKCIÓ IDEGEN NYELVEN

BESZÉDKÉSZSÉG: SZÓBELI INTERAKCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. kommunikációt kezdeményez egyszerű hétköznapi témában, a
    beszélgetést követi, egyszerű, nyelvi eszközökkel fenntartja és
    lezárja;

2. az életkorának megfelelő mindennapi helyzetekben a tanult nyelvi
    eszközökkel megfogalmazott kérdéseket tesz fel, és válaszol a hozzá
    intézett kérdésekre;

3. véleményét, gondolatait, érzéseit egyre magabiztosabban fejezi ki a
    tanult nyelvi eszközökkel;

4. a tanult nyelvi elemeket többnyire megfelelően használja,
    beszédszándékainak megfelelően, egyszerű spontán helyzetekben;

5. váratlan, előre nem kiszámítható eseményekre, jelenségekre és
    történésekre is reagál egyszerű célnyelvi eszközökkel, személyes
    vagy online interakciókban;

6. bekapcsolódik a tanórán az interakciót igénylő nyelvi
    tevékenységekbe, abban társaival közösen részt vesz, a begyakorolt
    nyelvi elemeket tanári segítséggel a játék céljainak megfelelően
    alkalmazza.

ÍRÁSKÉSZSÉG: ÍRÁSBELI INTERAKCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. üzeneteket ír,

2. véleményét írásban, egyszerű nyelvi eszközökkel megfogalmazza, és
    arról írásban interakciót folytat.

INFORMÁCIÓKÖZVETÍTÉS IDEGEN NYELVEN

SZÓBELI TUDÁSMEGOSZTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. rövid, egyszerű, ismert nyelvi eszközökből álló kiselőadást tart
    változatos feladatok kapcsán, hagyományos vagy digitális alapú
    vizuális eszközök támogatásával;

2. felhasználja a célnyelvet tudásmegosztásra.

ÍRÁSBELI TUDÁSMEGOSZTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. találkozik az életkorának és nyelvi szintjének megfelelő célnyelvi
    ismeretterjesztő tartalmakkal;

2. néhány szóból vagy mondatból álló jegyzetet készít írott szöveg
    alapján.

MINDENNAPI IDEGENNYELV-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. egyszerűen megfogalmazza személyes véleményét, másoktól véleményük
    kifejtését kéri, és arra reagál, elismeri vagy cáfolja mások
    állítását, kifejezi egyetértését vagy egyet nem értését;

2. kifejez tetszést, nem tetszést, akaratot, kívánságot, tudást és nem
    tudást, ígéretet, szándékot, dicséretet, kritikát;

3. információt cserél, információt kér, információt ad;

4. kifejez kérést, javaslatot, meghívást, kínálást és ezekre reagálást;

5. kifejez alapvető érzéseket, például örömöt, sajnálkozást, bánatot,
    elégedettséget, elégedetlenséget, bosszúságot, csodálkozást,
    reményt;

6. kifejez és érvekkel alátámasztva mutat be szükségességet,
    lehetőséget, képességet, bizonyosságot, bizonytalanságot;

7. értelmez és használja az idegen nyelvű írott, olvasott és hallott
    tartalmakat a tanórán kívül is,

8. felhasználja a célnyelvet ismeretszerzésre;

9. használja a célnyelvet életkorának és nyelvi szintjének megfelelő
    aktuális témákban és a hozzájuk tartozó szituációkban;

10. találkozik életkorának és nyelvi szintjének megfelelő célnyelvi
    szórakoztató tartalmakkal;

11. összekapcsolja az ismert nyelvi elemeket egyszerű kötőszavakkal
    (például: és, de, vagy);

12. egyszerű mondatokat összekapcsolva mond el egymást követő
    eseményekből álló történetet, vagy leírást ad valamilyen témáról;

13. a tanult nyelvi eszközökkel és nonverbális elemek segítségével
    tisztázza mondanivalójának lényegét;

14. ismeretlen szavak valószínű jelentését szövegösszefüggések alapján
    kikövetkezteti az életkorának és érdeklődésének megfelelő, konkrét,
    rövid szövegekben;

15. alkalmaz nyelvi funkciókat rövid társalgás megkezdéséhez,
    fenntartásához és befejezéséhez;

16. nem értés esetén a meg nem értett kulcsszavak vagy fordulatok
    ismétlését vagy magyarázatát kéri, visszakérdez, betűzést kér;

17. megoszt alapvető személyes információkat és szükségleteket magáról
    egyszerű nyelvi elemekkel;

18. ismerős és gyakori alapvető helyzetekben, akár telefonon vagy
    digitális csatornákon is, többnyire helyesen és érthetően fejezi ki
    magát az ismert nyelvi eszközök segítségével.

ÖNÁLLÓ NYELVTANULÁS

NYELVTANULÁSI ÉS NYELVHASZNÁLATI STRATÉGIÁK

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan használ alapszintű nyelvtanulási és nyelvhasználati
    stratégiákat;

2. hibáit többnyire észreveszi és javítja;

3. ismer szavakat, szókapcsolatokat a célnyelven a témakörre jellemző,
    életkorának és érdeklődésének megfelelő más tudásterületen
    megcélzott tartalmakból.

NYELVTANULÁSI CÉLOK

A nevelési-oktatási szakasz végére a tanuló:

1. egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki
    magának;

2. céljai eléréséhez megtalálja és használja a megfelelő eszközöket;

3. céljai eléréséhez társaival párban és csoportban együttműködik.

HALADÁS ÉRTÉKELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. nyelvi haladását többnyire fel tudja mérni,

2. társai haladásának értékelésében segítően részt vesz.

VALÓS NYELVHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. a tanórán kívüli, akár játékos nyelvtanulási lehetőségeket
    felismeri, és törekszik azokat kihasználni;

2. felhasználja a célnyelvet szórakozásra és játékos nyelvtanulásra.

DIGITÁLIS NYELVHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. digitális eszközöket és felületeket is használ nyelvtudása
    fejlesztésére,

2. értelmez egyszerű, szórakoztató kisfilmeket

INTERKULTURALITÁS, ORSZÁGISMERET

CÉLNYELVI KULTÚRÁK

A nevelési-oktatási szakasz végére a tanuló:

1. megismeri a célnyelvi országok főbb jellemzőit és kulturális
    sajátosságait;

2. további országismereti tudásra tesz szert;

3. célnyelvi kommunikációjába beépíti a tanult interkulturális
    ismereteket;

4. találkozik célnyelvi országismereti tartalmakkal.

A HAZAI KULTÚRA KÖZVETÍTÉSE CÉLNYELVEN

A nevelési-oktatási szakasz végére a tanuló:

1. találkozik a célnyelvi, életkorának és érdeklődésének megfelelő
    hazai és nemzetközi legfőbb hírekkel, eseményekkel;

2. megismerkedik hazánk legfőbb országismereti és történelmi
    eseményeivel célnyelven.

CÉLNYELVI KULTÚRÁKHOZ KAPCSOLÓDÓ NYELVI ELEMEK

A nevelési-oktatási szakasz végére a tanuló:

1. a célnyelvi kultúrákhoz kapcsolódó alapvető tanult nyelvi elemeket
    használja;

2. idegen nyelvi kommunikációjában ismeri és használja a célnyelv főbb
    jellemzőit;

3. következetesen alkalmazza a célnyelvi betű és jelkészletet

DIGITÁLIS ESZKÖZÖK ÉS FELÜLETEK HASZNÁLATA IDEGEN NYELVEN

A nevelési-oktatási szakasz végére a tanuló:

1. egyénileg vagy társaival együttműködve szóban vagy írásban
    projektmunkát vagy kiselőadást készít, és ezeket digitális eszközök
    segítségével is meg tudja valósítani;

2. találkozik az érdeklődésének megfelelő akár autentikus szövegekkel
    elektronikus, digitális csatornákon tanórán kívül is.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. szóban és írásban is megold változatos kihívásokat igénylő,
    többnyire valós kommunikációs helyzeteket leképező feladatokat az
    élő idegen nyelven;

2. szóban és írásban létrehoz szövegeket különböző szövegtípusokban;

3. értelmez nyelvi szintjének megfelelő hallott és írott célnyelvi
    szövegeket kevésbé ismert témákban és szövegtípusokban is;

4. a tanult nyelvi elemek és kommunikációs stratégiák segítségével
    írásbeli és szóbeli interakciót folytat és tartalmakat közvetít
    idegen nyelven;

5. kommunikációs szándékának megfelelően alkalmazza a nyelvi funkciókat
    és megszerzett szociolingvisztikai, pragmatikai és interkulturális
    jártasságát;

6. nyelvtudását képes fejleszteni tanórán kívüli eszközökkel,
    lehetőségekkel és helyzetekben is, valamint a tanultakat és
    gimnáziumban a második idegen nyelv tanulásában is alkalmazza;

7. felkészül az aktív nyelvtanulás eszközeivel az egész életen át
    történő tanulásra;

8. használ hagyományos és digitális alapú nyelvtanulási forrásokat és
    eszközöket;

9. alkalmazza nyelvtudását kommunikációra, közvetítésre, szórakozásra,
    ismeretszerzésre hagyományos és digitális csatornákon;

10. törekszik a célnyelvi normához illeszkedő kiejtés, beszédtempó és
    intonáció megközelítésére;

11. beazonosítja nyelvtanulási céljait és egyéni különbségeinek
    tudatában, ezeknek megfelelően fejleszti nyelvtudását;

12. első idegen nyelvéből sikeresen érettségit tesz a céljainak
    megfelelő szinten.

SZÖVEGALKOTÁS IDEGEN NYELVEN

BESZÉDKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. visszaad tankönyvi vagy más tanult szöveget, elbeszélést, nagyrészt
    folyamatos és érthető történetmeséléssel, a cselekményt logikusan
    összefűzve;

2. összefüggően, érthetően és nagyrészt folyékonyan beszél az ajánlott
    tématartományokhoz tartozó és az érettségi témákban a tanult nyelvi
    eszközökkel, felkészülést követően;

3. beszámol saját élményen, tapasztalaton alapuló vagy elképzelt
    eseményről a cselekmény, a körülmények, az érzések és gondolatok
    ismert nyelvi eszközökkel történő rövid jellemzésével;

4. ajánlott tématartományhoz kapcsolódó képi hatás kapcsán saját
    gondolatait, véleményét és érzéseit is kifejti az ismert nyelvi
    eszközökkel;

5. összefoglalja ismert témában nyomtatott vagy digitális alapú
    ifjúsági tartalmak lényegét röviden és érthetően;

6. közép- és emelt szintű nyelvi érettségi szóbeli feladatokat old meg;

7. összefüggő, folyékony előadásmódú szóbeli prezentációt tart
    önállóan, felkészülést követően, az érettségi témakörök közül
    szabadon választott témában, IKT-eszközökkel támogatva
    mondanivalóját;

8. kreatív, változatos műfajú szövegeket alkot szóban, kooperatív
    munkaformákban;

9. beszámol akár az érdeklődési körén túlmutató környezeti eseményről a
    cselekmény, a körülmények, az érzések és gondolatok ismert nyelvi
    eszközökkel történő összetettebb, részletes és világos
    jellemzésével;

10. összefüggően, világosan és nagyrészt folyékonyan beszél az ajánlott
    tématartományhoz tartozó és az idevágó érettségi témákban, akár
    elvontabb tartalmakra is kitérve;

11. alkalmazza a célnyelvi normához illeszkedő, természeteshez közelítő
    kiejtést, beszédtempót és intonációt.

ÍRÁSKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. írásban röviden indokolja érzéseit, gondolatait, véleményét már
    elvontabb témákban;

2. leír összetettebb cselekvéssort, történetet, személyes élményeket,
    elvontabb témákban;

3. információt vagy véleményt közlő és kérő, összefüggő feljegyzéseket,
    üzeneteket ír;

4. alkalmazza a formális és informális regiszterhez köthető
    sajátosságokat;

5. használ szövegkohéziós és figyelemvezető eszközöket;

6. megold változatos írásbeli, feladatokat szövegszinten;

7. papíralapú vagy IKT-eszközökkel segített írott projektmunkát készít
    önállóan vagy kooperatív munkaformában;

8. összefüggő szövegeket ír önállóan, akár elvontabb témákban;

9. a szövegek létrehozásához nyomtatott vagy digitális segédeszközt,
    szótárt használ;

10. beszámol saját élményen, tapasztalaton alapuló, akár az érdeklődési
    körén túlmutató vagy elképzelt személyes eseményről a cselekmény, a
    körülmények, az érzések és gondolatok ismert nyelvi eszközökkel
    történő összetettebb, részletes és világos jellemzésével;

11. beszámol akár az érdeklődési körén túlmutató közügyekkel,
    szórakozással kapcsolatos eseményről a cselekmény, a körülmények, az
    érzések és gondolatok ismert nyelvi eszközökkel történő
    összetettebb, részletes és világos jellemzésével;

12. a megfelelő szövegtípusok jellegzetességeit követi.

SZÖVEGÉRTÉS IDEGEN NYELVEN

BESZÉDÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi a szintjének megfelelő célnyelvi, komplexebb tanári
    magyarázatokat a nyelvórákon;

2. megérti a célnyelvi, életkorának és érdeklődésének megfelelő hazai
    és nemzetközi hírek, események lényegét;

3. kikövetkezteti a szövegben megjelenő elvontabb nyelvi elemek
    jelentését az ajánlott témakörökhöz kapcsolódó témákban;

4. értelmezi a szövegben megjelenő összefüggéseket;

5. megérti, értelmezi és összefoglalja az összetettebb, a
    tématartományhoz kapcsolódó összefüggő hangzó szöveget, és értelmezi
    a szövegben megjelenő összefüggéseket;

6. megérti és értelmezi az összetettebb, az ajánlott témakörökhöz
    kapcsolódó összefüggő szövegeket, és értelmezi a szövegben megjelenő
    összefüggéseket;

7. megérti az ismeretlen nyelvi elemeket is tartalmazó hangzó szöveg
    lényegi tartalmát;

8. megérti a hangzó szövegben megjelenő összetettebb részinformációkat;

9. megérti az elvontabb tartalmú hangzószövegek lényegét, valamint a
    beszélők véleményét is;

10. alkalmazza a hangzó szövegből nyert információt feladatok megoldása
    során;

11. célzottan keresi az érdeklődésének megfelelő autentikus szövegeket
    tanórán kívül is, ismeretszerzésre és szórakozásra;

12. a tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét
    számára kevésbé ismert témákban és szituációkban is;

13. a tanult nyelvi elemek segítségével megérti a hangzó szöveg lényegét
    akár anyanyelvi beszélők köznyelvi kommunikációjában a számára
    kevésbé ismert témákban és szituációkban is;

14. megérti és értelmezi a legtöbb televíziós hírműsort;

15. megért szokványos tempóban folyó autentikus szórakoztató és
    ismeretterjesztő tartalmakat, változatos csatornákon.

OLVASÁSÉRTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. elolvas és értelmez nyelvi szintjének megfelelő irodalmi szövegeket;

2. megérti és értelmezi a lényeget az ajánlott tématartományokhoz
    kapcsolódó összefüggő, akár autentikus írott szövegekben;

3. megérti és értelmezi az összefüggéseket és a részleteket az ajánlott
    tématartományokhoz kapcsolódó összefüggő, akár autentikus írott
    szövegekben;

4. értelmezi a számára ismerős, elvontabb tartalmú szövegekben
    megjelenő ismeretlen nyelvi elemeket;

5. a szövegkörnyezet alapján kikövetkezteti a szövegben előforduló
    ismeretlen szavak jelentését;

6. megérti az ismeretlen nyelvi elemeket is tartalmazó írott szöveg
    tartalmát;

7. megérti és értelmezi az írott szövegben megjelenő összetettebb
    részinformációkat;

8. kiszűr konkrét információkat nyelvi szintjének megfelelő szövegből,
    és azokat összekapcsolja egyéb ismereteivel;

9. alkalmazza az írott szövegből nyert információt feladatok megoldása
    során;

10. keresi az érdeklődésének megfelelő, célnyelvi, autentikus szövegeket
    szórakozásra és ismeretszerzésre tanórán kívül is;

11. egyre változatosabb, hosszabb, összetettebb és elvontabb szövegeket,
    tartalmakat értelmez és használ.

INTERAKCIÓ IDEGEN NYELVEN

BESZÉDKÉSZSÉG: SZÓBELI INTERAKCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. részt vesz a változatos szóbeli interakciót és kognitív kihívást
    igénylő nyelvórai tevékenységekben;

2. szóban ad át nyelvi szintjének megfelelő célnyelvi tartalmakat valós
    nyelvi interakciót leképező szituációkban;

3. a társalgásba aktívan, kezdeményezően és egyre magabiztosabban
    bekapcsolódik az érdeklődési körébe tartozó témák esetén vagy az
    ajánlott tématartományokon belül;

4. társalgást kezdeményez, a megértést fenntartja, törekszik mások
    bevonására, és szükség esetén lezárja azt az egyes tématartományokon
    belül, akár anyanyelvű beszélgetőtárs esetében is;

5. a társalgást hatékonyan és udvariasan fenntartja, törekszik mások
    bevonására, és szükség esetén lezárja azt, akár ismeretlen
    beszélgetőtárs esetében is;

6. előkészület nélkül részt tud venni személyes jellegű, vagy
    érdeklődési körének megfelelő ismert témáról folytatott
    társalgásban,

7. érzelmeit, véleményét változatos nyelvi eszközökkel szóban
    megfogalmazza és arról interakciót folytat;

8. a mindennapi élet különböző területein, a kommunikációs helyzetek
    széles körében tesz fel releváns kérdéseket információszerzés
    céljából, és válaszol megfelelő módon a hozzá intézett célnyelvi
    kérdésekre;

9. aktívan, kezdeményezően és magabiztosan vesz részt a változatos
    szóbeli interakciót és kognitív kihívást igénylő nyelvórai
    tevékenységekben;

10. társaival a kooperatív munkaformákban és a projektfeladatok
    megoldása során is törekszik a célnyelvi kommunikációra;

11. egyre szélesebb körű témákban, nyelvi kommunikációt igénylő
    helyzetekben reagál megfelelő módon, felhasználva általános és
    nyelvi háttértudását, ismereteit, alkalmazkodva a társadalmi
    normákhoz;

12. váratlan, előre nem kiszámítható eseményekre, jelenségekre és
    történésekre jellemzően célnyelvi eszközökkel is reagál tanórai
    szituációkban;

13. szóban és írásban, valós nyelvi interakciók során jó
    nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő
    szinten vesz részt az egyes tématartományokban és az idetartozó
    érettségi témákban.

ÍRÁSKÉSZSÉG: ÍRÁSBELI INTERAKCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. informális és életkorának megfelelő formális írásos üzeneteket ír,
    digitális felületen is;

2. véleményét írásban, tanult nyelvi eszközökkel megfogalmazza és arról
    írásban interakciót folytat;

3. véleményét írásban változatos nyelvi eszközökkel megfogalmazza és
    arról interakciót folytat;

4. írásban átad nyelvi szintjének megfelelő célnyelvi tartalmakat valós
    nyelvi interakciók során;

5. írásban és szóban, valós nyelvi interakciók során jó
    nyelvhelyességgel, megfelelő szókinccsel, a természeteshez közelítő
    szinten vesz részt az egyes tématartományokban és az idetartozó
    érettségi témákban.

INFORMÁCIÓKÖZVETÍTÉS IDEGEN NYELVEN

SZÓBELI TUDÁSMEGOSZTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. összetett információkat ad át és cserél;

2. egyénileg vagy kooperáció során létrehozott projektmunkával
    kapcsolatos kiselőadást tart önállóan, összefüggő és folyékony
    előadásmóddal, digitális eszközök segítségével, felkészülést
    követően;

3. használ célnyelvi tartalmakat tudásmegosztásra.

ÍRÁSBELI TUDÁSMEGOSZTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. ismer más tantárgyi tartalmakat, részinformációkat célnyelven,

2. összefoglal és lejegyzetel, írásban közvetít rövid olvasott vagy
    hallott szövegeket;

3. környezeti témákban a kommunikációs helyzetek széles körében
    hatékonyan ad át és cserél információt;

4. írott szöveget igénylő projektmunkát készít olvasóközönségnek;

5. írásban közvetít célnyelvi tartalmakat valós nyelvi interakciót
    leképező szituációkban.

MINDENNAPI IDEGENNYELV-HASZNÁLAT

SZOCIOLINGVISZTIKAI MEGFELELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével további alapvető érzéseket fejez ki (pl. aggódást,
    félelmet, kételyt);

2. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez érdeklődést és érdektelenséget, szemrehányást,
    reklamálást;

3. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez kötelezettséget, szándékot, kívánságot,
    engedélykérést, feltételezést;

4. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez ítéletet, kritikát, tanácsadást;

5. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez segítségkérést, ajánlást és ezekre történő
    reagálást;

6. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez ok-okozat viszony vagy cél meghatározását;

7. tanult kifejezések alkalmazásával és az alapvető nyelvi szokások
    követésével kifejez emlékezést és nem emlékezést.

PRAGMATIKAI MEGFELELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. összekapcsolja a mondatokat megfelelő kötőszavakkal, így követhető
    leírást ad, vagy nem kronológiai sorrendben lévő eseményeket is
    elbeszél;

2. a kohéziós eszközök szélesebb körét alkalmazza szóbeli vagy írásbeli
    megnyilatkozásainak érthetőbb, koherensebb szöveggé szervezéséhez;

3. több különálló elemet összekapcsol összefüggő lineáris
    szempontsorrá.

KOMMUNIKÁCIÓS STRATÉGIÁK

A nevelési-oktatási szakasz végére a tanuló:

1. képes rendszerezni kommunikációját: jelzi szándékát, kezdeményez,
    összefoglal és lezár;

2. használ kiemelést, hangsúlyozást, helyesbítést;

3. körülírással közvetíti a jelentéstartalmat, ha a megfelelő szót nem
    ismeri;

4. ismert témákban a szövegösszefüggés alapján kikövetkezteti az
    ismeretlen szavak jelentését, megérti az ismeretlen szavakat is
    tartalmazó mondat jelentését;

5. félreértéshez vezető hibáit kijavítja, ha beszédpartnere jelzi a
    problémát; a kommunikáció megszakadása esetén más stratégiát
    alkalmazva újrakezdi a mondandóját;

6. a társalgás vagy eszmecsere menetének fenntartásához alkalmazza a
    rendelkezésére álló nyelvi és stratégiai eszközöket;

7. nem értés esetén képes a tartalom tisztázására.

VALÓS NYELVHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. mondanivalóját kifejti kevésbé ismerős helyzetekben is nyelvi
    eszközök széles körének használatával;

2. a tanult nyelvi elemeket adaptálni tudja kevésbé begyakorolt
    helyzetekhez is;

3. szóbeli és írásbeli közlései során változatos nyelvi struktúrákat
    használ;

4. a tanult nyelvi funkciókat és nyelvi eszköztárát életkorának
    megfelelő élethelyzetekben megfelelően alkalmazza;

5. szociokulturális ismeretei (például célnyelvi társadalmi szokások,
    testbeszéd) már lehetővé teszik azt, hogy társasági szempontból is
    megfelelő kommunikációt folytasson;

6. szükség esetén eltér az előre elgondoltaktól, és mondandóját a
    beszédpartnerekhez, hallgatósághoz igazítja;

7. az ismert nyelvi elemeket vizsgahelyzetben is használja.

ÖNÁLLÓ NYELVTANULÁS

NYELVTANULÁSI ÉS NYELVHASZNÁLATI STRATÉGIÁK

A nevelési-oktatási szakasz végére a tanuló:

1. megértést nehezítő hibáit önállóan javítani tudja;

2. nyelvtanulási céljai érdekében alkalmazza a tanórán kívüli
    nyelvtanulási lehetőségeket;

3. célzottan keresi az érdeklődésének megfelelő autentikus szövegeket
    tanórán kívül is, ismeretszerzésre és szórakozásra;

4. felhasználja a célnyelvű, legfőbb hazai és nemzetközi híreket
    ismeretszerzésre és szórakozásra;

5. használ célnyelvi elemeket más tudásterületen megcélzott
    tartalmakból;

6. használ célnyelvi tartalmakat ismeretszerzésre;

7. használ ismeretterjesztő anyagokat nyelvtudása fejlesztésére;

8. hibáit az esetek többségében önállóan is képes javítani;

9. hibáiból levont következtetéseire többnyire épít nyelvtudása
    fejlesztése érdekében.

NYELVTANULÁSI CÉLOK

A nevelési-oktatási szakasz végére a tanuló:

1. egy összetettebb nyelvi feladat, projekt végéig tartó célokat tűz ki
    magának;

2. megfogalmaz hosszú távú nyelvtanulási célokat saját maga számára;

3. nyelvtanulási céljai érdekében tudatosabban foglalkozik a
    célnyelvvel;

4. céljai eléréséhez megtalálja és használja a megfelelő eszközöket,
    módokat;

5. céljai eléréséhez társaival párban és csoportban is együttműködik;

6. beazonosít nyelvtanulási célokat és ismeri az ezekhez tartozó
    nyelvtanulási és nyelvhasználati stratégiákat;

7. használja a nyelvtanulási és nyelvhasználati stratégiákat
    nyelvtudása fenntartására és fejlesztésére;

8. hatékonyan alkalmazza a tanult nyelvtanulási és nyelvhasználati
    stratégiákat;

9. céljai eléréséhez önszabályozóan is dolgozik;

10. az első idegen nyelvből sikeres érettségit tesz legalább
    középszinten.

A HALADÁS ÉRTÉKELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. nyelvi haladását fel tudja mérni;

2. használ önértékelési módokat nyelvtudása felmérésére;

3. egyre tudatosabban használja az ön-, tanári, vagy társai értékelését
    nyelvtudása fenntartására és fejlesztésére;

4. használja az ön-, tanári, vagy társai értékelését nyelvtudása
    fenntartására és fejlesztésére;

5. hiányosságait, hibáit felismeri, azokat egyre hatékonyabban
    kompenzálja, javítja a tanult stratégiák felhasználásával.

VALÓS NYELVHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. nyelvtanulási céljai érdekében él a valós nyelvhasználati
    lehetőségekkel;

2. használja a célnyelvet életkorának és nyelvi szintjének megfelelő
    aktuális témákban és a hozzájuk tartozó szituációkban;

3. az ismert nyelvi elemeket vizsgahelyzetben is használja;

4. beszéd- és írásprodukcióját tudatosan megtervezi, hiányosságait
    igyekszik kompenzálni;

5. nyelvi produkciójában és recepciójában önállóságot mutat, és egyre
    kevesebb korlát akadályozza.

DIGITÁLIS NYELVHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. törekszik releváns digitális tartalmak használatára
    beszédkészségének, szókincsének és kiejtésének továbbfejlesztése
    céljából;

2. digitális eszközöket és felületeket is magabiztosan használ
    nyelvtudása fejlesztésére;

3. digitális eszközöket és felületeket is használ a célnyelven
    ismeretszerzésre és szórakozásra.

INTERKULTURALITÁS, ORSZÁGISMERET

CÉLNYELVI KULTÚRÁK

A nevelési-oktatási szakasz végére a tanuló:

1. alkalmazza a célnyelvi kultúráról megszerzett ismereteit informális
    kommunikációjában,

2. ismeri a célnyelvi országok történelmének és jelenének legfontosabb
    vonásait,

3. tájékozott a célnyelvi országok jellemzőiben és kulturális
    sajátosságaiban;

4. tájékozott, és alkalmazni is tudja a célnyelvi országokra jellemző
    alapvető érintkezési és udvariassági szokásokat.

HAZAI KULTÚRA KÖZVETÍTÉSE CÉLNYELVEN

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és keresi a főbb hasonlóságokat és különbségeket saját
    anyanyelvi és a célnyelvi közösség szokásai, értékei, attitűdjei és
    meggyőződései között;

2. átadja célnyelven a magyar értékeket;

3. ismeri a célnyelvi és saját hazájának kultúrája közötti
    hasonlóságokat és különbségeket;

4. interkulturális tudatosságára építve felismeri a célnyelvi és saját
    hazájának kultúrája közötti hasonlóságokat és különbségeket, és a
    magyar értékek átadására képessé válik;

5. környezetének kulturális értékeit célnyelven közvetíti.

CÉLNYELVI KULTÚRÁKHOZ KAPCSOLÓDÓ NYELVI ELEMEK

A nevelési-oktatási szakasz végére a tanuló:

1. kikövetkezteti a célnyelvi kultúrákhoz kapcsolódó egyszerű,
    ismeretlen nyelvi elemeket;

2. a célnyelvi kultúrákhoz kapcsolódó tanult nyelvi elemeket
    magabiztosan használja;

3. interkulturális ismeretei segítségével társasági szempontból is
    megfelelő kommunikációt folytat írásban és szóban;

NYELVI VÁLTOZATOK

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a legfőbb hasonlóságokat és különbségeket az ismert nyelvi
    változatok között;

2. megfogalmaz főbb hasonlóságokat és különbségeket az ismert nyelvi
    változatok között;

3. alkalmazza a nyelvi változatokról megszerzett ismereteit informális
    kommunikációjában;

4. megérti a legfőbb nyelvi dialektusok egyes elemeit is tartalmazó
    szóbeli közléseket.

DIGITÁLIS ESZKÖZÖK ÉS FELÜLETEK HASZNÁLATA IDEGEN NYELVEN

A nevelési-oktatási szakasz végére a tanuló:

1. digitális eszközökön és csatornákon keresztül is alkot szöveget
    szóban és írásban;

2. digitális eszközökön és csatornákon keresztül is megérti az ismert
    témához kapcsolódó írott vagy hallott szövegeket;

3. digitális eszközökön és csatornákon keresztül is alkalmazza az
    ismert témához kapcsolódó írott vagy hallott szövegeket;

4. digitális eszközökön és csatornákon keresztül is folytat célnyelvi
    interakciót az ismert nyelvi eszközök segítségével;

5. digitális eszközökön és csatornákon keresztül is folytat a
    természeteshez közelítő célnyelvi interakciót az ismert nyelvi
    eszközök segítségével;

6. digitális eszközökön és csatornákon keresztül is megfelelő nyelvi
    eszközökkel alkot szöveget szóban és írásban;

7. alkalmazza az életkorának és érdeklődésének megfelelő digitális
    műfajok főbb jellemzőit.

***II.3.2.2. KLASSZIKUS IDEGEN NYELV***

***A) ALAPELVEK, CÉLOK***

A klasszikus idegen nyelvek esetében a tanulás társadalmi hasznossága
különbözik az élő idegen nyelvekétől. Míg az élő nyelv vonatkozásában az
a cél, hogy a tanulás során megszerzett nyelvi készségek a gyakorlatban,
tehát hétköznapi, iskolán kívüli tevékenységek során közvetlenül
hasznosuljanak, azaz a nyelvtanuló a nyelvet ismeretszerzés és
kommunikáció céljaira használja, a klasszikus nyelvek tanítását úgy kell
megtervezni, hogy a tanulók kisebb része fogja csak az órán megszerzett
készségeket közvetlen kommunikációs eszközként használni. A klasszikus
nyelvek ismerete több hivatásban közvetlenül hasznosul, és az
információszerzés eszközévé válik. A klasszikus nyelvek tanításának
éppen ezért legfontosabb része a nyelvi tudatosság és a problémamegoldó
kompetenciák fejlesztése. A képzés fejleszti a nyelvi tudatosságot, a
kulturális nyitottságot és az elemzőkészség segítségével a nyelvi
megértés módszertanát. A klasszikus nyelvek oktatásában olyan
ismereteknek is helyük van, amelyek az élő idegen nyelvek korszerű
tanításában nem kapnak szerepet. Egy klasszikus nyelv tanulása három,
egymással nagyjából egyenrangú területen fejleszti a képességeket: az
adott klasszikus nyelv ismerete, a nyelvet hordozó kultúrához kapcsolódó
tapasztalatok és az általános nyelvi kompetenciák.

A klasszikus nyelvvel megismerkedő tanuló önálló nyelvhasználóvá válik,
tehát a megszerzett nyelvi ismereteit képes önállóan alkalmazni, és
képes valamely klasszikus nyelven megfogalmazott szöveget önállóan
értelmezni. Használja az értelmezéshez ma szokásos eszközöket, akár
nyomtatott formában, akár digitális formában állnak rendelkezésére. A
klasszikus nyelvek tanítása során rövidebb szövegek alapos, pontos és
mindenre kiterjedő értelmezése az alapvető eszköz: a koncentrált
figyelem, az apró nyelvi különbségek felismerése és tudatosítása
fejleszti a kognitív képességek tág körét.

A klasszikus nyelvek tanulója a klasszikus kultúrára vonatkozó ismeretei
révén felismeri, hogy a különböző civilizációk történetileg kialakult és
változékony rendszerek, melyeket csak az elvonatkoztatás és az
önreflexió eszközeivel értelmezhetünk. A viszonyítás fontosságának
megértése hozzájárul a saját kultúra mélyebb megértéséhez is. A
klasszikus nyelvek ismerete ezen túlmenően az európai és benne a magyar
művelődés hagyományainak mélyebb megismeréséhez vezet, segít megérteni a
múltat mint a jelen előzményét, erősíti a kulturális identitást.

A klasszikus nyelvek oktatása a nyelvet elsődlegesen nem kommunikációs
eszköznek, hanem információs rendszernek tekinti, ezért a közvetlen
kommunikáció helyett a hangsúlyt a grammatikai jelek jelentéshordozó
funkciójának megértetésére és a rendszerszerűség bemutatására helyezi.
Ennek tudatosítása és elsajátítása nagymértékben fejleszti az analitikai
készséget, amely a nyelvi tudatosság egyik alapfeltétele. Segíti ezáltal
az anyanyelv működésének jobb megértését, és előkészíti újabb idegen
nyelvek elsajátítását.

A klasszikus idegen nyelv tanulásának célja, hogy a tanuló:

1. a második idegen nyelvként választott klasszikus nyelvből a
    gimnázium végére eléri a KER A2 szintet;

2. a tanulási folyamat végére átlátja az adott klasszikus nyelv
    szerkezetét;

3. alkalmassá válik a nyelvi szintjén lévő, klasszikus nyelven írott
    szövegek önálló olvasására, értelmezésére;

4. anyanyelvén meg tudja fogalmazni az olvasott szöveg nyelvi és
    kulturális sajátosságait;

5. képes önálló szótárhasználatra;

6. ismeri a nyelvleírás terminológiáit.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON***

1. Adaptált és könnyített szövegek

2. A klasszikus normáknak nagyjából megfelelő nem klasszikus szövegek

3. A tanulók nyelvi szintjének megfelelő klasszikus szövegek

4. Kitekintésként: a tanulók nyelvi szintjén túlmutató nagy jelentőségű
    szövegek

5. A magyar kultúra szempontjából kiemelkedően fontos szövegek

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. képes ismeretlen klasszikus nyelvű szövegben is tájékozódni,
    felismeri annak alapvető grammatikai struktúráit (például alany,
    állítmány, egyeztetett jelzős szerkezetek);

2. ismeri az adott klasszikus nyelv alapvető grammatikai szerkezetét,
    meg tudja nevezni a grammatikai funkciókat;

3. rendelkezik olyan szókinccsel, amely lehetővé teszi, hogy
    összefüggéseket ismerjen fel egy szövegben;

4. szótár segítségével képes nyelvi szintjének megfelelő szöveg
    fordítására;

5. tisztában van saját világának és az ókornak mint idegen világnak az
    alapvető kulturális különbözőségével;

6. ismeri a tanult klasszikus nyelv továbbélésének legfontosabb
    területeit;

7. magyar nyelven képes beszélni az olvasott szövegekről.

SZÖVEGÉRTÉS

NYELVI CÉLOK

A nevelési-oktatási szakasz végére a tanuló:

1. képes a mondatnál magasabb szintű szövegalkotó struktúrák
    felismerésére is;

2. meg tudja határozni a szöveg alapvető kommunikációs funkcióit.

GRAMMATIKAI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a tanult klasszikus nyelv grammatikai rendszerét, beleértve a
    legfontosabb rendhagyóságokat, az igeneves szerkezeteket, illetve a
    mondattan alapvető jelenségeit;

2. képes a grammatika és a jelentés összekapcsolására;

3. ismeri a grammatikai eszközök elnevezését.

KÖZVETÍTŐ KÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. képes az olvasott szöveg alapvető üzenetét megfogalmazni magyar
    nyelven;

2. képes az irodalmi eszközök, a stílust hordozó retorika és a jelentés
    összefüggésének felismerésére;

3. képes megjelölni a szöveg nem fordítható retorikai eszköztárát is.

KULTURÁLIS ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. rendelkezik a szövegek értelmezéséhez szükséges kulturális
    ismeretekkel.

2. képes arra, hogy megmutassa a mai kultúránktól idegen elemeket a
    klasszikus nyelveket létrehozó kultúrákban, és ezekről képes
    reflektált véleményt alkotni.

*II.3.3. Matematika*

***A) ALAPELVEK, CÉLOK***

A matematika tanulásának legfontosabb célja, hogy a tanuló:

1. megtapasztalja a matematika értékeit, hasznosságát, szépségét;

2. megismerje a matematikai gondolkodás természetét és a matematika
    alapvető sajátosságait;

3. fejlessze a szövegértését, a szövegalkotó és absztrakciós képességét
    a matematika nyelvének és szimbólumainak szóbeli és írásbeli
    alkalmazása során;

4. fejlessze a számolási készségét, a modellezési, a problémamegoldó és
    döntési képességét;

5. fejlessze a logikus, pontos, kreatív, mérlegelő, stratégiai és
    rendszerező gondolkodását;

6. alkalmazható tudásra tegyen szert.

***A tantárgy tanításának specifikus jellemzői az 1--4. évfolyamon***

Az alapfokú képzés első évfolyamát kezdő, jellemzően 6-7 éves
kisiskolások gondolkodása konkrétumokhoz kötött. A tanuló matematikai
tartalmú tevékenységei során alakulnak az ismeretei és formálódik a
gondolkodása, amelyet az egyedi tapasztalatokból kiemelkedő jellemzők
(tulajdonságok és összefüggések) megismerése vezet majd az
általánosítások és az absztrakció felé. Az 1--4. évfolyamon a matematika
műveléséhez szükséges ismeretek kialakítása, a matematikai -- a
számfogalomhoz, a számérzékhez, a mentális számegyeneshez kapcsolódó --
képességek kibontakoztatása, az alapkészségek kialakítása alapvető
feladat úgy, hogy a spirálisan épülő tananyag fogalmai egy megértett,
jól működtethető ismeretrendszert alapozzanak meg.

A matematika tanulásának alapvető módszere a valóságon alapuló,
személyes, cselekvő tapasztalatszerzés, amely a különböző érzékszervek
bevonásával, mozgással, valamint szemléletükben és matematikai
tartalmukban egyaránt változatos eszközök használatával, játékokkal
valósul meg.

Ebben az alapozó nevelési-oktatási szakaszban a matematika egységes és
széles alapozását a további lépések átgondolt megtervezésével kell
megvalósítani. A tevékenységek során működő alkotó gondolkodásnak
kiemelt szerepe van a megértésben. A tárgyi tevékenységek és a
kapcsolódó változatos képi ábrázolások alapozzák meg a későbbi
absztrakciót. A konkrétumokhoz való sokszori vissza-visszalépések során
alakulnak a matematikai fogalmak, összefüggések és eljárások. Ezek
alapozzák meg a felső tagozaton és középiskolában megjelenő szimbolikus
gondolkodást.

A tanuló ebben a nevelési-oktatási szakaszban találkozik olyan egyszerű
problémákkal, amelyek megoldásában szerepet játszik a megfigyelés, az
értelmezés, az összefüggések felfedezése, ezáltal fejlődik gondolkodása,
problémamegoldó képessége. Már a rendszerező készségek fejlődésének
kezdeti fázisában is képes a lehetőségek megkülönböztetésére, azok
rendszerezett felsorolására. Alsó tagozaton az új elemeket tartalmazó,
problémát jelentő helyzetek nagyrészt örömteli tevékenységekben, játékos
helyzetekben merülnek fel, a tanuló így gyakorolhatja, alkalmazhatja
megszerzett ismereteit, fejlesztheti problémamegoldó, rendszerező és
döntési képességeit. Elkezdi megtapasztalni, hogy a matematika
segítségével hogyan lehet leírni a közvetlen környezete természeti,
vizuális, technikai jelenségeit, tevékenységeit.

A matematikai nevelés szoros kapcsolatban van a zenei, mozgásos és
nyelvi fejlesztéssel.

A mindennapi kommunikáció során a tanuló fokozatosan tanulja meg és
alkalmazza a matematika nyelvét, a relációszókincset, a viszonyok
kifejezését. Kezdetben megmutatással, rajzzal, mozgással, saját
szavakkal közli gondolatait, és ezt lehet fokozatosan egyre pontosabbá,
szakszerűbbé tenni.

A tanuló fokozatosan tanulja meg a kooperatív tevékenységek során az
együttműködést, egymás elfogadását. Kérdéseket tesz fel, meghallgatja,
megfigyeli a választ, mások véleményét; a tévedéseket tolerálja; ezekkel
párhuzamosan érvelni tanul saját véleménye mellett.

Szükséges a tanulók egyéni szintjének, adottságainak, képességeinek
megfelelő differenciálása. Ennek formája a tevékenykedtetés, az
eszközhasználat megválasztása, a többszintű problémafelvetés, a
digitális tananyagok által kínált fejlesztő lehetőségek alkalmazása, az
időbeli korlátok kiiktatása, a segítségnyújtás, a páros vagy csoportos
munka szervezése és az értékelés.

A tanuló a digitális eszközöket lehetőség szerint már ebben a
nevelési-oktatási szakaszban a tanulás, gyakorlás szolgálatába állítja:
a műveletek, a problémamegoldás gyakorlására számítógépes fejlesztő
játékokat használ.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

Az alapfokú képzés első, a matematikai alapkészségek kialakítását
legfőbb célként megjelölő nevelési-oktatási szakaszát követően az 5--8.
évfolyamon a matematika tanulása-tanítása során a tudástartalmak
fokozatosan válnak egyre elvontabbá. A konkrét tárgyi tevékenységekből
kiindulva a képi szemléltetések, ábrázolások mellett megjelennek a
szimbolikus modellek. A tanuló a fogalmak, jelenségek elemzése útján
eljut azok megértésen alapuló meghatározásához, a definíciók
előkészítése során tulajdonságokat, sejtéseket fogalmaz meg, s kialakul
a megoldást alátámasztó indoklás igénye. Felismeri a matematika kisebb
egységeinek belső struktúráját.

A tanítás fő módszere továbbra is a felfedeztetés, a konkrét
tevékenységből, játékból, hétköznapi szituációból fakadó indukció. A
tanulási tevékenység és problémamegoldás során a tanulót ösztönözni kell
egyszerű problémák felfedezésére, megfogalmazására és a mindennapi
életből vett szöveges problémák matematikai szempontú értelmezésére. A
tanuló konkrét helyzetek megoldására képi és szimbolikus modelleket,
stratégiákat alkalmaz és alkot, ezáltal fejlődik problémamegoldó és
problémaalkotó képessége.

A kombinatív képességek területén a lehetőségek strukturált
felsorolásából fokozatosan kialakulnak a rendszerezést segítő konkrét
eszközök, stratégiák alkalmazásának készségei.

Felső tagozatban az ismert számok köre bővül a törtekkel és a negatív
számokkal úgy, hogy a tanuló ezekkel műveleteket tud végezni. A
tanulás-tanítás egyik lényeges elvárása, hogy a különböző, szöveggel,
számokkal megadott matematikai szituációk képi, majd szimbolikus
modelljeinek bevezetése fokozatos legyen. A tanuló a megismert
szimbólumokkal egyszerű műveleteket végez, ismeri ezek tulajdonságait.

Az 5--8. évfolyamon a természettudományi, a digitális technológiai és a
gazdasági ismeretek tanulási-tanítási tartalmakban való megjelenése
lehetővé teszi a matematika alkalmazhatóságának, hasznosságának
bemutatását.

Fejlődnek a tanuló készségei a matematikai kommunikáció terén is. A
matematikai kifejezéseket helyesen használja, a fogalmakat értelmezi,
megmagyarázza, gyakorlati helyzetekben jól alkalmazza. Ismereteit
összefoglalva prezentálni tudja.

A tanuló a közös munkában tevékenyen részt vesz. Eseti feladatokban és
projektekben mások véleményét elfogadja, és ha különbözik a véleményük,
igyekszik érvekkel meggyőzni társait. Az új fogalmak, magasabb szintű
absztrakciót igénylő tudástartalmak bevezetésekor az egyéni
adottságokhoz, ismeretekhez alkalmazkodó differenciálás biztosítja a
megfelelő tempójú haladást annak a tanulónak, akinél ezek a lépések
hosszabb időt, több szemléltetést igényelnek. Ezzel a lassabban haladó
tanuló sem veszíti el érdeklődését és reményét a matematika megértése
iránt.

A matematikai fejlesztő játékok és a számítógép, illetve más digitális
eszközök mellett a tanuló megismerkedik olyan matematikai szoftverekkel,
amelyek a matematikai tudást és a digitális kompetenciákat együtt
fejlesztik.

Ebben a nevelési-oktatási szakaszban az ellenőrzés, értékelés csak a
tanult ismeretek alkalmazására terjed ki.

***A tantárgy tanításának specifikus jellemzői a 9--12. évfolyamon***

A matematika tanulása-tanítása tekintetében az egyik legfőbb feladat a
középfokú képzés évfolyamain a tanuló önálló, rendszerezett, logikus
gondolkodásának kialakítása, fejlesztése. A 9. évfolyamtól kezdve a
spirális felépítésnek megfelelően -- a meglévő készségekre, képességekre
és ismeretekre alapozva -- fokozatosan, egyre absztraktabb formában épül
fel a matematika belső struktúrája (fogalmak definíciója, tételek,
bizonyítások).

Az alapfokú képzés nevelési-oktatási szakaszait jellemző tanuláshoz és
tanításhoz képest a 9--12. évfolyamokon fokozatosan hangsúlyosabbá válik
a matematika deduktív jellege, de az új fogalmakat, ismereteket továbbra
is szemléltetéssel, tapasztalással, tanulói tevékenységekre építve, a
valósághoz kapcsolva kell bevezetni.

Jól megválasztott problémák tárgyalása során válik szükségessé az új
fogalmak bevezetése és pontos definiálása, valamint tanári irányítással
a tételek, általános összefüggések is felfedeztethetők a tanulókkal.
Ezen folyamat során fejlődik a szintetizáló és modellalkotó képesség. A
felfedezett tételek és összefüggések egy része bizonyítás nélkül is
gyarapítja a matematikai eszköztárat. Néhány tétel bizonyítása azonban
elengedhetetlen része a matematika tanításának, hiszen a bizonyításokon
keresztül mutatható meg a matematika logikus és következetes felépítése.
Az új fogalmak megalkotása, az összefüggések, stratégiák felfedezése és
az ismereteknek feladatok, problémák megoldása során történő tudatos
alkalmazása fejleszti a kombinatív készséget, a meglévő ismeretek
mobilizálásának készségeit, a problémamegoldó gondolkodás eltérő
típusainak adekvát használatát. A matematika tanulásának-tanításának
egyik célja, hogy fejlődjön a tanuló mérlegelő gondolkodása, az adatok
elemzését, szintézisét és értékelését lehetővé tevő készségek rendszere.

Ebben a nevelési-oktatási szakaszban az ismert számok köre az
irracionális számokkal bővül, új műveletek bevezetésére kerül sor a
permanenciaelv alapján, a tanuló egyre inkább képes lesz szimbólumokkal
műveleteket végezni.

A matematika a maga hagyományos és modern eszközeivel segítséget ad a
természettudományok, az informatika, a technika és a humán tanulási
területek ismeretanyagának tanulmányozásához, a mindennapi problémák, a
természeti és a gazdasági folyamatok értelmezéséhez és kezeléséhez.

A tanuló a matematika szaknyelvét érti és tudatosan használja.
Életkorának megfelelő matematikai, matematikatörténeti szöveget képes
önállóan olvasni, értelmezni. Mind írásban, mind szóban képes
gondolatait a matematika szaknyelvének megfelelő alkalmazásával közölni.
A tanuló különböző forrásokat (tankönyv, függvénytáblázat, saját
jegyzet, digitális források) használhat az órákon és a számonkérések
alkalmával bizonyos tételek, azonosságok, képletek felidézésére.

A tanuló társaival közösen tervez és hajt végre kooperatív
tevékenységeket, projekteket. A közös munkában érvel, képes a vitára, az
érvei ütköztetésére.

Ebben az életkorban is érvényesül a tanuló érdeklődésének, adottságának,
absztrakciós szintjének megfelelő differenciálás. Ez jelentheti a
Nat-ban leírt tananyagtartalmak lehetőségekhez igazított bővítését is.

A tanuló számoló- és számítógépet, a tanulást és szemléltetést segítő
szoftvereket, digitális információforrásokat használ, a matematika
alkalmazását segítő számítógépes programokat ismer meg.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Gondolkodási módszerek -- halmazszemlélet, matematikai logika,
    rendszerezés

2. Gondolkodási módszerek -- szöveges feladatok

3. Aritmetika, algebra -- számok, számtulajdonságok és számok közötti
    kapcsolatok

4. Aritmetika, algebra -- alapműveletek

5. Aritmetika, algebra -- mérés, mennyiségi viszonyok

6. Függvények és sorozatok

7. Geometria -- tájékozódás térben, síkban

8. Geometria -- alkotások és transzformációk térben, síkban

9. Geometria -- testek és alakzatok

10. Statisztika és valószínűség

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Gondolkodási módszerek -- halmazok, matematikai logika

2. Gondolkodási módszerek -- kombinatorika, gráfok

3. Aritmetika, algebra -- természetes számok, számelmélet, egész
    számok, racionális számok, törtek

4. Aritmetika, algebra -- arányosság, százalékszámítás

5. Aritmetika, algebra -- hatvány, négyzetgyök

6. Aritmetika, algebra -- betűs kifejezések, egyenletek

7. Függvények és sorozatok

8. Geometria -- síkgeometria

9. Geometria -- térgeometria

10. Statisztika és valószínűség

***FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON***

1. Gondolkodási módszerek -- halmazok, matematikai logika

2. Gondolkodási módszerek -- kombinatorika, gráfok

3. Aritmetika, algebra -- alapműveletek, valós számok,
    százalékszámítás, betűs kifejezések

4. Aritmetika, algebra -- hatvány, gyök, logaritmus

5. Aritmetika, algebra -- természetes számok halmaza, számelméleti
    ismeretek

6. Aritmetika, algebra -- egyenletek, egyenlőtlenségek,
    egyenletrendszerek

7. Függvények és sorozatok

8. Geometria -- síkgeometria

9. Geometria -- térgeometria

10. Geometria -- trigonometria

11. Geometria -- koordinátageometria

12. Statisztika és valószínűség

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. tudatos megfigyeléseket tesz a környező világ tárgyaira, ezek
    viszonyára vonatkozóan;

2. tájékozódik a környező világ mennyiségi és formai világában;

3. megérti a tanult ismereteket és használja azokat a feladatok
    megoldása során;

4. a környezetében lévő dolgokat szétválogatja, összehasonlítja és
    rendszerezi egy-két szempont alapján;

5. jártas a mérőeszközök használatában, a mérési módszerekben;

6. helyes képzete van a természetes számokról, érti a számnevek és
    számjelek épülésének rendjét;

7. helyesen értelmezi az alapműveleteket tevékenységekkel, szövegekkel,
    és jártas azok elvégzésében fejben és írásban;

8. megfigyeli jelenségek matematikai tartalmát, és le tudja ezeket írni
    számokkal, műveletekkel vagy geometriai alakzatokkal;

9. életkorának megfelelően eligazodik környezetének térbeli és időbeli
    viszonyaiban;

10. érti a korának megfelelő, matematikai tartalmú hallott és olvasott
    szövegeket;

11. megszerzett ismereteit digitális eszközökön is alkalmazza.

RENDSZEREZŐ GONDOLKODÁS

VÁLOGATÁS, HALMAZOK ALKOTÁSA, VIZSGÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönböztet, azonosít egyedi konkrét látott, hallott, mozgással,
    tapintással érzékelhető tárgyakat, dolgokat, helyzeteket, jeleket;

2. válogatásokat végez saját szempont szerint személyek, tárgyak,
    dolgok, számok között;

3. felismeri a mások válogatásában együvé kerülő dolgok közös és a
    különválogatottak eltérő tulajdonságát;

4. folytatja a megkezdett válogatást felismert szempont szerint;

5. személyek, tárgyak, dolgok, szavak, számok közül kiválogatja az
    adott tulajdonsággal rendelkező összes elemet;

6. azonosítja a közös tulajdonsággal rendelkező dolgok halmazába nem
    való elemeket;

7. megnevezi egy adott tulajdonság szerint ki nem válogatott elemek
    közös tulajdonságát a tulajdonság tagadásával;

8. barkochbázik valóságos és elképzelt dolgokkal is, kerüli a
    felesleges kérdéseket;

9. halmazábrán is elhelyez elemeket adott címkék szerint;

10. adott, címkékkel ellátott halmazábrán elhelyezett elemekről eldönti,
    hogy a megfelelő helyre kerültek-e; a hibás elhelyezést javítja;

11. talál megfelelő címkéket halmazokba rendezett elemekhez;

12. megfogalmaz adott halmazra vonatkozó állításokat; értelemszerűen
    használja a „mindegyik", „nem mindegyik", „van köztük...", „"egyik
    sem..." és a velük rokon jelentésű szavakat;

13. két szempontot is figyelembe vesz egyidejűleg;

14. két meghatározott tulajdonság egyszerre történő figyelembevételével
    szétválogat adott elemeket: tárgyakat, személyeket, szavakat,
    számokat, alakzatokat;

15. felsorol elemeket konkrét halmazok közös részéből;

16. megfogalmazza a halmazábra egyes részeibe kerülő elemek közös,
    meghatározó tulajdonságát; helyesen használja a logikai „nem" és a
    logikai „és" szavakat, valamint velük azonos értelmű kifejezéseket;

17. megítéli, hogy adott halmazra vonatkozó állítás igaz-e, vagy hamis;

18. keresi az okát annak, ha a halmazábra valamelyik részébe nem
    kerülhet egyetlen elem sem;

19. hiányos állításokat igazzá tevő elemeket válogat megadott
    alaphalmazból.

HALMAZOK MINT A TERMÉSZETES SZÁM FOGALMÁNAK EGYIK TAPASZTALATI ALAPJA

A nevelési-oktatási szakasz végére a tanuló:

1. összehasonlít véges halmazokat az elemek száma szerint;

2. ismeri két halmaz elemeinek kölcsönösen egyértelmű megfeleltetését
    (párosítását) az elemszámok szerinti összehasonlításra;

3. helyesen alkalmazza a feladatokban a több, kevesebb, ugyanannyi
    fogalmakat 10 000-es számkörben;

4. helyesen érti és alkalmazza a feladatokban a „valamennyivel" több,
    kevesebb fogalmakat.

RENDEZÉS, RENDSZEREZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. adott elemeket elrendez választott és megadott szempont szerint is;

2. sorba rendezett elemek közé elhelyez további elemeket a felismert
    szempont szerint;

3. két, három szempont szerint elrendez adott elemeket többféleképpen
    is; segédeszközként használja a táblázatos elrendezést és a
    fadiagramot;

4. megkeresi egyszerű esetekben a két, három feltételnek megfelelő
    összes elemet, alkotást;

5. megfogalmazza a rendezés felismert szempontjait;

6. megkeresi két, három szempont szerint teljes rendszert alkotó,
    legfeljebb 48 elemű készlet hiányzó elemeit, felismeri az elemek
    által meghatározott rendszert.

Problémamegoldó gondolkodás

PROBLÉMAMEGOLDÁS

A nevelési-oktatási szakasz végére a tanuló:

1. a tevékenysége során felmerülő problémahelyzetben megoldást keres;

2. megfogalmazott problémát tevékenységgel, megjelenítéssel,
    átfogalmazással értelmez;

3. az értelmezett problémát megoldja;

4. a problémamegoldás során a sorrendben végzett tevékenységeket
    szükség szerint visszafelé is elvégzi;

5. megoldását értelmezi, ellenőrzi;

6. kérdést tesz fel a megfogalmazott probléma kapcsán.

SZÖVEGES FELADATOK MEGOLDÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi, elképzeli, megjeleníti a szöveges feladatban
    megfogalmazott hétköznapi szituációt;

2. szöveges feladatokban megfogalmazott hétköznapi problémát megold
    matematikai ismeretei segítségével;

3. tevékenység, ábrarajzolás segítségével megold egyszerű,
    következtetéses, szöveges feladatokat;

4. megkülönbözteti az ismert és a keresendő (ismeretlen) adatokat;

5. megkülönbözteti a lényeges és a lényegtelen adatokat;

6. az értelmezett szöveges feladathoz hozzákapcsol jól megismert
    matematikai modellt;

7. a megválasztott modellen belül meghatározza a keresett adatokat;

8. a modellben kapott megoldást értelmezi az eredeti problémára; arra
    vonatkoztatva ellenőrzi a megoldást;

9. választ fogalmaz meg a felvetett kérdésre.

ISMERETEK FELHASZNÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan emlékezetébe vési az észlelt tárgyakat, személyeket,
    dolgokat, és ezek jellemző tulajdonságait, elrendezését, helyzetét;

2. tudatosan emlékezetébe vés szavakat, számokat, utasítást, adott
    helyzetre vonatkozó megfogalmazást;

3. kérésre, illetve problémahelyzetben felidézi a kívánt, szükséges
    emlékképet;

4. alkalmazza a felismert törvényszerűségeket analógiás esetekben;

5. összekapcsolja az azonos matematikai tartalmú tevékenységek során
    szerzett tapasztalatait;

6. példákat gyűjt konkrét tapasztalatai alapján matematikai állítások
    alátámasztására;

7. egy állításról ismeretei alapján eldönti, hogy igaz vagy hamis;

8. ismeretei alapján megfogalmaz önállóan is egyszerű állításokat;

9. egy- és többszemélyes logikai játékban döntéseit mérlegelve előre
    gondolkodik.

TÁJÉKOZÓDÁS MENNYISÉGI VISZONYOK KÖZÖTT

TERMÉSZETES SZÁMOK ÉS KAPCSOLATAIK

A nevelési-oktatási szakasz végére a tanuló:

1. érti és helyesen használja a több, kevesebb, ugyanannyi relációkat
    halmazok elemszámával kapcsolatban, valamint a kisebb, nagyobb,
    ugyanakkora relációkat a megismert mennyiségekkel (hosszúság, tömeg,
    űrtartalom, idő, terület, pénz) kapcsolatban 10 000-es számkörben;

2. használja a kisebb, nagyobb, egyenlő kifejezéseket a természetes
    számok körében;

3. kis darabszámokat ránézésre felismer többféle rendezett alakban;

4. megszámlál és leszámlál; adott (alkalmilag választott vagy
    szabványos) egységgel meg- és kimér a 10 000-es számkörben;
    oda-vissza számlál kerek tízesekkel, százasokkal, ezresekkel;

5. ismeri a következő becslési módszereket: közelítő számlálás,
    közelítő mérés, mérés az egység többszörösével; becslését finomítja
    újrabecsléssel;

6. nagyság szerint sorba rendez számokat, mennyiségeket;

7. megadja és azonosítja számok sokféle műveletes alakját;

8. megtalálja a számok helyét, közelítő helyét egyszerű számegyenesen,
    számtáblázatokban, a számegyenesnek ugyanahhoz a pontjához rendeli a
    számokat különféle alakjukban a 10 000-es számkörben;

9. megnevezi a 10 000-es számkör számainak egyes, tízes, százas, ezres
    szomszédjait, tízesekre, százasokra, ezresekre kerekített értékét;

10. számokat jellemez tartalmi és formai tulajdonságokkal;

11. számot jellemez más számokhoz való viszonyával;

12. helyesen írja az arab számjeleket;

13. ismeri a római számjelek közül az I, V, X jeleket, hétköznapi
    helyzetekben felismeri az ezekkel képzett számokat.

SZÁMOK HELYI ÉRTÉKES ALAKJA

A nevelési-oktatási szakasz végére a tanuló:

1. összekapcsolja a tízes számrendszerben a számok épülését a különféle
    számrendszerekben végzett tevékenységeivel;

2. érti a számok ezresekből, százasokból, tízesekből és egyesekből való
    épülését, ezresek, százasok, tízesek és egyesek összegére való
    bontását;

3. érti a számok számjegyeinek helyi, alaki, valódi értékét;

4. helyesen írja és olvassa a számokat a tízes számrendszerben 10
    000-ig.

MÉRŐESZKÖZ HASZNÁLATA, MÉRÉSI MÓDSZEREK

A nevelési-oktatási szakasz végére a tanuló:

1. megbecsül, mér alkalmi és szabványos mértékegységekkel hosszúságot,
    tömeget, űrtartalmat és időt;

2. helyesen alkalmazza a mérési módszereket, használ skálázott
    mérőeszközöket, helyes képzete van a mértékegységek nagyságáról;

3. helyesen használja a hosszúságmérés, az űrtartalommérés és a
    tömegmérés szabványegységei közül a következőket: mm, cm, dm, m, km;
    ml, cl, dl, l; g, dkg, kg;

4. ismeri az időmérés szabványegységeit: az órát, a percet, a
    másodpercet, a napot, a hetet, a hónapot, az évet;

5. ismer hazai és külföldi pénzcímleteket 10 000-es számkörben;

6. alkalmazza a felváltást és beváltást különböző pénzcímletek között;

7. összeveti azonos egységgel mért mennyiség és mérőszáma nagyságát,
    összeveti ugyanannak a mennyiségnek a különböző egységekkel való
    mérésekor kapott mérőszámait;

8. megméri különböző sokszögek kerületét különböző egységekkel;

9. területet mér különböző egységekkel lefedéssel vagy darabolással;

10. ismer a terület és kerület mérésére irányuló tevékenységeket.

ALAPMŰVELETEK

A nevelési-oktatási szakasz végére a tanuló:

1. helyesen értelmezi a 10 000-es számkörben az összeadást, a kivonást,
    a szorzást, a bennfoglaló és az egyenlő részekre osztást;

2. helyesen használja a műveletek jeleit;

3. hozzákapcsolja a megfelelő műveletet adott helyzethez, történéshez,
    egyszerű szöveges feladathoz;

4. értelmezi a műveleteket megjelenítéssel, modellezéssel, szöveges
    feladattal;

5. megérti a következő kifejezéseket: tagok, összeg, kisebbítendő,
    kivonandó, különbség, tényezők, szorzandó, szorzó, szorzat,
    osztandó, osztó, hányados, maradék;

6. számolásaiban felhasználja a műveletek közti kapcsolatokat,
    számolásai során alkalmazza konkrét esetekben a legfontosabb
    műveleti tulajdonságokat;

7. megold hiányos műveletet, műveletsort az eredmény ismeretében, a
    műveletek megfordításával is;

8. alkalmazza a műveletekben szereplő számok (kisebbítendő, kivonandó
    és különbség; tagok és összeg; tényezők és szorzat; osztandó, osztó
    és hányados) változtatásának következményeit;

9. szöveghez, valós helyzethez kapcsolva zárójelet tartalmazó
    műveletsort értelmez, elvégez.

SZÁMOLÁS

A nevelési-oktatási szakasz végére a tanuló:

1. alkalmazza a számolást könnyítő eljárásokat;

2. fejben pontosan összead és kivon a 100-as számkörben;

3. érti a szorzó- és bennfoglaló táblák kapcsolatát;

4. emlékezetből tudja a kisegyszeregy és a megfelelő bennfoglalások,
    egyenlő részekre osztások eseteit a számok tízszereséig;

5. fejben pontosan számol a 100-as számkörben egyjegyűvel való szorzás
    és maradék nélküli osztás során;

6. fejben pontosan számol a 10 000-es számkörben a 100-as számkörben
    végzett műveletekkel analóg esetekben;

7. érti a 10-zel, 100-zal, 1000-rel való szorzás, osztás kapcsolatát a
    helyiérték-táblázatban való jobbra, illetve balra tolódással, fejben
    pontosan számol a 10 000-es számkörben a számok 10-zel, 100-zal,
    1000-rel történő szorzásakor és maradék nélküli osztásakor;

8. elvégzi a feladathoz szükséges észszerű becslést, mérlegeli a
    becslés során kapott eredményt;

9. teljes négyjegyűek összegét, különbségét százasokra kerekített
    értékekkel megbecsüli, teljes kétjegyűek két- és egyjegyűvel való
    szorzatát megbecsüli;

10. helyesen végzi el az írásbeli összeadást, kivonást;

11. helyesen végzi el az írásbeli szorzást egy- és kétjegyű szorzóval,
    az írásbeli osztást egyjegyű osztóval.

TÖRTRÉSZEK, NEGATÍV SZÁMOK

A nevelési-oktatási szakasz végére a tanuló:

1. tevékenységekkel megjelenít egységtörteket és azok többszöröseit
    különféle mennyiségek és többféle egységválasztás esetén;

2. a kirakást, mérést és a rajzot mint modellt használja a törtrészek
    összehasonlítására;

3. a negatív egész számokat irányított mennyiségként (hőmérséklet,
    tengerszint alatti magasság, idő) és hiányként (adósság) értelmezi;

4. nagyság szerint összehasonlítja a természetes számokat és a negatív
    egész számokat a használt modellen belül.

TÁJÉKOZÓDÁS ÉS ALKOTÁS TÉRBEN ÉS SÍKON

ALKOTÁS TÉRBEN ÉS SÍKON

A nevelési-oktatási szakasz végére a tanuló:

1. szabadon épít, kirak formát, mintát adott testekből, síklapokból;

2. minta alapján létrehoz térbeli, síkbeli alkotásokat;

3. sormintát, síkmintát felismer, folytat;

4. alkotásában követi az adott feltételeket;

5. testeket épít élekből, lapokból; elkészíti a testek élvázát,
    hálóját; testeket épít képek, alaprajzok alapján; elkészíti egyszerű
    testek alaprajzát;

6. síkidomokat hoz létre különféle eszközök segítségével;

7. alaklemezt, vonalzót, körzőt használ alkotáskor;

8. megtalálja az összes, több feltételnek megfelelő építményt, síkbeli
    kirakást;

9. megfogalmazza az alkotásai közti különbözőséget.

ALAKZATOK GEOMETRIAI TULAJDONSÁGAI

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti és szétválogatja szabadon választott vagy
    meghatározott geometriai tulajdonságok szerint a gyűjtött,
    megalkotott testeket, síkidomokat;

2. megfigyeli az alakzatok közös tulajdonságát, megfelelő címkéket
    talál megadott és halmazokba rendezett alakzatokhoz;

3. megtalálja a közös tulajdonsággal nem rendelkező alakzatokat;

4. megnevezi a tevékenységei során előállított, válogatásai során
    előkerülő alakzatokon megfigyelt tulajdonságokat;

5. különbséget tesz testek és síkidomok között;

6. megnevezi a sík és görbült felületeket, az egyenes és görbe
    vonalakat, szakaszokat tapasztalati ismeretei alapján;

7. kiválasztja megadott síkidomok közül a sokszögeket;

8. megnevezi a háromszögeket, négyszögeket, köröket;

9. megkülönböztet tükrösen szimmetrikus és tükrösen nem szimmetrikus
    síkbeli alakzatokat;

10. megszámlálja az egyszerű szögletes test lapjait;

11. megnevezi a téglatest lapjainak alakját, felismeri a téglatesten az
    egybevágó lapokat, megkülönbözteti a téglatesten az éleket,
    csúcsokat;

12. tudja a téglalap oldalainak és csúcsainak számát, összehajtással
    megmutatja a téglalap szögeinek egyenlőségét;

13. megmutatja a téglalap azonos hosszúságú oldalait és
    elhelyezkedésüket, megmutatja és megszámlálja a téglalap átlóit és
    szimmetriatengelyeit;

14. megfigyeli a kocka mint speciális téglatest és a négyzet mint
    speciális téglalap tulajdonságait;

15. megnevezi megfigyelt tulajdonságai alapján a téglatestet, kockát,
    téglalapot, négyzetet;

16. megfigyelt tulajdonságaival jellemzi a létrehozott síkbeli és
    térbeli alkotást, mintázatot.

TRANSZFORMÁCIÓK

A nevelési-oktatási szakasz végére a tanuló:

1. tapasztalattal rendelkezik mozgással, kirakással a tükörkép
    előállításáról;

2. szimmetrikus alakzatokat hoz létre térben, síkban különböző
    eszközökkel; felismeri a szimmetriát valóságos dolgokon, síkbeli
    alakzatokon;

3. megépíti, kirakja, megrajzolja hálón, jelölés nélküli lapon
    sablonnal, másolópapír segítségével alakzat tükörképét, eltolt
    képét;

4. ellenőrzi a tükrözés, eltolás helyességét tükör vagy másolópapír
    segítségével;

5. követi a sormintában vagy a síkmintában lévő szimmetriát;

6. térben, síkban az eredetihez hasonló testeket, síkidomokat alkot
    nagyított vagy kicsinyített elemekből; az eredetihez hasonló
    síkidomokat rajzol hálón.

TÉRBELI ÉS SÍKBELI TÁJÉKOZÓDÁS

A nevelési-oktatási szakasz végére a tanuló:

1. helyesen használja az irányokat és távolságokat jelölő kifejezéseket
    térben és síkon;

2. tájékozódik lakóhelyén, bejárt terepen: bejárt útvonalon visszatalál
    adott helyre, adott utca és házszám alapján megtalál házat;

3. térképen, négyzethálón megtalál pontot két adat segítségével.

FÜGGVÉNYSZERŰ GONDOLKODÁS

ÖSSZEFÜGGÉSEK ÉS KAPCSOLATOK FELISMERÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. részt vesz memóriajátékokban különféle tulajdonságok szerinti párok
    keresésében;

2. megfogalmazza a személyek, tárgyak, dolgok, időpontok, számok,
    testek, síklapok közötti egyszerű viszonyokat, kapcsolatokat;

3. érti a problémákban szereplő adatok viszonyát;

4. megfogalmazza a felismert összefüggéseket;

5. összefüggéseket keres sorozatok elemei között.

SZABÁLYOK ÉRTELMEZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megadott szabály szerint sorozatot alkot; megértett probléma
    értelmezéséhez, megoldásához sorozatot, táblázatot állít elő
    modellként;

2. tárgyakkal, logikai készletek elemeivel kirakott periodikus
    sorozatokat folytat;

3. elsorolja az évszakokat, hónapokat, napokat, napszakokat egymás
    után, tetszőleges kezdőponttól is;

4. ismert műveletekkel alkotott sorozat, táblázat szabályát felismeri;
    ismert szabály szerint megkezdett sorozatot, táblázatot helyesen,
    önállóan folytat;

5. tárgyakkal, számokkal kapcsolatos gépjátékhoz szabályt alkot;
    felismeri az egyszerű gép megfordításával nyert gép szabályát;

6. felismer kapcsolatot elempárok, elemhármasok tagjai között;

7. szabályjátékok során létrehoz a felismert kapcsolat alapján további
    elempárokat, elemhármasokat;

8. a sorozatban, táblázatban, gépjátékokban felismert összefüggést
    megfogalmazza saját szavaival, nyíljelöléssel vagy nyitott
    mondattal.

ADATOK MEGFIGYELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. adatokat gyűjt a környezetében;

2. adatokat rögzít későbbi elemzés céljából;

3. gyűjtött adatokat táblázatba rendez, diagramon ábrázol;

4. adatokat gyűjt ki táblázatból, adatokat olvas le diagramról;

5. jellemzi az összességeket.

VALÓSZÍNŰSÉGI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. részt vesz olyan játékokban, kísérletekben, melyekben a véletlen
    szerepet játszik;

2. különbséget tesz tapasztalatai alapján a „biztos", „lehetetlen",
    „lehetséges, de nem biztos" események között;

3. megítéli „biztos", „lehetetlen", „lehetséges, de nem biztos"
    eseményekkel kapcsolatos állítások igazságát;

4. tapasztalatai alapján tippet fogalmaz meg arról, hogy két esemény
    közül melyik esemény valószínűbb olyan véletlentől függő szituációk
    során, melyekben a két esemény valószínűsége között jól belátható a
    különbség;

5. tetszőleges vagy megadott módszerrel összeszámlálja az egyes
    kimenetelek előfordulásait olyan egyszerű játékokban, kísérletekben,
    amelyekben a véletlen szerepet játszik;

6. a valószínűségi játékokban, kísérletekben megfogalmazott előzetes
    sejtését, tippjét összeveti a megfigyelt előfordulásokkal.

MATEMATIKAI KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan értelmezi a hallott, olvasott matematikai tartalmú
    szöveget;

2. helyesen használja a mennyiségi viszonyokat kifejező szavakat,
    nyelvtani szerkezeteket;

3. szöveges feladatokban a különböző kifejezésekkel megfogalmazott
    műveleteket megérti;

4. megfelelő szókincset és jeleket használ mennyiségi viszonyok
    kifejezésére szóban és írásban;

5. megfelelően használja szóban és írásban a nyelvtani szerkezeteket
    matematikai tartalmuk szerint;

6. szöveget, ábrát alkot matematikai jelekhez, műveletekhez;

7. játékos feladatokban személyeket, tárgyakat, számokat, formákat
    néhány meghatározó tulajdonsággal jellemez;

8. kérdést fogalmaz meg, ha munkája során nehézségbe ütközik;

9. nyelvi szempontból megfelelő választ ad a feladatokban megjelenő
    kérdésekre.

DIGITÁLISESZKÖZ-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. használ matematikai képességfejlesztő számítógépes játékokat,
    programokat;

2. alkalmazza a tanult infokommunikációs ismereteket matematikai
    problémák megoldása során.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. rendelkezik a matematikai problémamegoldáshoz szükséges
    eszközrendszerrel, melyet az adott problémának megfelelően tud
    alkalmazni;

2. felismeri a hétköznapi helyzetekben a matematikai vonatkozásokat, és
    ezek leírására megfelelő modellt használ;

3. megfogalmaz sejtéseket, és logikus érveléssel ellenőrzi azokat;

4. helyesen használja a matematikai jelöléseket írásban;

5. olvassa és érti az életkorának megfelelő matematikai tartalmú
    szövegeket;

6. tanulási módszerei változatosak: szóbeli közlés, írott szöveg és
    digitális csatornák útján egyaránt képes az ismeretek
    elsajátítására;

7. matematikai ismereteit össze tudja kapcsolni más tanulásterületeken
    szerzett tapasztalatokkal;

8. matematikai ismereteit alkalmazza a pénzügyi tudatosság területét
    érintő feladatok megoldásában.

RENDSZEREZŐ GONDOLKODÁS

ESZKÖZRENDSZER ALKALMAZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. különböző szövegekhez megfelelő modelleket készít;

2. számokat, számhalmazokat, halmazműveleti eredményeket számegyenesen
    ábrázol;

3. konkrét szituációkat szemléltet gráfok segítségével;

4. egyismeretlenes elsőfokú egyenletet lebontogatással és mérlegelvvel
    megold.

ÁLLÍTÁSOK KEZELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. állítások logikai értékét (igaz vagy hamis) megállapítja;

2. igaz és hamis állításokat fogalmaz meg;

3. tanult minták alapján néhány lépésből álló bizonyítási gondolatsort
    megért és önállóan összeállít;

4. a logikus érvelésben a matematikai szaknyelvet következetesen
    alkalmazza társai meggyőzésére.

RENDEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. elemeket halmazba rendez több szempont alapján;

2. részhalmazokat konkrét esetekben felismer és ábrázol;

3. véges halmaz kiegészítő halmazát (komplementerét), véges halmazok
    közös részét (metszetét), egyesítését (unióját) képezi és ábrázolja
    konkrét esetekben;

4. a természetes számokat osztóik száma alapján és adott számmal való
    osztási maradékuk szerint csoportosítja;

5. síkbeli tartományok közül kiválasztja a szögtartományokat, nagyság
    szerint összehasonlítja, méri, csoportosítja azokat;

6. csoportosítja a háromszögeket szögeik és oldalaik szerint;

7. ismeri a speciális négyszögek legfontosabb tulajdonságait, ezek
    alapján elkészíti a halmazábrájukat;

8. valószínűségi játékokat, kísérleteket végez, ennek során az adatokat
    tervszerűen gyűjti, rendezi és ábrázolja digitálisan is.

ISMERETEK ALKALMAZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. összeszámlálási feladatok megoldása során alkalmazza az összes eset
    áttekintéséhez szükséges módszereket;

2. a háromszögek és a speciális négyszögek tulajdonságait alkalmazza
    feladatok megoldásában;

3. a kocka, a téglatest, a hasáb, a gúla, a gömb tulajdonságait
    alkalmazza feladatok megoldásában;

4. konkrét adatsor esetén átlagot számol, megállapítja a leggyakoribb
    adatot (módusz), a középső adatot (medián), és ezeket
    összehasonlítja.

PROBLÉMAMEGOLDÓ GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. matematikából, más tantárgyakból és a mindennapi életből vett
    egyszerű szöveges feladatokat következtetéssel vagy egyenlettel
    megold;

2. gazdasági, pénzügyi témájú egyszerű szöveges feladatokat
    következtetéssel vagy egyenlettel megold;

3. gyakorlati problémák megoldása során előforduló mennyiségeknél
    becslést végez;

4. megoldását ellenőrzi;

5. ismeri a százalék fogalmát, gazdasági, pénzügyi és mindennapi
    élethez kötődő százalékszámítási feladatokat megold.

TÁJÉKOZÓDÁS MENNYISÉGI VISZONYOK KÖZÖTT

RACIONÁLIS SZÁMOK FELÉPÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. érti és alkalmazza a számok helyi értékes írásmódját nagy számok
    esetén;

2. érti és alkalmazza a számok helyi értékes írásmódját tizedes törtek
    esetén;

3. ismeri a római számjelek közül az L, C, D, M jeleket, felismeri az
    ezekkel képzett számokat a hétköznapi helyzetekben;

4. ismeri az egész számokat;

5. meghatározza konkrét számok ellentettjét, abszolút értékét;

6. ábrázol törtrészeket, meghatároz törtrészeknek megfelelő
    törtszámokat;

7. megfelelteti egymásnak a racionális számok közönséges tört és
    tizedes tört alakját;

8. ismeri a racionális számokat, tud példát végtelen nem szakaszos
    tizedes törtre;

9. meghatározza konkrét számok reciprokát.

MŰVELETEK ELVÉGZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és helyesen alkalmazza a műveleti sorrendre és a
    zárójelezésre vonatkozó szabályokat fejben, írásban és géppel
    számolás esetén is a racionális számok körében;

2. ismeri és alkalmazza a 2-vel, 3-mal, 4-gyel, 5-tel, 6-tal, 9-cel,
    10-zel, 100-zal való oszthatóság szabályait;

3. ismeri a prímszám és az összetett szám fogalmakat; el tudja
    készíteni összetett számok prímtényezős felbontását 1000-es
    számkörben;

4. meghatározza természetes számok legnagyobb közös osztóját és
    legkisebb közös többszörösét;

5. pozitív egész számok pozitív egész kitevőjű hatványát kiszámolja;

6. négyzetszámok négyzetgyökét meghatározza;

7. egyszerű betűs kifejezésekkel összeadást, kivonást végez, és
    helyettesítési értéket számol;

8. egy- vagy kéttagú betűs kifejezést számmal szoroz, két tagból közös
    számtényezőt kiemel;

9. írásban összead, kivon és szoroz;

10. gyakorlati feladatok megoldása során legfeljebb kétjegyű egész
    számmal írásban oszt. A hányadost megbecsüli;

11. gyakorlati feladatok megoldása során tizedes törtet legfeljebb
    kétjegyű egész számmal írásban oszt. A hányadost megbecsüli;

12. a műveleti szabályok ismeretében ellenőrzi számolását. A kapott
    eredményt észszerűen kerekíti;

13. a gyakorlati problémákban előforduló mennyiségeket becsülni tudja,
    feladatmegoldásához ennek megfelelő tervet készít;

14. elvégzi az alapműveleteket a racionális számok körében, eredményét
    összeveti előzetes becslésével.

MÉRÉS, MÉRTÉKEGYSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az idő, a tömeg, a hosszúság, a terület, a térfogat és az
    űrtartalom szabványmértékegységeit, használja azokat mérések és
    számítások esetén;

2. egyenes hasáb, téglatest, kocka alakú tárgyak felszínét és
    térfogatát méréssel megadja, egyenes hasáb felszínét és térfogatát
    képlet segítségével kiszámolja; a képleteket megalapozó
    összefüggéseket érti;

3. idő, tömeg, hosszúság, terület, térfogat és űrtartalom
    mértékegységeket átvált helyi értékes gondolkodás alapján,
    gyakorlati célszerűség szerint;

4. meghatározza háromszögek és speciális négyszögek kerületét,
    területét.

TÁJÉKOZÓDÁS ÉS ALKOTÁS TÉRBEN ÉS SÍKON

TÁJÉKOZÓDÁS ÉS ALKOTÁS TÉRBEN

A nevelési-oktatási szakasz végére a tanuló:

1. a kocka, a téglatest, a hasáb és a gúla hálóját elkészíti;

2. testeket épít képek, nézetek, alaprajzok, hálók alapján;

3. ismeri a gömb tulajdonságait;

4. ismeri a kocka, a téglatest, a hasáb és a gúla következő
    tulajdonságait: határoló lapok típusa, száma, egymáshoz viszonyított
    helyzete; csúcsok, élek száma; lapátló, testátló.

TÁJÉKOZÓDÁS SÍKON

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a tengelyesen szimmetrikus háromszöget;

2. ismeri a speciális négyszögeket: trapéz, paralelogramma, téglalap,
    deltoid, rombusz, húrtrapéz, négyzet;

3. tapasztalatot szerez a síkbeli mozgásokról gyakorlati helyzetekben;

4. felismeri a síkban az egybevágó alakzatokat;

5. felismeri a kicsinyítést és a nagyítást hétköznapi helyzetekben;

6. ismeri a kör részeit; különbséget tesz egyenes, félegyenes és
    szakasz között;

7. ismeri a háromszögek tulajdonságait: belső és külső szögek összege,
    háromszög-egyenlőtlenség;

8. ismeri a Pitagorasz-tételt és alkalmazza számítási feladatokban;

9. ismeri a négyszögek tulajdonságait: belső és külső szögek összege,
    konvex és konkáv közti különbség, átló fogalma.

ALKOTÁS SÍKON

A nevelési-oktatási szakasz végére a tanuló:

1. a szerkesztéshez tervet, előzetes ábrát készít;

2. ismeri az alapszerkesztéseket: szakaszfelező merőlegest,
    szögfelezőt, merőleges és párhuzamos egyeneseket szerkeszt, szöget
    másol;

3. megszerkeszti alakzatok tengelyes és középpontos tükörképét;

4. geometriai ismereteinek felhasználásával pontosan szerkeszt több
    adott feltételnek megfelelő ábrát.

FÜGGVÉNYSZERŰ GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. konkrét halmazok elemei között megfeleltetést hoz létre;

2. felismeri az egyenes és a fordított arányosságot konkrét
    helyzetekben;

3. tájékozódik a koordináta-rendszerben: koordinátáival adott pontot
    ábrázol, megadott pont koordinátáit leolvassa;

4. értéktáblázatok adatait grafikusan ábrázolja;

5. egyszerű grafikonokat jellemez;

6. felismeri és megalkotja az egyenes arányosság grafikonját;

7. sorozatokat adott szabály alapján folytat;

8. néhány tagjával adott sorozat esetén felismer és megfogalmaz képzési
    szabályt.

VALÓSZÍNŰSÉGI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. valószínűségi játékokban érti a lehetséges kimeneteleket, játékában
    stratégiát követ;

2. ismeri a gyakoriság és a relatív gyakoriság fogalmát. Ismereteit
    felhasználja a „lehetetlen", a „biztos" és a „kisebb, nagyobb
    eséllyel lehetséges" kijelentések megfogalmazásánál.

MATEMATIKAI KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. helyesen használja a tanult matematikai fogalmakat megnevező
    szakkifejezéseket;

2. adatokat táblázatba rendez, diagramon ábrázol hagyományos és
    digitális eszközökkel is;

3. különböző típusú diagramokat megfeleltet egymásnak;

4. megadott szempont szerint adatokat gyűjt ki táblázatból, olvas le
    hagyományos vagy digitális forrásból származó diagramról, majd
    rendszerezés után következtetéseket fogalmaz meg;

5. konkrét esetekben halmazokat felismer és ábrázol;

6. értelmezi a táblázatok adatait, az adatoknak megfelelő ábrázolási
    módot kiválasztja, és az ábrát elkészíti.

DIGITÁLISESZKÖZ-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. ismer táblázatkezelő programot, tud adatokat összehasonlítani,
    elemezni;

2. a fejszámoláson és az írásban végzendő műveleteken túlmutató
    számolási feladatokhoz és azok ellenőrzéséhez számológépet használ;

3. ismer és használ dinamikus geometriai szoftvereket, tisztában van
    alkalmazási lehetőségeikkel;

4. ismer és használ digitális matematikai játékokat, programokat;

5. alkalmazza a tanult infokommunikációs ismereteket matematikai
    problémák megoldása során.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeretei segítségével, a megfelelő modell alkalmazásával megold
    hétköznapi és matematikai problémákat, a megoldást ellenőrzi és
    értelmezi;

2. megérti a környezetében jelen lévő logikai, mennyiségi,
    függvényszerű, térbeli és statisztikai kapcsolatokat;

3. sejtéseket fogalmaz meg és logikus lépésekkel igazolja azokat;

4. adatokat gyűjt, rendez, ábrázol, értelmez;

5. a matematikai szakkifejezéseket és jelöléseket helyesen használja
    írásban és szóban egyaránt;

6. megérti a hallott és olvasott matematikai tartalmú szövegeket;

7. felismeri a matematika különböző területei közötti kapcsolatokat;

8. a matematika tanulása során digitális eszközöket és különböző
    információforrásokat használ;

9. a matematikát más tantárgyakhoz kapcsolódó témákban is használja;

10. matematikai ismereteit alkalmazza a pénzügyi tudatosság területét
    érintő feladatok megoldásában.

RENDSZEREZŐ GONDOLKODÁS

RENDSZEREZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. adott halmazt diszjunkt részhalmazaira bont, osztályoz;

2. matematikai vagy hétköznapi nyelven megfogalmazott szövegből a
    matematikai tartalmú információkat kigyűjti, rendszerezi;

3. felismeri a matematika különböző területei közötti kapcsolatot.

HALMAZOK, MATEMATIKAI LOGIKA

A nevelési-oktatási szakasz végére a tanuló:

1. látja a halmazműveletek és a logikai műveletek közötti
    kapcsolatokat;

2. halmazokat különböző módokon megad;

3. halmazokkal műveleteket végez, azokat ábrázolja és értelmezi;

4. véges halmazok elemszámát meghatározza;

5. alkalmazza a logikai szita elvét;

6. adott állításról eldönti, hogy igaz vagy hamis;

7. alkalmazza a tagadás műveletét egyszerű feladatokban;

8. ismeri és alkalmazza az „és", a (megengedő és kizáró) „vagy" logikai
    jelentését;

9. megfogalmazza adott állítás megfordítását;

10. megállapítja egyszerű „ha \... , akkor \..." és „akkor és csak
    akkor" típusú állítások logikai értékét;

11. helyesen használja a „minden" és „van olyan" kifejezéseket;

12. tud egyszerű állításokat indokolni és tételeket bizonyítani.

KOMBINATORIKA, GRÁFOK

A nevelési-oktatási szakasz végére a tanuló:

1. megold sorba rendezési és kiválasztási feladatokat;

2. konkrét szituációkat szemléltet és egyszerű feladatokat megold
    gráfok segítségével.

PROBLÉMAMEGOLDÓ GONDOLKODÁS

PROBLÉMAMEGOLDÁS

A nevelési-oktatási szakasz végére a tanuló:

1. adott problémához megoldási stratégiát, algoritmust választ, készít;

2. a problémának megfelelő matematikai modellt választ, alkot;

3. a kiválasztott modellben megoldja a problémát;

4. a modellben kapott megoldását az eredeti problémába
    visszahelyettesítve értelmezi, ellenőrzi és az észszerűségi
    szempontokat figyelembe véve adja meg válaszát;

5. geometriai szerkesztési feladatoknál vizsgálja és megállapítja a
    szerkeszthetőség feltételeit.

EGYENLETEK, EGYENLŐTLENSÉGEK, EGYENLETRENDSZEREK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza a következő egyenletmegoldási módszereket:
    mérlegelv, grafikus megoldás, szorzattá alakítás;

2. megold elsőfokú egyismeretlenes egyenleteket és egyenlőtlenségeket,
    elsőfokú kétismeretlenes egyenletrendszereket;

3. megold másodfokú egyismeretlenes egyenleteket és egyenlőtlenségeket;
    ismeri és alkalmazza a diszkriminánst, a megoldóképletet és a
    gyöktényezős alakot;

4. megold egyszerű, a megfelelő definíció alkalmazását igénylő
    exponenciális egyenleteket, egyenlőtlenségeket;

5. egyenletek megoldását behelyettesítéssel, értékkészlet-vizsgálattal
    ellenőrzi.

TÁJÉKOZÓDÁS MENNYISÉGI VISZONYOK KÖZÖTT

MÉRÉS, MÉRTÉKEGYSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a mérés alapelvét, alkalmazza konkrét alap- és származtatott
    mennyiségek esetén;

2. ismeri a hosszúság, terület, térfogat, űrtartalom, idő
    mértékegységeit és az átváltási szabályokat. Származtatott
    mértékegységeket átvált;

3. sík- és térgeometriai feladatoknál a problémának megfelelő
    mértékegységben adja meg válaszát.

A TERMÉSZETES SZÁMOK HALMAZA, SZÁMELMÉLETI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza az oszthatóság alapvető fogalmait;

2. összetett számokat felbont prímszámok szorzatára;

3. meghatározza két természetes szám legnagyobb közös osztóját és
    legkisebb közös többszörösét, és alkalmazza ezeket egyszerű
    gyakorlati feladatokban;

4. ismeri és alkalmazza az oszthatósági szabályokat;

5. érti a helyi értékes írásmódot 10-es és más alapú számrendszerekben.

VALÓS SZÁMOK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a számhalmazok épülésének matematikai vonatkozásait a
    természetes számoktól a valós számokig;

2. a kommutativitás, asszociativitás, disztributivitás műveleti
    azonosságokat helyesen alkalmazza különböző számolási helyzetekben;

3. racionális számokat tizedes tört és közönséges tört alakban is
    felír;

4. ismer példákat irracionális számokra;

5. ismeri a valós számok és a számegyenes kapcsolatát;

6. ismeri és alkalmazza az abszolút érték, az ellentett és a reciprok
    fogalmát;

7. a számolással kapott eredményeket nagyságrendileg megbecsüli, és így
    ellenőrzi az eredményt;

8. valós számok közelítő alakjaival számol, és megfelelően kerekít.

HATVÁNY, GYÖK, LOGARITMUS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza a négyzetgyök fogalmát és azonosságait;

2. ismeri és alkalmazza az n-edik gyök fogalmát;

3. ismeri és alkalmazza a normálalak fogalmát;

4. ismeri és alkalmazza az egész kitevőjű hatvány fogalmát és a
    hatványozás azonosságait;

5. ismeri és alkalmazza a racionális kitevőjű hatvány fogalmát és a
    hatványozás azonosságait;

6. ismeri és alkalmazza a logaritmus fogalmát;

7. műveleteket végez algebrai kifejezésekkel;

8. ismer és alkalmaz egyszerű algebrai azonosságokat;

9. átalakít algebrai kifejezéseket összevonás, szorzattá alakítás,
    nevezetes azonosságok alkalmazásával.

ARÁNYOSSÁG, SZÁZALÉKSZÁMÍTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza az egyenes és a fordított arányosságot;

2. ismeri és alkalmazza a százalékalap, -érték, -láb, -pont fogalmát.

TÁJÉKOZÓDÁS ÉS ALKOTÁS TÉRBEN ÉS SÍKON

GEOMETRIAI ALAPFOGALMAK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és használja a pont, egyenes, sík (térelemek) és szög
    fogalmát;

2. ismeri és feladatmegoldásban alkalmazza a térelemek kölcsönös
    helyzetét, távolságát és hajlásszögét;

3. ismeri és alkalmazza a nevezetes szögpárok tulajdonságait;

4. ismeri az alapszerkesztéseket, és ezeket végre tudja hajtani
    hagyományos vagy digitális eszközzel.

HÁROMSZÖGEK, NÉGYSZÖGEK, SOKSZÖGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza a háromszögek oldalai, szögei, oldalai és
    szögei közötti kapcsolatokat; a speciális háromszögek
    tulajdonságait;

2. ismeri és alkalmazza a háromszög nevezetes vonalaira, pontjaira és
    köreire vonatkozó fogalmakat és tételeket;

3. ismeri és alkalmazza a Pitagorasz-tételt és megfordítását;

4. kiszámítja háromszögek területét;

5. ismeri és alkalmazza speciális négyszögek tulajdonságait,
    területüket kiszámítja;

6. ismeri és alkalmazza a szabályos sokszög fogalmát; kiszámítja a
    konvex sokszög belső és külső szögeinek összegét;

7. átdarabolással kiszámítja sokszögek területét.

A KÖR ÉS RÉSZEI

A nevelési-oktatási szakasz végére a tanuló:

1. ki tudja számolni a kör és részeinek kerületét, területét;

2. ismeri a kör érintőjének fogalmát, kapcsolatát az érintési pontba
    húzott sugárral;

3. ismeri és alkalmazza a Thalész-tételt és megfordítását.

GEOMETRIAI TRANSZFORMÁCIÓK

A nevelési-oktatási szakasz végére a tanuló:

1. ismer példákat geometriai transzformációkra;

2. ismeri és alkalmazza a síkbeli egybevágósági transzformációkat és
    tulajdonságaikat; alakzatok egybevágóságát;

3. ismeri és alkalmazza a középpontos hasonlósági transzformációt, a
    hasonlósági transzformációt és az alakzatok hasonlóságát;

4. ismeri és alkalmazza a hasonló síkidomok kerületének és területének
    arányára vonatkozó tételeket;

5. megszerkeszti egy alakzat tengelyes, illetve középpontos tükörképét,
    pont körüli elforgatottját, párhuzamos eltoltját hagyományosan és
    digitális eszközzel;

6. ismeri a vektorokkal kapcsolatos alapvető fogalmakat;

7. ismer és alkalmaz egyszerű vektorműveleteket;

8. alkalmazza a vektorokat feladatok megoldásában.

TRIGONOMETRIA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri hegyesszögek szögfüggvényeinek definícióját a derékszögű
    háromszögben;

2. ismeri tompaszögek szögfüggvényeinek származtatását a hegyesszögek
    szögfüggvényei alapján;

3. ismeri a hegyes- és tompaszögek szögfüggvényeinek összefüggéseit;

4. alkalmazza a szögfüggvényeket egyszerű geometriai számítási
    feladatokban;

5. a szögfüggvény értékének ismeretében meghatározza a szöget;

6. ismeri és alkalmazza a szinusz- és a koszinusztételt.

TÉRGEOMETRIA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza a hasáb, a henger, a gúla, a kúp, a gömb, a
    csonkagúla, a csonkakúp (speciális testek) tulajdonságait;

2. lerajzolja a kocka, téglatest, egyenes hasáb, egyenes körhenger,
    egyenes gúla, forgáskúp hálóját;

3. kiszámítja a speciális testek felszínét és térfogatát egyszerű
    esetekben;

4. ismeri és alkalmazza a hasonló testek felszínének és térfogatának
    arányára vonatkozó tételeket.

KOORDINÁTAGEOMETRIA

A nevelési-oktatási szakasz végére a tanuló:

1. megad pontot és vektort koordinátáival a derékszögű
    koordináta-rendszerben;

2. koordináta-rendszerben ábrázol adott feltételeknek megfelelő
    ponthalmazokat;

3. koordináták alapján számításokat végez szakaszokkal, vektorokkal;

4. ismeri és alkalmazza az egyenes egyenletét;

5. egyenesek egyenletéből következtet az egyenesek kölcsönös
    helyzetére;

6. kiszámítja egyenesek metszéspontjainak koordinátáit az egyenesek
    egyenletének ismeretében;

7. megadja és alkalmazza a kör egyenletét a kör sugarának és a
    középpont koordinátáinak ismeretében.

FÜGGVÉNYSZERŰ GONDOLKODÁS

FÜGGVÉNYEK

A nevelési-oktatási szakasz végére a tanuló:

1. megad hétköznapi életben előforduló hozzárendeléseket;

2. adott képlet alapján helyettesítési értékeket számol, és azokat
    táblázatba rendezi;

3. táblázattal megadott függvény összetartozó értékeit ábrázolja
    koordináta-rendszerben;

4. képlettel adott függvényt hagyományosan és digitális eszközzel
    ábrázol;

5. adott értékkészletbeli elemhez megtalálja az értelmezési tartomány
    azon elemeit, amelyekhez a függvény az adott értéket rendeli;

6. a grafikonról megállapítja függvények alapvető tulajdonságait.

SOROZATOK

A nevelési-oktatási szakasz végére a tanuló:

1. számtani és mértani sorozatokat adott szabály alapján felír,
    folytat;

2. a számtani, mértani sorozat n-edik tagját felírja az első tag és a
    különbség (differencia)/hányados (kvóciens) ismeretében;

3. a számtani, mértani sorozatok első n tagjának összegét kiszámolja;

4. mértani sorozatokra vonatkozó ismereteit használja gazdasági,
    pénzügyi, természettudományi és társadalomtudományi problémák
    megoldásában.

VALÓSZÍNŰSÉGI GONDOLKODÁS

LEÍRÓ STATISZTIKA

A nevelési-oktatási szakasz végére a tanuló:

1. adott cél érdekében tudatos adatgyűjtést és rendszerezést végez;

2. hagyományos és digitális forrásból származó adatsokaság alapvető
    statisztikai jellemzőit meghatározza, értelmezi és értékeli;

3. adatsokaságból adott szempont szerint oszlop- és kördiagramot készít
    hagyományos és digitális eszközzel;

4. ismeri és alkalmazza a sodrófa (box-plot) diagramot adathalmazok
    jellemzésére, összehasonlítására;

5. felismer grafikus manipulációkat diagramok esetén.

VALÓSZÍNŰSÉGSZÁMÍTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. tapasztalatai alapján véletlen jelenségek jövőbeni kimenetelére
    észszerűen tippel;

2. ismeri és alkalmazza a klasszikus valószínűségi modellt és a
    Laplace-képletet;

3. véletlen kísérletek adatait rendszerezi, relatív gyakoriságokat
    számol, nagy elemszám esetén számítógépet alkalmaz;

4. konkrét valószínűségi kísérletek esetében az esemény, eseménytér,
    elemi esemény, relatív gyakoriság, valószínűség, egymást kizáró
    események, független események fogalmát megkülönbözteti és
    alkalmazza;

5. ismeri és egyszerű esetekben alkalmazza a valószínűség geometriai
    modelljét;

6. meghatározza a valószínűséget visszatevéses, illetve visszatevés
    nélküli mintavétel esetén.

MATEMATIKAI KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. a megfelelő matematikai tankönyveket, feladatgyűjteményeket,
    internetes tartalmakat értőn olvassa, a matematikai tartalmat
    rendszerezetten kigyűjti és megérti;

2. a matematikai fogalmakat és jelöléseket megfelelően használja;

3. önállóan kommunikál matematika tartalmú feladatokkal kapcsolatban;

4. matematika feladatok megoldását szakszerűen prezentálja írásban és
    szóban a szükséges alapfogalmak, azonosságok, definíciók és tételek
    segítségével;

5. szöveg alapján táblázatot, grafikont készít, ábrát, kapcsolatokat
    szemléltető gráfot rajzol, és ezeket kombinálva prezentációt készít
    és mutat be;

6. ismer a tananyaghoz kapcsolódó matematikatörténeti vonatkozásokat.

DIGITÁLISESZKÖZ-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. számológép segítségével alapműveletekkel felírható számolási
    eredményt; négyzetgyököt; átlagot; szögfüggvények értékét, illetve
    abból szöget; logaritmust; faktoriálist; binomiális együtthatót;
    szórást meghatároz;

2. digitális környezetben matematikai alkalmazásokkal dolgozik;

3. megfelelő informatikai alkalmazás segítségével szöveget szerkeszt,
    táblázatkezelő programmal diagramokat készít;

4. ismereteit digitális forrásokból kiegészíti, számítógép segítségével
    elemzi és bemutatja;

5. prezentációhoz informatív diákat készít, ezeket logikusan és
    következetesen egymás után fűzi és bemutatja;

6. kísérletezéshez, sejtés megfogalmazásához, egyenlet grafikus
    megoldásához és ellenőrzéshez dinamikus geometriai, grafikus és
    táblázatkezelő szoftvereket használ;

7. szerkesztési feladatok euklideszi módon történő megoldásához
    dinamikus geometriai szoftvert használ.

*II.3.4. Történelem és állampolgári ismeretek*

A tanulási terület a *történelem*, az *állampolgári ismerete*k, valamint
a *hon- és népismeret* tantárgyakat foglalja magában. Középpontjában az
emberi civilizáció -- és annak részeként a magyarság -- által
létrehozott kulturális, társadalmi, politikai és gazdasági eredmények
bemutatása, emberek és közösségeik viszonyának tanulmányozása áll.

A történelem az emberi közösségeknek a múltról alkotott tudása; egyfelől
az emberekkel megtörtént eseményekről tanúskodó különböző források és
bizonyítékok, másfelől az ezekről alkotott interpretációk és vélekedések
összessége.

A történelmi tudás meghatározó önmagunk és mások megismerése
szempontjából, hiszen az egyén és közösségeinek helyzete mindig annak a
következménye, amit előtte más emberek és közösségek gondoltak, tettek
és tapasztaltak. A mai politikai, társadalmi, gazdasági és kulturális
jelenségek és a hozzájuk kapcsolódó vélekedések a történelmi folyamatok
aktuális termékei, jelenkori állomásai. A történelmi tudás érthetővé
teszi egyrészt azt, hogy a múlt alakította a jelent, és emiatt érdemes
tanulmányozni; másrészt azt, hogy a jövőt a jelen fogja alakítani, ezért
ennek a múlt segítségével történő megértése fontos és szükséges. A
történelem tanulása során lényeges annak felismerése, hogy a történelmi
folyamatoknak okai és következményei vannak -- ezért is értelmezhetőek
utólag --, de nem determináltak: emberi értékválasztások, szándékok,
cselekedetek alakítják őket.

A történelemtanítás fő feladata olyan ismeretek és értékek közvetítése,
valamint kompetenciák elsajátíttatása, amelyek hozzásegítik a tanulót,
hogy tájékozott, aktív és elkötelezett állampolgárrá, kisebb és nagyobb
közösségeinek felelős tagjává váljék.

A történelemtanítás feladata továbbá, hogy a tanuló ismerje fel, hogy a
történelem szereplői különféle mintákat közvetítenek a jelen embere
számára is, a múlt tanulságai kihatnak saját döntéseinkre. Fontos, hogy
tudatosítsa, a jelen, annak minden eseménye a jövő történelme, s így ő
is annak résztvevője.

A történelemtanulás során a tanuló megismerkedik a magyar és a
világtörténelem legfontosabbnak tartott eseményeivel, jelenségeivel,
folyamataival és szereplőivel. Ez jelentős mértékben elősegíti, hogy a
tanuló megismerje és elsajátítsa azt a kulturális kódrendszert, amely
lehetővé teszi számára identitása, valamint a magyar nemzet és a
keresztény normarendszeren alapuló európai civilizáció iránti
elkötelezettsége kialakítását és megerősítését.

A történelem tantárgy tanulása folyamán a tanuló számos olyan
történettel, konfliktussal, dilemmával, emberi magatartással és sorssal
találkozik és foglalkozik, amely nemcsak tájékozottsága, életismerete és
gondolkodási képessége kibontakozásához járul hozzá, hanem erkölcsi és
érzelmi fejlődését is szolgálja. A magyar történelem tanulása és
tanulmányozása segít megértenie Magyarország és a magyar nemzet helyét
és helyzetét a világban, és segít elmélyíteni benne a hazaszeretet
érzését.

Az *állampolgári ismeretek* tantárgynak a megszerzett történelmi tudásra
szervesen épülő témái és tevékenységei -- az alap- és középfokú
tanulmányok zárásaként a 8. és 12. évfolyamon -- a tanuló számára fontos
és hasznos ismereteket ad az állam működéséről és intézményeiről,
valamint a család és az állam gazdasági szerepéről. A tantárgy tudást,
kultúrát és normákat közvetít, és a tanulót hozzásegíti ahhoz, hogy
hazáját szerető, önálló és felelős, demokratikus gondolkodású polgárrá,
a kisebb-nagyobb közösségek értékalkotó tagjává váljon, valamint ahhoz,
hogy ismerje és gyakorolni tudja az aktív és felelős állampolgári léthez
szükséges eljárásokat, élni tudjon a társadalmi intézményrendszer
nyújtotta lehetőségekkel.

Fő cél a normakövető magatartás és a társadalmi felelősségvállalás
megalapozása, a szabadság és felelősség, valamint az alapvető jogok és
kötelességek egyensúlyának megismerése. További cél a jogrendszer
mindennapi élettel való összefüggéseinek értelmezése, a jogérvényesülés
területeinek és az érdekérvényesítés lehetőségeinek feltárása. Cél
továbbá az alkotmányosságon alapuló politikai kultúra ismérveinek és
jelentőségének megértése, mely így a tanuló szocializációjának szerves
részévé válik.

Az is cél, hogy a tanuló ismerje és értelmezze a nemzeti és európai
identitás kettős jelentőségét az egyén és a közösség szempontjából is.

A tantárgy műveltségtartalmában nem az elméleti jelleg dominál. Az
állampolgári ismeretek elsajátítása és aktív kialakítása a tanuló
tapasztalataira, élményvilágára, önálló és csoportos vizsgálódásaira
épít. A kooperatív tanulási technikák elősegítik a másik ember
véleményének megértését, az empatikus viszonyulást, a társadalmi
kérdések közös megbeszélését és az erkölcsi dilemmák megvitatását.

A *hon- és népismeret* tantárgy tanulása során a tanuló megismerkedik
nemzeti kultúránk nagy hagyományú elemeivel, a magyar néphagyománnyal és
a hazánkban élő nemzetiségek kulturális emlékeivel, szokásaival,
jelenével.

Teret biztosít azoknak az élményszerű egyéni és közösségi
tevékenységeknek, amelyek a család, az otthon, a lakóhely, a szülőföld
tiszteletét alapozzák meg. Segíti az egyéni, családi, közösségi,
nemzeti, nemzetiségi identitástudat és történeti tudat kialakítását.

Tudatosítja a tanulóban, hogy elsősorban nemzete saját hagyományainak,
értékeinek megismerése, elsajátítása és gyakorlása mellett nyitottá
válhat a velünk élő nemzetiségek, vallási közösségek, a szomszéd és a
rokon népek, valamint a világ többi népének kultúrája, az egyetemes
értékek iránt is.

***II.3.4.1. Történelem***

***A) ALAPELVEK, CÉLOK***

Az iskolai történelemtanulás alapját a történettudomány, valamint a
hagyomány által legfontosabbnak elismert történetek, tények, személyek,
események, folyamatok és jelenségek megismerése adja. A történelmi
ismeretek egyfelől megalapozzák a történelmi műveltséget, amely a
tanulót hozzásegíti nemzeti identitása erősítéséhez, valamint ahhoz,
hogy azonosulni tudjon kultúránk alapértékeivel. Másfelől a történelem
ismerete alapvető feltétele annak is, hogy a tanulóban kialakuljon a
történelemről, illetve a társadalmi kérdésekről való árnyalt gondolkodás
és kommunikáció képessége. A történelemtanulás során a tanuló különböző
tevékenységeket, műveleteket végez, így elsajátítja azokat a
kompetenciákat, amelyek hozzájárulnak ahhoz, hogy megértse a múlt és a
jelen társadalmi, politikai, gazdasági és kulturális jelenségeit.

A történelem természete szerint értelmező jellegű, a tényekről alkotott
különböző vélemények szükségszerűen vitákat eredményeznek. A
történelemtanulás során ezek a viták arra ösztönzik a tanulót, hogy
elgondolkodjon az emberi értékekről, illetve az élet alapvető dilemmáit
megjelenítő olyan fogalmakról, mint például az igazságosság, hűség,
hatékonyság, empátia és felelősség. A viták úgy szolgálhatják a
történelemtanulást, ha a tanulóban világossá válik a történelmi tény és
interpretáció közötti különbség, illetve ha megerősödnek benne
társadalmunk és európai, zsidó-keresztény gyökerű civilizációnk
alapértékei.

A történelemtanítás egyik irányítóelve, hogy a magyar történelmet
kontinuitásában, az európai, illetve egyetemes történelmet szigetszerűen
tárgyalja. A történelem tantárgy tantervének középpontjában a magyar
nemzet és Magyarország története áll. Ez a témák arányán, az egyes témák
részletezettségén túl abban is megmutatkozik, hogy több általános
európai jelenség is konkrét magyar példákon keresztül kerül bemutatásra.
Ennek révén a tanuló a magyar történelmi jelenségeket elsősorban nem
általános modellek alapján, hanem a konkrét történelmi helyzet
jellegzetességeit figyelembe véve tanulmányozhatja. Ez a megközelítés
hozzásegíti a tanulót, hogy megértse és méltányolja a magyarság, a
magyar nemzet, illetve Magyarország sajátos helyzetéből adódó
jelenségeket és folyamatokat, így alakulhat ki benne a tényeken alapuló
reális és pozitív nemzettudat.

A történelemtanuláshoz kapcsolódnak a tantervi tartalmakon és a tanórai
munkán kívül a nemzeti ünnepek és emléknapok is, amelyek iskolai
megrendezése segíti a sorsfordító események felidézését, a kollektív
emlékezetben való rögzítését, az emlékezetkultúra kialakítását és
ápolását.

A tanulási folyamat hatékonyságát elősegítik az egyéni és társas tanulás
változatos módszerei és formái, melyek az együttműködés közösségi
élményének a megélését eredményezik. Az iskolai történelemtanítás akkor
tudja elérni a céljait, ha a régmúlt és közelmúlt megismerése
változatos, a tantárgy sajátosságaiból adódó módszerekkel történik,
miközben törekszik az élményszerűségre. Az egyes témák és módszerek
kiválasztása során fontos szempont a tanuló érdeklődésének felkeltése és
fenntartása. Az iskolai történelemtanulás ez által olyan örömforrás
lehet, amely arra ösztönzi a tanulót, hogy az iskolából kilépve is
megmaradjon benne a történelem, illetve a közélet iránti érdeklődés.

A történelemtanulás célja, hogy a tanuló:

1. alapvető ismereteket szerezzen a magyarság, a magyar nemzet és
    Magyarország, az európai civilizáció, valamint az emberiség
    múltjáról;

2. a megszerzett ismeretek által erős, határozott magyar identitással
    rendelkezzen, és így a magyarságot egyszerre tekintse a történelem
    során kialakult emberi közösségnek, valamint természetes
    vonatkoztatási pontnak;

3. elsajátítsa a közös kulturális kód leglényegesebb elemeit
    (szimbólumok, történelmi személyek, történetek, fogalmak,
    alkotások);

4. képes legyen a múlt és a jelen társadalmi, gazdasági, politikai és
    kulturális folyamatairól és jelenségeiről árnyalt, megalapozott
    véleményt alkotni, illetve ezek alakításában cselekvően részt
    vállalni;

5. ismerje a demokratikus államszervezet működését, a jogállamiság
    elveit, az emberi jogokat, és legyen tisztában állampolgári jogaival
    és kötelességeivel;

6. képes legyen a társadalmi viszonyok és folyamatok értelmezésére;

7. ismerje a piacgazdaság működésének elveit, előnyeit és hátrányait,
    lehetőségeit, valamint veszélyeit, és ismereteit alkalmazni tudja
    saját döntései meghozatala során.

A történelemtanulás célja továbbá, hogy a tanulóban:

1. kialakuljon a magyar nemzet és az emberiség története során
    > megnyilvánuló kiemelkedő emberi teljesítmények iránti tisztelet, a
    > történelmi tragédiák áldozatainak szenvedése iránti együttérzés és
    > méltó megemlékezés igénye;

2. kialakuljon az európai civilizációs identitás is, amely az
    > antikvitás, a zsidó-keresztény kultúra, valamint a polgári jogrend
    > alapértékeire épül;

3. kialakuljon a demokratikus elkötelezettség, amely alapvető értéknek
    > tartja a többségi döntéshozatalt, az emberi jogokat, valamint az
    > állampolgári jogokat és kötelességeket;

4. kialakuljon a társadalmi felelősség, szolidaritás és normakövetés,
    > amely alapvető értéknek tartja a közösség iránti elköteleződést,
    > valamint az egyéni választási lehetőségeket;

5. kialakuljon az egyéni kezdeményezőkészség és felelősségvállalás,
    > amely egyszerre tartja alapvető értéknek a szabadságot és a
    > felelősséget, valamint a közösség számára a fenntarthatóság, az
    > élhető élet biztosítását.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

A tantárgy sajátosságából következően jellemző a kronológiai rendezőelv,
de ez a tanulás-tanítás egyes szakaszaiban nem élvez kizárólagosságot. A
magyar történelmet jellemzően kontinuitásában, az egyetemes történelmet
szigetszerűen, csak a legjelentősebb, illetve a nemzeti történelemre
hatást gyakorló eseményeken keresztül láttatja.

A történelemtanulás ugyan az ötödik évfolyamon kezdődik, de iskolai
előzményekre is támaszkodik; az 1--4. évfolyamon elsajátított
kompetenciákra éppúgy, mint az alsó tagozat történelmi tárgyú
olvasmányaira, illetve a tanulónak a nemzeti ünnepek iskolai
megünneplése és az emléknapok nyomán keletkező tudására.

Az életkori sajátosságoknak megfelelően az 5--6. évfolyamon konkrét
élethelyzetek, szokások képszerű bemutatása, történetek elmesélése, azaz
a történettanításon alapuló történelemtanítás jellemző, amely a
történelmi tudatosság és szemlélet kialakítását alapozza meg. A tantervi
témák nagyobb részét történeti korok szerint beágyazott
életmód-történeti és portré típusú témakörök adják. Előbbiek a régmúlt
korok embereinek életét mutatják be egy-egy konkrét, ám tipikus
település mikrovilágán, illetve az ezekhez kapcsolódó történeteken
keresztül. Utóbbiak a magyar történelem néhány kiemelkedő
személyiségének életét, munkásságát és történelmi jelentőségét dolgozzák
fel. A tanítási folyamatban az egyetemes és a magyar történelem
integráltan jelenik meg. Az egyetemes történelem általánosan jellemző
jelenségei, folyamatai elsősorban magyar példákon keresztül kerülnek
bemutatásra. Az életmódtörténetet és a történelmi portrékat olyan témák
egészítik ki, amelyek a mondák és legendák világát, a kereszténységet,
illetve egy-egy korszak hadviselését és világképét dolgozzák fel.

A 7--8. évfolyamra a tanuló formálódó absztrakt gondolkodása és az
ok-okozati összefüggések jobb megértése jellemző, s ez megengedi, hogy a
tananyag alapvetően kronologikus elrendezésű legyen, ami a
legalkalmasabb a mai világot meghatározó 19-20. századi folyamatok
értelmezésére. Az egyetemes és a magyar történeti témák vegyesen,
korszakokhoz köthető témakörökbe rendezve jelennek meg. Kisebb
mértékben, de továbbra is jellemző, hogy az általános jelenségeket a
magyar történelem példáin keresztül mutatjuk be. Az eseménytörténetet a
tanítási időszak végén egy tematikus egység *(Együttélés a
Kárpát-medencében*), illetve több szintetizáló témakör egészíti ki.
Utóbbiak hosszmetszeti jellegűek: a demokrácia, demográfia, a valamint
egyes kiemelt régiók történetét tekintik át kezdetektől napjainkig,
valamint külön témakörként szerepel a *Mérlegen a magyar történelem*,
amely hazánk múltjának közös értékelésére ad lehetőséget. Ezen témakörök
legfőbb célja a négy év alatt tanultak áttekintése mellett a bennük
szereplő témáknak magasabb szintű, szintetizáló értelmezése.

Az általános iskolai történelemtanulásban a történetmesélés, a
képszerűség és a tevékenység-központú megközelítés a témák
kiválasztásának, illetve feldolgozásának egyaránt fontos vezérelve. Mind
az egyes korszakok életmódját bemutató témakörök, témák, mind a
kiemelkedő személyiségeket bemutató portrék, mind pedig az egyéb témák
tárgyalásakor a tanuló képzeletét megragadó történeteken, a személyes
életéhez kapcsolható élményeken keresztül vezet az út. Ugyanakkor a
történetmesélésen keresztül fejlődik a tanulónak a történelemtanuláshoz
elengedhetetlen elbeszélő képessége, narratív kompetenciája is.

A Nat tanévenként legfeljebb 20 téma feldolgozását írja elő. Egy-egy
téma feldolgozásához minimálisan 2-6 tanóra szükséges, ami általában a
rendelkezésre álló időkeret kb. 80%-át veszi igénybe. Miközben az éves
időkeret a korábbi tantervekhez képest nem változott, a kötelező a témák
a korábbinál kevesebb ismeretet ölelnek fel. Mindez időt és alkalmat ad
a tevékenységalapú tanulásra, az ismeretek alkalmazására, a tanulási
eredményekben megfogalmazott kompetenciák fejlesztésére, valamint a
tanár által leghatékonyabbnak tartott tanulási technikák alkalmazására.
Az évfolyamonkénti fennmaradó szabad órakeretet a tanár a
kerettantervvel és a helyi tantervvel összhangban szabadon használhatja
fel a helyi tantervben szereplő helytörténeti vagy egyéb téma
tanítására, speciális órák szervezésére, illetve más tantárgyakkal
együttműködve közös projektek megvalósítására. A szabad órakeret
felhasználása alapvetően a készségek és képességek fejlesztését, az
élményközpontú tanulás erősítését szolgálja.

A helyi tanterv alapján a tanár a kerettantervből évente két témát
mélységelvű feldolgozásra jelöl ki, amelyre több idő, a javasolt
időkereten felül összesen további 6-10 óra tervezhető**.** A mélységelvű
témákra tervezett órák legalább 70%-a a magyar történelemhez kapcsolódó
téma feldolgozására használható. A mélységelvű tanítás lehetőséget ad az
adott téma részletesebb ismeretekkel, többféle megközelítési móddal és
tevékenységgel történő feldolgozására.

A történelemtanítás során a múlt megismertetése mellett nagy hangsúlyt
kap a történelem értelmezése, a differenciált történelmi gondolkodás
megalapozása, amelyben meghatározó a tartalmi és értelmező kulcsfogalmak
adaptív használata. Az értelmező kulcsfogalmak értő használata segíti a
tanulókat az összefüggések feltárásában és megértésében, ezáltal
gyakorlottá válnak magasabb szintű gondolati műveletek (analízis,
szintézis, értékelés) végrehajtásában. A tartalmi kulcsfogalmak olyan
fogalmi eszközkészletet alkotnak, amelynek belső tartalmi elemei a
történelemtanulás során folyamatosan bővülnek. Tudatos használatuk
elősegíti a történelmi ismeretek rendszerezését, felidézését és
alkalmazását. Az értelmező és tartalmi kulcsfogalmak megértése és
elsajátítása a történelemtanulás egész idején át tartó folyamat, amely
különböző kontextusokban való használatuk során valósul meg. Az 5--8.
évfolyamokban ennek megalapozása történik.

A tanuló az 5-8. évfolyamon a következő kulcsfogalmakat használja:

*Értelmező kulcsfogalmak*: történelmi idő, történelmi forrás, ok és
következmény, változás és folyamatosság, tény és bizonyíték, történelmi
jelentőség, értelmezés, történelmi nézőpont.

Tartalmi kulcsfogalmak:

1. politikai: politika, állam, államszervezet, államforma, monarchia,
    köztársaság, egyeduralom, demokrácia, önkormányzat, jog, törvény,
    birodalom;

2. társadalmi: társadalom, társadalmi csoport, réteg, nemzet,
    népcsoport, életmód;

3. gazdasági: gazdaság, pénz, piac, mezőgazdaság, ipar, kereskedelem,
    adó, önellátás, árutermelés, falu, város;

4. eszme- és vallástörténeti: kultúra, művészet, hit, vallás, egyház,
    világkép.

***A tantárgy tanításának specifikus jellemzői a 9--12.***
***évfolyamon***

A történelemtanulás elsősorban a narratív megismerésre épül: történetek,
leírások, sémák és interpretációk egymásra rakódása és kölcsönhatása
révén fejlődik a tanuló történelmi tudása. Míg az általános iskolában
történetek elbeszélése az elsődleges, a középiskolai
történelemtanulásban előtérbe kerül a történelmi források feldolgozása
és értelmezése, ami segíti a problémamegoldó, elemző gondolkodás
fejlődését, a történelmi szemléletmód kialakulását.

A 9--12. évfolyamok tanterve spirális módon épül az 5--8. évfolyamokéra,
és nem ismétli automatikusan azokat. A kronológiai rendezőelv a
középiskolai szakaszban dominánsabbá válik.

A 9--10. évfolyamon egyetemes történelemből csak az általános, illetve a
magyar történelemre hatással bíró jelenségek kerülnek előtérbe,
klasszikus eseménytörténet, egyes országok története nem. A hangsúly a
korszakok gazdasági változásainak, társadalmi szerkezetének, politikai
modelljeinek és világképének bemutatásán van. Az egyetemes és a magyar
történeti témák általában külön témakörökbe szerveződnek, mivel előbbiek
jobbára tematikus, utóbbiak pedig tematikus és eseménytörténeti
jellegűek. A magyar történelem eseményei és folyamatai az egyetemes
történelem által felrajzolt háttér előtt, nemzetközi összefüggésekbe
ágyazva jelennek meg.

A 11--12. évfolyamon kerülnek sorra a jelenismeret szempontjából
legfontosabb 19--21. századi események, jelenségek, folyamatok. Itt
gyakran már az egyetemes történelem is eseménytörténeti jelleggel
szerepel.

Egy-egy téma feldolgozásához minimálisan 2-6 tanóra szükséges, ami
általában a rendelkezésre álló időkeret kb. 80%-át veszi igénybe. Mindez
időt és alkalmat ad a tevékenységalapú tanulásra, az ismeretek
alkalmazására, a tanulási eredményekben megfogalmazott kompetenciák
fejlesztésére, valamint a tanár által leghatékonyabbnak tartott tanulási
technikák alkalmazására. Az évfolyamonkénti fennmaradó szabad órakeretet
a tanár a kerettantervvel és a helyi tantervvel összhangban szabadon
használhatja fel a helyi tantervben szereplő helytörténeti vagy egyéb
téma tanítására, speciális órák szervezésére, illetve más tantárgyakkal
együttműködve közös projektek megvalósítására. A szabad órakeret
felhasználása alapvetően a készségek és képességek fejlesztését, az
élményközpontú tanulás erősítését szolgálja.

A helyi tanterv alapján a tanár a kerettantervből évente két témát
mélységelvű feldolgozásra jelöl ki, amelyre több idő, a javasolt
időkereten felül összesen további 6-10 óra tervezhető**.** A mélységelvű
témákra tervezett órák legalább 70%-a a magyar történelemhez kapcsolódó
téma feldolgozására használható. A mélységelvű tanítás lehetőséget ad az
adott téma részletesebb ismeretekkel, többféle megközelítési móddal és
tevékenységgel történő feldolgozására.

Az értelmező és tartalmi kulcsfogalmak megértése és elsajátítása a
történelemtanulás egész idején át tartó folyamat, amely különböző
kontextusokban való használatuk során valósul meg. A 9--12. évfolyamokon
az 5--8. évfolyamokon bevezetett értelmező és tartalmi kulcsfogalmak
használatának elmélyítése történik.

A tanuló a 9-12. évfolyamon a következő kulcsfogalmakat használja:

*Értelmező kulcsfogalmak*: történelmi idő, történelmi forrás és
bizonyíték; ok és következmény; változás és folyamatosság; történelmi
jelentőség, értelmezés, történelmi nézőpont és interpretáció.

Tartalmi kulcsfogalmak:

1. politikai: politika, állam, államszervezet, államforma, köztársaság,
    diktatúra, demokrácia, parlamentarizmus, monarchia, önkormányzat,
    közigazgatás, hatalmi ágak, jog, alkotmány, alaptörvény, törvény,
    rendelet, birodalom;

2. társadalmi: társadalom, társadalmi csoport, illetve réteg,
    népesedés, demográfia, migráció, nemzet, etnikum, identitás,
    életmód;

3. gazdasági: gazdaság, pénz, piac, mezőgazdaság, ipar, kereskedelem,
    adó, önellátás, árutermelés;

4. eszme- és vallástörténeti: kultúra, művészet, vallás, hit, egyház,
    civilizáció, eszme, ideológia, világkép.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Személyes történelem: körülöttem a történelem; címer, zászló,
    pecsét, az idő mérése.

2. Fejezetek az ókor történetéből: az ókori Egyiptom világa; az ókori
    Hellász öröksége; az ókori Róma öröksége; a görög-római hadviselés;
    képek a népvándorlás korából.

3. A kereszténység: az Ószövetség népe; Jézus élete, tanításai és a
    kereszténység.

4. A középkor világai: élet a várban -- egy magyar vár (pl. Visegrád)
    és uradalom bemutatásával; élet a kolostorban -- egy magyar kolostor
    (pl. Pannonhalma) bemutatásával; élet a középkori városban -- egy
    magyar város (pl. Buda) bemutatásával; a keresztes lovagok világa.

5. Képek és portrék az Árpád-kor történetéből: történetek a magyarok
    eredetéről; honfoglalás és kalandozások; Szent István és a magyar
    állam; Árpád-házi királyportrék; Árpád-kori szentek; Árpád-kori
    győztes harcok és csaták; Magyarország koronázási jelvényei.

6. Képek és portrék a középkori magyar állam virágkorából: magyar
    királyportrék a 14--15. századból; Hunyadi János, a törökverő;
    Hunyadi Mátyás, a reneszánsz uralkodó.

7. Új látóhatárok: a földrajzi felfedezések; korai kapitalizmus; a
    vallási megújulás; az új világkép kialakulása.

8. Portrék és történetek Magyarország kora újkori történetéből: a török
    háborúk hősei; Bocskai, Bethlen és Zrínyi; II. Rákóczi Ferenc és
    szabadságharca; Mária Terézia.

9. Élet a kora újkori Magyarországon: élet a török hódoltság kori
    Magyarországon -- egy konkrét település (pl. Debrecen vagy
    Kecskemét) bemutatásával; élet a 18. századi Magyarországon -- egy
    konkrét település (pl. Temesvár) bemutatásával.

10. Forradalmak kora: ipari forradalom; társadalmi-politikai forradalom.

11. A magyar nemzeti ébredés és polgárosodás kora: a reformkor; a
    forradalom; képek a szabadságharc történetéből; a kiegyezés.

12. A modern kor születése: a nemzeti eszme és a birodalmak kora;
    politikai eszmék: liberalizmus, konzervativizmus, szocializmus.

13. A dualizmus kora: felzárkózás Európához: a modernizálódó
    Magyarország; a millenniumi Magyarország.

14. Az első világháború és következményei: az első világháború,
    Magyarország a háborúban; Magyarország 1918--1919-ben; a trianoni
    békediktátum.

15. Totális diktatúrák: a kommunista Szovjetunió; a nemzetiszocialista
    Németország.

16. A Horthy-korszak: a politika irányai; gazdasági, társadalmi és
    kulturális fejlődés.

17. A második világháború: háború földön, tengeren és levegőben;
    Magyarország a világháború idején; a háború borzalmai; a holokauszt.

18. A megosztott világ: a hidegháború; a Nyugat.

19. Magyarország szovjetizálása: a kommunista diktatúra kiépítése
    Magyarországon; a Rákosi-diktatúra; deportálások „békeidőben".

20. A forradalomtól az ezredfordulóig: az 1956-os forradalom és
    szabadságharc; a kádári diktatúra; élet a Kádár-rendszerben; a
    rendszerváltoztatás; Magyarország a rendszerváltoztatás után.

21. Együttélés a Kárpát-medencében: a határon túli magyarok; a
    magyarországi nemzetiségek a 19. századtól napjainkig.

22. Népesedés és társadalom: a hagyományos, illetve agrártársadalmak; a
    modern, illetve ipari társadalmak.

23. A demokratikus állam: a modern demokrácia gyökerei; a modern magyar
    állam.

24. Régiók története: Magyarország és az Európai Unió; Közép-Európa; az
    Amerikai Egyesült Államok; India; Kína; a Közel-Kelet.

25. Mérlegen a magyar történelem: lábnyomaink a nagyvilágban; a magyar
    megmaradás kérdései.

***FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON***

1. Civilizáció és államszervezet az ókorban: a Közel-Kelet
    civilizációi; a görög civilizáció; az athéni demokrácia; a római
    civilizáció; a római köztársaság.

2. Vallások az ókorban: politeizmus és monoteizmus; a kereszténység
    kezdete.

3. Hódító birodalmak: egy eurázsiai birodalom: a hunok; az Arab
    Birodalom és az iszlám.

4. A középkori Európa: a parasztság világa; az egyházi rend; a nemesi
    rend; a polgárok világa.

5. A magyar nép eredete és az Árpád-kor: magyar őstörténet és
    honfoglalás; az államalapítás; a magyar állam megszilárdulása az
    Árpád-korban.

6. A középkori Magyar Királyság fénykora: az Anjouk; a török fenyegetés
    árnyékában; Hunyadi Mátyás; a magyar középkor kulturális hagyatéka.

7. A kora újkor: a földrajzi felfedezések; a korai kapitalizmus;
    reformáció Európában és Magyarországon; „Hitviták tüzében".

8. A török hódoltság kora Magyarországon: az ország három részre
    szakadása; a két magyar állam; a török kiűzése és a török kor
    mérlege.

9. A felvilágosodás kora: a felvilágosodás; a brit alkotmányos
    monarchia és az amerikai köztársaság működése; a francia forradalom
    és hatása.

10. Magyarország a 18. században: a Rákóczi-szabadságharc; Magyarország
    újranépesülése és újranépesítése; a felvilágosult abszolutizmus
    reformja.

11. Új eszmék és az iparosodás kora: liberalizmus, nacionalizmus és
    konzervativizmus; az ipari forradalom hullámai.

12. A reformkor: a politikai élet színterei; a reformkor fő kérdései.

13. A forradalom és szabadságharc: a forradalom céljai és eredményei; a
    szabadságharc főbb eseményei és kiemelkedő szereplői.

14. Nemzetállamok születése és a szocialista eszmék megjelenése: a
    szocializmus és a munkásmozgalom; a polgári nemzetállam megteremtése
    (Németország, Amerikai Egyesült Államok, Japán).

15. A dualizmus kori Magyarország: a kiegyezés és a dualizmus rendszere;
    a nemzeti és nemzetiségi kérdés, a cigányság helyzete; az ipari
    forradalom Magyarországon; társadalom és életmód a dualizmus
    korában.

16. A nagy háború: az első világháború előzményei: az első világháború;
    az első világháború jellemzői és hatása; Magyarország a
    világháborúban.

17. Az átalakulás évei: szocialista és nemzeti törekvések: a birodalmak
    bomlása; az Osztrák-Magyar Monarchia és a történelmi Magyarország
    szétesése; a tanácsköztársaság és az ellenforradalom; a Párizs
    környéki békék; a trianoni békediktátum.

18. A két világháború között: a kommunista Szovjetunió; a Nyugat és a
    gazdasági világválság; a nemzetiszocialista Németország.

19. A Horthy-korszak: talpra állás Trianon után; az 1930-as évek
    Magyarországa.

20. A második világháború: a tengelyhatalmak sikerei; a szövetségesek
    győzelme; Magyarország a második világháborúban: mozgástér és
    kényszerpálya; a holokauszt; a második világháború jellemzői; az
    ország pusztulása, deportálások a Gulágra.

21. A két világrendszer szembenállása: a kétpólusú világ kialakulása; a
    hidegháború; a gyarmatok felszabadulása.

22. Háborútól forradalomig: az átmenet évei Magyarországon; a
    szovjetizálás Magyarországon; a Rákosi-diktatúra.

23. Az 1956-os forradalom és szabadságharc: a forradalom; a nemzet
    szabadságharca.

24. A kádári diktatúra: a pártállami diktatúra és működése; gazdaság,
    társadalom, életmód.

25. A kétpólusú világ és felbomlása: a Nyugat a 20. század második
    felében; a szocializmus válsága és megrendülése; a kétpólusú világ
    megszűnése.

26. A rendszerváltoztatás folyamata: a Kádár-rendszer végnapjai; a
    rendszerváltoztatás; a piacgazdaság kiépülése.

27. A világ a 21. században: az átalakuló világ; a globális világ.

28. Magyarország a 21. században: a demokrácia működése Magyarországon;
    a magyar bel- és külpolitika főbb jellemzői; Magyarország és az
    Európai Unió.

29. A magyarság és a magyarországi nemzetiségek a 20--21. században: a
    határon túli magyarok; a magyarországi nemzetiségek, a magyarországi
    cigányság.

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. alapvető ismeretekkel rendelkezik a magyar nemzet, Magyarország és
    az európai civilizáció és Földünk legfontosabb régióinak múltjáról
    és jelenéről;

2. képes a múlt és jelen alapvető társadalmi, gazdasági, politikai és
    kulturális folyamatairól, jelenségeiről véleményt alkotni;

3. megérti, hogy minden történelmi eseménynek és folyamatnak okai és
    következményei vannak;

4. ismeri a közös magyar nemzeti, az európai, valamint az egyetemes
    emberi civilizáció kulturális örökségének, kódrendszerének
    legalapvetőbb elemeit;

5. különbséget tud tenni a múltról szóló fiktív történetek és a
    történelmi tények között;

6. megérti és méltányolja, hogy a múlt viszonyai, az emberek
    gondolkodása, értékítélete eltért a maitól;

7. alapvető ismereteket szerez a demokratikus államszervezetről, a
    társadalmi együttműködés szabályairól és a piacgazdaságról.

A nevelési-oktatási szakasz végére a tanulóban:

1. kialakul a múlt iránti érdeklődés;

2. megerősödik benne a nemzeti identitás és hazaszeretet érzése; büszke
    népe múltjára, ápolja hagyományait, és méltón emlékezik meg hazája
    nagyjairól;

3. kialakulnak az európai civilizációs identitás alapelemei;

4. kialakulnak a társadalmi felelősség és normakövetés, az egyéni
    kezdeményezőkészség és a hazája, közösségei és embertársai iránti
    felelősségvállalás, az aktív állampolgárság, valamint a demokratikus
    elkötelezettség alapelemei.

TÖRTÉNELMI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és fel tudja idézni a magyar és az európai történelmi
    hagyományhoz kapcsolódó legfontosabb mítoszokat, mondákat,
    történeteket, elbeszéléseket;

2. be tudja mutatni a különböző korok életmódjának és kultúrájának főbb
    vonásait és az egyes történelmi korszakokban élt emberek életét
    befolyásoló tényezőket;

3. tisztában van a kereszténység kialakulásának főbb állomásaival,
    ismeri a legfontosabb tanításait és hatását az európai civilizációra
    és Magyarországra;

4. ismeri a magyar történelem kiemelkedő alakjait, cselekedeteiket,
    illetve szerepüket a magyar nemzet történetében;

5. fel tudja idézni a magyar történelem legfontosabb eseményeit,
    jelenségeit, folyamatait és fordulópontjait a honfoglalástól
    napjainkig;

6. képes felidézni a magyar nemzet honvédő és szabadságharcait,
    példákat hoz a hazaszeretet, önfeláldozás és hősiesség
    megnyilvánulásaira;

7. tisztában van a középkor és újkor világképének fő vonásaival, a 19.
    és 20. század fontosabb politikai eszméivel és azok hatásaival;

8. ismeri és be tudja mutatni a 19. és 20. századi modernizáció
    gazdasági, társadalmi és kulturális hatásait Magyarországon és a
    világban;

9. ismeri a különböző korok hadviselési szokásait, az első és a második
    világháború legfontosabb eseményeit, jellemzőit, valamint napjainkra
    is hatással bíró következményeit;

10. fel tudja idézni az első és második világháború borzalmait, érveket
    tud felsorakoztatni a békére való törekvés mellett;

11. ismeri a nemzetiszocialista és a kommunista diktatúrák főbb
    jellemzőit, az emberiség ellen elkövetett bűneiket,
    ellentmondásaikat és ezek következményeit, továbbá a velük szembeni
    ellenállás példáit;

12. felismeri a különbségeket a demokratikus és a diktatórikus
    berendezkedések között, érvel a demokrácia értékei mellett;

13. példákat tud felhozni arra, hogy a történelem során miként járultak
    hozzá a magyarok Európa és a világ kulturális, tudományos és
    politikai fejlődéséhez;

14. ismeri a magyarság, illetve a Kárpát-medence népei együttélésének
    jellemzőit néhány történelmi korszakban, beleértve a határon kívüli
    magyarság sorsát, megmaradásáért folytatott küzdelmét, példákat hoz
    a magyar nemzet és a közép-európai régió népeinek kapcsolatára és
    együttműködésére;

15. valós képet alkotva képes elhelyezni Magyarországot a globális
    folyamatokban a történelem során és napjainkban;

16. ismeri hazája államszervezetét.

ISMERETSZERZÉS ÉS FORRÁSHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. képes ismereteket szerezni személyes beszélgetésekből, tárgyak,
    épületek megfigyeléséből, olvasott és hallott, valamint a különböző
    médiumok által felkínált szöveges és képi anyagokból;

2. kiemel lényeges információkat (kulcsszavakat, tételmondatokat)
    elbeszélő vagy leíró, illetve rövidebb magyarázó írott és hallott
    szövegekből és az ezek alapján megfogalmazott kérdésekre egyszerű
    válaszokat adni;

3. megadott szempontok alapján, tanári útmutatás segítségével
    történelmi információkat gyűjt különböző médiumokból és forrásokból
    (könyvek, atlaszok, kronológiák, könyvtárak, múzeumok, médiatárak,
    filmek; nyomtatott és digitális, szöveges és vizuális források);

4. képes élethelyzetek, magatartásformák megfigyelése által értelmezni
    azokat;

5. megadott szempontok alapján tudja értelmezni és rendszerezni a
    történelmi információkat;

6. felismeri, hogy melyik szöveg, kép, egyszerű ábra, grafikon vagy
    diagram kapcsolódik az adott történelmi témához;

7. képen, egyszerű ábrán, grafikonon, diagramon ábrázolt folyamatot,
    jelenséget saját szavaival le tud írni;

8. képes egyszerű esetekben forráskritikát végezni, valamint
    különbséget tenni források között típus és szövegösszefüggés
    alapján;

9. össze tudja vetni a forrásokban található információkat az
    ismereteivel, párhuzamot tud vonni különböző típusú (pl. szöveges és
    képi) történelmi források tartalma között;

10. meg tudja vizsgálni, hogy a történet szerzője résztvevője vagy
    kortársa volt-e az eseményeknek;

11. egyszerű következtetéseket von le, és véleményt tud alkotni
    különböző források hitelességéről és releváns voltáról.

TÁJÉKOZÓDÁS IDŐBEN ÉS TÉRBEN

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a nagy történelmi korszakok elnevezését és időhatárait,
    néhány kiemelten fontos esemény, jelenség és történelmi folyamat
    időpontját;

2. biztonsággal használja az idő tagolására szolgáló kifejezéseket,
    történelmi eseményre, jelenségre, folyamatra, korszakra való
    utalással végez időmeghatározást;

3. ismeretei segítségével időrendbe tud állítani történelmi
    eseményeket, képes az idő ábrázolására pl. időszalag segítségével;

4. a tanult történelmi eseményeket, jelenségeket, személyeket, ikonikus
    szimbólumokat, tárgyakat, képeket hozzá tudja rendelni egy adott
    történelmi korhoz, régióhoz, államhoz;

5. biztonsággal használ különböző történelmi térképeket a fontosabb
    történelmi események helyszíneinek azonosítására, egyszerű
    jelenségek, folyamatok leolvasására, értelmezésére, vaktérképen való
    elhelyezésére;

6. egyszerű alaprajzokat, modelleket, térképvázlatokat (pl.
    települések, épületek, csaták) tervez és készít.

SZAKTÁRGYI KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan, folyamatos beszéddel képes eseményeket, történeteket
    elmondani, történelmi személyeket bemutatni, saját véleményt
    megfogalmazni;

2. össze tudja foglalni saját szavaival hosszabb elbeszélő vagy leíró,
    valamint rövidebb magyarázó szövegek tartalmát;

3. az általa gyűjtött történelmi adatokból, szövegekből rövid tartalmi
    ismertetőt tud készíteni;

4. képes önálló kérdések megfogalmazására a tárgyalt történelmi
    témával, eseményekkel, folyamatokkal kapcsolatban;

5. képes rövid fogalmazások készítésére egy-egy történetről, történelmi
    témáról;

6. különböző történelmi korszakok, történelmi és társadalmi kérdések
    tárgyalása során szakszerűen alkalmazza az értelmező és tartalmi
    kulcsfogalmakat, továbbá használja a témához kapcsolódó történelmi
    fogalmakat;

7. tud egyszerű vizuális rendezőket készíteni és kiegészíteni
    hagyományos vagy digitális módon (táblázatok, ábrák, tablók, rajzok,
    vázlatok) egy történelmi témáról;

8. egyszerű történelmi témáról tanári útmutatás segítségével
    kiselőadást és digitális prezentációt állít össze és mutat be;

9. egyszerű történelmi kérdésekről tárgyilagos véleményt tud
    megfogalmazni, állításait alátámasztja;

10. meghallgatja és megérti -- adott esetben elfogadja -- mások
    véleményét, érveit;

11. tanári segítséggel dramatikusan, szerepjáték formájában tud
    megjeleníteni történelmi eseményeket, jelenségeket, személyiségeket.

TÖRTÉNELMI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. adott történetben különbséget tud tenni fiktív és valós, irreális és
    reális elemek között;

2. képes megfigyelni, értelmezni és összehasonlítani a történelemben
    előforduló különböző emberi magatartásformákat és élethelyzeteket;

3. a történelmi eseményekkel, folyamatokkal és személyekkel
    kapcsolatban önálló kérdéseket fogalmaz meg;

4. feltételezéseket fogalmaz meg történelmi személyek cselekedeteinek
    mozgatórugóiról, és adatokkal, érvekkel alátámasztja azokat;

5. a történelmi szereplők megnyilvánulásainak szándékot tulajdonít,
    álláspontjukat azonosítja;

6. önálló véleményt képes megfogalmazni történelmi szereplőkről,
    eseményekről, folyamatokról;

7. felismeri és értékeli a különböző korokra és régiókra jellemző
    tárgyakat, alkotásokat, életmódokat, szokásokat, változásokat, képes
    azokat összehasonlítani egymással, illetve a mai korral;

8. társadalmi és erkölcsi problémákat azonosít adott történetek,
    történelmi események, különböző korok szokásai alapján;

9. példákat hoz a történelmi jelenségekre, folyamatokra;

10. feltételezéseket fogalmaz meg néhány fontos történelmi esemény és
    folyamat feltételeiről, okairól és következményeiről, és tényekkel
    alátámasztja azokat;

11. több szempontból képes megkülönböztetni a történelmi jelenségek és
    események okait és következményeit (pl. hosszú vagy rövid távú,
    gazdasági, társadalmi vagy politikai);

12. felismeri, hogy az emberi cselekedet és annak következménye között
    szoros kapcsolat van.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. megbízható ismeretekkel bír az európai, valamint az egyetemes
    történelem és mélyebb tudással rendelkezik a magyar történelem
    fontosabb eseményeiről, történelmi folyamatairól, fordulópontjairól;

2. képes a múlt és jelen társadalmi, gazdasági, politikai és kulturális
    folyamatairól, jelenségeiről többszempontú, tárgyilagos érveléssel
    alátámasztott véleményt alkotni, ezekkel kapcsolatos problémákat
    megfogalmazni;

3. ismeri a közös magyar nemzeti és európai, valamint az egyetemes
    emberi civilizáció kulturális örökségének, kódrendszerének lényeges
    elemeit;

4. különbséget tud tenni történelmi tények és történelmi interpretáció,
    illetve vélemény között;

5. képes következtetni történelmi események, folyamatok és jelenségek
    okaira és következményeire;

6. képes a tanulási célhoz megfelelő információforrást választani, a
    források között szelektálni, azokat szakszerűen feldolgozni és
    értelmezni;

7. kialakul a hiteles és tárgyilagos forráshasználat és kritika igénye;

8. képes a múlt eseményeit és jelenségeit a saját történelmi
    összefüggésükben értelmezni, illetve a jelen viszonyait kapcsolatba
    hozni a múltban történtekkel;

9. ismeri a demokratikus államszervezet működését, a társadalmi
    együttműködés szabályait, a piacgazdaság alapelveit; autonóm és
    felelős állampolgárként viselkedik.

A nevelési-oktatási szakasz végére a tanulóban:

1. kialakul és megerősödik a történelmi múlt, illetve a társadalmi,
    politikai, gazdasági és kulturális kérdések iránti érdeklődés;

2. kialakulnak a saját értékrend és történelemszemlélet alapjai;

3. elmélyül a nemzeti identitás és hazaszeretet, büszke népe múltjára,
    ápolja hagyományait, és méltón emlékezik meg hazája nagyjairól;

4. megerősödnek az európai civilizációs identitás alapelemei;

5. megerősödik és elmélyül a társadalmi felelősség és normakövetés, az
    egyéni kezdeményezőkészség, a hazája, közösségei és embertársai
    iránt való felelősségvállalás, valamint a demokratikus
    elkötelezettség.

TÖRTÉNELMI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az ókori civilizációk legfontosabb jellemzőit, valamint az
    athéni demokrácia és a római állam működését, hatásukat az európai
    civilizációra;

2. felidézi a monoteista vallások kialakulását, legfontosabb
    jellemzőiket, tanításaik főbb elemeit, és bemutatja terjedésüket;

3. bemutatja a keresztény vallás civilizációformáló hatását, a
    középkori egyházat, valamint a reformáció és a katolikus megújulás
    folyamatát és kulturális hatásait; érvel a vallási türelem, illetve
    a vallásszabadság mellett;

4. képes felidézni a középkor gazdasági és kulturális jellemzőit,
    világképét, a kor meghatározó birodalmait és bemutatni a rendi
    társadalmat;

5. ismeri a magyar nép őstörténetére és a honfoglalásra vonatkozó
    tudományos elképzeléseket és tényeket, tisztában van legfőbb
    vitatott kérdéseivel, a különböző tudományterületek kutatásainak
    főbb eredményeivel;

6. értékeli az államalapítás, valamint a kereszténység felvételének
    jelentőségét;

7. felidézi a középkori magyar állam történetének fordulópontjait,
    legfontosabb uralkodóink tetteit;

8. ismeri a magyarság törökellenes küzdelmeit, fordulópontjait és
    hőseit; felismeri, hogy a magyar és az európai történelem alakulását
    meghatározóan befolyásolta a török megszállás;

9. be tudja mutatni a kora újkor fő gazdasági és társadalmi
    folyamatait, ismeri a felvilágosodás eszméit, illetve azok
    kulturális és politikai hatását, valamint véleményt formál a francia
    forradalom európai hatásáról;

10. összefüggéseiben és folyamatában fel tudja idézni, miként hatott a
    magyar történelemre a Habsburg Birodalomhoz való tartozás, bemutatja
    az együttműködés és konfrontáció megnyilvánulásait, a függetlenségi
    törekvéseket és értékeli a Rákóczi-szabadságharc jelentőségét;

11. ismeri és értékeli a magyar nemzetnek a polgári átalakulás és
    nemzeti függetlenség elérésére tett erőfeszítéseit a reformkor, az
    1848/49-es forradalom és szabadságharc, illetve az azt követő
    időszakban; a kor kiemelkedő magyar politikusait és azok nézeteit,
    véleményt tud formálni a kiegyezésről;

12. fel tudja idézni az ipari forradalom szakaszait, illetve azok
    gazdasági, társadalmi, kulturális és politikai hatásait; képes
    bemutatni a modern polgári társadalom és állam jellemzőit és a 19.
    század főbb politikai eszméit, valamint felismeri a hasonlóságot és
    különbséget azok mai formái között;

13. fel tudja idézni az első világháború előzményeit, a háború
    jellemzőit és fontosabb fordulópontjait, értékeli a háborúkat lezáró
    békék tartalmát, és felismeri a háborúnak a 20. század egészére
    gyakorolt hatását;

14. bemutatja az első világháború magyar vonatkozásait, a háborús
    vereség következményeit; példákat tud hozni a háborús helytállásra;

15. képes felidézni azokat az okokat és körülményeket, amelyek a
    történelmi Magyarország felbomlásához vezettek;

16. tisztában van a trianoni békediktátum tartalmával és
    következményeivel, be tudja mutatni az ország talpra állását, a
    Horthy-korszak politikai, gazdasági, társadalmi és kulturális
    viszonyait, felismeri a magyar külpolitika mozgásterének
    korlátozottságát;

17. össze tudja hasonlítani a nemzetiszocialista és a kommunista
    ideológiát és diktatúrát, példák segítségével bemutatja a rendszerek
    embertelenségét és a velük szembeni ellenállás formáit;

18. képes felidézni a második világháború okait, a háború jellemzőit és
    fontosabb fordulópontjait, ismeri a holokausztot és a hozzávezető
    okokat;

19. bemutatja Magyarország revíziós lépéseit, a háborús részvételét, az
    ország német megszállását, a magyar zsidóság tragédiáját, a szovjet
    megszállást, a polgári lakosság szenvedését, a hadifoglyok
    embertelen sorsát;

20. össze tudja hasonlítani a nyugati demokratikus világ és a kommunista
    szovjet blokk politikai és társadalmi berendezkedését, képes
    jellemezni a hidegháború időszakát, bemutatni a gyarmati rendszer
    felbomlását és az európai kommunista rendszerek összeomlását;

21. bemutatja a kommunista diktatúra magyarországi kiépítését, működését
    és változatait, az 1956-os forradalom és szabadságharc okait,
    eseményeit és hőseit, összefüggéseiben szemléli a
    rendszerváltoztatás folyamatát, felismerve annak történelmi
    jelentőségét;

22. bemutatja a gyarmati rendszer felbomlásának következményeit, India,
    Kína és a közel-keleti régió helyzetét és jelentőségét;

23. ismeri és reálisan látja a többpólusú világ jellemzőit napjainkban,
    elhelyezi Magyarországot a globális világ folyamataiban;

24. bemutatja a határon túli magyarság helyzetét, a megmaradásért való
    küzdelmét Trianontól napjainkig;

25. ismeri a magyar cigányság történetének főbb állomásait, bemutatja
    jelenkori helyzetét;

26. ismeri a magyarság, illetve a Kárpát-medence népei együttélésének
    jellemzőit, példákat hoz a magyar nemzet és a közép-európai régió
    népeinek kapcsolatára, különös tekintettel a visegrádi
    együttműködésre;

27. ismeri hazája államszervezetét, választási rendszerét.

ISMERETSZERZÉS ÉS FORRÁSHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan tud használni általános és történelmi, nyomtatott és
    digitális információforrásokat (tankönyv, kézikönyvek, szakkönyvek,
    lexikonok, képzőművészeti alkotások, könyvtár és egyéb adatbázisok,
    filmek, keresők);

2. önállóan információkat tud gyűjteni, áttekinteni, rendszerezni és
    értelmezni különböző médiumokból és írásos vagy képi forrásokból,
    statisztikákból, diagramokból, térképekről, nyomtatott és digitális
    felületekről;

3. tud forráskritikát végezni és különbséget tenni a források között
    hitelesség, típus és szövegösszefüggés alapján;

4. képes azonosítani a különböző források szerzőinek a szándékát,
    bizonyítékok alapján értékeli egy forrás hitelességét;

5. képes a szándékainak megfelelő információkat kiválasztani különböző
    műfajú forrásokból;

6. összehasonlítja a forrásokban talált információkat saját
    ismereteivel, illetve más források információival és megmagyarázza
    az eltérések okait;

7. képes kiválasztani a megfelelő forrást valamely történelmi állítás,
    vélemény alátámasztására vagy cáfolására.

TÁJÉKOZÓDÁS IDŐBEN ÉS TÉRBEN

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a magyar és az európai történelem tanult történelmi
    korszakait, időszakait, és képes azokat időben és térben elhelyezni;

2. az egyes események, folyamatok idejét konkrét történelmi korhoz,
    időszakhoz kapcsolja vagy viszonyítja, ismeri néhány kiemelten
    fontos esemény, jelenség időpontját, kronológiát használ és készít;

3. össze tudja hasonlítani megadott szempontok alapján az egyes
    történelmi korszakok, időszakok jellegzetességeit az egyetemes és a
    magyar történelem egymáshoz kapcsolódó eseményeit;

4. képes azonosítani a tanult egyetemes és magyar történelmi
    személyiségek közül a kortársakat;

5. felismeri, hogy a magyar történelem az európai történelem része, és
    példákat tud hozni a magyar és európai történelem kölcsönhatásaira;

6. egyszerű történelmi térképvázlatot alkot hagyományos és digitális
    eljárással;

7. a földrajzi környezet és a történeti folyamatok összefüggéseit
    példákkal képes alátámasztani;

8. képes különböző időszakok történelmi térképeinek összehasonlítására,
    a történelmi tér változásainak és a történelmi mozgások követésére
    megadott szempontok alapján a változások hátterének feltárásával.

SZAKTÁRGYI KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. képes a történelmi jelenségeket általános és konkrét történelmi
    fogalmak, tartalmi és értelmező kulcsfogalmak felhasználásával
    értelmezni és értékelni;

2. fel tud ismerni fontosabb történelmi fogalmakat meghatározás
    alapján;

3. képes kiválasztani, rendezni és alkalmazni az azonos korhoz, témához
    kapcsolható fogalmakat;

4. össze tudja foglalni rövid és egyszerű szaktudományos szöveg
    tartalmát;

5. képes önállóan vázlatot készíteni és jegyzetelni;

6. képes egy-egy korszakot átfogó módon bemutatni;

7. történelmi témáról kiselőadást, digitális prezentációt alkot és
    mutat be;

8. történelmi tárgyú folyamatábrákat, digitális táblázatokat,
    diagramokat készít, történelmi, gazdasági, társadalmi és politikai
    modelleket vizuálisan is meg tud jeleníteni;

9. megadott szempontok alapján történelmi tárgyú szerkesztett szöveget
    (esszét) tud alkotni, amelynek során tételmondatokat fogalmaz meg,
    állításait több szempontból indokolja és következtetéseket von le;

10. társaival képes megvitatni történelmi kérdéseket, amelynek során
    bizonyítékokon alapuló érvekkel megindokolja a véleményét, és
    választékosan reflektál mások véleményére, árnyalja saját
    álláspontját.

TÖRTÉNELMI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. képes felismerni, megfogalmazni és összehasonlítani különböző
    társadalmi és történelmi problémákat, értékrendeket, jelenségeket,
    folyamatokat;

2. a tanult ismereteket problémaközpontúan tudja rendezni;

3. hipotéziseket alkot történelmi személyek, társadalmi csoportok és
    intézmények viselkedésének mozgatórugóiról;

4. önálló kérdéseket fogalmaz meg történelmi folyamatok, jelenségek és
    események feltételeiről, okairól és következményeiről;

5. önálló véleményt tud alkotni történelmi eseményekről, folyamatokról,
    jelenségekről és személyekről;

6. képes különböző élethelyzetek, magatartásformák megfigyelése által
    következtetések levonására, erkölcsi kérdéseket is felvető
    történelmi helyzetek felismerésére és megítélésére;

7. a változás és a fejlődés fogalma közötti különbséget ismerve képes
    felismerni és bemutatni azokat azonos korszakon belül, vagy azokon
    átívelően;

8. képes összevetni, csoportosítani és súlyozni az egyes történelmi
    folyamatok, jelenségek, események okait, következményeit, és
    ítéletet alkotni azokról, valamint a benne résztvevők szándékairól;

9. összehasonlít különböző, egymáshoz hasonló történeti helyzeteket,
    folyamatokat, jelenségeket;

10. képes felismerni konkrét történelmi helyzetekben, jelenségekben és
    folyamatokban valamely általános szabályszerűség érvényesülését;

11. összehasonlítja és kritikusan értékeli az egyes történelmi
    folyamatokkal, eseményekkel és személyekkel kapcsolatos eltérő
    álláspontokat;

12. feltevéseket fogalmaz meg, azok mellett érveket gyűjt, illetve
    mérlegeli az ellenérveket;

13. felismeri, hogy a jelen társadalmi, gazdasági, politikai és
    kulturális viszonyai a múltbeli események, tényezők
    következményeiként alakultak ki.

II.3.4.2. Állampolgári ismeretek

A\) Alapelvek, célok

A globalizáció korában az ember és a magyar nemzet számos új kihívással
szembesül. Az egyénnek olyan kérdésekre kell válaszokat találnia, olyan
problémákra szükséges megoldásokat keresnie, amelyek befolyásolják,
esetenként pedig meghatározzák lehetőségeit, mindennapi életének
alakítását, lokális és tágabb környezetét egyaránt. Az emberi létezésre,
az egyén és a közösség viszonyára, a társadalmi változásokra vonatkozó
válaszoknak nemcsak az egyéni akaratok, érdekek érvényesítése, hanem a
társadalom alapegységét jelentő család, a lokális közösségek és a nemzet
jövőjének alakítása szempontjából is jelentősége van.

A tantárgy civilizációs értékeket és mintákat közvetít, erősíti a
felelősségtudatot és a nemzethez tartozás érzését. Ennek érdekében
segíti a szituációhoz igazodó kommunikáció kialakítását, a másik ember
gondolatainak, véleményének megértését, az együttműködés lehetőségeinek
feltérképezését és közösségformáló erejének megtapasztalását, a hazafias
érzés kialakítását, az alapvető honvédelmi ismeretek elsajátítását, a
nemzedékek közötti párbeszéd erősítését, valamint a környezeti,
gazdasági-pénzügyi fenntarthatóság feltételeinek kiemelését.

A tantárgy keretében a tanuló elsajátítja a tudatos és felelős
állampolgári léthez szükséges alapvető ismereteket, a szociális
kompetenciák fejlesztése révén megismeri és gyakorolja azokat az
eljárásokat, készségeket, amelyek a társadalmi részvételéhez, mindennapi
boldogulásához szükségesek. A tanuló megismeri és megérti, hogy a haza
védelme nem csak a fegyveres erők feladata, hanem minden magyar
állampolgáré, ezért a honvédelem ügye a lehető legszélesebb nemzeti
egységet képviseli.

A tantárgy a saját tapasztalatokra épülő ismeretszerzésre, élményalapú
tanulásra, továbbá javaslatok megfogalmazására, tervek készítésére is
lehetőséget biztosít, számos esetben feltételezi a több nézőpontú
mérlegelő gondolkodás és szemléletmód érvényesülését. Segíti a
mindennapi élet megszervezését, támpontokat nyújt a felnőtt szerepekre
való felkészüléshez.

A jogi kultúra fejlesztése, az alapvető emberi jogok értelmezése, a
szabadságértékek kiemelése, a társadalmi normák, a szabadság és
felelősség kapcsolatának tudatosítása segíti az autonóm személyiség
kialakulását. A saját vélemény kifejezése, a társak gondolatainak,
véleményének megértése, a vitakultúra fejlesztése a demokratikus attitűd
megalapozásához járul hozzá.

Az állampolgári ismeretek tanulásának célja, hogy a tanuló:

1. megértse a jogérvényesülés és a jogi, erkölcsi normák társadalmi
    jelentőségét, hogy képes legyen egyensúlyt teremteni a közösség
    érdekei és az egyén törekvései között;

2. ismerje a demokratikus jogállam felépítését és működését, hogy
    tájékozott, a közügyek iránt érdeklődő személyiséggé válhasson;

3. nevezze meg a magyar nemzettudat sajátosságait, értse meg a nemzeti
    identitás jelentőségét az egyén és a közösség szempontjából is;

4. ismerje lakóhelye kulturális és néprajzi értékeit, váljék
    élményalapúvá lokálpatriotizmusa, személyiségébe épüljenek be a
    nemzeti közösséghez tartozás emocionális összetevői; mélyüljön el
    hazaszeretete;

5. értse meg a haza védelmének fontosságát, ismerje meg, hogy a
    honvédelem milyen feladatköröket foglal magában, és milyen
    kötelezettséget ró minden magyar állampolgárra;

6. sajátítsa el azokat az ismereteket és készségeket, amelyek a
    mindennapi életben való tájékozódását segítik;

7. segítséget kapjon a felnőtt szerepekre való felkészüléséhez, hogy
    életpályáját, jövőjét megalapozottan tervezhesse meg;

8. törekedjék a generációk közötti kapcsolat kialakítására, a
    nemzedékek közötti párbeszéd erősítésére;

9. értse meg a környezeti és gazdasági-pénzügyi fenntarthatóság
    jelentőségét a mindennapi életben, az egyén, a család, a település,
    a régió és az állam szintjén, valamint globális perspektívában is;

10. támpontokat kapjon ahhoz, hogy pénzügyi döntéseit körültekintően és
    megalapozottan hozhassa meg;

11. szemléletét problémamegoldó attitűd jellemezze, fejlődjék
    rendszerszemlélete, nyitottá váljon javaslatok megfogalmazására,
    tervek, tervezetek elkészítésére;

12. fejlessze a konstruktív alkalmazkodáson alapuló hiteles és hatékony
    kommunikációs készségeit;

13. megtapasztalja a társas együttműködés közösségformáló szerepét, és
    felismerje társadalmi jelentőségét;

14. az információk szerzéséhez és rendszerezéséhez, fotógalériák
    összeállításához, beszámolók elkészítéséhez használja az
    infokommunikációs eszközöket.

A tantárgy tanításának specifikus jellemzői a 8. évfolyamon

Az állampolgári ismeretek tantárgy tevékenységei révén segíti a tanuló
szocializációját: tájékozódását a mindennapi életben, felelős
állampolgárrá válását, egyúttal felkészülését a felnőtt szerepekre.

A tanuló tapasztalataira és élményeire építve a 8. évfolyamon a tantárgy
keretében vizsgálja saját társadalmi környezetét, lakóhelyének
intézményeit.

Tudatosul benne, hogy a jogérvényesülés feltételezi a normakövető
magatartást is. A közösen kiválasztott témák és a minden résztvevő által
elfogadott szabályrendszer alapján megrendezett viták, vitanapok nemcsak
az érvek-ellenérvek ütköztetésére alkalmasak, hanem végső soron a
politikai kultúra megalapozását is jelenthetik.

A családhoz, a lakóhelyhez, a kortárscsoporthoz, az iskolához és a
nemzethez mint közösségekhez való kapcsolódás, a velük történő
azonosulás a tanulási tevékenységek sokféle lehetőségét biztosítják. A
tanuló megtanulja a másik ember életének, értékeinek, gondolatainak
tiszteletben tartását, erősödik benne az empátia, kialakul benne az
emberi cselekedetek okai megismerésének igénye. A tantárgy elősegíti az
előítéletek csökkentését és a nemzedékek közötti kapcsolat és párbeszéd
erősítését. Az igazságszolgáltatás és az alapvető ellátórendszerek
funkciójának és működésének bemutatásával, a mindennapi ügyintézésben
való jártasság megalapozásával is segíti a tanuló társadalmi
integrációját, felkészülését a felnőtt szerepekre.

A fenntarthatósági szemlélet alakítása elsősorban a családi háztartásra,
a környezettudatos életvitel kialakítására vonatkozik, ugyanakkor
elősegíti a globális problémák kontextusban történő értelmezését is. A
pénzügyi tudatosság fejlesztése elősegíti, hogy a tanuló pénzügyeit
megfontoltan intézze, és tudatos fogyasztóvá váljon.

Az állampolgári ismeretek tantárgy lehetőséget biztosít az egyéni és a
társas tanulásra is, amelyek egyaránt fejlesztik a szociális
kompetenciákat és az önismeretet.

A *Családtörténeti kutatás, a Településkutatás* és a *Szakmák,
foglalkozások, mesterség és hivatás* projektet az iskolai témahetek,
tematikus hetek, tematikus napok keretében valósíthatják meg az
osztályok, illetve a tanulócsoportok.

A tantárgy tanulási folyamatában a következő értelmező és tartalmi
kulcsfogalmak használata segíti a tanulót a megértésben, a
rendszerezésben, a magyarázatok és következtetések megfogalmazásában és
a problémaközpontú kérdések megvitatásában.

*Értelmező kulcsfogalmak:* okok és következmények, folyamatosság és
változás, interpretáció, identitás, szocializáció; honvédelem; haza;
pénzügyi tudatosság; környezeti, gazdasági-pénzügyi fenntarthatóság.

*Tartalmi kulcsfogalmak:* család; település, intézmény, szervezet;
társadalmi csoport; nemzet, nemzetiség; állampolgár;
társadalombiztosítás; közteherviselés; katonai szolgálat; demokratikus
jogállam.

A tantárgy tanításának specifikus jellemzői a 12. évfolyamon

Az állampolgári ismeretek tantárgy tanulása révén a 12. évfolyamon a
tanuló támpontokat kap a mindennapi életben történő tájékozódáshoz és
eligazodáshoz, ismereteket és értelmezési szempontokat hasznosít a
társadalmi jelenségek megértéséhez. Az ismeretek, az alapvető fogalmak
elsajátításán túl lehetőség nyílik készségfejlesztésre, olyan
tevékenységek kipróbálására és gyakorlására, amelyek segítik
felkészülését a felnőttkori szerepekre, élethelyzetekre, megalapozzák a
tájékozott, nyitott, érdeklődő és kritikus polgári mentalitást. Az
egyéni szocializáció alakítását, fejlesztését a családra vonatkozó
tanulási eredmények támogatják: a párválasztás, a felelős családtervezés
szempontjainak értelmezésével és megbeszélésével, a gyermekvállalás
demográfiai jelentőségének kiemelésével.

A tanuló történelmi tanulmányai során szerzett tudását felidézve,
megszerzett ismereteit tovább építi az alapjogok elemzése, a szabadság
és felelősség kapcsolatának értelmezése, az önálló életvitel
kialakításához, a mindennapi élet megszervezéséhez szükséges jogi
ismeretek elsajátításával. A tanuló értelmezi a nemzeti identitás
alkotóelemeit, társaival megbeszéli a lokálpatriotizmus és a hazafiság
lehetséges megnyilvánulási formáit, megismeri a határon túl és a
diaszpórában élő magyar közösségek életét, eredményeit, törekvéseit és
gondjait. Értelmezi és társaival megvitatja a honvédelem és a
biztonságpolitika alapkérdéseit.

A környezeti, gazdasági fenntarthatóság és a pénzügyi tudatosság
továbbfejlesztésekor lehetőség nyílik a különböző vélemények,
álláspontok értelmezésére, tervek készítésére, megvalósításuk közös
mérlegelésére és a kockázatvállalás tudatosítására.

A tanuló felismeri, hogy a véleménynyilvánítás, az érvelés és a vita nem
öncélú tevékenység, mert a középpontban egy fontos téma vagy probléma
áll. Megismeri mások véleményét, és számíthat arra, hogy ő is kifejtheti
álláspontját. A vita révén több szempontot figyelembe vevő megoldás
születhet. Felismeri, hogy a vita közösségi élménye mintául szolgálhat
későbbi életszakaszaiban is.

A tanuló tudatosan készül későbbi munkavállalói szerepére, ezért
megismeri a munka világát érintő alapvető jogi szabályozást, illetve
tájékozódik a munkaerő-piaci szerepekről, a munkaerőpiac helyzetéről,
változásairól. Felismeri a szerepét a társadalmi munkamegosztásban.

A tantárgy tanulási folyamatában a következő értelmező és tartalmi
kulcsfogalmak használata segíti a tanulót a megértésben, a
rendszerezésben, a magyarázatok és következtetések megfogalmazásában és
a problémaközpontú kérdések megvitatásában.

*Értelmező kulcsfogalmak:* okok és következmények, folyamatosság és
változás, interpretáció, identitás, szocializáció; pénzügyi tudatosság;
környezeti, gazdasági-pénzügyi fenntarthatóság; hazaszeretet;
honvédelem.

*Tartalmi kulcsfogalmak:* család; település, intézmény, szervezet;
társadalmi csoport; nemzet; alkotmány, állam, állampolgár, állampolgári
jogok és kötelességek, norma; választójog; önkormányzat; családi
háztartás, államháztartás, adó, közteherviselés; pénzügyi intézmény,
bank, hitel; fogyasztóvédelem; vállalkozás; nemzetiség; katonai
szolgálat; rendvédelem; katasztrófavédelem.

B\) Fő témakörök

FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON

1. A család, a családi szocializáció jellemzői, a hagyományos
    családmodell

2. A család gazdálkodása és pénzügyei

3. Településünk, lakóhelyünk megismerése

4. Nemzet, nemzetiség; a haza iránti kötelezettségeink

5. A magyar állam alapvető intézményei, az állam szerepe a gazdaságban

6. Mindennapi ügyintézés, felkészülés a felnőttkori szerepekre,
    feladatokra

7. A fogyasztóvédelem alapjai

8. A nagy ellátórendszerek: oktatás, egészségügy és a szociális ellátás

FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON

1. A család, a családi szocializáció

2. A család gazdálkodása és pénzügyei

3. Szabadság és felelősség; jogok és kötelezettségek, a társadalmi
    felelősségvállalás

4. Nemzet, nemzettudat; lokálpatriotizmus, hazafiság, honvédelem

5. A magyar állam intézményei, az állam gazdasági szerepvállalása

6. A mindennapi ügyintézés területei és megszervezése

7. Fogyasztóvédelem

8. Környezet- és természetvédelem

9. Bankrendszer, hitelfelvétel

10. Vállalkozás és vállalat

C\) Tanulási eredmények

ÁTFOGÓ CÉLKÉNT KITŰZÖTT TANULÁSI EREDMÉNYEK AZ 5--8. ÉVFOLYAMON

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a család, mint a társadalom alapvető intézményének szerepét,
    és értelmezi jellemzőit.

2. lokálpatriotizmusa megerősödik, személyiségébe beépülnek a nemzeti
    közösséghez tartozás, a hazaszeretet emocionális összetevői.

3. ismeri a demokratikus jogállam működésének alapvető sajátosságait,
    alapvető kötelezettségeit.

4. jártasságot szerez mindennapi ügyeinek intézésében.

5. saját pénzügyeiben tudatos döntéseket hoz.

AZ EGYÉNI ÉS CSALÁDI SZOCIALIZÁCIÓ SEGÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi a család mint a társadalom alapvető intézményének szerepét
    és jellemzőit;

2. értelmezi a családi kohézió alapelemeit, jellemzőit: együttműködés,
    szeretetközösség, kölcsönösség, tisztelet;

3. felismeri a családi szocializációnak az ember életútját befolyásoló
    jelentőségét.

AZ AKTÍV TÁRSADALMI CSELEKVÉS ÉS A FELELŐS ÁLLAMPOLGÁRI MAGATARTÁS
MEGALAPOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a magyar állam alapvető intézményeinek feladatkörét és
    működését;

2. értelmezi a törvényalkotás folyamatát;

3. ismeri a saját településének, lakóhelyének alapvető jellemzőit,
    értelmezi a településen működő intézmények és szervezetek szerepét
    és működését;

4. a lakóhelyével kapcsolatos javaslatokat fogalmaz meg, tervet készít
    a település fejlesztésének lehetőségeiről;

5. felismeri a jogok és kötelességek közötti egyensúly kialakításának
    és fenntartásának fontosságát, megismeri a haza iránti
    kötelezettségeit, feladatait.

A LOKÁLPATRIOTIZMUS ÉS A NEMZETI IDENTITÁS ERŐSÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri településének, lakóhelyének kulturális, néprajzi értékeit, a
    település történetének alapvető eseményeit és fordulópontjait;

2. megfogalmazza a nemzeti identitás jelentőségét az egyén és a
    közösség szempontjából is;

3. felismeri a nemzetek, nemzetállamok fontosságát a globális világban;

4. megismeri és értelmezi a honvédelem jelentőségét, feladatait és
    szerepét.

FELKÉSZÜLÉS A FELNŐTT SZEREPEKRE

A nevelési-oktatási szakasz végére a tanuló:

1. azonosítja a mindennapi ügyintézés alapintézményeit, az alapvető
    ellátó rendszerek funkcióját és működési sajátosságait;

2. azonosítja az igazságszolgáltatás intézményeit és működésük
    jellemzőit, megismeri az alapvető ellátórendszereket és funkcióikat;

3. megismeri és értelmezi a diákmunka alapvető jogi feltételeit,
    kereteit;

4. információkat gyűjt és értelmez a foglalkoztatási helyzetről, a
    szakmaszerkezet változásairól.

A FENNTARTHATÓSÁG ÉS A PÉNZÜGYI TUDATOSSÁG SZEMLÉLETÉNEK ÉS
GYAKORLATÁNAK TOVÁBBFEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a családi háztartás összetevőit, értelmezi a család
    gazdálkodását meghatározó és befolyásoló tényezőket;

2. felismeri a családi háztartás gazdasági-pénzügyi fenntarthatóságának
    és a környezettudatos életvitel kialakításának társadalmi
    jelentőségét;

3. értelmezi az állam gazdasági szerepvállalásának területeit;

4. felismeri a közteherviselés gazdasági, társadalmi és erkölcsi
    jelentőségét, a társadalmi felelősségvállalás fontosságát;

5. fogyasztási szokásaiban érvényesíti a tudatosság szempontjait is.

A TÁRSAS EGYÜTTMŰKÖDÉS ÉS A KOMMUNIKÁCIÓS KULTÚRA FEJLESZTÉSE, A
VÉLEMÉNYALKOTÁS TÁMOGATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a véleménynyilvánítás, érvelés, a párbeszéd és a vita
    társadalmi hasznosságát;

2. képes arra, hogy feladatai egy részét a társas tanulás keretében
    végezze el;

3. önállóan vagy társaival együttműködve javaslatokat fogalmaz meg,
    tervet, tervezetet készít;

4. önállóan vagy segítséggel használja az infokommunikációs eszközöket.

ÁTFOGÓ CÉLKÉNT KITŰZÖTT TANULÁSI EREDMÉNYEK A 9--12. ÉVFOLYAMON

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a család szerepét, alapvető feladatait az egyén és a nemzet
    szempontjából egyaránt;

2. értékeli a nemzeti identitás jelentőségét az egyén és a közösség
    szempontjából is;

3. ismeri a választások alapelveit és a törvényhozás folyamatát;

4. megismeri a demokratikus jogállam működésének alapvető
    sajátosságait;

5. érti és vallja a haza védelmének, a nemzetért történő tenni akarás
    fontosságát;

6. a mindennapi életének megszervezésében alkalmazza a jogi és
    gazdasági-pénzügyi ismereteit;

7. saját pénzügyeiben tudatos döntéseket hoz;

8. felismeri az életpálya-tervezés és a munkavállalás egyéni és
    társadalmi jelentőségét;

9. ismeri a munka világát érintő alapvető jogi szabályozást, a
    munkaerőpiac jellemzőit, tájékozódik a foglalkoztatás és a
    szakmaszerkezet változásairól.

AZ EGYÉNI ÉS A CSALÁDI SZOCIALIZÁCIÓ SEGÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi a család mint a társadalom alapvető intézményének szerepét
    és jellemzőit;

2. társaival megbeszéli a párválasztás, a családtervezés fontos
    szakaszait, szempontjait és a gyermekvállalás demográfiai
    jelentőségét: tájékozódás, minták, orientáló példák, átgondolt
    tervezés, felelősség;

3. felismeri, hogy a családtagok milyen szerepet töltenek be a
    szocializáció folyamatában;

4. értelmezi a családi szocializációnak az ember életútját befolyásoló
    jelentőségét.

AZ AKTÍV TÁRSADALMI CSELEKVÉS ÉS FELELŐS ÁLLAMPOLGÁRI MAGATARTÁS
MEGALAPOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az alapvető emberi jogok egyetemes és társadalmi
    jelentőségét;

2. bemutatja Magyarország Alaptörvényének legfontosabb részeit:
    Alapvetés; Az állam; Szabadság és felelősség;

3. érti a társadalmi normák és az egyéni cselekedetek, akaratok, célok
    egyeztetésének, összehangolásának követelményét. Elméleti és
    tapasztalati úton ismereteket szerez a társadalmi
    felelősségvállalásról, a segítségre szorulók támogatásának
    lehetőségeiről;

4. megérti a honvédelem szerepét az ország biztonságának
    fenntartásában, megismeri a haza védelmének legfontosabb
    feladatcsoportjait és területeit, az egyén kötelezettségeit;

5. felismeri és értelmezi az igazságosság, az esélyegyenlőség
    biztosításának jelentőségét és követelményeit;

6. értelmezi a választójog feltételeit és a választások alapelveit;

7. értelmezi a törvényalkotás folyamatát.

A LOKÁLPATRIOTIZMUS ÉS A NEMZETI IDENTITÁS ERŐSÍTÉSE, A HONVÉDELEM
SZEREPÉNEK, FELADATAINAK ÉS LEHETSÉGES MÓDOZATAINAK MEGISMERÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a nemzeti érzület sajátosságait és a hazafiság fontosságát,
    lehetséges megnyilvánulási formáit;

2. véleményt alkot a nemzetállamok és a globalizáció összefüggéseiről;

3. felismeri a világ magyarsága mint nemzeti közösség összetartozásának
    jelentőségét;

4. érti és felismeri a honvédelem mint nemzeti ügy jelentőségét;

5. felismeri és értékeli a helyi, regionális és országos
    közgyűjtemények nemzeti kulturális örökség megőrzésében betöltött
    szerepét.

FELKÉSZÜLÉS A FELNŐTT SZEREPEKRE

A nevelési-oktatási szakasz végére a tanuló:

1. azonosítja a mindennapi ügyintézés alapintézményeit;

2. életkori sajátosságainak megfelelően jártasságot szerez a jog
    területének mindennapi életben való alkalmazásában;

3. tájékozott a munkavállalással kapcsolatos szabályokban.

A FENNTARTHATÓSÁG ÉS A PÉNZÜGYI TUDATOSSÁG SZEMLÉLETÉNEK ÉS
GYAKORLATÁNAK TOVÁBBFEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megtervezi egy fiktív család költségvetését;

2. saját pénzügyi döntéseit körültekintően, megalapozottan hozza meg;

3. megismeri a megalapozott, körültekintő hitelfelvétel szempontjait,
    illetve feltételeit;

4. azonosítja az állam gazdasági szerepvállalásának elemeit;

5. felismeri és megérti a közteherviselés nemzetgazdasági, társadalmi
    és morális jelentőségét;

6. életvitelébe beépülnek a tudatos fogyasztás elemei, életmódjában
    figyelmet fordít a környezeti terhelés csökkentésére, érvényesíti a
    fogyasztóvédelmi szempontokat;

7. értelmezi a vállalkozás indítását befolyásoló tényezőket.

A TÁRSAS EGYÜTTMŰKÖDÉS ÉS A KOMMUNIKÁCIÓS KULTÚRA FEJLESZTÉSE, A
VÉLEMÉNYALKOTÁS TÁMOGATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a véleménynyilvánítás, érvelés, a párbeszéd és a vita
    társadalmi hasznosságát;

2. képes arra, hogy feladatait akár önálló, akár társas tanulás révén
    végezze el, célorientáltan képes az együttműködésre;

3. önállóan vagy társaival együttműködve javaslatokat fogalmaz meg;

4. tiszteletben tartja a másik ember értékvilágát, gondolatait és
    véleményét, ha szükséges, kritikusan viszonyul emberi
    cselekedetekhez, magatartásformákhoz;

5. megismerkedik a tudatos médiafogyasztói magatartással és a közösségi
    média használatával;

6. a tanulási tevékenységek szakaszaiban használja az infokommunikációs
    eszközöket, lehetőségeket, tisztában van azok szerepével, innovációs
    potenciáljával és veszélyeivel is.

***II.3.4.3. Hon- és népismeret***

***A) ALAPELVEK, CÉLOK***

A globalizációs folyamatok térhódítása mellett egyre nagyobb szükség van
arra, hogy a felnövekvő nemzedék stabil értékrenddel, erős gyökerekkel
rendelkezzen, ismerje és becsülje nemzeti értékeinket, alakuljon ki
benne a nemzethez tartozás tudata. A nemzeti és családi hagyományok,
értékek elhalványulása, a nevelés folyamatának háttérbe szorulása miatt
a hagyományos értékrend közvetítését, a szülőföldhöz kötődés
megerősítését, a nemzeti értékek megismertetését az iskolának is
hangsúlyosan fel kell vállalnia.

A tantárgy lehetőséget ad a tapasztalati úton történő tanulásra, az
ismeretek élménypedagógiai módszerekkel történő elsajátítására. Fontos
szerepet kap a saját gyűjtőmunka, a szűkebb és tágabb szülőföld
értékeinek, hagyományainak tanári támogatással történő felfedezése, a
megismert néphagyományok feldolgozása, megjelenítése egyéni és
csoportmunkában. Az önálló ismeretszerzésben szerepet kap a család és a
tágabb rokoni, szomszédi környezet. Az idősebb korosztály ismereteinek
befogadása erősíti a nemzedékek közötti párbeszédet, formálja a
nemzethez, a családhoz, valamint a szűkebb és tágabb közösséghez
tartozás tudatát. A különböző generációk ismereteinek, életmódjának
felfedezése fejleszti a több nézőpontú gondolkodást, a mérlegelés
képességét. A tantárgy a múltra támaszkodva erősíti a tanulóban a
családja és a nemzete jövőjéért érzett felelősségtudatot.

A tantárgy keretében a tanuló megismeri, megérti a hagyományos
gazdálkodó életmód szemléletét, amely a természettel való harmonikus
együttélésre alapul, valamint a városi polgári életforma hagyományait.
Mintát kap a közösségi életforma működésére, az egymásnak nyújtott
kölcsönös segítség megvalósítható formáira. Részese lehet a játékok
segítségével megvalósuló nevelési folyamatnak, amely életkori
sajátosságainak megfelelően nyújt mintát a hagyományos, családon belüli
szerepkörök, feladatkörök elsajátítására.

A nevelési-oktatási szakaszok közötti átmenet rugalmasabbá tételét
biztosítja az alsó tagozat módszertanának átvezető alkalmazása, az
önálló tanulás magasabb szintjére történő fokozatos áttérés. A hon- és
népismeret körébe tartozó ismeretek az alsó tagozat legtöbb tantárgyában
megjelennek (magyar nyelv és irodalom, természetismeret, ének-zene,
vizuális kultúra, testnevelés), ezért a célkitűzés itt az előzetes tudás
rendszerezése, az új ismeretek rendszerbe illesztése, az összefüggések
megláttatása, amelyek megvalósítását az alsó tagozat tudáselemeinek
átfogó ismerete és a modern tanítói módszertan képes támogatni.

A hon- és népismeret a hagyományos népi élet társadalmi jelenségeinek
személyes megközelítésével, a magyar népszokások megismerésével
megalapozza a jelenkori társadalom felépítésének, működésének
megértését, összehasonlítási alapot biztosít, és lehetőséget teremt a
változások felfedezésére, az ok-okozati összefüggések feltárására,
ezáltal a tanuló elkötelezetté válik a közösségért végzett munkára és a
felelősségvállalásra.

A hon- és népismeret tanulásának célja, hogy a tanuló:

1. felfedezze, hogy a nemzedékeken át létrehozott közösségi tradíció
    közel hozza a múltat, és segít eligazodni a jelenben, egyben irányt
    mutat a jövő számára is;

2. megértse, hogy a néphagyomány az általános emberi értékek hordozója,
    ezért ismerete az általános műveltség része;

3. megalapozza saját nemzeti önismeretét, megértse a nemzeti identitás
    jelentőségét az egyén és a közösség szempontjából egyaránt;

4. indíttatást kapjon a szűkebb és tágabb szülőföld, a magyar
    nyelvterület hagyományainak és történelmi emlékeinek felfedezésére;

5. megismerje szűkebb környezete nemzeti értékeit, mintát kapjon a
    lokálpatriotizmus élményszerű kifejezésére;

6. nyitottá váljon a velünk élő nemzetiségek, a szomszéd és rokon
    népek, a világ többi népének értékei iránt;

7. mintát kapjon a hagyományos férfi és női szerepekre való
    felkészüléshez, hogy a klasszikus családmodell alapjait el tudja
    sajátítani;

8. megismerje az önellátó és a városi életmód jellemzőit,
    szokáselemeit;

9. elsajátítson népi játékokat, népdalokat, köszöntő és színjátékszerű
    népszokásokat;

10. megismerje a magyarság és a Magyarországon élő nemzetiségek
    népművészetét, népszokásait.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

A hon- és népismeret tartalmazza a népünk kulturális örökségére
leginkább jellemző sajátosságokat, nemzeti kultúránk nagy múltú elemeit,
a magyar néphagyományt. Teret biztosít azoknak az élményszerű egyéni és
közösségi tevékenységeknek, amelyek a család, az otthon, a lakóhely, a
szülőföld, a haza és népei megbecsüléséhez, a velük való azonosuláshoz
vezetnek. Segíti az egyéni, családi, közösségi, nemzeti azonosságtudat
és a hazaszeretet kialakítását. Megalapozza és áthatja a különböző
tanítási területeket. Rendszerezett ismeretanyagként pedig lehetőséget
teremt a magyar népi kultúra értékein keresztül a saját és a különböző
népek kultúráinak megismerésére, a környezet értékeit megbecsülő és védő
magatartás, illetve a szociális érzékenység kialakítására.

A tanuló felfedezi, hogy a nemzedékek során át létrehozott közösségi
hagyomány összeköti őt a múlttal, segít neki eligazodni a jelenben, és
irányt mutat a jövőre nézve. Felismeri, hogy az emberiség évezredek óta
felhalmozódott tapasztalatai a legegyszerűbb, és éppen ezért a
legfontosabb mindennapi kérdésekre adott gyakorlati válaszok tárházát
alkotják. Megérti, hogy a néphagyomány az általános emberi értékek
hordozója is, ezért ismerete az általános műveltséghez tartozik.

A tantárgy megalapozza a tanuló nemzeti önismeretét, nemzettudatát, a
tevékeny hazaszeretetet. Tudatosítja a tanulóban, hogy először minden
népnek a saját hagyományát, nemzeti értékeit kell megismernie, hogy
azután másokét -- a nemzetiségek, a szomszéd- és rokon népek, a világ
többi népének kultúráját --, az egyetemes értékeket, a köztük lévő
kölcsönhatást is megérthesse. Ösztönöz a szűkebb és tágabb szülőföld, a
magyar nyelvterület és a Kárpát-medence hagyományainak és történelmi
emlékeinek felfedezésére, a még emlékezetből felidézhető vagy élő
néphagyományok gyűjtésére. Bővíti a tanuló művelődéstörténeti
ismereteit, ösztönzi a hagyományőrzést, népi kultúránk, nemzeti
értékeink megbecsülését. Értékrendjével hozzájárul a tanuló értelmi,
érzelmi, etikai és esztétikai neveléséhez, a természettel való
harmonikus kapcsolata kialakításához és a társadalomba való
beilleszkedéséhez.

A tanítás során -- pedagógiai és néprajzi szempontok szerint
kiválasztott hon- és népismereti, néprajzi forrásanyagok
felhasználásával -- minél több lehetőséget kell teremteni a
néphagyományok élményszerű megismerésére. Törekedni kell a tanuló
cselekvő és alkotó részvételére a tanulás során, hogy tapasztalati úton,
személyes élményeken keresztül jusson el az elméleti ismeretekig, az
összefüggések meglátásáig. Az ismeretek megszerzésében fontos szerepet
kap a tanulók kooperatív csoportokban történő együttműködése is. A
pedagógusnak az oktatási folyamat során törekednie kell az önszabályozó
tanulás kialakítására, a tanulási folyamat koordinálására.
Ismeretközvetítő tevékenysége mellett a hangsúly a segítő-támogató
szerepkörre kerül.

A tanulási területen belül a távoli ismeretek közötti összefüggések
felkutatását elősegítő feladatok, az aktív tanulási formák és a
változatos tanulásszervezési módok alkalmazása elősegíti az
alkalmazásképes tudás megszerzését, és segíti a motivált tanulás
fenntartását.

A tanulócsoportok részére ajánlott, választható projektek lehetővé
teszik egy-egy témakör komplex megismerését, élményszerű feldolgozását,
az ismeretek többféle kontextusban való vizsgálatát.

A tanév során legalább egy alkalommal ajánlott ellátogatni a helyi vagy
a közelben lévő tájházba, szabadtéri néprajzi múzeumba, hogy a tanuló
autentikus környezetben találkozzon a tárgyi kultúrával, és külső
szakértői tudás igénybevételével részt vehessen múzeumpedagógiai
foglalkozáson.

A hon- és népismeret tantárgy bemutatja a régebbi korok városi és falusi
életét, középpontba helyezve a mindennapi tevékenységeket és az ünnepi
szokásokat, valamint ezek táji eltéréseit. Az ismeretkörnek a lokális
értékek megismerése ad keretet.

A hagyományos életmód értelmezését, a mai kor életmódjával való
összevetését segíti a tartalmi és értelmező kulcsfogalmak használata.

Az értelmező kulcsfogalmak használata segíti a tanulót az összefüggések
feltárásában és megértésében, a tartalmi kulcsfogalmak tudatos
használata pedig segíti a hon- és népismeret ismereteinek
rendszerezését, felidézését és alkalmazását.

*Értelmező kulcsfogalmak:* identitás, nevelés, normarendszer, változás,
ok-okozati összefüggés.

*Tartalmi kulcsfogalmak:* család; lakóhely, néprajzi táj, közösség,
haza, nemzet, nemzetiség, életmód, népszokás, hagyomány.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Az én világom: Családunk története; Szomszédság, rokonság; Az én
    városom, falum; A hazai táj. Helytörténet, helyi hagyományok,
    nevezetességek; Gyermek- és diákélet a múltban.

2. Találkozás a múlttal: Nagyszüleink, dédszüleink világa falun és
    városban; A paraszti ház és háztartás. Népi mesterségek; A
    hétköznapok rendje (életvitel, étkezési szokások,
    ruházat-népviselet); Jeles napok, hagyományos népi és vallási ünnepi
    szokások; Hitélet és közösségi élet falun és városban; Az emberi
    élet meghatározó állomásai; Szórakozás a múltban falun és városban.

3. Örökségünk, hagyományaink, nagyjaink: Az ősi magyar kultúra
    hagyatéka; Magyarok a Kárpát- medencében és Moldvában; Néprajzi
    tájak, tájegységek és etnikai csoportok a Kárpát-medencében. A
    szomszédos országok; A hazánkban élő nemzetiségek kultúrája és
    hagyományai (pl.: roma/cigány népismeret elemei); Természeti és
    épített örökségünk; Hungarikumok; A magyar tudomány és kultúra
    eredményei és hatásai a világban.

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. képessé válik a nemzedékek közötti párbeszédre;

2. megismeri Magyarország és a Kárpát-medence kulturális hagyományait,
    tiszteletben tartja más kultúrák értékeit;

3. ismeri és adekvát módon használja az ismeretkör tartalmi
    kulcsfogalmait;

4. az önálló ismeretszerzés során alkalmazni tudja az interjúkészítés,
    a forráselemzés módszerét;

5. képes az együttműködésre csoportmunkában, alkalmazza a tevékenységek
    lezárásakor az önértékelést és a társak értékelését.

A LOKÁLPATRIOTIZMUS ÉS A NEMZETI IDENTITÁS ERŐSÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megismeri a közvetlen környezetében található helyi értékeket,
    felhasználva a digitálisan elérhető adatbázisokat is;

2. megbecsüli szűkebb lakókörnyezetének épített örökségét, természeti
    értékeit, helyi hagyományait;

3. megéli a közösséghez tartozást, nemzeti önazonosságát, kialakul
    benne a haza iránti szeretet és elköteleződés.

A CSALÁD, A CSALÁDTAGOK, A KÖZÖSSÉG EMBERI ÉRTÉKEI IRÁNTI ÉRZÉKENYSÉG
FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megtapasztalja a legszűkebb közösséghez, a családhoz, a lokális
    közösséghez tartozás érzését;

2. nyitottá válik a hagyományos családi és közösségi értékek
    befogadására;

3. felismeri a néphagyományok közösségformáló, közösségmegtartó erejét;

4. tiszteletben tartja más kultúrák értékeit.

A NEMZEDÉKEK KÖZÖTTI TÁVOLSÁG CSÖKKENTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. érdeklődő attitűdjével erősíti a nemzedékek közötti párbeszédet;

2. megbecsüli és megismeri az idősebb családtagok tudását,
    tapasztalatait, nyitott a korábbi nemzedékek életmódjának,
    normarendszerének megismerésére.

A KÖRNYEZETTUDATOS SZEMLÉLET FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a természeti környezet meghatározó szerepét a más tájakon
    élő emberek életmódbeli különbségében;

2. belátja, hogy a természet kínálta lehetőségek felhasználásának
    elsődleges szempontja a szükségletek kielégítése, a mértéktartás
    alapelvének követése;

3. megismeri a gazdálkodó életmódra jellemző újrahasznosítás elvét, és
    saját életében is megpróbálja alkalmazni.

AZ ÖNÁLLÓ ISMERETSZERZÉSBEN VALÓ JÁRTASSÁG NÖVELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. önálló néprajzi gyűjtés során, digitális archívumok, írásos
    dokumentumok tanulmányozásával ismereteket szerez családja,
    lakóhelye múltjáról, hagyományairól;

2. szöveges és képi források, digitalizált archívumok elemzése,
    feldolgozása alapján bővíti tudását.

A KOMMUNIKÁCIÓS KÉSZSÉG, A TÁRSAS EGYÜTTMŰKÖDÉS FEJLESZTÉSE, A
VÉLEMÉNYALKOTÁS GYAKORLATÁNAK ERŐSÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a csoportban elfoglalt helyét és szerepét, törekszik a
    személyiségének, készségeinek és képességeinek, érdeklődési körének
    legjobban megfelelő feladatok vállalására;

2. tevékenyen részt vesz a csoportmunkában zajló együttműködő alkotási
    folyamatban, digitális források közös elemzésében;

3. meghatározott helyzetekben önálló véleményt tud alkotni, a társak
    véleményének meghallgatását követően álláspontját felül tudja
    bírálni, döntéseit át tudja értékelni.

*II.3.5. Etika / hit- és erkölcstan*

***A) ALAPELVEK, CÉLOK***

A tantárgy középpontjában az erkölcsi nevelés áll, amely a tanuló
erkölcsi érzékének és erkölcsi gondolkodásának fejlesztését jelenti. A
tanulás folyamán az erkölcsi kategóriák jelentéstartalmának folyamatos
gazdagítása, szükség esetén újraértelmezése, élethelyzetekre
vonatkoztatása, valamint az ezekből következő etikai kérdések felvetése
történik. A tananyag alapvető értékeket közvetít: a segítés, megértés,
együttérzés, törődés, szabadság, felelősség, igazságosság,
becsületesség, méltányosság, tolerancia, önazonosság. Ezek a tanuló
lelkiismeretének fejlődését szolgálják. A témák feldolgozása a tanulót
megfontolt döntésre, kulturált véleményalkotásra és felelős
tevékenységre készteti.

A tanulóközösség tevékenységei mintát nyújtanak arra, hogy milyen
érzelmi-, érdekkonfliktusok és viselkedésmódok segítik vagy akadályozzák
az együttműködést. Az ajánlott tantárgyi tartalmak és tanulói
tevékenységek olyan képességeket is fejlesztenek, melyek a tanulót az
életvezetésében hatékonnyá és tudatosabbá, társai és környezete
problémái iránt érzékenyebbé teszik, erősítik identitását, aktív
társadalmi cselekvésre késztetik és segítik a nehéz helyzetek
megoldásában.

A tantárgy -- jellegénél fogva -- elsősorban a személyes és társas
képességeket, illetve a tudatos társadalmi részvételt és
felelősségvállalást fejleszti. Fő feladata olyan ismeretek és értékek
közvetítése, valamint kompetenciák elsajátíttatása, amelyek hozzásegítik
a tanulót, hogy tájékozott, aktív és elkötelezett állampolgárrá, kisebb
és nagyobb közösségeinek felelős tagjává váljék.

Az etika tanulása során a tanuló megismerkedik a magyar, az európai és a
világtörténelem etikailag is fontos eseményeivel, jelenségeivel,
folyamataival és szereplőivel. Ez jelentős mértékben elősegíti, hogy a
tanuló megismerje és elsajátítsa azt a kulturális kódrendszert, amely
lehetővé teszi számára identitása, valamint a magyar nemzet és a
keresztény, keresztyén normarendszeren alapuló európai civilizáció
iránti elkötelezettsége kialakítását és megerősítését.

A tantárgy tanulása folyamán a tanuló számos olyan történettel,
konfliktussal, dilemmával, emberi magatartással és sorssal találkozik és
foglalkozik, amely nemcsak tájékozottsága, életismerete és gondolkodási
képessége kibontakozásához járul hozzá, hanem erkölcsi és érzelmi
fejlődését is szolgálja.

Teret biztosít azoknak az élményszerű egyéni és közösségi
tevékenységeknek, amelyek a család, az otthon, a lakóhely, a szülőföld
tiszteletét alapozzák meg. Segíti az egyéni, családi, közösségi,
nemzeti, nemzetiségi identitástudat és történeti tudat kialakítását.

Tudatosítja a tanulóban, hogy elsősorban nemzete saját hagyományainak,
értékeinek megismerése, elsajátítása és gyakorlása mellett válhat
nyitottá a velünk élő nemzetiségek, vallási közösségek, a szomszéd és a
rokon népek, valamint a világ többi népének kultúrája, az egyetemes
értékek iránt is.

Az etika tanterv főbb pedagógiai alapelvei:

1. A tanulók komplex személyiségfejlesztése, értelmi, érzelmi,
    társas-lelkületi formálás és a cselekvő magatartásra, viselkedésre
    buzdítás.

2. A teljes személyiség aktivizálása, a belső motiváció felkeltése és
    ébren tartása.

3. Célrendszere és ajánlásai élményt adók, személyiséget, meggyőződést
    formálók.

4. A tananyagok kiválasztása és annak megvalósítása során figyelembe
    veszi az egyes korosztályok tipikus életkori sajátosságait és
    lehetséges élethelyzeteit, valamint lehetőséget kíván adni a tanulók
    és tanulócsoportok egyéni sajátosságai szerinti differenciálásra.

5. Az aktív, cselekvő viselkedés, magatartás megélésére ösztönzi a
    tanulókat a különböző élethelyzeteikben.

6. Fontosnak tartja a nevelés három színterét: család, iskola,
    társadalom.

7. Az etika tantárgyban elsődleges az érzelmi, érzületi nevelés, a
    morál- és morális fejlesztés, amely során a gyermekek
    cselekedtetése, meggyőződésének formálása elengedhetetlenül
    szükséges a lelkiismeretes magatartás megszilárdulása érdekében.

8. A nevelés mindig egy társadalmi közegben történik, így a nemzeti
    értékeink megismerése és megőrzése alapfeladat.

Az etika természete szerint értelmező jellegű is, a tényekről alkotott
különböző vélemények szükségszerűen vitákat eredményeznek. A tanulás
során ezek a viták arra ösztönzik a tanulót, hogy elgondolkodjon az
emberi értékekről, illetve az élet alapvető dilemmáit megjelenítő olyan
fogalmakról, mint például az igazságosság, hűség, hatékonyság, empátia
és felelősség. A viták úgy szolgálhatják a tanulást, ha a tanulóban
megerősödnek társadalmunk és európai zsidó-keresztény, keresztyén
gyökerű civilizációnk alapértékei.

Ez a megközelítés hozzásegíti a tanulót, hogy megértse és méltányolja a
magyarság, a magyar nemzet, illetve Magyarország sajátos helyzetéből
adódó jelenségeket és folyamatokat, így alakulhat ki benne a tényeken
alapuló reális és pozitív nemzettudat.

A tanulási folyamat hatékonyságát elősegítik az egyéni és társas tanulás
változatos módszerei és formái, melyek az együttműködés közösségi
élményének a megélését eredményezik.

Az etika tantárgy alapvető célja annak felismertetése, hogy a kulturális
hagyományokban gyökerező etikai elvek, társas szabályok,
szocio-emocionális készségek miként járulnak hozzá az egyéni és
közösségi identitás formálódásához, stabilitásához, valamint az egyének
és csoportok közti együttműködés megteremtéséhez. A tanuló elsősorban az
európai vallásokkal való megismerkedésen keresztül, az elsajátított
információkra reflektálva tanulmányozza, hogy az egyes hitrendszerek
milyen értékek mentén kínálnak értelmezési keretet az emberi létezésre
vonatkozó gondolkodáshoz. A társadalmi együttélést szabályozó, egymástól
eltérő jogrendszerek egyes elemeinek értékközpontú vizsgálata elősegíti
a társadalmi csoportok és egyének érdekérvényesítésének megértését.

Az etika tantárgy tanulásának célja, hogy a tanuló:

1. olyan tartalmakat kapjon, amelyek segítik saját értékrendjének
    gazdagításában, és az alapvető értékek emberi életben betöltött
    szerepének megértésében;

2. tisztelettel tekintsen az életre, az emberi méltóságra;

3. az európai, a nemzeti hitvilág megismerése során tekintse át, hogy a
    vallás mit tart értékesnek az emberi viszonyulások tekintetében;

4. támogatást kapjon a kultúra elemeinek elsajátítása során;

5. legyen képes céltudatos, felelősségteljes, alaposan mérlegelt
    döntések meghozatalára;

6. törekedjen tudatos, cselekvő magatartásra, életvezetésre, közösségi
    felelősségvállalásra;

7. sajátítsa el a közös kulturális kód leglényegesebb elemeit:
    szimbólumok, történelmi személyek, történetek, fogalmak, alkotások;

8. fejlessze társas-lelkületi készségeit, öntudatos gondolkodását,
    jövőképét.

Célja továbbá, hogy a tanulóban:

9. kialakuljon a magyar nemzet és az emberiség története során
    megnyilvánuló kiemelkedő emberi teljesítmények iránti tisztelet, a
    történelmi tragédiák áldozatainak szenvedése iránti együttérzés és
    méltó megemlékezés igénye;

10. kialakuljon az európai civilizációs identitás is, amely az
    antikvitás, a zsidó-keresztény kultúra, valamint a polgári jogrend
    alapértékeire épül;

11. kialakuljon a demokratikus elkötelezettség, amely alapvető értéknek
    tartja a többségi döntéshozatalt, az emberi jogokat, valamint az
    állampolgári jogokat és kötelességeket;

12. kialakuljon a társadalmi felelősség, szolidaritás és normakövetés,
    amely alapvető értéknek tartja a közösség iránti elkötelezettséget,
    valamint az egyéni választási lehetőségeket;

13. kialakuljon az egyéni kezdeményezőkészség és felelősségvállalás,
    amely egyszerre tartja alapvető értéknek a szabadságot és a
    felelősséget, valamint a közösség számára a fenntarthatóság, az
    élhető élet biztosítását.

***A tantárgy tanításának specifikus jellemzői az 1--4. évfolyamon***

Az 1--4. évfolyamon a tanuló kognitív fejlődésére művelet előtti és
konkrét műveleti szakaszok egyaránt jellemzőek. Az erkölcsi normák és
szabályok keletkezésének magyarázatában, a tevékenységek motivációjában
és a transzcendens hit fejlődésében is nagy szerepet kap a tanuló által
elfogadott tekintélyszemély véleménye, viselkedési mintája; ugyanakkor a
jó-rossz cselekedetek elbírálásában már megjelennek a tettek -- egyénre
és környezetére vonatkoztatott következmények mérlegelése is. Az éntudat
és önismeret fejlődésében a pszichológiai én kibontakozásának folyamata
kezdődik el.

Az etika tantárgy célkitűzéseinek elérése érdekében fontos olyan
nevelési-oktatási módszerek kiválasztása, amelyek figyelembe veszik a
gyermekek fejlődési sajátosságait, és megfelelően reagálnak arra, hogy a
tanulócsoportokban minden esetben egyének közötti különbségekkel,
fejlettségi szintekkel kell számolni. A tanulók közötti különbségekre
történő megfelelő reagálás fontos feltétele, hogy a motivált tanulás
elérése érdekében nyitott társas tanulási környezet jöjjön létre,
amelyben minden tanuló úgy érzi, meghallgatják őt és értékesnek tekintik
aktivitását. A tanulás fizikai környezete a foglalkozások és munkaformák
jellegéhez rugalmasan alkalmazkodik. A pedagógus által vezetett,
érdeklődés alapján kialakított tanulói csoportokban a megbeszélések
funkcionális tevékenységekkel, mesékkel, történetekkel, csoportos
önismereti játékokkal, zenével, képzőművészettel és drámajátékkal
gazdagodnak. Az egyéni és a csoportos feladatok megtervezésénél és
kivitelezésénél digitális eszközöket is alkalmaznak.

Az alkalmazott pedagógiai módszerek olyan feltételeket teremtenek,
amelyek lehetővé teszik, hogy a tanulók rácsodálkozzanak a jelenségekre,
kérdezzenek, igazolják saját véleményüket és meghallgassák mások
véleményét. A nevelési-oktatási folyamatban alkalmazott többféle módszer
lehetővé teszi, hogy a tanulók a tanulás aktív szereplőiként fejlesszék
kompetenciáikat.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

Az 5--8. évfolyamon tanulók kognitív fejlődésére a fejlettségi szintek
heterogenitása következtében a konkrét műveleti és a formális műveleti
szakaszok sajátosságai egyaránt jellemzőek. A normák megállapításában,
cselekedetek elbírálásában, a büntetés megállapításának mértékében a
tekintély helyét átveszik a közös megegyezésen alapuló szabályok, az
erkölcsi ítéletekben megjelenik a cselekvő szándékának megítélése. Az
adott kortárscsoportnak és a tanuló által elfogadott tekintélyszemélynek
való megfelelés motiváló ereje mellett megjelenik az aktuális
csoportnormák megkérdőjelezése, az igazságosság hiányának felismerése
is.

Az éntudat és önismeret fejlődésének következtében kialakul a saját
érzelmek és gondolatok megfigyelése és ellenőrzése. A szükségletek
szabályozása tekintetében az 5--6. évfolyamokhoz tartozó, jellemzően
11--12 éves tanulók énfejlődését a kapcsolatok által szerzett
tapasztalatok uralják. A tanuló számos helyzetben változtatja társas
identitását, ami a saját szükségletek társas elvárásoknak való
alávetéséhez is vezethet. A hit fejlődésében még a csoport többségének
értékrendjéhez való igazodás dominál, de már megkezdődik az önálló
identitás kialakítása is.

Az érzelemszabályozás területén a feltétel nélküli elfogadás igénye a
családtól a kortárscsoport irányába mozdul el. Különbség jelenik meg a
fiúk és lányok között az érzelmek elrejtésének és kinyilvánításának
szabályozásában. A fiúk szocializációjában egyre jelentősebb szerepet
kap a félelem és a szomorúság elrejtésének igénye, mert ezeknek az
érzelmeknek a kinyilvánítása az erőtlenség kifejezéseként is
értelmezhető. Serdülőkorban a szociális készségek alapvető elemét képező
empátia fejlődésére jellemző, hogy határai jelentősen kitágulnak, mert a
serdülő fogékonnyá válik a társadalmi életben megnyilvánuló
igazságtalanságokra. A személyes kapcsolatokban a fiúk a lányokkal
szemben úgy szocializálódnak, hogy az érzelmeiket titkolják, és
elsősorban a kognitív empátia kinyilvánításával ápolják személyes
kapcsolataikat.

A tantárgy célkitűzéseinek elérése érdekében olyan nevelési-oktatási
módszerek kiválasztása ajánlott, amelyek figyelembe veszik a serdülők
fejlődési sajátosságait, és megfelelően reagálnak arra, hogy a
tanulócsoportokban minden esetben egyének közötti különbségekkel,
fejlettségi szintbéli heterogenitással kell számolni. A tanulók közötti
különbségekre történő megfelelő reagálás fontos feltétele, hogy a
motivált tanulás elérése érdekében olyan társas tanulási környezet
jöjjön létre, ahol minden tanuló úgy érzi, meghallgatják őt, és
értékesnek tekintik aktivitását. A pedagógus által vezetett,
érdeklődésen alapuló tanulói csoportokban a tréninggyakorlatokhoz
kapcsolódó megbeszélések funkcionális tevékenységekkel, történetekkel,
csoportos önismereti játékokkal, zenével, képzőművészettel és
drámajátékkal gazdagodnak. Az egyéni és a csoportos feladatok
megtervezésénél és kivitelezésénél folyamatosan biztosítani kell a
szükséges digitális eszközök alkalmazásának lehetőségét.

Az alkalmazott pedagógiai módszerek olyan feltételeket teremtenek,
amelyek lehetővé teszik, hogy a tanulók rácsodálkozzanak a jelenségekre,
kérdezzenek, igazolják saját véleményüket és meghallgassák mások
véleményét. A munkaformák között fontos szerepet tölt be a kooperatív
csoportmunka, az egyéni és csoportos projektfeladatok szervezése és
értékelése.

A tantárgy támogatja a tanulók pozitív érzületi fejlődését és tanulását,
a tanulóknak készségfejlesztési lehetőségeket és fogalmi eszközöket
biztosít ahhoz, hogy megvizsgálják, felépítsék identitásukat,
világszemléletüket. Minden tanulónak a fejlődést segítő pedagógiai
attitűddel megvalósított segítségnyújtásra, iránymutatásra van szüksége,
különösen a társas képességek és a gondolkodási készségek
fejlesztéséhez. Fontos, hogy differenciált bánásmód keretében a tanulók
egyéni támogatást nyerjenek a tanulási és tanítási módszerek
megválasztásában.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Éntudat -- Önismertet

2. Család -- Helyem a családban

3. Helyem az osztály közösségében

4. A társas együttélés kulturális gyökerei: Nemzet -- Helyem a
    társadalomban

5. A természet rendjének megőrzése a fenntarthatóság érdekében

6. Az európai kultúra hatása az egyén értékrendjére

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Éntudat -- Önismertet

2. Család -- Helyem a családban

3. Társas tudatosság és társas kapcsolatok -- Helyem a társas-lelkületi
    közösségekben

4. A társas együttélés kulturális gyökerei: Nemzet -- Helyem a
    társadalomban

5. A természet rendjének megőrzése a fenntarthatóság érdekében

6. Az európai kultúra emberképe, hatása az egyén értékrendjére

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. azonosítja saját helyzetét, erősségeit és fejlesztendő területeit,
    és ennek tudatában rövid távú célokat tartalmazó tervet tud
    kialakítani saját egyéni fejlődésére vonatkozóan;

2. felismeri a hatékony kommunikációt segítő és akadályozó verbális és
    nonverbális elemeket és magatartásformákat;

3. törekszik az érzelmek konstruktív és együttérző kifejezésmódjainak
    alkalmazására;

4. felismeri az egyes cselekvésekre vonatkozó erkölcsi szabályokat,
    viselkedése szervezésénél figyelembe veszi ezeket;

5. törekszik szabadságra, alkalmazkodásra, bizalomra, őszinteségre,
    tiszteletre és igazságosságra a különféle közösségekben;

6. felismeri saját lelkiismerete működését;

7. pozitív attitűddel viszonyul a nemzeti és a nemzetiségi hagyományok,
    a nemzeti ünnepek és az egyházi ünnepkörök iránt;

8. érdeklődést tanúsít a gyermeki jogok és kötelezettségek megismerése
    iránt a családban, az iskolában és az iskolán kívüli közösségekben;

9. megfogalmazza gondolatait az élet néhány fontos kérdéséről és a
    róluk tanított elképzelésekkel kapcsolatosan;

10. erkölcsi fogalomkészlete tudatosodik és gazdagodik, az erkölcsi
    értékfogalmak, a segítség, önzetlenség, tisztelet, szeretet,
    felelősség, igazságosság, méltányosság, lelkiismeretesség,
    mértékletesség jelentésével;

11. megismeri az élet tiszteletének és a felelősségvállalásának az
    elveit, hétköznapi szokásai alakításánál tekintettel van társas és
    fizikai környezetére.

ÖNISMERETI ÉS ÖNSZABÁLYOZÁSI KÉSZSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. a csoportos tevékenységek keretében felismeri és megjeleníti az
    alapérzelmeket, az alapérzelmeken kívül is felismeri és megnevezi a
    saját érzelmi állapotokat;

2. felismeri, milyen tevékenységeket, helyzeteket kedvel, illetve nem
    kedvel, azonosítja saját viselkedésének jellemző elemeit;

3. célokat tűz ki maga elé, és azonosítja a saját céljai eléréséhez
    szükséges főbb lépéseket;

4. meggyőződése, hogy a hiányosságok javíthatók, a gyengeségek
    fejleszthetők, és ehhez teljesíthető rövid távú célokat tűz maga elé
    saját tudásának és képességeinek fejlesztése céljából;

5. céljai megvalósítása során önkontrollt, siker esetén önjutalmazást
    gyakorol.

A NEHÉZ ÉLETHELYZETEKKEL TÖRTÉNŐ MEGKÜZDÉS KÉSZSÉGEI (REZILIENCIA)

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a testi-lelki egészség fogalmát és főbb szempontjait ,
    motivált a krízisek megelőzésében, és a megoldáskeresésben;

2. rendelkezik a stresszhelyzetben keletkezett negatív érzelmek
    kezeléséhez saját módszerekkel;

3. felismeri az őt ért bántalmazást, ismer néhány olyan segítő bizalmi
    személyt, akihez segítségért fordulhat;

4. megfogalmazza a nehéz élethelyzettel (pl.:új családtag érkezése vagy
    egy családtag eltávozása) kapcsolatos érzéseit;

5. felismeri annak fontosságát, hogy sorsfordító családi események
    kapcsán saját érzelmeit felismerje, megélje, feldolgozza, s azokat
    elfogadható módon kommunikálja a környezete felé.

TÁRSAS HELYZETEK ÉSZLELÉSE; INTERPERSZONÁLIS KÉSZSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az életkorának megfelelő beszélgetés alapvető szabályait;

2. mások helyzetébe tudja képzelni magát, és megérti a másik személy
    érzéseit;

3. különbséget tesz verbális és nem verbális jelzések között;

4. megkülönbözteti a felnőttekkel és társakkal folytatott társas
    helyzeteket;

5. megkülönbözteti a sértő és tiszteletteljes közlési módokat fizikai
    és digitális környezetben egyaránt, barátsággá alakuló kapcsolatokat
    kezdeményez;

6. saját érdekeit másokat nem bántó módon fejezi ki, az ehhez
    illeszkedő kifejezésmódokat ismeri és alkalmazza;

7. alkalmazza az asszertív viselkedés elemeit konfliktushelyzetben és
    másokkal kezdeményezett interakcióban, baráti kapcsolatokat tart
    fenn.

ÉRTÉKEKRE ÉS ERKÖLCSI ALAPELVEKRE ALAPOZOTT DÖNTÉSHOZATAL

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a „jó" és „rossz" közötti különbséget a közösen megbeszélt
    eseményekben és történetekben;

2. erkölcsi érzékenységgel reagál az igazmondást, a becsületességet, a
    személyes és szellemi tulajdont, valamint az emberi méltóság
    tiszteletben tartását érintő helyzetekre fizikai és digitális
    környezetben is.

AKTÍV SZEREPVÁLLALÁS A CSALÁDBAN ÉS A CSALÁDON KÍVÜLI KÖZÖSSÉGEK
ÉLETÉBEN

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a családi szokások jelentőségét és ezek természetes
    különbözőségét (alkalmazkodik az éjszakai pihenéshez, az étkezéshez,
    a testi higiénéhez fűződő, a tanulási és a játékidőt meghatározó
    családi szokásokhoz);

2. megérti az ünneplés jelentőségét, elkülöníti a családi, a nemzeti,
    az állami és egyéb ünnepeket, és az egyházi ünnepköröket, aktív
    résztvevője a közös ünnepek előkészületeinek;

3. szerepet vállal iskolai rendezvényeken, illetve azok
    előkészítésében, vagy iskolán belül szervezett szabadidős programban
    vesz részt;

4. képes azonosítani a szeretet és az elfogadás jelzéseit.

A NEMZETI ÉS EURÓPAI IDENTITÁST MEGHATÁROZÓ KULTURÁLIS ÉRTÉKEK
MEGISMERÉSE ÉS POZITÍV ÉRTÉKELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a közvetlen lakóhelyéhez kapcsolódó nevezetességeket ismeri, ezekről
    információkat gyűjt fizikai és digitális környezetben, társaival
    együtt meghatározott formában bemutatót készít;

2. érdeklődést mutat Magyarország történelmi emlékei iránt, ismer
    közülük néhányat;

3. az életkorához illeszkedő mélységben ismeri a nemzeti, az állami
    ünnepek, egyházi ünnepkörök jelentését, a hozzájuk kapcsolódó
    jelképeket;

4. a lakóhelyén élő nemzetiségek tagjai, hagyományai iránt nyitott,
    ezekről információkat gyűjt fizikai és digitális környezetben.

KÖZÖSSÉGI AKTIVITÁS TERVEZÉSE A JOGRENDSZEREK ISMERETÉRE TÁMASZKODÓ
GONDOLKODÁS ALAPJÁN

A nevelési-oktatási szakasz végére a tanuló:

1. tájékozott a testi és érzelmi biztonságra vonatkozó gyermeki
    jogokról;

2. tájékozott a képességek kibontakoztatását érintő gyermeki jogokról,
    ennek családi, iskolai és iskolán kívüli következményeiről, a
    gyermekek kötelességeiről;

3. irodalmi szemelvények alapján példákat azonosít igazságos és
    igazságtalan cselekedetekre, saját élmény alapján példát hoz ilyen
    helyzetekre, valamint részt vesz ezek megbeszélésében, tanítói
    vezetéssel;

4. magatartásával az igazságosság, a fair play elveinek betartására
    törekszik, és ezáltal igyekszik mások bizalmát elnyerni.

SZEMÉLYES VÉLEMÉNY KIALAKÍTÁSA A VILÁGVALLÁSOK EMBERKÉPÉRŐL ÉS ETIKAI
TANÍTÁSÁRÓL

A nevelési-oktatási szakasz végére a tanuló:

1. a bibliai szövegekre támaszkodó történetek megismerése alapján
    értelmezi, milyen vallási eseményhez kapcsolódik egy-egy adott
    ünnep;

2. más vallások ünnepei közül ismer néhányat;

3. azonosítja az olvasott vagy hallott bibliai tanításokban, mondákban,
    mesékben a megjelenő együttélési szabályokat;

4. a bibliai történetekben megnyilvánuló igazságos és megbocsátó
    magatartásra saját életéből példákat hoz, vagy megkezdett történetet
    a megadott szempont szerint fejez be.

TÁRSADALMI FELELŐSSÉGVÁLLALÁS ÉS ELKÖTELEZŐDÉS A FENNTARTHATÓ JÖVŐ IRÁNT

A nevelési-oktatási szakasz végére a tanuló:

1. ismer néhány kihalt vagy kihalófélben lévő élőlényt, tájékozott a
    jelenség magyarázatában, és ezekről információt gyűjt fizikai és
    digitális környezetben is;

2. a szabályok jelentőségét különböző kontextusokban azonosítja és
    társaival megvitatja, a szabályszegés lehetséges következményeit
    megfogalmazza;

3. játékvásárlási szokásaiban példát hoz olyan elemekre, amelyek révén
    figyelembe vehetők a környezetvédelmi szempontok, és felhívja társai
    figyelmét is ezekre;

4. a naponta használt csomagolóeszközök kiválasztásában megindokolja,
    hogy milyen elvek alkalmazása támogatja a környezetvédelmi
    szempontok érvényesülését, és ezekre társai figyelmét is felhívja.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. fogalmi rendszerében kialakul az erkölcsi jó és rossz, mint minősítő
    kategória, az erkölcsi dilemma és az erkölcsi szabályok fogalma,
    ezeket valós vagy fiktív példákhoz tudja kötni;

2. erkölcsi szempontok alapján vizsgálja személyes helyzetét; etikai
    alapelvek és az alapvető jogok szerint értékeli saját és mások
    élethelyzetét, valamint néhány társadalmi problémát;

3. környezetében felismeri azokat a személyeket, akiknek tevékenysége
    példaként szolgálhat mások számára; ismeri néhány kiemelkedő
    személyiség életművét, és elismeri hozzájárulását a tudomány, a
    technológia, a művészetek gyarapításához, nemzeti és európai
    történelmünk alakításához;

4. azonosítja saját szerepét a családi viszonyrendszerben, felismeri
    azokat az értékeket, amelyek a család összetartozását, harmonikus
    működését és az egyén egészséges fejlődését biztosítják;

5. felismeri saját és mások érzelmi állapotát, az adott érzelmi
    állapotot kezelő viselkedést tud választani; a helyzethez igazodó
    társas konfliktus-megoldási eljárás alkalmazására törekszik;

6. elfogadó attitűdöt tanúsít az eltérő társadalmi, kulturális helyzetű
    vagy különleges bánásmódot igénylő tanulók iránt;

7. véleményt formál a zsidó keresztény, keresztyén értékrenden alapuló
    vallások erkölcsi értékeiről, feltárja, hogy hogyan jelennek meg
    ezek az értékek az emberi viselkedésben a közösségi szabályokban;

8. a személyes életben is megvalósítható tevékenységeket végez, ami
    összhangban van a teremtett rend megőrzésével, a fenntartható
    jövővel;

9. strukturált önmegfigyelésre alapozva megismeri személyiségének egyes
    jellemzőit, saját érdeklődési körét, speciális pályaérdeklődését.

ÖNISMERETI ÉS ÖNSZABÁLYOZÁSI KÉSZSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. megismeri az identitás fogalmát és jellemzőit, azonosítja saját
    identitásának néhány elemét;

2. különbséget tesz a valóságos és a virtuális identitás között,
    megérti a virtuális identitás jellemzőit;

3. megfelelő döntéseket hoz arról, hogy az online térben milyen
    információkat oszthat meg önmagáról;

4. reflektív tanulási gyakorlatot alakít ki, önálló tanulási feladatot
    kezdeményez;

5. dramatikus eszközökkel megjelenített helyzetekben különböző érzelmi
    állapotok által vezérelt viselkedést eljátszik, és ennek a másik
    személyre tett hatását a csoporttal közösen megfogalmazza;

6. valósághűen elmondja, hogy a saját érzelmi állapotai milyen hatást
    gyakorolnak a társas kapcsolatai alakítására, a tanulási
    tevékenységére;

7. kérdőívek használatával felismeri pályaérdeklődését és
    továbbtanulási céljait.

A NEHÉZ ÉLETHELYZETEKKEL TÖRTÉNŐ MEGKÜZDÉS KÉSZSÉGEI

A nevelési-oktatási szakasz végére a tanuló:

1. ismer testi és mentális egészséget őrző tevékenységeket és felismeri
    a saját egészségét veszélyeztető hatásokat; megfogalmazza saját
    intim terének határait;

2. megismer olyan mintákat és lehetőségeket, amelyek segítségével a
    krízishelyzetek megoldhatók, és tudja, hogy adott esetben hová
    fordulhat segítségért;

3. azonosítja a valós és virtuális térben történő zaklatások fokozatait
    és módjait, van terve a zaklatások elkerülésére, kivédésére, tudja,
    hogy hová fordulhat segítségért.

TÁRSAS HELYZETEK ÉSZLELÉSE ÉS INTERPERSZONÁLIS KÉSZSÉGEK

A nevelési oktatási szakasz végére a tanuló:

1. képes a saját véleményétől eltérő véleményekhez tisztelettel
    viszonyulni, a saját álláspontja mellett érvelni, konszenzusra
    törekszik;

2. felismeri a konfliktus kialakulására utaló jelzéseket, vannak
    megoldási javaslatai a konfliktusok békés megoldására;

3. azonosítja a csoportban elfoglalt helyét és szerepét, törekszik a
    személyiségének legjobban megfelelő feladatok ellátására;

4. törekszik mások helyzetének megértésére, felismeri a mások érzelmi
    állapotára és igényeire utaló jelzéseket, a fizikai és a digitális
    környezetben egyaránt;

5. nyitott és segítőkész a nehéz helyzetben lévő személyek iránt;

6. értelmezi a szabadság és önkorlátozás, a tolerancia és a szeretet
    megjelenését és határait egyéni élethelyzeteiben.

ÉRTÉKEKRE ÉS ERKÖLCSI ALAPELVEKRE ALAPOZOTT DÖNTÉSHOZATAL

A nevelési oktatási szakasz végére a tanuló:

1. azonosítja a számára fontos közösségi értékeket, indokolja, hogy
    ezek milyen szerepet játszanak a saját életében;

2. azonosítja, értékeli az etikus és nem etikus cselekvések lehetséges
    következményeit;

3. a csoporthoz való csatlakozás vagy az onnan való kiválás esetén
    összeveti a csoportnormákat és saját értékrendjét, mérlegeli az őt
    érő előnyöket és hátrányokat.

AKTÍV SZEREPVÁLLALÁS A CSALÁDBAN ÉS A CSALÁDON KÍVÜLI KÖZÖSSÉGEK
ÉLETÉBEN

A nevelési oktatási szakasz végére a tanuló:

1. azonosítja és összehasonlítja a családban betöltött szerepeket és
    feladatokat;

2. érzékeli a családban előforduló, bizalmat érintő konfliktusos
    területeket;

3. felismeri saját családjának viszonyrendszerét, átéli a családot
    összetartó érzelmeket és társas lelkületi értékeket, a különböző
    generációk családot összetartó szerepét;

4. megfogalmazza, hogy a szeretetnek, hűségnek, elkötelezettségnek,
    bizalomnak, tiszteletnek milyen szerepe van a társas lelkületi
    kapcsolatokban.

A NEMZETI ÉS EURÓPAI IDENTITÁST MEGHATÁROZÓ KULTURÁLIS ÉRTÉKEK
MEGISMERÉSE ÉS POZITÍV ÉRTÉKELÉSE

A nevelési oktatási szakasz végére a tanuló:

1. fizikai vagy digitális környezetben információt gyűjt és megosztja
    tudását a sport, a technika vagy a művészetek területén a nemzet és
    Európa kultúráját meghatározó kiemelkedő személyiségekről és
    tevékenységükről;

2. ismeri a nemzeti identitást meghatározó kulturális értékeket,
    indokolja miért fontos ezek megőrzése;

3. azonosítja a nemzeti és az európai értékek közös jellemzőit, az
    európai kulturális szellemiség, értékrendszer meghatározó elemeit;

4. összefüggéseket gyűjt a keresztény, keresztyén vallás és az európai,
    nemzeti értékvilágról, a közös jelképekről, szimbólumokról, az
    egyházi ünnepkörökről.

KÖZÖSSÉGI AKTIVITÁS TERVEZÉSE A JOGRENDSZEREK ISMERETÉRE TÁMASZKODÓ
GONDOLKODÁS ALAPJÁN

A nevelési oktatási szakasz végére a tanuló:

1. ismeri a rá vonatkozó gyermekjogokat, az ezeket szabályozó főbb
    dokumentumokat, értelmezi kötelezettségeit, részt vesz a
    szabályalkotásban;

2. részt vesz a különleges bánásmódot igénylő tanulók megértését segítő
    feladatokban, programokban, kifejti saját véleményét;

3. értelmezi a norma- és szabályszegés következményeit, és etikai
    kérdéseket vet fel velük kapcsolatban;

4. azonosítja az egyéni, családi és a társadalmi boldogulás,
    érvényesülés feltételeit;

5. életkorának megfelelő szinten értelmezi a családi élet mindennapjait
    befolyásoló fontosabb jogszabályok nyújtotta lehetőségeket (például
    családi pótlék, családi adókedvezmény, gyermekvédelmi támogatás).

SZEMÉLYES VÉLEMÉNY KIALAKÍTÁSA A VALLÁS ETIKAI TANÍTÁSÁRÓL

A nevelési oktatási szakasz végére a tanuló:

1. feltárja, hogyan jelenik meg a hétköznapok során a vallás emberi
    életre vonatkozó tanítása;

2. értelmezi a szeretetnek, az élet tisztelete elvének a kultúrára
    gyakorolt hatását;

3. egyéni cselekvési lehetőségeket fogalmaz meg az erkölcsi értékek
    érvényesítésére;

4. saját életét meghatározó világnézeti meggyőződés birtokában a
    kölcsönös tolerancia elveit valósítja meg a tőle eltérő nézetű
    személyekkel való kapcsolata során.

TÁRSADALMI FELELŐSSÉGVÁLLALÁS ÉS ELKÖTELEZŐDÉS A FENNTARTHATÓ JÖVŐ IRÁNT

A nevelési oktatási szakasz végére a tanuló:

1. folyamatosan frissíti az emberi tevékenység környezetre gyakorolt
    hatásaival kapcsolatos ismereteit fizikai és digitális
    környezetében, mérlegelő szemlélettel vizsgálja a technikai fejlődés
    lehetőségeit;

2. megismeri és véleményezi a természeti erőforrások felhasználására, a
    környezetszennyeződés, a globális és társadalmi egyenlőtlenségek
    problémájára vonatkozó etikai felvetéseket;

3. értelmezi a teremtett rend, világ, a fenntarthatóság összefüggéseit,
    az emberiség ökológiai cselekvési lehetőségeit.

*II.3.6. Természettudomány és földrajz*

A tanulásterület az anyagi világ jelenségeit, az élővilágot és földrajzi
környezetet a természettudományok módszereivel vizsgálja, valamint
megjelenít ezzel összefüggő társadalomtudományi és gazdasági ismereteket
is. Megalapozza a természettudományos műveltség mindennapi életben
felhasználható képességeit és ismereteit, elősegíti a
természettudományos és műszaki életpályák választását. A tanulás-tanítás
tervezése során figyelembe kell venni a tanulónak a természetes gyermeki
kíváncsiságtól a tudományos megismerés képességének megszerzéséig
jellemző személyiségfejlődését. A tanulók aktív részvételére épülő
tanulási módszerekkel a tartalmi tudás és a képességek együttes
fejlesztését, a tudás közösségi interakciókban történő építését kell
lehetővé tenni. Ebben nagyobb teret kaphat a tanuló érdeklődése, aki a
vizsgált jelenségeket több tudományterület összefüggésében elemezheti,
felhasználva az információs és kommunikációs technológiák adta
lehetőségeket. A természettudományos műveltség az alapvető fogalmak és
elméletek mellett a tudomány természetéről szerzett tudást és a
mindennapi életben alkalmazható vizsgálati és probléma-megoldási
készségek fejlesztését is magában foglalja olyan területeken, mint az
egészségtudatosság, a természeti és épített környezet védelme, vagy a
tudatos technológiahasználat. A tanulásterület fontos részét képezik a
természettudomány alkalmazásának etikai kérdései, amelyek
kapcsolódhatnak az etika tantárgy releváns tartalmaihoz. A
természettudományos és műszaki életpályákra való felkészülés, az MTMI
kompetenciák fejlesztése részletesebb tartalmi tudást és mélyebb
megértést kíván, ezt a tanulók érdeklődése, igénye és képességei
szerinti emelt szintű képzésekkel és tematikus programokkal lehet
biztosítani.

A természettudományos és földrajzi tudáselemek az alapfokú
nevelési-oktatási szakaszban az 1--2. évfolyamon a magyar nyelv és
irodalom tanulási területének tudásbővítést és olvasásfejlődést támogató
olvasmányaiban kaphatnak helyet. Ezt követően a 3--4. évfolyamon az
alapkészségek fejlesztését középpontba állító környezetismeret tantárgy
jeleníti meg a tanulási területet. Az 5--6. évfolyamon a biológia, a
kémia és a fizika tartalmi moduljaiból egységes keretbe foglalt
természettudomány tantárgy a tartalmi tudás bővítése mellett a
természettudomány vizsgálati módszereinek megismertetését, a gondolkodás
és tudásalkalmazás készségeinek fejlesztését is célul tűzi ki. A 7--8.
évfolyamokon önállóan jelennek meg a fizika, kémia és a biológia
tantárgyak, de továbbra is fontos cél az összefüggések megismerése, a
tudásalkalmazáshoz szükséges kompetenciák megszerzése, ezért is
szükséges, hogy a témakörök zárásához lehetőleg integráló tanórák is
kapcsolódjanak. A tárgyi és személyi feltételek megléte esetén az
intézmény helyi tanterve alapján ebben a szakaszban is lehetőség van
egységes természettudományos tantárgy kialakítására.

A középfokú nevelési-oktatási szakaszban, a 9--12. évfolyamon a
gimnáziumokban fizika, kémia, biológia és földrajz szaktárgyakban
folytatódik a tanulásterület tanítása. A 10. évfolyam végéig kitűzött
tanulási eredmények elérése minden tanuló számára kötelező. A 11--12.
évfolyamon az intézmények a tanulók számára választható módon, a
szabadon felhasználható órakeretből biztosíthatják a természettudományi
tantárgyak emelt óraszámban történő tanulását. A természettudományos és
műszaki életpályákra való felkészítés mellett fontos, hogy az
intézmények ebben a képzési szakaszban is biztosítsanak választható
lehetőséget a természettudományos műveltség életkorhoz igazodó
fejlesztésére. A természettudományos ismeretek és kiemelten az MTMI
készségek fejlesztése érdekében a gimnáziumban a 11. évfolyamon azon
tanulóknak, akik nem tanulnak emelt óraszámban vagy fakultáción
természettudományos tantárgyat, egy jelenségek vizsgálatán alapuló,
komplex szemléletmóddal oktatott, a természettudományos műveltséget
bővítő integrált természettudomány tantárgyat vagy a fizika, kémia,
biológia, földrajz tantárgyak egyikét kell tanulniuk heti két óra
időkeretben.

Speciális esetben a gimnáziumi típusú középiskolák a 9--12. évfolyamon,
a helyi tantervükben meghatározott módon, integrált tantárgy
szervezésével is biztosíthatják a természettudományos szaktárgyak
részletes tanulási eredményeinek teljesítését. A szakgimnáziumok,
technikumok 9-10. évfolyamán és a szakképző iskolák 9. évfolyamán
egységes természettudomány tantárgy tanulására kerül sor. A
szakgimnáziumokban és a technikumokban ezt követően az adott
szakterülethez kapcsolódó diszciplináris tantárgy (biológia, kémia,
fizika vagy földrajz) oktatása folyik.

A földrajz tantárgy tanulása-tanítása a 7. évfolyamon a
természettudományos szaktantárgyakkal párhuzamosan kezdődik. A tantárgy
a természettudomány és társadalomtudomány jellemzőit egyszerre hordozza,
ezért a természettudományos tudástartalmakhoz való kapcsolódást a
kereszttartalmak illeszkedésével, a nem tantárgyspecifikus készségek
alkalmazásával, és a transzverzális kompetenciákra épülő
tudásalkalmazással kell megvalósítani (például gazdaság- és
társadalomföldrajzi ismeretek, hon- és népismeret). A megszerzendő
ismereteket, a kialakítandó készségeket és a középiskolai szintnek
megfelelő kompetenciákat a 10. évfolyam végéig minden tanuló számára
elérhető tanulási eredmények alapján kell meghatározni. A 11--12.
évfolyamon az alap, illetve emelt óraszámban folytatódó tanulás-tanítást
a szabadon választható órakereten belül kell kialakítani.

***II.3.6.1**. **KÖRNYEZETISMERET***

***A) ALAPELVEK, CÉLOK***

A környezetismeret tantárgy a természettudomány és földrajz tanulási
terület bevezető tantárgya. A tantárgy keretein belül végzett, aktív
tanulásra épülő tevékenységek olyan tapasztalatokat tesznek lehetővé a
tanulók számára, amelyek illeszkednek életkori sajátosságaikhoz,
kognitív fejlődésükhöz, és belső motivációjukra támaszkodnak. Ezek a
tevékenységek támogatást nyújtanak ahhoz, hogy a tanulók megismerjék
testi jellemzőiket, a szűkebb és tágabb környezetüket, megértsék a
fizikai tulajdonságaik mentén és a környezetben zajló változásokat,
ráébredjenek alapvető ok-okozati összefüggésekre. Az olyan
tevékenységek, mint a megfigyelés, leírás, összehasonlítás,
csoportosítás, mérés, kísérletezés, a tanulók kíváncsiságára
támaszkodnak, és megalapozzák a tanulás későbbi szakaszaiban megjelenő
tantárgyak tudásbázisának kialakítását. A tantárgy tanulása erősíti az
ember és a természet, az élő, az élettelen és a kultúra által
létrehozott környezet iránti érdeklődést, felkelti a tanulók
motivációját a természettudományok és a földrajz tárgykörébe tartozó
problémák egyre mélyebb megismerése iránt.

A környezetismeret tanításának legfontosabb célja, hogy a tanuló:

1. *elsajátítsa és gyakorolja* a természettudományos ismeretszerzés
    módszereit;

2. fejlessze a megfigyelő, leíró, azonosító, megkülönböztető
    képességét;

3. fejlessze a mérési technikáját és a kísérletezésekhez szükséges
    képességeit;

4. fejlessze problémamegoldó gondolkodását, kommunikációs és
    vitakészségét;

5. rendezze tapasztalatain keresztül szerzett ismereteit;

6. a mindennapi életben és a szaktárgyak eredményes tanulásában
    alkalmazásra képes tudásra tegyen szert;

7. tanulja meg szeretni, tisztelni és védeni a környezetét, ismerje a
    környezettudatos életmód szokásait;

8. ismerje az egészségtudatos életmód szokásait.

***A tantárgy tanításának specifikus jellemzői a 3--4. évfolyamon***

Az alsó tagozatos gyermek többnyire érdeklődéssel fordul az élő és
élettelen környezet felé. Erre az érdeklődésre alapozva kell biztosítani
számára a megismerés, felfedezés örömét, így formálható a természethez
való viszonya, arról való gondolkodása.

A környezetismeret tanításának legfontosabb célja a 3--4. évfolyamon
azon képességek, szokások fejlesztése, melyek a felsőbb évfolyamokon a
természettudományos tárgyak tanulásához szükségesek.

Az életkorból és a fejlesztési feladatokból következően biztosítani
kell, hogy a tanuló cselekvő tapasztalatszerzés útján elemi szinten
sajátítsa el a természettudományos ismeretszerzés alapvető módszereit,
ne csupán biológiai, földrajzi, kémiai, fizikai ismeretek átadása
történjen. A tanulási folyamat során az ismeretszerző módszerek
elsajátításán keresztül a megismerési képességek fejlesztése a fő cél,
az ismeretanyag pedig az ezek megtanulását, gyakorlását szolgáló eszköz.

A megfigyelés, leírás, összehasonlítás, csoportosítás, mérés,
kísérletezés módszereit gyakorolva fejlődik tehát a tanuló megfigyelő,
leíró, azonosító, megkülönböztető képessége, mérési technikája, valamint
kísérletezési képessége.

A megértéshez, a fogalomalkotás tapasztalati előkészítéséhez szükség van
a célzott és folyamatos megfigyelésre. A minőségi, azaz érzékelhető
tulajdonságok megismerése kapcsán fontos kiemelni, hogy a puszta
érzékelés nem azonos a megfigyeléssel. A megfigyelés során az érzékelt
jelenség lényeges jellemzőit kell kiemelni a lényegtelenek közül.

A tanuló a leírás alkalmazásával szóban, rajzban, írásban rögzíti
tapasztalatait, és ennek során jut el a megnevezéshez.

Az összehasonlítás vezet el a lényeges jegyek kiemelésén túl az
összefüggések meglátásához. Ha a tanuló felismeri az azonos és különböző
tulajdonságokat, képessé válik a megfigyelt jellemzők rendezésére, az
azonos fogalmi jegyek alapján történő csoportosításra.

Fontos a mérhető, úgynevezett mennyiségi tulajdonságok megismerése,
ehhez az alapvető mennyiségek mérésének megbízható szinten történő
elsajátítása, mert a mérés módszerét mindegyik természettudományos
tantárgy alkalmazza. Ehhez szükséges a mérések metodikájának,
szabályainak, a helyes eszközhasználat módjának megismerése, gyakorlása.

Egyszerű kísérletek végzésével kell előkészíteni a későbbi
természettudományos módszer megismerését. A kísérletezésnek ebben az
életkorban a kíváncsiságra építve kell megvalósulnia, a gyermeket
körülvevő mindennapi problémák körüljárására, érdekes jelenségek
megfigyeltetésére kell irányulnia. A módszer alkalmazása közben tovább
fejlődik minden megismerési képesség, alapozódnak a tanuló szokásai,
megismerkedik a kísérletezés során használt számos eszközzel.

Az ismeretszerző módszerek elsajátítása közben megindul a fogalmak
kialakításának folyamata, de ez nem zárul le a 4. évfolyam végén.
Ekkorra még nem alakulnak ki a kész fogalmak, csupán tapasztalati
előkészítésük zajlik.

A környezetismeret tanítása a 3. évfolyamon kezdődik, és a technika és
tervezés, a magyar nyelv és irodalom, a vizuális kultúra, a matematika
és az etika tantárgyak keretei között, az 1--2. évfolyamon megvalósult
fejlesztésekre és tevékenységekre épül.

Az összehasonlítás, csoportosítás, rendezés, mérés a matematikai
készségfejlesztést is segíti, illetve a leírás módszerével fejleszthetők
a kommunikációs képességek is. A környezetismeret-órán végzett
tevékenységek többsége a társak közötti kooperációt igényli.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 3--4. ÉVFOLYAMON***

1. Megismerési módszerek

2. Tájékozódás az időben

3. Tájékozódás a térben

4. Élő környezet

5. Anyagok és folyamatok

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 3--4.
ÉVFOLYAMON***

A nevelési oktatási szakasz végére a tanuló:

1. ismeretei bővítéséhez nyomtatott és digitális forrásokat használ;

2. megfigyelés, mérés és kísérletezés közben szerzett tapasztalatairól
    szóban, rajzban, írásban beszámol;

3. projektmunkában, csoportos tevékenységekben vesz részt;

4. felismeri a helyi természet- és környezetvédelmi problémákat;

5. szöveggel, táblázattal és jelekkel adott információkat értelmez.

MEGISMERÉSI MÓDSZEREK

MEGFIGYELÉS, ÖSSZEHASONLÍTÁS, CSOPORTOSÍTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. adott szempontok alapján algoritmus szerint élettelen anyagokon és
    élőlényeken megfigyeléseket végez;

2. felismeri az élőlényeken, élettelen dolgokon az érzékelhető
    tulajdonságokat;

3. összehasonlítja az élőlényeket és az élettelen anyagokat;

4. adott szempontok alapján képes élettelen anyagokat összehasonlítani,
    csoportosítani;

5. adott szempontok alapján képes élőlényeket összehasonlítani,
    csoportosítani;

6. megfigyeléseinek, összehasonlításainak és csoportosításainak
    tapasztalatait szóban, rajzban, írásban rögzíti, megfogalmazza;

7. figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat
    (például olvadás, forrás, fagyás, párolgás, lecsapódás, égés,
    ütközés);

8. a megfigyelésekhez, összehasonlításokhoz és csoportosításokhoz
    kapcsolódó ismereteit felidézi.

MÉRÉS

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az élettelen anyagokon és az élőlényeken a mérhető
    tulajdonságokat;

2. algoritmus szerint, előzetes viszonyítás, majd becslés után
    méréseket végez, becsült és mért eredményeit összehasonlítja;

3. a méréshez megválasztja az alkalmi vagy szabvány mérőeszközt,
    mértékegységet;

4. az adott alkalmi vagy szabvány mérőeszközt megfelelően használja;

5. a mérésekhez kapcsolódó ismereteit felidézi;

6. a méréseket és azok tapasztalatait a mindennapi életben alkalmazza.

KÍSÉRLETEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. tanítói segítséggel egyszerű kísérleteket végez;

2. a kísérletezés elemi lépéseit annak algoritmusa szerint
    megvalósítja;

3. a tanító által felvetett problémákkal kapcsolatosan hipotézist
    fogalmaz meg, a vizsgálatok eredményét összeveti hipotézisével;

4. az adott kísérlethez választott eszközöket megfelelően használja;

5. a kísérletek tapasztalatait a mindennapi életben alkalmazza;

6. figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat
    (például a természet változásai, időjárási elemek);

7. a vizsgálatok tapasztalatait megfogalmazza, rajzban, írásban
    rögzíti;

8. a feladatvégzés során társaival együttműködik.

TÁJÉKOZÓDÁS AZ IDŐBEN

A nevelési-oktatási szakasz végére a tanuló:

1. megfelelően eligazodik az időbeli relációkban, ismeri és használja
    az életkorának megfelelő időbeli relációs szókincset;

2. megfelelő sorrendben sorolja fel a napszakokat, a hét napjait, a
    hónapokat, az évszakokat, ismeri ezek időtartamát, relációit;

3. felismeri a napszakok, évszakok változásai, valamint a Föld mozgásai
    közötti összefüggéseket;

4. az évszakokra vonatkozó megfigyeléseket végez, tapasztalatait
    rögzíti, és az adatokból következtetéseket von le;

5. figyelemmel kísér rövidebb-hosszabb ideig tartó folyamatokat
    (például víz körforgása, emberi élet szakaszai, növények csírázása,
    növekedése);

6. naptárt használ, időintervallumokat számol, adott eseményeket
    időrend szerint sorba rendez;

7. napirendet tervez és használ;

8. analóg és digitális óráról leolvassa a pontos időt.

TÁJÉKOZÓDÁS A TÉRBEN

TÉRKÉPÉSZETI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és használja az életkorának megfelelő térbeli relációs
    szókincset;

2. megnevezi és iránytű segítségével megállapítja a fő- és
    mellékvilágtájakat;

3. irányokat ad meg viszonyítással;

4. megkülönböztet néhány térképfajtát: domborzati, közigazgatási,
    turista, autós;

5. felismeri és használja az alapvető térképjeleket: felszínformák,
    vizek, települések, útvonalak, államhatárok;

6. a tanterméről, otthona valamely helyiségéről egyszerű alaprajzot
    készít és leolvas;

7. tájékozódik az iskola környékéről és településéről készített
    térképvázlattal és térképpel, az iskola környezetéről egyszerű
    térképvázlatot készít;

8. felismeri a különböző domborzati formákat, felszíni vizeket, ismeri
    jellemzőiket, ezeket terepasztalon vagy saját készítésű modellen
    előállítja;

9. felismeri lakóhelyének jellegzetes felszínformáit;

10. domborzati térképen felismeri a felszínformák és vizek jelölését.

TOPOGRÁFIAI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. térkép segítségével megnevezi hazánk szomszédos országait, megyéit,
    saját megyéjét, megyeszékhelyét, környezetének nagyobb településeit,
    hazánk fővárosát, és ezeket megtalálja a térképen is;

2. térkép segítségével megnevezi Magyarország jellemző felszínformáit
    (síkság, hegy, hegység, domb, dombság), vizeit (patak, folyó, tó),
    ezeket terepasztalon vagy saját készítésű modellen előállítja;

3. térkép segítségével megmutatja hazánk nagytájait, felismeri azok
    jellemző felszínformáit.

ÉLŐ KÖRNYEZET

A NÖVÉNYEK ÉS AZ ÁLLATOK

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri, megnevezi és megfigyeli az életfeltételeket,
    életjelenségeket;

2. felismeri, megnevezi és megfigyeli egy konkrét növény választott
    részeit, algoritmus alapján a részek tulajdonságait, megfogalmazza,
    mi a növényi részek szerepe a növény életében;

3. növényt ültet és gondoz, megfigyeli a fejlődését, tapasztalatait
    rajzos formában rögzíti;

4. felismeri, megnevezi és megfigyeli egy konkrét állat választott
    részeit, algoritmus alapján a részek tulajdonságait, megfogalmazza,
    mi a megismert rész szerepe az állat életében.

ÉLETKÖZÖSSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. algoritmus alapján megfigyeli és összehasonlítja a saját
    lakókörnyezetében fellelhető, jellemző növények és állatok
    jellemzőit, a megfigyelt tulajdonságok alapján csoportokba rendezi
    azokat;

2. ismeri a lakóhelyéhez közeli életközösségek (erdő, mező-rét,
    víz-vízpart) főbb jellemzőit;

3. algoritmus alapján megfigyeli és összehasonlítja hazánk természetes
    és mesterséges élőhelyein, életközösségeiben élő növények és állatok
    jellemzőit, a megfigyelt jellemzőik alapján csoportokba rendezi
    azokat;

4. megnevezi a megismert életközösségekre jellemző élőlényeket,
    használja az életközösségekhez kapcsolódó kifejezéseket;

5. konkrét példán keresztül megfigyeli és felismeri az élőhely, életmód
    és testfelépítés kapcsolatát;

6. felismeri a lakóhelyéhez közeli életközösségek és az ott élő
    élőlények közötti különbségeket (pl. természetes -- mesterséges
    életközösség, erdő -- mező, rét -- víz, vízpart -- park, díszkert --
    zöldséges, gyümölcsöskert esetében);

7. megfigyeléseit mérésekkel (például időjárási elemek, testméret),
    modellezéssel, egyszerű kísérletek végzésével (például láb- és
    csőrtípusok) egészíti ki;

8. felismeri, hogy az egyes fajok környezeti igényei eltérőek;

9. felismeri az egyes életközösségek növényei és állatai közötti
    jellegzetes kapcsolatokat;

10. példákkal mutatja be az emberi tevékenység természeti környezetre
    gyakorolt hatását, felismeri a természetvédelem jelentőségét;

11. egyéni és közösségi környezetvédelmi cselekvési formákat ismer meg
    és gyakorol közvetlen környezetében.

AZ EMBER SZERVEZETE

A nevelési-oktatási szakasz végére a tanuló:

1. megnevezi az ember életszakaszait;

2. ismeri az emberi szervezet fő életfolyamatait;

3. megnevezi az emberi test részeit, fő szerveit, ismeri ezek
    működését, szerepét;

4. megnevezi az érzékszerveket és azok szerepét a megismerési
    folyamatokban;

5. belátja az érzékszervek védelmének fontosságát, és ismeri ezek
    eszközeit, módjait;

6. ismer betegségeket, felismeri a legjellemzőbb betegségtüneteket, a
    betegségek megelőzésének alapvető módjait.

AZ EMBER ÉS KÖRNYEZETE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az egészséges, gondozott környezet jellemzőit,
    megfogalmazza, milyen hatással van a környezet az egészségére;

2. tisztában van az egészséges életmód alapelveivel, összetevőivel, az
    emberi szervezet egészséges testi és lelki fejlődéséhez szükséges
    szokásokkal,azokat igyekszik betartani;

3. felismeri, mely anyagok szennyezhetik környezetünket a mindennapi
    életben, mely szokások vezetnek környezetünk károsításához, egyéni
    és közösségi környezetvédelmi cselekvési formákat ismer meg és
    gyakorol közvetlen környezetében (pl. madárbarát kert, iskolakert
    kiépítésében, fenntartásában való részvétel, iskolai környezet
    kialakításában, rendben tartásában való részvétel, települési
    természet- és környezetvédelmi tevékenységben való részvétel);

4. elsajátít olyan szokásokat és viselkedésformákat, amelyek a
    károsítások megelőzésére irányulnak (pl. hulladékminimalizálás --
    anyagtakarékosság, újrahasználat és -felhasználás, tömegközlekedés,
    gyalogos vagy kerékpáros közlekedés előnyben részesítése,
    energiatakarékosság);

5. felelősségtudattal rendelkezik a szűkebb, illetve tágabb környezete
    iránt.

ANYAGOK ÉS FOLYAMATOK

AZ ÉLŐLÉNYEK ÉS AZ ÉLETTELEN ANYAGOK TULAJDONSÁGAI

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az élőlényeken, élettelen anyagokon az érzékelhető és
    mérhető tulajdonságokat;

2. adott szempontok alapján élettelen anyagokat és élőlényeket
    összehasonlít, csoportosít;

3. azonosítja az anyagok halmazállapotait, megnevezi és összehasonlítja
    azok alapvető jellemzőit;

4. egyszerű kísérletek során megfigyeli a halmazállapot-változásokat:
    fagyás, olvadás, forrás, párolgás, lecsapódás;

5. megismeri és modellezi a víz természetben megtett útját, felismeri a
    folyamat ciklikus jellegét;

6. tanítói segítséggel égéssel kapcsolatos egyszerű kísérleteket végez,
    csoportosítja a megvizsgált éghető és éghetetlen anyagokat;

7. megfogalmazza a tűz és az égés szerepét az ember életében;

8. megnevezi az időjárás fő elemeit, időjárási megfigyeléseket tesz,
    méréseket végez;

9. megfigyeli a mozgások sokféleségét, csoportosítja a mozgásformákat:
    hely- és helyzetváltoztató mozgás;

10. megfigyeli a növények csírázásának és növekedésének feltételeit,
    ezekre vonatkozóan egyszerű kísérletet végez.

***II.3.6.2. TERMÉSZETTUDOMÁNY***

***A) ALAPELVEK, CÉLOK***

A természettudomány tantárgy alapvető szerepet játszik a tudományos és
technológiai műveltség kialakításában, a természettudományokkal való
ismerkedés korai szakaszában. Összekötő szerepet tölt be az alsó
tagozatos környezetismeret és a 7. osztálytól diszciplináris keretek
között oktatott természettudományos tárgyak (biológia, fizika, földrajz,
kémia) között. Ugyanakkor a tantárgynak van egy horizontális vetülete
is, hiszen a természettudományi tanulmányok sok esetben építenek a más
tantárgyak (főleg a magyar nyelv és irodalom, a matematika és a
történelem) keretében megszerzett tudásra, készségekre, kompetenciákra.

A fenti megállapításokból kiindulva a természettudomány tárgy négy olyan
alapdiszciplína (biológia, fizika, földrajz és kémia) köré szerveződik,
amelyek a természeti törvényszerűségek, rendszerek és folyamatok
megismerésével foglalkoznak. Ennek megfelelően a természettudomány tárgy
célja e komplex tudásanyag integrálása az egyes természeti rendszerek
közötti alapvető összefüggések megvilágítása révén.

A természettudomány tanulási-tanítási folyamatában alapvető szerepe van
a tanuló számára releváns problémák, életszerű helyzetek megismerésének,
amit a tantárgy a felvetett probléma integrált szemléletű tárgyalásával,
a tanulók aktív közreműködésével, egyszerű -- akár otthon is elvégezhető
-- kísérletek tervezésével, végrehajtásával, megfigyelésével és
elemzésével érhet el. Mindezeket nagyon fontos kiegészíteni terepi
tevékenységekkel is, amik nem csupán a természetben történő
vizsgálódásokat jelentik, hanem akár városi környezetben is
megvalósulhatnak. Az élményszerű, a tanuló gondolkodásához, problémáihoz
közel álló, gyakorlatorientált ún. kontextusalapú tananyag-feldolgozás
hatékonyabb, mert az ismeretek rendszerezésével zárul.

A természettudomány tananyaga tehát mindenkihez szól, nem csak azokhoz,
akik a későbbiekben komolyabban szeretnének természettudományokkal
foglalkozni. Szervesen kapcsolódik a hétköznapi élethez és erősen
gyakorlatorientált. Feltárja a természettudományok társadalmunkban és az
egyén életében betöltött szerepét. Nem tartalmaz sok ismeretet és
fogalmat, viszont annál több gyakorlati jellegű tevékenységet,
megfigyelést, tapasztalást épít be. Hagy időt az elmélyült
feldolgozásra, az esetleges megértési problémák megbeszélésére,
tekintettel van az információfeldolgozás memóriakapacitására, a kognitív
terhelésre. Figyel a megfelelő, már részben szakmai nyelvhasználatra és
kommunikációra. A tárgy célja inkább a fogalmi megértés, és nem az
információk szigorú megtanítása, valódi problémamegoldást kínál.
Előnyben részesíti az életszerű természettudományos problémák
csoportmunkában (projektmódszerrel, kutatásalapú tanítással) történő
feldolgozását. Megfelelően használja a kísérleteket, a terepi
foglalkozásokat, megfigyeléseket, melyeknek mindig világos a célja, és a
manuális készségek mellett a fogalmi megértést is fejlesztik.
Hangsúlyozza a kísérleti problémamegoldás lépéseit, különös tekintettel
a várható eredmény becslésére (hipotézisalkotásra). Az ellenőrzés során
döntően a megértést, a logikus gondolkodást méri.

Az 5--6. évfolyamon oktatott természettudomány tantárgy a helyi
tantervben szabályozott módon egy az 5--8. évfolyamon oktatott integrált
természettudomány tantárgy alapmoduljaként is tanítható.

A természettudomány tanításának legfontosabb célja, hogy a tanuló:

1. ráébredjen a természeti rendszerek és a természetben zajló
    folyamatok komplexitására, alapvető okaira és magyarázataira;

2. képessé váljon az önálló ismeretszerzésre, az összefüggések
    felismerésére és az egyszerű elemzések elvégzésére a tanulói
    kísérletek, terepi megfigyelések és vizsgálatok révén, azzal, hogy a
    távlati cél a felsőbb évfolyamokon való értő és önálló munkavégzés
    lehetőségének megalapozása;

3. elsajátítsa a természettudományok egységét szem előtt tartó
    szintetizáló gondolkodásmódot, legyen képes folyamatokat rendszerben
    szemlélni;

4. tudjon kritikusan gondolkodni az adott természeti, környezeti
    problémáról, illetve hogy felismerje az áltudományos információkat,
    amely nagyban hozzájárul a felelős és tudatos állampolgári
    szerepvállalás kialakításához;

5. hozzáférjen a mindennapi életben hasznosítható természettudományos
    tudáshoz, amelynek révén a mindennapi életükben előforduló
    egyszerűbb problémákat tudjon megoldani, és kialakuljon benne az
    értő, felelős döntéshozás képessége;

6. a természetben lejátszódó folyamatok vizsgálatával, a várható
    következmények megértésével cselekvőképes, a környezetért
    felelősséggel tenni akaró állampolgárrá váljon, ezzel is
    hangsúlyozva, hogy az ember egyénként és egy nagyobb közösség
    részeként egyaránt felelős természeti környezetéért, annak jövőbeni
    állapotáért;

7. felismerje és megértse, hogy az élhető jövő záloga a
    környezettudatos, fenntarthatóságot szem előtt tartó gondolkodás;

8. tudatos eszközhasználóvá váljon az infokommunikációs eszközök
    használata és a digitális kompetenciák fejlesztése révén;

9. segítséget kapjon a későbbi műszaki vagy természettudományos
    pályaválasztáshoz.

***A tantárgy tanításának specifikus jellemzői az 5--6. évfolyamon***

Az 5--6. osztályos gyerekek korcsoporti sajátosságaikból adódóan
többnyire kíváncsisággal, érdeklődéssel fordulnak az élő és élettelen
környezet, a természet felé. Erre az érdeklődésre alapozva kell
biztosítani számukra azoknak a készségeknek és képességeknek a
fejlesztését, amelyek alkalmassá teszik majd őket a felsőbb évfolyamokon
a magasabb szintű természettudományok világában történő eligazodásra. A
természettudomány tanításának legfontosabb célja tehát azoknak a
képességeknek, készségeknek, szokásoknak a fejlesztése, amelyek alsó
tagozatban a környezetismeret tantárggyal lettek megalapozva, és amelyek
a felsőbb évfolyamokon a természettudományos tárgyak tanulásához
szükségesek.

Az életkorból és a fejlesztési feladatokból következően biztosítani
kell, hogy a tanuló cselekvő tapasztalatszerzés útján már haladó szinten
és integrált módon sajátítsa el a természettudományos ismeretszerzés
módszereit és ne diszciplináris természettudományos tárgyakat tanuljon
egymás mellett az összefüggések nélkülözésével. A tanulási folyamat
során a későbbi diszciplináris tárgyakat megalapozó ismeretanyag
megtanulása mellett az ismeretszerző módszerek elsajátítása,
begyakorlása a fő cél.

A megfigyelés, leírás, összehasonlítás, csoportosítás, rendezés, mérés,
kísérletezés módszereit önállóan gyakorolva fejlődik a tanuló
megfigyelő, leíró, azonosító és megkülönböztető képessége, mérési
technikája, amelyet az alsó tagozattal ellentétben már tanári segítség
nélkül is képes megvalósítani.

A megértéshez, fogalomalkotáshoz szükség van a célzott és folyamatos
megfigyelésre, ami azonban nem csupán érzékelést jelent. A megfigyelés
annyival több, mint az érzékelés, hogy a megfigyelés során a
jelenségeket valamilyen szempontból elemezni kell, a lényegtelen
dolgokat ki kell emelni a lényegesek közül. A megfigyelt jelenségeket
ezután leírják valamilyen formában, ami ebben az életkorban nem csak
írás lehet, hanem gyakran rajz vagy más manuális vagy verbális
készségeket igénylő forma.

Az összehasonlítás vezet el a lényeges elemek kiemelésén túl az
összefüggések meglátásához. A tanuló a hasonló és a különböző
tulajdonságok felismerésével képessé válik a megfigyelt jellemzők
rendszerezésére, csoportosítására.

Az alapvető mennyiségek mérését a tanuló már alsó tagozatban
megbízhatóan elsajátította, 5--6. osztályban ennek elmélyítése és
begyakorlása, a mérendő mennyiségek körének kibővítése történik, hiszen
a mérés módszerét a későbbiekben minden természettudományos tárgy
alkalmazza.

A tanuló egyszerű kísérletek megtervezésével, kivitelezésével és a
következtetések levonásával készül fel a felsőbb évfolyamokon is
jellemző természettudományos kísérletezésekre.

Mindezen gyakorlati jellegű kompetenciák elsajátítása közben folytatódik
az alsó tagozatban megkezdett fogalomalkotás folyamata, amely 7.
osztálytól a diszciplináris tárgyak keretein belül mélyül el.

A természettudomány órán használt logikai feladatok jól kiegészítik a
matematikai készségfejlesztést, a tapasztalatok és következtetések
leírása vagy szóbeli kifejtése pedig fejleszti mind a szóbeli, mind az
írásbeli kommunikációs képességeket is. A természettudomány órán végzett
tevékenységek többsége csoportosan vagy páros munkában történik, ami a
kooperációs képességeket fejleszti.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 5--6. ÉVFOLYAMON***

1. Megfigyelési és mérési módszerek

2. Tájékozódás az időben

3. Tájékozódás a térben

4. Élő környezetünk

5. Anyagok és folyamatok

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--6.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeléseket, összehasonlításokat, csoportosításokat, méréseket
    és kísérleteket végez;

2. megfigyeléseiről, tapasztalatairól szóban, írásban, rajzban
    beszámol;

3. a szöveggel, táblázattal és jelekkel kapott információt önállóan
    értelmezi, azokból következtetéseket von le;

4. ismeretei bővítéséhez tanári útmutatás mellett kutatásokat végez a
    nyomtatott és digitális források felhasználásával;

5. kialakul benne a szűkebb, illetve tágabb környezet iránti
    felelősségtudat;

6. kialakul benne a természettudomány iránti érdeklődés.

AZ ÉLŐ ÉS ÉLETTELEN KÖRNYEZET VIZSGÁLATÁNAK ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és megfigyeli a környezetben előforduló élő és élettelen
    anyagokat, megadott vagy önállóan kitalált szempontok alapján
    csoportosítja azokat;

2. felismer és megfigyel különböző természetes és mesterséges
    anyagokat, ismeri azok tulajdonságait, felhasználhatóságukat, ismeri
    a természetes és mesterséges környezetre gyakorolt hatásukat;

3. önállóan végez becsléseket, méréseket és használ mérőeszközöket
    különféle fizikai paraméterek meghatározására;

4. önállóan végez egyszerű kísérleteket.

ANYAGOK ÉS TULAJDONSÁGAIK

A nevelési-oktatási szakasz végére a tanuló:

1. korábbi tapasztalatai és megfigyelései révén felismeri a víz
    különböző tulajdonságait, különböző szempontok alapján rendszerezi a
    vizek fajtáit;

2. bizonyítja, és hétköznapi példákkal alátámasztja a víz fagyásakor
    történő térfogat-növekedést;

3. megfigyeli a különböző halmazállapot-változásokhoz (olvadás, fagyás,
    párolgás, forrás, lecsapódás) kapcsolódó folyamatokat, példákat
    gyűjt hozzájuk a természetben, a háztartásban és az iparban;

4. kísérletek során megfigyeli a különböző halmazállapotú anyagok
    vízben oldódásának folyamatát;

5. felismeri az olvadás és az oldódás közötti különbséget kísérleti
    tapasztalatok alapján;

6. elsajátítja a tűzveszélyes anyagokkal való bánásmódot, ismeri tűz
    esetén a szükséges teendőket;

7. megfigyeli a talaj élő és élettelen alkotóelemeit, tulajdonságait,
    összehasonlít különböző típusú talajféleségeket, valamint
    következtetések révén felismeri a talajnak, mint rendszernek a
    komplexitását;

8. korábbi tapasztalatai és megfigyelései révén felismeri a levegő
    egyes tulajdonságait;

9. vizsgálat révén azonosítja a tipikus lágyszárú és fásszárú növények
    részeit;

10. megkülönbözteti a hely- és helyzetváltoztatást és példákat keres
    ezekre megadott szempontok alapján.

MÉRÉSEK, MÉRTÉKEGYSÉGEK, MÉRŐESZKÖZÖK

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan végez becsléseket, méréseket és használ mérőeszközöket a
    hőmérséklet, a hosszúság, a tömeg, az űrtartalom és az idő
    meghatározására;

2. észleli, méri az időjárási elemeket, a mért adatokat rögzíti,
    ábrázolja;

3. Magyarországra vonatkozó adatok alapján kiszámítja a napi
    középhőmérsékletet, a napi és évi közepes hőingást;

4. leolvassa és értékeli a Magyarországra vonatkozó éghajlati diagramok
    és éghajlati térképek adatait.

MEGFIGYELÉS, KÍSÉRLETEZÉS, TAPASZTALÁS

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeli a mágneses kölcsönhatásokat, kísérlettel igazolja a
    vonzás és a taszítás jelenségét, példákat ismer a mágnesesség
    gyakorlati életben való felhasználására;

2. megfigyeli a testek elektromos állapotát és a köztük lévő
    kölcsönhatásokat, ismeri ennek gyakorlati életben való megjelenését;

3. megfigyeléseken és kísérleten keresztül megismeri az
    energiatermelésben szerepet játszó anyagokat és az energiatermelés
    folyamatát;

4. kísérletekkel igazolja a növények életfeltételeit;

5. kísérleti úton megfigyeli az időjárás alapvető folyamatait,
    magyarázza ezek okait és következményeit.

TÁJÉKOZÓDÁS AZ IDŐBEN

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri az idő múlásával bekövetkező változásokat és ezek
    összefüggéseit az élő és élettelen környezet elemein;

2. tudja értelmezni az időt különböző dimenziójú skálákon;

3. tervet készít saját időbeosztására vonatkozóan;

4. megfigyeli a természet ciklikus változásait;

5. megérti a Föld mozgásai és a napi, évi időszámítás közötti
    összefüggéseket;

6. modellezi a Nap és a Föld helyzetét a különböző napszakokban és
    évszakokban.

A TÉRBELI TÁJÉKOZÓDÁS FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. meghatározza az irányt a valós térben;

2. érti a térkép és a valóság közötti viszonyt;

3. tájékozódik a térképen és a földgömbön.

ALAPVETŐ TÉRKÉPÉSZETI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. mágneses kölcsönhatásként értelmezi az iránytű működését;

2. felismeri a felszínformák ábrázolását a térképen;

3. megérti a méretarány és az ábrázolás részletessége közötti
    összefüggéseket;

4. fő- és mellékégtájak segítségével meghatározza különböző földrajzi
    objektumok egymáshoz viszonyított helyzetét;

5. felismeri és használja a térképi jelrendszert és a térképfajtákat
    (domborzati térkép, közigazgatási térkép, autós térkép, turista
    térkép).

TOPOGRÁFIAI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a földrészeket és az óceánokat a különböző méretarányú és
    ábrázolásmódú térképeken;

2. felismeri a nevezetes szélességi köröket a térképen;

3. megfogalmazza Európa és Magyarország tényleges és viszonylagos
    földrajzi fekvését;

4. ismeri a főfolyó, a mellékfolyó és a torkolat térképi ábrázolását;

5. felismeri és megnevezi a legjelentősebb hazai álló- és folyóvizeket;

6. bejelöli a térképen Budapestet és a saját lakóhelyéhez közeli
    fontosabb nagyvárosokat és a szomszédos országokat.

GYAKORLATI JELLEGŰ TÉRKÉPÉSZETI ISMERETEK (AZ ISKOLA KÖRNYÉKÉNEK
MEGISMERÉSE SORÁN, TEREPI MUNKÁBAN)

A nevelési-oktatási szakasz végére a tanuló:

1. a valóságban megismert területről egyszerű, jelrendszerrel ellátott
    útvonaltervet, térképet készít;

2. tájékozódik a terepen térképvázlat, iránytű és GPS segítségével;

3. meghatározott szempontok alapján útvonalat tervez a térképen;

4. használni tud néhány egyszerű térinformatikai alkalmazást.

AZ ÉLŐLÉNYEK FELÉPÍTÉSE ÉS AZ ÉLŐLÉNYTÁRSULÁSOK ALAPVETŐ FOLYAMATAI

A nevelési-oktatási szakasz végére a tanuló:

1. komplex rendszerként értelmezi az élő szervezeteket és az ezekből
    felépülő élőlénytársulásokat;

2. tisztában van az életfeltételek és a testfelépítés közti
    kapcsolattal;

3. tisztában van azzal, hogy az élő rendszerekbe történő beavatkozás
    káros hatásokkal járhat.

A NÖVÉNYEK TESTFELÉPÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és megnevezi a növények életfeltételeit, életjelenségeit;

2. összehasonlít ismert hazai termesztett vagy vadon élő növényeket
    adott szempontok (testfelépítés, életfeltételek, szaporodás)
    alapján;

3. felismeri és megnevezi a növények részeit, megfigyeli jellemzőiket,
    megfogalmazza ezek funkcióit;

4. összehasonlítja ismert hazai termesztett vagy vadon élő növények
    részeit megadott szempontok alapján;

5. ismert hazai termesztett vagy vadon élő növényeket különböző
    szempontok szerint csoportosít;

6. azonosítja a lágyszárú és a fás szárú növények testfelépítése
    közötti különbségeket.

AZ ÁLLATOK TESTFELÉPÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és megnevezi az állatok életfeltételeit és
    életjelenségeit;

2. összehasonlít ismert hazai házi vagy vadon élő állatokat adott
    szempontok (testfelépítés, életfeltételek, szaporodás) alapján;

3. felismeri és megnevezi az állatok testrészeit, megfigyeli
    jellemzőiket, megfogalmazza ezek funkcióit;

4. az állatokat különböző szempontok szerint csoportosítja;

5. azonosítja a gerinctelen és a gerinces állatok testfelépítése
    közötti különbségeket;

6. mikroszkóp segítségével megfigyel egysejtű élőlényeket.

ÉLŐLÉNYTÁRSULÁSOK ALAPVETŐ FOLYAMATAI

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeli hazánk erdei élőlénytársulásainak főbb jellemzőit;

2. életközösségként értelmezi az erdőt, mezőt, vizes élőhelyeket;

3. felismeri és magyarázza az élőhely -- életmód - testfelépítés
    összefüggéseit az erdők életközössége esetén;

4. példákkal bizonyítja, rendszerezi és következtetéseket von le az
    erdei élőlények környezethez történő alkalmazkodására vonatkozóan;

5. táplálékláncokat és azokból táplálékhálózatot állít össze a
    megismert erdei növény- és állatfajokból;

6. példákon keresztül bemutatja az erdőgazdálkodási tevékenységek
    életközösségre gyakorolt hatásait;

7. tisztában van az erdő természetvédelmi értékével, fontosnak tartja
    annak védelmét;

8. megfigyeli hazánk fátlan élőlénytársulásainak főbb jellemzőit;

9. megadott szempontok alapján összehasonlítja a rétek és a
    szántóföldek életközösségeit;

10. felismeri és magyarázza az élőhely - életmód - testfelépítés
    összefüggéseit a rétek életközössége esetén;

11. példákkal bizonyítja, rendszerezi és következtetéseket von le a
    mezei élőlények környezethez történő alkalmazkodására vonatkozóan;

12. táplálékláncokat és azokból táplálékhálózatot állít össze a
    megismert mezei növény- és állatfajokból;

13. példákon keresztül mutatja be a mezőgazdasági tevékenységek
    életközösségre gyakorolt hatásait;

14. tisztában van a fátlan társulások természetvédelmi értékével,
    fontosnak tartja annak védelmét;

15. megfigyeli hazánk vízi és vízparti élőlénytársulásainak főbb
    jellemzőit;

16. összehasonlítja a vízi és szárazföldi élőhelyek környezeti
    tényezőit;

17. felismeri és magyarázza az élőhely -- életmód - testfelépítés
    összefüggéseit a vízi és vízparti életközösségek esetén;

18. példákkal bizonyítja, rendszerezi és következtetéseket von le a vízi
    élőlények környezethez történő alkalmazkodására vonatkozóan;

19. táplálékláncokat és ezekből táplálékhálózatot állít össze a
    megismert vízi és vízparti növény- és állatfajokból;

20. példákon keresztül bemutatja a vízhasznosítás és a vízszennyezés
    életközösségre gyakorolt hatásait;

21. tisztában van a vízi társulások természetvédelmi értékével,
    fontosnak tartja annak védelmét.

AZ EMBERI SZERVEZET EGÉSZSÉGES MŰKÖDÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. érti, hogy a szervezet rendszerként működik;

2. tisztában van a testi és lelki egészség védelmének fontosságával;

3. tisztában van az egészséges környezet és az egészségmegőrzés közti
    összefüggéssel;

4. felismeri és megnevezi az emberi test fő részeit, szerveit;

5. látja az összefüggéseket az egyes szervek működése között;

6. érti a kamaszkori testi és lelki változások folyamatát, élettani
    hátterét;

7. tisztában van az egészséges életmód alapelveivel, azokat igyekszik
    betartani.

AZ ÉLETTELEN KÖRNYEZET ELEMEI, ALAPVETŐ FOLYAMATAI

A nevelési-oktatási szakasz végére a tanuló:

1. összetett rendszerként értelmezi az egyes földi szférák működését;

2. ismeri a természeti erőforrások energiatermelésben betöltött
    szerepét;

3. tisztában van a természeti erők szerepével a felszínalakításban.

AZ ENERGIA FOGALMA

A nevelési-oktatási szakasz végére a tanuló:

1. csoportosítja az energiahordozókat különböző szempontok alapján;

2. példákat hoz a megújuló és a nem megújuló energiaforrások
    felhasználására;

3. megismeri az energiatermelés hatását a természetes és a mesterséges
    környezetre.

A FÖLD BELSŐ ÉS KÜLSŐ ERŐI ÉS FOLYAMATAI

A nevelési-oktatási szakasz végére a tanuló:

1. megállapítja, összehasonlítja és csoportosítja néhány jellegzetes
    hazai kőzet egyszerűen vizsgálható tulajdonságait;

2. példákat hoz a kőzetek tulajdonságai és a felhasználásuk közötti
    összefüggésekre;

3. tisztában van azzal, hogy a talajpusztulás világméretű probléma;

4. ismer olyan módszereket, melyek a talajpusztulás ellen hatnak
    (tápanyag-visszapótlás, komposztkészítés, ökológiai kertművelés);

5. felismeri és összehasonlítja a gyűrődés, a vetődés, a földrengés és
    a vulkáni tevékenység hatásait;

6. magyarázza a felszín lejtése, a folyó vízhozama, munkavégző
    képessége és a felszínformálás közti összefüggéseket;

7. magyarázza az éghajlat és a folyók vízjárása közötti
    összefüggéseket;

8. megnevezi az éghajlat fő elemeit;

9. jellemezi és összehasonlítja az egyes éghajlati övezeteket (forró,
    mérsékelt, hideg);

10. értelmezi az évszakok változását;

11. értelmezi az időjárás-jelentést;

12. piktogramok alapján megfogalmazza a várható időjárást.

***II.3.6.3. BIOLÓGIA***

***A) ALAPELVEK, CÉLOK***

A nevelés-oktatás alapvető értékeinek közvetítése és az emberkép
formálása a tantárgy kiemelt feladata. Erre jó lehetőséget biztosít az a
tartalmi és készségfejlesztési kapcsolódás, ami a biológia és a
testi-lelki egészségre nevelés, az önismeret, emberismeret, valamint a
fenntarthatóság pedagógiája között fennáll. A korábbi évek tanulmányai
után a tanulók részletesebben és elmélyültebben foglalkozhatnak az
életjelenségek megértésével, az élőlények sokféleségével és az
életközösségek működésével. Az emberi szervezetről szerzett ismeretek
fejlesztik a tanulók testképét és önismeretét, hozzájárulnak az
egészséges életvitel értékének felismeréséhez, az ezt segítő attitűdök
és szokások kialakításához. Az életközösségek vizsgálata, folyamataik
azonosítása összekötheti a múlt adatait a jelenben észlelhető
változásokkal, amelyből előrejelzés tehető a várható jövővel
kapcsolatban is. A fenntarthatóságot a biológiai rendszerekre
vonatkoztatva vizsgálják a tanulók, összefüggéseket keresnek az emberi
tevékenységgel, a gazdaság és társadalom működésével is. A tartalmi
tudás építése kiegészül a természettudományos vizsgálati és gondolkodási
módszerek elsajátításával, amely a mindennapi életben felmerülő
problémák vizsgálatában is alkalmazható. A tudomány természetének és
működésének megértése és begyakorlása aktív tanulási módszerekkel
történik, amely a tanulók együttműködésére és kommunikációjára
alapozódik. Az interaktív tanulás megnyitja a probléma-azonosítástól a
vizsgálatokon át a magyarázat kereséséig vezető megismerési utat,
amelyen többféle nyomtatott és elektronikus információforrás
felhasználásával, megfelelő támogatással, de önállóan, az egyéni
érdeklődés és tanulási szükségletek figyelembevételével haladhatnak a
tanulók. A mindenki által megszerezhető és alkalmazható
természettudományos műveltség fejlesztése mellett fontos, hogy különféle
tanulói utak nyíljanak az életpálya-építés, a szakirányú továbbtanulás
felé is. Ezt szolgálhatja az MTMI alapismeretek beépítése a
biológiatanulás folyamatába. A problémamegoldás, kreativitás és
innovatív gondolkodás fejlesztésének feltétele a tanulók érdeklődésének
fenntartása, az önálló ismeretszerzés képességének erősítése. A tanulás
második, középiskolai szakaszában a tartalmi tudás elmélyítése és a
további képességfejlesztés alapot ad a biológia szakirányú továbbtanulás
felé vezető, fakultatív tanulás számára is. A biológia a 7--8.
évfolyamon a helyi tantervben szabályozott módon, integrált
természettudomány tantárgy részeként, annak biológia moduljaként is
tanítható. Speciális esetben az integrált tanulásszervezésre a
középiskolai szakaszban is lehetőség van.

A biológia tanulásának célja, hogy a tanuló:

1. ismerje a biológiai kutatások célját és módszereit:
    hipotézisalkotás, megfigyelés, kísérlettervezés és kivitelezés,
    adatelemzés és következtetés;

2. elemezzen megvalósult kutatásokat, kísérlet leírásokat, végezzen el
    egyszerűbb biológiai kísérleteket;

3. biológiai problémák vizsgálata során alkalmazza a következtetés, a
    korrelatív, analógiás, statisztikus és rendszerszintű gondolkodás
    műveleteit, fejlessze és alkalmazza mérlegelő gondolkodását,
    kreativitását;

4. konkrét példák alapján ismerje meg az élettudományok különböző
    alkalmazási lehetőségeit, alkosson tényekre alapozott véleményt ezek
    társadalmi, etikai, gazdasági, technológiai és környezeti
    következményeiről;

5. **tudjon biológiai vonatkozású információkat keresni,** azok
    hitelességének, használhatóságának elemzéséhez széleskörűen
    tájékozódjon könyvtári és egyéb adatbázisok, nyomtatott és digitális
    források segítségével;

6. **ismerje fel az élőlények sokféleségét, lássa, hogy közös
leszármazás alapján hogyan sorolhatók csoportokba;**

7. **ismerje, és konkrét biológiai problémák magyarázatában alkalmazza
az evolúció elméletét;**

8. **érdeklődjön az élő természet megismerése iránt, szerezzen
    személyes élményeket a természetben, igyekezzen ezeket alkotó módon
    kifejezni;**

9. **vizsgáljon a környezetében előforduló életközösségeket,
    tapasztalatait használja fel a felépítésük és működésük megértésére,
    fogalmazzon meg javaslatot a védelmükre;**

10. **legyen tájékozott a természetvédelem fontosságával, módszereivel
    és törvényi szabályozásával kapcsolatban;**

11. **értelmezze átfogóan, a természet, a társadalom és a gazdaság
    területére kiterjedően a fenntarthatóság fogalmát, ismerje a
    fenntarthatóság gondolatára vezető tudományos tényeket, modelleket;
    **

12. **ismerje fel a földi életközösségek jövőjéért viselt felelősségét,
    a személyes cselekvés lehetőségét;**

13. **az emberi test felépítéséről és működéséről szerzett tudását
    használja fel a személyes és közösségi egészségmegőrzéssel
    kapcsolatos döntéseiben;**

14. **tájékozódjon és gondolkodjon saját testi-lelki egészségéről,
    tekintse azt olyan értéknek, amely megőrzéséért nap mint nap tennie
    kell;**

15. **ismerje saját testi-szellemi fejlődésének biológiai alapjait, a
    növekedéssel, a nemi éréssel járó változásokat, ismerje fel és
fogadja el az ebben megfigyelhető egyéni különbségeket;**

16. **egészségértése és egészségműveltsége segítse az eligazodását az
    egészségügyi ellátórendszerben, valamint a betegség esetén szükséges
    tennivalók ellátásában;**

17. **élettani, egészségügyi ismeretei és gyakorlati felkészültsége
    alapján, szükség esetén tudjon elsősegélyt nyújtani.**

***A tantárgy tanításának specifikus jellemzői a 7--8. évfolyamon***

A korábbi évek egységes szemléletét megőrizve, fokozatosan ki kell
építeni a természeti jelenségek szaktudományos módszerekkel történő
vizsgálatát lehetővé tevő tanulói *kompetenciákat*. Ezek a megfigyelések
a mérés és a kísérletezés gyakorlati eszköztárát alkotják, valamint
olyan gondolkodási műveletek, amelyek segítik a természettudományos
modellek kialakítását. A természeti jelenségekkel kapcsolatban a tanuló
kutatási kérdéseket, előfeltevéseket fogalmaz meg, néhányat ezek közül
kísérletekkel is vizsgál. A természeti környezetben végzett vizsgálatok
során méréseket végez, adatokat rögzít és értékel. Felkeresi a
természettudományi bemutatóhelyeket, állat- és növénykerteket, nemzeti
parkokat és természetvédelmi területeket, ismerkedik a mezőgazdasági
technológiák biológiai alapjaival. Az élet biológiai értelmezését a
mikroszkópos vizsgálatok során mélyíti el, megismerve a sejtek és
szövetek szerveződési szintjét. Egyszerű élettani kísérletekkel
vizsgálja az élet kémiai és fizikai alapjait, összefüggésbe hozza ezeket
a szervezetszintű életfolyamatokkal. Tudását szövegek, grafikonok,
táblázatok elemzésével bővíti, képessé válik az információk különféle
formáinak átalakítására.

A képességek fejlesztése mellett a biológia tantárgy
*műveltségközvetítő* szerepe részben a fogalmak és elméletek
elsajátítását, részben a tudás működő rendszerré formálását jelenti. A
tanuló az egyedi objektumok és jelenségek vizsgálata során alkalmazza és
elmélyíti a már megismert fogalmakat, elméleteket. Tanulmányozza az élet
folytonosságát, a fajok változását, melyet az evolúció elméletével
hozhat összefüggésbe. Egyre teljesebb képe alakul ki az emberi
szervrendszerekről, saját testéről. Megismeri a sejtalkotók felépítését
és működését, a szervezetszintű folyamatokat a sejtek működésével is
kapcsolatba hozza. Az élet folytonosságát az egyed szintjén is
értelmezi, megérti a növekedés és a fejlődés jellegzetességeit.
Tudatosul benne az egészség megőrzésének fontossága, megismeri az ehhez
szükséges életvitel elemeit, megérti az orvosi, szűrővizsgálatok és
védőoltások lényegét, fontosságát.

Az aktív és interaktív tanulási helyzetek a tanuló *személyiségét* is
fejlesztik. A kérdések felvetése és a válaszok keresése másokkal
együttműködve történik, eközben a kommunikációs készségek is fejlődnek.
A tanuló megfelelő tanári támogatással, de növekvő önállósággal
dolgozhat, lehetőséget kap kreatív ötletek megfogalmazására vagy a
mérlegelő gondolkodás gyakorlására. Megtanul egy feladat megoldása
érdekében hatékonyan együttműködni társaival, a viták és megbeszélések
során tudományosan megalapozott érveket fogalmaz meg. Konkrét példák
alapján felismeri a tudomány bizonyítékokra alapozott működését,
valamint az elméletek adott korra és tudásszintre vonatkozó
érvényességét.

A biológia a 7--8. évfolyamon önálló tantárgyként, vagy a helyi
tantervben szabályozott módon, integrált természettudomány tantárgy
részeként, annak biológia moduljaként is tanítható.

***A tantárgy tanításának specifikus jellemzői a 9--10. évfolyamon***

A középiskola első éveiben a tanuló mélyebben megismeri a biológia
tudományának működését, a problémák vizsgálatára kísérleteket végez,
esetenként tervez. Megismeri a változók típusait, a beállításuk módját,
a tudományosság kritériumait. Meglévő tudása és új információk keresése
által képessé válik hipotézisei megfogalmazására, amelyet a kísérleti
eredmények alapján bizonyít vagy cáfol. Csoportos tanulási helyzetekben
megosztja társaival eredményeit és elképzeléseit, a felelősségteljes
döntési helyzetekben és vitákban koherens és tudományosan megalapozott
érvekkel támasztja alá véleményét. Az életközösségeket a természetben is
vizsgálja, képes hosszabb, projektalapú tanulási feladatok elvégzésére.
A megfigyelésekből és mérésekből kapott információkat szövegesen,
adatbázisokban és képek, videók formájában is rögzíti, ezek alapján
következtetéseket, magyarázatokat fogalmaz meg. Az életfolyamatok
megértésének új szintjét jelentik a korszerű molekuláris biológiai
ismeretek, amelyek elmélyítik a sejtekről tanultakat és elvezetnek a
modern biológiai témák felé. A tanuló felvet és társaival megbeszél
olyan, a biológia technológiai alkalmazásával összefüggő kérdéseket,
mint például a géntechnológia, a klónozás vagy az őssejtek gyógyászati
célú alkalmazása. Megismerkedik a biológiai adatbázisokkal, a biológiai
hálózatok törvényszerűségeivel, a hálózatelmélet adta kutatási
lehetőségekkel és eredményekkel. Az élet szerveződésével és jövőjével
kapcsolatos kép teljessége a bioszféra szintű folyamatok
tanulmányozásával rajzolódik ki. A történeti előzményeket a biológiai
evolúció modellje alapján értelmezi, a jelen globális problémáit
összefüggésbe hozza az emberi tevékenységgel is. A cselekvési
lehetőségek számbavételével formálódik a tanuló természeti
örökségünkért, a Föld jövőjéért érzett felelős attitűdje is. Ebben a
tanulási szakaszban ágazódik el a természettudományos műveltség építése
és a természettudományos életpályákra való felkészítés. A
továbbtanulásra vállalkozók számára ez a korábban megismert témák
elmélyítését, a fogalmak és elméletek tudományos igényű rendszerezését
jelenti, és a modern biológia tanulására is több lehetőségük nyílik.
Olyan témakörök kerülnek feldolgozásra, amelyek a mindennapi élet
kontextusaihoz kapcsolódnak és több tudományterületről elsajátított
tudás alkalmazását teszik lehetővé. Speciális esetben, a helyi tanterv
által szabályozott módon a biológia más természettudományos
tantárgyakkal integrált tanulására is lehetőség van.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 7--8. ÉVFOLYAMON***

1. A biológia tudományának céljai és vizsgálati módszerei

2. Az élet kialakulása és szerveződése

3. Az élet formái, működése és fejlődése

4. Életközösségek vizsgálata

5. Az élővilág és az ember kapcsolata

6. A fenntarthatóság fogalma, biológiai összefüggései

7. Az emberi szervezet felépítése, működése

8. Életmód és egészség

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

1. A biológia kutatási céljai és módszerei

2. Az élet eredete és szerveződése

3. Az életközösségek jellemzői és típusai

4. Öröklődés és evolúció

5. A biotechnológia módszerei és alkalmazása

6. Az ember szervezete és egészsége

7. A bioszféra egyensúlya, fenntarthatóság

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 7--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti a természettudományosan vizsgálható és nem
    vizsgálható problémákat, ismeri és megfelelő támogatás mellett
    alkalmazza a megfigyelés, mérés, kísérletezés módszereit;

2. a biológiai problémák vizsgálata során keresi a kapcsolatot az okok
    és okozatok, az egyedi jelenségek és az általános törvényszerűségek
    között;

3. felismeri és a biológiai jelenségek vizsgálata során figyelembe
    veszi az élő rendszerek egymásba épülő szerveződési szintjeit;

4. lényegi biológiai jellemzőik alapján megkülönbözteti az élővilág
    főbb csoportjait, a mikrobákat, növényeket és állatokat, tényekre
    alapozottan magyarázza a közöttük fennálló kapcsolatokat;

5. felismeri és példákkal bizonyítja az élőlények és környezetük
    közötti kölcsönhatásokat, az alkalmazkodás evolúciós jelentőségét;

6. több szempont figyelembevételével elemzi a környezet- és
    természetvédelmi problémákat, felismeri a védelemre szoruló élő
    természeti értékeket, ismeri az ezt szolgáló törvényi hátteret és a
    közösségi cselekvési lehetőségeket;

7. életmódját az egészségmegőrzés szempontjait figyelembe véve,
    tudatosabban alakítja.

A BIOLÓGIA TUDOMÁNYA

A BIOLÓGIA KUTATÁSI CÉLJAI ÉS MÓDSZEREI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a biológia tudományának kutatási céljait, elismeri a tudósok
    munkáját és felelősségét, képet alkot a biológia fejlődéséről, érti
    a jelenkori kutatások jelentőségét;

2. érti és példákkal igazolja, hogy a tudományos elképzelések az adott
    kor tudásán és világképén nyugszanak, fejlődésük és cseréjük a
    megismerési folyamat természetes jellemzője.

A BIOLÓGIA TANULÁSÁHOZ SZÜKSÉGES KÉSZSÉGEK, KÉPESSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. a biológiai jelenségekkel kapcsolatban kérdéseket, előfeltevéseket
    fogalmaz meg, tudja, hogy ezek akkor vizsgálhatók tudományosan, ha
    lehetőség van a bizonyításra vagy cáfolatra;

2. útmutató alapján, másokkal együttműködve kísérleteket hajt végre,
    azonosítja és beállítja a kísérleti változókat, a kapott adatok
    alapján következtetéseket fogalmaz meg;

3. tisztában van a mérhetőség jelentőségével, törekszik az elérhető
    legnagyobb pontosságra, de tisztában van ennek korlátaival is;

4. alapfokon alkalmazza a rendszerszintű gondolkodás műveleteit,
    azonosítani tudja egy biológiai rendszer részeit, kapcsolatait és
    funkcióit, érti a csoportképzés jelentőségét, a tanult csoportokba
    besorolást végez;

5. digitális eszközökkel képeket, videókat, adatokat rögzít, keres és
    értelmez, kritikus és etikus módon használ fel, alkotásokat készít;

6. megkülönbözteti a bulvár, a népszerűsítő és a tudományos típusú
    közléseket, médiatermékeket, törekszik a megtévesztés, az
    áltudományosság leleplezésére;

7. biológiai rendszerekkel, jelenségekkel kapcsolatos képi
    információkat szóban vagy írásban értelmez, alkalmazza a
    vizualizálás, az ábrákban való összefoglalás módszerét;

8. a vizsgált biológiai jelenségekkel kapcsolatos megfigyeléseit,
    következtetéseit és érveit érthetően és pontosan fogalmazza meg,
    ezeket szükség esetén rajzokkal, fotókkal, videókkal egészíti ki;

9. természetvédelmi, bioetikai, egészségműveltségi témákban tényekre
    alapozottan érvel, vitákban többféle nézőpontot is figyelembe vesz;

10. önállóan vagy másokkal együttműködve kivitelez tanulási projekteket.

AZ ÉLŐVILÁG FEJLŐDÉSE ÉS SZERVEZŐDÉSE

AZ ÉLET LEGEGYSZERŰBB FORMÁI

A nevelési-oktatási szakasz végére a tanuló:

1. tényekre alapozott érveket fogalmaz meg a baktériumok jelentőségével
    kapcsolatban, értékeli egészségügyi, környezeti és biotechnológiai
    jelentőségüket;

2. vázlatrajz, fotó vagy mikroszkópos megfigyelés alapján felismeri és
    megnevezi a sejtmagvas sejttípus legfontosabb alkotórészeit,
    megfogalmazza a sejtekben zajló életfolyamatok lényegi jellemzőit;

3. képek, videók és mikroszkópos megfigyelések alapján összehasonlítja
    a növényi és az állati sejtek felépítését és működését, példák
    alapján értelmezi az egysejtű életmód jellegzetességeit.

AZ ÉLŐVILÁG FEJLŐDÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a biológiai problémák vizsgálatában figyelembe veszi az evolúciós
    fejlődés szempontjait, a földtörténeti időskálán el tudja helyezni
    ennek mérföldköveit;

2. értelmezi a rátermettség és a természetes szelekció fogalmát, tudja,
    hogy azt a véletlenszerű események és az önszerveződés is
    befolyásolhatják.

AZ ÉLŐVILÁG ORSZÁGAI

A nevelési-oktatási szakasz végére a tanuló:

1. alaktani jellemzők összehasonlítása alapján felismer néhány
    fontosabb növény- és állatcsoportot, ezekbe besorolást végez;

2. konkrét példák vizsgálata alapján összehasonlítja a gombák, a
    növények és az állatok testfelépítését, életműködéseit és
    életmódját, ennek alapján érvel az önálló rendszertani csoportba
    sorolásuk mellett.

KÖRNYEZET ÉS ÉLŐVILÁG KAPCSOLATA, FENNTARTHATÓSÁG

BOLYGÓNK ÉLŐVILÁGA

A nevelési-oktatási szakasz végére a tanuló:

1. alapfokon ismeri a földrészek, óceánok legjellegzetesebb növény és
    állatfajait;

2. a földrészek természetes növényzetét ábrázoló tematikus térképek,
    fényképek, ábrák segítségével azonosítja bolygónk biomjait;

3. néhány jellegzetes faj példáján keresztül felismeri a kontinensek
    éghajlati övezetei, kialakult talajtípusai és az ott élő növényvilág
    közötti kapcsolatokat;

4. néhány jellegzetes faj példáján keresztül felismeri a kontinensek
    jellegzetes növényei és az ott élő állatvilág közötti kapcsolatot;

5. néhány tengeri növény és állatfaj megismerése során felismeri, hogy
    bolygónk legnagyobb életközössége a világtengerekben él.

ÉLETKÖZÖSSÉGEK VIZSGÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. másokkal együttműködve vizsgál környezetében található
    életközösségeket, az elkészített rajzok, fotók, videók és adatok
    alapján elemzi az élettelen környezeti tényezők és az élőlények
    közötti kapcsolatokat;

2. leírások, fotók, ábrák, filmek alapján értelmezi és bemutatja az
    élőlények környezethez való alkalmazkodásának jellegzetes módjait és
    példáit;

3. leírások, filmek és saját megfigyelései alapján elemzi az állatok
    viselkedésének alaptípusait, ezek lényegi jellemzőit konkrét példák
    alapján bemutatja;

4. esetleírások, filmek és saját megfigyelései alapján felismeri az
    adott életközösségek biológiai értékeit, értékeli a
    lakókörnyezetében található életközösségek környezeti állapot és
    életminőség-javító hatását;

5. érti és elfogadja, hogy az élő természet rendelkezik olyan
    értékekkel, amelyeket törvényi eszközökkel is védeni kell, ismeri
    ennek formáit, felhívja a figyelmet az általa észlelt
    természetkárosításra;

6. az életformák sokféleségét megőrzendő értékként kezeli, felismeri a
    benne rejlő esztétikai szépséget, érvel a biológiai sokféleség
    veszélyeztetése ellen;

7. tájékozódik a környezetében található védett fajokról,
    életközösségekről, ezek eszmei értékéről és biológiai
    jelentőségéről, Ismeri a hazai nemzeti parkok számát, területi
    elhelyezkedését, bemutatja védendő életközösségeik alapvető
    jellemzőit;

8. kritikusan és önkritikusan értékeli az emberi tevékenység természeti
    környezetre gyakorolt hatását, életvitelében tudatosan követi a
    természet- és környezetvédelem szempontjait;

9. egységben látja az életközösségek múltbeli, jelenkori és várható
    jövőbeli állapotát, azok jövőbeli állapotára valószínűségi
    előrejelzést fogalmaz meg, felismeri és vállalja a jövőjük iránti
    egyéni és közösségi felelősséget;

10. ismeri a növények gondozásának biológiai alapjait, Több szempontot
    is figyelembe véve értékeli a növények, a növénytermesztés
    élelmezési, ipari és környezeti jelentőségét;

11. kritikusan vizsgálja a haszonállatok tartási módjai és a fajra
    jellemző igények közötti ellentmondásokat, ismeri és érti a
    nagyüzemi technológiák és a humánus állattartási módok közötti
    különbségeket.

AZ EMBER SZERVEZETE, AZ EGÉSZSÉGES ÉLETMÓD

SZERVRENDSZEREK ÉS SZERVEK FELÉPÍTÉSE ÉS MŰKÖDÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és megfelelő szempontok szerint értékeli az emberi szervezet
    állapotát, folyamatait jellemző fontosabb adatokat, azokat
    összefüggésbe hozza a testi és lelki állapotával, egészségével;

2. kiegyensúlyozott saját testképpel rendelkezik, amely figyelembe
    veszi az egyéni adottságokat, a nem és a korosztály fejlődési
    jellegzetességeit, valamint ezek sokféleségét;

3. az emberi test megfigyelése alapján azonosítja a főbb testtájakat és
    testrészeket, elemzi ezek arányait és szimmetria viszonyait;

4. felismeri az emberi bőr, a csontváz és vázizomzat főbb elemeit, ezek
    kapcsolódási módjait, értelmezi a mozgási szervrendszer felépítése
    és az ember mozgásképessége közötti összefüggéseket;

5. felismeri a gyakorolt sportok testi és lelki fejlesztő hatását és a
    velük járó terheléseket, baleseti veszélyeket, tanácsokat fogalmaz
    meg ezek elkerülésére;

6. szövegek, ábrák, folyamatvázlatok, videók és szimulációk alapján
    azonosítja a táplálkozási-, keringési-, légzési-, kiválasztási
    szervrendszerek alapvető biológiai funkcióit, az életfolyamatok
    lépéseit;

7. ábrák, makettek alapján felismeri az ember ideg- és
    hormonrendszerének fontosabb szerveit, folyamatvázlatok, videók és
    szimulációk alapján azonosítja az alapvető biológiai funkcióit,
    értelmezi a szabályozás elvét;

8. felismeri, hogy az immunrendszer is információkat dolgoz fel,
    azonosítja a rendszer főbb szerveit, sejtes elemeit és kémiai
    összetevőit;

9. azonosítja az emberi egyedfejlődés főbb szakaszait, bemutatja az
    emberi nemek testi különbözőségének kialakulását, tisztában van a
    felelős szexuális magatartás ismérveivel, értékeli a szexualitás
    egyéni életviteli és párkapcsolati jelentőségét.

EGÉSZSÉGMEGŐRZÉS, ELSŐSEGÉLY

A nevelési-oktatási szakasz végére a tanuló:

1. az egészséggel, életmóddal foglalkozó weboldalak, tematikus média
    források információit kritikusan elemzi, igyekszik tudományos
    bizonyítékokra alapozott híreket, érveket és tanácsokat elfogadni;

2. az egészséget személyes és közösségi értékként értelmezi, érdeklődik
    az egészségmegőrzéssel kapcsolatos információk iránt, mérlegeli azok
    tudományos hitelességét, kritikusan kezeli a gyógyszerekkel,
    gyógyászattal kapcsolatos reklámokat;

3. értékeli a személyi és környezeti higiénia egészségmegőrzéssel
    kapcsolatos jelentőségét, ennek alapelveit személyes környezetében
    is igyekszik alkalmazni;

4. ismeri a kórokozó, a fertőzés és a járvány fogalmait,
    megkülönbözteti a vírusos és bakteriális fertőző betegségeket,
    felismeri az antibiotikumok helyes használatának fontosságát;

5. ismeri a szív- és érrendszeri betegségek kockázati tényezőit,
    igyekszik tudatosan alakítani étkezési szokásait, törekszik az
    életmódjának megfelelő energia- és tápanyagbevitelre, a normál
    testsúly megőrzésére;

6. tudja, hogy a daganatos betegségek kialakulását az életmód és a
    környezet is befolyásolja, és hogy gyógyításuk esélyét a korai
    felismerés nagymértékben növeli;

7. érti az orvosi diagnosztikai eljárások célját, ismeri ennek elvét és
    főbb módszereit, értékeli a megfelelő diagnózis felállításának
    jelentőségét;

8. felméri a baleseti sérülések kockázatait, igyekszik ezeket
    elkerülni, a bekövetkezett balesetek esetében felismeri a sérülés,
    vérzés vagy mérgezés jeleit, ezekről megfelelő beszámolót tud adni;

9. a bekövetkezett balesetet, rosszullétet felismeri, segítséget
    (szükség esetén mentőt) tud hívni, valamint a tőle elvárható módon
    (életkori sajátosságainak megfelelően) elsősegélyt tud nyújtani;

10. tudja alkalmazni az alapszintű újraélesztést mellkas kompressziók és
    lélegeztetés (CPR) kivitelezésével, felismeri ennek szükségességét
    és vállalja a beavatkozást;

11. tényekkel igazolja a testi és lelki egészség közötti kapcsolatot,
    tud ennek egyéni és társadalmi összefüggéseiről, érvel az
    egészségkárosító szokások és függőségek ellen.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. a mindennapi élettel összefüggő problémák megoldásában alkalmazza a
    természettudományos gondolkodás műveleteit, rendelkezik a biológiai
    problémák vizsgálatához szükséges gyakorlati készségekkel;

2. az élő rendszerek belső működése és környezettel való kapcsolataik
    elemzésében alkalmazza a rendszerszintű gondolkodás műveleteit;

3. életközösségek vizsgálata alapján értelmezi a környezet és az
    élőlények felépítése és működése közötti összefüggést, érti az
    ökológiai egyensúly jelentőségét, érvel a biológiai sokféleség
    megőrzése mellett;

4. az emberi test és pszichikum felépítéséről és működéséről szerzett
    ismereteit önismeretének fejlesztésében, egészséges életvitelének
    kialakításában alkalmazza;

5. felismeri a helyi és a globális környezeti problémák összefüggését,
    érvel a Föld és a Kárpát-medence természeti értékeinek védelme
    mellett, döntéseket hoz és cselekszik a fenntarthatóság érdekében.

A BIOLÓGIA TUDOMÁNYA ÉS TANULÁSA

KUTATÁSI CÉLOK, TÁRSADALMI, TECHNOLÓGIAI JELENTŐSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a biológiai kutatások alapvető céljait, legfontosabb
    területeit, értékeli az élet megértésében, az élővilág
    megismerésében és megóvásában játszott szerepét;

2. példákkal igazolja a biológiai ismereteknek a világképünk és a
    technológia fejlődésében betöltött szerepét, gazdasági és társadalmi
    jelentőségét.

KUTATÁSI ÉS GONDOLKODÁSI KÉSZSÉGEK, ESZKÖZÖK ÉS MÓDSZEREK

A nevelési-oktatási szakasz végére a tanuló:

1. az élő rendszerek vizsgálata során felismeri az analógiákat,
    korrelációkat, alkalmazza a statisztikus és a rendszerszintű
    gondolkodás műveleteit, kritikusan és kreatívan mérlegeli a
    lehetőségeket, bizonyítékokra alapozva érvel, több szempontot is
    figyelembe vesz;

2. a vizsgált biológiai jelenségek magyarázatára előfeltevést fogalmaz
    meg, ennek bizonyítására vagy cáfolatára kísérletet tervez és
    kivitelez, azonosítja és beállítja a kísérleti változókat,
    megfigyeléseket és méréseket végez;

3. biológiai vonatkozású adatokat elemez, megfelelő formába rendez,
    ábrázol, ezek alapján előrejelzéseket, következtetéseket fogalmaz
    meg, a már ábrázolt adatokat értelmezi;

4. egyénileg és másokkal együttműködve célszerűen és biztonságosan
    alkalmaz biológiai vizsgálati módszereket, ismeri a fénymikroszkóp
    működésének alapelvét, képes azt használni;

5. érti a biológia molekuláris szintű vizsgálati módszereinek elméleti
    alapjait és felhasználási lehetőségeit, értékeli a biológiai
    kutatásokból származó nagy mennyiségű adat feldolgozásának
    jelentőségét.

DIGITÁLIS KOMPETENCIA, KOMMUNIKÁCIÓ ÉS EGYÜTTMŰKÖDÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a biológiai jelenségek vizsgálata során digitális szöveget, képet,
    videót keres, értelmez és felhasznál, vizsgálja azok
    megbízhatóságát, jogszerű és etikus felhasználhatóságát;

2. biológiai vizsgálatok során elvégzi az adatrögzítés és -rendezés
    műveleteit, ennek alapján tényekkel alátámasztott következtetéseket
    von le;

3. ismeri a tudományos közlések lényegi jellemzőit;

4. tájékozódik a biotechnológia és a bioetika kérdéseiben, ezekről
    folyó vitákban tudományosan megalapozott érveket alkot;

5. a valós és virtuális tanulási közösségekben, másokkal együttműködve
    megtervez és kivitelez biológiai vizsgálatokat, projekteket.

AZ ÉLŐ RENDSZEREK SZERVEZŐDÉSE

AZ ÉLŐVILÁG EGYSÉGE, A FELÉPÍTÉS ÉS MŰKÖDÉS ALAPELVEI

A nevelési-oktatási szakasz végére a tanuló:

1. tudja a biológiai problémákat és magyarázatokat a megfelelő szinttel
    összefüggésben értelmezni;

2. tényekkel bizonyítja az élőlények elemi összetételének hasonlóságát,
    a biogén elemek, a víz, az ATP és a makromolekulák élő
    szervezetekben betöltött alapvető szerepét;

3. megérti, miért és hogyan mehetnek végbe viszonylag alacsony
    hőmérsékleten, nagy sebességgel kémiai reakciók a sejtekben,
    vizsgálja az enzimműködést befolyásoló tényezőket;

4. értékeli és példákkal igazolja a különféle szintű biológiai
    szabályozás szerepét az élő rendszerek normál működési állapotának
    fenntartásában;

5. magyarázza, hogy a sejt az élő szervezetek szerkezeti és működési
    egysége;

6. ábrák, animációk alapján értelmezi, és biológiai tényekkel
    alátámasztja, hogy a vírusok az élő és élettelen határán állnak.

A SEJT ÉS A GENOM SZERVEZŐDÉSE ÉS MŰKÖDÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a felépítés és működés összehasonlítása alapján bemutatja a sejtes
    szerveződés kétféle formájának közös jellemzőit és alapvető
    különbségeit, értékeli ezek jelentőségét;

2. tényekkel igazolja a baktériumok anyagcsere sokfélesége, gyors
    szaporodása és alkalmazkodóképessége közötti összefüggést;

3. felismeri az összetett sejttípus mikroszkóppal megfigyelhető
    sejtalkotóit, magyarázza a sejt anyagcsere-folyamatainak lényegét;

4. ismeri az örökítőanyag többszintű szerveződését, képek, animációk
    alapján értelmezi a sejtekben zajló biológiai információ
    tárolásának, átírásának és kifejeződésének folyamatait;

5. tudja, hogy a sejtekben és a sejtek között bonyolult jelforgalmi
    hálózatok működnek, amelyek befolyásolják a génműködést, és
    felelősek lehetnek a normál és a kóros működésért is;

6. összehasonlítja a sejtosztódás típusait, megfogalmazza ezek
    biológiai szerepét, megérti, hogy a soksejtű szervezetek a
    megtermékenyített petesejt és utódsejtjei meghatározott számú
    osztódásával és differenciálódásával alakulnak ki;

7. felismeri az összefüggést a rák kialakulása és a sejtciklus zavarai
    között, megérti, hogy mit tesz a sejt és a szervezet a daganatok
    kialakulásának megelőzéséért.

A SEJT ÉS A MAGASABB SZERVEZŐDÉSI SZINTEK KAPCSOLATA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az őssejt fogalmát, különféle típusait és azok jellemzőit,
    különbséget tesz őssejt és daganatsejt között;

2. fénymikroszkópban, ábrán vagy fotón felismeri és jellemzi a főbb
    állati és növényi szövettípusokat, elemzi, hogy milyen funkciók
    hatékony elvégzésére specializálódtak;

3. ismeri és példákkal bizonyítja az élőlények szén- és
    energiaforrásainak különféle lehetőségeit, az anyagcseretípusok
    közötti különbséget;

4. vázlatrajzok, folyamatábrák és animációk alapján azonosítja a
    fotoszintézis és a sejtlégzés fő szakaszainak sejten belüli helyét
    és struktúráit, a fontosabb anyagokat és az energiaátalakítás
    jellemzőit;

5. a sejtszintű anyagcsere folyamatok alapján magyarázza a növények és
    állatok közötti ökológiai szintű kapcsolatot, a termelő és fogyasztó
    szervezetek közötti anyagforgalmat.

AZ ÉLŐVILÁG FEJLŐDÉSE, BIOLÓGIAI EVOLÚCIÓ

AZ ÉLET EREDETE ÉS FELTÉTELEI

A nevelési-oktatási szakasz végére a tanuló:

1. a földi élet keletkezését biológiai kísérletek és elméletek alapján
    magyarázza;

2. érti és tényekkel igazolja az ősbaktériumok különleges élőhelyeken
    való életképességét;

3. biológiai és csillagászati tények alapján mérlegeli a Földön kívüli
    élet valószínűsíthető feltételeit és lehetőségeit.

A VÁLTOZÉKONYSÁG MOLEKULÁRIS ALAPJAI, EGYEDSZINTŰ ÖRÖKLŐDÉS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az örökítőanyag bázissorrendjének vagy bázisainak
    megváltozásához vezető folyamatokat, konkrét esetekben azonosítja
    ezek következményeit;

2. a géntechnológia céljának és módszertani alapjainak ismeretében,
    kritikai szemlélettel elemzi a genetikai módosítások előnyeit és
    kockázatait;

3. érti az örökítőanyagban tárolt információ és a kifejeződő
    tulajdonságok közötti összefüggést, megkülönbözteti a genotípust és
    a fenotípust;

4. megérti a genetikai információ nemzedékek közötti átadásának
    törvényszerűségeit, ezeket konkrét esetek elemzésében alkalmazza;

5. felismeri a kapcsolatot az életmód és a gének kifejeződése között,
    érti, hogy a sejt és az egész szervezet jellemzőinek kialakításában
    és fenntartásában kiemelt szerepe van a környezet általi
    génaktivitás-változásoknak.

A BIOLÓGIAI EVOLÚCIÓ, ADAPTÍV ÉS NEM ADAPTÍV FOLYAMATOK

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a természetes változatosság szerveződését, az evolúciós
    változások eredetét és elterjedését magyarázó elemi folyamatokat,
    felismer és magyaráz mikro- és makroszintű evolúciós jelenségeket;

2. példákkal igazolja, hogy a szelekció a különböző szerveződési
    szinteken értelmezhető tulajdonságokon keresztül egyidejűleg hat;

3. példákkal mutatja be az élővilág főbb csoportjainak evolúciós
    újításait, magyarázza, hogy ezek hogyan segítették elő az adott
    élőlénycsoport elterjedését;

4. érti és elfogadja, hogy a mai emberek egy fajhoz tartoznak, és az
    evolúció során kialakult nagyrasszok értékükben nem különböznek, a
    biológiai és kulturális örökségük az emberiség közös kincse;

5. morfológiai, molekuláris biológiai adatok alapján egyszerű
    származástani kapcsolatokat elemez, törzsfát készít;

6. ismeri az evolúció befolyásolásának lehetséges módjait (például
    mesterséges szelekció, fajtanemesítés, géntechnológia), értékeli
    ezek előnyeit és esetleges hátrányait.

AZ EMBER SZERVEZETE ÉS EGÉSZSÉGE

AZ EMBERI SZERVEZET ANATÓMIÁJA, ÉLETTANA ÉS EGÉSZSÉGVÉDELME

A nevelési-oktatási szakasz végére a tanuló:

1. megérti a környezeti állapot és az ember egészsége közötti
    összefüggéseket, azonosítja az ember egészségét veszélyeztető
    tényezőket, felismeri a megelőzés lehetőségeit, érvényesíti az
    elővigyázatosság elvét;

2. elemzi az ember mozgásképességének biokémiai, szövettani és
    biomechanikai alapjait, ezeket összefüggésbe hozza a mindennapi
    élet, a sport és a munka mozgásformáival, értékeli a rendszeres
    testmozgás szerepét egészségének megőrzésében;

3. az emberi test kültakarójának, váz- és izomrendszerének elemzése
    alapján magyarázza az ember testképének, testalkatának és
    mozgásképességének biológiai alapjait;

4. a táplálkozás-, a légzés-, a keringés- és a kiválasztás
    szervrendszerének elemzése alapján magyarázza az emberi szervezet
    anyag- és energiaforgalmi működésének biológiai alapjait;

5. az ideg-, hormon- és immunrendszer elemzése alapján magyarázza az
    emberi szervezet információs rendszerének biológiai alapjait;

6. felsorolja az emberi egyedfejlődés főbb szakaszait, magyarázza
    hogyan és miért változik a szervezetünk az életkor előrehaladásával,
    értékeli a fejlődési szakaszok egészségvédelmi szempontjait, önmagát
    is elhelyezve ebben a rendszerben.

AZ EMBERI NEMEK ÉS A SZAPORODÁS BIOLÓGIAI ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a férfi és a női nemi szervek felépítését és működését, a
    másodlagos nemi jellegeket és azok kialakulási folyamatát,
    ismereteit összekapcsolja a szaporító szervrendszer egészségtanával;

2. biológiai ismereteit is figyelembe véve értékeli az emberi
    szexualitás párkapcsolattal és a tudatos családtervezéssel
    összefüggő jelentőségét;

3. megérti a fogamzásgátlók hatékonyságáról szóló információkat, a
    személyre szabott, orvosilag ellenőrzött fogamzásgátlás fontosságát;

4. ismeri a fogamzás feltételeit, a terhesség jeleit, bemutatja a
    magzat fejlődésének szakaszait, értékeli a terhesség alatti
    egészséges életmód jelentőségét.

A LELKI EGYENSÚLY ÉS A TESTI ÁLLAPOT ÖSSZEFÜGGÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a biológiai működések alapján magyarázza a stressz fogalmát,
    felismeri a tartós stressz egészségre gyakorolt káros hatásait,
    igyekszik azt elkerülni, csökkenteni;

2. ismeri a gondolkodási folyamatokat és az érzelmi és motivációs
    működéseket meghatározó tényezőket, értékeli az érzelmi és az
    értelmi fejlődés kapcsolatát;

3. ismeri a mentális egészség jellemzőit, megérti annak feltételeit,
    ezek alapján megtervezi az egészségmegőrző magatartásához szükséges
    életviteli elemeket;

4. megérti az idegsejtek közötti jelátviteli folyamatokat, és
    kapcsolatba hozza azokat a tanulás és emlékezés folyamataival, a
    drogok hatásmechanizmusával;

5. az agy felépítése és funkciója alapján magyarázza az információk
    feldolgozásával, a tanulással összefüggő folyamatokat, értékeli a
    tanulási képesség jelentőségét az egyén és a közösség szempontjából;

6. biológiai folyamatok alapján magyarázza a függőség kialakulását,
    felismeri a függőségekre vezető tényezőket, ezek kockázatait és
    következményeit.

AZ EGÉSZSÉGÜGYI RENDSZER ISMERETE, ELSŐSEGÉLYNYÚJTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az orvosi diagnosztika, a szűrővizsgálatok és védőoltások
    célját, lényegét, értékeli ezek szerepét a betegségek megelőzésében
    és a gyógyulásban;

2. megkülönbözteti a házi- és a szakorvosi ellátás funkcióit, ismeri az
    orvoshoz fordulás módját, tisztában van a kórházi ellátás
    indokaival, jellemzőivel;

3. ismeri a leggyakoribb fertőző betegségek kiváltó okait, ismeri a
    fertőzések elkerülésének lehetőségeit és a járványok elleni
    védekezés módjait;

4. ismeri a leggyakoribb népbetegségek (pl. szívinfarktus, stroke,
    cukorbetegség, allergia, asztma) kockázati tényezőit, felismeri ezek
    kezdeti tüneteit;

5. képes a bekövetkezett balesetet, rosszullétet felismerni,

6. képes a sérült vagy beteg személy ellátását a rendelkezésre álló
    eszközökkel (vagy eszköz nélkül) megkezdeni, segítséget (szükség
    esetén mentőt) hívni;

7. szükség esetén alkalmazza a felnőtt alapszintű újraélesztés
    műveleteit (CPR), képes a félautomata defibrillátort alkalmazni.

KÖRNYEZET ÉS ÉLŐVILÁG KAPCSOLATA

AZ ÉLŐHELYEK JELLEMZŐI, A POPULÁCIÓK KÖZÖTTI KAPCSOLATOK

A nevelési-oktatási szakasz végére a tanuló:

1. példákkal mutatja be a fontosabb hazai szárazföldi és vizes
    életközösségek típusait, azok jellemzőit és előfordulásait;

2. megfigyelések, leírások és videók alapján azonosítja a populációk
    közötti kölcsönhatások típusait, az ezzel összefüggő etológiai
    jellemzőket, bemutatja ezek jellegét, jelentőségét;

3. érti az ökológiai mutatókkal, bioindikációs vizsgálatokkal
    megvalósuló környezeti állapotelemzések céljait, adott esetben
    alkalmazza azok módszereit;

4. ismeri a levegő-, a víz- és a talajszennyezés forrásait, a szennyező
    anyagok típusait és példáit, konkrét esetek alapján elemzi az
    életközösségekre gyakorolt hatásukat.

AZ ÉLŐHELYI KÖRNYEZETHEZ VALÓ ALKALMAZKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és példákkal igazolja az állatok viselkedésének a
    környezethez való alkalmazkodásban játszott szerepét;

2. felismeri a természetes élőhelyeket veszélyeztető tényezőket,
    kifejti álláspontját az élőhelyvédelem szükségességéről, egyéni és
    társadalmi megvalósításának lehetőségeiről.

AZ ÉLETKÖZÖSSÉGEK BIOLÓGIAI SOKFÉLESÉGE

A nevelési-oktatási szakasz végére a tanuló:

1. érti a biológiai sokféleség fogalmát, ismer a meghatározásra
    alkalmas módszereket, értékeli a bioszféra stabilitásának
    megőrzésében játszott szerepét;

2. érti az ökológiai egyensúly fogalmát, értékeli a jelentőségét,
    példákkal igazolja az egyensúly felborulásának lehetséges
    következményeit;

3. érti az ökológiai rendszerek működése és a biológiai sokféleség
    közötti kapcsolatot, konkrét életközösségek vizsgálata alapján
    táplálkozási piramist, hálózatot elemez.

A FENNTARTHATÓSÁG ELVE, SZEMPONTJAI

AZ EMBERI TEVÉKENYSÉG HATÁSA A BIOSZFÉRÁRA

A nevelési-oktatási szakasz végére a tanuló:

1. konkrét példák alapján vizsgálja a bioszférában végbemenő
    folyamatokat, elemzi ezek idő- és térbeli viszonyait, azonosítja az
    emberi tevékenységgel való összefüggésüket;

2. a kutatások adatai és előrejelzései alapján értelmezi a globális
    éghajlatváltozás élővilágra gyakorolt helyi és bioszféra szintű
    következményeit;

3. példák alapján elemzi a levegő-, a víz- és a talajszennyeződés, az
    ipari és természeti katasztrófák okait és ezek következményeit, az
    emberi tevékenységnek az élőhelyek változásához vezető hatását,
    ennek alapján magyarázza egyes fajok veszélyeztetettségét.

A FENNTARTHATÓ ÉLETVITEL, TECHNOLÓGIA ÉS GAZDÁLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. érti és elfogadja, hogy a jövőbeli folyamatokat a jelen cselekvései
    alakítják, tudja, hogy a folyamatok tervezése, előrejelzése
    számítógépes modellek alapján lehetséges;

2. értékeli a környezet- és természetvédelem fontosságát, megérti a
    nemzetközi összefogások és a hazai törekvések jelentőségét,
    döntéshozatalai során saját személyes érdekein túl a természeti
    értékeket és egészség-megőrzési szempontokat is mérlegeli;

3. történeti adatok és jelenkori esettanulmányok alapján értékeli a
    mezőgazdaság, erdő- és vadgazdaság, valamint a halászat természetes
    életközösségekre gyakorolt hatását, példák alapján bemutatja az
    ökológiai szempontú, fenntartható gazdálkodás technológiai
    lehetőségeit;

4. megérti a biotechnológiai eljárások és a bionika eredményeinek
    alkalmazási lehetőségeit, értékeli az információs technológiák
    alkalmazásának orvosi, biológiai jelentőségét.

A FÖLD ÉS A KÁRPÁT-MEDENCE ÉRTÉKEI

A nevelési-oktatási szakasz végére a tanuló:

1. érvel a Föld, mint élő bolygó egyedisége mellett, tényekre
    alapozottan és kritikusan értékeli a természeti okokból és az emberi
    hatásokra bekövetkező változásokat;

2. ismeri a Kárpát-medence élővilágának sajátosságait, megőrzendő
    értékeit, ezeket összekapcsolja a hazai nemzeti parkok
    tevékenységével.

***II.3.6.4. FIZIKA***

***A) ALAPELVEK, CÉLOK***

A természettudományok, és így a fizika az emberi megismerés fontos és
hatékony eszközei. A természet alaptörvényeinek feltárása, azok
alkalmazása világunk jobb megértése, és technikai civilizációnk
fejlesztése érdekében közvetlen, életminőségünket befolyásoló előnyökkel
jár. A fizikai műveltség alapvető kulturális érték, megőrzése és
gyarapítása az egymást követő nemzedékek kiemelt feladata, a jövő iránti
elkötelezettség megnyilvánulása.

A fizika oktatása során a hangsúly a fizikai gondolkodásmódra, a fizika
megismerési módszereire, mindennapi életben való alkalmazhatóságára
esik, olyan ismeretekre, melyekre a nem szakirányba továbbtanuló
tanulónak is szüksége van. Az oktatási, tanulási folyamat mélyíti a
szükséges szakmai ismereteket, támogatja a tudásalkalmazást,
összekapcsolja a tantárgyon belüli és a tantárgyak közötti releváns
információkat és szervesen épít a jelenségalapú tudásszervezés
alapelveire.

A 7--8. évfolyamon a tanuló életkori sajátosságainak megfelelően a
tananyag feldolgozása szorosan kapcsolódik a mindennapok problémáihoz,
azokból indul ki és azokra keresi a választ. Kerüli a túlzott
absztrakciót, miközben az aktív tanulás megvalósításával, a
kísérletezésre, megfigyelésre épülő tapasztalatok összegyűjtésével,
értelmezésével megteremti a szaktudományos ismeretek befogadásának
valódi alapjait. A fizika a 7--8. évfolyamon a helyi tantervben
szabályozott módon integrált természettudomány tantárgy részeként, annak
fizika moduljaként is oktatható.

A középiskola szintjén a tapasztalatok összegyűjtése, azok
formalizálása, egyszerűbb modellek alkotása, a modell és a valóság
összevetése új készségek kialakítását igényli annak érdekében, hogy
olyan hatékony megismerési rendszer alakuljon ki, amely megalapozza az
elméleti kutatást, valamint a mérnöki, technikai fejlesztéseket is. A
természettudományos megismerési folyamat élményszerű megélése a fizika
oktatásának meghatározó eleme ebben a nevelési szakaszban. Speciális
esetben a középiskolában is lehetőség nyílik az integrált
tanulásszervezésre.

A mai korban az információkat, a szakismereteket az egyre könnyebben és
hatékonyabban használható digitális adatbázisok biztosítják. Ugyanakkor
az adatbázisok sikeres használatához személyes tudásra is szükség van. A
természettudományos és mérnöki pályákra készülőknek tisztában kell
lenniük az ismeretrendszerek fő struktúrájával, kulcsfogalmainak
jelentésével és megfelelő matematikai kompetenciákkal is rendelkezniük
kell.

A tantárgy céljai közt szerepel a fizika természettudományos és
általános társadalmi kontextusának kibontása, mely leginkább a
tudománytörténet érdekesebb fejezeteinek tanulmányozása révén válik
lehetővé. A fizika művelése, mint minden természettudományos
tevékenység, működése és hatásai okán társadalmi jelenség.

A fizika tanulásának célja, hogy a tanuló:

1. azonosítani tudja a fizika körébe tartozó problémákat, a természeti
    és technikai környezet leírására a megfelelő fizikai mennyiségeket
    használja, a jelenségek értelmezése során a megismert fizikai
    elveket alkalmazza;

2. a megismert jelenségek kapcsán egyszerű számolásokat végezzen,
    grafikus formában megfogalmazott feladatokat oldjon meg, egyszerű
    méréseket, megfigyeléseket tervezzen, végrehajtson, kiértékeljen,
    ábrákat készítsen;

3. tudjon információkat keresni a vizsgált tudományterülethez
    kapcsolódóan a rendelkezésre álló információforrásokban,
    elektronikus adathordozókon, nyitottan közelítsen az újdonságokhoz
    folyamatos érdeklődés mellett;

4. ismerje meg a fenntartható fejlődés fogalmát és fizikai
    vonatkozásait, elősegítve ezzel a természet és környezet, illetve a
    fenntartható fejlődést segítő életmód iránti felelősségteljes
    elköteleződés kialakulását;

5. felismerjen és megértsen a természettudományok különböző területei
    között fennálló kapcsolatokat konkrét jelenségek kapcsán;

6. eligazodjon a közvetlen természeti és technikai környezetükben,
    illetve a tanultakat alkalmazni tudja a mindennapokban használt
    eszközök működési elvének megértésére, a biztonságos eszközhasználat
    elsajátítására;

7. felismerje az ember és környezetének kölcsönhatásából fakadó
    előnyöket és problémákat, tudatosítsa az emberiség felelősségét a
    környezet megóvásában;

8. fel tudja tárni a megfigyelt jelenségek ok-okozati hátterét;

9. képessé váljon Univerzumunkat és az embert kölcsönhatásában
    szemlélni, az emberiség fejlődéstörténetét, jelenét és jövőjét és az
    Univerzum történetét összekapcsolni;

10. tisztába kerüljön azzal, hogy a tudomány művelése alapvetően
    társadalmi jelenség;

11. megtanuljon különbséget tenni a valóság és az azt leképező
    természettudományos modellek, leírások és világról alkotott képek
    között;

12. felismerje, hogy a természet egységes egész, szétválasztását
    résztudományokra csak a jobb kezelhetőség, áttekinthetőség
    indokolja, a fizika törvényei általánosak, amelyek a kémia, a
    biológia, a földtudományok és az alkalmazott műszaki tudományok
    területén is érvényesek.

***A tantárgy tanításának specifikus jellemzői a 7--8. évfolyamon***

A fizika a 7--8. évfolyamon önálló tantárgyként vagy a helyi tantervben
szabályozott módon, az integrált természettudomány tantárgy részeként,
annak fizika moduljaként is oktatható. Az életkori szakasznak
megfelelően a tananyag feldolgozása jelenségközpontú, tehát valamilyen
megfogható, megfigyelhető, megtapasztalható jelenségből kiindulva kerül
feldolgozásra. A témaválasztás gyakorlatorientált, az egyes témák
feldolgozásának célja mindig valamilyen gyakorlati, a mindennapokban
hasznos ismeret megszerzése. Fontos szempont a tananyagválasztás során
az aktualitásra való törekvés, tehát az anyag a hétköznapjainkban
aktuálisan használt eszközeink működésének megértésére fókuszál, ahol
lehetséges, az aktuális hírekre reflektál. Ebben az életkori szakaszban
a tananyag feldolgozása elsődlegesen kvalitatív, és ebben a minőségében
inkább leíró, megfigyelő, mint értelmező, miközben cél az értelmező
gondolkodás fejlesztése, az életkornak megfelelő szintű modellalkotás.
Nem cél, hogy ezek a modellek maradéktalanul megfeleljenek a magasabb
tudományosság igényeinek, inkább a tanuló életkori sajátosságaiból,
előzetes tudásából kiindulva szolgáljanak eszközként a
természettudományos gondolkodás elsajátításához. A tananyag feldolgozása
során alkalmazandók a differenciálás elvei, a magyarázatok mélységét a
diákcsoport képességeihez kell igazítani. A műveltségtartalmak ebben az
életkori szakaszban a közvetlen környezet jelenségeinek megfigyeléséhez,
a mindennapokban használt eszközök működésének leírásához kapcsolódnak.
Ugyanakkor a tanuló általános képet szerez a Világegyetem
nagyságrendjeiről, ezen belül a Föld elhelyezkedéséről az Univerzumban,
valamint a természeti folyamatokat és technikai eszközök működését
egyaránt meghatározó energetikai viszonyokról.

***A*** ***tantárgy tanításának specifikus jellemzői a 9--10.
évfolyamon***

A fizika a középiskolában a szaktárgyi ismeretek elmélyítésén, az
összefüggések megértésén túl a mindenki számára fontos, mindennapokban
használható ismeretek bemutatására törekszik. A tanulónak a tantárggyal
való foglalkozás során fel kell ismernie, hogy a fizika hasznos, az élet
minden fontos területén megjelenik, ismerete gyakorlati előnyökkel jár.
A cél a problémaközpontúság, a gyakorlatiasság és az ismeretek
egyensúlyának megteremtése a motiváció folyamatos fenntartásának és
minden tanuló eredményes tanulásának érdekében, megteremtve a
lehetőségét annak, hogy a tanuló logikusan gondolkodó, a világ belső
összefüggéseit megértő, felelős döntésekre kész felnőtté váljon.
Korunkban a hatékony oktatás elképzelhetetlen aktív tanulás nélkül, ami
a tanár részéről egyszerre kíván módszertani sokféleséget és új
értékelési eljárások meghonosítását. Fontos megmutatni a tanulónak azt,
hogy természettudományos tudásunk az osztatlan emberi műveltség része,
és ezer szálon kapcsolódik a humán kultúrához, a lét nagy kérdéseihez. A
fizika tanulása-tanítása során a természettudományos világkép fejlődik,
átalakul, és ez a fejlődés a technikai fejlődést alapozza meg. A fizika
tanulása során elsajátítandó az a szemlélet, amely a tudomány működését
olyan társadalmi jelenségnek tekinti, amelynek szabályozása,
háttérintézményei, témaválasztása, következtetései megjelennek
mindennapi döntéseinkben, értékítéletünkben. A tudomány és a gazdaság
szoros kapcsolatban van, és kapcsolatrendszerük legfőbb jellemzőinek
megismerése elengedhetetlen a felelős állampolgári viselkedés
elsajátításához. A tudomány egyben olyan módszer, működési forma,
szabályrendszer, mely pontosan definiálja önmagát, és ennek köszönhetően
könnyen azonosíthatóvá válnak a tudományosság látszatát keltő, de
valójában tudományosan megalapozatlan elképzelések. A tanulási terület
műveltségtartalmai a közvetlen környezetünkről megszerezhető ismereteket
mélyítik el, ugyanakkor kitekintést adnak a tágabb környezetre is. Az
emberiség globális problémáira hívják fel a figyelmet, s bemutatják a
modern természettudomány újszerű, szemléletformáló eredményeit, valamint
azt az eredményekben rejlő perspektívát, mely az elméleti kutatás és a
technikai fejlődés előtt áll.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 7--8. ÉVFOLYAMON***

A javasolt kontextusalapú tananyag-felépítés nagyfokú rugalmasságot tesz
lehetővé. Így a fizikai ismeretek feldolgozása mind diszciplináris, mind
integrált oktatás formájában megvalósítható.

1. Fizikai jelenségek megfigyelése, egyszerű értelmezése

2. Mozgások a környezetünkben, a közlekedés

3. A levegő, a víz, a szilárd anyagok

4. Fontosabb mechanikai, hőtani, elektromos és optikai eszközeink
    > működésének alapjai, fűtés és világítás a háztartásban

5. Az energia megjelenési formái, megmaradása, energiatermelés és
    > felhasználás

6. A Föld, a Naprendszer és a Világegyetem, a Föld jövője, megóvása

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

1. A fizikai jelenségek megfigyelése, modellalkotás, értelmezés,
    tudományos érvelés

2. Mozgások a környezetünkben, a közlekedés kinematikai és dinamikai
    vonatkozásai

3. A halmazállapotok és változásuk, a légnemű, folyékony és szilárd
    anyagok tulajdonságai

4. Az emberi test fizikájának elemei

5. Fontosabb mechanikai, hőtani és elektromos eszközeink működésének
    alapjai, fűtés és világítás a háztartásban

6. A hullámok szerepe a képek és hangok rögzítésében, továbbításában

7. Az energia megjelenési formái, megmaradása, energiatermelés és
    -felhasználás

8. Az atom szerkezete, fénykibocsátás, radioaktivitás

9. A Föld, a Naprendszer és a Világegyetem, a Föld jövője, megóvása, az
    űrkutatás eredményei

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 7--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a legfontosabb természeti jelenségeket, azok életkorának
    > megfelelően egyszerűsített, a fizikai mennyiségeken és törvényeken
    > alapuló magyarázatait;

2. értelmezni tudja lakóhelyét a Földön, a Föld helyét a
    > Naprendszerben, a Naprendszer helyét a galaxisunkban és az
    > Univerzumban;

3. tájékozott a Földünket és környezetünket fenyegető globális
    > problémákban, ismeri az emberi tevékenység lehetséges szerepét
    > ezek kialakulásában;

4. felismeri, hogyan jelennek meg a fizikai ismeretek a gyakran
    > használt technikai eszközök működésében;

5. ismeri a világot leíró fontos fizikai mennyiségeket, azok
    > jelentését, jellemző nagyságrendjeit a mindennapokban;

6. egyszerű, a megértést segítő számítási feladatokat old meg;

7. szemléletes példákon keresztül felismeri a fizikai ismeretek
    > bővülése és a társadalmi-gazdasági folyamatok, történelmi
    > események közötti kapcsolatot;

8. önállóan keres és olvas fizikai témájú ismeretterjesztő szövegeket,
    > törekszik a lényeg kiemelésére.

A FIZIKA MINT TERMÉSZETTUDOMÁNYOS MEGISMERÉSI MÓDSZER

FIZIKAI MEGFIGYELÉSEK, KÍSÉRLETEK VÉGZÉSE, AZ EREDMÉNYEK ÉRTELMEZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeléseket és kísérleteket végez a környezetében, az abból
    származó tapasztalatokat rögzíti;

2. felismeri a tudomány által vizsgálható jelenségeket, azonosítani
    tudja a tudományos érvelést, kritikusan vizsgálja egy elképzelés
    tudományos megalapozottságát;

3. hétköznapi eszközökkel méréseket végez, rögzíti a mérések
    eredményeit, leírja a méréssorozatokban megfigyelhető tendenciákat,
    ennek során helyesen használja a közismert mértékegységeket;

4. jó becsléseket tud adni egyszerű számítás, következtetés
    segítségével;

5. értelmezni tud egy jelenséget, megfigyelést valamilyen korábban
    megismert vagy saját maga által alkotott egyszerű elképzelés
    segítségével.

A FIZIKA TUDOMÁNYÁNAK TERÜLETEI, LEGÚJABB EREDMÉNYEI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a fizika fontosabb szakterületeit;

2. tájékozott a fizika néhány új eredményével kapcsolatban.

A FIZIKA MŰVELÉSÉNEK TÁRSADALMI VONATKOZÁSAI, KÖRNYEZET- ÉS
TERMÉSZETTUDATOSSÁG

A FIZIKA SZEREPE A KÖRNYEZET MEGÓVÁSÁBAN

A nevelési-oktatási szakasz végére a tanuló:

1. tudja azonosítani a széles körben használt technológiák
    környezetkárosító hatásait, és fizikai ismeretei alapján javaslatot
    tesz a károsító hatások csökkentésének módjára;

2. környezetében zajszintméréseket végez számítógépes mérőeszközzel,
    értelmezi a kapott eredményt;

3. ismeri az ózonpajzs elvékonyodásának és az ultraibolya sugárzás
    erősödésének tényét és lehetséges okait.

AZ ENERGIAGAZDÁLKODÁS FIZIKAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. tisztában van azzal, hogy az energiának ára van, gyakorlati példákon
    keresztül ismerteti az energiatakarékosság fontosságát, ismeri az
    energiatermelés környezeti hatásait, az energiabiztonság fogalmát;

2. ismeri a jövő tervezett energiaforrásaira vonatkozó legfontosabb
    elképzeléseket;

3. ismeri a zöldenergia és fosszilis energia fogalmát, az erőművek
    energiaátalakításban betöltött szerepét, az energiafelhasználás
    módjait és a háztartásokra jellemző fogyasztási adatokat.

A GLOBÁLIS PROBLÉMÁK EGYSZERŰBB FIZIKAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az éghajlatváltozás problémájának összetevőit, lehetséges
    okait. Tisztában van a hagyományos ipari nyersanyagok földi
    készleteinek végességével és e tény lehetséges következményeivel;

2. tudatában van az emberi tevékenység természetre gyakorolt lehetséges
    negatív hatásainak és az ezek elkerülésére használható fizikai
    eszközöknek és eljárásoknak (pl. porszűrés, szennyezők távolról való
    érzékelése alapján elrendelt forgalomkorlátozás);

3. tisztában van az űrkutatás aktuális céljaival, legérdekesebb
    eredményeivel.

A FIZIKAI ISMERETEK BŐVÜLÉSÉNEK GAZDASÁGI, TÁRSADALMI HATÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. megismeri jelentős fizikusok életének és tevékenységének
    legfontosabb részleteit, azok társadalmi összefüggéseit (pl. Isaac
    Newton, Arkhimédész, Galileo Galilei, Jedlik Ányos);

2. felismeri a fizikai kutatás által megalapozott technikai fejlődés
    egyes fejezeteinek a társadalomra, illetve a történelemre gyakorolt
    hatását, meg tudja fogalmazni a természettudomány fejlődésével
    kapcsolatos alapvető etikai kérdéseket.

A TERMÉSZETI JELENSÉGEK ÉS A TECHNIKAI ESZKÖZÖK, TECHNOLÓGIÁK FIZIKÁJA

A LEGÁLTALÁNOSABB TERMÉSZETI JELENSÉGEK FIZIKAI ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a környezetében előforduló legfontosabb természeti jelenségek
    (például időjárási jelenségek, fényviszonyok változásai, égi
    jelenségek) fizikai magyarázatát;

2. tudja, miben nyilvánulnak meg a kapilláris jelenségek, ismer ezekre
    példákat a gyakorlatból (pl. növények tápanyagfelvétele a talajból).

A GYAKRAN HASZNÁLT TECHNIKAI ESZKÖZÖK, TECHNOLÓGIÁK FIZIKAI ALAPJAI,
BIZTONSÁGOS HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a legfontosabb, saját maga által használt eszközök (például
    közlekedési eszközök, elektromos háztartási eszközök, szerszámok)
    működésének fizikai lényegét;

2. tisztában van az önvezérelt járművek működésének elvével, illetve
    néhány járműbiztonsági rendszer működésének fizikai hátterével;

3. ismeri az aktuálisan használt elektromos fényforrásokat, azok
    fogyasztását és fényerejét meghatározó mennyiségeket, a háztartásban
    gyakran használt áramforrásokat;

4. ismeri a villamos energia felhasználását a háztartásban, az
    energiatakarékosság módozatait, az érintésvédelmi és biztonsági
    rendszereket és szabályokat;

5. ismeri néhány gyakran használt optikai eszköz részeit, átlátja
    működési elvüket;

6. érti a színek kialakulásának elemi fizikai hátterét.

AZ EGÉSZSÉGES ÉLETMÓD FIZIKAI HÁTTERE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a hallás folyamatát, a levegő hullámzásának szerepét a hang
    továbbításában. Meg tudja nevezni a halláskárosodáshoz vezető főbb
    tényezőket;

2. ismeri a látás folyamatát, a szem hibáit és a szemüveg szerepét ezek
    kijavításában, a szem megerőltetésének (például számítógép)
    következményeit;

3. átlátja a táplálékok energiatartalmának szerepét a szervezet
    energiaháztartásában és az ideális testsúly megtartásában.

FIZIKAI SZAKISMERETEK

MOZGÁSOK A KÖRNYEZETÜNKBEN

A nevelési-oktatási szakasz végére a tanuló:

1. megfelelően tudja összekapcsolni a hely- és időadatokat. Különbséget
    tesz az út és elmozdulás fogalma között, ismeri, és ki tudja
    számítani az átlagsebességet, a mértékegységeket megfelelően
    használja, tudja, hogy lehetnek egyenletes és nem egyenletes
    mozgások, ismeri a testek sebességének nagyságrendjét;

2. meghatározza az egyenes vonalú egyenletes mozgást végző test
    sebességét, a megtett utat, az út megtételéhez szükséges időt;

3. tisztában van a mozgások kialakulásának okával, ismeri az erő
    szerepét egy mozgó test megállításában, elindításában, valamilyen
    külső hatás kompenzálásában;

4. egyszerű eszközökkel létrehoz periodikus mozgásokat, méri a
    periódusidőt, fizikai kísérleteket végez azzal kapcsolatban, hogy
    mitől függ a periódusidő;

5. érti a hullámmozgás lényegét és a jellemző legfontosabb
    mennyiségeket: frekvencia, amplitúdó, hullámhossz, terjedési
    sebesség.

AZ ENERGIA

A nevelési-oktatási szakasz végére a tanuló:

1. kvalitatív ismeretekkel rendelkezik az energia szerepéről, az
    energiaforrásokról, az energiaátalakulásokról;

2. előidéz egyszerű energiaátalakulással járó folyamatokat (melegítés,
    szabadesés), megnevezi az abban szereplő energiákat.

AZ ANYAG ÉS HALMAZÁLLAPOTAI

A nevelési-oktatási szakasz végére a tanuló:

1. jellemzi az anyag egyes halmazállapotait, annak sajátságait, ismeri
    a halmazállapot-változások jellemzőit, a halmazállapot-változások és
    a hőmérséklet alakulásának kapcsolatát;

2. kísérletezés közben, illetve a háztartásban megfigyeli a folyadékok
    és szilárd anyagok melegítésének folyamatát, és szemléletes képet
    alkot a melegedést kísérő változásokról, a melegedési folyamatot
    befolyásoló tényezőkről;

3. tudja magyarázni a folyadékokban való úszás, lebegés és elmerülés
    jelenségét, az erre vonatkozó sűrűségfeltételt;

4. tisztában van a rugalmasság és rugalmatlanság fogalmával, az erő és
    az általa okozott deformáció közötti kapcsolat jellegével, be tudja
    mutatni az anyag belső szerkezetére vonatkozó legegyszerűbb
    modelleket, kvalitatív jellemzőket.

ELEKTROMOS ÉS MÁGNESES JELENSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az elektromos állapot fogalmát, kialakulását, és magyarázza
    azt az anyagban lévő töltött részecskék és a közöttük fellépő
    erőhatások segítségével;

2. szemléletes képpel rendelkezik az elektromos áramról, ismeri az
    elektromos vezetők és szigetelők fogalmát;

3. használja a feszültség, áramerősség, ellenállás mennyiségeket
    egyszerű áramkörök jellemzésére;

4. tudja, hogy a Földnek mágneses tere van, ismeri ennek legegyszerűbb
    dipól közelítését. Ismeri az állandó mágnes sajátságait, az
    iránytűt;

5. tisztában van a fény egyenes vonalú terjedésével, szabályos
    visszaverődésének törvényével, erre hétköznapi példákat hoz;

6. gyakorlati példákon keresztül ismeri a fény és anyag legelemibb
    kölcsönhatásait (fénytörés, fényvisszaverődés, elnyelés, sugárzás),
    az árnyékjelenségeket, mint a fény egyenes vonalú terjedésének
    következményeit, a fehér fény felbonthatóságát.

A VILÁGEGYETEM

A nevelési-oktatási szakasz végére a tanuló:

1. érti a nappalok és éjszakák változásának fizikai okát,
    megfigyelésekkel feltárja a holdfázisok változásának fizikai
    hátterét, látja a Nap szerepét a Naprendszerben mint gravitációs
    centrum és mint energiaforrás;

2. ismeri a csillagok fogalmát, számuk és méretük nagyságrendjét,
    ismeri a világűr fogalmát, a csillagászati időegységeket (nap,
    hónap, év) és azok kapcsolatát a Föld és Hold forgásával és
    keringésével;

3. tisztában van a galaxisok mibenlétével, számuk és méretük
    nagyságrendjével. Ismeri a Naprendszer bolygóinak fontosabb fizikai
    jellemzőit.

A DIGITÁLIS TECHNOLÓGIÁK HASZNÁLATA

INTERNETHASZNÁLAT, PREZENTÁCIÓKÉSZÍTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. fizikai szövegben, videóban el tudja különíteni a számára világos és
    nem érthető, további magyarázatra szoruló részeket;

2. az internet segítségével adatokat gyűjt a legfontosabb fizikai
    jelenségekről;

3. tanári útmutatás felhasználásával magabiztosan használ magyar nyelvű
    mobiltelefonos, táblagépes applikációkat fizikai tárgyú információk
    keresésére;

4. ismer megbízható fizikai tárgyú magyar nyelvű internetes forrásokat;

5. egyszerű számítógépes prezentációkat készít egy adott témakör
    bemutatására;

6. projektfeladatok megoldása során önállóan, illetve a csoporttagokkal
    közösen különböző prezentációkat hoz létre a tapasztalatok és
    eredmények bemutatására.

MÉRÉS, TERVEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi a sportolást segítő kisalkalmazások által mért fizikai
    adatokat. Méréseket végez a mobiltelefon szenzorainak segítségével.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a helyét a Világegyetemben, látja a Világegyetem időbeli
    > fejlődését, lehetséges jövőjét, az emberiség és a Világegyetem
    > kapcsolatának kulcskérdéseit;

2. tisztában van azzal, hogy a fizika átfogó törvényeket ismer fel,
    > melyek alkalmazhatók jelenségek értelmezésére, egyes események
    > minőségi és mennyiségi előrejelzésére;

3. felismeri, hogyan jelennek meg a fizikai ismeretek a mindennapi
    > tevékenységek során, valamint a gyakran használt technikai
    > eszközök működésében;

4. ismeri a világot leíró legfontosabb természeti jelenségeket, az
    > azokat leíró fizikai mennyiségeket, azok jelentését, jellemző
    > nagyságrendjeit;

5. gyakorlati oldalról ismeri a tudományos megismerési folyamatot:
    > megfigyel, mér, adatait összeveti az egyszerű modellekkel, korábbi
    > ismereteivel. Ennek alapján következtet, megerősít, cáfol;

6. egyszerű fizikai rendszerek esetén a lényeges elemeket a
    > lényegtelenektől el tudja választani, az egyszerűbb számításokat
    > el tudja végezni és a helyes logikai következtetéseket le tudja
    > vonni, illetve táblázatokat, ábrákat, grafikonokat tud értelmezni;

7. tájékozott a Földünket és környezetünket fenyegető globális
    > problémákban, ismeri az emberi tevékenység szerepét ezek
    > kialakulásában;

8. látja a fizikai ismeretek bővülése és a társadalmi-gazdasági
    > folyamatok, történelmi események közötti kapcsolatot;

9. tud önállóan fizikai témájú ismeretterjesztő szövegeket olvasni, a
    > lényeget kiemelni, el tudja különíteni a számára világos, valamint
    > a nem érthető, további magyarázatra szoruló részeket;

10. tudományos ismereteit érveléssel meg tudja védeni, vita során ki
    > tudja fejteni véleményét, érveit és ellenérveit, mérlegelni tudja
    > egy elképzelés tudományos megalapozottságát.

A FIZIKA MINT TERMÉSZETTUDOMÁNYOS MEGISMERÉSI MÓDSZER

FIZIKAI MEGFIGYELÉSEK, KÍSÉRLETEK VÉGZÉSE, AZ EREDMÉNYEK ÉRTELMEZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. egyszerű méréseket, kísérleteket végez, az eredményeket rögzíti;

2. fizikai kísérleteket önállóan is el tud végezni;

3. ismeri a legfontosabb mértékegységek jelentését, helyesen használja
    a mértékegységeket számításokban, illetve az eredmények
    összehasonlítása során;

4. a mérések és a kiértékelés során alkalmazza a rendelkezésre álló
    számítógépes eszközöket, programokat;

5. megismételt mérések segítségével, illetve a mérés körülményeinek
    ismeretében következtet a mérés eredményét befolyásoló tényezőkre;

6. egyszerű, a megértést segítő számolási feladatokat old meg,
    táblázatokat, ábrákat, grafikonokat értelmez, következtetést von le,
    összehasonlít;

7. el tudja választani egyszerű fizikai rendszerek esetén a lényeges
    elemeket a lényegtelenektől;

8. gyakorlati oldalról ismeri a tudományos megismerési folyamatot:
    megfigyelés, mérés, a tapasztalatok, mérési adatok rögzítése,
    rendszerezése, ezek összevetése valamilyen egyszerű modellel vagy
    matematikai összefüggéssel, a modell (összefüggés)
    továbbfejlesztése.

A FIZIKA TUDOMÁNYA, LEGÚJABB EREDMÉNYEI, MÓDSZEREI

A nevelési-oktatási szakasz végére a tanuló:

1. tudja, hogyan születnek az elismert, új tudományos felismerések,
    ismeri a tudományosság kritériumait;

2. tisztában van azzal, hogy a fizika átfogó törvényeket ismer fel,
    melyek alkalmazhatók jelenségek értelmezésére, egyes események
    minőségi és mennyiségi előrejelzésére;

3. felismeri a tudomány által vizsgálható jelenségeket, azonosítani
    tudja a tudományos érvelést, kritikusan vizsgálja egy elképzelés
    tudományos megalapozottságát;

4. ismeri a fizika főbb szakterületeit, néhány új eredményét.

A TUDOMÁNYOS ÉRVELÉS ÉS VITA, AZ EREDMÉNYEK PREZENTÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. kialakult véleményét mérési eredményekkel, érvekkel támasztja alá.

A FIZIKA MŰVELÉSÉNEK TÁRSADALMI VONATKOZÁSAI, KÖRNYEZET- ÉS
TERMÉSZETTUDATOSSÁG

A FIZIKA SZEREPE A KÖRNYEZET MEGÓVÁSÁBAN

A nevelési-oktatási szakasz végére a tanuló:

1. tisztában van a különböző típusú erőművek használatának előnyeivel
    és környezeti kockázatával;

2. érti az atomreaktorok működésének lényegét, a radioaktív hulladékok
    elhelyezésének problémáit;

3. ismeri a környezet szennyezésének leggyakoribb forrásait, fizikai
    vonatkozásait.

AZ ENERGIAGAZDÁLKODÁS FIZIKAI KÉRDÉSEI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a megújuló és a nem megújuló energiaforrások használatának és
    az energia szállításának legfontosabb gyakorlati kérdéseit;

2. az emberiség energiafelhasználásával kapcsolatos adatokat gyűjt, az
    információkat szemléletesen mutatja be;

3. tudja, hogy a Föld elsődleges energiaforrása a Nap, ismeri a
    napenergia felhasználási lehetőségeit, a napkollektor és a napelem
    mibenlétét, a közöttük lévő különbséget.

A GLOBÁLIS PROBLÉMÁK FIZIKAI HÁTTERE

A nevelési-oktatási szakasz végére a tanuló:

1. átlátja az ózonpajzs szerepét a Földet ért ultraibolya sugárzással
    kapcsolatban;

2. tisztában van az éghajlatváltozás kérdésével, az üvegházhatás
    jelenségével a természetben, a jelenség erőssége és az emberi
    tevékenység kapcsolatával;

3. ismeri az űrkutatás történetének főbb fejezeteit, jövőbeli
    lehetőségeit, tervezett irányait;

4. tisztában van az űrkutatás ipari-technikai civilizációra gyakorolt
    hatásával, valamint az űrkutatás tágabb értelemben vett céljaival
    (értelmes élet keresése, új nyersanyagforrások felfedezése).

A FIZIKA ÉS A TÁRSADALMI-GAZDASÁGI FEJLŐDÉS ÖSSZEFÜGGÉSEI

A nevelési-oktatási szakasz végére a tanuló:

1. néhány konkrét példa alapján felismeri a fizika tudásrendszerének
    fejlődése és a társadalmi-gazdasági folyamatok, történelmi események
    közötti kapcsolatot;

2. el tudja helyezni lakóhelyét a Földön, a Föld helyét a
    Naprendszerben, a Naprendszer helyét a galaxisunkban és az
    Univerzumban;

3. átlátja az emberiség és a Világegyetem kapcsolatának kulcskérdéseit.

NEVES FIZIKUSOK ÉLETE, MUNKÁSSÁGA

A nevelési-oktatási szakasz végére a tanuló:

1. adatokat gyűjt és dolgoz fel a legismertebb fizikusok életével,
    tevékenységével, annak gazdasági, társadalmi hatásával, valamint
    emberi vonatkozásaival kapcsolatban (Galileo Galilei, Michael
    Faraday, James Watt, Eötvös Loránd, Marie Curie, Ernest Rutherford,
    Niels Bohr, Albert Einstein, Szilárd Leó, Wigner Jenő, Teller Ede).

A TERMÉSZETI JELENSÉGEK ÉS TECHNIKAI ESZKÖZÖK, TECHNOLÓGIÁK FIZIKÁJA

A LEGÁLTALÁNOSABB TERMÉSZETI JELENSÉGEK FIZIKAI ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a legfontosabb természeti jelenségeket (például légköri
    jelenségek, az égbolt változásai, a vízzel kapcsolatos jelenségek),
    azok megfelelően egyszerűsített, a fizikai mennyiségeken és
    törvényeken alapuló magyarázatait;

2. tudja, hogyan jönnek létre a természet színei, és hogyan észleljük
    azokat;

3. ismeri a villámok veszélyét, a villámhárítók működését, a helyes
    magatartást zivataros, villámcsapás-veszélyes időben;

4. ismeri a légnyomás változó jellegét, a légnyomás és az időjárás
    kapcsolatát.

A GYAKRAN HASZNÁLT TECHNIKAI ESZKÖZÖK, TECHNOLÓGIÁK FIZIKAI ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. érti a legfontosabb közlekedési eszközök -- gépjárművek, légi és
    vízi járművek -- működésének fizikai elveit;

2. átlátja a korszerű lakások és házak hőszabályozásának fizikai
    kérdéseit (fűtés, hűtés, hőszigetelés);

3. ismeri a háztartásban használt fontosabb elektromos eszközöket, az
    elektromosság szerepét azok működésében, szemléletes képe van a
    váltakozó áramról;

4. tisztában van a konyhai tevékenységek (melegítés, főzés, hűtés)
    fizikai vonatkozásaival;

5. átlátja a jelen közlekedése, közlekedésbiztonsága szempontjából
    releváns gyakorlati ismereteket, azok fizikai hátterét;

6. ismeri az egyszerű gépek elvének megjelenését a hétköznapokban,
    mindennapi eszközeinkben;

7. tisztában van az aktuálisan használt világító eszközeink működési
    elvével, energiafelhasználásának sajátosságaival, a korábban
    alkalmazott megoldásokhoz képesti előnyeivel;

8. ismeri a mindennapi életben használt legfontosabb elektromos
    energiaforrásokat, a gépkocsi-, mobiltelefon-akkumulátorok
    legfontosabb jellemzőit;

9. ismeri az elektromágneses hullámok szerepét az információ- (hang-,
    kép-) átvitelben, ismeri a mobiltelefon legfontosabb tartozékait
    (SIM kártya, akkumulátor stb.), azok kezelését, funkcióját;

10. ismeri a digitális fényképezőgép működésének elvét;

11. tisztában van az elektromágneses hullámok frekvenciatartományaival,
    a rádióhullámok, mikrohullámok, infravörös hullámok, a látható fény,
    az ultraibolya hullámok, a röntgensugárzás, a gamma-sugárzás
    gyakorlati felhasználásával;

12. tisztában van az elektromos áram veszélyeivel, a veszélyeket
    csökkentő legfontosabb megoldásokkal (gyerekbiztos csatlakozók,
    biztosíték, földvezeték szerepe);

13. ismeri az elektromos fogyasztók használatára vonatkozó
    balesetvédelmi szabályokat;

14. ismeri az elektromos hálózatok kialakítását a lakásokban,
    épületekben, az elektromos kapcsolási rajzok használatát;

15. érti a generátor, a motor és a transzformátor működési elvét,
    gyakorlati hasznát.

AZ EGÉSZSÉGES ÉLETMÓD FIZIKAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az emberi hangérzékelés fizikai alapjait, a hang mint hullám
    jellemzőit, keltésének eljárásait;

2. átlátja a húros hangszerek és a sípok működésének elvét, az
    ultrahang szerepét a gyógyászatban, ismeri a zajszennyezés fogalmát;

3. ismeri az emberi szemet mint képalkotó eszközt, a látás
    mechanizmusát, a gyakori látáshibák (rövid- és távollátás) okát, a
    szemüveg és a kontaktlencse jellemzőit, a dioptria fogalmát;

4. ismeri a radioaktív izotópok néhány orvosi alkalmazását
    (nyomjelzés);

5. tisztában van az elektromos áram élettani hatásaival, az emberi test
    áramvezetési tulajdonságaival, az idegi áramvezetés jelenségével;

6. ismeri a szervezet energiaháztartásának legfontosabb tényezőit, az
    élelmiszerek energiatartalmának szerepét;

7. átlátja a gyakran alkalmazott orvosdiagnosztikai vizsgálatok,
    illetve egyes kezelések fizikai megalapozottságát, felismeri a
    sarlatán, tudományosan megalapozatlan kezelési módokat.

FIZIKAI SZAKISMERETEK

MOZGÁSOK A KÖRNYEZETÜNKBEN

A nevelési-oktatási szakasz végére a tanuló:

1. helyesen használja az út, a pálya és a hely fogalmát, valamint a
    sebesség, átlagsebesség, pillanatnyi sebesség, gyorsulás, elmozdulás
    fizikai mennyiségeket a mozgás leírására;

2. tud számításokat végezni az egyenes vonalú egyenletes mozgás
    esetében: állandó sebességű mozgások esetén a sebesség ismeretében
    meghatározza az elmozdulást, a sebesség nagyságának ismeretében a
    megtett utat, a céltól való távolság ismeretében a megérkezéshez
    szükséges időt;

3. ismeri a szabadesés jelenségét, annak leírását, tud esésidőt
    számolni, mérni, becsapódási sebességet számolni;

4. egyszerű számításokat végez az állandó gyorsulással mozgó testek
    esetében;

5. ismeri a periodikus mozgásokat (ingamozgás, rezgőmozgás) jellemző
    fizikai mennyiségeket, néhány egyszerű esetben tudja mérni a
    periódusidőt, megállapítani az azt befolyásoló tényezőket;

6. ismeri az egyenletes körmozgást leíró fizikai mennyiségeket
    (pályasugár, kerületi sebesség, fordulatszám, keringési idő,
    centripetális gyorsulás), azok jelentését, egymással való
    kapcsolatát;

7. érti, hogyan alakulnak ki és terjednek a mechanikai hullámok, ismeri
    a hullámhossz és a terjedési sebesség fogalmát;

8. egyszerű esetekben kiszámolja a testek lendületének nagyságát,
    meghatározza irányát;

9. egyszerűbb esetekben alkalmazza a lendület-megmaradás törvényét,
    ismeri ennek általános érvényességét;

10. tisztában van az erő mint fizikai mennyiség jelentésével,
    mértékegységével, ismeri a newtoni dinamika alaptörvényeit,
    egyszerűbb esetekben alkalmazza azokat a gyorsulás meghatározására,
    a korábban megismert mozgások értelmezésére;

11. egyszerűbb esetekben kiszámolja a mechanikai kölcsönhatásokban
    fellépő erőket (nehézségi erő, nyomóerő, fonálerő, súlyerő,
    súrlódási erők, rugóerő), meghatározza az erők eredőjét;

12. ismeri a bolygók, üstökösök mozgásának jellegzetességeit;

13. tudja, mit jelentenek a kozmikus sebességek (körsebesség, szökési
    sebesség);

14. érti a testek súlya és a tömege közötti különbséget, a súlytalanság
    állapotát, a gravitációs mező szerepét a gravitációs erő
    közvetítésében;

15. érti a tömegvonzás általános törvényét, és azt, hogy a gravitációs
    erő bármely két test között hat.

AZ ENERGIA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a mechanikai munka fogalmát, kiszámításának módját,
    mértékegységét, a helyzeti energia, a mozgási energia, a rugalmas
    energia, a belső energia fogalmát;

2. konkrét esetekben alkalmazza a munkatételt, a mechanikai energia
    megmaradásának elvét a mozgás értelmezésére, a sebesség
    kiszámolására;

3. néhány egyszerűbb, konkrét esetben (mérleg, libikóka) a
    forgatónyomatékok meghatározásának segítségével vizsgálja a testek
    egyensúlyi állapotának feltételeit, összeveti az eredményeket a
    megfigyelések és kísérletek tapasztalataival.

AZ ANYAG ÉS HALMAZÁLLAPOTAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a Celsius- és az abszolút hőmérsékleti skálát, a gyakorlat
    szempontjából nevezetes néhány hőmérsékletet, a termikus
    kölcsönhatás jellemzőit;

2. gyakorlati példákon keresztül ismeri a hővezetés, hőáramlás és
    hősugárzás jelenségét, a hőszigetelés lehetőségeit, ezek
    anyagszerkezeti magyarázatát;

3. értelmezi az anyag viselkedését hőközlés során, tudja, mit jelent az
    égéshő, a fűtőérték és a fajhő;

4. tudja a halmazállapot-változások típusait (párolgás, forrás,
    lecsapódás, olvadás, fagyás, szublimáció);

5. tisztában van a halmazállapot-változások energetikai viszonyaival,
    anyagszerkezeti magyarázatával, tudja, mit jelent az olvadáshő,
    forráshő, párolgáshő, egyszerű számításokat végez a
    halmazállapot-változásokat kísérő hőközlés meghatározására;

6. ismeri a hőtágulás jelenségét, jellemző nagyságrendjét;

7. ismeri a hőtan első főtételét, és tudja alkalmazni néhány egyszerűbb
    gyakorlati szituációban (palackba zárt levegő, illetve állandó
    nyomású levegő melegítése);

8. tisztában van a megfordítható és nem megfordítható folyamatok
    közötti különbséggel;

9. ismeri a víz különleges tulajdonságait (rendhagyó hőtágulás, nagy
    olvadáshő, forráshő, fajhő), ezek hatását a természetben, illetve
    mesterséges környezetünkben;

10. ismeri az időjárás elemeit, a csapadékformákat, a csapadékok
    kialakulásának fizikai leírását;

11. ismeri a nyomás, hőmérséklet, páratartalom fogalmát, a levegő mint
    ideális gáz viselkedésének legfontosabb jellemzőit. Egyszerű
    számításokat végez az állapothatározók megváltozásával kapcsolatban;

12. tisztában van a repülés elvével, a légellenállás jelenségével;

13. ismeri a hidrosztatika alapjait, a felhajtóerő fogalmát, hétköznapi
    példákon keresztül értelmezi a felemelkedés, elmerülés, úszás,
    lebegés jelenségét, tudja az ezt meghatározó tényezőket, ismeri a
    jelenségkörre épülő gyakorlati eszközöket.

ELEKTROMOS ÉS MÁGNESES JELENSÉGEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az elektrosztatikus alapjelenségeket (dörzselektromosság,
    töltött testek közötti kölcsönhatás, földelés), ezek gyakorlati
    alkalmazásait;

2. átlátja, hogy az elektromos állapot kialakulása a töltések
    egyenletes eloszlásának megváltozásával van kapcsolatban;

3. érti Coulomb törvényét, egyszerű esetekben alkalmazza elektromos
    töltéssel rendelkező testek közötti erő meghatározására;

4. tudja, hogy az elektromos kölcsönhatást az elektromos mező
    közvetíti;

5. tudja, hogy az áram a töltött részecskék rendezett mozgása, és ez
    alapján szemléletes elképzelést alakít ki az elektromos áramról;

6. gyakorlati szinten ismeri az egyenáramok jellemzőit, a feszültség,
    áramerősség és ellenállás fogalmát;

7. érti Ohm törvényét, egyszerű esetekben alkalmazza a feszültség,
    áramerősség, ellenállás meghatározására, tudja, hogy az ellenállás
    függ a hőmérséklettől;

8. ki tudja számolni egyenáramú fogyasztók teljesítményét, az általuk
    felhasznált energiát;

9. ismeri az egyszerű áramkör és egyszerűbb hálózatok alkotórészeit,
    felépítését;

10. értelmezni tud egyszerűbb kapcsolási rajzokat, ismeri kísérleti
    vizsgálatok alapján a soros és a párhuzamos kapcsolások legfontosabb
    jellemzőit;

11. elektromágnes készítése közben megfigyeli és alkalmazza, hogy az
    elektromos áram mágneses mezőt hoz létre;

12. megmagyarázza, hogyan működnek az általa megfigyelt egyszerű
    felépítésű elektromos motorok: a mágneses mező erőt fejt ki az
    árammal átjárt vezetőre;

13. ismeri az elektromágneses indukció jelenségének lényegét, fontosabb
    gyakorlati vonatkozásait, a váltakozó áram fogalmát.

AZ ATOMOK ÉS A FÉNY

A nevelési-oktatási szakasz végére a tanuló:

1. tudja, hogy a fény elektromágneses hullám, és hogy terjedéséhez nem
    kell közeg;

2. ismeri az elektromágneses hullámok jellemzőit (frekvencia,
    hullámhossz, terjedési sebesség), azt, hogy milyen körülmények
    határozzák meg ezeket. A mennyiségek kapcsolatára vonatkozó egyszerű
    számításokat végez;

3. ismeri a színek és a fény frekvenciája közötti kapcsolatot, a fehér
    fény összetett voltát, a kiegészítő színek fogalmát, a szivárvány
    színeit;

4. ismeri a fénytörés és visszaverődés törvényét, megmagyarázza, hogyan
    alkot képet a síktükör;

5. a fókuszpont fogalmának felhasználásával értelmezi, hogyan térítik
    el a fényt a domború és homorú tükrök, a domború és homorú lencsék;

6. ismeri az optikai leképezés fogalmát, a valódi és látszólagos kép
    közötti különbséget. Egyszerű kísérleteket tud végezni tükrökkel és
    lencsékkel;

7. megfigyeli a fényelektromos jelenséget, tisztában van annak Einstein
    által kidolgozott magyarázatával, a frekvencia (hullámhossz) és a
    foton energiája kapcsolatával;

8. ismeri Rutherford szórási kísérletét, mely az atommag felfedezéséhez
    vezetett;

9. ismeri az atomról alkotott elképzelések változásait, a
    Rutherford-modellt és Bohr-modellt, látja a modellek hiányosságait;

10. megmagyarázza az elektronmikroszkóp működését az elektron
    hullámtermészetének segítségével;

11. átlátja, hogyan használják a vonalas színképet az anyagvizsgálat
    során;

12. ismeri az atommag felépítését, a nukleonok típusait, az izotóp
    fogalmát, a nukleáris kölcsönhatás jellemzőit;

13. átlátja, hogy a maghasadás és magfúzió miért alkalmas
    energiatermelésre, ismeri a gyakorlati megvalósulásuk lehetőségeit,
    az atomerőművek működésének alapelvét, a csillagok
    energiatermelésének lényegét;

14. ismeri a radioaktív sugárzások típusait, az alfa-, béta- és
    gamma-sugárzások leírását és tulajdonságait;

15. ismeri a felezési idő, aktivitás fogalmát, a sugárvédelem
    lehetőségeit.

A VILÁGEGYETEM

A nevelési-oktatási szakasz végére a tanuló:

1. megvizsgálja a Naprendszer bolygóin és holdjain uralkodó, a Földétől
    eltérő fizikai környezet legjellemzőbb példáit, azonosítja ezen
    eltérések okát, a legfontosabb esetekben megmutatja, hogyan
    érvényesülnek a fizika törvényei a Föld és a Hold mozgása során;

2. szabad szemmel vagy távcsővel megfigyeli a Holdat, a Hold
    felszínének legfontosabb jellemzőit, a holdfogyatkozás jelenségét, a
    látottakat fizikai ismeretei alapján értelmezi;

3. ismeri a Nap, mint csillag legfontosabb fizikai tulajdonságait, a
    Nap várható jövőjét, a csillagok lehetséges fejlődési folyamatait;

4. átlátja és szemlélteti a természetre jellemző fizikai mennyiségek
    nagyságrendjeit (atommag, élőlények, Naprendszer, Univerzum);

5. a legegyszerűbb esetekben azonosítja az alapvető fizikai
    kölcsönhatások és törvények szerepét a Világegyetem felépítésében és
    időbeli változásaiban.

A DIGITÁLIS TECHNOLÓGIÁK HASZNÁLATA

FIZIKAI ADATBÁZISOK, INTERAKTÍV SZIMULÁCIÓK HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. használ helymeghatározó szoftvereket, a közeli és távoli
    környezetünket leíró adatbázisokat, szoftvereket;

2. a vizsgált fizikai jelenségeket, kísérleteket bemutató animációkat,
    videókat keres és értelmez.

AZ INTERNET KRITIKUS HASZNÁLATA FIZIKAI INFORMÁCIÓK GYŰJTÉSÉRE

A nevelési-oktatási szakasz végére a tanuló:

1. ismer magyar és idegen nyelvű megbízható fizikai tárgyú honlapokat;

2. készségszinten alkalmazza a különböző kommunikációs eszközöket,
    illetve az internetet a főként magyar, illetve idegen nyelvű,
    fizikai tárgyú tartalmak keresésére;

3. fizikai szövegben, videóban el tudja különíteni a számára világos,
    valamint nem érthető, további magyarázatra szoruló részeket;

4. az interneten talált tartalmakat több forrásból is ellenőrzi.

SZÁMÍTÓGÉPES PREZENTÁCIÓK KÉSZÍTÉSE FIZIKAI INFORMÁCIÓK BEMUTATÁSÁRA,
MEGOSZTÁSÁRA

A nevelési-oktatási szakasz végére a tanuló:

1. a forrásokból gyűjtött információkat számítógépes prezentációban
    mutatja be;

2. az egyszerű vizsgálatok eredményeinek, az elemzések, illetve a
    következtetések bemutatására prezentációt készít;

3. a projektfeladatok megoldása során önállóan, illetve a
    csoporttagokkal közösen különböző médiatartalmakat, prezentációkat,
    rövidebb-hosszabb szöveges produktumokat hoz létre a tapasztalatok,
    eredmények, elemzések, illetve következtetések bemutatására.

MÉRÉSI ADATOK SZÁMÍTÓGÉPES KIÉRTÉKELÉSE, TÁBLÁZATOK, GRAFIKONOK
KÉSZÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a vizsgálatok során kinyert adatokat egyszerű táblázatkezelő
    szoftver segítségével elemzi, az adatokat grafikonok segítségével
    értelmezi;

2. használ mérésre, adatelemzésre, folyamatelemzésre alkalmas összetett
    szoftvereket (például hang és mozgókép kezelésére alkalmas
    programokat).

***II.3.6.5. KÉMIA***

***A) ALAPELVEK, CÉLOK***

A kémia olyan ismeretek, módszerek, eljárások összessége, amelyek
ismerete és értő alkalmazása nélkül nem lenne fenntartható az életünk. A
kémia által tervezett, fejlesztett molekulák, anyagok teremtik meg az
alapját pl. az informatikai lehetőségeknek, épített környezetünk
kialakításának, az emberi élet és annak minősége biztosításának. A kémia
oktatása során hangsúlyozni kell az új, izgalmas, az emberi élet
minőségét javító anyagok felfedezésének, kifejlesztésének célját, utalni
kell az orvosbiológiai alkalmazásokra, az űrkutatásra, az elektronikus
eszközökben felhasználható új anyagok kialakítására, hangsúlyozni kell a
gyógyszerkutatás, a gyógyszeripar és a vegyipar szerepét.

Mély kémiai ismeretekre van szükség a gazdaság számos kiemelt fontosságú
területén: például a vegyiparban, a gyógyszeriparban, az
atomenergia-termelésben, az egészségügyben, a környezetvédelemben, a
minőségbiztosításban vagy éppen a biotechnológiában.

A kémiai ismeretek az anyag tulajdonságaiban bekövetkező változásokhoz,
illetve az ezen változásokat alapvetően meghatározó hatásokhoz
kapcsolódnak (összetétel, szerkezet, energia, kémiai reakció).
Alapvetően azok az elvek, törvények tartoznak ide, amelyek az anyag- és
energia-megmaradással foglalkoznak, a változásokat kísérő
atomszerkezethez köthető jelenségek leírását tartalmazzák. A kémia
tárgyalásmódjára jellemző, hogy az összetétel és a szerkezet alapvetően
meghatározza a tulajdonságokat, illetve az anyagok tulajdonságaiból
következtethetünk a szerkezetre, esetleg az összetételre is. Az anyagot
felépítő részecskék (atomok, ionok, molekulák) egymáshoz viszonyított
elhelyezkedése (halmazszerkezet) és a köztük kialakuló kölcsönhatások
határozzák meg az anyagok jellemzőit.

A kémia tanulási-tanítási folyamatában alapvető szerepe van a tanuló
számára releváns problémák, életszerű helyzetek során megjelenő kémiai
vonatkozások megismerésének, amit a tanuló aktív közreműködésével,
egyszerű -- akár otthon is elvégezhető -- kísérletek tervezésével,
végrehajtásával, megfigyelésével és elemzésével célszerű elérni. Az
élményszerű, a tanuló gondolkodásához, problémáihoz közel álló ún.
kontextusalapú tananyag-feldolgozás -- különösen a 7--8. évfolyamon --
hatékonyabb, mivel az ismeretek rendszerezésével zárul.

A kémia az anyagok tulajdonságainak és változásainak feltárása és
leírása mellett a változások mennyiségi viszonyaival is foglalkozik,
ezért a kémia tanulása során az alapvető mennyiségekkel való ismerkedés
is a tananyag részét képezi.

A kémia tananyaga mindenkihez szól, nem csak azokhoz, akik vegyészek
vagy természettudósok akarnak lenni. Szervesen kötődik a hétköznapi
élethez és hangsúlyozottan élmény- és alkalmazásközpontú. Feltárja a
kémia társadalmunkban és az egyén életében betöltött szerepét. Nem
tartalmaz túlzottan sok, a tanulók számára csak nehezen megérthető és
feldolgozható fogalmat, ismeretet. Az összefüggések megértésére és a
természettudományos gondolkodásmód kialakítására fekteti a hangsúlyt.
Hagy időt az elmélyült feldolgozásra, az esetleges megértési problémák
megbeszélésére, tekintettel van az információfeldolgozás
memóriakapacitására, a kognitív terhelésre. A részecske- és
szimbólumszintű fogalmak nem túl korai bevezetése a tananyag megértését
szolgálja. A képletek és egyenletek gyakoroltatása pont akkora hangsúlyt
kap, hogy a tanuló a mindennapokban előforduló jelöléseket képes legyen
értelmezni. Figyel a megfelelő nyelvhasználatra és a kommunikációra.
Célja a fogalmi megértés, valódi problémamegoldást kínál, és nem az
információk megtanítására törekszik. Az algoritmikus problémák helyett
inkább nyílt végű és játékos feladatokat használ. Előnyben részesíti az
életszerű kémiai problémák aktív tanulással, lehetőleg csoportmunkában
történő feldolgozását.

Megfelelően használja a kémiai kísérleteket, melyeknek mindig világos a
célja, és elsősorban a fogalmi megértést fejlesztik, és csak másodsorban
a manuális készségeket. A felsőbb évfolyamokon hangsúlyozza a kísérleti
problémamegoldás lépéseit, különös tekintettel a várható eredmény
becslésére (hipotézisalkotásra). Az ellenőrzés során döntően a
megértést, és nem a visszamondást, a gondolkodást, és nem a memorizálást
méri.

A kémia tanítása is hozzájárul a Nat bevezetőjében megfogalmazott
tanulási és nevelési célok eléréséhez.

A testi-lelki egészségre nevelést segíti elő a helyes táplálkozás,
valamint a káros szenvedélyek kémiai alapjainak megismerése. Az
önismeret és az együttműködő képesség fejlesztését szolgálják -- többek
között -- azok a kooperatív tevékenységek, amelyekkel a kémia néhány
anyagrészét dolgozzák fel a tanulók. A kémiai információk gyűjtése,
rendszerbe foglalása és bemutatása fejleszti a kommunikációs kultúrát és
a médiahasználatot. A kémiával kapcsolatos pályán dolgozó kutatók és
mérnökök munkájának a megismerése hozzájárul a tudatos
életpálya-tervezéshez. A híres magyar kémikusok és kémiai Nobel-díjasok
bemutatása erősíti a tanulók nemzeti és európai azonosságtudatát,
hazaszeretetét. Az emberiség néhány globális problémája
(éghajlatváltozás, víz-, levegő- és talajszennyezés) kémiai
vonatkozásának tárgyalása hozzájárul a tanulóknak a fenntartható jelen
és jövő iránti elkötelezettségének kialakításához és megerősítéséhez.

A kémia oktatása a 7. évfolyamon kezdődik. A 7--8. évfolyamon -- a helyi
tantervben szabályozott módon -- önálló tárgyként vagy a
természettudomány integrált tantárgy részeként, mint kémia modul is
oktatható. Speciális esetben az integrált tanulásszervezésre a
középiskolai szakaszban is lehetőség van.

A kémia tanulásának célja, hogy a tanuló:

1. érdeklődését felkeltse a környezetben zajló fizikai és kémiai
    változások okai, magyarázata, komplexitása, elméleti háttere iránt;

2. ismerje a mindennapi életben előforduló alapvető vegyülettípusokat,
    legyen tisztában alapvető kémiai fogalmakkal, jelenségekkel és
    reakciókkal, legyen anyagismerete;

3. önálló ismeretszerzési, illetve összefüggés-felismerési készségei
    fejlődjenek a kísérletek, laboratóriumi vizsgálatok, nyomtatott vagy
    digitális információforrások önálló vagy csoportban történő elemzése
    révén, ami megalapozza az értő, önálló munkavégzés lehetőségét;

4. problémaorientált, elemző és mérlegelő gondolkodása alakuljon ki,
    ami nélkülözhetetlen az információs társadalomra jellemző hír- és
    információdömpingben történő eligazodáshoz, a felelős és tudatos
    állampolgári szerepvállaláshoz;

5. tanulmányozza a természetben lejátszódó folyamatokat, valamint
    átgondolja a várható következményeket, cselekedni képes, a
    környezetért felelősséggel tenni akaró magatartást alakítson ki,
    ezzel is hangsúlyozva, hogy az ember egyénként és egy nagyobb
    közösség részeként egyaránt felelős a természeti környezetéért,
    annak jövőbeni állapotáért, felismeri és megérti, hogy a
    környezettudatos, a fenntarthatóságot szem előtt tartó gondolkodás
    az élhető jövő záloga;

6. a köznapi életben használt vegyi anyagok és az azokkal végzett
    felelősségteljes munka alapvető ismereteinek elsajátítása mellett
    tanulja meg a mindennapi életben hasznosítható kémiai ismereteket,
    és alakuljon ki benne az értő, felelős döntési képesség készsége.

***A tantárgy tanításának specifikus jellemzői a 7--8. évfolyamon***

A kémia oktatása a 7. évfolyamon kezdődik. A 7--8. évfolyamon -- a helyi
tantervben szabályozott módon -- önálló tárgyként vagy a
természettudomány integrált tantárgy részeként, mint kémia modul is
oktatható.

Az általános iskolai kémiai ismeretek tanításának célja egyrészt a
természettudományok iránti érdeklődés felkeltése, a természettudományos
szemléletmód kialakításának megkezdése, valamint a kémia a társadalom és
az egyén életében betöltött szerepének bemutatása. E célokat a tanuló
számára releváns problémák, életszerű helyzetek kémiai vonatkozásainak
tárgyalásával, a tanuló aktív közreműködésével, egyszerű -- akár otthon
is elvégezhető -- kísérletek tervezésével, végrehajtásával,
megfigyelésével és elemzésével kell elérni. Az érdeklődés felkeltése és
fenntartása érdekében célszerű a kémia tanítását, valamint az egyes
témakörök tárgyalását is a tanuló számára érdekes problémák, kísérletek
tanulmányozásával és megbeszélésével kezdeni.

Ebben a szakaszban kezdődik el a részecskeszemlélet kialakítása, a
tudományos ismeretek és a hétköznapi tapasztalatokon alapuló naiv
elméletek ütköztetése is. Kiemelt figyelmet kell szentelni a tanuló
gondolkodásának megismerésére, a fogalmi megértési problémák
feltárására, a metafogalmi tudás kialakítására.

A kémiatanítás ezen szakaszának fő célja a környezetünkben található
legfontosabb anyagok, folyamatok és jelenségek megismerése. Olyan
ismeretek és kompetenciák biztosítása, amelyek segítik a tanulók
mindennapi életét, ugyanakkor biztosítják a természettudományos
gondolkodás továbbfejlesztésének és a továbbtanulásnak a lehetőségét is.

***A tantárgy tanításának specifikus jellemzői a 9--10. évfolyamon***

A középiskolai kémiai ismeretek tanításának célja egyrészt a
természettudományos szemléletmód továbbfejlesztése, a különböző
tantárgyakban tanult ismeretek természettudományos műveltséggé történő
integrálása, másrészt az elvontabb kémiai ismeretek, fogalmak
feldolgozása, a kémiát továbbtanulásra választó tanulók ismereteinek
megalapozása.

A természettudományos műveltség kialakítását olyan komplex problémák
tárgyalásával lehet elősegíteni, melyek megoldása igényli a kémiai,
fizikai, biológiai és természetföldrajzi ismeretek integrálását. Ilyenek
lehetnek például: a megújuló és nem megújuló energiahordozók
felhasználása; a víz, a talaj és a levegő szennyezése, tisztítása; a
hulladékkezelés és hulladékhasznosítás; ételeink és italaink;
gyógyszerek és „csodaszerek".

A gimnáziumi kémiatanulás hozzájárul ahhoz, hogy a fizika, kémia,
biológia és földrajz tantárgyak által közvetített tartalmak egységes
természettudományos műveltséggé rendeződjenek. 14--16 éves korban a
tanuló szellemileg és érzelmileg is nagyon fogékony a környezeti
kérdésekre. Már kezdi átlátni a világot, érzékeli és érti az
ellentmondásos helyzeteket, erős a kritikai érzéke, és érzelmileg,
értelmileg is nagyon nyitott. Fontos cél és egyben lehetőség a
gimnáziumi környezeti nevelés érdekében a biológia, a földrajz, a fizika
és a kémia tárgyak ismeretanyagának beépítése a rokon tantárgyak
ismeretrendszerébe. Komoly eredményeket lehet így elérni a környezeti
nevelés terén a tanuló világképe, környezetszemlélete, értékrendje és
mindennapi szokásai tekintetében is.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 7--8. ÉVFOLYAMON***

A javasolt kontextusalapú tananyag-felépítés nagyfokú rugalmasságot tesz
lehetővé. Így a kémiai ismeretek feldolgozása mind diszciplináris, mind
integrált oktatás formájában megvalósítható.

1. A kísérleti megfigyeléstől a modellalkotásig

2. Az anyagi halmazok

3. Atomok, molekulák, ionok

4. Kémiai reakciók

5. Kémia a természetben

6. Kémia a mindennapokban

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

A 9--10. évfolyamra is javasolt kontextusalapú tananyag-felépítés
nagyfokú rugalmasságot tesz lehetővé. A tartalmi elemek a következő
témakörök köré szerveződnek:

1. Az anyagok szerkezete és tulajdonságai

2. A kémiai átalakulások

3. A szén egyszerű szerves vegyületei

4. Az életműködések kémiai alapjai

5. Elemek és szervetlen vegyületeik

6. Kémia az ipari termelésben és a mindennapokban

7. Környezeti kémia és környezetvédelem

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 7--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. tanári segítséggel vagy csoportban el tud végezni egyszerű kémiai
    kísérleteket, és precízen meg tudja figyelni és lejegyezni a
    tapasztalatait;

2. alkalmazza a természettudományos problémamegoldás lépéseit egyszerű
    kémiai problémák megoldásában;

3. képes analógiás és mérlegelő gondolkodásra kémiai kontextusban;

4. tudja használni az internetet kémiai információk keresésére, képes
    számítógépes prezentáció formájában kémiával kapcsolatos eredmények,
    információk bemutatására, megosztására;

5. képes mobiltelefon, táblagép segítségével a vizsgálatait fénykép,
    illetve videófelvétel formájában dokumentálni;

6. tudja használni a részecskemodellt az anyagok tulajdonságainak
    értelmezésére;

7. ismeri a kémiának az egyén és a társadalom életében betöltött
    szerepét, ezt konkrét példákkal tudja alátámasztani;

8. tisztában van a háztartásban leggyakrabban előforduló anyagok
    felhasználásának előnyeivel és veszélyeivel, a biztonságos
    vegyszerhasználat szabályaival;

9. ismeri az alapvető laboratóriumi vegyszereket, azok tulajdonságait
    és felhasználását;

10. ismeri a környezetében végbemenő alapvető folyamatok tudományos
    hátterét.

A KÉMIA MINT TERMÉSZETTUDOMÁNYOS MEGISMERÉSI MÓDSZER

A KÉMIAI KÍSÉRLET MINT MEGISMERÉSI MÓDSZER

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti a kísérletet, a tapasztalatot és a magyarázatot;

2. megismeri egy egyszerű laboratórium felépítését, anyagait és
    eszközeit;

3. megismeri néhány köznapi anyag legfontosabb tulajdonságait és az
    anyagok vizsgálatának egyszerű módszereit.

A TERMÉSZETTUDOMÁNYOS GONDOLKODÁS ALAPVETŐ MŰVELETEI

A nevelési-oktatási szakasz végére a tanuló:

1. tudja és érti, hogy közkeletű hiedelmeket nem szabad tényeknek
    tekinteni;

2. tudja és érti, hogy a hétköznapi, a mindennapi tapasztalatokon
    alapuló gondolkodás nem elégséges a tudományos problémák
    megoldásához;

3. tudja és érti, hogy attól még, hogy egy elem vagy vegyület
    mesterségesen került előállításra vagy természetes úton került
    kinyerésre, még ugyanolyan tulajdonságai vannak, ugyanannyira lehet
    veszélyes vagy veszélytelen, mérgező vagy egészséges.

MENNYISÉGI GONDOLKODÁS A KÉMIÁBAN: A SZTÖCHIOMETRIA ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a természettudományos vizsgálatok során alkalmazott
    legfontosabb mennyiségeket és azok kapcsolatát.

A KÉMIA TÁRSADALMI VONATKOZÁSAI, KÖRNYEZET- ÉS TERMÉSZETTUDATOSSÁG

KÖRNYEZETVÉDELEM: A TERMÉSZETI ÉS ÉPÍTETT KÖRNYEZETRE VESZÉLYES ANYAGOK

A nevelési-oktatási szakasz végére a tanuló:

1. megérti és példákkal szemlélteti az emberi tevékenység és a
    természeti környezet kölcsönös kapcsolatát kémiai szempontok
    alapján;

2. kiselőadás vagy projektmunka keretében ismerteti a háztartási
    hulladék összetételét, felhasználásának és csökkentésének
    lehetőségeit, különös figyelemmel a veszélyes hulladékokra;

3. kiselőadás keretében beszámol egy a saját települését érintő
    környezetvédelmi kérdés kémiai vonatkozásairól;

4. konkrét lépéseket tesz a saját maga részéről annak érdekében, hogy
    mérsékelje a környezetszennyezést (pl. energiatakarékosság,
    szelektív hulladékgyűjtés, tudatos vásárlás);

5. azonosítja és példát hoz fel a környezetében előforduló leggyakoribb
    levegőt, vizet és talajt szennyező forrásokra;

6. ismeri természeti környezetének, azon belül a légkörnek, a
    kőzetburoknak, a természetes vizeknek és az élővilágnak a
    legalapvetőbb anyagait;

7. tisztában van azzal, hogy a bennünket körülvevő anyagokat a
    természetben található anyagokból állítjuk elő.

GLOBÁLIS PROBLÉMÁK KÉMIAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. érti a globális klímaváltozás, a savas esők, az ózonréteg
    károsodásának, valamint a szmogoknak a kialakulását és emberiségre
    gyakorolt hatását.

KÉMIA A MINDENNAPOKBAN

A TÁPLÁLKOZÁS, EGÉSZSÉGVÉDELEM KÉMIAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. tisztában van vele, hogy az életfolyamatainkhoz szükséges anyagokat
    a táplálékunkból vesszük fel zsírok, fehérjék, szénhidrátok, ásványi
    sók és vitaminok formájában;

2. tud érvelni a változatos táplálkozás és az egészséges életmód
    mellett;

3. képes a forgalomban lévő kemikáliák (növényvédő szerek, háztartási
    mosó- és tisztítószerek) címkéjén feltüntetett használati útmutató
    értelmezésére, az azok felelősségteljes használatára.

KÉMIAI SZAKISMERETEK

A KÉMIA JELRENDSZERE

A nevelési-oktatási szakasz végére a tanuló:

1. szöveges leírás vagy kémiai szimbólum alapján megkülönbözteti az
    atomokat, a molekulákat és ionokat;

2. ismeri a legfontosabb elemek vegyjelét, illetve vegyületek képletét.

A RÉSZECSKEMODELL

A nevelési-oktatási szakasz végére a tanuló:

1. egyszerű modelleket (golyómodellt) használ az anyagot felépítő
    kémiai részecskék modellezésére;

2. ismeri a halmazállapot-változásokat, konkrét példát mond a
    természetből (légköri jelenségek) és a mindennapokból;

3. különbséget tesz elemi részecske és kémiai részecske, valamint atom,
    molekula és ion között;

4. tudja, hogy az atom atommagból és elektronburokból épül fel;

5. fel tudja írni a kisebb atomok elektronszerkezetét a héjakon lévő
    elektronok számával (Bohr-féle atommodell);

6. tudja, hogy az atom külső elektronjainak jut fontos szerep a
    molekula- és ionképzés során;

7. érti egyszerű molekulák kialakulását (H2, Cl2, O2, N2, H2O, HCl,
    CH4, CO2) és fel tudja írni a képletüket;

8. érti az egyszerű ionok kialakulását (Na+, K+, Mg2+, Ca2+, Al3+, Cl-,
    O2-) és analógiás gondolkodással következtet az egy oszlopban
    található elemekből képződő ionok képletére;

9. érti az ionvegyületek képletének megállapítását;

10. ismeri a köznapi anyagok molekula- és halmazszerkezetét (hidrogén,
    oxigén, nitrogén, víz, metán, szén-dioxid, gyémánt, grafit, vas,
    réz, nátrium-klorid);

11. érti, hogy az atomok és ionok között jellemzően erősebb, a molekulák
    között gyengébb kémiai kötések alakulhatnak ki;

12. tudja, hogy melyek az anyag fizikai tulajdonságai;

13. a részecskemodell alapján értelmezi az oldódást;

14. részecskeszemlélettel értelmezi az oldódás folyamatát és az oldatok
    összetételét;

15. példát mond a valódi oldatra és a kolloid oldatra;

16. tudja, hogy a keverékek alkotórészeit az alkotórészek egyedi
    tulajdonságai alapján választhatjuk szét egymástól, ismer konkrét
    példákat az elválasztási műveletekre (pl. bepárlás, szűrés,
    ülepítés);

17. a részecskemodell alapján értelmezi az egyszerű kémiai reakciókat;

18. ismeri a kémiai reakciók végbemenetelének legalapvetőbb feltételeit
    (ütközés, energia);

19. ismeri a katalizátor fogalmát, érti a katalizátorok működési elvének
    a lényegét;

20. ismeri a köznapi élet szempontjából legalapvetőbb kémiai reakciókat
    (pl. égési reakciók, egyesülések, bomlások, savak és bázisok
    reakciói, fotoszintézis);

21. ismer sav-bázis indikátorokat, érti felhasználásuk jelentőségét;

22. ismeri a korrózió fogalmát és a fémek csoportokba sorolását
    korrózióállóságuk alapján, érti a vas korróziójának lényegét,
    valamint korrózióvédelmi módjait.

ELEMEK ÉS VEGYÜLETEK

A nevelési-oktatási szakasz végére a tanuló:

1. különbséget tesz elem, vegyület és keverék között;

2. képes egyszerű kísérletek elvégzésére és elemzésére az elemekkel,
    vegyületekkel és keverékekkel kapcsolatban;

3. tudja, hogy a különféle ásványokból, kőzetekből építőanyagokat (pl.
    meszet, betont, üveget) és fémeket (pl. vasat és alumíniumot)
    gyártanak;

4. ismeri a kőolaj feldolgozásának módját, fő alkotóit, a
    szénhidrogéneket, tudja, hogy ezekből számos termék
    (motorhajtóanyag, kenőanyag, műanyag, textília, mosószer) is készül.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a mindennapi életben előforduló fontosabb vegyülettípusokat,
    tisztában van az élettelen és élő természet legfontosabb kémiai
    fogalmaival, jelenségeivel és az azokat működtető reakciótípusokkal;

2. önállóan vagy csoportban el tud végezni egyszerű kémiai kísérleteket
    és megbecsüli azok várható eredményét;

3. alkalmazza a természettudományos problémamegoldás lépéseit egyszerű
    kémiai problémák megoldásában;

4. képes az analógiás, a korrelatív és a mérlegelő gondolkodásra kémiai
    kontextusban;

5. képes számítógépes prezentáció formájában kémiával kapcsolatos
    eredmények, információk bemutatására, megosztására, a mérési adatok
    számítógépes feldolgozására, szemléltetésére;

6. tudja használni a részecskemodellt az anyagok tulajdonságainak és
    átalakulásainak értelmezésére;

7. ismeri a kémiának az egyén és a társadalom életében betöltött
    szerepét;

8. tisztában van a háztartásban leggyakrabban előforduló anyagok
    felhasználásának előnyeivel és veszélyeivel, a biztonságos
    vegyszerhasználat szabályaival.

A KÉMIA MINT TERMÉSZETTUDOMÁNYOS MEGISMERÉSI MÓDSZER

A KÉMIAI KÍSÉRLET MINT MEGISMERÉSI MÓDSZER

A nevelési-oktatási szakasz végére a tanuló:

1. egyedül vagy csoportban elvégez egyszerű kémiai kísérleteket leírás
    vagy szóbeli útmutatás alapján és értékeli azok eredményét;

2. egyedül vagy csoportban elvégez összetettebb,
    halmazállapot-változással és oldódással kapcsolatos kísérleteket és
    megbecsüli azok várható eredményét.

A TERMÉSZETTUDOMÁNYOS GONDOLKODÁS ALAPVETŐ MŰVELETEI

A nevelési-oktatási szakasz végére a tanuló:

1. kémiai vizsgálatainak tervezése során alkalmazza az analógiás
    gondolkodás alapjait és használja az „egyszerre csak egy tényezőt
    változtatunk" elvet;

2. ismeri az anyagmennyiség és a mól fogalmát, érti bevezetésük
    szükségességét és egyszerű számításokat végez m, n és M
    segítségével;

3. analógiás gondolkodással következtet a szerves vegyület
    tulajdonságára a funkciós csoportja ismeretében;

4. használja a fémek redukáló sorát a fémek tulajdonságainak
    megjóslására, tulajdonságaik alátámasztására.

A DIGITÁLIS KOMPETENCIA FEJLESZTÉSE

SZÖVEGEK, KÉPEK, ADATOK KERESÉSE, ÉRTELMEZÉSE, KRITIKUS ÉS ETIKUS
FELHASZNÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. ismer megbízható magyar és idegen nyelvű internetes forrásokat
    kémiai tárgyú, elemekkel és vegyületekkel kapcsolatos képek és
    szövegek gyűjtésére;

2. magabiztosan használ magyar és idegen nyelvű mobiltelefonos,
    táblagépes applikációkat kémiai tárgyú információk keresésére.

ALKOTÁS DIGITÁLIS ESZKÖZÖKKEL

A nevelési-oktatási szakasz végére a tanuló:

1. a különböző, megbízható forrásokból gyűjtött információkat
    számítógépes prezentációban mutatja be;

2. mobiltelefonos, táblagépes alkalmazások segítségével
    médiatartalmakat, illetve bemutatókat hoz létre.

A KÉMIA TÁRSADALMI VONATKOZÁSAI, KÖRNYEZET- ÉS TERMÉSZETTUDATOSSÁG

KÖRNYEZETVÉDELEM: VESZÉLYES ÉS KÜLÖNLEGES ANYAGOK, NYERSANYAGOK,
ANYAGKÖRFORGALOM

A nevelési-oktatási szakasz végére a tanuló:

1. alapvető szinten ismeri a természetes környezetet felépítő légkör,
    vízburok, kőzetburok és élővilág kémiai összetételét;

2. érti a környezetünk megóvásának a jelentőségét az emberi civilizáció
    fennmaradása szempontjából;

3. ismeri a zöld kémia lényegét, a környezetbarát folyamatok előtérbe
    helyezését, példákat mond újonnan előállított, az emberiség jólétét
    befolyásoló anyagokra (pl. új gyógyszerek, lebomló műanyagok,
    intelligens textíliák);

4. ismeri a legfontosabb környezetszennyező forrásokat és anyagokat,
    valamint ezeknek az anyagoknak a környezetre gyakorolt hatását;

5. példákkal szemlélteti egyes kémiai technológiák, illetve bizonyos
    anyagok felhasználásának környezetre gyakorolt pozitív és negatív
    hatásait.

ENERGIAGAZDÁLKODÁS: ENERGIAFORRÁSOK ÉS ENERGIAHORDOZÓK MEGISMERÉSE ÉS
ÖSSZEHASONLÍTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a bioüzemanyagok legfontosabb típusait.

GLOBÁLIS PROBLÉMÁK KÉMIAI VONATKOZÁSAI

A nevelési-oktatási szakasz végére a tanuló:

1. példákkal szemlélteti az emberiség legégetőbb globális problémáit
    (globális éghajlatváltozás, ózonlyuk, ivóvízkészlet csökkenése,
    energiaforrások kimerülése), és azok kémiai vonatkozásait;

2. ismeri az emberiség előtt álló legnagyobb kihívásokat, kiemelten
    azok kémiai vonatkozásai (energiahordozók, környezetszennyezés,
    fenntarthatóság, új anyagok előállítása);

3. példákon keresztül szemlélteti az antropogén tevékenységek kémiai
    vonatkozású környezeti következményeit;

4. kiselőadás vagy projektmunka keretében mutatja be a 20. század
    néhány nagy környezeti katasztrófáját és azt, hogy milyen
    tanulságokat vonhatunk le azok megismeréséből;

5. ismeri a légkör kémiai összetételét és az azt alkotó gázok fontosabb
    tulajdonságait, példákat mond a légkör élőlényekre és élettelen
    környezetre gyakorolt hatásaira, ismeri a legfontosabb légszennyező
    gázokat, azok alapvető tulajdonságait, valamint a
    környezetszennyezésének hatásait, ismeri a légkört érintő globális
    környezeti problémák kémiai hátterét és ezen problémák megoldására
    tett erőfeszítéseket;

6. ismeri a természetes vizek típusait, azok legfontosabb kémiai
    összetevőit a víz körforgásának és tulajdonságainak tükrében,
    példákat mond vízszennyező anyagokra, azok forrására, a szennyezés
    lehetséges következményeire, ismeri a víztisztítás folyamatának
    alapvető lépéseit, valamint a tiszta ivóvíz előállításának a módját;

7. érti a kőzetek és a környezeti tényezők talajképző szerepét, példát
    mond alapvető kőzetekre, ásványokra, érti a különbséget a hulladék
    és a szemét fogalmi megkülönböztetése között, ismeri a hulladékok
    típusait, kezelésük módját, környezetre gyakorolt hatásait.

TUDOMÁNY ÉS ÁLTUDOMÁNY MEGKÜLÖNBÖZTETÉSE, A TÉNYEKEN ALAPULÓ DÖNTÉSEK
JELENTŐSÉGE

A nevelési-oktatási szakasz végére a tanuló:

1. érti a különbséget a tudományos és áltudományos információk között,
    konkrét példát mond a köznapi életből tudományos és áltudományos
    ismeretekre, információkra;

2. ismeri a tudományos megközelítés lényegét (objektivitás,
    reprodukálhatóság, ellenőrizhetőség, bizonyíthatóság);

3. látja az áltudományos megközelítés lényegét (feltételezés,
    szubjektivitás, bizonyítatlanság), felismeri az áltudományosságra
    utaló legfontosabb jeleket.

KÉMIA A MINDENNAPOKBAN

TÁPLÁLKOZÁS, EGÉSZSÉGVÉDELEM

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az élelmiszereink legfontosabb összetevőinek, a
    szénhidrátoknak, a fehérjéknek, valamint a zsíroknak és olajoknak a
    molekulaszerkezetét és a tulajdonságait, felsorolja a háztartásban
    megtalálható legfontosabb élelmiszerek tápanyagait, példát mond
    bizonyos összetevők (fehérjék, redukáló cukrok, keményítő)
    kimutatására, ismeri a legfontosabb élelmiszeradalék-csoportokat,
    alapvető szinten értelmezi egy élelmiszer tájékoztató címkéjét;

2. ismeri a gyógyszer fogalmát és a gyógyszerek fontosabb csoportjait
    hatásuk alapján, alapvető szinten értelmezi a gyógyszerek mellékelt
    betegtájékoztatóját;

3. ismeri a leggyakrabban használt élvezeti szerek (szeszes italok,
    dohánytermékek, kávé, energiaitalok, drogok) hatóanyagát, ezen
    szerek használatának veszélyeit, érti az illegális drogok
    használatával kapcsolatos alapvető problémákat, példákat mond
    illegális drogokra, ismeri a doppingszer fogalmát, megérti és
    értékeli a doppingszerekkel kapcsolatos információkat;

4. ismeri a méreg fogalmának jelentését, érti az anyagok mennyiségének
    jelentőségét a mérgező hatásuk tekintetében, példát mond növényi,
    állati és szintetikus mérgekre, ismeri a mérgek szervezetbe
    jutásának lehetőségeit (tápcsatorna, bőr, tüdő), ismeri és felismeri
    a különböző anyagok csomagolásán a mérgező anyag piktogramját, képes
    ezeknek az anyagoknak a felelősségteljes használatára, ismeri a
    köznapi életben előforduló leggyakoribb mérgeket, mérgezéseket (pl.
    szén-monoxid, penészgomba-toxinok, gombamérgezések, helytelen égetés
    során keletkező füst anyagai, drogok, nehézfémek), tudja, hogy a
    mérgező hatás nem az anyag szintetikus eredetének a következménye.

A MINDENNAPI ÉLETBEN HASZNÁLT LEGFONTOSABB ANYAGOK FIZIKAI ÉS KÉMIAI
TULAJDONSÁGAI, ÁTALAKULÁSAI, EZEK ÖSSZEFÜGGÉSE A FELHASZNÁLÁSSAL

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a természetben megtalálható legfontosabb nyersanyagokat;

2. érti az anyagok átalakításának hasznát, valamint konkrét példákat
    mond vegyipari termékek előállítására;

3. ismeri a különböző nyersanyagokból előállítható legfontosabb
    termékeket;

4. érti, hogy az ipari (vegyipari) termelés során különféle, akár a
    környezetre vagy szervezetre káros anyagok is keletkezhetnek,
    amelyek közömbösítése, illetve kezelése fontos feladat;

5. képes az ismeretein alapuló tudatos vásárlással és tudatos
    életvitellel a környezetének megóvására;

6. érti a mészkőalapú építőanyagok kémiai összetételét és átalakulásait
    (mészkő, égetett mész, oltott mész), ismeri a beton alapvető
    összetételét, előállítását és felhasználásának lehetőségeit, ismeri
    a legfontosabb hőszigetelő anyagokat;

7. érti, hogy a fémek többsége a természetben vegyületek formájában van
    jelen, ismeri a legfontosabb redukciós eljárásokat (szenes,
    elektrokémiai redukció), ismeri a legfontosabb ötvözeteket, érti az
    ötvözetek felhasználásának előnyeit;

8. ismeri a fosszilis energiahordozók fogalmát és azok legfontosabb
    képviselőit, érti a kőolaj ipari lepárlásának elvét, ismeri a
    legfontosabb párlatok nevét, összetételét és felhasználási
    lehetőségeit, példát mond motorhajtó anyagokra, ismeri a
    töltőállomásokon kapható üzemanyagok típusait és azok
    felhasználását;

9. ismeri a műanyag fogalmát és a műanyagok csoportosításának
    lehetőségeit eredetük, illetve hővel szemben mutatott viselkedésük
    alapján, konkrét példákat mond műanyagokra a környezetéből, érti
    azok felhasználásának előnyeit, ismeri a polimerizáció fogalmát,
    példát ad monomerekre és polimerekre, ismeri a műanyagok
    felhasználásának előnyeit és hátrányait, környezetre gyakorolt
    hatásukat;

10. ismeri a mosó- és tisztítószerek, valamint a fertőtlenítőszerek
    fogalmi megkülönböztetését, példát mond a környezetéből gyakran
    használt mosó-és tisztítószerre és fertőtlenítőszerre, ismeri a
    szappan összetételét és a szappangyártás módját, ismeri a hypo
    kémiai összetételét és felhasználási módját, érti a mosószerek
    mosóaktív komponenseinek (a felületaktív részecskéknek) a mosásban
    betöltött szerepét;

11. ismeri a kemény víz és a lágy víz közötti különbséget, érti a kemény
    víz és egyes mosószerek közötti kölcsönhatás (kicsapódás)
    folyamatát.

VEGYSZERISMERET, BIZTONSÁGOS VEGYSZERHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a mindennapi életben előforduló növényvédő szerek
    használatának alapvető szabályait, értelmezi a növényvédő szereknek
    a leírását, felhasználási útmutatóját, példát mond a növényvédő
    szerekre a múltból és a jelenből (bordói lé, korszerű peszticidek),
    ismeri ezek hatásának elvi alapjait;

2. ismeri a legfontosabb (N-, P-, K-tartalmú) műtrágyák kémiai
    összetételét, előállítását és felhasználásának szükségszerűségét.

KÉMIAI SZAKISMERETEK

A RÉSZECSKEMODELL

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az atom felépítését, az elemi részecskéket, valamint azok
    jellemzőit, ismeri az izotópok legfontosabb tulajdonságait, érti a
    radioaktivitás lényegét és példát mond a radioaktív izotópok
    gyakorlati felhasználására;

2. ismeri az atom elektronszerkezetének kiépülését a Bohr-féle
    atommodell szintjén, tisztában van a vegyértékelektronok kémiai
    reakciókban betöltött szerepével;

3. értelmezi a periódusos rendszer fontosabb adatait (vegyjel,
    rendszám, relatív atomtömeg), alkalmazza a periódusszám és a
    (fő)csoportszám jelentését a héjak és a vegyértékelektronok
    szempontjából, ismeri a periódusos rendszer fontosabb csoportjainak
    a nevét, és az azokat alkotó elemek vegyjelét;

4. ismeri a molekulaképződés szabályait, ismeri az elektronegativitás
    fogalmát, és érti a kötéspolaritás lényegét, a kovalens kötést
    jellemzi száma és polaritása szerint, megalkotja egyszerű molekulák
    szerkezeti képletét, ismeri a legalapvetőbb molekulaalakokat
    (lineáris, síkháromszög, tetraéder, piramis, V-alak), valamint ezek
    meghatározó szerepét a molekulák polaritása szempontjából;

5. meghatározza egyszerű molekulák polaritását, és ennek alapján
    következtet a közöttük kialakuló másodrendű kémiai kötésekre,
    valamint oldhatósági jellemzőikre, érti, hogy a moláris tömeg és a
    molekulák között fellépő másodrendű kötések hogyan befolyásolják az
    olvadás- és forráspontot, ezeket konkrét példákkal támasztja alá.

SZERKEZET ÉS TULAJDONSÁG KAPCSOLATA

A nevelési-oktatási szakasz végére a tanuló:

1. érti a részecske szerkezete és az anyag fizikai és kémiai
    tulajdonságai közötti alapvető összefüggéseket;

2. ismeri az egyszerű ionok atomokból való létrejöttének módját, ezt
    konkrét példákkal szemlélteti, ismeri a fontosabb összetett ionok
    molekulákból való képződésének módját, tudja a nevüket,
    összegképletüket, érti egy ionvegyület képletének a megszerkesztését
    az azt alkotó ionok képlete alapján, érti az ionrács felépülési
    elvét, az ionvegyület képletének jelentését, konkrét példák
    segítségével jellemzi az ionvegyületek fontosabb tulajdonságait;

3. ismeri a fémek helyét a periódusos rendszerben, érti a fémes kötés
    kialakulásának és a fémek kristályszerkezetének a lényegét, érti a
    kapcsolatot a fémek kristályszerkezete és fontosabb tulajdonságai
    között, konkrét példák segítségével (pl. Fe, Al, Cu) jellemzi a
    fémes tulajdonságokat, összehasonlításokat végez;

4. ismeri a fémrács szerkezetét és az ebből adódó alapvető fizikai
    tulajdonságokat;

5. ismeri az anyagok csoportosításának a módját a kémiai összetétel
    alapján, ismeri ezeknek az anyagcsoportoknak a legfontosabb közös
    tulajdonságait, példákat mond minden csoport képviselőire, tudja,
    hogy az oldatok a keverékek egy csoportja;

6. érti a „hasonló a hasonlóban jól oldódik" elvet, ismeri az oldatok
    töménységével és az oldhatósággal kapcsolatos legfontosabb
    ismereteket, egyszerű számítási feladatokat old meg az oldatok
    köréből (tömegszázalék, anyagmennyiség-koncentráció);

7. adott szempontok alapján összehasonlítja a három halmazállapotba
    (gáz, folyadék, szilárd) tartozó anyagok általános jellemzőit,
    ismeri Avogadro gáztörvényét és egyszerű számításokat végez gázok
    térfogatával standard körülmények között, érti a
    halmazállapot-változások lényegét és energiaváltozását.

KÉMIAI VÁLTOZÁSOK

A nevelési-oktatási szakasz végére a tanuló:

1. érti a fizikai és kémiai változások közötti különbségeket;

2. ismeri a kémiai reakciók végbemenetelének feltételeit, ismeri, érti
    és alkalmazza a tömegmegmaradás törvényét a kémiai reakciókra;

3. ismeri a kémiai reakciók csoportosítását többféle szempont szerint:
    a reagáló és a képződő anyagok száma, a reakció energiaváltozása,
    időbeli lefolyása, iránya, a reakcióban részt vevő anyagok
    halmazállapota szerint;

4. a kémiai reakciókat szimbólumokkal írja le;

5. konkrét reakciókat termokémiai egyenlettel is felír, érti a
    termokémiai egyenlet jelentését, ismeri a reakcióhő fogalmát, a
    reakcióhő ismeretében megadja egy reakció energiaváltozását,
    energiadiagramot rajzol, értelmez, ismeri a termokémia főtételét és
    jelentőségét a többlépéses reakciók energiaváltozásának a
    meghatározásakor;

6. érti a katalizátorok hatásának az elvi alapjait;

7. ismer egyirányú és egyensúlyra vezető kémiai reakciókat, érti a
    dinamikus egyensúly fogalmát, ismeri és alkalmazza az egyensúly
    eltolásának lehetőségeit Le Châtelier elve alapján;

8. ismeri a fontosabb savakat, bázisokat, azok nevét, képletét,
    Brønsted sav-bázis elmélete alapján értelmezi a sav és bázis
    fogalmát, ismeri a savak és bázisok erősségének és értékűségének a
    jelentését, konkrét példát mond ezekre a vegyületekre, érti a víz
    sav-bázis tulajdonságait, ismeri az autoprotolízis jelenségét és a
    víz autoprotolízisének a termékeit;

9. konkrét példákon keresztül értelmezi a redoxireakciókat
    oxigénfelvétel és oxigénleadás alapján, ismeri a redoxireakciók
    tágabb értelmezését elektronátmenet alapján is, konkrét példákon
    bemutatja a redoxireakciót, eldönti egy egyszerű redoxireakció
    egyenlete ismeretében az elektronátadás irányát, az oxidációt és
    redukciót, megadja az oxidálószert és a redukálószert.

NEMFÉMES ELEMEK ÉS VEGYÜLETEIK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az anyagok jellemzésének logikus szempontrendszerét:
    anyagszerkezet -- fizikai tulajdonságok -- kémiai tulajdonságok --
    előfordulás -- előállítás -- felhasználás;

2. ismeri a hidrogén, a halogének, a kalkogének, a nitrogén, a szén és
    fontosabb vegyületeik fizikai és kémiai sajátságait, különös
    tekintettel a köznapi életben előforduló anyagokra;

3. alkalmazza az anyagok jellemzésének szempontjait a hidrogénre,
    kapcsolatot teremt az anyag szerkezete és tulajdonságai között;

4. ismeri a halogének képviselőit, jellemzi a klórt, ismeri a
    hidrogén-klorid és a nátrium-klorid tulajdonságait;

5. ismeri az oxigént és a vizet, ismeri az ózont, mint az oxigén
    allotróp módosulatát, ismeri mérgező hatását (szmogban) és
    UV-elnyelő hatását (ózonpajzsban);

6. ismeri a ként, a kén-dioxidot és a kénsavat;

7. ismeri a nitrogént, az ammóniát, a nitrogén-dioxidot és a
    salétromsavat;

8. ismeri a vörösfoszfort és a foszforsavat, fontosabb tulajdonságaikat
    és a foszfor gyufagyártásban betöltött szerepét;

9. összehasonlítja a gyémánt és a grafit szerkezetét és tulajdonságait,
    különbséget tesz a természetes és mesterséges szenek között, ismeri
    a természetes szenek felhasználását, ismeri a koksz és az aktív szén
    felhasználását, példát mond a szén reakcióira (pl. égés), ismeri a
    szén oxidjainak (CO, CO2) a tulajdonságait, élettani hatását,
    valamint a szénsavat és sóit, a karbonátokat.

FÉMEK ÉS FÉMVEGYÜLETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a fontosabb fémek (Na, K, Mg, Ca, Al, Fe, Cu, Ag, Au, Zn)
    fizikai és kémiai tulajdonságait;

2. kísérletek tapasztalatainak ismeretében értelmezi a fémek egymáshoz
    viszonyított reakciókészségét oxigénnel, sósavval, vízzel és más
    fémionok oldatával, érti a fémek redukáló sorának felépülését,
    következtet fémek reakciókészségére a sorban elfoglalt helyük
    alapján;

3. ismeri a fémek helyét a periódusos rendszerben, megkülönbözteti az
    alkálifémeket, az alkáliföldfémeket, ismeri a vas, az alumínium, a
    réz, valamint a nemesfémek legfontosabb tulajdonságait;

4. ismeri a fémek köznapi szempontból legfontosabb vegyületeit, azok
    alapvető tulajdonságait (NaCl, Na2CO3, NaHCO3, Na3PO4, CaCO3,
    Ca3(PO4)2, Al2O3, Fe2O3, CuSO4);

5. tisztában van az elektrokémiai áramforrások felépítésével és
    működésével, ismeri a Daniell-elem felépítését és az abban végbemenő
    folyamatokat, az elem áramtermelését;

6. érti az elektromos áram és a kémiai reakciók közötti
    összefüggéseket: a galvánelemek áramtermelésének és az
    elektrolízisnek a lényegét;

7. ismeri az elektrolizáló cella felépítését és az elektrolízis
    lényegét a hidrogén-klorid-oldat grafitelektródos elektrolízise
    kapcsán, érti, hogy az elektromos áram kémiai reakciók
    végbemenetelét segíti, példát ad ezek gyakorlati felhasználására
    (alumíniumgyártás, galvanizálás);

8. ismer eljárásokat fémek ércekből történő előállítására (vas,
    alumínium).

EGYSZERŰ SZERVES VEGYÜLETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a szerves vegyületeket felépítő organogén elemeket, érti a
    szerves vegyületek megkülönböztetésének, külön csoportban
    tárgyalásának az okát, az egyszerűbb szerves vegyületeket szerkezeti
    képlettel és összegképlettel jelöli;

2. ismeri a telített szénhidrogének homológ sorának felépülési elvét és
    fontosabb képviselőiket, ismeri a metán fontosabb tulajdonságait,
    jellemzi az anyagok szempontrendszere alapján, ismeri a homológ
    soron belül a forráspont változásának az okát, valamint a
    szénhidrogének oldhatóságát, ismeri és egy-egy kémiai egyenlettel
    leírja az égés, a szubsztitúció és a hőbontás folyamatát;

3. érti az izoméria jelenségét, példákat mond konstitúciós izomerekre;

4. ismeri a telítetlen szénhidrogének fogalmát, az etén és az acetilén
    szerkezetét és fontosabb tulajdonságait, ismeri és
    reakcióegyenletekkel leírja a telítetlen szénhidrogének jellemző
    reakciótípusait, az égést, az addíciót és a polimerizációt;

5. ismeri a legegyszerűbb szerves kémiai reakciótípusokat;

6. felismeri az aromás szerkezetet egy egyszerű vegyületben, ismeri a
    benzol molekulaszerkezetét és fontosabb tulajdonságait, tudja, hogy
    számos illékony aromás szénhidrogén mérgező;

7. példát mond közismert halogéntartalmú szerves vegyületre (pl.
    kloroform, vinil-klorid, freonok, DDT, tetrafluoretén), és ismeri
    felhasználásukat;

8. ismeri, és vegyületek képletében felismeri a legegyszerűbb
    oxigéntartalmú funkciós csoportokat: a hidroxilcsoportot, az
    oxocsoportot, az étercsoportot;

9. ismeri az alkoholok fontosabb képviselőit (metanol, etanol, glikol,
    glicerin), azok fontosabb tulajdonságait, élettani hatásukat és
    felhasználásukat;

10. felismeri az aldehidcsoportot, ismeri a formaldehid tulajdonságait,
    az aldehidek kimutatásának módját, felismeri a ketocsoportot, ismeri
    az aceton tulajdonságait, felhasználását;

11. ismeri, és vegyületek képletében felismeri a karboxilcsoportot és az
    észtercsoportot, ismeri az egyszerűbb és fontosabb karbonsavak
    (hangyasav, ecetsav, zsírsavak) szerkezetét és lényeges
    tulajdonságaikat;

12. az etil-acetát példáján bemutatja a kis szénatomszámú észterek
    jellemző tulajdonságait, tudja, hogy a zsírok, az olajok, a
    foszfatidok, a viaszok egyaránt az észterek csoportjába tartoznak;

13. szerkezetük alapján felismeri az aminok és az amidok egyszerűbb
    képviselőit, ismeri az aminocsoportot és az amidcsoportot.

BIOLÓGIAI SZEMPONTBÓL JELENTŐS SZERVES VEGYÜLETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a biológiai szempontból fontos szerves vegyületek
    építőelemeit (kémiai összetételét, a nagyobbak alkotó molekuláit);

2. ismeri a lipid gyűjtőnevet, tudja, hogy ebbe a csoportba hasonló
    oldhatósági tulajdonságokkal rendelkező vegyületek tartoznak,
    felsorolja a lipidek legfontosabb képviselőit, felismeri azokat
    szerkezeti képlet alapján, ismeri a lipidek csoportjába tartozó
    vegyületek egy-egy fontos szerepét az élő szervezetben;

3. ismeri a szénhidrátok legalapvetőbb csoportjait, példát mond
    mindegyik csoportból egy-két képviselőre, ismeri a szőlőcukor
    képletét, összefüggéseket talál a szőlőcukor szerkezete és
    tulajdonságai között, ismeri a háztartásban található szénhidrátok
    besorolását a megfelelő csoportba, valamint köznapi tulajdonságaikat
    (ízük, oldhatóságuk) és felhasználásukat, összehasonlítja a
    keményítő és a cellulóz molekulaszerkezetét és tulajdonságait,
    valamint szerepüket a szervezetben és a táplálékaink között;

4. tudja, hogy a fehérjék aminosavakból épülnek fel, ismeri az
    aminosavak általános szerkezetét és azok legfontosabb
    tulajdonságait, ismeri a fehérjék elsődleges, másodlagos,
    harmadlagos és negyedleges szerkezetét, érti e fajlagos molekulák
    szerkezetének a kialakulását, példát mond a fehérjék szervezetben és
    élelmiszereinkben betöltött szerepére, ismeri a fehérjék
    kicsapásának módjait és ennek jelentőségét a mérgezések kapcsán.

***II.3.6.6. FÖLDRAJZ***

***A) ALAPELVEK, CÉLOK***

Földrajzi, földtudományi és környezeti jelenségek, folyamatok sokasága
jellemzi bolygónkat. Ezek különböző tér- és időbeli léptékekben a Föld
különböző pontjain jelentkeznek. Egyre összetettebbé váló világunk
komplex problémáinak megértése megköveteli az eddigi tanítási, tanulási
stratégiák megújulását: a leíró jellegű, ismeretközlő hagyományokkal
szakítva, de a szaktárgyi tudást el nem vetve számos, a korábbiakban
kevésbé hangsúlyos kompetencia kialakítását és fejlesztését célként
megjelölve. A saját tevékenységeken, a hétköznapi megfigyeléseken és
tapasztalatokon alapuló földrajztanítás nem pusztán leírja a jelenséget,
hanem annak okát és következményeit is feltárja. Mindez a
természeti-környezeti és a társadalmi-gazdasági folyamatokat
szintetizálva, a jelen eseményein túlmutatva értékelésre,
problémamegoldásra, jövőképalkotásra ösztönöz.

A földrajzoktatás során a tanuló megismerheti szűkebb és tágabb
környezete természeti és társadalmi-gazdasági jellemzőit, a körülötte
zajló folyamatokat és ezek összefüggéseit. A földrajz szemléletformáló,
szintetizáló tantárgyként olyan, a hétköznapokban használható
ismereteket, eszközöket, módszereket ad a tanuló kezébe, amelyek segítik
a tájékozódást egyre összetettebbé váló világunkban, és hozzájárulnak
ahhoz, hogy felnőtt életében felelős, környezettudatos, aktív
állampolgárrá váljon.

A földrajz tanításának célja, hogy a tanuló:

1. a jelenségek, folyamatok természet- és a társadalomtudományi
    szempontú vizsgálatával a komplexitást szem előtt tartó,
    szintetizáló gondolkodást alakítson ki;

2. sajátítsa el az önálló földrajzi információszerzés és -feldolgozás,
    illetve összefüggés-felismerés készségét;

3. fejlessze a problémaorientált, elemző és mérlegelő gondolkodását,
    mely készségek nélkülözhetetlenek az információs társadalomra
    jellemző hír- és információdömpingben történő eligazodáshoz, a
    felelős és tudatos állampolgári szerepvállaláshoz;

4. alakítsa ki a térbeli tájékozódási készségét, valamint a térbeli
    folyamatok ok-okozati összefüggéseinek felismerési és elemzési
    képességét;

5. vizsgálja meg napjaink természeti, társadalmi-gazdasági és
    környezeti folyamatait, jelenségeit és a közöttük lévő
    kölcsönhatásokat, valamint a várható következmények átgondolásával
    alakítson ki cselekedni képes és a környezetért felelősséggel tenni
    akaró magatartást, fontos a tanulóval felismertetni és megértetni,
    hogy a környezettudatos, a fenntarthatóságot szem előtt tartó
    gondolkodás az élhető jövő záloga;

6. a térinformatikai, illetve infokommunikációs eszközök használata
    révén digitális kompetenciáját fejleszteni tudja annak érdekében,
    hogy tudatos eszközhasználóvá váljon;

7. szülőföldhöz és magyarsághoz való kötődése kialakuljon és
    elmélyüljön;

8. fejlődjön esztétikai érzéke és értékítélete a különböző természetes
    és mesterséges tájak bemutatásával;

9. megértse a globalizáció folyamatát és hatásait, reálisan lássa a
    világban elfoglalt helyünket, nemzeti értékeinket, ismerje fel a
    nemzeti és az európai önazonosság felvállalásának és megőrzésének
    fontosságát;

10. a térbeli-társadalmi egyenlőtlenségek által kiváltott folyamatok
    földrajzi okainak és lehetséges társadalmi-gazdasági
    következményeinek bemutatása révén empatikus, problémamegoldó
    gondolkodást, illetve az érvek ütköztetésére épülő vitakultúrát
    alakítson ki;

11. érdeklődését felkeltse az aktuális társadalmi-gazdasági és
    környezeti folyamatok megismerése, megértése, illetve megvitatása
    iránt;

12. napjaink társadalomföldrajzi folyamatainak bemutatása révén a
    toleráns, egymás tiszteletét szem előtt tartó magatartás
    kialakítására törekedjen;

13. a mindennapi életben hasznosítható pénzügyi-gazdasági ismeretek
    megismerésével az értő, felelős pénzügyi döntési képességét
    fejlessze;

14. a globális világ pénzügyi-gazdasági folyamatainak megismerésével a
    gazdasági élet eseményeiben eligazodó aktív, kreatív és rugalmas
    állampolgári gondolkodás és vállalkozásra kész attitűd kialakulását
    fejlessze;

15. pályaválasztását a tantárgy komplexitására, szintetizáló jellegére,
    a tantárgy által közvetített földrajzi-földtani, környezeti,
    gazdasági ismeretekre, gondolkodás- és szemléletmódra építve
    elősegítse.

***A tantárgy tanításának specifikus jellemzői a 7--8. évfolyamon***

A 7--8. évfolyamos földrajzi tananyag a földrajzi tartalmakat a
közelitől a távoli felé, azaz a közvetlen lakóhely (település) felől
Magyarország földrajzán keresztül a kontinentális, majd végül a globális
folyamatok felé haladva mutatja be. Az ismereteket a földrajzi
szempontból tipikus természet- és társadalomföldrajzi folyamatokra,
összefüggésekre fűzi fel, középpontba állítva a földrajzi eredetű
problémák komplex bemutatását. A földrajztanítás tudatosan épít a
természettudomány tantárgy révén megszerzett földrajzi tudásra, és
kapcsolódik az állampolgári ismeretek tantárgyhoz.

A földrajzoktatás jellemzői a 7--8. évfolyamon:

1. a tananyag valamely, a tanuló lakóhelyén vagy annak környékén a
    hétköznapok során megfigyelhető, megtapasztalható földrajzi
    jelenségből, folyamatból, illetve természeti és társadalmi
    folyamatokat magában foglaló komplex problémából indul ki;

2. az egyes témák feldolgozásának célja gyakorlati, a mindennapi
    életben hasznosítható ismeretek megszerzése, képességek kialakítása;

3. a tananyag a jelen folyamataira, jelenségeire és azok lehetséges
    következményeire összpontosít, tudatosan építve a hagyományos és
    digitális térképi, grafikus és szöveges adatforrásokból
    megszerezhető aktuális információkra;

4. a leíró jellegű ismeretközvetítés visszaszorításával párhuzamosan a
    tanuló aktív közreműködésén, munkáltatásán alapuló tudásépítést és
    készségfejlesztést tartja szem előtt;

5. támogatja a tanuló információszerző és -feldolgozó, prognosztizáló
    képességének fejlődését;

6. a természeti és társadalmi-gazdasági környezetet komplexitásában,
    összefüggéseiben a földrajzi eredetű problémákra fókuszálva
    vizsgálja;

7. megismerteti a tanulót a földrajzi ismeretszerzés legfontosabb,
    hiteles forrásaival, azok használatával, az információ
    feldolgozásának módszereivel, eszközeivel, probléma- és célorientált
    stratégiáival;

8. kialakítja a tanulóban a földrajzi problémák iránti érzékenységet,
    valamint a problémákra történő reflektálás képességét;

9. földrajzi jelenségek, problémák, természeti és társadalmi kockázatok
    feldolgozásával elősegíti a véleményformálás képességének
    kialakulását.

***A tantárgy tanításának specifikus jellemzői a 9--10. évfolyamon***

A 9--12. évfolyamos földrajzi tananyag a természeti és társadalmi
környezet összefüggéseivel, kölcsönhatásaival foglalkozik.

Kilencedik évfolyamon a cél a mindennapok tapasztalataira, illetve a
tanulói kísérletekre, vizsgálatokra, szöveges és képi forrásokra
alapozva a geoszférák természeti folyamatainak, törvényszerűségeinek
megismerése, megértése. További cél a természetföldrajzi folyamatok
okozta veszélyek és kockázatok felismerése, illetve a természeti
erőforrások és a társadalmi-gazdasági folyamatok közti kapcsolatok
feltárása. A Világegyetem végtelen teréből indulva bolygónk mozgásait és
ezek földrajzi következményeit, majd a geoszférák tipikus folyamatait,
jelenségeit vizsgálja a tanuló.

Tizedik évfolyamon a 21. század társadalmi és gazdasági folyamatainak,
illetve az azokat befolyásoló tényezők szerepének bemutatása jelenti a
földrajzoktatás fő célját. A korábbi évfolyamokon kialakított
készségekre, képességekre építve, azokat fejlesztve a globális világ
jellemzőivel, annak tipikus országaival, országcsoportjaival ismerkedik
meg a tanuló a földrajzi folyamatokra visszavezethető problémák
feltárására és a megoldások keresésére fókuszálva. Mindez szöveges és
képi forrásokat, online tartalmakat használva kritikus elemző munkával
történik. A munkát a globális folyamatok és a fenntarthatóság
kapcsolatának, illetve az egyéni szerepvállalás kérdésének bemutatása
zárja.

A középiskolai földrajzoktatás komplex ismeretanyaga révén segíti a
tanuló pályaválasztását, eligazodását a munka világában, felkészíti őt a
szakirányú felsőfokú tanulmányokra. Hozzájárul ahhoz, hogy a
középiskolai földrajzi tanulmányok befejezésekor biztonsággal tudjon
eligazodni a természeti és a társadalmi környezetben, illetve képes
legyen földrajzi ismeretei alkalmazására a mindennapi életben. A
földrajzoktatás ahhoz is hozzájárul, hogy az iskolából kilépő tanuló
képes legyen felelős döntéshozatalra az állampolgári szerep gyakorlása
során, valamint kialakuljon benne az igény arra, hogy későbbi élete
folyamán önállóan tovább gyarapítsa földrajzi ismereteit.

A földrajzoktatás jellemzői a 9--10. évfolyamon:

1. a tananyag földrajzi jelenségekből, illetve természeti és társadalmi
    folyamatokat magában foglaló, komplex földrajzi problémákból indul
    ki;

2. az egyes témák feldolgozásának célja gyakorlati, a mindennapi
    életben hasznosítható ismeretek megszerzése, képességek elmélyítése;

3. a tananyag a jelen folyamataira, jelenségeire és azok lehetséges
    következményeire összpontosít, tudatosan építve a hagyományos és
    digitális térképi, grafikus és szöveges adatforrásokból
    megszerezhető aktuális információkra;

4. a leíró jellegű ismeretközvetítés helyett a tanuló aktív
    közreműködésén, munkáltatásán alapuló tudásépítést és
    készségfejlesztést tartja szem előtt;

5. támogatja a tanuló információszerző és -feldolgozó, prognosztizáló
    képességének fejlődését;

6. a természeti és társadalmi környezetet komplexitásában,
    összefüggéseiben vizsgálja;

7. megismerteti a tanulót a földrajzi ismeretszerzés legfontosabb,
    hiteles forrásaival, azok használatával, eszközeivel, probléma- és
    célorientált stratégiáival;

8. kialakítja a tanulóban a földrajzi problémák iránti érzékenységet,
    valamint a problémákra való reflektálás képességét;

9. földrajzi jelenségek, problémák, természeti és társadalmi kockázatok
    feldolgozásával elősegíti a véleményformálás képességének
    kialakulását;

10. kialakítja a tanulóban a természeti veszélyek és környezeti
    kockázatok reális értékelésének képességét, továbbá tudatosítja
    bennük a megelőzés és a védekezés jelentőségét.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 7--8. ÉVFOLYAMON***

1. Tájékozódás a földrajzi térben

2. Közvetlen lakókörnyezetünk földrajza

3. Magyarország földrajza

4. A Kárpát-medence térsége

5. Európa és a távoli kontinensek eltérő fejlettségű térségei, tipikus
    tájai

6. A földrajzi övezetesség rendszere

7. Életünk és a gazdaság: a pénz és a munka világa

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

1. Tájékozódás a kozmikus térben és az időben

2. A kőzetburok

3. A légkör

4. A vízburok

5. A geoszférák kölcsönhatásai és összefüggései

6. Átalakuló települések, eltérő demográfiai problémák a 21. században

7. A nemzetgazdaságtól a globális világgazdaságig

8. Magyarország és Kárpát-medence a 21. században

9. A pénz és a tőke mozgásai a világgazdaságban

10. Helyi problémák, globális kihívások, a fenntartható jövő dilemmái

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 7--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. céljaival, feladataival összhangban kiválasztja és használja a
    földrajzi térben való tájékozódást segítő hagyományos és digitális
    eszközöket;

2. különböző földrajzi témákhoz kapcsolódóan adatokat, információkat
    gyűjt nyomtatott és elektronikus forrásokból, azokat értelmezi,
    rendszerezi, illetve ábrázolja;

3. földrajzi tartalmú szövegeket értelmez;

4. önállóan készített prezentációban bemutatja és elemzi egy adott
    terület természet- és társadalomföldrajzi adottságait, ezekből
    fakadó környezeti és társadalmi problémáit, és megoldási
    lehetőség(ek)et kínál;

5. véleményt alkot földrajzi témájú szövegekben bemutatott
    jelenségekről, folyamatokról, információkról;

6. vitában, szerepjátékban, szituációs játékban szerepének és az adott
    helyzetnek megfelelő, logikus érveket fogalmaz meg;

7. megold egyszerű földrajzi tartalmú logikai és számítási feladatokat;

8. társaival együttműködésben old meg földrajzi témájú feladatokat,
    képes a tudásmegosztásra;

9. alkalmazza földrajzi tudását a mindennapi életben a környezettudatos
    döntések meghozatalában, felelősséget érez döntései
    következményeiért;

10. tanulásához tudatosan használja a különböző típusú és tartalmú
    térképeket;

11. érdeklődik más országok földrajzi jellemzői, kultúrája, hagyományai
    iránt;

12. képes földrajzi ismeretei önálló bővítésére és rendszerezésére.

FÖLDRAJZI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. földrajzi tartalmú adatok, adatsorok alapján következtéseket von le,
    következményeket fogalmaz meg;

2. megadott szempontok alapján rendszerezi földrajzi ismereteit,
    rendszerbeli viszonyokat állapít meg;

3. összehasonlít tipikus tájakat, megfogalmazza azok közös és eltérő
    földrajzi vonásait;

4. megkülönbözteti a tényeket a véleményektől;

5. megismeri hazánk és Európa, majd a távoli kontinensek legalapvetőbb
    természet- és társadalomföldrajzi jellemzőit, melynek során kialakul
    a Földről alkotott, a valóságot visszatükröző kognitív térképe.

FÖLDRAJZI TARTALMÚ INFORMÁCIÓSZERZÉS ÉS -FELDOLGOZÁS,
DIGITÁLISESZKÖZ-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. megadott szempontok alapján információkat gyűjt hagyományos és
    digitális információforrásokból;

2. adatokat rendszerez és ábrázol digitális eszközök segítségével;

3. digitális eszközök segítségével bemutatja szűkebb és tágabb
    környezetének földrajzi jellemzőit;

4. megadott szempontok alapján tájakkal, országokkal kapcsolatos
    földrajzi tartalmú szövegeket, képi információhordozókat dolgoz fel;

5. közvetlen környezetének földrajzi megismerésére terepvizsgálódást
    tervez és kivitelez.

TÁJÉKOZÓDÁS A FÖLDRAJZI TÉRBEN ÉS IDŐBEN

A nevelési-oktatási szakasz végére a tanuló:

1. azonosítja a jelenségek időbeli jellemzőit;

2. használja a földrajzi térben való tájékozódást segítő hagyományos és
    digitális eszközöket;

3. gyakorlati feladatokat (pl. távolság- és helymeghatározás,
    utazástervezés) old meg nyomtatott és digitális térkép segítségével;

4. tájékozódik különböző típusú és tartalmú térképeken, biztonsággal
    leolvassa azok információtartalmát, a térképen elhelyez földrajzi
    elemeket;

5. képes egyszerű térképvázlatok, útvonaltervek elkészítésére.

TÁJÉKOZÓDÁS REGIONÁLIS FÖLDRAJZI KÉRDÉSEKBEN -- A FÖLDRAJZI JELLEMZŐK
ELEMZÉSE, ÖSSZEFÜGGÉSEK FELISMERÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. bemutatja és értékeli lakókörnyezetének földrajzi jellemzőit, ismeri
    annak természeti és társadalmi erőforrásait;

2. javaslatot fogalmaz meg lakókörnyezete jövőbeli, környezeti
    szempontokat szem előtt tartó, fenntartható fejlesztésére;

3. rendszerezi, csoportosítja és értékeli Magyarország és a
    Kárpát-medence térségének természeti és társadalmi-gazdasági
    erőforrásait, illetve bemutatja a természeti és társadalmi
    adottságok szerepének, jelentőségének időbeli változásait, a
    területi fejlettség különbségeit;

4. összehasonlít, illetve komplex módon, problémaközpontú
    megközelítéssel vizsgál pl. hazai nagytájakat, tájakat, régiókat,
    településeket;

5. következtet Magyarország és a Kárpát-medence térségében előforduló
    természeti és környezeti veszélyek kialakulásának okaira, várható
    következményeire, térbeli jellemzőire;

6. elkötelezett szűkebb és tágabb környezete természeti és
    társadalmi-gazdasági értékeinek megismerése és megőrzése iránt;

7. megnevez az egyes kontinensekre, országcsoportokra, meghatározó
    jelentőségű országokra jellemző társadalmi-gazdasági folyamatokat,
    ott előállított termékeket, szolgáltatásokat;

8. probléma- és értékközpontú megközelítéssel jellemzi Európa és az
    Európán kívüli kontinensek tipikus tájait, településeit, térségeit;

9. ismerteti az Európai Unió társadalmi-gazdasági jellemzőit, példákkal
    igazolja világgazdasági szerepét;

10. bemutatja a nemzetközi szintű munkamegosztás és fejlettségbeli
    különbségek kialakulásának okait és következményeit;

11. híradásokban közölt regionális földrajzi információkra reflektál;

12. reális alapokon nyugvó magyarság- és Európa-tudattal rendelkezik;

13. nyitott más országok, nemzetiségek szokásainak, kultúrájának
    megismerése iránt.

TÁJÉKOZÓDÁS A GEOSZFÉRÁK JELLEMZŐINEK ÉS FOLYAMATAINAK ÖSSZEFÜGGÉSEIBEN

A nevelési-oktatási szakasz végére a tanuló:

1. bemutatja a földrajzi övezetesség rendszerét, ismerteti az övezetek,
    övek kialakulásának okait és elhelyezkedésének térbeli jellemzőit;

2. összehasonlítja az egyes övezetek, övek jellemzőit,
    törvényszerűségeket fogalmaz meg velük összefüggésben;

3. példákat nevez meg a természeti adottságok gazdálkodást, életvitelt
    befolyásoló szerepére;

4. az egyes térségek kapcsán földrajzi és környezeti veszélyeket és
    problémákat fogalmaz meg, valamint reflektál azokra.

TÁJÉKOZÓDÁS A VILÁG ÁLTALÁNOS TÁRSADALOM- ÉS GAZDASÁGFÖLDRAJZI
FOLYAMATAIBAN

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és értelmezi a társadalmi-gazdasági fejlettségbeli
    különbségek leírására alkalmazott mutatókat;

2. népesség- és településföldrajzi információk alapján jellemzőket
    fogalmaz meg, következtetéseket von le;

3. példákat sorol a globalizáció mindennapi életünket befolyásoló
    folyamataira;

4. foglalkoztatási adatokat értelmez és elemez, következtetéseket von
    le belőlük;

5. értelmezi a mindennapi életben jelen lévő pénzügyi tevékenységeket,
    szolgáltatásokat;

6. életkori sajátosságainak megfelelő helyzetekben alkalmazza pénzügyi
    ismereteit (pl. egyszerű költségvetés készítése, valutaváltás,
    diákvállalkozás tervezése);

7. megnevezi a vállalkozás működését befolyásoló tényezőket.

TÁJÉKOZÓDÁS A GLOBÁLIS PROBLÉMÁK ÖSSZEFÜGGÉSEIBEN

A nevelési-oktatási szakasz végére a tanuló:

1. szűkebb és tágabb környezetében földrajzi eredetű problémákat
    azonosít, magyarázza kialakulásuk okait;

2. helyi, regionális és a Föld egészére jellemző folyamatok közötti
    hasonlóságokat, összefüggéseket felismer;

3. példák alapján megfogalmazza a helyi környezetkárosítás tágabb
    környezetre kiterjedő következményeit, megnevezi és ok-okozati
    összefüggéseiben bemutatja a globálissá váló környezeti problémákat;

4. ismeri a környezet- és a természetvédelem alapvető feladatait és
    lehetőségeit a földrajzi, környezeti eredetű problémák
    mérséklésében, megoldásában;

5. érveket fogalmaz meg a tudatos fogyasztói magatartás, a
    környezettudatos döntések fontossága mellett;

6. a környezeti kérdésekkel, globális problémákkal kapcsolatos
    álláspontját logikus érvekkel támasztja alá, javaslatot fogalmaz meg
    a környezeti problémák mérséklésére.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan és kritikusan használja a földrajzi tartalmú nyomtatott és
    elektronikus információforrásokat a tanulásban és tudása önálló
    bővítésekor;

2. ismeretei alapján biztonsággal tájékozódik a valós és a digitális
    eszközök által közvetített virtuális földrajzi térben, földrajzi
    tartalmú adatokban, a különböző típusú térképeken;

3. képes összetettebb földrajzi tartalmú szövegek értelmezésére;

4. adott természeti, társadalmi-gazdasági témához kapcsolódóan írásbeli
    vagy szóbeli beszámolót készít, prezentációt állít össze;

5. összetettebb földrajzi számítási feladatokat megold, az eredmények
    alapján következtetéseket fogalmaz meg;

6. véleményt alkot aktuális társadalmi-gazdasági és környezeti
    kérdésekben, véleménye alátámasztására logikus érveket fogalmaz meg;

7. földrajzi tartalmú projektfeladatokat valósít meg társaival;

8. elkötelezett a természeti és a kulturális értékek, a kulturális
    sokszínűség megőrzése iránt;

9. döntéseit a környezeti szempontok figyelembevételével mérlegeli,
    felelős fogyasztói magatartást tanúsít;

10. nyitott a különböző szintű pénzügyi folyamatok és összefüggések
    megismerése iránt;

11. alkalmazza a más tantárgyak tanulása során megszerzett ismereteit
    földrajzi problémák megoldása során.

FÖLDRAJZI GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. földrajzi tartalmú adatok, információk alapján következtetéseket von
    le, tendenciákat ismer fel, és várható következményeket (prognózist)
    fogalmaz meg;

2. földrajzi megfigyelést, vizsgálatot, kísérletet tervez és valósít
    meg, az eredményeket értelmezi;

3. feltárja a földrajzi folyamatok, jelenségek közötti hasonlóságokat
    és eltéréseket, különböző szempontok alapján rendszerezi azokat;

4. megkülönbözteti a tényeket a véleményektől, adatokat, információkat
    kritikusan szemlél;

5. önálló, érvekkel alátámasztott véleményt fogalmaz meg földrajzi
    kérdésekben.

FÖLDRAJZI TARTALMÚ INFORMÁCIÓSZERZÉS ÉS -FELDOLGOZÁS,
DIGITÁLISESZKÖZ-HASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. céljainak megfelelően kiválasztja és önállóan használja a
    hagyományos, illetve digitális információforrásokat, adatbázisokat;

2. földrajzi tartalmú szövegek alapján lényegkiemelő összegzést készít
    szóban és írásban;

3. digitális eszközök segítségével bemutat és értelmez földrajzi
    jelenségeket, folyamatokat, törvényszerűségeket, összefüggéseket;

4. adatokat rendszerez és ábrázol hagyományos és digitális eszközök
    segítségével;

5. megadott szempontok alapján alapvető földrajzi-földtani
    folyamatokkal, tájakkal, országokkal kapcsolatos földrajzi tartalmú
    szövegeket, képi információhordozókat dolgoz fel;

6. a közvetlen környezetének földrajzi megismerésére terepvizsgálódást
    tervez és kivitelez.

TÁJÉKOZÓDÁS A KOZMIKUS TÉRBEN ÉS AZ IDŐBEN

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan használja a földrajzi és a kozmikus térben való
    tájékozódást segítő hagyományos és digitális eszközöket, ismeri a
    légi- és űrfelvételek sajátosságait, alkalmazási területeit;

2. képes problémaközpontú feladatok megoldására, környezeti változások
    összehasonlító elemzésére térképek és légi- vagy űrfelvételek
    párhuzamos használatával;

3. térszemlélettel rendelkezik a csillagászati és a földrajzi térben;

4. érti a Világegyetem tér- és időbeli léptékeit, elhelyezi a Földet a
    Világegyetemben és a Naprendszerben;

5. ismeri a Föld, a Hold és a bolygók jellemzőit, mozgásait és ezek
    következményeit, összefüggéseit;

6. értelmezi a Nap és a Naprendszer jelenségeit, folyamatait, azok
    földi hatásait;

7. egyszerű csillagászati és időszámítással kapcsolatos feladatokat,
    számításokat végez.

TÁJÉKOZÓDÁS A GEOSZFÉRÁK JELLEMZŐINEK ÉS FOLYAMATAINAK ÖSSZEFÜGGÉSEIBEN

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a Föld felépítésének törvényszerűségeit;

2. összefüggéseiben mutatja be a lemeztektonika és az azt kísérő
    jelenségek (földrengések, vulkanizmus, hegységképződés) kapcsolatát,
    térbeliségét, illetve magyarázza a kőzetlemez-mozgások lokális és az
    adott helyen túlmutató globális hatásait;

3. felismeri a történelmi és a földtörténeti idő eltérő nagyságrendjét,
    ismeri a geoszférák fejlődésének időbeli szakaszait, meghatározó
    jelentőségű eseményeit;

4. párhuzamot tud vonni a jelenlegi és múltbeli földrajzi folyamatok
    között;

5. felismeri az alapvető ásványokat és kőzeteket, tud példákat említeni
    azok gazdasági és mindennapi életben való hasznosítására;

6. ismeri a kőzetburok folyamataihoz kapcsolódó földtani veszélyek
    okait, következményeit, tér- és időbeli jellemzőit, illetve elemzi
    az alkalmazkodási, kármegelőzési lehetőségeket;

7. érti a különböző kőzettani felépítésű területek eltérő környezeti
    érzékenysége, terhelhetősége közti összefüggéseket;

8. ismeri a légkör szerkezetét, fizikai és kémiai jellemzőit,
    magyarázza az ezekben bekövetkező változások mindennapi életre
    gyakorolt hatását;

9. összefüggéseiben mutatja be a légköri folyamatokat és jelenségeket,
    illetve összekapcsolja ezeket az időjárás alakulásával;

10. tudja az időjárási térképeket és előrejelzéseket értelmezni egyszerű
    prognózisok készítésére;

11. felismeri a szélsőséges időjárási helyzeteket és tud a helyzetnek
    megfelelően cselekedni;

12. a légkör globális változásaival foglalkozó forrásokat kritikusan
    elemzi, érveken alapuló véleményt fogalmaz meg a témával
    összefüggésben;

13. megnevezi a légkör legfőbb szennyező forrásait és a szennyeződés
    következményeit, érti a lokálisan ható légszennyező folyamatok
    globális következményeit;

14. magyarázza az éghajlatváltozás okait, valamint helyi, regionális,
    globális következményeit;

15. ismeri a felszíni és felszín alatti vizek főbb típusait, azok
    jellemzőit, mennyiségi és minőségi viszonyaikat befolyásoló
    tényezőket, a víztípusok közötti összefüggéseket;

16. igazolja a felszíni és felszín alatti vizek egyre fontosabbá váló
    erőforrásszerepét és gazdasági vonatkozásait, bizonyítja a víz
    társadalmi folyamatokat befolyásoló természetét, védelmének
    szükségességét;

17. ismeri a vízburokkal kapcsolatos környezeti veszélyek okait, és
    reálisan számol a várható következményekkel;

18. tudatában van a személyes szerepvállalások értékének a globális
    vízgazdálkodás és éghajlatváltozás rendszerében;

19. összefüggéseiben, kölcsönhatásaiban mutatja be a földrajzi
    övezetesség rendszerének egyes elemeit, a természeti jellemzők
    társadalmi-gazdasági vonatkozásait;

20. összefüggéseiben mutatja be a talajképződés folyamatát, tájékozott a
    talajok gazdasági jelentőségével kapcsolatos kérdésekben, ismeri
    Magyarország fontosabb talajtípusait;

21. bemutatja a felszínformálás többtényezős összefüggéseit, ismeri és
    felismeri a különböző felszínformáló folyamatokhoz (szél, víz, jég)
    és kőzettípusokhoz kapcsolódóan kialakuló, felszíni és felszín
    alatti formakincset;

22. érti az ember környezet átalakító szerepét, ember és környezete
    kapcsolatrendszerét, illetve példák alapján igazolja az egyes
    geoszférák folyamatainak, jelenségeinek gazdasági következményeit,
    összefüggéseit.

TÁJÉKOZÓDÁS A VILÁG ÁLTALÁNOS TÁRSADALOM- ÉS GAZDASÁGFÖLDRAJZI
FOLYAMATAIBAN

A nevelési-oktatási szakasz végére a tanuló:

1. bemutatja a népességszám-változás időbeli és területi különbségeit,
    ismerteti okait és következményeit, összefüggését a fiatalodó és az
    öregedő társadalmak jellemző folyamataival és problémáival;

2. különböző népességi, társadalmi és kulturális jellemzők alapján
    bemutat egy kontinenst, országot, országcsoportot;

3. különböző szempontok alapján csoportosítja és jellemzi az egyes
    településtípusokat, bemutatja szerepkörük és szerkezetük
    változásait;

4. érti és követi a lakóhelye környékén zajló település- és
    területfejlődési, valamint demográfiai folyamatokat;

5. ismerteti a gazdaság szerveződését befolyásoló telepítő tényezők
    szerepének átalakulását, bemutatja az egyes gazdasági ágazatok
    jellemzőit, értelmezi a gazdasági szerkezetváltás folyamatát;

6. értelmezi és értékeli a társadalmi-gazdasági fejlettség
    összehasonlítására alkalmas mutatók adatait, a társadalmi-gazdasági
    fejlettség területi különbségeit a Föld különböző térségeiben;

7. értékeli az eltérő adottságok, erőforrások szerepét a
    társadalmi-gazdasági fejlődésben;

8. modellezi a piacgazdaság működését;

9. megnevezi és értékeli a gazdasági integrációk és a regionális
    együttműködések kialakulásában szerepet játszó tényezőket;

10. ismerteti a világpolitika és a világgazdaság működését befolyásoló
    nemzetközi szervezetek, együttműködések legfontosabb jellemzőit;

11. értelmezi a globalizáció fogalmát, a globális világ kialakulásának
    és működésének feltételeit, jellemző vonásait;

12. példák alapján bemutatja a globalizáció társadalmi-gazdasági és
    környezeti következményeit, mindennapi életünkre gyakorolt hatását.

TÁJÉKOZÓDÁS REGIONÁLIS FÖLDRAJZI KÉRDÉSEKBEN -- A FÖLDRAJZI JELLEMZŐK
ELEMZÉSE, ÖSSZEFÜGGÉSEK FELISMERÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megnevezi a világgazdaság működése szempontjából tipikus térségeket,
    országokat;

2. összehasonlítja az európai, ázsiai és amerikai erőterek gazdaságilag
    meghatározó jelentőségű országainak, országcsoportjainak szerepét a
    globális világban;

3. összefüggéseiben mutatja be a perifériatérség társadalmi-gazdasági
    fejlődésének jellemző vonásait, a felzárkózás nehézségeit;

4. ismerteti az Európai Unió működésének földrajzi alapjait, példák
    segítségével bemutatja az Európai Unión belüli társadalmi-gazdasági
    fejlettségbeli különbségeket, és megnevezi a felzárkózást segítő
    eszközöket;

5. példák alapján jellemzi és értékeli Magyarország
    társadalmi-gazdasági szerepét annak szűkebb és tágabb nemzetközi
    környezetében, az Európai Unióban;

6. bemutatja a területi fejlettségi különbségek okait és
    következményeit Magyarországon, megfogalmazza a felzárkózás
    lehetőségeit;

7. értékeli hazánk környezeti állapotát, megnevezi jelentősebb
    környezeti problémáit.

TÁJÉKOZÓDÁS A MONETÁRIS VILÁG ÖSSZEFÜGGÉSEIBEN

A nevelési-oktatási szakasz végére a tanuló:

1. magyarázza a monetáris világ működésének alapvető fogalmait,
    folyamatait és azok összefüggéseit, ismer nemzetközi pénzügyi
    szervezeteket;

2. bemutatja a működőtőke- és a pénztőkeáramlás sajátos vonásait,
    magyarázza eltérésük okait;

3. pénzügyi döntéshelyzeteket, aktuális pénzügyi folyamatokat értelmez
    és megfogalmazza a lehetséges következményeket;

4. pénzügyi lehetőségeit mérlegelve egyszerű költségvetést készít,
    értékeli a hitelfelvétel előnyeit és kockázatait;

5. alkalmazza megszerzett ismereteit pénzügyi döntéseiben, belátja a
    körültekintő, felelős pénzügyi tervezés és döntéshozatal
    fontosságát.

TÁJÉKOZÓDÁS A GLOBÁLIS PROBLÉMÁK ÖSSZEFÜGGÉSEIBEN

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és azonosítja a földrajzi tartalmú természeti,
    társadalmi-gazdasági és környezeti problémákat, megnevezi
    kialakulásuk okait, és javaslatokat fogalmaz meg megoldásukra;

2. rendszerezi a geoszférákat ért környezetkárosító hatásokat,
    bemutatja a folyamatok kölcsönhatásait;

3. példákkal igazolja a természetkárosítás és a természeti, illetve
    környezeti katasztrófák társadalmi következményeit, a
    környezetkárosodás életkörülményekre, életminőségre gyakorolt
    hatását, a lokális szennyeződés globális következményeit;

4. globális problémákhoz vezető, Földünkön egy időben jelenlévő,
    különböző természeti és társadalmi-gazdasági eredetű problémákat
    elemez, feltárja azok összefüggéseit, bemutatja mérséklésük
    lehetséges módjait és azok nehézségeit;

5. megfogalmazza az energiahatékony, nyersanyag-takarékos, illetve
    „zöld" gazdálkodás lényegét, valamint példákat nevez meg a
    környezeti szempontok érvényesíthetőségére a termelésben és a
    fogyasztásban;

6. megkülönbözteti a fogyasztói társadalom és a tudatos fogyasztói
    közösség jellemzőit;

7. a lakóhely adottságaiból kiindulva értelmezi a fenntartható fejlődés
    társadalmi, természeti, gazdasági, környezetvédelmi kihívásait;

8. megnevez a környezet védelmében, illetve humanitárius céllal
    tevékenykedő hazai és nemzetközi szervezeteket, példákat említ azok
    tevékenységére, belátja és igazolja a nemzetközi összefogás
    szükségességét;

9. értelmezi a fenntartható gazdaság, a fenntartható gazdálkodás
    fogalmát, érveket fogalmaz meg a fenntarthatóságot szem előtt tartó
    gazdaság, illetve gazdálkodás fontossága mellett;

10. bemutatja az egyén társadalmi szerepvállalásának lehetőségeit, a
    tevékeny közreműködés példáit a környezet védelme érdekében, illetve
    érvényesíti saját döntéseiben a környezeti szempontokat.

*II.3.7. Művészetek*

A nevelés a teljes **emberre** irányul, aki test-szellem és lélek
egysége, tehát a köznevelés célja nem választható el az élet céljától.

A **művészet** alapvető emberi szükséglet, a művészi alkotás pedig az
ember legősibb kifejezési módja. A művészet köznevelésben betöltött
szerepe nem önmagáért való, feladata egyensúlyt teremteni az
értelmi-érzelmi intelligencia fejlesztésében.

A tanuló belső világának érzelmi gazdagsága pozitívan hat a gondolkodás,
a kreativitás, az emlékezőtehetség képességeire. A művészetoktatás a Nat
nélkülözhetetlen része, a transzfer hatás következtében tartalmi
alkotóeleme lehet minden tantárgy eredményes tanításának.

A **művészeti** nevelés önálló tantárgyként az ének-zene, illetve a
vizuális kultúra oktatásában valósul meg.

A **művészeti** neveléshez kapcsolódó eredményes készségfejlesztés
tanári vezetéssel, a tanár és a tanuló együttműködését igénylő, élményt
jelentő, aktív alkotó tevékenységen keresztül valósítható meg az
iskolában.

A tanórai keretek között, mindenki számára elérhető művészeti nevelés
célja olyan képességek fejlesztése, melyek elősegíthetik a harmonikus
társadalmi együttélést, azaz a művészeti tantárgyak a művészet eszközeit
a személyiség egészének fejlesztése érdekében használják fel.

A művészeti tanóra egyrészt **keretet** biztosít a művészet különböző
területeinek megismerésén keresztül az érdeklődés felkeltésére, az
esztétikai alapfogalmak elsajátítására, ízlésformálásra, másrészt a
mérlegelő gondolkodás és a minőségi alkotómunka iránti igény
kialakítására.

Az *ének-zene tantárgy* oktatása, a magyar ének-zene oktatás Kodály
Zoltán zenepedagógiai és nemzetnevelési elveire épül. A köznevelésben
részt vevő minden tanuló számára elérhető fejlesztő tevékenység
elsősorban az **énekhangra** támaszkodik. Általa és a zeneoktatás fontos
részeként szereplő, a relatív szolmizáció eszközével tanítandó zenei
írás-olvasás elsajátításának célja, hogy kialakuljon az értékközpontú
zenei anyanyelv értő szeretete.

A tanulók zenei ismereteinek **alapja** kezdetben a népzene, a magyar
népdalkincs közvetítése és megtanulása, majd erre épül a magas
művészettel való foglalkozás.

Az ének-zene oktatásban kiemelt **szerepet** kap az éneklés,
kóruséneklés. A tanórákhoz közvetlenül kapcsolódóan védett idő
biztosítása szükséges az eredményes iskolai kórusműködés érdekében.

A kodályi filozófia lényege a közösségben éneklés, hiszen azok az
ismeretek, melyeket a tanulók az énekórán megtanultak, művészi
alkotómunkában nyernek jutalmat.

A kórusban a karvezető irányításával a különböző képességű tanulók
együtteséből új, közösségi érték jön létre, melyet **egyénileg** nem
tudnának létrehozni. A tanulók megtanulnak alkalmazkodni, felelősséget
vállalni, egy célért odaadóan dolgozni, igényességet, fegyelmet,
egymásra való figyelmet tanulnak.

*A vizuális kultúra tantárgy:* A vizuális nevelés a tanulók
személyiségfejlesztésének rendkívül fontos része, hiszen az itt
alkalmazott tevékenységekre jellemző alkotva tanulás, érzelmeket
gazdagító, empátiát, intuíciót és minőségérzéket, valamint önmagukkal
szembeni igényességet kialakító hatása működik, ami vitathatatlanul az
önépítő és önszabályozó egyén és közösség egyik alappillére.

A Nat alapelvei alapján a vizuális kultúra tantárgy
gyakorlatközpontúsága a vizuális megismerés, a közvetlen
tapasztalatszerzés, az elemző-szintetizáló gondolkodás egységében
értelmezhető, ebben a tekintetben a tanulók ténylegesen megvalósuló
alkotó munkája elengedhetetlenül fontos.

A vizuális kultúra tantárgy fontos célja, hogy segítse a tanulókat az
őket körülvevő világ vizuálisan értelmezhető jelenségeinek megértésében,
ezen belül a vizuális művészeti alkotások átélésében és értelmezésében,
illetve ennek segítségével környezetük tudatos alakításában. Emellett
nagyon fontos cél, hogy a tanulók tudatosítsák saját gyökereiket,
megismerjék a magyarság által létrehozott legfontosabb képzőművészeti és
építészeti műalkotásokat, nemzetünk hagyományos tárgykultúráját és
díszítőművészetét. A tantárgy kulcsszerepet játszik a tanulók érzelmi
fejlesztésében, mely az őket körülvevő világhoz való pozitív érzelmi
viszonyulásuk kialakításának fontos eszköze.

Ennek eredményeként **elérendő** cél, hogy a tanulók a magyar kultúrára
büszkék legyenek, és kötődjenek szülőföldünk értékeihez.

A Művészetek tanulásterület köznevelésben **betöltött** feladata tehát a
művészettel nevelés, illetve más tanulásterületekkel együtt a
képességfejlesztés transzferhatásának kihasználása, testben-lélekben
boldogságra és boldogulásra képes generációk felnevelése érdekében.

***II.3.7.1. ÉNEK-ZENE***

***A) ALAPELVEK, CÉLOK***

A zenei nevelés Magyarországon a Kodály Zoltán által megalkotott
filozófiát követve, tevékenység-központú módszer alapján valósul meg. A
**magyar** zenekultúra évszázados hagyománya, hatalmas dallamkincse
bőséges anyagot kínál a zenei oktatás minden szintjéhez. Zenei
anyanyelvünk általános műveltségünk megalapozásának és nemzeti
azonosságtudatunknak fontos tényezője.

„*Mielőtt más népeket akarunk megérteni, magunkat kell megértenünk.
Semmi sem alkalmasabb erre, mint a népdal.* Erre az alapra épülhet
**olyan** zenei műveltség, mely nemzeti, de lelket tár minden nép nagy
alkotásainak. Értékmérőt is kap a népdallal, aki e századok csiszolta
tökéletességhez méri, amit hall: nem tévesztik meg többé hamis
bálványok\..." (Kodály Zoltán)

Az ének-zenei nevelés a tanuló identitástudatának kialakítása és
személyiségfejlődése szempontjából kiemelkedő **fontosságú**. Fontos
szerepet tölt be nemzeti örökségünk és identitásunk ápolása a magyar
népzene, a magyar tájegységek, azok szokásainak, életének,
nevezetességeinek, táncainak megismerésével, mely a családszeretet, a
hűség, és a haza iránti elkötelezettség érzését is erősíti. A zenei
nevelés akkor lesz eredményes, ha kialakul a tanuló zenei anyanyelve,
mely által hazájához, nemzetéhez értelmileg és érzelmileg egyaránt
kötődik. Ebben a pedagógus munkájának érdemi szerepe van, melynek hatása
életre szóló, túlmutat az iskola falain.

Az ének-zenei nevelés speciálisan olyan készségeket, **képességeket**,
kompetenciákat is fejleszt, melyek hatással vannak egyéb, nem zenei
képességekre is (transzferhatás).

Az aktív zenei tevékenységek, mint a közös éneklés, zenélés, alkotás és
zenés tematikus projektekben való részvétel, segítik az adott művészeti
élmény **elmélyülését**. Teret adnak az önkifejezés, kreativitás
kibontakozásának, és az empátia kialakításának. A zenetörténeti,
zeneelméleti és kulturális műveltség a zenei élményekhez,
tevékenységekhez kapcsolódóan az értő befogadást és a mérlegelő
gondolkodást fejlesztik. A tanuló képes lesz a művészi gondolatok és az
őt körülvevő hétköznapi világ közötti kapcsolatok meglátására, valamint
az eltérő látásmódok megértésére és elfogadására is.

A hatékony zenei nevelés figyelembe veszi a tanuló életkori
sajátosságait és kognitív képességeit. Az oktatásspecifikus céljait alsó
és felső **évfolyamokon**, valamint a középfokú nevelésben is ezekre a
kritériumokra alapozza. A hallás-és ritmikai készségek az agy fiatalkori
plaszticitásának köszönhetően az alapfokú képzés 1--4. évfolyamain
fejleszthetők a leghatékonyabban. Erre építhető az 5--8. évfolyamokon,
valamint a középfokú oktatásban (9--10. évfolyam) a képzelet, a
kreativitás, zenei önkifejezés és az érzelmi intelligencia további
fejlesztése.

Az alapszintű zenei írás-olvasás eszköz jellegű. Célja, hogy segítse a
tanulót az éneklésben és a zene mint univerzális jelrendszer
megértésében. A relatív szolmizáció elsősorban a tiszta éneklés, az
intonáció fejlesztése céljából jelenik meg. A törzshangok neveinek
ismerete a zene tágabb megértését, a jelrendszerben való logikus
gondolkodást segíti. A zeneelméleti ismereteket a pedagógus a tanulók
életkori sajátosságaihoz és képességeihez mérten a tanórák szabadon
felhasználható részében mélyítheti.

Kodály Zoltán zenei nevelési elvei lényegének **megfelelően**, mely az
élményszerzés legfontosabb forrásaként a kóruséneklést nevezi meg, a Nat
lehetőséget biztosít a tanórán kívüli zenei tevékenységek -- mint a
koncertpedagógia, az iskolai néptáncoktatás, népdalkörök, népdalkórusok,
kamaraegyüttesek -- iskolai keretek közti és azon túli szerveződése
számára, melyhez a köznevelés és a kultúrát közvetítő intézmények,
szervezetek együttműködése elvárható és szükséges.

Az ének-zene tanulása elsősorban a készségeket és az ezeket megalapozó
képességeket fejleszti. A tanulók nem azonos képességszintről indulnak
és nem is **azonos** eredményességgel végzik tanulmányaikat. Ezért az
értékelés alapja az ének-zene tanulás minden életkori szakaszában
alapvetően a tanulói aktivitás, lelkesedés, kooperativitás, illetve a
tanulási képességek változása, a tanuló önmagához mért fejlődési szintje
kell, hogy legyen.

Az ének-zenei nevelés az intellektuális és az emocionális fejlesztés
egyensúlyára épül. Ennek alapja a Kodály Zoltán zenei nevelési elveire
épülő, teljes embert fejlesztő zenepedagógiai gyakorlat, melynek célja a
magyar nemzeti értékeket tisztelő és őrző, egyben európai műveltségű,
kreatív, önállóan gondolkodó ember nevelése.

Az ének-zene tanításának célja, hogy a tanuló:

1. megszeresse az éneklést, a zenét az e tárgyban megismert művészeti
    alkotások befogadásán keresztül;

2. keresse, ismerje fel és becsülje meg a művészi értékeket;

3. zenei képességei fejlődjenek az éneklésen, a ritmikai- és
    hallásfejlesztésen keresztül, valamint sajátítsa el az ezeket
    támogató zeneelméleti alapismereteket;

4. érzelmi intelligenciája, képzelete és kreativitása fejlődjön, melyek
    hatással vannak egyéb, nem zenei képességeire is;

5. komplex látásmódja kifejlődjön a zene társadalmi, történelmi és
    kulturális kontextusában történő megértésével;

6. erősítse nemzeti öntudatát a magyar népzene, a néphagyományok, a
    hazai tájegységek életének, szokásainak megismerésén keresztül;

7. elősegítse önállóságát és önkifejezését a társas és egyéni
    alkotóművészeti tevékenységek művelésén és a véleményformálás
    fejlesztésén keresztül;

8. megerősítse empátiáját a zenei tevékenységekben való közös
    együttműködéssel, egymás zenei produkciójának, véleményének,
    ízlésének tisztelettel való befogadásával.

***A tantárgy tanításának specifikus jellemzői az 1--4. évfolyamon***

Az ének-zene tanításának fő célja az alsó tagozaton az éneklés
megszerettetése, a pozitív zenei élmények és gyakorlati tapasztalatok
**megszerzése**, a zenei anyag örömteli, játékos formában történő
feldolgozása énekléssel és mozgással, dramatizált előadással, a zene
keltette gondolatok, érzelmek szóbeli és képi kifejezésével.

A ritmikai és hallásfejlesztés segítik a koordinációs képességek
**intenzív** fejlődését. A relatív szolmizáció elsősorban az intonáció
és a felismerő kottaolvasás fejlesztését támogatja. A zenei ismeretek
hozzájárulnak a megtapasztalt élmények tudatosításához. A tantárgy
tartalmában és módszertanában a közösségi jelleg jelen van az aktív órai
tevékenységekben, gyermekjátékokban és a kóruséneklésben

A tanítási órák keretében az éneklési és zenehallgatási anyaghoz
kapcsolódva a tanuló különböző mozgásos játékokban vesz részt. Kiemelt
szerepet kapnak a magyar **gyermekjátékdalok** és táncok, melyek
elősegítik a nemzeti kultúra megismerését és megszeretését.

További cél, hogy a tanuló a környezetében fellelhető hétköznapi
tárgyakat, természetben talált anyagokat, használati eszközöket,
valamint az emberi testet hangszerként is használja, amivel
kibontakoztathatja zenei kreativitását.

A tanuló képzeletének és komplex látásmódjának folyamatos
**fejlesztése** céljából biztosítani kell számára a széles körű zenei
tapasztalatszerzés lehetőségét.

Az első négy évfolyamon elsősorban alapképességek és készségek
fejlesztése zajlik, ezért a témakörök nem alkotnak zárt egységet, és nem
időrendben követik egymást. Az egyes témaköröknél megjelölt minimum
óraszámok csak arányaiban segítik a tájékozódást. A témakörökön belül
megjelenő fejlesztési feladatok átfedik egymást, egy-egy fejlesztési
feladat több különböző témakörben is megjelenik. Ezáltal a tanórákon
belül is érvényesül a komplexitás.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

A felső tagozat zenei nevelése az alsó tagozatban elsajátított alapozó
ismeretekre épül.

A különböző évfolyamokon megismert magyar népdalok, más népek dalai,
illetve jelentős zeneszerzők műdalai a tanulók élő dalrepertoárját
alkotják. A tanórákon továbbra is alapvető jelentőségű a zenei
aktivitás, a kifejező éneklés.

A zenehallgatás során megismert zeneművek széles palettája ugyancsak
aktív ismeretként van jelen. A megismert zenetörténeti korszakokhoz
tartozó művek kronológiai rendszerezése 7--8. osztályban történik.

A zene befogadását segíti a zeneművek tágabb történelmi, kulturális és
társadalmi kontextusban való értelmezése, ami lehetőséget ad arra is,
hogy fejlődjön a tanulók képzelete, kreativitása és kapcsolatot
találjanak a műalkotásokban megjelenő élethelyzetek és saját életük
között.

A ritmikai és dallami ismeretek fokozatos bővítése a hallásfejlesztés
szolgálatában áll, törekedni kell alkotó módon történő használatukra.

Amint az alsó tagozatban, a témakörök a felső tagozatban sem alkotnak
zárt egységet és nem időrendben követik egymást. Az egyes témaköröknél
megjelölt minimum óraszámok csak arányaiban segítik a tájékozódást. A
témakörökön belül megjelenő fejlesztési feladatok átfedik egymást,
egy-egy fejlesztési feladat több különböző témakörben is megjelenik.
Ezáltal a tanórákon belül is érvényesül a komplexitás.

Az ének-zenei nevelés egyik legfontosabb színtere a kórus. Ennek során a
tanulók -- egy közösség részeként -- pótolhatatlan zenei élményekkel
gazdagodnak, és igazi művészi tevékenység részeseivé válhatnak.

***A tantárgy tanításának specifikus jellemzői a 9--10. évfolyamon***

A tanulók életében, személyiségük formálódásában, műveltségük
megszerzésében jelentős a középiskolás időszak, melyhez a zenei nevelés
fontos segítséget nyújt.

A közös, kifejező éneklés továbbra is élményforrás, hiszen ez vezet el
legközvetlenebbül a zene átéléséhez. A tanulók ekkorra komoly
dalrepertoárral rendelkeznek, mely magyar népdalokból, más népek
dalaiból és műdalokból áll, ismétlése, bővítése állandó feladat.

A zenei befogadás eredményességét segíti a népdalokban és más
zeneművekben feltárt élethelyzetekkel történő érzelmi azonosulás, a
gondolatok verbális kifejtése, valamint a zenének a viselkedésre,
fejlődésre és agyműködésre gyakorolt hatásaiban való tájékozottság.

Fontos feladat a zeneművek gondolati tartalmát kifejező eszközök
felismertetése, valamint az összefüggések felfedeztetése a zenei
stíluskorszakok, történelmi események, képzőművészeti és irodalmi
alkotások között.

A tanuló alapszintű ismereteket alkalmaz a digitális technika zenei
felhasználásában.

A zenei élmények megszerzésének egyik legfontosabb színtere a kórus.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Zeneművek, énekes anyag

2. Zeneművek, zenehallgatás

3. Zenei ismeretek, ritmikai fejlesztés

4. Hallásfejlesztés

5. Zenei írás, olvasás

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Zeneművek, énekes anyag

2. Zeneművek, zenehallgatás

3. Zenei ismeretek, ritmikai fejlesztés

4. Hallásfejlesztés

5. Zenei írás, olvasás

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

1. Zeneművek, énekes anyag

2. Zeneművek, zenehallgatás

3. Komplex készségfejlesztés

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. csoportosan vagy önállóan, életkorának és hangi sajátosságainak
    > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra
    > és a daloknak megfelelő tempóra;

2. ismer legalább 180 gyermekdalt, magyar népdalt;

3. a tanult dalok, zenei részletek éneklésekor változatosan tudja
    > használni hangerejét a zenei kifejezésnek megfelelően;

4. hangszerkíséretes dalokat énekel tanára vagy hangszeren játszó
    > osztálytársa kíséretével;

5. a tanult dalokhoz kapcsolódó játékokban, táncokban, dramatizált
    > előadásokban osztálytársaival aktívan részt vesz;

6. fogalmi szinten megkülönbözteti az egyenletes lüktetést és a
    > ritmust;

7. érzékeli és hangoztatja az egyenletes lüktetést, az ütemhangsúlyt a
    > tanult dalokban, zenei szemelvényekben;

8. felismeri és hangoztatja a negyed, nyolcadpár, fél értékű
    > ritmusokat, a negyed és a fél értékű szünetet, tájékozódik a
    > 2/4-es ütemben, felismeri és használja az ütemvonalat,
    > záróvonalat, az ismétlőjelet;

9. felismeri és hangoztatja az összetett ritmusokat (szinkópa, nyújtott
    > és éles ritmus), az egész értékű kottát, a pontozott fél értékű
    > kottát és az egyedül álló nyolcadot azok szüneteivel, valamint
    > tájékozódik a 4/4-es és 3/4-es ütemben;

10. megkülönbözteti a páros és a páratlan lüktetést;

11. ritmizálva szólaltat meg mondókákat, gyermekverseket;

12. érzékeli a hangok magasságának változásait, különböző hangszíneket,
    > ellentétes dinamikai szinteket, ezeket felismeri az őt körülvevő
    > világ hangjaiban, tanult dalokban, zeneművekben;

13. a tanár által énekelt dalokat belső hallással követi;

14. megismeri, énekli és alkalmazza a pentaton hangkészlet hangjait;

15. ismeri a tanult, énekelt zenei anyaghoz köthető szolmizációs
    > hangokat, kézjelről énekel;

16. a dalokat tanári segítséggel szolmizálva énekli, kézjelekkel
    > mutatja;

17. tanári segítséggel képes leírni és olvasni egyszerű ritmusokat,
    > dallamfordulatokat;

18. érzékeli, hogy ugyanaz a dallamrészlet különböző magasságokban
    > írható, olvasható;

19. reprodukálja a tanult ritmusokat mozgással, testhangszerrel,
    > valamint egyszerű ritmushangszerekkel.

ZENEI BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. a zeneművek befogadásában kreatívan használja képzeletét;

2. adott szempontok alapján figyeli meg a hallgatott zeneművet.

ZENEI ALKOTÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. egyszerű ritmussorokat rögtönöz;

2. különböző hangszíneket, hangmagasságokat, ellentétes dinamikai
    > szinteket hallás után megfigyel és reprodukál;

3. rövid dallamsorokat rögtönöz.

KULTURÁLIS MŰVELTSÉG ÉS KULTURÁLIS BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. aktívan részt vesz az iskola vagy a helyi közösség hagyományos
    > ünnepein, tematikus projektjein;

2. megismeri a gyermekdalokhoz kapcsolódó játékokat.

ÖNÁLLÓ TANULÁS FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a tanári instrukciók alapján alakít, finomít zenei előadásmódján;

2. életkori sajátosságának megfelelően képessé válik a zeneművek
    > érzelmi és intellektuális befogadására.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

ZENEI ELŐADÓKÉSZSÉG

ÉNEKLÉS

A nevelési-oktatási szakasz végére a tanuló:

1. csoportosan vagy önállóan, életkorának és hangi sajátosságainak
    > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra,
    > kifejező, a zene stílusának megfelelő előadásra;

2. a zenei karaktereket differenciáltan tudja megszólaltatni a népi
    > vagy klasszikus stílusjegyeknek megfelelően egyszerű
    > többszólamúságban is;

3. 127 új dalt ismer;

4. emlékezetből énekli a Himnuszt és a Szózatot;

5. változatosan tudja alkalmazni a tempó és dinamikai, előadási
    > utasításokat (tempo giusto, parlando, rubato, piano, mezzoforte,
    > forte);

6. hangszerkíséretes dalokat énekel, tanára vagy hangszeren játszó
    > osztálytársa kíséretével;

7. a tanult dalokhoz kapcsolódó dramatizált előadásokban
    > osztálytársaival aktívan részt vesz.

RITMIKAI ÉS HALLÁSFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza az alapritmusok relációit (egész-, fél-,
    > negyedérték, nyolcadpár, fél- és negyedszünet) és az összetett
    > ritmusokat (szinkópa, kis- és nagy nyújtott, kis- és nagy éles,
    > triola, tizenhatodos ritmusok), grafikai jelüket és értéküket;

2. különböző ritmusképleteket eltérő tempókban is reprodukál;

3. érzékeli a tanult dalokban a váltakozó ütemek lüktetését;

4. hallás útján megfigyeli a tanult zeneművekben a dúr és moll
    > hangzását, melyeket zenei karakterekhez, hangulatokhoz kapcsol;

5. ismeri és jártasságot szerez a hétfokú skála szolmizációs hangjainak
    > írásában és olvasásában;

6. ismeri az előjegyzésekhez kapcsolódó abszolút hangneveket;

7. felismerő kottaolvasással követi és értelmezi a módosított hangok
    > szerepét a dalokban;

8. érti a hangköz és hármashangzat fogalmát és fogalmi szinten a
    > hangközök harmóniaalkotó szerepét;

9. fogalmi szinten ismeri a tiszta, kis- és nagy hangközöket (T1-T8),
    > és a fél és egész hangos építkezés logikáját;

10. megnevezi és beazonosítja a kottakép alapvető elemeit, például
    > tempójelzés;

11. ütemmutató, violin- és basszuskulcsok;

12. felismerő kottaolvasással együttesen követi a ritmikai és dallami
    > elemeket;

13. ismeretlen kotta esetében is vizuálisan nyomon tudja követni a
    > hallott ritmikai és dallami folyamatokat;

14. érti és azonosítja a különböző formarészek zenén belüli szerepét;

15. felismeri a homofon és polifon szerkesztést.

ZENEI BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. életkori sajátosságainak megfelelően megadott szempontok
    > segítségével értelmezi a különböző műfajú és stílusú zenéket,
    > követi a lineáris és vertikális zenei folyamatokat;

2. azonosítani tudja a zenetörténeti stílusok főbb jellemzőit, a
    > hozzájuk tartozó műfajokat, jellegzetes hangszereket,
    > hangszer-összeállításokat, ismeri történelmi, kulturális és
    > társadalmi hátterüket, mely segíti zeneértésüket;

3. megfigyeli, összehasonlítja a zenét és a cselekményt, azonosítja a
    > témát és a szereplőket, hangulatot, műfajt, korszakot;

4. azonosítja a tanult zenéket, megnevezi azok alkotóit és
    > keletkezésüknek ismert körülményeit;

5. követni tudja a zenei elemek összetartozását, egymást kiegészítő
    > vagy egymástól eltérő struktúráját, a zenei kifejezésben betöltött
    > funkcióját;

6. megfigyeli az egyes hangszerek hangszínének a hangszerjáték és az
    > előadásmód különbözőségéből adódó karakterét;

7. kérdéseket vet fel a zenemű üzenetére és kifejezőeszközeire
    > vonatkozóan, többféle szempontot érvényesítve alkot véleményt;

8. néhány mondattal összefoglalja a zenemű mondanivalóját (például
    > miért íródott, kinek szól, milyen gondolatokat, érzelmeket fejez
    > ki);

9. megtalálja a kapcsolatot a zeneművek által közvetített élethelyzetek
    > és a saját élethelyzete között.

ZENEI ALKOTÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult ritmikai és dallami elemeket alkalmazva egyszerű
    > ritmussorokat, dallamokat kiegészít és improvizál;

2. a zeneművektől inspirálódva produktívan használja képzeletét,
    > megfogalmazza a zene keltette érzéseit, gondolatait, véleményét;

3. a zeneművek befogadásának előkészítése során részt vesz olyan közös
    > kreatív zenélési formákban, melyek segítenek a remekművek közelébe
    > jutni, felhasználja énekhangját, az akusztikus környezet hangjait,
    > ütőhangszereket, egyszerűbb dallamhangszereket.

KULTURÁLIS MŰVELTSÉG ÉS KULTURÁLIS BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. a zeneműveket műfajok és zenei korszakok szerint értelmezi, ismeri
    > történelmi, kulturális és társadalmi hátterüket;

2. ismer néhány magyar és más kultúrákra jellemző hangszert;

3. hangzás és látvány alapján felismeri a legelterjedtebb népi
    > hangszereket és hangszeregyütteseket;

4. megkülönbözteti a giusto, parlando és rubato előadásmódú népdalokat;

5. megkülönbözteti a műzenét, népzenét és a népdalfeldolgozásokat;

6. a tanult népdalokat tájegységekhez, azok ma is élő hagyományaihoz,
    > jellegzetes népművészeti motívumaihoz, ételeihez köti;

7. aktívan részt vesz az iskola vagy a helyi közösség hagyományos
    > ünnepein és tematikus projektjeiben.

ÖNÁLLÓ TANULÁS FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. képessé válik a zeneművek érzelmi és intellektuális befogadására;

2. megfigyeli és felismeri az összefüggéseket zene és szövege, zenei
    > eszközök és zenei mondanivaló között;

3. a tanári instrukciók alapján alakít, finomít zenei előadásmódján;

4. ismer és használ internetes zenei adatbázisokat, gyűjteményeket;

5. önálló beszámolókat készít internetes és egyéb zenei adatbázisok,
    > gyűjtemények felhasználásával.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

ZENEI ELŐADÓKÉSZSÉG

ÉNEKLÉS

A nevelési-oktatási szakasz végére a tanuló:

1. csoportosan vagy önállóan, életkorának és hangi sajátosságainak
    > megfelelő hangmagasságban énekel, törekszik a tiszta intonációra;

2. a tanult dalokat stílusosan, kifejezően adja elő;

3. emlékezetből és kottakép segítségével énekel régi és új rétegű
    > magyar népdalokat, más népek dalait, műdalokat, kánonokat;

4. 43 új dalt ismer;

5. hangszerkíséretes dalokat énekel, tanára vagy hangszeren játszó
    > osztálytársa kíséretével, a műdalok előadásában alkalmazkodni tud
    > a hangszerkísérethez;

6. a tanult dalokhoz kapcsolódó dramatizált előadásokban
    > osztálytársaival aktívan részt vesz.

RITMIKAI ÉS HALLÁSFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. felismerő kottaolvasással követi a ritmikai és dallami elemeket;

2. megnevezi és beazonosítja a kottakép alapvető elemeit, például
    > tempójelzés, ütemmutató, violin- és basszuskulcsok;

3. ismeretlen kotta esetében is vizuálisan nyomon tudja követni a
    > hallott ritmikai és dallami folyamatokat;

4. fogalmi szinten ismeri a hangközök harmóniaalkotó szerepét;

5. érti és azonosítja a különböző formarészek zenén belüli szerepét.

ZENEI BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. különbséget tud tenni korszakok, műfajok között;

2. kezdetben tanári segítséggel, majd önállóan felismeri adott
    > zeneműben a stílus és korszak hatását;

3. a zeneműveket tanári segítséggel történelmi, földrajzi és társadalmi
    > kontextusba helyezi;

4. kérdéseket vet fel a zenemű üzenetére és kifejezőeszközeire
    > vonatkozóan;

5. kezdetben tanári segítséggel, majd önállóan azonosítja a zenében
    > megjelenő társadalmi, erkölcsi, vallási, és kulturális mintákat;

6. megfigyeli a különböző zenei interpretációk közötti különbségeket, s
    > azokat véleményezi;

7. a dalok és a zeneművek befogadásához, azok előadásához felhasználja
    > eddigi ritmikai, dallami, harmóniai ismereteit;

8. a zeneműveket összekapcsolja élethelyzetekkel, melyeket saját élete
    > és környezete jelenségeire, problémáira tud vonatkoztatni.

ZENEI ALKOTÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. társítani tudja a zeneművekben megfogalmazott gondolatokat
    > hangszerelési, szerkesztési megoldásokkal, kompozíciós
    > technikákkal, formai megoldásokkal;

2. alapszintű ismereteket alkalmaz a digitális technika zenei
    > felhasználásában;

3. az elsajátított zenei anyagot élményszerűen alkalmazza tematikus
    > projektekben;

4. részt vesz kreatív zenélési formákban.

KULTURÁLIS MŰVELTSÉG ÉS KULTURÁLIS BEFOGADÓKÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. ismer magyar és más kultúrákra jellemző zenei sajátságokat, s ezeket
    > újonnan hallott zeneművekben is felfedezi;

2. értelmezi a népdalok szövegét, mondanivalóját, megtalálja bennük
    > önmagát;

3. a tanult népdalokat tájegységekhez, azok ma is élő hagyományaihoz,
    > jellegzetes népművészeti motívumaihoz, ételeihez köti;

4. konkrét művek példáin keresztül tanári segítséggel elkülöníti és
    > egymással kapcsolatba hozza az irodalomhoz és különböző művészeti
    > ágakhoz (film, képzőművészet, tánc) tartozó alkotások jellemző
    > vonásait a nyelvi, vizuális, mozgási és multimédiás művészetekben
    > és a zenében;

5. konkrét, a tanár által választott műalkotásokon keresztül
    > összehasonlítja a különböző művészeti ágak kifejezőeszközeit,
    > például hogyan fejez ki azonos érzelmet, mondanivalót a zene és
    > más művészetek;

6. ismeri a főbb fővárosi zenei intézményeket (például Zeneakadémia,
    > Magyar Állami Operaház, MÜPA), főbb vidéki zenei centrumokat
    > (például Kodály Központ -- Pécs, Kodály Intézet -- Kecskemét),
    > továbbá lakóhelye művelődési intézményeit;

7. fogalmi szinten tájékozott a zenének a viselkedésre, fejlődésre és
    > agyműködésre gyakorolt hatásaiban;

8. aktívan részt vesz az iskola vagy a helyi közösség hagyományos
    > ünnepein, tematikus projektjein;

9. a zeneműveket zenetörténeti kontextusba tudja helyezni, kapcsolatot
    > talál történelmi, irodalmi, kultúrtörténeti vonatkozásokkal,
    > azonosítja a műfaji jellemzőket

ÖNÁLLÓ TANULÁS FEJLESZTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a hallott zeneműveket érzelmi és intellektuális módon közelíti meg,
    > érti és értékeli a művészeti alkotásokat;

2. órai tapasztalatai és saját ismeretanyaga alapján önálló véleményt
    > alkot a zenemű

3. mondanivalójáról, és azt az adott zeneműből vett példákkal
    > illusztrálja;

4. értelmezi és önállóan véleményezi a zenéhez társított szöveg és a
    > zene kapcsolatát;

5. a tanult dalok üzenetét, saját életének, környezetének jelenségeire
    > tudja vonatkoztatni;

6. önálló kutatást végez feladatai megoldásához nyomtatott és digitális
    > forrásokban;

7. zenei ízlése az értékek mentén fejlődik.

***II.3.7.2. DRÁMA ÉS SZÍNHÁZ***

***A) ALAPELVEK, CÉLOK***

A dráma és színház tanulása olyan komplex művészetpedagógiai tevékenység
része, amely a csoportos játékok együttes élménye révén segíti elő a
tanulók alkotó és kapcsolatteremtő képességének kibontakozását;
összpontosított, megtervezett munkára szoktatását; testi, térbeli
biztonságának javulását; idő- és ritmusérzékének fejlődését; valamint
szolgálja ön- és társismeretük gazdagodását; fejleszti az érzékelést, a
kommunikációt, a kooperációt. Különösen alkalmas a fogékonyság, a
fantázia, az önkifejezés, valamint a tolerancia, az együttműködés,
továbbá a konfliktuskezelési képesség és a felelősségvállalás és
autonómia kialakítására, a közösségi tudat és az önazonosság
erősítésére. A dramatikus tevékenységek egyszersmind fejlesztik a zenei
képességeket, a vizuális érzékenységet, a térérzékelést, a testtudatot,
a mozgáskultúrát, ily módon integrálják a többi művészeti kifejezési
formát is.

A dráma ebben az összefüggésben nem az irodalom harmadik műnemét
jelenti, hanem a cselekvő megismerést. A sokrétű és sokféle
tevékenységet követő elemző beszélgetések a fogalmi ismeretek
bővülésével járnak. A tanulók megszerzett tudásukat, alakulóban lévő
véleményüket, felvetődő kérdéseiket beépíthetik a megjelenítő játékokba.
A dramatikus tevékenységek így más tantárgyak gyakorlatorientált
tanulásában is alkalmazhatóak.

A dráma nyelve a színház. A dráma és színház komplex művészeti
ismereteket nyújt, és az alkotómunkával együtt járó élmények
segítségével hozzájárul a kreatív személyiség kialakításához. A kultúra
iránt nyitottságot, esztétikai érzékenységet alakít ki, és a művészetek
befogadására, értésére és művelésére nevel.

A dráma és színház tanulásának kiegészítői lehetnek az olyan tanórán
kívüli tevékenységek, mint a színházi nevelési és színházpedagógiai
foglalkozások, a színjátszó körök, a színházlátogatás stb.

A dráma és színház tanulásának célja, hogy a tanuló:

1. az élmény megélésén keresztül jusson el a megértésig;

2. alkalmat kapjon az önmagára és a világra vonatkozó kérdések
    megfogalmazására és a válaszok keresésére;

3. különböző élethelyzeteket védett környezetben, biztonságos keretek
    között vizsgáljon;

4. a drámán és színjátékon keresztül tanulja meg az önkifejezést;

5. részt vegyen közösségépítésben és közösségi alkotásban;

6. verbális és nonverbális kommunikációs készségei fejlődjenek;

7. empátiás készségeit erősítse a dramatikus tevékenységekben való
    együttműködéssel;

8. komplex látásmódot alakítson ki a dráma és a színház társadalmi,
    történelmi és kulturális szerepének megértésével.

**A tantárgy tanításának specifikus jellemzői a 7--8. évfolyamon**

A dráma és színház tantárgy önálló óraszámmal a 7--8. évfolyamon bír, de
más tantárgyakba, tanulási területekbe integráltan, esetleg moduláris
formában, illetve a szabad órakeret terhére az 1--6. évfolyamon is
alkalmazható.

A dramatikus tevékenységek tanulási eredményeinek elkülönítését kevésbé
a korosztályos, évfolyambeli különbségek határozzák meg, sokkal inkább a
korábban drámajátékkal töltött gyakorlati idő mennyisége és minősége, a
csoport adottságai, valamint az iskola nevelési-oktatási kívánalmai.

Mivel a dráma és színház tantárgy teljes egészében gyakorlatközpontú, a
tevékenységekkel való ismerkedés időszakában a különböző munkaformák
bevezetése, a szaknyelv elsajátítása is játékos keretek között zajlik.
Elsőrendű feladat a játékbátorság kialakítása, az együttműködés
megteremtése és az elfogadó légkör megalapozása.

A tanulói aktivitásra építő tevékenységek során támaszkodunk a tanulás
társas (páros, kiscsoportos, egészcsoportos munka) természetéből adódó
előnyökre és a differenciált egyéni munka adta lehetőségekre.

**A tantárgy tanításának specifikus jellemzői a 12. évfolyamon**

A dramatikus tevékenységek eredménycéljainak elkülönítését kevésbé a
korosztályos, évfolyambeli különbségek határozzák meg, sokkal inkább a
korábban drámajátékkal töltött gyakorlati idő mennyisége és minősége, a
csoport adottságai, valamint az iskola nevelési-oktatási kívánalmai.

Mivel a dráma és színház tantárgy teljes egészében gyakorlatközpontú, a
tevékenységekkel való ismerkedés időszakában a különböző munkaformák
bevezetése, a szaknyelv elsajátítása is fontos szerepet kap. Feladat a
játékbátorság fejlesztése, az együttműködés megteremtése és az elfogadó
légkör megalapozása. A haladó csoportokban a szaktanár és a csoport
közös döntése alapján összetettebb feladatok, tanulási helyzetek
kerülnek sorra.

A tanulói aktivitásra építő tevékenységek során támaszkodunk a tanulás
társas természetéből adódó előnyökre és a differenciált egyéni munka
adta lehetőségekre. A tanulók igényei és lehetőségei szerint több
önálló, egyéni vagy csoportos feladat adható.

A 12. évfolyamon a gyakorlati tevékenységek mellett hangsúlyosan
megjelennek a dráma- és színháztörténet, dráma- és színházelmélet,
kortárs dráma és színház területei, az ezekkel való ismerkedésben
továbbra is használhatóak a dramatikus formák.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 7--8. ÉVFOLYAMON***

1. Szabályjátékok, népi játékok

2. Dramatikus játékok (szöveggel, hanggal, bábbal, zenével, mozgással,
    tánccal)

3. Rögtönzés

4. Saját történetek feldolgozása

5. Műalkotások feldolgozása

6. Dramaturgiai alapfogalmak

7. A színház kifejezőeszközei (szöveg, hang, báb, zene, mozgás, tánc)

8. Színházi műfajok, stílusok

9. Színházi előadás megtekintése

***FŐ TÉMAKÖRÖK A 12. ÉVFOLYAMON***

1. Szabályjátékok

2. Dramatikus játékok (szöveggel, hanggal, bábbal, zenével, mozgással,
    tánccal)

3. Rögtönzés

4. Saját történetek feldolgozása

5. Műalkotások feldolgozása

6. Dramaturgiai ismeretek

7. A színház kifejezőeszközei (szöveg, hang, báb, zene, mozgás, tánc)

8. Dráma- és színháztörténet

9. Dráma- és színházelmélet

10. Kortárs dráma és színház

11. Színjátékos tevékenység (vers- és prózamondás, jelenet, előadás
    stb.)

12. Színházi előadás megtekintése

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 7--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza a különböző verbális és nonverbális
    kommunikációs eszközöket;

2. ismeri és alkalmazza a drámai és színházi kifejezés formáit;

3. aktívan részt vesz többféle dramatikus tevékenységben tanári
    irányítással, önállóan, illetve társakkal való együttműködésben;

4. saját gondolatot, témát, üzenetet fogalmaz meg a témához általa
    alkalmasnak ítélt dramatikus közlésformában;

5. megfogalmazza egy színházi előadás kapcsán élményeit, gondolatait.

CSOPORTOS JÁTÉK ÉS EGYÜTTMŰKÖDÉS

ÉRZÉKELÉS, MEGFIGYELÉS, FELISMERÉS, EMLÉKEZET, FANTÁZIA

A nevelési-oktatási szakasz végére a tanuló:

1. felfedezi a tér, az idő, a tempó, a ritmus sajátosságait és
    összefüggéseit;

2. megfigyeli, azonosítja és értelmezi a tárgyi világ jelenségeit;

3. felidézi a látott, hallott, érzékelt verbális, vokális, vizuális,
    kinetikus hatásokat;

4. kitalál és alkalmaz elképzelt verbális, vokális, vizuális, kinetikus
    hatásokat.

FIGYELEM, KONCENTRÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan irányítja és összpontosítja figyelmét a környezete
    jelenségeire;

2. koncentrált figyelemmel végzi a játékszabályok adta keretek között
    tevékenységeit.

ÖN- ÉS TÁRSISMERET

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeli, azonosítja és értelmezi a környezetéből érkező hatásokra
    adott saját válaszait;

2. értelmezi önmagát a csoport részeként, illetve a csoportos
    tevékenység alkotó közreműködőjeként;

3. fejleszti az együttműködésre és a konszenzus kialakítására irányuló
    gyakorlatát.

MEGJELENÍTÉS, KIFEJEZÉS, KÖZVETÍTÉS

VERBÁLIS ÉS NONVERBÁLIS KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. adekvát módon alkalmazza a verbális és nonverbális kifejezés
    eszközeit;

2. az alkotótevékenység során használja a megismert kifejezési
    formákat.

TÁRGYHASZNÁLAT, BÁBHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. felfedezi a tárgyi világ kínálta eszközöket, ezek művészi formáit
    (pl. a bábot és a maszkot).

TÉRHASZNÁLAT

A nevelési-oktatási szakasz végére a tanuló:

1. használja a tér sajátosságaiban rejlő lehetőségeket.

IMPROVIZÁCIÓS KÉSZSÉG

A nevelési-oktatási szakasz végére a tanuló:

1. felfedezi a szerepbe lépésben és az együttjátszásban rejlő
    lehetőségeket;

2. felismeri és alapszinten alkalmazza a kapcsolat létrehozásának és
    fenntartásának technikáit;

3. felfedezi a kommunikációs jelek jelentéshordozó és jelentésteremtő
    erejét;

4. felfedezi a feszültség élményét és szerepét a dramatikus
    tevékenységekben.

SZÍNJÁTÉKOS ESZKÖZÖK HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. felfedezi a színházi kommunikáció erejét;

2. alkalmazza a tanult dramatikus technikákat a helyzetek
    megjelenítésében;

3. felismeri a helyzetek feldolgozása során a szerkesztésben rejlő
    lehetőségeket.

MEGISMERÉS ÉS ÉRTELMEZÉS

TÖRTÉNETEK FELDOLGOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti és alapszinten alkalmazza a dramaturgiai
    alapfogalmakat;

2. értelmezi a megélt, a látott-hallott-olvasott, a kitalált
    történeteket a különböző dramatikus tevékenységek révén.

SZÍNHÁZI ÉLMÉNY FELDOLGOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a színházi élmény fontosságát;

2. a színházi előadást a dramatikus tevékenységek kiindulópontjául is
    használja.

PROBLÉMAKÖZPONTÚ GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és azonosítja a dramatikus szituációk jellemzőit
    (szereplők, viszonyrendszer, cél, szándék, akarat, konfliktus,
    feloldás);

2. felismeri és megvizsgálja a problémahelyzeteket és azok lehetséges
    megoldási alternatíváit.

A DRÁMA ÉS A SZÍNHÁZ MŰVÉSZETE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és azonosítja a dráma és a színház formanyelvi
    sajátosságait a látott előadásokban.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 12.
ÉVFOLYAMON***

CSOPORTOS JÁTÉK ÉS EGYÜTTMŰKÖDÉS

A nevelési-oktatási szakasz végére a tanuló:

1. érzékeli, megfigyeli, felismeri és felidézi a tér, idő, tempó,
    ritmus, a tárgyi világ, a vizuális, kinetikus és vokális hatások
    sajátosságait és összefüggéseit; kitalál és közlési szándékkal
    alkalmaz ilyen hatásokat konkrét vagy szimbolikus jelentéssel;

2. tudatosan irányítja és összpontosítja figyelmét a környezete
    jelenségeire és felhasználja azokat dramatikus tevékenységeiben,
    hosszan tartó, koncentrált figyelemmel végzi a játékszabályok adta
    keretek között tevékenységeit;

3. értelmezi önmagát egyénként és a csoport részeként, illetve a
    csoportos tevékenység alkotó közreműködőjeként, azonosítja és
    értelmezi a környezetéből érkező hatásokat és az azokra adott saját
    válaszait, továbbfejleszti együttműködésre és konszenzus
    kialakítására irányuló gyakorlatát, kiveszi részét a közös
    döntéshozatali folyamatokból.

MEGJELENÍTÉS, KIFEJEZÉS, KÖZVETÍTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. az alkotótevékenység során adekvát és alkotó módon használja a
    megismert és általa alkotott kifejezési formákat, felfedezi a tárgyi
    világ kínálta eszközöket, ezek művészi formáit, a színházi
    vizualitás eszköztárát, használja a tér sajátosságaiban rejlő
    lehetőségeket, adekvát módon alkalmazza a verbális és nonverbális
    kifejezés eszközeit;

2. ismeri és alkalmazza a szerepbe lépésben és az együttjátszásban
    rejlő lehetőségeket, felismeri az empátia jelentőségét, ismeri és
    tudatosan alkalmazza a kapcsolat létrehozásának és fenntartásának
    technikáit, ismeri és alkalmazza a kommunikációs jelek
    jelentéshordozó és jelentésteremtő erejét, a feszültség eszközeit és
    szerepét a dramatikus tevékenységekben;

3. felfedezi a színházi kommunikáció erejét, alkalmazza a tanult
    dramatikus technikákat a helyzetek megjelenítésében, a történetek
    dramatizálásában és a figurateremtés folyamatában, felismeri és
    alkalmazza a helyzetek feldolgozása során a szerkesztésben rejlő
    lehetőségeket;

4. felismeri és alkalmazza a választott előadóművészeti műfajok
    jellemző eszköztárát.

MEGISMERÉS ÉS ÉRTELMEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megkülönbözteti és tudatosan alkalmazza a dramaturgiai
    alapfogalmakat, értelmezi a megélt, a látott-hallott-olvasott, a
    kitalált történeteket a különböző dramatikus tevékenységek révén;

2. felfedezi a történetek jelentőségét az emberi kommunikációban, az
    értékek közvetítésében és a kulturális hagyomány átörökítésében;

3. felismeri a színházi élmény fontosságát, és a színházi előadást a
    dramatikus tevékenységek kiindulópontjául is használja, a színházi
    előadást önmaga és a világ megértésének élményszerű formájaként
    értelmezi, felismeri a színházi előadásokban a művészettel élés
    lehetőségének egyik formáját.

PROBLÉMAKÖZPONTÚ GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri és azonosítja a dramatikus szituációk jellemzőit, elemzi
    és dramatikus eszközökkel feldolgozza a problémahelyzeteket és
    megvizsgálja azok lehetséges megoldási alternatíváit, értékítéletet
    formál, amelyet a problémahelyzet jellemző vonásaival indokol.

DRÁMA ÉS A SZÍNHÁZ MŰVÉSZETE

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi és rendszerezi a dráma- és színháztörténeti, dráma- és
    színházelméleti, valamint a kortárs színházra vonatkozó ismereteit
    és élményeit, saját munkájában alkalmazza, a látott előadásokban
    felismeri és azonosítja a dráma és a színház formanyelvi
    sajátosságait.

***II.3.7.3. VIZUÁLIS KULTÚRA***

***A) ALAPELVEK, CÉLOK***

A vizuális kultúra tantárgy legfontosabb alapelve az aktív tanulói
alkotómunkán alapuló gyakorlatközpontúság.

A vizuális nevelés -- tantárgyi tanulási eredményeiben is jól
megmutatkozó -- alapelve továbbá, hogy az egyéni feladatmegoldás mellett
megfelelő teret kap a csoportban megvalósított alkotó és befogadó
tevékenység, hisz a csoportos feladatmegoldás segíti az önismeretet és
önszabályozást, az önértékelést, továbbá a másokra való odafigyelést és
elfogadást.

A tárgy tanítása akkor eredményes, ha a tanulók vizuális produktumot
hoznak létre. Az eredményes fejlesztés a különböző nevelési-oktatási
szakaszok életkori sajátosságainak ismeretében, változatos és játékos
módszerek használatával érhető el.

A tantárgy gyakorlati tevékenységeinek megalapozásában fontos szerepe
van a közvetlen, érzékszervi tapasztalatszerzésnek, a környezettel való
közvetlen kapcsolatnak és a kéz finommotorikájának fejlesztése érdekében
a minél gazdagabb anyag- és eszközhasználatnak.

Az alkotói képességek fejlesztése az alkotva befogadás elvét követi.

Az alkotó, vizuális önkifejező tevékenység a személyiségfejlesztés egyik
legizgalmasabb és legeredményesebb eszköze. Az alkotás felszabadító
ereje segíti az érzelmek felfedezését és tudatosítását. A vizuális
produktum létrehozása során az elemző, értelmező, szintetizáló és
újszerű megoldásokat kereső tevékenységek az összetett gondolkodási
műveletek gyakorlását is lehetővé teszik.

Az alkotva befogadás elvének élményszerű alkalmazása segíti leginkább a
sokrétű képességfejlesztés által megvalósuló személyiségfejlesztést.

A vizuális kultúra tantárgynak jelentős, el nem hanyagolható szerepe van
a sokoldalú, kiegyensúlyozott személyiség fejlesztésében,
kialakításában.

A tantárgy képességfejlesztő tulajdonságai jelennek meg a fejlesztési
területeinek és témaköreinek meghatározásában is. A fejlesztési
területek; a vizuális megfigyelés, leírás, emlékezet, belső képalkotás,
elemzés, értelmezés, vizuális ábrázolás és kifejezés, közlés és
kommunikáció, a kreativitás fejlesztése folyamatát figyelembe véve
kerültek kijelölésre. A vizuális megismerés és kreatív produktum
létrehozásának logikája mentén meghatározott szerkezeti egységekbe
került az adott nevelési-oktatási szakasz elvárt tanulási eredményeinek
részletes leírása.

A vizuális megismerés részeiként értelmezett alkotó és befogadó
tevékenységeket a tanterv keretrendszere nem különíti el. A tanulási
eredmények megfogalmazása szempontjából azonban gyakran elkülönülnek az
alkotó és befogadó tevékenységre vonatkozó követelmények.

A vizuális kultúra tanterv témakörei a tantárgy alapvető részterületeit,
a képző- és vizuális művészetet, a vizuális kommunikációt, valamint a
tárgy- és környezetkultúrát részletezik tovább. E részterületek jelzik a
tantárgy legfontosabb tartalmait.

A vizuális kultúra jellemzően képességfejlesztő tantárgy, ebből
következően a vizuális megismerés eszközként használható más
tudományterületek, más tantárgyak tartalmainak feldolgozásához is.

A vizuális kultúra ismeretanyagának legjelentősebb részét a magyar és az
egyetemes kultúrkincsből meríti.

A vizuális kultúra tanulásának célja, hogy a tanuló:

1. értelmezze és átélje a körülöttünk lévő vizuális jelenségeket;

2. megismerje a mindennapi életben a vizuális kommunikáció szerepét;

3. ismerjen meg újabb kommunikációs formákat;

4. értelmezze a vizuális művészet kultúraközvetítésben elfoglalt
    helyét;

5. megismerje a kulturális örökség jelentőségét;

6. megismerje a magyar vizuális kultúra meghatározó alkotóit,
    alkotásait;

7. hazaszeretete és Magyarországhoz való érzelmi kötődése erősödjön a
    magyar kultúrkincs megismerése által;

8. ismerje a vizuálisan megjelenő nemzeti szimbólumokat;

9. megtanulja a tárgyi népművészet megismerésén keresztül a
    hagyományőrzés jelentőségét;

10. megértse a társadalmi folyamatokat, kortárs jelenségeket és
    problémákat;

11. a tárgykultúra és az épített környezet megismerésén keresztül
    megértse a fenntartható fejlődés problémakörét;

12. fejlessze finommotorikáját gyakorlati tevékenységek által;

13. készségszinten használja a vizuális kultúrában alkalmazott alapvető
    anyagokat, eszközöket

14. kapjon lehetőséget az alkotó kísérletezéshez;

15. szerezzen gyakorlatot a kreatív feladatmegoldásban;

16. tanulja meg a megalapozott, önálló ítéletalkotást;

17. a művészet sajátos eszközeivel fejlődjön érzelmi élete, érzelmi
    intellektusa;

18. fejlessze az önkifejező képességét;

19. gyakorlatot szerezzen a társakkal, csoportban történő alkotásban,
    együttműködésben.

***A tantárgy tanításának specifikus jellemzői az 1--4. évfolyamon***

Az általános iskola alsó tagozatán a tantárgy legfontosabb feladata az
örömteli alkotótevékenység és alkotókedv fenntartása, valamint a
vizuális műveltség nyitott szemléletű megalapozása. Az élményalapú
tanulás megvalósítása érdekében fontos a változatos, a tanulók
érdeklődését felkeltő játékos, új technikákat és kifejezésmódokat
kipróbáló alkotó feladatok gyakorlása, ugyanakkor elengedhetetlen az
ismerkedés a vizuális kommunikáció eszközeivel, az adott korosztálynak
címzett médiakörnyezettel, illetve a térszemlélet fejlesztése érdekében
ebben a nevelési-oktatási szakaszban kiemelt szerepe van a tárgyak
élményszerű használatának és az épített, tervezett környezet elemző
vizsgálatának. A minél többféle alkotótechnika, köztük a különböző
hagyományos kézműves technikák kipróbálása egyedülálló módon képes a kéz
finommotoros fejlesztésére, amely ebben az iskolaszakaszban a
leghatékonyabb. A változatos technikákkal megvalósuló tárgyalkotás,
térben konstruálás, építés a térbeli gondolkodás fejlesztésének sajátos
és mással nem pótolható eszköze.

A kéz finommotoros fejlesztését nem lehet megoldani digitális
eszközökkel, azonban, miután mára az adott korosztály gyakorlott
felhasználója a digitális környezetnek, már ebben az iskolaszakaszban
fontos a vizuális kommunikáció pótolhatatlan szerepének tisztázása, és a
tudatos felhasználás erősítése. Az alsó tagozat legfontosabb feladata az
alapkészségek fejlesztése, amely összetett és komplex alkotó
tevékenységben (például vizualitás, beszéd, mozgás, dramatikus
tevékenység, zene) fokozottan eredményes, így tanulástervezés
szempontjából ebben a nevelési-oktatási szakaszban különös jelentősége
van a tantárgyi integrációs célok érvényesítésének.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

Az általános iskola felső tagozatán a tantárgy legfontosabb feladata az
örömteli és többféle cél érdekében folyó alkotótevékenység segítése, a
kreativitás fejlesztése céljából a kísérletezés bátorítása, a
problémamegoldó gondolkodás gyakorlása, valamint a vizuális műveltség
komplex szemléletű gyarapítása. Ebben a nevelési-oktatási szakaszban is
a tanulás, a közvetlen környezetből nyert információk megfigyelése,
leírása, elemzése a megismerés fontos forrása, miközben különös
jelentőséget nyer a kreativitás, problémamegoldó gondolkodás fejlesztése
is.

A kreatív alkotó, ezen belül a tervező tevékenység sajátos lehetőséget
biztosít a vizuális problémahelyzetekben a megoldáskeresésre, a
megoldások kipróbálására, majd értelmezésére és értékelésére. Felső
tagozaton -- csakúgy, mint az alsóban -- az élményalapú tanulás
megvalósítása a tanulók érdeklődését felkeltő játékos, újabb és újabb
technikákat és kifejezésmódokat kipróbáló egyéni vagy csoportos alkotó
feladatok megoldásával érhető el. Miután az adott korosztály
mindennapjainak meghatározó része a digitális környezet, egyre
hangsúlyosabb a vizuális kommunikáció lehetőségeinek, a különböző
mediális megjelenések mérlegelő értelmezésének a gyakorlása. Ebben az
iskolaszakaszban a vizuális műveltség nem csak a történeti korok
művészeti emlékeinek vizsgálatát jelenti, hanem a mindennapok részeként
a kortárs művészeti jelenségek értelmezését és magyarázatát is. A
körülöttünk lévő épített, tervezett környezet, tárgyi világ vizsgálatán
keresztül a hagyományőrzés lehetőségeinek is teret biztosít. A
művészettörténet képi ismeretanyaga az alkotva befogadás elvét követve
része a tantárgyi követelményeknek, megközelítése tematikus, mert a
művészet kultúraközvetítésben betöltött szerepe a rendelkezésre álló
óra- és időkeretben a tanterv témaköreihez kapcsolva eredményesebben
tanítható és tanulható. Az adott iskolaszakasz életkori sajátosságai a
különböző vizuális önkifejezési formák gyakorlásával gazdagítják a
tanulók személyiségét.

***A tantárgy tanításának specifikus jellemzői a 9--10. évfolyamon***

A középiskolában a tantárgy legfontosabb feladata összetett
feladatmegoldás esetében a kísérletezésnek, a vizuális ötletek
kidolgozott megvalósításának, illetve a megszerzett vizuális tudás
önálló és innovatív felhasználásának ösztönzése. Ebben a
nevelési-oktatási szakaszban cél a művészeti fejlesztés
transzferhatásait kihasználó, -- egyéb területek és tantárgyak tanulását
is támogató -- változatos alkotótevékenység gyakorlása. További cél
olyan kreatív, gyakran együttműködésen alapuló fejlesztési lehetőségek
kipróbálása, amelyek szakmai érdeklődéstől függetlenül képesek
felkészíteni a tanulókat a munkaerő-piaci elvárásokra, illetve a
vizuális műveltség mérlegelő szemléletű és személyes felhasználására.

A tantárgy tanításában, tanulásában a hangsúly a tapasztalatok
értelmezésének, felhasználásának, a megszerzett tudás alkalmazásával
problémahelyzetek megoldásának, majd a megoldások értékelésének és
esetleges újraértelmezésének, a kreatív, alkotó, tervező folyamatok
ösztönzésén van.

A művészet teoretikus megközelítése és a művészettörténet, az alkotva
befogadás elvét követve továbbra is tantárgyi követelmény, amely a
tematikus tartalomszervezés érvényesítésével a korábbi
iskolaszakaszokban megszerzett tudásra és vizuális ismeretanyagra épül.
Középiskolában is cél a változatos, a tanulók érdeklődésén alapuló, a
kortárs művészeti jelenségeket is feldolgozó, az önismeretet és
önazonosságot támogató vizuális önkifejezési formák gyakorlása. Fontos a
vizuális kommunikációs formák elemző megközelítése, a különböző mediális
megjelenések értelmező és mérlegelő vizsgálata, az épített, tervezett
környezet értelmezése környezettudatosság szempontjából, illetve a
designgondolkodás lehetőségeinek felfedezése. Az egyéni vagy csoportos
alkotással támogatott befogadó tevékenység akkor éri el a
legeredményesebben a célját, ha valós helyzetekből kiindulva fejleszti a
problémaérzékenységet, az empátiát, ha erősíti a társadalmi
felelősségvállalást és együttműködést.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Vizuális kifejezőeszközök

2. Síkbeli és térbeli alkotások

3. Vizuális információ

4. Médiahasználat

5. Álló- és mozgókép

6. Természetes és mesterséges környezet

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Vizuális művészeti jelenségek

2. Médiumok sajátosságai

3. Térbeli és időbeli viszonyok

4. Vizuális információ és befolyásolás

5. Környezet: technológia és hagyomány

***FŐ TÉMAKÖRÖK A 9--10. ÉVFOLYAMON***

1. Korszak, stílus, műfaj

2. Kortárs művészeti jelenségek

3. Vizuális közlés, a reklám hatásmechanizmusa

4. Digitális képalkotás, közösségi média

5. Design, divat, identitás

6. Környezet és fenntarthatóság, környezettudatosság

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. alkotó tevékenység közben bátran kísérletezik;

2. különböző eszközöket rendeltetésszerűen használ;

3. megérti és végrehajtja a feladatokat;

4. példák alapján különbséget tesz a hétköznapi és a művészi között;

5. gondolatait vizuálisan is érthetően megmagyarázza;

6. példák alapján azonosítja a médiafogyasztás mindennapi jelenségeit;

7. önálló döntéseket hoz a környezet alakításának szempontjából;

8. csoportban végzett feladatmegoldás során részt vállal a
    feladatmegoldásban, és figyelembe veszi társai álláspontját;

9. csoportban végzett feladatmegoldás közben képes érvelésre;

10. feladatmegoldás során betartja az előre ismertetett szabályokat;

11. egyszerű, begyakorolt feladatokat önállóan is elvégez;

12. véleményét önállóan megfogalmazza.

MEGFIGYELÉS, VIZUÁLIS EMLÉKEZET

IRÁNYÍTOTT MEGFIGYELÉSEK TÁRGYÁNAK JELLEMZÉSE, LEÍRÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. érzékszervi tapasztalatokat pontosan megfogalmaz mérettel, formával,
    színnel, felülettel, illattal, hanggal, mozgással kapcsolatban.

MEGADOTT SZEMPONTOK SEGÍTSÉGÉVEL, IRÁNYÍTOTT MEGFIGYELÉSEK ALAPJÁN
VIZUÁLIS MEGJELENÉSEK AZONOSÍTÁSA, RENDSZEREZÉSE, EMLÉKEZETBŐL IS

A nevelési-oktatási szakasz végére a tanuló:

1. közvetlen vizuális megfigyeléssel leolvasható egyszerű jellemzők
    alapján vizuális jelenségeket, képeket méret, irány, elhelyezkedés,
    mennyiség, szín szempontjából azonosít, kiválaszt, rendez,
    szövegesen pontosan leír és összehasonlít;

2. látványt, vizuális jelenségeket, képeket viszonylagos pontossággal
    emlékezetből azonosít, kiválaszt, megnevez, különböző szempontok
    szerint rendez.

LÁTOTT DOLGOK EGYSZERŰ MEGJELENÍTÉSE KÜLÖNBÖZŐ ESZKÖZÖKKEL

A nevelési-oktatási szakasz végére a tanuló:

1. vizuális jelenségeket, egyszerű látott képi elemeket különböző
    vizuális eszközökkel megjelenít: rajzol, fest, nyomtat, formáz,
    épít.

BELSŐ KÉPALKOTÁS, KÉPZETEK

ELKÉPZELT DOLGOK ÖNÁLLÓ VIZUÁLIS MEGJELENÍTÉSE ÉS SZÖVEGES MAGYARÁZATA

A nevelési-oktatási szakasz végére a tanuló:

1. élmények, elképzelt vagy hallott történetek, szövegek részleteit
    különböző vizuális eszközökkel egyszerűen megjeleníti: rajzol, fest,
    nyomtat, fotóz, formáz, épít;

<!-- -->

2. rövid szövegekhez, egyéb tananyagtartalmakhoz síkbeli és térbeli
    vizuális illusztrációt készít különböző vizuális eszközökkel:
    rajzol, fest, nyomtat, fotóz, formáz, épít és a képet, tárgyat
    szövegesen értelmezi;

3. egyszerű eszközökkel és anyagokból elképzelt teret rendez, alakít,
    egyszerű makettet készít egyénileg vagy csoportmunkában, és az
    elképzelést szövegesen is bemutatja, magyarázza;

4. elképzelt történeteket, irodalmi alkotásokat bemutat, dramatizál,
    ehhez egyszerű eszközöket: bábot, teret, díszletet, kelléket,
    egyszerű jelmezt készít csoportmunkában, és élményeit szövegesen
    megfogalmazza.

VIZUÁLIS ELEMZÉS, VIZUÁLIS ÉRTELMEZÉS

MEGADOTT SZEMPONTOK SEGÍTSÉGÉVEL VIZUÁLIS MEGJELENÉSEK, KÉPEK,
MOZGÓKÉPEK ÉRTELMEZÉSE, ÖSSZEHASONLÍTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. saját és társai vizuális munkáit szövegesen értelmezi, kiegészíti,
    magyarázza;

2. saját munkákat, képeket, műalkotásokat, mozgóképi részleteket
    szereplők karaktere, szín-, fényhatás, kompozíció, kifejezőerő
    szempontjából szövegesen elemez, összehasonlít;

3. képek, műalkotások, mozgóképi közlések megtekintése után önállóan
    megfogalmazza és indokolja tetszésítéletét;

4. képek, műalkotások, mozgóképi közlések megtekintése után adott
    szempontok szerint következtetést fogalmaz meg, megállapításait
    társaival is megvitatja.

MEGJELENÍTÉS, ÁBRÁZOLÁS, KONSTRUÁLÁS

LÁTVÁNY VÁLTOZATOS MEGJELENÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. különböző alakzatokat, motívumokat, egyszerű vizuális megjelenéseket
    látvány alapján, különböző vizuális eszközökkel, viszonylagos
    pontossággal megjelenít: rajzol, fest, nyomtat, formáz, épít;

2. adott cél érdekében fotót vagy rövid mozgóképet készít.

ÖNÁLLÓAN VÁLASZTOTT ESZKÖZÖKKEL TÉRBELI FORMA, ÉPÍTMÉNY, TÁRGY
TERVEZÉSE, LÉTREHOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. alkalmazza az egyszerű tárgykészítés legfontosabb technikáit: vág,
    ragaszt, tűz, varr, kötöz, fűz, mintáz;

2. adott cél érdekében alkalmazza a térbeli formaalkotás különböző
    technikáit egyénileg és csoportmunkában.

ADOTT VAGY VÁLASZTOTT INSPIRÁCIÓFORRÁS ALAPJÁN MINTATERVEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. gyűjtött természeti vagy mesterséges formák egyszerűsítésével, vagy
    a magyar díszítőművészet általa megismert mintakincsének
    felhasználásával mintát tervez.

MAGYARÁZAT ÉRDEKÉBEN GONDOLATOK VIZUÁLIS BEMUTATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. a tanulás során szerzett tapasztalatait, saját céljait, gondolatait
    vizuális megjelenítés segítségével magyarázza, illusztrálja
    egyénileg és csoportmunkában.

VIZUÁLIS KIFEJEZÉS

ÉRZELMEK, HANGULATOK AZONOSÍTÁSA ÉS MEGJELENÍTÉSE KÜLÖNBÖZŐ ESZKÖZÖKKEL

A nevelési-oktatási szakasz végére a tanuló:

1. saját és mások érzelmeit, hangulatait segítséggel megfogalmazza és
    egyszerű dramatikus eszközökkel eljátssza, vizuális eszközökkel
    megjeleníti.

SAJÁT ÉLMÉNYEK MEGFOGALMAZÁSA ÉS VIZUÁLIS MEGJELENÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. korábban átélt eseményeket, tapasztalatokat, élményeket különböző
    vizuális eszközökkel, élményszerűen megjelenít: rajzol, fest,
    nyomtat, formáz, épít, fotóz és magyarázza azt;

2. valós vagy digitális játékélményeit vizuálisan és dramatikusan
    feldolgozza: rajzol, fest, formáz, nyomtat, eljátszik, elmesél.

CÉLZOTT, DIREKT VIZUÁLIS KÖZLÉS

MEGADOTT SZEMPONTOK SEGÍTSÉGÉVEL VIZUÁLIS KOMMUNIKÁCIÓS ELEMEK
AZONOSÍTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. a vizuális nyelv elemeinek értelmezésével és használatával
    kísérletezik.

CÉLZOTTAN KOMMUNIKÁCIÓS SZÁNDÉKÚ VIZUÁLIS KÖZLÉSEK ÉRTELMEZÉSE,
MEGVITATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. az adott életkornak megfelelő rövid mozgóképi közléseket segítséggel
    elemez a vizuális kifejezőeszközök használatának tudatosítása
    érdekében;

2. egyszerű, mindennapok során használt jeleket felismer;

3. pontosan ismeri államcímerünk és nemzeti zászlónk felépítését,
    összetevőit, színeit;

4. az adott életkornak megfelelő tájékoztatást, meggyőzést,
    figyelemfelkeltést szolgáló, célzottan kommunikációs szándékú
    vizuális közléseket segítséggel értelmez;

5. azonosítja a gyerekeknek szóló vagy fogyasztásra ösztönző, célzottan
    kommunikációs szándékú vizuális közléseket.

ADOTT CÉL ÉRDEKÉBEN EGYSZERŰ VIZUÁLIS KOMMUNIKÁCIÓS MEGJELENÉS
LÉTREHOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. azonosítja a nonverbális kommunikáció eszközeit: mimika, gesztus,
   ezzel kapcsolatos tapasztalatait közlési és kifejezési
   helyzetekben használja;

2. adott cél érdekében egyszerű vizuális kommunikációt szolgáló
   megjelenéseket -- jel, meghívó, plakát -- készít egyénileg vagy
   csoportmunkában;

3. saját kommunikációs célból egyszerű térbeli tájékozódást segítő
   ábrát -- alaprajz, térkép -- készít;

4. időbeli történéseket egyszerű vizuális eszközökkel, segítséggel
   megjelenít.

KREATIVITÁS

ALKOTÓTEVÉKENYSÉG KÖZBEN SAJÁT ÖTLETEK BÁTOR HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. saját történetet alkot, és azt vizuális eszközökkel is tetszőlegesen
   megjeleníti;

2. adott álló- vagy mozgóképi megjelenéseket egyéni elképzelés szerint
   átértelmez;

3. különböző egyszerű anyagokkal kísérletezik, szabadon épít, saját
   célok érdekében konstruál.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. alkotó tevékenység közben önállóan kísérletezik, különböző megoldási
    utakat keres;

2. eszközhasználata begyakorlott, szakszerű;

3. anyaghasználata gazdaságos, átgondolt;

4. feladatokat önállóan megold, eredményeit érthetően bemutatja;

5. adott tanulási helyzetben a tanulási előrehaladás érdekében adekvát
    kérdést tesz fel;

6. feladatmegoldásai során felidézi és alkalmazza a korábban szerzett
    ismereteket, illetve kapcsolódó információkat keres;

7. felismeri a vizuális művészeti műfajok példáit;

8. használja és megkülönbözteti a különböző vizuális médiumok
    kifejezőeszközeit;

9. megkülönböztet művészettörténeti korszakokat, stílusokat;

10. felismeri az egyes témakörök szemléltetésére használt műalkotásokat,
    alkotókat az ajánlott képanyag alapján;

11. használja a térbeli és az időbeli viszonyok megjelenítésének
    különböző vizuális lehetőségeit;

12. példák alapján megérti a képmanipuláció és befolyásolás
    összefüggéseit;

13. példák alapján meg tudja magyarázni a tervezett, épített környezet
    és a funkció összefüggéseit;

14. érti a magyar és egyetemes kulturális örökség és hagyomány
    jelentőségét;

15. csoportban végzett feladatmegoldás során részt vállal a
    feladatmegoldásban, önállóan megtalálja saját feladatát, és
    figyelembe veszi társai álláspontját is;

16. feladatmegoldás során megállapodásokat köt, szabályt alkot, és
    betartja a közösen meghatározott feltételeket;

17. önállóan véleményt alkot, és azt röviden indokolja.

MEGFIGYELÉS, VIZUÁLIS EMLÉKEZET

ÖNÁLLÓ VIZUÁLIS MEGFIGYELÉSEK LEÍRÁSA, BEMUTATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. látványt, vizuális jelenségeket, műalkotásokat önállóan is pontosan,
    részletgazdagon szövegesen jellemez, bemutat;

2. látványok, vizuális jelenségek, alkotások lényeges, egyedi
    jellemzőit kiemeli, bemutatja.

FELADATMEGOLDÁS SORÁN VIZUÁLIS INFORMÁCIÓK ADEKVÁT KERESÉSE, FELIDÉZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. alkotómunka során felhasználja a már látott képi inspirációkat;

2. adott témával, feladattal kapcsolatos vizuális információkat és képi
    inspirációkat keres többféle forrásból.

MEGFIGYELÉSEK ÉRTELMEZHETŐ VIZUÁLIS RÖGZÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. megfigyeléseit, tapasztalatait, gondolatait vizuálisan rögzíti,
    mások számára érthető vázlatot készít.

BELSŐ KÉPALKOTÁS, KÉPZETEK

ELVONT FOGALMAK ÉS BELSŐ KÉPEK ÖSSZEKAPCSOLÁSA, MEGJELENÍTÉSE ÉS
MAGYARÁZAT

A nevelési-oktatási szakasz végére a tanuló:

2. különböző érzetek kapcsán belső képeinek, képzeteinek
    megfigyelésével tapasztalatait vizuálisan megjeleníti;

3. elvont fogalmakat, művészeti tartalmakat belső képek
    összekapcsolásával bemutat, magyaráz és különböző vizuális
    eszközökkel megjelenít.

BELSŐ KÉPEK ÉS ELKÉPZELÉSEK ÖNÁLLÓ VAGY ADOTT SZEMPONTOKNAK MEGFELELŐ
BEMUTATÁSA, VIZUÁLIS MEGJELENÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. adott tartalmi keretek figyelembevételével karaktereket, tereket,
    tárgyakat, helyzeteket, történeteket részletesen elképzel, fogalmi
    és vizuális eszközökkel bemutat és megjelenít, egyénileg és
    csoportmunkában is;

2. a valóság vagy a vizuális alkotások, illetve azok elemei által
    felidézett asszociatív módon generált képeket, történeteket
    szövegesen megfogalmaz, vizuálisan megjelenít, egyénileg és
    csoportmunkában is;

3. szöveges vagy egyszerű képi inspiráció alapján elképzeli és
    megjeleníti a látványt, egyénileg és csoportmunkában is;

4. látványok, képek részeinek, részleteinek alapján elképzeli a látvány
    egészét, fogalmi és vizuális eszközökkel bemutatja és megjeleníti,
    rekonstruálja azt.

VIZUÁLIS ELEMZÉS, VIZUÁLIS ÉRTELMEZÉS

VIZUÁLIS MEGFIGYELÉSEK EREDMÉNYEINEK FELHASZNÁLÁSA KÜLÖNBÖZŐ ELEMZÉSI
HELYZETEKBEN

A nevelési-oktatási szakasz végére a tanuló:

1. a látványokkal kapcsolatos objektív és szubjektív észrevételeket
    pontosan szétválasztja;

2. vizuális problémák vizsgálata során összegyűjtött információkat,
    gondolatokat különböző szempontok szerint rendez és összehasonlít, a
    tapasztalatait különböző helyzetekben a megoldás érdekében
    felhasználja.

VIZUÁLIS MEGJELENÉSEK, KÉPEK, MOZGÓKÉPEK ÖNÁLLÓ ÉRTELMEZÉSE,
ÖSSZEHASONLÍTÁSA TÖBBFÉLE SZEMPONT SZERINT

A nevelési-oktatási szakasz végére a tanuló:

1. vizuális megjelenések, képek, mozgóképek, médiaszövegek vizsgálata,
    összehasonlítása során feltárt következtetéseit megfogalmazza, és
    alkotó tevékenységében, egyénileg és csoportmunkában is
    felhasználja;

2. különböző korok és kultúrák szimbólumai és motívumai közül adott cél
    érdekében gyűjtést végez, és alkotó tevékenységében felhasználja a
    gyűjtés eredményeit.

KÜLÖNBÖZŐ MŰVÉSZETTÖRTÉNETI KOROKBAN, STÍLUSOKBAN KÉSZÜLT ALKOTÁSOK
MEGKÜLÖNBÖZTETÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. különböző művészettörténeti korokban, stílusokban készült
    alkotásokat, építményeket összehasonlít, megkülönböztet és
    összekapcsol más jelenségekkel, fogalmakkal, alkotásokkal, melyek
    segítségével alkotótevékenysége során újrafogalmazza a látványt.

MEGJELENÍTÉS, ÁBRÁZOLÁS, KONSTRUÁLÁS

ADOTT CÉL ÉRDEKÉBEN TÉRBELI FORMA, ÉPÍTMÉNY, TÁRGY TERVEZÉSE,
LÉTREHOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. adott koncepció figyelembevételével, tudatos anyag- és
    eszközhasználattal tárgyakat, tereket tervez és hoz létre, egyénileg
    vagy csoportmunkában is.

TÉRBELI ÉS IDŐBELI VÁLTOZÁSOK ÁLLÓ ÉS MOZGÓKÉPI MEGJELENÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. adott cél és szempontok figyelembevételével térbeli, időbeli
    viszonyokat, változásokat, eseményeket, történeteket egyénileg és
    csoportmunkában is rögzít, megjelenít;

2. ismeri a térábrázolás alapvető módszereit (pl. axonometria,
    perspektíva) és azokat alkotómunkájában felhasználja.

MAGYARÁZAT ÉRDEKÉBEN GONDOLATOK VIZUÁLIS BEMUTATÁSA, ILLUSZTRÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. gondolatait, terveit, észrevételeit, véleményét változatos vizuális
    eszközök segítségével prezentálja.

VIZUÁLIS KIFEJEZÉS

ALKOTÁSOK ELEMZÉSE KIFEJEZŐERŐ ÉS A KÖZVETÍTETT HATÁS ÉRTELMEZÉSÉVEL

A nevelési-oktatási szakasz végére a tanuló:

1. tetszésítélete alapján alkotásokról információkat gyűjt, kifejezőerő
    és a közvetített hatás szempontjából csoportosítja, és
    megállapításait felhasználja más szituációban;

2. megfogalmazza személyes viszonyulását, értelmezését adott vagy
    választott művész alkotásai, társadalmi reflexiói kapcsán.

SZEMÉLYES TÉMÁK, GONDOLATOK VIZUÁLIS MEGJELENÍTÉSE KIFEJEZÉSI SZÁNDÉKNAK
MEGFELELŐ VIZUÁLIS ESZKÖZÖKKEL

A nevelési-oktatási szakasz végére a tanuló:

1. látványok, képek, médiaszövegek, történetek, szituációk feldolgozása
    kapcsán személyes módon kifejezi, megjeleníti felszínre kerülő
    érzéseit, gondolatait, asszociációit;

2. vizuális megjelenítés során egyénileg és csoportmunkában is
    használja a kiemelés, figyelemirányítás, egyensúlyteremtés vizuális
    eszközeit.

DIREKT, CÉLZOTT VIZUÁLIS KÖZLÉS

CÉLZOTTAN KOMMUNIKÁCIÓS SZÁNDÉKÚ VIZUÁLIS KÖZLÉSEK ÉRTELMEZÉSE,
ELEMZÉSE, MEGVITATÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. egyszerű tájékoztató, magyarázó rajzok, ábrák, jelek, szimbólumok
    tervezése érdekében önállóan információt gyűjt;

2. célzottan vizuális kommunikációt szolgáló megjelenéseket értelmez és
    tervez a kommunikációs szándék és a hatáskeltés szempontjait
    kiemelve.

ADOTT CÉL ÉRDEKÉBEN VIZUÁLIS KOMMUNIKÁCIÓS MEGJELENÉS LÉTREHOZÁSA
TÖBBFÉLE, ADEKVÁT TECHNIKÁVAL

A nevelési-oktatási szakasz végére a tanuló:

1. a helyzetek, történetek ábrázolása, dokumentálása során egyénileg
    vagy csoportmunkában is felhasználja a kép és szöveg, a kép és hang
    viszonyában rejlő lehetőségeket;

2. adott témát, időbeli, térbeli folyamatokat, történéseket közvetít
    újabb médiumok képírási formáinak segítségével egyénileg vagy
    csoportmunkában is;

3. nem vizuális információkat (pl. számszerű adat, absztrakt fogalom)
    különböző célok (pl. tudományos, gazdasági, turisztikai) érdekében
    vizuális, képi üzenetté alakít.

KREATIVITÁS

PROBLÉMAMEGOLDÁS ÉRDEKÉBEN RUGALMAS ÉS NYITOTT KÍSÉRLETEZÉS, TÖBBFÉLE
MEGOLDÁS KERESÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. adott téma vizuális feldolgozása érdekében problémákat vet fel,
    megoldási lehetőségeket talál, javasol, a probléma megoldása
    érdekében kísérletezik;

2. nem konvencionális feladatok kapcsán egyéni elképzeléseit, ötleteit
    rugalmasan alkalmazva megoldást talál.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--10.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. feladatmegoldás közben kísérletezik, különböző megoldási utakat
    keres, és törekszik az egyéni megoldás igényes kivitelezésére;

2. eszköz- és anyaghasználat során adekvát döntést hoz;

3. adott tanulási helyzetben a tanulási előrehaladás érdekében
    problémákat keres, kérdéseket tesz fel, és ezekre önállóan is keresi
    a megoldásokat és válaszokat;

4. feladatmegoldásai során a felhasználás érdekében hatékonyan
    szelektálja a korábban szerzett ismereteket;

5. feladatmegoldásai során saját ötleteit és eredményeit bátran
    bemutatja;

6. példák alapján érti a művészet kultúraközvetítő szerepét;

7. példák alapján művészettörténeti korszakokat, stílusokat felismer és
    egymáshoz képest időben viszonylagos pontossággal elhelyez;

8. példák alapján felismeri és értelmezi a kortárs művészetben a
    társadalmi reflexiókat;

9. érti és meg tudja magyarázni a célzott vizuális közlések
    hatásmechanizmusát;

10. ismeri néhány példáját a digitális képalkotás közösségi médiában
    használt lehetőségének;

11. felismeri a designgondolkodás sajátosságait az őt körülvevő tárgy-
    és környezetkultúra produktumaiban;

12. a fenntarthatóság érdekében felelős döntéseket hoz a saját
    tervezett, épített környezetével kapcsolatban;

13. csoportban végzett feladatmegoldás során részt vállal a
    feladatmegoldásban, önállóan megtalálja saját feladatát, figyelembe
    veszi társai álláspontját, de az optimális eredmény érdekében
    hatékonyan érvényesíti érdekeit;

14. önálló véleményt alkot, és azt meggyőzően indokolja;

15. adott területen megtalálja a számára érdekes és fontos kihívásokat.

MEGFIGYELÉS, VIZUÁLIS EMLÉKEZET

ÖNÁLLÓ FELADATMEGOLDÁSOKBAN A VIZUÁLIS MEGFIGYELÉS ÉS MEGISMERÉS ADEKVÁT
HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. a látható világ vizuális összefüggéseinek megfigyeléseit ok-okozati
    viszonyoknak megfelelően rendszerezi;

2. alkotó és befogadó tevékenységei során érti, és komplex módon
    használja a vizuális nyelv eszközeit.

FELADATMEGOLDÁS SORÁN VIZUÁLIS INFORMÁCIÓK FELIDÉZÉSE ÉS KÖVETKEZETES
ÖSSZEGYŰJTÉSE A MEGOLDÁS ÉRDEKÉBEN

A nevelési-oktatási szakasz végére a tanuló:

1. a vizuális megjelenések mintáinak önálló megfigyelése és felismerése
    által konstrukciókat alkot, e megfigyelések szempontjainak
    összekapcsolásával definiál és következtet, mindezt társaival
    együttműködve alkotótevékenységébe is beilleszti;

2. adott feladatmegoldás érdekében meglévő vizuális ismeretei között
    megfelelően szelektál, a további szakszerű információszerzés
    érdekében adekvátan keres;

3. az alkotótevékenység során szerzett tapasztalatait önálló
    feladatmegoldás során beépíti, és az eredményes feladatmegoldás
    érdekében szükség szerint továbbfejleszti.

BELSŐ KÉPALKOTÁS, KÉPZETEK

FELADATMEGOLDÁS SORÁN A BELSŐ KÉPALKOTÁS LEHETŐSÉGÉNEK SZABAD
FELHASZNÁLÁSA ÉS HITELES MAGYARÁZATA

A nevelési-oktatási szakasz végére a tanuló:

1. alkotó feladatmegoldásai során az elraktározott, illetve a
    folyamatosan újraalkotott belső képeit, képzeteit szabadon párosítja
    a felkínált tartalmi elemek és látványok újrafogalmazásakor, amelyet
    indokolni is tud;

2. új ötleteket is felhasznál képek, tárgyak, terek megjelenítésének,
    átalakításának, rekonstruálásának megvalósításánál síkbeli, térbeli
    és időbeli produktumok létrehozása esetében.

VIZUÁLIS ELEMZÉS, VIZUÁLIS ÉRTELMEZÉS

VIZUÁLIS TAPASZTALATOK ÉS TUDÁS EREDMÉNYEINEK FELHASZNÁLÁSA KÜLÖNBÖZŐ
ELEMZÉSI HELYZETEKBEN BONYOLULTABB KÖVETKEZTETÉSEK ÖNÁLLÓ
MEGFOGALMAZÁSÁVAL

A nevelési-oktatási szakasz végére a tanuló:

1. a vizuális megjelenések elemzése és értelmezése során a befogadó és
    az alkotó szerepkört egyaránt megismerve reflexióit szemléletesen és
    szakszerűen fogalmazza meg szövegesen és képi megjelenítéssel is;

2. a művészi hatás megértése és magyarázata érdekében összehasonlít, és
    következtetéseket fogalmaz meg a különböző művészeti ágak kifejezési
    formáival kapcsolatban.

VIZUÁLIS ELEMZÉSI, ÉRTELMEZÉSI LEHETŐSÉGEK TANULÁSI CÉLNAK MEGFELELŐ
FELHASZNÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. adott és választott vizuális művészeti témában önállóan gyűjtött
    képi és szöveges információk felhasználásával részletesebb
    helyzetfeltáró, elemző, összehasonlító projektmunkát végez.

VIZUÁLIS MEGJELENÉSEK, KÉPEK, MOZGÓKÉPEK ADOTT SZEMPONT SZERINTI
ÉRTELMEZÉSE, ÖSSZEHASONLÍTÁSA, KÖVETKEZTETÉSEK MEGFOGALMAZÁSA AZ
ÖSSZEFÜGGÉSEK HANGSÚLYOZÁSÁVAL

A nevelési-oktatási szakasz végére a tanuló:

1. megfelelő érvekkel alátámasztva, mérlegelő szemlélettel viszonyul az
    őt körülvevő kulturális környezet vizuális értelmezéseinek mediális
    csatornáihoz, amit társaival is megvitat;

2. különböző mediális produktumokat vizuális jelrendszer, kommunikációs
    szándék és hatáskeltés szempontjából elemez, összehasonlít, és
    következtetéseit társaival is megvitatja.

KULTÚRÁK, MŰVÉSZETTÖRTÉNETI KOROK ÉS STÍLUSOK LEGFONTOSABB JELLEMZŐINEK
ÉS ÖSSZEFÜGGÉSEINEK ÉRTELMEZÉSE ÉS ÖSSZEHASONLÍTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. érti és megkülönbözteti a klasszikus és a modern művészet
    kultúrtörténeti összetevőit, közlésformáinak azonosságait és
    különbségeit;

2. adott vagy választott kortárs művészeti üzenetet személyes
    viszonyulás alapján, a társadalmi reflexiók kiemelésével értelmez;

3. adott szempontok alapján érti és megkülönbözteti a történeti korok
    és a modern társadalmak tárgyi és épített környezetének legfontosabb
    jellemzőit, miközben értelmezi kulturális örökségünk jelentőségét
    is.

AZ ÉPÍTETT, TERVEZETT KÖRNYEZET ELEMZŐ VIZSGÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. személyes élményei alapján elemzi a tárgy- és környezetkultúra,
    valamint a fogyasztói szokások mindennapi életre gyakorolt hatásait
    és veszélyeit, és ezeket társaival megvitatja.

MEGJELENÍTÉS, ÁBRÁZOLÁS, KONSTRUÁLÁS

SAJÁT KONCEPCIÓNAK MEGFELELŐ TÉRALAKÍTÁS

A nevelési-oktatási szakasz végére a tanuló:

1. az adott vagy a választott célnak megfelelően környezetátalakítás
    érdekében, társaival együttműködésben, környezetfelméréssel
    alátámasztva tervet készít, amelyet indokolni is tud.

ADOTT KONCEPCIÓ ÉRDEKÉBEN TÉRBELI ÉS IDŐBELI VÁLTOZÁSOK MOZGÓKÉPI
MEGJELENÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. bemutatás, felhívás, történetmesélés céljából térbeli és időbeli
   folyamatokat, történéseket, cselekményeket különböző eszközök
   segítségével rögzít.

GONDOLATOK VIZUÁLIS BEMUTATÁSA, ILLUSZTRÁLÁSA MAGYARÁZAT CÉLJÁBÓL

A nevelési-oktatási szakasz végére a tanuló:

1. adott feladatnak megfelelően alkalmazza az analóg és a digitális
   prezentációs technikákat, illetve az ezekhez kapcsolható álló- és
   mozgóképi lehetőségeket;

2. tervezési folyamat során a gondolkodás szemléltetése érdekében
   gondolatait mások számára is érthetően, szövegesen és képpel
   dokumentálja.

VIZUÁLIS KIFEJEZÉS

SZEMÉLYES TÉMÁK, GONDOLATOK HITELES VIZUÁLIS MEGJELENÍTÉSE A VÁLASZTOTT
MÉDIUM SAJÁTOSSÁGAINAK ÉRVÉNYESÍTÉSÉVEL.

A nevelési-oktatási szakasz végére a tanuló:

1. képalkotás és tárgyformálás során autonóm módon felhasználja
   személyes tapasztalatait a hiteles kifejezési szándék érdekében a
   választott médiumnak is megfelelően;

2. saját munkáit bátran újraértelmezi és felhasználja további
   alkotótevékenység során;

3. vizuális megjelenéseket, alkotásokat újraértelmez, áttervez és
   módosított kifejezési szándék vagy funkció érdekében újraalkot.

CÉLZOTT, DIREKT VIZUÁLIS KÖZLÉS

KÖZLÉSI SZÁNDÉKNAK MEGFELELŐ ÁLLÓ ÉS MOZGÓKÉPI KÖZLÉS LÉTREHOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. valós célokat szolgáló, saját kommunikációs helyzetnek megfelelő,
   képes és szöveges üzenetet felhasználó vizuális közlést hoz létre
   társaival együttműködésben is;

2. szabadon választott témában, társaival együtt ok-okozati
   összefüggéseken alapuló történetet alkot, amelynek részleteit
   vizuális eszközökkel is magyarázza, bemutatja.

KÖNNYEN ELÉRHETŐ, EGYSZERŰ TECHNIKAI LEHETŐSÉGEK, ALKALMAZÁSOK
HASZNÁLATA A KÉPALKOTÁS ÉRDEKÉBEN

A nevelési-oktatási szakasz végére a tanuló:

1. adott téma újszerű megjelenítéséhez illő technikai lehetőségeket
   kiválaszt, és adott vizuális feladatmegoldás érdekében megfelelően
   felhasznál.

MEDIÁLIS ÜZENETEK MÉRLEGELŐ SZEMLÉLETŰ ÉRTELMEZÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. technikai képnél és a számítógépes környezetben felismeri a
   manipuláció lehetőségét, és érti a befolyásolás vizuális
   eszközeinek jelentőségét.

KREATIVITÁS

KÍSÉRLETEZÉS PROBLÉMAMEGOLDÁS ÉRDEKÉBEN AZ ÖTLETEK VIZUÁLIS
MAGYARÁZATÁVAL ÉS RÉSZLETES KIDOLGOZÁSÁVAL

A nevelési-oktatási szakasz végére a tanuló:

1. adott feladatmegoldás érdekében ötleteiből rendszert alkot, a célok
   érdekében alkalmas kifejezési eszközöket és technikákat választ,
   az újszerű ötletek megvalósítása érdekében szabályokat újraalkot.

KREATÍV PROBLÉMAMEGOLDÁS SORÁN HATÉKONY EGYÜTTMŰKÖDÉS A TÁRSAKKAL

A nevelési-oktatási szakasz végére a tanuló:

1. egyéni munkáját hajlandó a közösségi alkotás érdekei alá rendelni, a
   hatékonyság érdekében az együttműködésre törekszik.

FELADATMEGOLDÁS SORÁN STRATÉGIAI SZEMPONTBÓL IS ÉRTELMEZHETŐ DÖNTÉS
ELÉRÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. a leghatékonyabb megoldás megtalálása érdekében felméri a
   lehetőségeket és azok feltételeit, amelyek komplex mérlegelésével
   döntést hoz az adott feladatokban.

***II.3.7.4. MOZGÓKÉPKULTÚRA ÉS MÉDIAISMERET***

***A) ALAPELVEK, CÉLOK***

Az interperszonális és a tömegkommunikáció, az elmúlt évtizedekben, és
különösen az utóbbi években alapvető változásokon ment keresztül,
köszönhetően a technikai-technológiai fejlődésnek és az ezzel
összefüggésben jelentősen átalakuló fogyasztói magatartásnak. Az
audiovizuális művészeti alkotások a permanens digitális-technológiai
forradalom következtében a mindennapok szerves részévé váltak a
fogyasztói társadalomban. A mozgóképkultúra és médiaismeret tanításának
célja az alapvető médiaműveltség megszerzése, különös tekintettel a
mozgóképi szövegértés fejlesztésére, a média társadalmi szerepének és
működésmódjának feltárására. Olyan képesség- és személyiségfejlesztő
eszközrendszer, amely szükséges ahhoz, hogy a tanulók magabiztosan
tudjanak tájékozódni és választani a médiumok világában, hogy az értő,
mérlegelő gondolkodással egyenrangú résztvevői lehessenek az új
társadalmi színtereken zajló érintkezésnek. A tömegkommunikációs
eszközök átalakulása, a közvetített információk mennyiségének folyamatos
emelkedése és ellenőrizetlensége új kihívások elé állítja a jelen és
jövő felnövekvő nemzedékeit. A tantárgy tanulása során a tanuló
megismeri az egyes médiatípusok alapelveit, az általuk közvetített
információk közötti szelekciós készség kialakítása, a nemzeti
önazonosságtudat és a hazafias érzés megerősítése. A felelős, a
családjáért és nemzetéért tenni akaró állampolgárrá váláshoz
elengedhetetlen a médiaműveltség megszerzése. A mozgóképi alkotások a
művészeti nevelés részeként erősítik a tanulók kulturális identitását,
tágítják látókörüket és fejlesztik az esztétikai ismereteiket. A
tantárgy tanítása szervesen kapcsolódik a digitális kultúra, a magyar
nyelv és irodalom tantárgyakhoz, valamint a művészetek tanulási
területhez.

A tantárgy tartalmi elemei és fejlesztési céljai, feladatai között
egyaránt szerepelnek művészetpedagógiai, kommunikációs, technológiai,
társadalomismereti, valamint az anyanyelvi kultúrával, a hazaszeretetre
neveléssel kapcsolatos összetevők is.

**A mozgóképkultúra és médiaismeret tanulásának célja, hogy a tanuló:**

1. megismerje és megértse a média és a tömegkommunikáció felépítését,
    működési alapelveiket, műfajaikat,

2. a mérlegelő gondolkodását fejlesztve, tudatosan és értékalapon
    válasszon az egyes médiatartalmak közül, képes legyen rendszerezve,
    többféle nézőpontból vizsgálni a digitális térben megjelenő
    információkat,

3. képessé váljon meghatározott publicisztikai és audiovizuális
    műfajokban kommunikációs tartalmakat létrehozni,

4. alakuljon ki benne a kereskedelmi kommunikációval és reklámmal
    kapcsolatos megfontolt fogyasztói szerep,

5. tudatosan, a lehetőségeket és a veszélyeket felmérve vegyen részt az
    online kommunikációban,

6. ismerje meg az értékhordozó audiovizuális művek, különösen az
    európai és a magyar filmművészet alkotásait, fordítson figyelmet a
    hazai audiovizuális kulturális örökség védelmére,

7. erősödjön az esztétikai érzéke, a filmművészeti alkotások
    segítségével váljon érzékenyebbé a nemzeti sorskérdéseink iránt.

### A tantárgy tanításának specifikus jellemzői a 12. évfolyamon

A mozgóképkultúra és médiaismeret tantárgy speciális abból a
szempontból, hogy a tanítás során átadandó ismeretek és fejlesztendő
készségek jelentős részben direkt módon a jelenkor kihívásainak felelnek
meg, hiszen a tanuló a hétköznapi életben folyamatosan szembesül,
találkozik a tantárgy témaköreivel, felvetett problémáival. Az internet
és a digitális eszközök a mindennapok szerves részévé váltak a
középiskolások számára. Az őket érő hatások és kihívások folyamatos
megújulást, a technológiai fejlődés nyomon követését igénylik a
pedagógiai munka során. A média szemléletformáló szerepe a 12.
évfolyamon már a történelem tantárgyban is erőteljesen megjelenik a
politika- és társadalomtörténeti tanulmányok során, valamint az
állampolgári ismeretek keretében, míg azokkal a lehetőségekkel és
eszközökkel, amelyeket ma nap mint nap használ a tanuló, a digitális
kultúra tantárgy részeként találkozik.

A tanuló a különféle médiatartalmak befogadásával és elemzésével
megtanulja rendszerezni a különféle forrásból származó információkat,
azok összevetésével, vizsgálatával képet alkot az ábrázolt valóságról. A
mérlegelő gondolkodás készségének fejlesztésével képessé válik arra,
hogy az egyes médiatartalmakat rendszerezze, előismeretei, valamint a
tantárgy tanulása során szerzett ismeretei segítségével több nézőpontú
megközelítéssel vizsgálja a tömeg- és közösségi kommunikációs térben
érkező információkat.

A tanuló megismeri a nyomtatott és elektronikus sajtó műfajait, azok
célrendszerét, közlésmódjuk sajátosságait. Képet alkot a társadalmi
véleményformáló szerepükről, helyükről, az egyénre és az egyes
társadalmi csoportokra, közösségekre gyakorolt hatásukról. A
nevelő-oktató munka során törekedni kell arra, hogy a tantárgy keretében
lehetőség nyíljon tapasztalati úton is megismerni a média működését egy
írott vagy elektronikus sajtóorgánum szerkesztőségének meglátogatásával,
illetve iskolaújság -- írott és online --, vlog, blog, iskolarádió,
online televízióadás működtetésével.

A tanuló szövegértési, szövegalkotási készségei 12. évfolyamon már
lehetővé teszik, hogy az egyes publicisztikai műfajokban általa
választott és meghatározott témákban egyaránt szövegeket alkosson,
illetve kapcsolódva történelmünkhöz, nemzeti kultúránkhoz, a
Kárpát-medence természeti kincseihez, audiovizuális formában, az okos
eszközöket felhasználva digitális tartalmakat készítsen. Megismerkedik a
médiaetika fogalmával, fogékonnyá válik a saját és mások személyiségi
jogainak tiszteletben tartására a kommunikációs térben is. Képessé válik
az alapvető adatvédelmi és információbiztonsági szabályok betartására az
interperszonális és a közösségi médiában történő kommunikációja során
egyaránt.

A magyar nyelv és irodalom, valamint a vizuális kultúra tantárgyak során
már kialakult a tanulóban a művészeti alkotások befogadásának, az
esztétikai élmény megélésének és kifejezésének a készsége. A magyar
filmtörténet klasszikusainak megismerése erősíti a tanuló nemzettudatát,
segít megérteni az elmúlt évtizedek világát, szülei, nagyszülei
gondolkodásmódját, szűkebb és tágabb környezetének múltját, bemutatja a
társadalmi interakciókat, illetve azok lenyomatait. A művészeti nevelés
ezen ága segíti megérteni az egyén és a közösség viszonyát, fejleszti a
tanuló érzelmi intelligenciáját. A tantárgy tanítása során törekedni
kell arra, hogy az alkotások befogadását elősegítve fejlődjön a tanuló
vitakultúrája is. Megtanulja az érzelmek, benyomások kifejezését,
értelmezi a képi ábrázolást a filmművészeti alkotásokban.

A tanulási folyamatban törekedni kell az önálló munkavégzésre, a
folyamatos reflexióra, az élmény- és tevékenységalapú munkamódszerekre
és a változatos munkaformákra.

Az értelmező kulcsfogalmak használata segíti a tanulót az összefüggések
feltárásában és megértésében, a tartalmi kulcsfogalmak tudatos
használata pedig segíti a tantárgy ismereteinek rendszerezését,
felidézését és alkalmazását.

*Értelmező kulcsfogalmak:* kultúra, tömegkultúra, identitás, esztétikum,
véleményformálás, nevelés, médiaetika, ok-okozati összefüggés, stílus,
interaktivitás.

*Tartalmi kulcsfogalmak:* média, tömegkommunikáció, közösségi média,
kereskedelmi média, közszolgálati média, nyilvánosság, médiatörvény,
sajtó, publicisztikai műfajok, hír, hírérték, film, archetípus,
audiovizualitás.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 12. ÉVFOLYAMON***

1. ***A média fogalma és kifejezőeszközei: műfajok, eszközök, írott és
    elektronikus sajtó; hír fogalma, típusai; a hírérték.***

2. ***A tömegkommunikáció fogalma, eszközei: nyilvánosság, hálózati
    kommunikáció, nonprofit, kereskedelmi, a közszolgálati és a
    közösségi média szerepe; a tömegtájékoztatás eszközei, felelőssége;
    az online kommunikáció lehetőségei és veszélyei.***

3. ***Tudatos médiahasználat az egyén és a társadalom szempontjából:
    szellemi önvédelem, a kereskedelmi célú médiatartalmak kezelése --
    tudatos fogyasztói magatartás kialakítása; az információbiztonság, a
    közszféra és a magánszféra a médiában.***

4. ***A média társadalmi szerepe, használata: médiaetika,
    médiaszabályozás, információáramlás irányítása, a véleményformálás
    lehetőségei, feladatai.***

5. ***A mozgóképi közlésmód kifejezőeszközei; szövegépítkezés a
    hagyományos és az új médiában, képi eszközök a digitalizáció előtt
    és most; az audiovizuális elemek esztétikai szerepe a művészi
    alkotásoknál, és céljai a kereskedelmi célú tartalomszolgáltatásban.
    ***

6. ***Kultúra és tömegkultúra: hatása az egyénre és a társadalomra;
    esztétikai minőség és a fogyasztói társadalom kölcsönhatása;
    jelenségek a médiában és a filmművészetben -- sztárok, szerepek,
    sztereotípiák, életformák; a virtuális valóság.***

7. ***A magyar film: alkotók és alkotások; a magyar filmművészet
    jelentősége és értékei a nemzeti kultúrában; a magyar film
    korszakai, sajátosságai; jeles magyar rendezők és színészek.***

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 12.
ÉVFOLYAMON***

### A nevelési-oktatási szakasz végére a tanuló

1. Rendelkezik átfogó egyetemes és magyar médiatörténeti ismeretekkel.
    Ismeri a különböző médiumok specifikumait, a fontosabb műfajokat,
    szerzőket, műveket.

2. Képes különböző médiatermékek értő befogadására. Ismeri a különböző
    médiumok formanyelvének alapvető eszköztárát, érzékeli ezek hatását
    az értelmezés folyamatára, valamint képes ezeknek az eszközöknek
    alapszintű alkalmazására is saját környezetében.

3. Rövid média-alkotások tervezése és kivitelezése révén szert tesz a
    különböző médiumok szövegalkotási folyamatainak elemi
    tapasztalataira, érti a különféle szövegtípusok eltérő működési
    elvét, s tud azok széles spektrumával kreatív befogadói viszonyt
    létrehozni.

4. Ismereteket szerez a filmkultúra területéről: érti a szerzői kultúra
    és a tömegkultúra eltérő működésmódját; felismeri az elterjedtebb
    filmműfajok jegyeit; különbséget tud tenni a különböző
    stílusirányzatokhoz tartozó alkotások között.

5. Ismeri a magyar filmművészet főbb szerzőit, jellegzetességeit és
    értékeit.

6. Tisztában van a sztárfogalom kialakulásával és módosulásával.
    Azonosítani tudja a sztárjelenséget a filmen és a médiában. Képes
    ismert filmszereplők és médiaszemélyiségek imázsának elemzésére, a
    háttérben fellelhető archetípusok meghatározására.

7. Ismeri a modern tömegkommunikáció fő működési elveit, jellemző
    vonásait, érti, milyen társadalmi és kulturális következményekkel
    járnak a kommunikációs rendszerben bekövetkező változások, ezek
    hatásait saját környezetében is észreveszi.

8. Értelmezni tudja a valóság és a média által a valóságról kialakított
    „kép" viszonyát, ismeri a reprezentáció fogalmát, és rendelkezik a
    médiatudatosság képességével.

9. Ismeri a médiareprezentáció, valószerűség és hitelesség
    kritériumait, a fikciós műfajok illetve a dokumentumjelleg
    különbségeit, a sztereotípia, tematizáció, valóságábrázolás,
    hitelesség, hír fogalmait. Képes saját értékelő viszonyt kialakítani
    ezekkel kapcsolatban.

10. Tisztában van a médiaipar, médiafogyasztás és -befogadás
    jellemzőivel. Ismeri a kereskedelmi, közszolgálati és nonprofit
    média, az alkotói szándék, célcsoport, a közönség, mint vevő és áru,
    a médiafogyasztási szokások jellegzetességeit, a médiafüggőség
    tüneteit.

11. Ismeri az internet-világ jelenségeit, a globális kommunikáció adta
    lehetőségeket. Érti a hálózati kommunikáció és a közösségi média
    működési módját, képes abban felelősen részt venni, ismeri és
    megfelelően használja annak alapvető szövegtípusait. Képes igényes
    önálló tartalmak alkotására és részt venni a lokális
    nyilvánosságban.

12. Ismeretekkel rendelkezik a médiaetika és a médiaszabályozás főbb
    kérdéseit illetően, saját álláspontot tud megfogalmazni azokkal
    kapcsolatban. Érti a média etikai környezetének és jogi
    szabályozásának tétjeit.

13. Tisztában van a média véleményformáló szerepével, az alkotók és
    felhasználók felelősségével, az egyének és közösségek jogaival.
    Ismeri a norma és normaszegés fogalmait a médiában.

14. Tudatosítja magában a nyilvános megszólalás szabadságával és
    felelősségével járó lehetőségeket és lehetséges következményeket,
    tisztában van a digitális zaklatás veszélyeivel. Tudatos
    médiahasználóként állást tud foglalni a média által közvetített
    értékek minőségével kapcsolatban.

*II.3.8. Technológia*

A Technológia tanulási területen szerzett tanulási tapasztalatok
közvetítésével a tanuló megismeri, hogy a korszakos jelentőségű
technológiai találmányok miként változtatták meg a társadalom működését
és a társadalmat alkotó egyének és csoportok mindennapi életét.
Tanulmányai eredményeként a tanuló a technológiák ismeretére és
magabiztos alkalmazására tesz szert a különböző társas környezetekben,
otthon, a munkahelyen és a szabadidős közösségekben. Az elsajátított
tudás és tudásalkalmazás lehetővé teszi, hogy tájékozott fogyasztóvá és
a termelésben kezdeményezően részt vevő vállalkozóvá váljon, valamint
mérlegelni tudja a termékeknek és szolgáltatásoknak az életminőségre
gyakorolt hatásait. Fontos cél, hogy a digitális technológiákat
megfelelő színvonalon tudja alkalmazni munkájában, a mindennapi életben,
továbbá ismerje ezeknek a globális kommunikációban betöltött szerepét,
előnyeit, hátrányait és veszélyeit. A tanuló olyan ismeretekre és
készségekre tesz szert, amelyek a tárgyak alkotásához, működő
szerkezetek létrehozásához vezető egyszerű és összetett munkafolyamatok
végrehajtására teszik alkalmassá. A tanulási területen szerzett tudás a
fejlett technológiai eszközök kialakítását megalapozó mérnöki és
technológiai gondolkodás sajátos jellemzőinek és jelentőségének
felismerését, valamint a technológiai újítások és a tudomány
összekapcsolódásának megértését segíti elő. Az elsajátított ismeretek és
készségek támogatják az olyan döntésekben való informált részvételt,
amelyek a környezetre, a fenntarthatóságra, valamint a kultúra egészére
vonatkozó következményekkel járnak. A tanuló tapasztalatot szerez a
munka világában szerepet játszó főbb készségekről, kompetenciákról, ezek
fejlesztésének lehetőségeiről és módjáról. A tanulási terület tantárgyi
célkitűzései alapján megvalósított tanulás és tanítás olyan ismeretek
megszerzését teszi lehetővé, amelyek kiterjednek a szakirányú
tanulmányok és foglalkozások körére, hozzájárulva ezzel a választható
foglalkozásokkal összefüggő jövőbeli elképzelések kialakulásához.

A Technológia tanulási terület a tanulás-tanítás során olyan
tevékenységeket kínál, melyek kutatásvezérelt probléma-meghatározást és
-megoldást igényelnek, új és ismeretlen anyagok felkutatását,
szerkezetek működésének megismerését, technológiai fogalmak
elsajátítását, készségek kialakítását és fejlesztését is lehetővé
teszik, valamint produktumok létrehozását eredményezik. Ezek a
tevékenységek hozzájárulnak az alkotó és mérlegelő gondolkodás
fejlesztéséhez, valamint a vállalkozói és innovatív készségek
megszerzéséhez. A tevékenységek által formálódó kompetenciák különösen
nagy jelentőségre tesznek szert a középfokú és felsőfokú tanulmányok
befejezését követően, amikor a tanulók az iskolát elhagyva a nemzeti és
a globális gazdaság szereplőivé válnak, és bekapcsolódnak a munka
világába.

A Technológia tanulási terület olyan tantárgyakat kapcsol egymáshoz,
amelyek eltérő tartalommal és célrendszerrel rendelkeznek, de
szemléletükben közös vonásokat jelenítenek meg: a digitális kultúra,
valamint a technika és tervezés.

A digitális kultúra tantárgy olyan kompetenciák megszerzését teszi
lehetővé, amelyek a mindennapi életben nélkülözhetetlenek, és elősegítik
az információs társadalom változásaihoz történő folyamatos
alkalmazkodást. A technológia fejlődése, az információ szerepének
felértékelődése, az egyén digitális környezetben elfoglalt helye és a
közösségi kapcsolatok új típusú tudást igényelnek. A tantárgy olyan
naprakész ismeretek átadását és olyan készségek kialakítását teszi
lehetővé, amelyek biztos alapot kínálnak a tanuló számára az információs
társadalomba való sikeres beilleszkedéshez. A tantárgy keretében végzett
tevékenység biztosítja a digitális kompetenciák megszerzését,
alkalmazását, egyúttal megteremti az elsajátított tudás aktív, önálló
bővítésének a lehetőségét olyan új ismeretekkel, melyek elérése a
digitális kommunikációs csatornák és eszközök igénybevételének
segítségével valósulhat meg. A digitális kompetenciák a reális
önértékelésben és önérvényesítésben, az informált adatfelhasználásban,
az információszerzésben és -szelektálásban, valamint az egyén és a
közösség által létrehozott eredményeknek és produktumoknak a digitális
környezetben történő megosztásában játszhatnak szerepet.

A digitális eszközökkel megvalósított hatékony és kreatív
problémamegoldás, tudásépítés és együttműködés kialakítását azok az
ismeretek, készségek és attitűdök alapozzák meg, amelyeket a tanuló a
digitális kultúra tantárgy tanulása során sajátít el, szerez meg. A
tantárgyi tevékenységek révén kialakított kompetenciák túlmutatnak e
tanulási terület keretein, beágyazódnak más tantárgyak tanulásába.

A technika és tervezés tantárgy a klasszikus és a korszerű, megújult
technika ismereteire alapozva a tanuló gyakorlati tevékenységét helyezi
előtérbe. A tantárgyi tevékenységek során teret kap a kétkezi munka és a
korszerű technológiára támaszkodó digitális anyagmegmunkálás. A
hagyományokat és értékeket megőrző tartalom kiegészül a 21. században
elvárt tudástartalmakkal. A tanulók olyan feladatokat kapnak, melyek
tevékenységelemeinek végzése megvilágítja számukra, mit jelent a
környezet szervezett átalakítása, miként járulnak hozzá a tudományos
eredmények a technikai újításokhoz, milyen speciális tudást igényel a
korszerű eszközök használata, milyen körülmények között lehetséges és
milyen eredményekre vezet az emberi és gépi munkával végzett
tevékenység. A tanulási folyamat gamifikált környezetben, hagyományos és
digitális alkotóműhelyekben, csoportos tevékenység keretében valósul
meg, ahol lehetőség nyílik rá, hogy a tanulók segítsenek egymásnak,
ugyanakkor tanuljanak is egymástól. A tevékenység mozgatórugója az
alkotás iránti vágy és a kész alkotás felett érzett büszkeség. Az ember
környezet átalakító tevékenységének, felelősségének megismerése,
megértése, az ehhez kapcsolódó erkölcsi és etikai kérdések feltárása, az
etikus magatartás kialakítása a tudás elsajátításának elválaszthatatlan
részét képezi. A tantárgy sokféle és különböző bonyolultsági szintű
feladat segítségével közvetíti a környezet-átalakítás eljárásainak,
folyamatainak, technológiáinak összefüggéseit, s mindehhez biztosítja,
hogy a tanulók technológiai készségeinek kialakítása aktív tanulás
keretében valósuljon meg.

***II.3.8.1. DIGITÁLIS KULTÚRA***

***A) ALAPELVEK, CÉLOK***

A digitális kultúra tantárgy célja olyan naprakész ismeretek és
készségek átadása és kialakítása, amelyek a tanulót az információs
társadalom sikeres és hasznos tagjává teszik.

A tantárgy keretében fontos szerepet kap az algoritmizálás és kódolás,
mivel elősegíti az olyan kompetenciák fejlesztését, mint a problémák
digitális környezetben történő megoldása, a kreativitás, az
együttműködés és a logikus gondolkodás. A tantárgy tanulása-tanítása
során kialakított kompetenciákat a tanuló képes lesz egyéb
tudásterületeken is alkalmazni, megszerzi az alapvető digitális
kompetenciákat.

A digitális kultúra tantárgy fontos feladata, hogy a tanuló képes legyen
a felmerülő problémákat a digitális környezet eszközeivel megoldani,
igénybe tudja venni az információs társadalom, e-Világ szolgáltatásait,
eleget tudjon tenni az állampolgári kötelességeinek.

A foglalkozások tervezésében és lebonyolításában -- az eltérő
tudásszinttel rendelkező tanulók fejlesztése terén -- nagy lehetőségeket
kínál a digitális technológia alkalmazása. A jelenkor kihívásaira
reagálva az iskolai tanulás és különösképpen a digitális kultúra
tantárgy feladata, hogy támogassa a fiatalokat a technológiával való
kapcsolattartásban, segítsen nekik kibővíteni és kiterjeszteni a
technológia használatát a projektfeladatok teljesítésében, az önálló és
csoportos tanulásban, az önképzésben, szem előtt tartva a kreatív
alkalmazás ösztönzését.

A digitális kultúra tantárgy tanulásának legalapvetőbb célja, hogy a
tanuló:

1. *megszerezze a digitális írástudás, a problémamegoldás és az
    információs technológia -- mint a tantárgy három fő témakörének --
    ismereteit;*

2. *felkészüljön a digitális kompetenciák széles körű alkalmazására
    úgy, hogy arra a más tudásterületekhez tartozó tananyagok
    feldolgozásakor már építeni tudjon;*

3. *rendszerezni tudja a digitális eszközök más forrásokból származó
    tudáselemeit;*

4. *ismerje a digitális eszközök használatával járó veszélyek
    kezelését, az ellenük való védekezést;*

5. *fejlessze tudatos felhasználói attitűdjét mind az egyén, mind a
    közösség, mind a társadalom szintjén;*

6. *megtanulja a problémák digitális eszközökkel való megoldásának
    módjait, beleértve egy adott probléma megoldásához szükséges
    algoritmusok értelmezését, kiválasztását, módosítását, illetve
    létrehozását.*

***A tantárgy tanításának specifikus jellemzői a 3--4. évfolyamon***

Az alsó tagozat 3--4. évfolyamán vezérelvként a digitális kultúra
életkori sajátosságokhoz illeszkedő, tevékenység- és cselekvésközpontú
tanulása, valamint alkalmazása kerül előtérbe. E szakasz elsődleges
feladata az attitűdformálás és a képességfejlesztés. A tanuló olyan
tapasztalatokhoz jut ezen a területen, melyek a 8--10 éves életkorban
releváns digitális környezetre, a felmerülő, digitális eszközökkel
megoldható problémákra adnak valós, kézzelfogható példákat.

A nevelés-oktatás minden esetben a tanuló tapasztalataiból, érzékszervi
megfigyeléseiből, a játéktevékenység során felmerülő szituációkból indul
ki. A tanulás fő útja az egyéni, párban történő és csoportos
tapasztalatszerzés, az információk egyéni vagy közös, cselekvéshez
kapcsolt feldolgozása. E tevékenység során mód nyílik egyéni utak
felfedeztetéses tanulás során való kipróbálására, az egyéni konstrukciók
megvalósítására.

A digitális kultúra tantárgyi órakeretben való tanulására 3. osztálytól
kerül sor, ám fontos szerepe van az ezt megelőző tapasztalatoknak is,
melyeket 1--2. osztályban, a digitális tananyagok használatakor, a
digitális eszközökkel megvalósuló képességfejlesztés, differenciálás
alkalmával szerez meg a tanuló.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

A mindennapi életben használt digitális eszközökkel megvalósított
megoldások megismerése a tantárgy hangsúlyos célját alkotja. A tanulás
és tanítás egyik feladata a tanuló eltérő informális tanulási utakon
összegyűjtött ismereteinek rendszerezése, kiegészítése. A tanuló a
digitális kompetenciák és a problémamegoldás képességének fejlesztése
során, a megfelelő szintű és biztonságos eszközhasználat gyakorlásával,
problémaorientált feladatmegoldási módszereket sajátít el. Ebben a
nevelési-oktatási szakaszban fontos célkitűzés, hogy a hétköznapi
életből vett feladatok mellett a többi tantárgy tanulása során
felbukkanó problémák is előkerüljenek.

Az algoritmizálás, programozás ismerete elősegíti az olyan készségek
fejlesztését, amelyek a problémamegoldásban, a kreativitás
kibontakozásában és a logikus gondolkodásban nélkülözhetetlenek.

Az információs technológiák a szolgáltatások igénybevétele során is
szerephez jutnak. A webes és mobilkommunikációs eszközök széles
választéka, a felhasználási területek gazdagsága a tanórák rugalmas
alakítását és a tanulók bevonását teszi szükségessé. Tárgyalni kell
továbbá a gyors elavulás, a biztonság, a tudatos felhasználói és
vásárlói magatartás, valamint a biztonsági okokból bevezetett
korlátozások problémarendszerét is.

***A tantárgy tanításának specifikus jellemzői a 9--12. évfolyamon***

A középfokú tanulmányaikat folytató tanuló társadalmi érintkezésében
aktív szerepet tölt be a digitális környezet használata, ezért alapvető
követelmény, hogy ennek elvi és gyakorlati kérdéseit folyamatosan
kövesse, tudását bővítse, és azt szükség esetén felelősséggel ossza meg.

Az informatikai eszközök megismerése felhasználói szemléletű, azaz a
tanulás és tanítás fókusza a gyakorlati problémák megoldásán van. Cél a
problémák tudatos, célszerű és hatékony kezelésének elsajátítása.

A problémák összetettségében építeni kell a korosztályra jellemző,
magasabb absztrakciós szintre, így a korábban elsajátított ismeretek
bővítése nemcsak konkrét új fogalmak bevezetését, hanem az ismeretek
felhasználási területének bővítését is jelenti.

Az algoritmizálásnál a hétköznapi feladatok mellett a más tantárgyakban
megjelenő folyamatok modellezése, vizsgálata továbbra is olyan cél,
amellyel a tantárgyi koncentráció erősíthető, és bemutatható a
programozás ilyen irányú hasznosításának lehetősége. A programozás
fogalmainak ismeretét a tanuló magas szintű, széles körben elterjedt, de
egyszerű formális programozási nyelv segítségével mélyíti el.

A tanulás és tanítás során figyelembe kell venni, hogy a digitális
technológia a lokális megoldásoktól a mobil- és a hálózatos rendszerek
irányába fejlődik tovább, és ez hatékony eszközrendszert teremt az
együttműködéshez. Az elterjedtebb szolgáltatások megismerésével
egyidejűleg a tanuló elsajátítja a rendszerek felhasználását a
csoportmunka, projektmunka szervezésében, lebonyolításában.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK A 3--4. ÉVFOLYAMON***

1. A digitális világ körülöttünk

2. A digitális eszközök használata

3. Alkotás digitális eszközökkel

4. Információszerzés az e-Világban

5. Védekezés a digitális világ veszélyei ellen

6. A robotika és a kódolás alapjai

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Algoritmizálás és blokkprogramozás

2. Online kommunikáció

3. Robotika

4. Szövegszerkesztés

5. Bemutató-készítés

6. Multimédiás elemek készítése

7. Táblázatkezelés

8. Az információs társadalom, e-Világ

9. A digitális eszközök használata

***FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON***

1. Algoritmizálás, formális programozási nyelv használata

2. Információs társadalom, e-Világ

3. Mobiltechnológiai ismeretek

4. Szövegszerkesztés

5. Számítógépes grafika

6. Multimédiás dokumentumok készítése

7. Online kommunikáció

8. Publikálás a világhálón

9. Táblázatkezelés

10. Adatbázis-kezelés

11. A digitális eszközök használata

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 3--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. elmélyülten dolgozik digitális környezetben, önellenőrzést végez;

2. megvizsgálja és értékeli az általa vagy társai által alkalmazott,
    létrehozott, megvalósított eljárásokat;

3. társaival együttműködve online és offline környezetben egyaránt
    megold különböző feladatokat, ötleteit, véleményét megfogalmazza,
    részt vesz a közös álláspont kialakításában;

4. kiválasztja az általa ismert informatikai eszközök és alkalmazások
    közül azokat, melyek az adott probléma megoldásához szükségesek;

5. eredményétől függően módosítja a problémamegoldás folyamatában az
    adott, egyszerű tevékenységsorokat;

6. a rendelkezésére álló eszközökkel, forrásokból meggyőződik a talált
    vagy kapott információk helyességéről.

AZ INFORMATIKAI ESZKÖZÖK HASZNÁLATA

ISMERKEDÉS AZ INFORMATIKAI KÖRNYEZETTEL

A nevelési-oktatási szakasz végére a tanuló:

1. közvetlen otthoni vagy iskolai környezetéből megnevez néhány
    informatikai eszközt, felsorolja fontosabb jellemzőit;

2. megfogalmazza, néhány példával alátámasztja, hogyan könnyíti meg a
    felhasználó munkáját az adott eszköz alkalmazása;

3. egyszerű feladatokat old meg informatikai eszközökkel. Esetenként
    tanítói segítséggel összetett funkciókat is alkalmaz.

GYERMEKEKNEK KÉSZÍTETT ALKALMAZÁSOK HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan vagy tanítói segítséggel választ más tantárgyak tanulásának
    támogatásához applikációkat, digitális tananyagot, oktatójátékot,
    képességfejlesztő digitális alkalmazást;

2. kezdetben tanítói segítséggel, majd önállóan használ néhány,
    életkorának megfelelő alkalmazást, elsősorban információgyűjtés,
    gyakorlás, egyéni érdeklődésének kielégítése céljából;

3. a feladathoz, problémához digitális eszközt, illetve alkalmazást,
    applikációt, felhasználói felületet választ; felsorol néhány érvet
    választásával kapcsolatosan.

DIGITÁLIS ÍRÁSTUDÁS

RAJZOS DOKUMENTUMOK DIGITÁLIS LÉTREHOZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. adott szempontok alapján megfigyel néhány, grafikai alkalmazással
    készített produktumot; személyes véleményét megfogalmazza;

2. grafikai alkalmazással egyszerű, közvetlenül hasznosuló rajzot,
    grafikát, dokumentumot hoz létre;

3. egy rajzos dokumentumot adott szempontok alapján értékel, módosít.

ADATOK ÉRTELMEZÉSE, CSOPORTOSÍTÁSA, TÁROLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. állításokat fogalmaz meg grafikonokról, infografikákról,
    táblázatokról; a kapott információkat felhasználja napi tevékenysége
    során;

2. információkat keres, a talált adatokat felhasználja digitális
    produktumok létrehozására.

PROBLÉMAMEGOLDÁS INFORMATIKAI ESZKÖZÖKKEL ÉS MÓDSZEREKKEL

A PROBLÉMA MEGOLDÁSÁHOZ SZÜKSÉGES MÓDSZEREK ÉS ESZKÖZÖK KIVÁLASZTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. értelmezi a problémát, a megoldási lehetőségeket eljátssza,
    megfogalmazza, egyszerű eszközök segítségével megvalósítja;

2. információt keres az interneten más tantárgyak tanulása során, és
    felhasználja azt;

3. egyszerű prezentációt, ábrát, egyéb segédletet készít.

ALGORITMUSOK VIZSGÁLATA, ELŐÁLLÍTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. felismer, eljátszik, végrehajt néhány hétköznapi tevékenysége során
    tapasztalt, elemi lépésekből álló, adott sorrendben végrehajtandó
    cselekvést;

2. egy adott, mindennapi életből vett algoritmust elemi lépésekre bont,
    értelmezi a lépések sorrendjét, megfogalmazza az algoritmus várható
    kimenetelét;

3. feladat, probléma megoldásához többféle algoritmust próbál ki.

KÓDOLÁS, FOLYAMATOK IRÁNYÍTÁSA, A ROBOTIKA ALAPJAI

A nevelési-oktatási szakasz végére a tanuló:

1. a valódi vagy szimulált programozható eszköz mozgását értékeli, hiba
    esetén módosítja a kódsorozatot a kívánt eredmény eléréséig.
    Tapasztalatait megfogalmazza, megvitatja társaival;

2. adott feltételeknek megfelelő kódsorozatot tervez és hajtat végre,
    történeteket, meserészleteket jelenít meg padlórobottal vagy más
    eszközzel;

3. alkalmaz néhány megadott algoritmust tevékenység, játék során, és
    néhány egyszerű esetben módosítja azokat.

INFORMÁCIÓS TECHNOLÓGIÁK

ADATAINK VÉDELME, INTERNETBIZTONSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. információkat keres az interneten, egyszerű eljárásokkal meggyőződik
    néhány, az interneten talált információ igazságértékéről;

2. kiválasztja a számára releváns információt, felismeri a hamis
    információt;

3. tisztában van a személyes adat fogalmával, törekszik megőrzésére,
    ismer néhány példát az e-Világ veszélyeivel kapcsolatban.

AZ INFORMÁCIÓS TECHNOLÓGIA ALKALMAZÁSA, DIGITÁLIS TANANYAGOK,
OKTATÓJÁTÉKOK HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és használja a kapcsolattartás formáit és a kommunikáció
    lehetőségeit a digitális környezetben;

2. ismeri a mobileszközök alkalmazásának előnyeit, korlátait, etikai
    vonatkozásait;

3. közvetlen tapasztalatokat szerez a digitális eszközök használatával
    kapcsolatban;

4. képes feladat, probléma megoldásához megfelelő applikáció, digitális
    tananyag, oktatójáték, képességfejlesztő digitális alkalmazás
    kiválasztására;

5. ismer néhány, kisiskolások részére készített portált,
    információforrást, digitálistananyag-lelőhelyet.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan használja a digitális eszközöket, az online kommunikáció
    eszközeit, tisztában van az ezzel járó veszélyekkel;

2. elsajátítja a digitális írástudás eszközeit, azokkal feladatokat old
    meg;

3. megismeri a felmerülő problémák megoldásának módjait, beleértve az
    adott feladat megoldásához szükséges algoritmus értelmezését,
    alkotását és számítógépes program készítését és kódolását a
    blokkprogramozás eszközeivel;

4. digitális tudáselemek felhasználásával, társaival együttműködve
    különböző problémákat old meg;

5. megismeri a digitális társadalom elvárásait, lehetőségeit és
    veszélyeit.

AZ INFORMATIKAI ESZKÖZÖK HASZNÁLATA

AZ INFORMATIKAI ESZKÖZÖK ÖNÁLLÓ HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. célszerűen választ a feladat megoldásához használható informatikai
    eszközök közül;

2. az informatikai eszközöket önállóan használja, a tipikus
    felhasználói hibákat elkerüli, és elhárítja az egyszerűbb
    felhasználói szintű hibákat;

3. értelmezi az informatikai eszközöket működtető szoftverek
    hibajelzéseit, és azokról beszámol.

AZ OPERÁCIÓS RENDSZEREK ÖNÁLLÓ HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan használja az operációs rendszer felhasználói felületét;

2. önállóan kezeli az operációs rendszer mappáit, fájljait és a
    felhőszolgáltatásokat;

3. használja a digitális hálózatok alapszolgáltatásait;

4. tapasztalatokkal rendelkezik a digitális jelek minőségével,
    kódolásával, tömörítésével, továbbításával kapcsolatos problémák
    kezeléséről.

DIGITÁLIS ÍRÁSTUDÁS

DOKUMENTUM LÉTREHOZÁSA SZÖVEGSZERKESZTŐ ÉS BEMUTATÓ-KÉSZÍTŐ
ALKALMAZÁSSAL

A nevelési-oktatási szakasz végére a tanuló:

1. egy adott feladat kapcsán önállóan hoz létre szöveges vagy
    multimédiás dokumentumokat;

2. ismeri és tudatosan alkalmazza a szöveges és multimédiás dokumentum
    készítése során a szöveg formázására, tipográfiájára vonatkozó
    alapelveket;

3. a tartalomnak megfelelően alakítja ki a szöveges vagy a multimédiás
    dokumentum szerkezetét, illeszti be, helyezi el és formázza meg a
    szükséges objektumokat;

4. ismeri és kritikusan használja a nyelvi eszközöket (például
    helyesírás-ellenőrzés, elválasztás).

KÜLÖNBÖZŐ TÍPUSÚ DOKUMENTUMOK ISKOLAI, TANÓRAI, HÉTKÖZNAPI CÉLÚ
FELHASZNÁLÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. a szöveges dokumentumokat többféle elrendezésben jeleníti meg
    papíron, tisztában van a nyomtatás környezetre gyakorolt hatásaival;

2. ismeri a prezentációkészítés alapszabályait, és azokat alkalmazza;

3. etikus módon használja fel az információforrásokat, tisztában van a
    hivatkozás szabályaival.

MULTIMÉDIÁS ELEMEK KÉSZÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. digitális eszközökkel önállóan rögzít és tárol képet, hangot és
    videót;

2. digitális képeken képkorrekciót hajt végre;

3. ismeri egy bittérképes rajzolóprogram használatát, azzal ábrát
    készít;

4. bemutató-készítő vagy szövegszerkesztő programban rajzeszközökkel
    ábrát készít.

PROBLÉMAMEGOLDÁS INFORMATIKAI ESZKÖZÖKKEL ÉS MÓDSZEREKKEL

EGYSZERŰ ALGORITMUSOK ELEMZÉSE, KÉSZÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. érti, hogyan történik az egyszerű algoritmusok végrehajtása a
    digitális eszközökön;

2. megkülönbözteti, kezeli és használja az elemi adatokat;

3. értelmezi az algoritmus végrehajtásához szükséges adatok és az
    eredmények kapcsolatát;

4. egyszerű algoritmusokat elemez és készít.

A KÓDOLÁS ESZKÖZEINEK ISMERETE, A BLOKKPROGRAMOZÁS ESZKÖZEINEK
HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a kódolás eszközeit;

2. adatokat kezel a programozás eszközeivel;

3. ismeri és használja a programozási környezet alapvető eszközeit;

4. ismeri és használja a blokkprogramozás alapvető építőelemeit;

5. a probléma megoldásához vezérlési szerkezetet (szekvencia, elágazás
    és ciklus) alkalmaz a tanult blokkprogramozási nyelven.

ADATOK KEZELÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. az adatokat táblázatos formába rendezi és formázza;

2. cellahivatkozásokat, matematikai tudásának megfelelő képleteket,
    egyszerű statisztikai függvényeket használ táblázatkezelő
    programban;

3. az adatok szemléltetéséhez diagramot készít.

TANTÁRGYI PROBLÉMÁK VIZSGÁLATA DIGITÁLIS ESZKÖZÖKKEL

A nevelési-oktatási szakasz végére a tanuló:

1. problémákat old meg táblázatkezelő program segítségével;

2. tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes
    szimulációjáról;

3. vizsgálni tudja a szabályozó eszközök hatásait a tantárgyi
    alkalmazásokban.

INFORMÁCIÓS TECHNOLÓGIÁK

AZ INFORMÁCIÓKERESÉSI TECHNIKÁK ALKALMAZÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az információkeresés technikáját, stratégiáját és több
    keresési szempont egyidejű érvényesítésének lehetőségét;

2. önállóan keres információt, a találatokat hatékonyan szűri;

3. az internetes adatbázis-kezelő rendszerek keresési űrlapját helyesen
    tölti ki.

AZ INFORMÁCIÓS TECHNOLÓGIÁN ALAPULÓ KOMMUNIKÁCIÓS FORMÁK HASZNÁLATA

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri, használja az elektronikus kommunikáció lehetőségeit, a
    családi és az iskolai környezetének elektronikus szolgáltatásait;

2. ismeri és betartja az elektronikus kommunikációs szabályokat.

ROBOTIKA

A nevelési-oktatási szakasz végére a tanuló:

1. mozgásokat vezérel szimulált vagy valós környezetben;

2. adatokat gyűjt szenzorok segítségével;

3. tapasztalatokkal rendelkezik az eseményvezérlésről;

4. ismeri a térinformatika és a 3D megjelenítés lehetőségeit.

EGYÜTTMŰKÖDÉS AZ INFORMÁCIÓS TECHNOLÓGIÁK HASZNÁLATA SORÁN

A nevelési-oktatási szakasz végére a tanuló:

1. tapasztalatokkal rendelkezik az iskolai oktatáshoz kapcsolódó
    mobileszközökre fejlesztett alkalmazások használatában;

2. tisztában van a hálózatokat és a személyes információkat érintő
    fenyegetésekkel, alkalmazza az adatok védelmét biztosító
    lehetőségeket;

3. védekezik az internetes zaklatás különböző formái ellen, szükség
    esetén segítséget kér.

AZ INFORMÁCIÓS TÁRSADALOM, E-VILÁG LEHETŐSÉGEINEK, KOCKÁZATAINAK
ISMERETE

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a digitális környezet, az e-Világ etikai problémáit;

2. ismeri az információs technológia fejlődésének gazdasági,
    környezeti, kulturális hatásait;

3. ismeri az információs társadalom múltját, jelenét és várható
    jövőjét;

4. online gyakorolja az állampolgári jogokat és kötelességeket.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) A 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az informatikai eszközök és a működtető szoftvereik célszerű
    választásának alapelveit, használja a digitális hálózatok
    alapszolgáltatásait, az online kommunikáció eszközeit, tisztában van
    az ezzel járó veszélyekkel, ezzel összefüggésben ismeri a
    segítségnyújtási, segítségkérési lehetőségeket;

2. gyakorlatot szerez dokumentumok létrehozását segítő eszközök
    használatában;

3. megismeri az adatkezelés alapfogalmait, képes a nagyobb
    adatmennyiség tárolását, hatékony feldolgozását biztosító eszközök
    és módszerek alapszintű használatára, érti a működésüket;

4. megismeri az algoritmikus probléma megoldásához szükséges
    módszereket és eszközöket, megoldásukhoz egy magas szintű formális
    programozási nyelv fejlesztői környezetét önállóan használja;

5. hatékonyan keres információt; az IKT-tudáselemek felhasználásával
    társaival együttműködve problémákat old meg;

6. ismeri az e-Világ elvárásait, lehetőségeit és veszélyeit.

AZ INFORMATIKAI ESZKÖZÖK HASZNÁLATA

AZ INFORMATIKAI ESZKÖZÖK FELHASZNÁLÁSÁNAK LEHETŐSÉGEI, AZ INFORMATIKAI
KÖRNYEZET

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és tudja használni a célszerűen választott informatikai
    eszközöket és a működtető szoftvereit, ismeri a felhasználási
    lehetőségeket;

2. ismeri a digitális eszközök és a számítógépek fő egységeit, ezek
    fejlődésének főbb állomásait, tendenciáit;

3. tudatosan alakítja informatikai környezetét, ismeri az ergonomikus
    informatikai környezet jellemzőit, figyelembe veszi a digitális
    eszközök egészségkárosító hatásait, óvja maga és környezete
    egészségét;

4. önállóan használja az informatikai eszközöket, elkerüli a tipikus
    felhasználói hibákat, elhárítja az egyszerűbb felhasználói hibákat.

A MOBILESZKÖZÖK, SZÁMÍTÓGÉPEK, HÁLÓZATOK OPERÁCIÓS RENDSZEREI,
FELHŐSZOLGÁLTATÁSOK

A nevelési-oktatási szakasz végére a tanuló:

1. céljainak megfelelően használja a mobileszközök és a számítógépek
    operációs rendszereit;

2. igénybe veszi az operációs rendszer és a számítógépes hálózat
    alapszolgáltatásait;

3. követi a technológiai változásokat a digitális információforrások
    használatával.

SEGÉDPROGRAMOK, DIGITÁLIS KÁRTEVŐK ELLENI VÉDEKEZÉS, ÁLLOMÁNYOK
TÖMÖRÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. használja az operációs rendszer segédprogramjait, és elvégzi a
    munkakörnyezet beállításait;

2. tisztában van a digitális kártevők elleni védekezés lehetőségeivel;

3. használja az állományok tömörítését és a tömörített állományok
    kibontását.

DIGITÁLIS ÍRÁSTUDÁS

NAGYMÉRETŰ SZÖVEGES DOKUMENTUMOK SZERKESZTÉSÉT ELŐSEGÍTŐ ESZKÖZÖK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri egy adott feladat megoldásához szükséges digitális eszközök
    és szoftverek kiválasztásának szempontjait;

2. speciális dokumentumokat hoz létre, alakít át és formáz meg;

3. tapasztalatokkal rendelkezik a formanyomtatványok, a sablonok, az
    előre definiált stílusok használatáról.

MULTIMÉDIÁS ÉS WEBES DOKUMENTUMOK SZERKESZTÉSE ÉS KÉSZÍTÉSE

A nevelési-oktatási szakasz végére a tanuló:

1. gyakorlatot szerez a fotó-, hang-, videó-, multimédia-szerkesztő, a
    bemutató-készítő eszközök használatában;

2. alkalmazza az információkeresés során gyűjtött multimédiás
    alapelemeket új dokumentumok készítéséhez;

3. dokumentumokat szerkeszt és helyez el tartalomkezelő rendszerben;

4. ismeri a HTML formátumú dokumentumok szerkezeti elemeit, érti a CSS
    használatának alapelveit; több lapból álló webhelyet készít.

GRAFIKUS ÁBRÁK KÉSZÍTÉSE ÉS KÉPSZERKESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. létrehozza az adott probléma megoldásához szükséges rasztergrafikus
    ábrákat;

2. létrehoz vektorgrafikus ábrákat;

3. digitálisan rögzít képet, hangot és videót, azokat manipulálja;

4. tisztában van a raszter-, a vektorgrafikus ábrák tárolási és
    szerkesztési módszereivel

PROBLÉMAMEGOLDÁS INFORMATIKAI ESZKÖZÖKKEL ÉS MÓDSZEREKKEL

ALGORITMIZÁLÁS, MÓDSZEREK, ESZKÖZÖK HASZNÁLATA, TÍPUSALGORITMUSOK

A nevelési-oktatási szakasz végére a tanuló:

1. érti az egyszerű problémák megoldásához szükséges tevékenységek
    lépéseit és kapcsolatukat;

2. ismeri a következő elemi adattípusok közötti különbségeket: egész,
    valós szám, karakter, szöveg, logikai;

3. ismeri az elemi és összetett adattípusok közötti különbségeket;

4. érti egy algoritmus-leíró eszköz alapvető építőelemeit, érti a
    típusalgoritmusok felhasználásának lehetőségeit;

5. példákban, feladatok megoldásában használja egy formális
    programozási nyelv fejlesztői környezetének alapszolgáltatásait;

6. szekvencia, elágazás és ciklus segítségével algoritmust hoz létre,
    és azt egy magas szintű formális programozási nyelven kódolja;

7. a feladat megoldásának helyességét teszteli.

ADATKEZELÉS TÁBLÁZATKEZELŐ ALKALMAZÁSSAL

A nevelési-oktatási szakasz végére a tanuló:

1. adatokat táblázatba rendez;

2. táblázatkezelővel adatelemzést és számításokat végez;

3. a problémamegoldás során függvényeket célszerűen használ;

4. nagy adathalmazokat tud kezelni;

5. az adatokat diagramon szemlélteti.

ADATKEZELÉS ADATBÁZIS-KEZELŐ RENDSZERREL

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az adatbázis-kezelés alapfogalmait;

2. az adatbázisban interaktív módon keres, rendez és szűr;

3. a feladatmegoldás során az adatbázisba adatokat visz be, módosít és
    töröl, űrlapokat használ, jelentéseket nyomtat;

4. strukturáltan tárolt nagy adathalmazokat kezel, azokból egyedi és
    összesített adatokat nyer ki.

SZÁMÍTÓGÉPES SZIMULÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. tapasztalatokkal rendelkezik hétköznapi jelenségek számítógépes
    szimulációjáról;

2. hétköznapi, oktatáshoz készült szimulációs programokat használ;

3. tapasztalatokat szerez a kezdőértékek változtatásának hatásairól a
    szimulációs programokban.

INFORMÁCIÓS TECHNOLÓGIÁK

INFORMÁCIÓKERESÉS ÉS ONLINE KOMMUNIKÁCIÓ

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és alkalmazza az információkeresési stratégiákat és
    technikákat, a találati listát a problémának megfelelően szűri,
    ellenőrzi annak hitelességét;

2. etikus módon használja fel az információforrásokat, tisztában van a
    hivatkozás szabályaival;

3. használja a két- vagy többrésztvevős kommunikációs lehetőségeket és
    alkalmazásokat;

4. ismeri és alkalmazza a fogyatékkal élők közötti kommunikáció
    eszközeit és formáit;

5. az online kommunikáció során alkalmazza a kialakult viselkedési
    kultúrát és szokásokat, a szerepelvárásokat.

MOBILTECHNOLÓGIAI ISMERETEK

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri és használja a mobiltechnológiát, kezeli a mobileszközök
    operációs rendszereit és használ mobilalkalmazásokat;

2. céljainak megfelelő alkalmazást választ, az alkalmazás funkcióira,
    kezelőfelületére vonatkozó igényeit megfogalmazza;

3. az applikációkat önállóan telepíti;

4. az iskolai oktatáshoz kapcsolódó mobileszközökre fejlesztett
    alkalmazások használata során együttműködik társaival.

E-ÁLLAMPOLGÁRSÁGI ISMERETEK, E-SZOLGÁLTATÁSOK, E-ÜGYINTÉZÉS,
E-KERESKEDELEM

A nevelési-oktatási szakasz végére a tanuló:

1. tisztában van az e-Világ -- e-szolgáltatások, e-ügyintézés,
    e-kereskedelem, e-állampolgárság, IT-gazdaság, környezet, kultúra,
    információvédelem -- biztonsági és jogi kérdéseivel;

2. tisztában van a digitális személyazonosság és az
    információhitelesség fogalmával;

3. a gyakorlatban alkalmazza az adatok védelmét biztosító
    lehetőségeket.

***II.3.8.2. TECHNIKA ÉS TERVEZÉS***

***A) ALAPELVEK, CÉLOK***

A technika és tervezés tantárgy a problémamegoldó gondolkodást, a saját
tapasztalás útján történő ismeretszerzést helyezi a középpontba, melynek
eszköze a tanórákon megvalósuló kreatív tervező és alkotó munka, a
hagyományos kézműves és a legmodernebb digitális technológiák
felhasználásával. A tantervben kiemelt szerepet kap a tanulni tudás, a
tanultak alkalmazása, a problémamegoldáson alapuló alkotás. Ezt
szolgálják a kínált tevékenységek, a nevelés, a kompetenciafejlesztés és
a műveltségtartalom leírt rendszere, az egyes elemek arányos
megjelenítése.

A tantárgy keretében végzett tevékenységek elősegítik, hogy a tanuló
aktív szerepkörben tervezési és végrehajtási készségeket alakítson ki az
életében felmerülő komplex gyakorlati problémák megoldásához. A tanuló a
tanulási folyamat során a közvetlen, mindennapi gyakorlati tevékenység
végzése közben valódi anyagokból felhasználható produktumokat hoz létre,
az életkorához illeszkedő, biztonságosan kezelhető szerszámok, eszközök
segítségével.

A tantárgy felhasználja a közismereti tárgyak keretében már elsajátított
ismeretek közül azokat, amelyek segíthetnek a mindennapi életben
felmerülő problémák megoldásában. Olyan cselekvőképesség kialakítása a
cél, amelynek mozgatója a felelősségérzet és az elköteleződés, alapja a
megfelelő autonómia és nyitottság, a megoldási komplexitás.

A tantárgy struktúrájában rugalmas, cselekvésre építő, tanulóközpontú
tanulásra ösztönöz. Az elsajátított tudás hozzájárul a mindennapi
életben használható készségek kialakításához és elősegíti a munka
világában történő alkalmazkodást.

A technika és tervezés tantárgy tanulásának célja, hogy a tanuló:

- alkalmazni tudja az előzetes tudását a mindennapi élet problémáinak
    megoldása során;

- alkotótevékenysége keretében elsajátítsa a produktum kivitelezése
    lépéssorának megtervezését, a terv alapján a tervhez illeszkedő
    kivitelező tevékenységek elvégzését, a szakszerű eszközhasználatot,
    az együttműködésre épülő munkakultúrát;

- ismerje az alapvető technikai folyamatokat;

- megismerje a különböző szakmacsoportok sajátosságait, ezzel
    támogatva a pályaorientációt, életpálya-tervezést.

A technika és tervezés tantárgy tanulásának célja, hogy a tanulóban
kialakuljon:

- a gyakorlati tevékenységekhez szükséges minden készség és képesség;

- a pozitív alkotó magatartás;

- a komplex gyakorlati problémák megoldási készsége;

- a felelős, környezettudatos beállítottság és a kritikus fogyasztói
    magatartás.

***A technika és tervezés tantárgy tanításának specifikus jellemzői az
1--4. évfolyamon***

Az ember környezetet átalakító tevékenységének, felelősségének
megismerése, megértése nem új feladat az iskolát kezdő tanuló számára,
hiszen az óvodában naponta végezhetett ilyen jellegű tevékenységeket, az
önellátás, önkiszolgálás, a tárgyalkotás, a kézműves és óvodakerti
tevékenységek terén.

Az alapfokú képzés első nevelési-oktatási szakaszában erre a motivációs
bázisra építhető a technika és tervezés tantárgy programja, középpontba
helyezve az alkotótevékenységet, a gyakorlati feladatvégzést. A
motiváció felkeltésére a tanuló aktív tanulási folyamatba történő
bevonásával, játékba ágyazott minta- és modellkövetéssel,
tapasztalatszerzéssel, felfedezéssel, az alkotó fantázia mozgósításával
nyílik lehetőség. A tudás elsajátításának nagyobb egységekbe történő
szerveződését nagymértékben segíti elő a környezeti tapasztalások
eredményeként kialakult szokásrend, melynek kitüntetett eseményeit
képezik a megelevenített néphagyományok, ünnepek, jeles napok.

Kiemelt feladat a kézügyesség életkori sajátosságainak megfelelő
fejlesztése, mely meghatározó segítséget jelenthet az íráskészség
kialakításában. A tanórákon végzett tudatos, tervszerű átalakító,
megmunkáló tevékenységek magukba foglalják a különböző anyagok
megismerését, a megmunkálhatóság megtapasztalását, a tervező és
technikai folyamatok alkalmazását, a feladatvégzés során keletkező
maradványanyagok környezettudatos elhelyezését.

***A tantárgy tanításának specifikus jellemzői az 5--7. évfolyamon***

A tantárgy az alapfokú képzés ezen második nevelési--oktatási
szakaszában szervesen épít a tanulók előzetes tudására, az
alkotótevékenység során elsajátított technikai ismeretekre, az eszköz-
és szerszámhasználat műveleti lépései során felmutatott ügyességre,
valamint a tervezési és kivitelezési önállóságra.

A tantárgy tanterve az 5--7. évfolyamon több lehetőséget, modult kínál.
A modul kiválasztásával az iskola az igényeihez, a sajátosságaihoz
igazodó, helyi program fő tartalmait tudja kialakítani. Mindegyik modul
komplex alkotó folyamatok tervezésével és elvégzésével biztosítja az
elmélyülést, a változatos tanórai tevékenységet. Ezzel a tervezett
rugalmassággal teremti meg a lehetőséget arra, hogy a helyi tanterv
szerint megvalósuló, tevékenység- és gyakorlatközpontú tanulási-tanítási
folyamat -- melyben az alkotótevékenység és az ismeretek szerzése
egymástól elválaszthatatlan -- jobban alkalmazkodjon a helyi igényekhez,
lehetőségekhez, sajátosságokhoz, jobban támogassa a pályaválasztást.

Két év után akár más modul választható, vagy az intézmény tárgyi és
személyi feltételrendszeréhez igazodó módon egy modul több tanéven át
alkalmazható.

A tantárgy átfogó célként kitűzött eredményeinek megvalósítása a
választott modul szerinti speciális tanulási környezet kialakítását
igényli, mely környezet -- lehetőség szerint -- egy biztonságos
szaktanterem, anyagok megmunkálására alkalmas műhelyterem, ételkészítési
gyakorlatok elvégzésére alkalmas szaktanterem, illetve szabadtéri
helyszín, iskolakert.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Anyagok a környezetünkben

2. Tárgykészítés különböző anyagokból, építés, szerelés

3. Otthon -- család -- életmód

4. Jeles napok, ünnepek

5. Közlekedés

***FŐ TÉMAKÖRÖK AZ 5--7. ÉVFOLYAMON***

A tantárgy -- hagyományaiból építkezve -- négy választható modult
tartalmaz. Az egyes modulok egymással egyenértékűek. A választott modul
szerinti feladatokat az intézmény helyi tantervében kell rögzíteni.

A MODUL: ÉPÍTETT KÖRNYEZET -- TÁRGYALKOTÁS TECHNOLÓGIÁI

1. Modell- és makettépítés technológiái

2. A település kialakulása, településtípusok

3. Építészet -- forma és funkció, anyagok és szerkezetek

4. Közterek, közösségi terek, középületek

5. A települések közműellátása, a legfontosabb közművek,
    közszolgáltatások

6. Az egészséges település

7. A lakás jellemzői, lakástípusok, funkciók, helyiségek

8. Lakás, lakókörnyezet -- funkciók, berendezések

9. Lakás karbantartása -- a legfontosabb állagmegóvási, karbantartási
    munkák

10. Korszerű, egészséges lakás és lakókörnyezet

11. Közlekedés egykor és ma

12. Közlekedés, közlekedési rendszerek

13. Komplex modellezési feladatok

B MODUL: HÁZTARTÁS -- ÖKONÓMIA -- ÉLETVITEL TECHNOLÓGIÁI

1. Gazdálkodás, munkamegosztás

2. Otthon a lakásban

3. Táplálkozás és ételkészítés

4. Textiltechnika

5. Szabad alkotás

C MODUL: KERTÉSZETI TECHNOLÓGIÁK

1. Ősz a zöldségesben

2. Élet a talpunk alatt

3. Téli tevékenységek

4. Zöldségnövények termesztése

5. Gyógynövénytermesztés és szántóföldi kultúrák

6. A talaj gondozása, javítása

7. Talajművelés az iskolakertben és a nagyüzemben

8. Növényvédelem a biokertben

9. Zöldségfélék, gyógynövények termesztése az iskolakertben

10. Ősz a gyümölcsösben

11. Gyümölcsfák szaporítása

12. Gyümölcstermesztés az iskolakertben

D MODUL: MODELLEZÉS -- TÁRGYALKOTÁS TECHNOLÓGIÁI

1. Műszaki kommunikáció

2. Anyagok és alakításuk

3. Papír

4. Textil

5. Természetes és mesterséges faanyagok

6. Fém

7. Műanyag

8. Gépek, gépelemek

9. Mechanikai hajtások, mechanizmusok

10. Gépek felépítése, gépelemek

11. Környezetünk gépei, gépszerelési gyakorlatok

12. Elektromos áram, elektromos áramkör

13. Fogyasztók és kapcsolók soros és párhuzamos kapcsolása

14. Irányítástechnika alapjai -- vezérlés, szabályozás

15. Áramkört tartalmazó komplex modell tervezése és kivitelezése

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. elkülöníti a természeti és mesterséges környezet jellemzőit;

2. felismeri, hogy tevékenységei során változtatni tud a közvetlen
    környezetén;

3. kitartó a munkavégzésben, szükség esetén segítséget kér, segítséget
    ad;

4. szöveg vagy rajz alapján épít, tárgyakat készít, alkalmazza a tanult
    munkafolyamatokat, terveit megosztja;

5. munkafolyamatokat, technológiákat segítséggel algoritmizál;

6. megadott szempontok mentén értékeli saját, a társak, a csoport
    munkáját, viszonyítja a kitűzött célokhoz;

7. alkotótevékenysége során megéli, megismeri a jeles napokat,
    ünnepeket, hagyományokat mint értékeket;

8. tevékenysége során munkatapasztalatot szerez, megéli az alkotás
    örömét, az egyéni és csapatsiker élményét;

9. felismeri az egymásért végzett munka fontosságát, a munkamegosztás
    értékét.

ALKOTÓTEVÉKENYSÉG

ANYAGOK VIZSGÁLATA ÉS KIVÁLASZTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. az anyagok tulajdonságairól érzékszervi úton, önállóan szerez
    ismereteket -- szín, alak, átlátszóság, szag, keménység,
    rugalmasság, felületi minőség;

2. alkotótevékenysége során figyelembe veszi az anyag tulajdonságait,
    felhasználhatóságát.

TÁRGYKÉSZÍTÉS, TERVEZÉS, KIVITELEZÉS, ÉRTÉKELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. adott szempontok alapján egyszerűbb tárgyakat önállóan tervez,
    készít, alkalmazza a tanult munkafolyamatokat;

2. egyszerű szöveges, rajzos és képi utasításokat hajt végre a
    tevékenysége során;

3. alkotótevékenysége során előkészítő, alakító, szerelő és
    felületkezelő műveleteket végez el;

4. saját és társai tevékenységét a kitűzött célok mentén, megadott
    szempontok szerint reálisan értékeli;

5. értékelés után megfogalmazza tapasztalatait, következtetéseket von
    le a későbbi eredményesebb munkavégzés érdekében.

TECHNIKAI-PROBLÉMAMEGOLDÓ GONDOLKODÁS

AZ EMBERI TEVÉKENYSÉG KÖRNYEZETE

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri, hogy tevékenysége során tud változtatni közvetlen
    környezetén, megóvhatja, javíthat annak állapotán;

2. rendet tart a környezetében;

3. törekszik a takarékos anyagfelhasználásra;

4. szelektíven gyűjti a hulladékot.

PROBLÉMA-MEGOLDÁSI STRATÉGIA

A nevelési-oktatási szakasz végére a tanuló:

1. rendelkezik az életkorának megfelelő szintű probléma-felismerési,
    probléma-megoldási képességgel.

ÉLETVITEL

ÉLETVEZETÉS, FENNTARTHATÓSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a családellátó tevékenységeket, melyek keretében vállalt
    feladatait az iskolai önellátás során munkamegosztásban végzi --
    terítés, rendrakás, öltözködés, növények, állatok gondozása stb.;

2. otthoni és iskolai környezetének, tevékenységeinek balesetveszélyes
    helyzeteit felismeri, és ismeri megelőzésük módját.

FOGYASZTÓI, PÉNZÜGYI-GAZDÁLKODÁSI TUDATOSSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. takarékosan gazdálkodik az anyaggal, energiával, idővel;

2. ismeri a tudatos vásárlás néhány fontos elemét.

KÖRNYEZET- ÉS EGÉSZSÉGTUDATOSSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az egészségmegőrzés tevékenységeit;

2. ismeri és használni, alkalmazni tudja a legfontosabb közlekedési
    lehetőségeket, szabályokat, viselkedési elvárásokat.

MUNKAKULTÚRA

MUNKAVÉGZÉSI SZOKÁSOK

A nevelési-oktatási szakasz végére a tanuló:

1. tudatosan megtartja az egészséges és biztonságos munkakörnyezetét;

2. az elvárt feladatokban önállóan dolgozik -- elvégzi a műveletet.

FELKÉSZÜLÉS A MUNKA VILÁGÁRA

A nevelési-oktatási szakasz végére a tanuló:

1. társaival munkamegosztás szerint együttműködik a csoportos
    munkavégzés során;

2. felismeri az egymásért végzett munka fontosságát, a munkamegosztás
    értékét;

3. ismeri a környezetében fellelhető, megfigyelhető szakmák, hivatások
    jellemzőit.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--7.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri a felhasznált anyagok vizsgálati lehetőségeit és módszereit,
    tulajdonságait, és céljainak megfelelően választ a rendelkezésre
    álló anyagokból;

2. tevékenységét megtervezi, terveit másokkal megosztja;

3. ismeri és betartja az alapvető munkavédelmi szabályokat;

4. tervek mentén folytatja alkotótevékenységét;

5. célszerűen választja ki és rendeltetésszerűen használja a szükséges
    szerszámokat, eszközöket, digitális alkalmazásokat;

6. törekszik a balesetmentes tevékenységre, a munkaterületen rendet
    tart;

7. munkavégzéskor szabálykövető, kooperatív magatartást követ;

8. ismeri az egyes műveletek jelentőségét a munka biztonságának,
    eredményességének vonatkozásában;

9. a tevékenység során társaival együttműködik, feladatmegosztás
    szerint tevékenykedik;

10. az elkészült produktumot használatba veszi, a tervhez viszonyítva
    értékeli saját, mások és a csoport munkáját;

11. értékeli az elvégzett munkákat, az értékelésben elhangzottakat
    felhasználja a későbbi munkavégzés során;

12. értékként tekint saját és mások alkotásaira, a létrehozott
    produktumokra;

13. felismeri az emberi cselekvés jelentőségét és felelősségét a
    környezetalakításban.

ALKOTÓTEVÉKENYSÉG

ANYAGOK VIZSGÁLATA ÉS KIVÁLASZTÁSA

A nevelési-oktatási szakasz végére a tanuló:

1. önállóan szerez információt megfigyelés, vizsgálat, adatgyűjtés
    útján;

2. környezeti, fenntarthatósági szempontokat is mérlegelve, céljainak
    megfelelően választ a rendelkezésre álló anyagokból.

TERVEZÉS, KIVITELEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. tevékenységét önállóan vagy társakkal együttműködve tervezi;

2. terveit a műszaki kommunikáció alkalmazásával osztja meg;

3. a terv szerinti lépések megtartásával, önellenőrzéssel halad
    alkotótevékenységében;

4. alkalmazza a forma és funkció összefüggéseit, önállóan választ
    szerszámot, eszközt;

5. a megismert szerszámokat és eszközöket önállóan, az újakat tanári
    útmutatással használja;

6. részt vesz a munkavégzési szabályok megalkotásában, betartja azokat;

7. csoportmunkában feladatot vállal, részt vesz a döntéshozatalban, és
    a döntésnek megfelelően tevékenykedik;

8. felméri és tervezi a tevékenység munkavédelmi szabályait;

9. a csoportban feladata szerint tevékenykedik, tudását megosztja.

ÉRTÉKELÉS

A nevelési-oktatási szakasz végére a tanuló:

1. adott szempontok mentén értékeli saját és mások munkáját;

2. a használatbavétel során, az eltéréseket kiindulópontként alkalmazva
    javaslatot tesz produktuma továbbfejlesztésére;

3. megérti az egyén felelősségét a közös értékteremtésben.

TECHNIKAI-PROBLÉMAMEGOLDÓ GONDOLKODÁS

A nevelési-oktatási szakasz végére a tanuló:

1. szempontokat határoz meg a környezeti állapot felméréséhez, bizonyos
    eltéréseket számszerűsít;

2. érti és értékeli a globális változásokat érintő lehetséges
    megoldások és az emberi tevékenység szerepét, jelentőségét;

3. tevékenységének tervezésénél és értékelésénél figyelembe vesz
    környezeti szempontokat;

4. felismeri a technikai fejlődés és a társadalmi, gazdasági fejlődés
    kapcsolatát;

5. a problémamegoldás során önállóan vagy társakkal együtt fogalmaz meg
    megoldási alternatívákat;

6. komplex szempontrendszer mentén választ stratégiát, optimalizál.

ÉLETVITEL

TUDATOS ÉLETVEZETÉS, KÖRNYEZETI, TÁRSADALMI ÉS GAZDASÁGI FENNTARTHATÓSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. holisztikus szemlélettel rendelkezik, az összefüggések megértésére
    törekszik;

2. döntéseit tudatosság jellemzi, alternatívákat mérlegel;

3. felismeri a személyes cselekvés jelentőségét a globális problémák
    megoldásában;

4. felismeri saját felelősségét életvezetése megtervezésében és
    megszervezésében, tudatosan gazdálkodik a rendelkezésre álló anyagi
    és nem anyagi erőforrásokkal.

FOGYASZTÓI, PÉNZÜGYI-GAZDÁLKODÁSI, KÖRNYEZET- ÉS EGÉSZSÉGTUDATOSSÁG

A nevelési-oktatási szakasz végére a tanuló:

1. rendszerszinten végzi az elemzést és az alkalmazást;

2. tisztában van a saját, a családi és a társadalmi erőforrásokkal és
    az azokkal való hatékony és tudatos gazdálkodás módjaival;

3. egészség- és környezettudatosan dönt és tevékenykedik.

MUNKAKULTÚRA

MUNKAVÉGZÉSI SZOKÁSOK

A nevelési-oktatási szakasz végére a tanuló:

1. terv szerint tevékenykedik, probléma esetén észszerű kockázatokat
    felvállal;

2. önismeretére építve vállal feladatokat, szem előtt tartva a csapat
    eredményességét;

3. alkalmazkodik a változó munkafeladatokhoz, szerepelvárásokhoz;
    vezetőként tudatosan vezeti a csoport döntési folyamatát;

4. alkalmazza a döntés-előkészítés, döntéshozatal eljárásait, hibás
    döntésein változtat;

5. az egyes részfeladatokat rendszerszinten szemléli;

6. érti a társadalmi munkamegosztás lényegét, az egyes foglalkoztatási
    ágazatok jelentőségét.

FELKÉSZÜLÉS A MUNKA VILÁGÁRA -- PÁLYAORIENTÁCIÓ, ÉLETPÁLYA-TERVEZÉS

A nevelési-oktatási szakasz végére a tanuló:

1. ismeri az egyes modulokhoz kapcsolódó foglalkozások jellemzőit,
    ezekkel kapcsolatban megfogalmazza saját preferenciáit;

2. a fizikai és digitális környezetből információt gyűjt a számára
    vonzó foglalkozások alkalmassági és képesítési feltételeiről, keresi
    a vállalkozási lehetőségeket, a jövedelmezőséget és a jellemző
    tanulási utakat.

*II.3.9. Testnevelés és egészségfejlesztés*

***A) ALAPELVEK, CÉLOK***

A 21. századi emberkép egyik hangsúlyos összetevője, hogy az egyén aktív
és cselekvő, ugyanakkor reflektív mérlegelésre képes. A cselekvő embert
a mozgáshoz kapcsolódó helyes attitűdök, a fizikailag aktív életmód,
önmaga reális elfogadása, a közösségi felelősségvállalással egybekötött
autonómia, az újító kezdeményezésekre való nyitottság és a megbízható
megoldások alkalmazásának képessége jellemzi.

A testileg és lelkileg egészséges ember az egészség állapotát, a
harmonikus életet értékként éli meg. Az egészséges és harmonikus
életvitelt megalapozó szokások a nevelés-oktatás egyes szakaszaiban a
pedagógusok személyes példamutatásán keresztül alakulnak ki.

A mozgáshoz, mint alapkompetenciához kapcsolódó képességek, készségek,
az adott szituációnak megfelelően mobilizálódó motoros képességek és
készségek jelentik az alapját annak, hogy az adott egyén a társadalom
aktív tagjává váljon, és az egész életen át tartó mozgásos cselekvési
biztonság jellemezze.

A Testnevelés és egészségfejlesztés tanulási terület elsősorban a
személyes és társas kompetencia fejlesztése révén járul hozzá az
általános kompetenciákban és a nevelési-oktatási célokban megfogalmazott
törekvések megvalósulásához. Hangsúlyos szerepet kapnak a szomatikus
egészséggel, a társas-érzelmi jólléttel, a biztonsággal, az emberi
kapcsolatokkal összefüggő kompetenciák. Az iskolai fejlesztés kiterjed a
jó időszervezésre, a konstruktív kooperációra, valamint a testi jóllét
és a motoros teljesítőképesség kialakítására is.

A tanulási terület **testnevelés** tantárgyának tanulása-tanítása során
szerzett ismeretek, kialakított készségek és megfelelően formált
attitűdök más általános kompetenciáknál is megjelennek. Példaként
említhető a társadalmi részvétel és felelősségvállalás, ahol az iskola
hozzásegíti a tanulót ahhoz, hogy toleránssá váljon a fogyatékossággal
élők iránt.

A Testnevelés és egészségfejlesztés tanulási területhez tartozó
testnevelés tantárgy legfontosabb célja, hogy a tanuló:

1. *megismerje a mozgáshoz kapcsolódó helyes attitűdöket, a fizikailag
    aktív életmód élethosszig tartó jótékony hatásait;*

2. *megtanulja a testnevelés és egészségfejlesztés szakkifejezéseit,
    helyes terminológiáját;*

3. *mozgásműveltségét olyan szintre fejlessze, hogy alkalmassá váljon a
    hatékony mozgásos cselekvéstanulásra, az önálló testedzésre;*

4. *az alsó tagozat végére legalább egy úszásnemben megtanuljon úszni;*

5. *életkorának, testi adottságának megfelelően fejlessze motoros
    teljesítőképességét, váljon képessé saját motoros teljesítmény- és
    fittségi szintje tudatos befolyásolására, elfogulatlan
    értékelésére;*

6. *a testmozgás, a testnevelés és a sport eszközeivel fejlessze
    önismeretét, érzelmi-akarati készségeit és képességeit, alakítson ki
    szabálykövető magatartásmintákat;*

7. *fejlessze társas-érzelmi jóllétét, társas-közösségi kapcsolatait,
stressztűrő és -kezelő képességét;*

8. *váljon képessé a baleseti források és az egészséget károsító,
    veszélyes szokások, tevékenységek értelmezésére.*

A tantárgy célja továbbá, hogy a gyógytestnevelésre utalt tanuló -- az
egyéni sajátosságainak maximális figyelembevételével -- ismerje meg a
testnevelés, az egészségfejlesztés és a sport azon eszközeit,
módszereit, amelyek segítséget nyújthatnak az egészségi állapota és a
motoros teljesítőképessége lehető legnagyobb mértékű helyreállításához,
valamint az esélyegyenlőség megteremtéséhez.

***A tantárgy tanításának specifikus jellemzői az 1--4. évfolyamon***

Az iskolát kezdő tanuló még rendszeres, folyamatos mozgástevékenységet
végez, primer szükséglete a mozgásos cselekvés. A tanuló a testnevelés
tantárgy keretében megtanul megfelelő iramot diktálva futni, különféle
módokon ugrani és dobni. Elsajátít támasz- és függéshelyzeteket, hely-
és helyzetváltoztatásokat támaszban és függésben. A legváltozatosabb
feladathelyzetekben, a korosztályának megfelelő szintű kreativitással
képessé válik, hogy részt vegyen különböző labda(sport)játékokban.
Megismeri a küzdősportok szerepét, a szabadban végzett testedzés
fontosságát. Megtanul legalább egy úszásnemben úszni. Törekszik a
biomechanikailag helyes testtartás kialakítására. Az alsó tagozaton is
lépéseket kell tenni annak érdekében, hogy teljesülhessen a tananyag
spirális építkezése, a mozgásos ismeretek fokozatos bővülése.

Ebben az életkorban a tantárgy tanulásának és tanításának alapvető
módszere a játék, a játékosság, amely elsősorban a koordinációs
képességek szenzitív időszakban történő fejlesztésére szolgál. A
mozgásos játékok közben fejlődik a térbeli és az időbeli tájékozódási
képesség, az irányérzék, bővül a relációs szókincs. Kiemelt feladat a
cselekvő tapasztalatszerzés, amely a testmozgás mellett sokféle
érzékelés bevonásával és változatos eszközök használatával valósítható
meg.

A tantárgy tanulása, miközben a tanulót folyamatos döntési helyzet elé
állítja, differenciáltan fejleszti személyiségét és társas kapcsolatait.
Emellett nevelési, fejlesztési célként -- elsősorban az első évfolyamon
-- fontos szerepet kap az önkiszolgálás.

***A tantárgy tanításának specifikus jellemzői az 5--8. évfolyamon***

Az 5--8. évfolyamon tanulók mozgástevékenységét, mozgásos cselekvését
leginkább a prepubertás- és egyes esetekben a pubertáskor
mozgásfejlődésének sajátos biológiai érési tényezői határozzák meg.
Tekintettel az életkorra jellemző jelentős egyéni eltérésekre, még
inkább előtérbe kerül a differenciált fejlesztés, az individuális
bánásmód, a tanuló személyiségvonásaiban és társas kapcsolataiban beállt
változások empatikus kezelése.

Ebben az életkorban megváltozik a motoros képességek belső
összefüggésrendszere, a képességek és a készségek átstrukturálódnak. A
fizikailag aktív, rendszeresen sportoló tanulónál az átrendeződés
általában gördülékenyebben megy végbe. Az inaktívabb életmódot folytató
tanulónál azonban nehézkes a mozgásvégrehajtás, a korábbiaknál nagyobb
fáradékonyság, a mozgások koordinálatlansága, csökkenő mozgástanulási és
-szabályozó képesség, valamint fokozódó mozgásváltozékonyság figyelhető
meg.

A testnevelés tantárgy tanításának és tanulásának specifikus jellemzője,
hogy a tanuló megtanul kitartóan és megfelelő iramot diktálva futni,
különféle módokon ugrani és dobni, valamint különböző tornaelemeket és
gyakorlatokat sajátít el. Képessé válik rá, hogy különböző
labdajátékokban részt vegyen, a legváltozatosabb feladathelyzetekben,
korosztályának megfelelő szintű kreativitással. Megismeri a küzdősportok
szerepét, a szabadban végzett testedzés fontosságát. Amennyiben a
lehetőségek adottak hozzá, tovább mélyíti úszástudását. Tudatosan
törekszik a biomechanikailag helyes testtartás kialakítására. A felső
tagozaton is lépéseket kell tenni annak érdekében, hogy teljesüljön a
tananyag spirális építkezése és a mozgásos ismeretek fokozatos bővülése.

A tantárgy tanításának és tanulásának ebben az életkorban is alapvető
módszere a játék, a játékosság, amely elsősorban itt is a koordinációs
képességek fejlesztésére szolgál. A mozgásos játékok közben fejlődik a
térbeli és az időbeli tájékozódási képesség, az irányérzék, bővül a
relációs szókincs. Kiemelt feladat a cselekvő tapasztalatszerzés, amely
a testmozgás mellett sokféle érzékelés bevonásával és változatos
eszközök használatával valósítható meg.

***A tantárgy tanításának specifikus jellemzői a 9--12. évfolyamon***

A középfokú képzésben a 9--12. évfolyam -- jellemzően a 14--18 éves
életkor -- az egyéni sajátosságok és a nemi különbségek kialakulásának
időszaka. A tanuló mozgástevékenységét, mozgásos cselekvéseit a
pubertás- és a posztpubertáskor mozgásfejlődésének sajátos biológiai
érési tényezői határozzák meg. A 9--12. évfolyam tanulójára is igaz az
5--8. osztálynál már említett kitétel, miszerint az életkorra jellemző
jelentős egyéni eltérések miatt előtérbe kerül a differenciált
fejlesztés, az individuális bánásmód és a tanuló problémáinak empatikus
kezelése.

A serdülőkorban és az azt követő években -- a leányoknál jellemzőbben --
megnövekszik a testsúly, megváltozik a testösszetétel. A fizikai
inaktivitás, az egészségtelen életmód és étrend ebben az életkorban
ágyaz meg a fiatal- és felnőttkori elhízottságnak, amely számos betegség
rizikófaktora.

A tanuló szervezete a fejlődés-fejlesztés során számos hatást integrál,
ami hosszú időre -- egyesesetekben élethosszig -- megszabja a
személyiség, ezen belül a motoros készségek fejlődésének irányát,
fejlettségének színvonalát. A mindennapi életben megfigyelhető motoros
fenotípus ezeknek a hatásoknak az eredője.

A testnevelés tantárgy tanulásának és tanításának továbbra is specifikus
jellemzője marad, hogy a tanuló megtanul kitartóan és megfelelő iramot
diktálva futni, különféle módokon ugrani és dobni, valamint különböző
tornaelemeket és gyakorlatokat sajátít el. Képessé válik a
legváltozatosabb feladathelyzetekben korosztályának megfelelő szintű
kreativitással különböző labdajátékokban részt venni. Megismeri az
önvédelem és a küzdősportok szerepét, a szabadban végzett testedzés
fontosságát. Amennyiben a lehetőségek adottak hozzá, tovább mélyíti
úszástudását. Tudatosan alkalmaz gyakorlatokat a biomechanikailag helyes
testtartás kialakítására. A középiskolában is lépéseket kell tenni annak
érdekében, hogy teljesülhessen a tananyag spirális építkezése, a
mozgásos ismeretek fokozatos bővülése. Ebben az életkorban a
pszichomotorium markáns jellemzőjévé válik az alkotás mint tevékenység,
amely során a tanuló nemcsak reprodukál, hanem tervez és produkál is.

***B) FŐ TÉMAKÖRÖK***

***FŐ TÉMAKÖRÖK AZ 1--4. ÉVFOLYAMON***

1. Gimnasztika és rendgyakorlatok -- prevenció, relaxáció

2. Kúszások és mászások

3. Járások és futások

4. Szökdelések és ugrások

5. Dobások és ütések

6. Támasz-, függés- és egyensúlygyakorlatok

7. Labdás gyakorlatok

8. Testnevelési és népi játékok

9. Küzdőfeladatok és -játékok

10. Foglalkozások alternatív környezetben

11. Úszás

12. Gyógytestnevelés

***FŐ TÉMAKÖRÖK AZ 5--8. ÉVFOLYAMON***

1. Gimnasztika és rendgyakorlatok -- prevenció, relaxáció

2. Atlétikai jellegű feladatmegoldások

3. Tornajellegű feladatmegoldások

4. Sportjátékok (az iskola tárgyi és személyi feltételeinek
    függvényében két sportjáték választása)

5. Testnevelési és népi játékok

6. Önvédelmi és küzdősportok

7. Alternatív környezetben űzhető mozgásformák

8. Úszás (amennyiben adottak a körülmények)

9. Gyógytestnevelés

***FŐ TÉMAKÖRÖK A 9--12. ÉVFOLYAMON***

1. Gimnasztika és rendgyakorlatok -- prevenció, relaxáció

2. Atlétikai jellegű feladatmegoldások

3. Tornajellegű feladatmegoldások

4. Ritmikus gimnasztika és aerobik (választható)

5. Sportjátékok (az iskola tárgyi és személyi feltételeinek
    függvényében két sportjáték választása)

6. Testnevelési és népi játékok

7. Önvédelmi és küzdősportok

8. Alternatív környezetben űzhető mozgásformák

9. Úszás (amennyiben adottak a körülmények)

10. Gyógytestnevelés

***C) TANULÁSI EREDMÉNYEK***

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 1--4.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. életkorának és testi adottságának megfelelően fejlődött motoros
    teljesítőképessége a hozzá kapcsolódó ismeretekkel olyan mérvű, hogy
    képes a saját teljesítménye tudatos befolyásolására;

2. mozgáskultúrája olyan szintre fejlődött, hogy képes a hatékony
    mozgásos cselekvéstanulásra, testedzésre;

3. ismeri a testnevelés életkorához igazodó elméleti ismeretanyagát,
    szakkifejezéseit, helyes terminológiáját, érti azok szükségességét;

4. megismeri az elsősegélynyújtás jelentőségét, felismeri a baleseti és
    egészségkárosító veszélyforrásokat, képes azonnali segítséget kérni;

5. önismerete, érzelmi-akarati készségei és képességei a testmozgás, a
    testnevelés és a sport eszközei által megfelelően fejlődtek.

MOZGÁSKULTÚRA-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult mozgásformákat összefüggő cselekvéssorokban, jól
    koordináltan kivitelezi;

2. a tanult mozgásforma könnyed és pontos kivitelezésének
    elsajátításáig fenntartja érzelmi-akarati erőfeszítéseit;

3. a sportjátékok, a testnevelési és népi játékok művelése során
    egyaránt törekszik a szabályok betartására;

4. nyitott az alapvető mozgásformák újszerű és alternatív környezetben
    történő alkalmazására, végrehajtására.

MOTOROSKÉPESSÉG-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. megfelelő motoros képességszinttel rendelkezik az alapvető
    mozgásformák viszonylag önálló és tudatos végrehajtásához;

2. olyan szintű relatív erővel rendelkezik, amely lehetővé teszi
    összefüggő cselekvéssorok kidolgozását, az elemek közötti összhang
    megteremtését;

3. megfelelő általános állóképesség-fejlődést mutat.

MOZGÁSKÉSZSÉG-KIALAKÍTÁS -- MOZGÁSTANULÁS

A nevelési-oktatási szakasz végére a tanuló:

1. az alapvető mozgásformákat külsőleg meghatározott ritmushoz, a
    társak mozgásához igazított sebességgel és dinamikával képes
    végrehajtani;

2. mozgásműveltsége szintjénél fogva pontosan hajtja végre a keresztező
    mozgásokat;

3. futását összerendezettség, lépésszabályozottság, ritmusosság
    jellemzi;

4. különböző mozgásai jól koordináltak, a hasonló mozgások szimultán és
    egymást követő végrehajtása jól tagolt;

5. az egyszerűbb mozgásformákat jól koordináltan hajtja végre, a
    hasonló mozgások szimultán és egymást követő végrehajtásában
    megfelelő szintű tagoltságot mutat;

6. a különböző ugrásmódok alaptechnikáit és előkészítő mozgásformáit a
    vezető műveletek ismeretében tudatosan és koordináltan hajtja végre;

7. a különböző dobásmódok alaptechnikáit és előkészítő mozgásformáit a
    vezető műveletek ismeretében tudatosan és koordináltan hajtja végre;

8. a támasz- és függésgyakorlatok végrehajtásában a testtömegéhez
    igazodó erő- és egyensúlyozási képességgel rendelkezik;

9. a funkcionális hely- és helyzetváltoztató mozgásformáinak
    kombinációit változó feltételek között koordináltan hajtja végre;

10. labdás ügyességi szintje lehetővé teszi az egyszerű taktikai
    helyzetekre épülő folyamatos, célszerű játéktevékenységet;

11. a megtanultak birtokában örömmel, a csapat teljes jogú tagjaként
    vesz részt a játékokban;

12. vállalja a társakkal szembeni fizikai kontaktust, sportszerű
    test-test elleni küzdelmet valósít meg;

13. ismeri és képes megnevezni a küzdőfeladatok, esések, tompítások
    játék- és baleset-megelőzési szabályait;

14. ellenőrzött tevékenység keretében mozog a szabad levegőn, egyúttal
    tudatosan felkészül az időjárás kellemetlen hatásainak elviselésére
    sportolás közben;

15. az elsajátított egy (vagy több) úszásnemben helyes technikával
    úszik.

JÁTÉKOK

A nevelési-oktatási szakasz végére a tanuló:

1. a testnevelési és népi játékokban tudatosan, célszerűen alkalmazza
    az alapvető mozgásformákat;

2. játék közben az egyszerű alaptaktikai elemek tudatos alkalmazására
    törekszik, játék- és együttműködési készsége megmutatkozik;

3. célszerűen alkalmaz sportági jellegű mozgásformákat
    sportjáték-előkészítő kisjátékokban;

4. játéktevékenysége közben a tanult szabályokat betartja.

VERSENGÉSEK, VERSENYEK

A nevelési-oktatási szakasz végére a tanuló:

1. a versengések és a versenyek közben toleráns a csapattársaival és az
    ellenfeleivel szemben;

2. a versengések és a versenyek tudatos szereplője, a közösséget
    pozitívan alakító résztvevő;

3. a szabályjátékok közben törekszik az egészséges versenyszellem
    megőrzésére;

4. felismeri a sportszerű és sportszerűtlen magatartásformákat,
    betartja a sportszerű magatartás alapvető szabályait.

PREVENCIÓ, ÉLETVITEL

A nevelési-oktatási szakasz végére a tanuló:

1. felismeri a különböző veszély- és baleseti forrásokat,
    elkerülésükhöz tanári segítséget kér;

2. ismeri a keringési, légzési és mozgatórendszerét fejlesztő alapvető
    mozgásformákat;

3. tanári segítséggel megvalósít a biomechanikailag helyes testtartás
    kialakítását elősegítő gyakorlatokat;

4. tanári irányítással, ellenőrzött formában végzi a testnevelés --
    számára nem ellenjavallt -- mozgásanyagát;

5. aktívan vesz részt az uszodában végzett mozgásformák
    elsajátításában, gyakorlásában;

6. a családi háttere és a közvetlen környezete adta lehetőségeihez
    mérten rendszeresen végez testmozgást.

EGÉSZSÉGES TESTI FEJLŐDÉS, EGÉSZSÉGFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. az öltözködés és a higiéniai szokások terén teljesen önálló, adott
    esetben segíti társait;

2. megismeri az életkorának megfelelő sporttáplálkozás alapelveit,
    képes különbséget tenni egészséges és egészségtelen tápanyagforrások
    között;

3. a szabadban végzett foglalkozások során nem csupán ügyel környezete
    tisztaságára és rendjére, hanem erre felhívja társai figyelmét is;

4. ismeri a helyes testtartás egészségre gyakorolt pozitív hatásait.

A gyógytestnevelésre utalt tanuló megismeri a testnevelés, az
egészségfejlesztés és a sport azon eszközeit, módszereit, amelyek
segítséget nyújthatnak számára az egészségi állapotának és a motoros
teljesítőképességének lehető legnagyobb mértékű helyreállításához.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 5--8.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. életkorának és testi adottságának megfelelően fejlődött motoros
    teljesítőképessége olyan mérvű, hogy képes a saját teljesítménye és
    fittségi szintje tudatos befolyásolására;

2. sokoldalú mozgásműveltségének birtokában eredményesen tanul
    összetett mozgásformákat;

3. ismeri és használja a testnevelés életkorához igazodó elméleti
    ismeretanyagát, szakkifejezéseit, helyes terminológiáját;

4. önismerete, érzelmi-akarati készségei és képességei a testmozgás, a
    testnevelés és a sport eszközei által megfelelően fejlődtek;

5. képes értelmezni az életben adódó baleseti forrásokat és az
    egészséget károsító, veszélyes szokásokat, tevékenységeket.

MOZGÁSKULTÚRA-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult alapvető mozgásformák kombinációiból álló cselekvéssorokat
    változó térbeli, időbeli, dinamikai feltételek mellett
    készségszinten kivitelezi;

2. a tanult mozgásforma készségszintű kivitelezése közben fenntartja
    érzelmi-akarati erőfeszítéseit;

3. minden sporttevékenységében forma- és szabálykövető attitűddel
    rendelkezik, ez tevékenységének automatikus részévé válik;

4. nyitott az alapvető mozgásformák újszerű és alternatív környezetben
    történő felhasználására, végrehajtására.

MOTOROSKÉPESSÉG-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a motoros képességeinek fejlődési szintje révén képes az összhang
    megteremtésére a cselekvéssorainak elemei között;

2. relatív erejének birtokában képes a sportágspecifikus
    mozgástechnikák koordinált, készségszintű kivitelezésére;

3. az alapvető mozgásainak koordinációjában megfelelő begyakorlottságot
    mutat, és képes a változó környezeti feltételekhez célszerűen
    illeszkedő végrehajtásra;

4. a (meg)tanult erő-, gyorsaság-, állóképesség- és ügyességfejlesztő
    eljárásokat tanári irányítással tudatosan alkalmazza.

MOZGÁSKÉSZSÉG-KIALAKÍTÁS -- MOZGÁSTANULÁS

A nevelési-oktatási szakasz végére a tanuló:

1. futótechnikája -- összefüggő cselekvéssor részeként -- eltérést
    mutat a vágta- és a tartós futás közben;

2. a rajttechnikákat a játékok, a versengések és a versenyek közben
    készségszinten használja;

3. magabiztosan alkalmazza a távol- és magasugrás, valamint a
    kislabdahajítás és súlylökés -- számára megfelelő -- technikáit;

4. segítségadással képes egy-egy általa kiválasztott tornaelem
    bemutatására és a tanult elemekből önállóan alkotott gyakorlatsor
    kivitelezésére;

5. a torna, a ritmikus gimnasztika, tánc és aerobik jellegű
    mozgásformákon keresztül tanári irányítás mellett fejleszti
    esztétikai-művészeti tudatosságát és kifejezőképességét;

6. a testnevelési és sportjáték közben célszerű, hatékony játék- és
    együttműködési készséget mutat;

7. a tanári irányítást követve, a mozgás sebességét növelve hajt végre
    önvédelmi fogásokat, ütéseket, rúgásokat, védéseket és
    ellentámadásokat;

8. ellenőrzött tevékenység keretében rendszeresen mozog, edz, sportol a
    szabad levegőn, egyúttal tudatosan felkészül az időjárás kellemetlen
    hatásainak elviselésére sportolás közben;

9. az elsajátított egy (vagy több) úszásnemben helyes technikával,
    készségszinten úszik.

JÁTÉKOK

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult testnevelési és népi játékok mellett folyamatosan, jól
    koordináltan végzi a választott sportjátékokat;

2. a sportjátékok előkészítő kisjátékaiban tudatosan és célszerűen
    alkalmazza a technikai és taktikai elemeket;

3. a küzdő jellegű feladatokban életkorának megfelelő asszertivitást
    mutatva tudatosan és célszerűen alkalmazza a támadó és védő
    szerepeknek megfelelő technikai és taktikai elemeket.

VERSENGÉSEK, VERSENYEK

A nevelési-oktatási szakasz végére a tanuló:

1. a versengések és a versenyek közben toleráns a csapattársaival és az
    ellenfeleivel szemben, ezt tőlük is elvárja;

2. a versengések és a versenyek közben közösségformáló, csapatkohéziót
    kialakító játékosként viselkedik;

3. egészséges versenyszellemmel rendelkezik, és tanári irányítás vagy
    ellenőrzés mellett képes a játékvezetésre.

PREVENCIÓ, ÉLETVITEL

A nevelési-oktatási szakasz végére a tanuló:

1. megoldást keres a különböző veszély- és baleseti források
    elkerülésére;

2. tanári segítséggel, egyéni képességeihez mérten, tervezetten,
    rendezetten és rendszeresen fejleszti keringési, légzési és
    mozgatórendszerét;

3. tervezetten, rendezetten és rendszeresen végez a biomechanikailag
    helyes testtartás kialakítását elősegítő gyakorlatokat;

4. a mindennapi sporttevékenységébe tudatosan beépíti a korrekciós
    gyakorlatokat;

5. tudatosan, összehangoltan végzi a korrekciós gyakorlatait és uszodai
    tevékenységét, azok megvalósítása automatikussá, mindennapi életének
    részévé válik;

6. rendszeresen végez számára megfelelő vízi játékokat, és hajt végre
    úszástechnikákat;

7. ismeri a tanult mozgásformák gerinc- és ízületvédelmi szempontból
    helyes végrehajtását;

8. megnevez és bemutat egyszerű relaxációs gyakorlatokat;

9. a családi háttere és a közvetlen környezete adta lehetőségeihez
    mérten tervezetten, rendezetten és rendszeresen végez testmozgást.

EGÉSZSÉGES TESTI FEJLŐDÉS, EGÉSZSÉGFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a higiéniai szokások terén teljesen önálló, adott esetben segíti
    társait;

2. az életkorának és alkati paramétereinek megfelelően tervezett,
    rendezett és rendszeres, testmozgással összefüggő táplálkozási
    szokásokat alakít ki;

3. a szabadban végzett foglalkozások során nem csupán ügyel környezete
    tisztaságára és rendjére, hanem erre felhívja társai figyelmét is;

4. a helyes testtartás egészségre gyakorolt pozitív hatásai ismeretében
    önállóan is kezdeményez ilyen tevékenységet.

A gyógytestnevelésre utalt tanuló megismeri a testnevelés, az
egészségfejlesztés és a sport azon eszközeit, módszereit, amelyek
segítséget nyújthatnak számára az egészségi állapotának és a motoros
teljesítőképességének lehető legnagyobb mértékű helyreállításához.

***ÁTFOGÓ CÉLKÉNT KITŰZÖTT, VALAMINT A FEJLESZTÉSI TERÜLETEKHEZ
KAPCSOLÓDÓ TANULÁSI EREDMÉNYEK (ÁLTALÁNOS KÖVETELMÉNYEK) AZ 9--12.
ÉVFOLYAMON***

A nevelési-oktatási szakasz végére a tanuló:

1. alkotó módon használja a testnevelés életkorához igazodó
    ismeretanyagát, szakkifejezéseit, helyes terminológiáját;

2. megismeri és mindennapjai részévé teszi a mozgáshoz kapcsolódó
    helyes attitűdöket, a fizikailag aktív életmód és a társas-érzelmi
    jóllét élethosszig tartó jótékony hatásait;

3. képes elhárítani a baleseti és veszélyforrásokat, magabiztosan
    segíteni és elsősegélyt nyújtani embertársainak;

4. társas-közösségi kapcsolatai, valamint stressztűrő és -kezelő
    képességei megfelelő szintre fejlődtek;

5. toleráns a testi és más fogyatékossággal élő személyek iránt,
    megismeri és tiszteletben tartja a szexuális kultúra alapelveit,
    elfogadja az egészségügyi szűrések és a környezetvédelem
    fontosságát.

MOZGÁSKULTÚRA-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult mozgásformákat alkotó módon, a testedzés és a sportolás
    minden területén használja;

2. a testedzéshez, a sportoláshoz kívánatosnak tartott jellemzőknek
    megfelelően (fegyelmezetten, határozottan, lelkiismeretesen,
    innovatívan és kezdeményezően) törekszik végrehajtani az
    elsajátított mozgásformákat;

3. sporttevékenységében spontán, automatikus forma- és szabálykövető
    attitűdöt követ;

4. nyitott az alapvető és sportágspecifikus mozgásformák újszerű és
    alternatív környezetben történő felhasználására, végrehajtására.

MOTOROSKÉPESSÉG-FEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. olyan szintű motoros képességekkel rendelkezik, amelyek lehetővé
    teszik a tanult mozgásformák alkotó módon történő végrehajtását;

2. relatív erejének birtokában a tanult mozgásformákat változó
    környezeti feltételek mellett, hatékonyan és készségszinten
    kivitelezi;

3. a különböző sportágspecifikus mozgásformákat változó környezeti
    feltételek mellett, hatékonyan és készségszinten hajtja végre;

4. a (meg)tanult erő-, gyorsaság-, állóképesség- és ügyességfejlesztő
    eljárásokat önállóan, tanári ellenőrzés nélkül alkalmazza;

5. tanári ellenőrzés mellett digitálisan méri és értékeli a
    kondicionális és koordinációs képességeinek változásait, ezekből
    kiindulva felismeri saját motoros képességbeli hiányosságait, és
    ezeket a képességeket tudatosan és rendszeresen fejleszti.

MOZGÁSKÉSZSÉG-KIALAKÍTÁS -- MOZGÁSTANULÁS

A nevelési-oktatási szakasz végére a tanuló:

1. a korábbi évfolyamokon elért eredményeihez képest folyamatosan
    javítja futóteljesítményét, amelyet önmaga is tudatosan nyomon
    követ;

2. a rajtolási módokat a játékok, versenyek, versengések közben
    hatékonyan, kreatívan alkalmazza;

3. képes a kiválasztott ugró- és dobótechnikákat az ilyen jellegű
    játékok, versengések és versenyek közben, az eredményesség
    érdekében, egyéni sajátosságaihoz formálva hatékonyan alkalmazni;

4. önállóan képes az általa kiválasztott elemkapcsolatokból
    tornagyakorlatot összeállítani, majd bemutatni;

5. a torna, ritmikus gimnasztika, aerobik és tánc jellegű
    mozgásformákon keresztül fejleszti esztétikai-művészeti tudatosságát
    és kifejezőképességét;

6. a zenei ütemnek megfelelően, készségszintű koordinációval végzi a
    kiválasztott ritmikus gimnasztika, illetve aerobik mozgásformákat;

7. önállóan képes az életben adódó, elkerülhetetlen veszélyhelyzetek
    célszerű hárítására;

8. a különböző eséstechnikák készségszintű elsajátítása mellett a
    választott küzdősport speciális mozgásformáit célszerűen alkalmazza;

9. rendszeresen mozog, edz, sportol a szabad levegőn, erre −
    lehetőségeihez mérten − társait is motiválja;

10. az elsajátított egy (vagy több) úszásnemben vízbiztosan,
    készségszinten úszik, a természetes vizekben is;

11. önállóan képes az elkerülhetetlen vízi veszélyhelyzetek célszerű
    kezelésére.

JÁTÉKOK

A nevelési-oktatási szakasz végére a tanuló:

1. a tanult testnevelési, népi és sportjátékok összetett technikai és
    taktikai elemeit kreatívan, az adott játékhelyzetnek megfelelően,
    célszerűen, készségszinten alkalmazza;

2. játéktevékenységét kreativitást mutató játék- és együttműködési
    készség jellemzi.

VERSENGÉSEK, VERSENYEK

A nevelési-oktatási szakasz végére a tanuló:

1. a versengések és a versenyek közben toleráns a csapattársaival és az
    ellenfeleivel szemben, ezt tőlük is elvárja;

2. a versengések és a versenyek közben közösségformáló, csapatkohéziót
    kialakító játékosként viselkedik;

3. a szabályjátékok alkotó részese, képes szabálykövető játékvezetésre.

PREVENCIÓ, ÉLETVITEL

A nevelési-oktatási szakasz végére a tanuló:

1. megoldást keres a különböző veszély- és baleseti források
    elkerülésére, erre társait is motiválja;

2. az egyéni képességeihez mérten, mindennapi szokásrendszerébe építve
    fejleszti keringési, légzési és mozgatórendszerét;

3. belső igénytől vezérelve rendszeresen végez a biomechanikailag
    helyes testtartás kialakítását elősegítő gyakorlatokat;

4. mindennapi tevékenységének tudatos részévé válik a korrekciós
    gyakorlatok végzése;

5. a szárazföldi és az uszodai korrekciós gyakorlatait készségszinten
    sajátítja el, azokat tudatosan rögzíti;

6. önállóan, de tanári ellenőrzés mellett végez számára megfelelő
    uszodai tevékenységet;

7. a családi háttere és a közvetlen környezete adta lehetőségeihez
    mérten, belső igénytől vezérelve, alkotó módon, rendszeresen végez
    testmozgást;

8. ismer és alkalmaz alapvető relaxációs technikákat.

EGÉSZSÉGES TESTI FEJLŐDÉS, EGÉSZSÉGFEJLESZTÉS

A nevelési-oktatási szakasz végére a tanuló:

1. mindennapi életének részeként kezeli a testmozgás, a sportolás
    közbeni higiéniai és tisztálkodási szabályok betartását;

2. az életkorának és alkati paramétereinek megfelelő pozitív,
    egészégtudatos, testmozgással összefüggő táplálkozási szokásokat
    alakít ki;

3. a szabadban végzett foglalkozások során nem csupán ügyel környezete
    tisztaságára és rendjére, hanem erre felhívja társai figyelmét is;

4. megoldást keres a testtartási rendellenesség kialakulásának
    megakadályozására, erre társait is motiválja.

A gyógytestnevelésre utalt tanuló megismeri a testnevelés, az
egészségfejlesztés és a sport azon eszközeit, módszereit, amelyek
segítséget nyújthatnak számára az egészségi állapotának és a motoros
teljesítőképességének lehető legnagyobb mértékű helyreállításához. A
gyógytestnevelésre utalt tanuló esetében a mindennapi testnevelés
megvalósítása részben vagy egészben gyógytestnevelés formájában
történhet. "

12\. A Rendelet Melléklet I. Rész I.1. pontjában az „*a Nemzeti
alaptanterv (a továbbiakban: Nat)*" szövegrész helyébe az „*a Nat*"
szöveg lép.

13\. A Rendelet Melléklet I. Rész I.1.1. pont Állampolgárságra,
demokráciára nevelés alpontjában a „kritikai" szövegrész helyébe a
„mérlegelő" szöveg lép.

14\. A Rendelet Melléklet I. Rész I.2. pontjában a „*kiadott, illetve
jóváhagyott*" szövegrész helyébe a „*közzétett*" szöveg lép.

15\. A Rendelet Melléklet I. Rész I.2.1. pont

*a)* A mindennapos testnevelés című alpontjában a „*Testnevelés és
sport*" szövegrész helyébe a „*Testnevelés és egészségfejlesztés*"
szöveg,

*b)* A nemzetiségi nevelés és oktatás elvei című alpontban az *„az
óraterv kialakításakor biztosítani kell egy idegen nyelv oktatását is;"*
szövegrész helyébe az *„az óraterv kialakításakor biztosítani kell egy
idegen nyelv oktatását is. Az intézmény pedagógiai programjában dönthet
úgy, hogy az idegen nyelv oktatásához biztosított óraszámot a
nemzetiségi nyelv oktatására részben vagy egészben átcsoportosítja. A
romani és beás nyelvek mellett kötelező az idegen nyelv;"* szöveg

lép.

16\. A Rendelet Melléklet II. Rész címe a „KOMPETENCIÁK, TUDÁSTARTALMAK"
címre módosul.

17\. A Rendelet Melléklet II. Rész II.2.1. pontjában

*a)* a *„Képzési jellege szerint lehet szakiskola, szakközépiskola vagy
gimnázium; ezek a szakképesítés, az ágazat vagy a tagozatok szerint
eltérő programok alapján haladnak."* szövegrész helyébe a *„Képzési
jellege szerint lehet gimnázium, szakgimnázium, technikum vagy szakképző
iskola; ezek a szakképesítés, szakképzettség vagy az ágazat szerint
eltérő programok alapján haladnak."* szöveg,

*b)* az *„A Nat-ban megjelenített műveltségterületi követelmények azonos
szerkezetűek. Az Alapelvek, célok című fejezetet a Fejlesztési feladatok
követik, majd a fent említett nevelési-oktatási szakaszoknak megfelelően
a közműveltségi tartalmak zárják."* szövegrész helyébe az *„A tartalmi
szabályozókban tanulási terület alatt műveltségi területet kell
érteni."* szöveg

lép.

18\. Hatályát veszti a Rendelet Melléklet I. Rész I.2. pontjában

a\) a „választott",

b\) az „átlagosan 10%-os"

szövegrész.

19\. Hatályát veszti a Rendelet Melléklet I. Rész I.2. pont A hit- és
erkölcstan oktatására vonatkozó szabályok című alpontja.

20\. Hatályát veszti a Rendelet Melléklet I. Rész I.2.1. pont Az Arany
János programok című alpontjában az *„Az Arany János
Kollégiumi-Szakiskolai Program célja, hogy hatékonyan segítse a korai
iskolaelhagyás csökkenését, a kollégium és a szakiskola befogadó
pedagógiai környezetben, differenciált tanulásszervezéssel és
pályaorientációval esélyt teremtsen a halmozottan hátrányos helyzetű
tanulóknak a piacképes szakma megszerzésére."* szövegrész.

21\. Hatályát veszti a Rendelet Melléklet II. Rész II.2. pont

*a)* II.2.2. alpontja,

*b)* II.2.3. alpontja.

22\. Hatályát veszti a Rendelet Melléklet III. Része.
