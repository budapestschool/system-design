# Miért szükséges a BPS egyedi megoldás

A miniszter által közzétett kerettantervek mellett a Budapest School Általános Iskola és Gimnázium (BPS, Budapest School) egyedi megoldásainak alkalmazása azért szükséges, hogy egy személyreszabható, tanárok által vezetett, önvezérelt tanulást előtérbe helyező tanulási környezetet tudjunk létrehozni a Budapest School telephelyein.

## A Budapest School egy személyre szabható családi, közösségi iskola

A BPS a NAT és Nkt. elveit és előírásait alapul véve egyedi megoldásokat alkalmaz abból a célból, hogy megvalósíthassa a családi-közösségi iskola koncepcióját. A közösségi iskola a szülők jelentős bevonását is jelenti, akik tevékeny és alakító részesei a gyerek tanulásának. A családi iskola keretei eltérnek az Nkt.-ban, NAT-ban és a kerettantervekben megfogalmazottaktól elsősorban a körben, hogy a tanárok (tanulásszervezők) irányítási, döntési jogköre, felhatalmazása tágabb.

## A Budapest School célja, hogy a gyerekek tanulási útjukat sajátjukként éljék meg

Egyedi megoldások alkalmazása segíti a BPS-t abban, hogy megvalósíthassa, hogy a gyerekek tanulása saját döntés eredménye legyen, és felelősséget érezzenek annak alakításáért, az újabb és újabb kihívások megtalálásáért. A NAT tananyag tartalmának szem előtt tartásával a gyerek haladása ("tanulási útja") nem kizárólag lineáris lehet a BPS rendszerében. Nagyobb hangsúly kerülhet így egyes tantárgyakban, ismeretanyagban való elmélyedésre.

## A Budapest School iskola-szervezési rendszerének alapja a ,,tanulóközösség"

Ez az alapegység eltér az osztály-rendszertől. Számos kutatás bizonyítja, hogy a gyerekek mentális életkora és az egy területeken való fejlettsége között jelentős különbségek lehetnek. Az egyedi megoldások lehetővé teszik, hogy a BPS-ben kevert korosztályú csoportok működjenek állandó közösségként ("tanulóközösség"), illetve, hogy rugalmasan, különböző szempontok szerint legyenek alakíthatók az egyes foglalkozásokon, projektekben részt vevő tanuló csoportok.

## A Budapest School rendszerét a tanárok folyamatosan alakítják, fejlesztik

A Budapest School pedagógiai rendszere a tanár feladatának tekinti, hogy mindig az optimálisnak tűnő tanulási, tanítási, gyakorlási módszert válassza. Ez - az általánosan lefektetett oktatási jogszabályokban foglaltaknál - nagyobb felelősség és egyben nagyobb felhatalmazás is a tanárok, tanulásszervezők irányában. Az ehhez szükséges rugalmasságot az egyedi megoldások biztosítják.

## A Budapest School kiemelt hangsúlyt fektet az érzelmi biztonságra, a gyerek boldogságára

A gyerekeket érő érzelmi behatások folyamatos nyomon követése, ezek jelentőségének felismerése, a megfelelő válaszok keresése összetett és több szakterületet érintő pedagógiai feladat. Célszerű, ám nehezen kivitelezhető, ha e körben az iskola-otthon határ nem jelenik meg élesen a gyerekek mindennapjaiban. Ennek megoldásában az egy tanárra jutó kisebb gyerekszám, és ezáltal nagyobb figyelem, a tanárok (tanulásszervezők) mentor szerepe, illetve az segít, hogy a tanulóközösség közösség a gyerek-tanár-szülő hármasból áll össze. Az egyedi megoldások lehetővé teszik, hogy a Budapest School kiemelt figyelmet fordítson a gyerekek boldogságára.

## A Budapest Schoolban a tantárgyközi tevékenységek vannak előtérben

A gyerekek mindennapjait meghatározó foglalkozások több műveltségi területet, többféle kompetenciát, több tantárgy anyagát is lefedhetik, és egy tantárgy anyagát több foglalkozás is érintheti.
