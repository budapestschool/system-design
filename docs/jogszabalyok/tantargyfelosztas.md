# Gyerekek száma a különböző telephelyeken
|telephely| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|---------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|POZSONYI |  9|  4| 10|  7|  5| 11| 14| 11|  3|  0|  0|  0|
|NÁNÁSI   |  5| 12| 10|  4|  3|  1|  0|  0|  0|  0|  0|  0|
|RAKPART  |  0|  2|  3|  2|  2|  1|  6| 13| 32| 16|  3|  0|
|SOLYMAR  | 12| 14|  8|  5|  1|  1|  4|  0|  0|  0|  0|  0|
|BREZNO   | 24| 19| 15| 20|  0|  0|  0|  0|  0|  0|  0|  0|
# Tantárgyi óraszámok
|           Tantárgy            | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|-------------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|állampolgári ismeretek         |  0|  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  1|
|biológia                       |  0|  0|  0|  0|  0|  0|  0|  0|  3|  2|  0|  0|
|digitális kultúra              |  0|  0|  1|  1|  1|  1|  1|  1|  2|  1|  2|  0|
|dráma és színház               |  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|
|első élő idegen nyelv          |  0|  0|  0|  2|  3|  3|  3|  3|  3|  3|  4|  4|
|ének-zene                      |  2|  2|  2|  2|  2|  1|  1|  1|  1|  1|  0|  0|
|etika                          |  1|  1|  1|  1|  1|  1|  2|  2|  0|  0|  0|  0|
|fizika                         |  0|  0|  0|  0|  0|  0|  0|  0|  2|  3|  0|  0|
|földrajz                       |  0|  0|  0|  0|  0|  0|  0|  0|  2|  1|  0|  0|
|hon- és népismeret             |  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|
|kémia                          |  0|  0|  0|  0|  0|  0|  0|  0|  1|  2|  0|  0|
|környezetismeret               |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|magyar nyelv és irodalom       |  7|  7|  5|  5|  4|  4|  3|  3|  3|  4|  4|  4|
|második idegen nyelv           |  0|  0|  0|  0|  0|  0|  0|  0|  3|  3|  3|  3|
|matematika                     |  4|  4|  4|  4|  4|  4|  3|  3|  3|  3|  3|  3|
|mozgóképkultúra és médiaismeret|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  1|
|technika és tervezés           |  1|  1|  1|  1|  1|  1|  1|  0|  0|  0|  0|  0|
|természettudomány              |  0|  0|  0|  0|  2|  2|  4|  5|  0|  0|  2|  0|
|testnevelés                    |  5|  5|  5|  5|  5|  5|  5|  5|  5|  5|  5|  5|
|történelem                     |  0|  0|  0|  0|  2|  2|  2|  2|  2|  2|  3|  3|
|vizuális kultúra               |  2|  2|  2|  1|  1|  1|  1|  1|  1|  1|  0|  0|
|kötött célú órakeret           |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  4|  4|
elso luca.david@budapestschool.org
elso fanni.pilz@budapestschool.org
elso rupert.zsofi@budapestschool.org
# Ki, mit tanít kinek a BREZNO telephelyen
|          Tanár           |        Tantárgy        | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|--------------------------|------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|Szabó Adrienn             |magyar nyelv és irodalom|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Bundschuh Hanna           |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Csóti Orsolya             |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kemenes-Nagy Dominika Rita|mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Fülöp Gabriella           |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|kulsos                    |digitális kultúra       |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |első élő idegen nyelv   |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |környezetismeret        |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |magyar nyelv és irodalom|  0|  0|  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |matematika              |  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  0|  0|  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |vizuális kultúra        |  0|  2|  2|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  0|  0|  2|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|Porteleki Sára            |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Zsigmond Alexandra Petra  |magyar nyelv és irodalom|  0|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |matematika              |  4|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |vizuális kultúra        |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Pintér Zsuzsanna          |etika                   |  1|  1|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
# Ki, mit tanít kinek a NÁNÁSI telephelyen
|          Tanár           |        Tantárgy        | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|--------------------------|------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|Monori Ágnes              |digitális kultúra       |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |első élő idegen nyelv   |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |környezetismeret        |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|Csányi András             |etika                   |  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |magyar nyelv és irodalom|  7|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |matematika              |  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |vizuális kultúra        |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kovács Beáta              |digitális kultúra       |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |etika                   |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |környezetismeret        |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |magyar nyelv és irodalom|  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |matematika              |  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |vizuális kultúra        |  0|  0|  2|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|Fésűs Bernadett Rita      |matematika              |  0|  0|  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  0|  0|  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|
|Kara Dávid                |vizuális kultúra        |  0|  0|  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|
|Kemenes-Nagy Dominika Rita|etika                   |  0|  0|  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|
|Pilz Fanni                |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Fülöp Gabriella           |első élő idegen nyelv   |  0|  0|  0|  0|  3|  3|  0|  0|  0|  0|  0|  0|
|                          |magyar nyelv és irodalom|  0|  0|  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|
|Szabady Helga             |természettudomány       |  0|  0|  0|  0|  2|  2|  0|  0|  0|  0|  0|  0|
|James John Veryzer        |etika                   |  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |magyar nyelv és irodalom|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |matematika              |  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |technika és tervezés    |  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  0|  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |vizuális kultúra        |  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  0|  2|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kovács Krisztián          |testnevelés             |  0|  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|
|Kovács Krisztina          |történelem              |  0|  0|  0|  0|  2|  2|  0|  0|  0|  0|  0|  0|
|kulsos                    |digitális kultúra       |  0|  0|  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|
|                          |hon- és népismeret      |  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|
|                          |testnevelés             |  0|  0|  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|
|                          |ének-zene               |  0|  0|  0|  0|  2|  1|  0|  0|  0|  0|  0|  0|
|Dávid Luca                |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Rupert Zsófia             |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
# Ki, mit tanít kinek a POZSONYI telephelyen
|       Tanár        |        Tantárgy        | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|--------------------|------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|Szabó Adrienn       |digitális kultúra       |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |első élő idegen nyelv   |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |környezetismeret        |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |matematika              |  0|  0|  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |testnevelés             |  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |vizuális kultúra        |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |ének-zene               |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|Fésűs Bernadett Rita|fizika                  |  0|  0|  0|  0|  0|  0|  0|  0|  2|  0|  0|  0|
|                    |kémia                   |  0|  0|  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|
|                    |technika és tervezés    |  0|  0|  0|  0|  1|  1|  1|  0|  0|  0|  0|  0|
|Kara Dávid          |vizuális kultúra        |  0|  0|  0|  0|  1|  1|  1|  1|  1|  0|  0|  0|
|Dudás Fanni         |magyar nyelv és irodalom|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |technika és tervezés    |  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |testnevelés             |  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |ének-zene               |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kollányi Fruzsina   |matematika              |  0|  0|  0|  0|  4|  4|  3|  3|  3|  0|  0|  0|
|Csóti Henriett      |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kántor Katalin      |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kelen Anna          |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kéri Júlia          |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kökényesi Ágnes     |biológia                |  0|  0|  0|  0|  0|  0|  0|  0|  3|  0|  0|  0|
|                    |természettudomány       |  0|  0|  0|  0|  2|  2|  4|  5|  0|  0|  0|  0|
|Cságoly Kristóf     |etika                   |  1|  1|  1|  1|  1|  1|  2|  2|  0|  0|  0|  0|
|                    |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |történelem              |  0|  0|  0|  0|  2|  2|  2|  0|  0|  0|  0|  0|
|Kovács Krisztián    |testnevelés             |  0|  0|  0|  0|  5|  5|  5|  5|  5|  0|  0|  0|
|Kovács Krisztina    |magyar nyelv és irodalom|  0|  0|  0|  0|  4|  4|  3|  3|  3|  0|  0|  0|
|                    |történelem              |  0|  0|  0|  0|  0|  0|  0|  2|  2|  0|  0|  0|
|                    |állampolgári ismeretek  |  0|  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|
|Szende Krisztina    |első élő idegen nyelv   |  0|  0|  0|  0|  3|  3|  3|  3|  3|  0|  0|  0|
|                    |második idegen nyelv    |  0|  0|  0|  0|  0|  0|  0|  0|  3|  0|  0|  0|
|kulsos              |digitális kultúra       |  0|  0|  0|  0|  1|  1|  1|  1|  2|  0|  0|  0|
|                    |dráma és színház        |  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|
|                    |hon- és népismeret      |  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|
|                    |ének-zene               |  0|  0|  0|  0|  2|  1|  1|  1|  1|  0|  0|  0|
|Fülöp László        |magyar nyelv és irodalom|  0|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |matematika              |  4|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |technika és tervezés    |  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |testnevelés             |  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |vizuális kultúra        |  2|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |ének-zene               |  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Papp Borbála        |digitális kultúra       |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |környezetismeret        |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |magyar nyelv és irodalom|  0|  0|  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |matematika              |  0|  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |technika és tervezés    |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |testnevelés             |  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |vizuális kultúra        |  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                    |ének-zene               |  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Major Rita          |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Zsemberovszky Zita  |földrajz                |  0|  0|  0|  0|  0|  0|  0|  0|  2|  0|  0|  0|
# Ki, mit tanít kinek a RAKPART telephelyen
|                Tanár                |        Tantárgy        | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|-------------------------------------|------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|Gaál-Horváth Bernadett               |digitális kultúra       |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |etika                   |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |környezetismeret        |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|Fábián Anna Mária (Kovács Anna Mária)|etika                   |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Bundschuh Hanna                      |etika                   |  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Koren Balázs                         |matematika              |  0|  0|  0|  0|  4|  4|  3|  3|  3|  0|  0|  0|
|                                     |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Szabó Bálint                         |fizika                  |  0|  0|  0|  0|  0|  0|  0|  0|  2|  3|  0|  0|
|                                     |matematika              |  0|  0|  0|  0|  0|  0|  0|  0|  0|  3|  3|  0|
|                                     |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  0|  0|  1|  1|  1|  0|  0|  0|  0|  0|
|                                     |természettudomány       |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  2|  0|
|Fésűs Bernadett Rita                 |kémia                   |  0|  0|  0|  0|  0|  0|  0|  0|  1|  2|  0|  0|
|Kara Dávid                           |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  0|  2|  2|  1|  1|  1|  1|  0|  0|  0|  0|  0|
|Kemenes-Nagy Dominika Rita           |etika                   |  0|  0|  0|  0|  1|  1|  2|  2|  0|  0|  0|  0|
|Szekeres Eszter                      |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Fülöp Gabriella                      |második idegen nyelv    |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  3|  0|
|Bánáti Hajnalka                      |biológia                |  0|  0|  0|  0|  0|  0|  0|  0|  3|  0|  0|  0|
|                                     |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |természettudomány       |  0|  0|  0|  0|  2|  2|  4|  5|  0|  0|  0|  0|
|Kökényesi Ágnes                      |biológia                |  0|  0|  0|  0|  0|  0|  0|  0|  0|  2|  0|  0|
|Szende Krisztina                     |első élő idegen nyelv   |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  4|  0|
|kulsos                               |digitális kultúra       |  0|  0|  0|  0|  1|  1|  1|  1|  2|  1|  2|  0|
|                                     |dráma és színház        |  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|
|                                     |hon- és népismeret      |  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|
|                                     |kötött célú órakeret    |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  4|  0|
|                                     |testnevelés             |  0|  0|  0|  0|  5|  5|  5|  5|  5|  5|  5|  0|
|                                     |ének-zene               |  0|  0|  0|  0|  2|  1|  1|  1|  1|  1|  0|  0|
|Mohai Mónika                         |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |történelem              |  0|  0|  0|  0|  2|  2|  2|  2|  2|  2|  3|  0|
|                                     |állampolgári ismeretek  |  0|  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|
|Schubert Tamás                       |magyar nyelv és irodalom|  0|  0|  0|  0|  4|  4|  3|  3|  3|  0|  0|  0|
|                                     |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Varga Virág                          |első élő idegen nyelv   |  0|  0|  0|  2|  3|  3|  3|  3|  3|  3|  0|  0|
|                                     |második idegen nyelv    |  0|  0|  0|  0|  0|  0|  0|  0|  3|  3|  0|  0|
|                                     |vizuális kultúra        |  0|  0|  0|  0|  0|  0|  0|  1|  1|  1|  0|  0|
|Zsemberovszky Zita                   |földrajz                |  0|  0|  0|  0|  0|  0|  0|  0|  2|  1|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  0|  0|  0|  0|  0|  0|  0|  0|  4|  4|  0|
# Ki, mit tanít kinek a SOLYMAR telephelyen
|                Tanár                |        Tantárgy        | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |10 |11 |12 |
|-------------------------------------|------------------------|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
|Gaál-Horváth Bernadett               |magyar nyelv és irodalom|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Szabó Adrienn                        |digitális kultúra       |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|Monori Ágnes                         |digitális kultúra       |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |etika                   |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |környezetismeret        |  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  0|  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Fábián Anna Mária (Kovács Anna Mária)|mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kovalóczy Anna                       |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Fésűs Bernadett Rita                 |matematika              |  0|  0|  0|  0|  4|  4|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  0|  0|  1|  1|  0|  0|  0|  0|  0|  0|
|Inkrét Boglárka                      |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kemenes-Nagy Dominika Rita           |magyar nyelv és irodalom|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kollányi Fruzsina                    |matematika              |  0|  0|  0|  0|  0|  0|  3|  0|  0|  0|  0|  0|
|Perczel-Szabó Katalin                |etika                   |  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |magyar nyelv és irodalom|  0|  7|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  4|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  1|  1|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  5|  5|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  2|  2|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kökényesi Ágnes                      |természettudomány       |  0|  0|  0|  0|  2|  2|  4|  0|  0|  0|  0|  0|
|Kovács Ágnes                         |mentoridő               |  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|  0|
|Kovács Krisztina                     |magyar nyelv és irodalom|  0|  0|  0|  0|  4|  0|  0|  0|  0|  0|  0|  0|
|Szende Krisztina                     |első élő idegen nyelv   |  0|  0|  0|  0|  3|  3|  0|  0|  0|  0|  0|  0|
|kulsos                               |digitális kultúra       |  0|  0|  0|  0|  1|  1|  1|  0|  0|  0|  0|  0|
|                                     |dráma és színház        |  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|
|                                     |első élő idegen nyelv   |  0|  0|  0|  0|  0|  0|  3|  0|  0|  0|  0|  0|
|                                     |hon- és népismeret      |  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|
|                                     |technika és tervezés    |  0|  0|  0|  0|  0|  0|  1|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  0|  0|  0|  5|  5|  5|  0|  0|  0|  0|  0|
|                                     |történelem              |  0|  0|  0|  0|  2|  2|  2|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  0|  0|  0|  0|  1|  1|  1|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  0|  0|  0|  2|  1|  1|  0|  0|  0|  0|  0|
|Porteleki Sára                       |első élő idegen nyelv   |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |környezetismeret        |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |matematika              |  0|  0|  0|  4|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |testnevelés             |  0|  0|  0|  5|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |vizuális kultúra        |  0|  0|  0|  1|  0|  0|  0|  0|  0|  0|  0|  0|
|                                     |ének-zene               |  0|  0|  0|  2|  0|  0|  0|  0|  0|  0|  0|  0|
|Zsemberovszky Zita                   |magyar nyelv és irodalom|  0|  0|  0|  0|  0|  4|  3|  0|  0|  0|  0|  0|
|Pintér Zsuzsanna                     |etika                   |  0|  0|  0|  1|  1|  1|  2|  0|  0|  0|  0|  0|
# (Szak)Tanárok jegyzéke
|                Tanár                |   Feladatellátási hely(ek)    |                   Tantárgyak                    |                                                                            Végzettség(ek)                                                                             |          Diploma kiállítója           |      Diploma száma      | Nkt. 3. sz. Melléklet szerinti alkalmazáshoz szükséges szakképzettség |
|-------------------------------------|-------------------------------|-------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|-------------------------|-----------------------------------------------------------------------|
|Bánáti Hajnalka                      |RAKPART                        |biológia és környezettan szakos tanár            |biológia, mentoridő, természettudomány                                                                                                                                 |Eötvös Loránd Tudományegyetem          |PTD/BL005429             |tanár                                                                  |
|Bundschuh Hanna                      |BREZNO,RAKPART                 |tanító                                           |etika, magyar nyelv és irodalom, mentoridő, technika és tervezés, testnevelés, ének-zene                                                                               |Eötvös Loránd Tudományegyetem          |[tanító diploma 2020-ban]|tanító                                                                 |
|Cságoly Kristóf                      |POZSONYI                       |történelem szakos tanár, hittan tanár            |etika, mentoridő, történelem                                                                                                                                           |Pázmány Péter Katolikus Egyetem        |PTF004581 PTF028193      |tanár                                                                  |
|Csányi András                        |NÁNÁSI                         |tanító                                           |etika, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                                                            |Tessedik Sámuel Főiskola               |PTD030376                |tanító                                                                 |
|Dudás Fanni                          |POZSONYI                       |gyógypedagógus                                   |magyar nyelv és irodalom, mentoridő, technika és tervezés, testnevelés, ének-zene                                                                                      |Eötvös Loránd Tudományegyetem          |PTE001950                |tanító                                                                 |
|Fábián Anna Mária (Kovács Anna Mária)|RAKPART,SOLYMAR                |tanító                                           |etika, magyar nyelv és irodalom, matematika, mentoridő, technika és tervezés, testnevelés, ének-zene                                                                   |Kolozsvári Babes-Bolyai Tudományegyetem|208538                   |tanító                                                                 |
|Fésűs Bernadett Rita                 |NÁNÁSI,POZSONYI,RAKPART,SOLYMAR|matematika-kémia szakos tanár, fejlesztőpedagógus|fizika, kémia, matematika, technika és tervezés                                                                                                                        |Eötvös Loránd Tudományegyetem          |PTC007829                |tanár                                                                  |
|Fülöp Gabriella                      |BREZNO,NÁNÁSI,RAKPART          |magyar-orosz szakos tanár, angol nyelvtanár      |első élő idegen nyelv, magyar nyelv és irodalom, mentoridő, második idegen nyelv                                                                                       |Eötvös Loránd Tudományegyetem          |384/87                   |tanár                                                                  |
|Fülöp László                         |POZSONYI                       |tanító                                           |magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                                                                   |Eötvös Loránd Tudományegyetem          |PTH045169                |tanító                                                                 |
|Gaál-Horváth Bernadett               |RAKPART,SOLYMAR                |tanító                                           |digitális kultúra, etika, környezetismeret, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, ének-zene                                         |Eötvös Loránd Tudományegyetem          |PTH045170                |tanító                                                                 |
|James John Veryzer                   |NÁNÁSI                         |tanító                                           |etika, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                                                            |The University of Melbourne            |198905630                |tanító                                                                 |
|Kara Dávid                           |NÁNÁSI,POZSONYI,RAKPART        |képzőművész tanár                                |mentoridő, vizuális kultúra                                                                                                                                            |Magyar Képzőművészeti Egyetem          |PTD039271                |tanár                                                                  |
|Kemenes-Nagy Dominika Rita           |BREZNO,NÁNÁSI,RAKPART,SOLYMAR  |gyógypedagógus                                   |etika, magyar nyelv és irodalom, matematika, mentoridő, vizuális kultúra                                                                                               |Eötvös Loránd Tudományegyetem          |ELTE-BGGYK-1048/2014     |tanító                                                                 |
|Kökényesi Ágnes                      |POZSONYI,RAKPART,SOLYMAR       |biológia és környezettan szakos tanár            |biológia, természettudomány                                                                                                                                            |Eötvös Loránd Tudományegyetem          |PTE007224                |tanár                                                                  |
|Kollányi Fruzsina                    |POZSONYI,SOLYMAR               |matematika szakos tanár                          |matematika                                                                                                                                                             |Eötvös Loránd Tudományegyetem          |PTE007338                |tanár                                                                  |
|Koren Balázs                         |RAKPART                        |matematika szakos tanár                          |matematika, mentoridő                                                                                                                                                  |Budapesti Műszaki Egyetem              |PTB/BL000313             |tanár                                                                  |
|Kovács Beáta                         |NÁNÁSI                         |tanító                                           |digitális kultúra, etika, környezetismeret, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                       |Károli Gáspár Református Egyetem       |PTE018210                |tanító                                                                 |
|Kovács Krisztián                     |NÁNÁSI,POZSONYI                |sportedző                                        |testnevelés                                                                                                                                                            |Szinergia Üzleti Szakképző Főiskola    |371189                   |testnevelés tantárgy tanítására alkalmazott sportedző (Nkt. 98. § (16))|
|Kovács Krisztina                     |NÁNÁSI,POZSONYI,SOLYMAR        |magyar-történelem szakos tanár                   |magyar nyelv és irodalom, történelem, állampolgári ismeretek                                                                                                           |Eszterházy Károly Főiskola             |PTF037687                |tanár                                                                  |
|Mészáros Emőke                       |(POZSONYI)                     |matematika szakos tanár                          |                                                                                                                                                                       |Eötvös Loránd Tudományegyetem          |PTF003707                |tanár                                                                  |
|Mohai Mónika                         |RAKPART                        |francia nyelv és történelem szakos tanár         |mentoridő, történelem, állampolgári ismeretek                                                                                                                          |Eötvös Loránd Tudományegyetem          |PTO001439                |tanár                                                                  |
|Monori Ágnes                         |NÁNÁSI,SOLYMAR                 |tanító                                           |digitális kultúra, első élő idegen nyelv, etika, környezetismeret, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene|Eötvös Loránd Tudományegyetem          |PTD040871                |tanító                                                                 |
|Papp Borbála                         |POZSONYI                       |gyógypedagógus                                   |digitális kultúra, környezetismeret, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                              |Eötvös Loránd Tudományegyetem          |PTF6533                  |tanító                                                                 |
|Perczel-Szabó Katalin                |SOLYMAR                        |tanító                                           |etika, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                                                            |Berzsenyi Dániel Tanárképző Főiskola   |PTC027103                |tanító                                                                 |
|Pintér Zsuzsanna                     |BREZNO,SOLYMAR                 |filozófia szakos tanár                           |etika                                                                                                                                                                  |Eötvös Loránd Tudományegyetem          |PTG003834                |tanár                                                                  |
|Porteleki Sára                       |BREZNO,SOLYMAR                 |tanító                                           |első élő idegen nyelv, környezetismeret, matematika, mentoridő, testnevelés, vizuális kultúra, ének-zene                                                               |Apor Vilmos Katolikus Főiskola         |PTH001472                |tanító                                                                 |
|Schubert Tamás                       |RAKPART                        |magyar-történelem szakos tanár                   |magyar nyelv és irodalom, mentoridő                                                                                                                                    |Eötvös Loránd Tudományegyetem          |PTE002724                |tanár                                                                  |
|Szabady Helga                        |NÁNÁSI                         |biológia szakos tanár                            |természettudomány                                                                                                                                                      |József Attila Tudományegyetem          |218/1989.                |tanár                                                                  |
|Szabó Adrienn                        |BREZNO,POZSONYI,SOLYMAR        |tanító                                           |digitális kultúra, első élő idegen nyelv, környezetismeret, magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene       |Eötvös Loránd Tudományegyetem          |PTB066644                |tanító                                                                 |
|Szabó Bálint                         |RAKPART                        |matematika-fizika szakos tanár                   |fizika, matematika, mentoridő, technika és tervezés, természettudomány                                                                                                 |Eötvös Loránd Tudományegyetem          |PTF000805                |tanár                                                                  |
|Szende Krisztina                     |POZSONYI,RAKPART,SOLYMAR       |angol nyelvtanár                                 |első élő idegen nyelv, második idegen nyelv                                                                                                                            |Eötvös Loránd Tudományegyetem          |PTG002063                |tanár                                                                  |
|Varga Virág                          |RAKPART                        |angol nyelvtanár, rajz szakos tanár              |első élő idegen nyelv, második idegen nyelv, vizuális kultúra                                                                                                          |Szegedi Tudományegyetem                |218/2004 PTE007424       |tanár                                                                  |
|Zsemberovszky Zita                   |POZSONYI,RAKPART,SOLYMAR       |magyar szakos tanár, földrajz szakos tanár       |földrajz, magyar nyelv és irodalom                                                                                                                                     |Pécsi Tudományegyetem                  |PTJ039378                |tanár                                                                  |
|Zsigmond Alexandra Petra             |BREZNO                         |gyógypedagógus                                   |magyar nyelv és irodalom, matematika, technika és tervezés, testnevelés, vizuális kultúra, ének-zene                                                                   |Eötvös Loránd Tudományegyetem          |PTF006543                |tanító                                                                 |
# Mentorok (de nem szaktanárok) jegyzéke
|     Tanár     |Feladatellátási hely|        Végzettség(ek)         |       Diploma kiállítója       | Diploma száma  |              Nkt. 3. sz. Melléklet szerinti alkalmazáshoz szükséges szakképzettség              |
|---------------|--------------------|-------------------------------|--------------------------------|----------------|-------------------------------------------------------------------------------------------------|
|Csóti Henriett |POZSONYI            |neveléstudomány szakos bölcsész|Eötvös Loránd Tudományegyetem   |PTF007543       |fejlesztő pedagógus                                                                              |
|Csóti Orsolya  |BREZNO              |neveléstudomány szakos bölcsész|Eötvös Loránd Tudományegyetem   |PTK001359       |fejlesztő pedagógus                                                                              |
|Dávid Luca     |NÁNÁSI              |jelmez- és bábtervező          |Magyar Képzőművészeti Egyetem   |PTC041756       |Alapfokú művészeti iskola szín- és bábművészeti, ág: a tanszaknak megfelelő művész szakképzettség|
|Inkrét Boglárka|SOLYMAR             |pszichológus                   |Kéroli Gáspár Református Egyetem|PTJ048692       |iskolapszichológus                                                                               |
|Kántor Katalin |POZSONYI            |óvodapedagógus                 |Apor Vilmos Katolikus Főiskola  |PTB057887       |óvodapedagógus, fejlesztő pedagógus                                                              |
|Kelen Anna     |POZSONYI            |óvodapedagógus                 |Eötvös Loránd Tudományegyetem   |PTG007527       |óvodapedagógus, fejlesztő pedagógus                                                              |
|Kéri Júlia     |POZSONYI            |pszichológia szakos tanár      |Eötvös Loránd Tudományegyetem   |PTH006541       |tanár                                                                                            |
|Kovács Ágnes   |SOLYMAR             |beszédfejlesztő tanár          |Eötvös Loránd Tudományegyetem   |PTD041891       |fejlesztő pedagógus                                                                              |
|Kovalóczy Anna |SOLYMAR             |pszichológus (viselkedéselemző)|Károli Gáspár Református Egyetem|KRE-BA-1617/2014|iskolapszichológus                                                                               |
|Major Rita     |POZSONYI            |teológus                       |Evangélikus Hittudományi Egyetem|PTF003468       |tanár                                                                                            |
|Pilz Fanni     |NÁNÁSI              |pszichológus (viselkedéselemző)|Károli Gáspár Református Egyetem|PTE055277       |iskolapszichológus                                                                               |
|Rupert Zsófia  |NÁNÁSI              |óvodapedagógus                 |Eötvös Loránd Tudományegyetem   |PTF005526       |óvodapedagógus, fejlesztő pedagógus                                                              |
|Szekeres Eszter|RAKPART             |pszichológus                   |Károli Gáspár Református Egyetem|PTE054418       |iskolapszichológus                                                                               |
# Külsős tanárok aránya 
Összes tanított tanóra:  1225
Külsős által tanított tanóra:  188
Külsős által tanított tanáórák aránya: 15 %
## Mentorok
* BREZNO telephelyen összesen 6 mentor van, ebből 1 külsős.
* NÁNÁSI telephelyen összesen 3 mentor van, ebből 0 külsős.
* POZSONYI telephelyen összesen 7 mentor van, ebből 0 külsős.
* RAKPART telephelyen összesen 7 mentor van, ebből 0 külsős.
* SOLYMAR telephelyen összesen 4 mentor van, ebből 0 külsős.
# Mentorok telephelyenként
|Telephely|              Mentor              |
|---------|----------------------------------|
|BREZNO   |b.hanna@budapestschool.org        |
|BREZNO   |csoti.orsi@budapestschool.org     |
|BREZNO   |domino@budapestschool.org         |
|BREZNO   |fulop.gabi@budapestschool.org     |
|BREZNO   |kulsos                            |
|BREZNO   |p.sari@budapestschool.org         |
|NÁNÁSI   |fanni.pilz@budapestschool.org     |
|NÁNÁSI   |luca.david@budapestschool.org     |
|NÁNÁSI   |rupert.zsofi@budapestschool.org   |
|POZSONYI |fanni.dudas@budapestschool.org    |
|POZSONYI |heni@budapestschool.org           |
|POZSONYI |kati.kantor@budapestschool.org    |
|POZSONYI |kelen.panni@budapestschool.org    |
|POZSONYI |keri.julia@budapestschool.org     |
|POZSONYI |kristof.csagoly@budapestschool.org|
|POZSONYI |rita.major@budapestschool.org     |
|RAKPART  |balazs.koren@budapestschool.org   |
|RAKPART  |balint.szabo@budapestschool.org   |
|RAKPART  |david.kara@budapestschool.org     |
|RAKPART  |eszter.szekeres@budapestschool.org|
|RAKPART  |hajnalka.banati@budapestschool.org|
|RAKPART  |monika.mohai@budapestschool.org   |
|RAKPART  |tamas.schubert@budapestschool.org |
|SOLYMAR  |anna.fabian@budapestschool.org    |
|SOLYMAR  |anna.kovaloczy@budapestschool.org |
|SOLYMAR  |bogi.inkret@budapestschool.org    |
|SOLYMAR  |kovacs.agi@budapestschool.org     |
