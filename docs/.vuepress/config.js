toc = require("./toc.js");
module.exports = {
  title: "Tanulni tanulunk",
  description: "A Budapest School Modell ",
  base: "/",
  dest: "public",
  plugins: [
    [
      "check-md",
      {
        pattern: "**/*.md",
      },
      "@vuepress/google-analytics",
      {
        ga: "UA-69820281-7", // UA-00000000-0
      },
      "vuepress-plugin-medium-zoom",
      {
        //  selector: '.my-wrapper .my-img',
        delay: 1000,
        options: {
          margin: 24,
          background: "#BADA55",
          scrollOffset: 0,
        },
      },
    ],
    [
      "sitemap",
      {
        hostname: "https://model.budapestschool.org",
      },
    ],
  ],
  themeConfig: {
    logo: "/bps_logo.png",
    nav: [
      { text: "BPS iskolák", link: "https://budapestschool.org" },
      { text: "Akkreditáció", link: "/jogszabalyok/akkreditacio.html" },
    ],
    sidebar: toc.sidebar,
    lastUpdated: "Utolsó modosítás",
  },
};
