const fs = require("fs");
const path = require("path");
const { exec } = require("child_process");

// Adjust the path to your toc.js file
const toc = require("./docs/.vuepress/toc.js");

// Function to extract file paths, titles, and levels from the sidebar
function getChapters(items, baseDir = "", level = 1) {
  const chapters = [];

  items.forEach(item => {
    let chapter = {
      title: item.title,
      sections: []
    };

    item.children.forEach(child => {
      const filePath = path.join(baseDir, child[0]);
      const title = child[1];

      chapter.sections.push({ title, filePath });
    });
    chapters.push(chapter);
  });
  return chapters;
}

function adjustHeadingLevels(content, increaseBy) {
  return content.replace(/^(#{1,6})(\s)/gm, function(match, hashes, space) {
    const newLevel = Math.min(hashes.length + increaseBy, 6);
    return "#".repeat(newLevel) + space;
  });
}

// Define the metadata
const metadata = `---
title: "Budapest School Egyedi Pedagógia Programja és Szervezeti Működési Szabályzata"
author: "BPS-tanárok"
date: "${new Date().toLocaleDateString("hu-HU")}"
lang: "hu"
geometry: margin=1in
---

`;

// Start with the metadata
let combinedContent = metadata;

// Get the list of files from the sidebar
const chapters = getChapters(toc.sidebar);

chapters.forEach(chapter => {
  // Add chapter title as level 1 heading
  combinedContent += `# ${chapter.title}\n\n`;

  chapter.sections.forEach(section => {
    // Resolve the full path
    const fullPath = path.resolve(__dirname, "docs", section.filePath);

    // Check if the file exists
    if (fs.existsSync(fullPath)) {
      let content = fs.readFileSync(fullPath, "utf8");

      // Adjust heading levels in the content
      content = adjustHeadingLevels(content, 1);

      combinedContent += content + "\n\n";
    } else {
      console.warn(`File not found: ${fullPath}`);
    }
  });
});

// Write the combined content to a new Markdown file
fs.writeFileSync("combined.md", combinedContent);

console.log("Combined Markdown file created as combined.md");

// Optional: Convert the combined Markdown file to PDF using Pandoc
exec(
  "pandoc combined.md -o output.pdf --pdf-engine=xelatex  --toc",
  (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing Pandoc: ${error.message}`);
      return;
    }
    if (stderr) {
      console.error(`Pandoc stderr: ${stderr}`);
      return;
    }
    console.log("PDF file created as output.pdf");
  }
);
